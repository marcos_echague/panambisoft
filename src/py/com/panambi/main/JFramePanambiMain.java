package py.com.panambi.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import py.com.panambi.bean.Session;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorSession;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.informes.JIConsultaProductos;
import py.com.panambi.informes.JIConsultaProveedores;
import py.com.panambi.informes.JIConsultarAuditoriaGeneral;
import py.com.panambi.informes.JIConsultarCuotasAtrasadas;
import py.com.panambi.informes.JIReportePagosEmpleados;
import py.com.panambi.informes.JIConsultarRegistroProductoPerdido;
import py.com.panambi.informes.JIGenerarCodigoBarra;
import py.com.panambi.informes.JIGenerarExtractoCuotas;
import py.com.panambi.informes.JIGenerarPagare;
import py.com.panambi.informes.JIReporteCompras;
import py.com.panambi.informes.JIReporteFlujoCaja;
import py.com.panambi.informes.JIReporteGastos;
import py.com.panambi.informes.JIReporteIngresosEgresos;
import py.com.panambi.informes.JIReporteVentas;
import py.com.panambi.login.DlgLogin;
import py.com.panambi.login.DlgLoginValidaAdministrador;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JPanelTittle;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.windows.DlgAcercade;
import py.com.panambi.swing.windows.DlgChangePassword;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JIAnularFactura;
import py.com.panambi.swing.windows.JIAsignacionDescuentoTemporada;
import py.com.panambi.swing.windows.JIAsignarEmpleadoUsuario;
import py.com.panambi.swing.windows.JIBanco;
import py.com.panambi.swing.windows.JIBonificacion;
import py.com.panambi.swing.windows.JICargoEmpleado;
import py.com.panambi.swing.windows.JICliente;
import py.com.panambi.swing.windows.JICompra;
import py.com.panambi.swing.windows.JIConsultarAnularCompra;
import py.com.panambi.swing.windows.JIConsultarAnularDeudaEmpleado;
import py.com.panambi.swing.windows.JIConsultarAnularDevolucionProducto;
import py.com.panambi.swing.windows.JIConsultarAnularGasto;
import py.com.panambi.swing.windows.JIConsultarAnularPagoCuota;
import py.com.panambi.swing.windows.JIConsultarAnularPagoSalario;
import py.com.panambi.swing.windows.JIConsultarAnularProductoPerdido;
import py.com.panambi.swing.windows.JIConsultarDevolucion;
import py.com.panambi.swing.windows.JIConsultarNotaCredito;
import py.com.panambi.swing.windows.JIConsultarStock;
import py.com.panambi.swing.windows.JIConsultarVenta;
import py.com.panambi.swing.windows.JIDevolucionProducto;
import py.com.panambi.swing.windows.JIEmpleado;
import py.com.panambi.swing.windows.JIFormaPago;
import py.com.panambi.swing.windows.JIGasto;
import py.com.panambi.swing.windows.JIMovimientoProducto;
import py.com.panambi.swing.windows.JIPagoAguinaldo;
import py.com.panambi.swing.windows.JIPagoCuota;
import py.com.panambi.swing.windows.JIPagoLiquidacion;
import py.com.panambi.swing.windows.JIPagoProveedores;
import py.com.panambi.swing.windows.JIPagoSalario;
import py.com.panambi.swing.windows.JIParametros;
import py.com.panambi.swing.windows.JIPerfilUsuario;
import py.com.panambi.swing.windows.JIPermiso;
import py.com.panambi.swing.windows.JIProducto;
import py.com.panambi.swing.windows.JIProductoPerdido;
import py.com.panambi.swing.windows.JIProveedor;
import py.com.panambi.swing.windows.JISucursal;
import py.com.panambi.swing.windows.JITemporada;
import py.com.panambi.swing.windows.JITipoDescuentoSalario;
import py.com.panambi.swing.windows.JITipoGasto;
import py.com.panambi.swing.windows.JIUsuario;
import py.com.panambi.swing.windows.JIVenta;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JFramePanambiMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5681372437553119135L;
	public static Session session = null;
	public static Properties appproperties = new Properties();
	private JDesktopPane desktopPane;
	private JPanelTittle panelTittle;
	private JPanel panelSuperior;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem;
	private JMenu menu_1;
	private JMenuItem menuItem_1;
	private JMenu menu_2;
	private JMenuItem menuItem_4;
	private JMenuItem menuItem_5;
	private JMenu menu_3;
	private JMenuItem menuItem_6;
	private JMenuItem menuItem_7;
	private JMenuItem menuItem_8;
	private JMenuItem mntmAsignarDescuentoPor;
	private JMenu menu_4;
	private JMenu mnmRRHH;
	private JMenuItem mntmCargos;
	private JMenuItem mntmPagoSalario;
	private JPanel panelMenu;
	static Logger logger = Logger.getLogger(JFramePanambiMain.class.getName());
	private JToolBar toolBar;
	private JButtonPanambi btnVentas;
	private JButtonPanambi btnCompras;
	private JMenuItem mntmUsuarios;
	private JMenuItem mntmPerfilesDeUsuarios;
	private JMenuItem mntmTiposDeDescuentos;
	private JMenu mnAyuda;
	private JMenuItem mntmAyuda;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	private JMenu mnEmpleados;
	private JMenuItem mntmGestionarEmpleados;
	private JMenuItem mntmAsignarsacarUsuario;
	private JMenu mnReportes;
	private JMenuItem mntmProductos;
	private JMenuItem mntmListarProveedores;
	private JMenuItem mntmGenerarCodigoDe;
	private JMenu mnGastos;
	private JMenuItem mntmRegistrarGasto;
	private JMenuItem mntmTiposDeGastos;
	private JMenu mnProductoPerdido;
	private JMenuItem mntmRegistrarProductoPerdido;
	private JMenuItem mntmConsultarProductosPerdidos;
	private JMenuItem mntmListarProductosPerdidos;
	private JMenu mnCoutas;
	private JMenuItem mntmListarCuotasAtrasadas;
	private JMenuItem mntmGenerarExtractoDe;
	private JMenuItem mntmProveedores;
	private JMenuItem mntmFormasDePago;
	private JMenuItem mntmSucursales;
	private JMenuItem mntmBancos;
	private JMenuItem mntmPagoDeAguinaldos;
	private JMenuItem mntmPagoDeLiquidacion;
	private JMenuItem menuPagoProveedores;
	private JMenu mnDevolucionDeProducto;
	private JMenuItem mntmRegistrarDevolucion;
	private JMenuItem mntmConsultaranularDevolucion;
	private JMenu mnCuotas;
	private JMenuItem mntmRegistrarPagoDe;
	private JMenuItem mntmConsultaranularPagoDe;
	private JMenuItem mntmAnularFactura;
	private JMenuItem mntmAuditoria;
	private JMenuItem mntmPermisos;
	private JMenuItem mntmNotasDeCredito;
	private JMenuItem mntmConsultaranularGastos;
	private JMenuItem mntmBonificaciones;
	private JMenuItem mntmConsultarVentas;
	private JMenuItem mntmConsultaranularCompras;
	private JButtonPanambi btnAyuda;
	private JButtonPanambi btnSalir;
	private JMenuItem mntmReporteDeCompras;
	private JMenuItem mntmReporteDeVentas;
	private JMenuItem mntmImpresionPagares;
	private JMenuItem mntmReporteIngresosEgresos;
	private JMenuItem mntmConsultarStock;
	private JMenuItem mntmReporteDeFlujo;
	private JMenuItem mntmConsultaranularPagosA;
	private JMenuItem mntmACercaDe;
	private List<Sucursal> sucursales = new ArrayList<Sucursal>();
	private JMenu mnSucursalOperativa;
	private JMenuItem mntmCambiarContrasea;
	private JMenuItem mntmConsultaranularDeudasDe;
	private JMenuItem mntmConsultarDevoluciones;
	private JMenuItem mntmReporteDeGastos;
	private JMenuItem mntmReporteDePagos;
	private JMenuItem mntmParametrosDelSistema;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			appproperties.load(new FileInputStream("conf/panambi.properties"));
			Properties log4j = new Properties();
			log4j.load(new FileInputStream(new File("conf/log4j.properties")));
			PropertyConfigurator.configure(log4j);
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JFramePanambiMain frame = new JFramePanambiMain();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
						logger.error(e.getMessage(), e);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public JFramePanambiMain() {
		initialize();
	}

	private void initialize() {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			UIManager.put("Button.defaultButtonFollowsFocus", true);
			UIManager.put("Button.showMnemonics", true);
			UIManager.put("ToolTip.font", FontPanambi.getTableInstance());
			SwingUtilities.updateComponentTreeUI(this);
			setIconImage(Toolkit.getDefaultToolkit().getImage(JFramePanambiMain.class.getResource("/py/com/panambi/images/mariposaDerecha.png")));
			setTitle("Panambi Soft");
			setName("JFrameMain");
			setBounds(100, 100, 847, 523);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			DlgLogin dlgLogin = new DlgLogin(this);
			dlgLogin.setVisible(true);
			if (JFramePanambiMain.session == null) {
				System.exit(0);
			}
			
			getContentPane().add(getDesktopPane(), BorderLayout.CENTER);
			setExtendedState(JFrame.MAXIMIZED_BOTH);
			getContentPane().add(getPanelSuperior(), BorderLayout.NORTH);
			

			fichero = new File("./bin/py/com/panambi/help/help_set.hs");
			hsURL = fichero.toURI().toURL();

			helpset = new HelpSet(getClass().getClassLoader(), hsURL);
			hb = helpset.createHelpBroker();

			hb.enableHelpOnButton(mntmAyuda, "presentacion", helpset);
			hb.enableHelpKey(getRootPane(), "presentacion", helpset);
			this.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					doExit();
					super.windowClosed(e);
				}

				@Override
				public void windowClosing(WindowEvent e) {
					doExit();
					super.windowClosing(e);
				}
			});
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	private void doExit() {
		try {
			File tmpdir = new File("tmp");
			if (tmpdir.exists()) {
				if (tmpdir.isDirectory()) {
					File[] tempfiles = tmpdir.listFiles();
					for (int i = 0; i < tempfiles.length; i++) {
						File file = tempfiles[i];
						try {
							file.deleteOnExit();
						} catch (Exception e) {
						}
					}
				}
			}
			if (JFramePanambiMain.session != null && JFramePanambiMain.session.getConn() != null) {
				ControladorSession controladorSession = new ControladorSession();
				controladorSession.finalizarSesion(JFramePanambiMain.session.getConn(), JFramePanambiMain.session);
				JFramePanambiMain.session.getConn().close();
			}
			System.exit(0);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	public JDesktopPane getDesktopPane() {
		if (desktopPane == null) {
			desktopPane = new JDesktopPane();
			desktopPane.setBackground(new Color(112, 128, 144));
		}
		return desktopPane;
	}

	private JPanelTittle getPanelTittle() {
		if (panelTittle == null) {
			panelTittle = new JPanelTittle();
			panelTittle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelTittle.setBackground(new Color(210, 180, 140));
		}
		return panelTittle;
	}

	private JPanel getPanelSuperior() throws Exception {
		if (panelSuperior == null) {
			panelSuperior = new JPanel();
			panelSuperior.setLayout(new BoxLayout(panelSuperior, BoxLayout.Y_AXIS));
			panelSuperior.add(getPanelTittle());
			panelSuperior.add(getPanelMenu());
		}
		return panelSuperior;
	}

	private JMenuBar getMenuBar_1() throws Exception {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.setMaximumSize(new Dimension(1920, 30));
			menuBar.setMinimumSize(new Dimension(0, 30));
			menuBar.setPreferredSize(new Dimension(800, 30));
			menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
			menuBar.setMargin(new Insets(2, 2, 2, 2));
			menuBar.add(getMenu());
			menuBar.add(getMenu_1());
			menuBar.add(getMenu_2());
			menuBar.add(getMenu_3());
			menuBar.add(getMenu_4());
			menuBar.add(getMnReportes());
			menuBar.add(getMnSucursalOperativa());
			menuBar.add(getMnAyuda());
		}
		return menuBar;
	}

	private JMenu getMenu() {
		if (menu == null) {
			menu = new JMenu("Inicio");
			menu.setMnemonic('I');
			menu.add(getMntmCambiarContrasea());
			menu.add(getMenuItem());
		}
		return menu;
	}

	private JMenuItem getMenuItem() {
		if (menuItem == null) {
			menuItem = new JMenuItem("Salir");
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		}
		return menuItem;
	}

	private void doSalir() {
		Integer ret = JOptionPane.showConfirmDialog(this, "Desea salir del sistema ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (ret == JOptionPane.YES_OPTION) {
			doExit();
		}
	}

	private JMenu getMenu_1() {
		if (menu_1 == null) {
			menu_1 = new JMenu("Compras");
			menu_1.setMnemonic('C');
			menu_1.add(getMenuItem_1());
			menu_1.add(getMntmConsultaranularCompras());
			menu_1.add(getMenuPagoProveedores());
			menu_1.add(getMnGastos());
		}
		return menu_1;
	}

	private JMenuItem getMenuItem_1() {
		if (menuItem_1 == null) {
			menuItem_1 = new JMenuItem("Registrar compra");
			menuItem_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
			menuItem_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarCompra();
				}
			});
		}
		return menuItem_1;
	}

	private void doRegistrarCompra() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				//EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JICompra jicompra = new JICompra();
						getDesktopPane().add(jicompra);
						jicompra.centerIt();
						jicompra.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});

		} finally {
			PanambiUtils.setDefaultCursor(this);
		}

	}

	private void doProveedores() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIProveedor jalta = new JIProveedor();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMenu_2() {
		if (menu_2 == null) {
			menu_2 = new JMenu("Ventas");
			menu_2.setMnemonic('V');
			menu_2.add(getMenuItem_4());
			menu_2.add(getMntmConsultarVentas());
			menu_2.add(getMenuItem_5());
			menu_2.add(getMnCuotas());
			menu_2.add(getMnDevolucionDeProducto());
			menu_2.add(getMntmAnularFactura());
		}
		return menu_2;
	}

	private JMenuItem getMenuItem_4() {
		if (menuItem_4 == null) {
			menuItem_4 = new JMenuItem("Registar venta");
			menuItem_4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
			menuItem_4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doRegistrarVenta();
				}
			});
		}
		return menuItem_4;
	}

	private void doRegistrarVenta() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIVenta jalta = new JIVenta();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMenuItem_5() {
		if (menuItem_5 == null) {
			menuItem_5 = new JMenuItem("Clientes");
			menuItem_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
			menuItem_5.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCliente();
				}
			});
		}
		return menuItem_5;
	}

	private void doCliente() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JICliente jalta = new JICliente();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMenu_3() {
		if (menu_3 == null) {
			menu_3 = new JMenu("Productos");
			menu_3.setMnemonic('P');
			menu_3.add(getMenuItem_6());
			menu_3.add(getMenuItem_7());
			menu_3.add(getMntmGenerarCodigoDe());
			menu_3.add(getMenuItem_8());
			menu_3.add(getMnProductoPerdido());
			menu_3.add(getMntmAsignarDescuentoPor());
			menu_3.add(getMntmConsultarStock());
		}
		return menu_3;
	}

	private JMenuItem getMenuItem_6() {
		if (menuItem_6 == null) {
			menuItem_6 = new JMenuItem("Gestionar producto");
			menuItem_6.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
			menuItem_6.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doProductos();
				}
			});
		}
		return menuItem_6;
	}

	private JMenuItem getMenuItem_7() {
		if (menuItem_7 == null) {
			menuItem_7 = new JMenuItem("Gestionar temporada");
			menuItem_7.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
			menuItem_7.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTemporadas();
				}
			});
		}
		return menuItem_7;
	}

	private JMenuItem getMenuItem_8() {
		if (menuItem_8 == null) {
			menuItem_8 = new JMenuItem("Movimiento de producto");
			menuItem_8.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
			menuItem_8.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doMovimientoProducto();
				}
			});
		}
		return menuItem_8;
	}

	private void doMovimientoProducto() {
		try {
			PanambiUtils.setWaitCursor(this);

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JIMovimientoProducto jalta = new JIMovimientoProducto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});

		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmAsignarDescuentoPor() {
		if (mntmAsignarDescuentoPor == null) {
			mntmAsignarDescuentoPor = new JMenuItem("Asignar descuento por temporada");
			mntmAsignarDescuentoPor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
			mntmAsignarDescuentoPor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAsignarDescuentoTemporada();
				}
			});
		}
		return mntmAsignarDescuentoPor;
	}

	private JMenu getMenu_4() {
		if (menu_4 == null) {
			menu_4 = new JMenu("Administrar");
			menu_4.setMnemonic('A');
			menu_4.add(getMnmRRHH());
			menu_4.add(getMntmUsuarios());
			menu_4.add(getMntmPerfilesDeUsuarios());
			menu_4.add(getMntmProveedores());
			menu_4.add(getMntmFormasDePago());
			menu_4.add(getMntmSucursales());
			menu_4.add(getMntmBancos());
			menu_4.add(getMntmPermisos());
			menu_4.add(getMntmParametrosDelSistema());

		}
		return menu_4;
	}

	private JMenu getMnmRRHH() {
		if (mnmRRHH == null) {
			mnmRRHH = new JMenu("RRHH");
			mnmRRHH.setMnemonic('R');
			mnmRRHH.add(getMnEmpleados());
			mnmRRHH.add(getMntmCargos());
			mnmRRHH.add(getMntmTiposDeDescuentos());
			mnmRRHH.add(getMntmBonificaciones());
			mnmRRHH.add(getMenuItem_15());
			mnmRRHH.add(getMntmPagoDeAguinaldos());
			mnmRRHH.add(getMntmPagoDeLiquidacion());
			mnmRRHH.add(getMntmConsultaranularPagosA());
			mnmRRHH.add(getMntmConsultaranularDeudasDe());
		}
		return mnmRRHH;
	}

	private void doGasto() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIGasto jalta = new JIGasto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doGestionarEmpleados() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIEmpleado jalta = new JIEmpleado();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmCargos() {
		if (mntmCargos == null) {
			mntmCargos = new JMenuItem("Cargos");
			mntmCargos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmCargos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCargoEmpleado();
				}
			});
		}
		return mntmCargos;
	}

	private void doCargoEmpleado() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JICargoEmpleado jalta = new JICargoEmpleado();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doAsignarDescuentoTemporada() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIAsignacionDescuentoTemporada jalta = new JIAsignacionDescuentoTemporada();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMenuItem_15() {
		if (mntmPagoSalario == null) {
			mntmPagoSalario = new JMenuItem("Pago de salarios");
			mntmPagoSalario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmPagoSalario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagoSalario();
				}
			});
		}
		return mntmPagoSalario;
	}

	private void doPagoSalario() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPagoSalario jalta = new JIPagoSalario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doTemporadas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						JITemporada jalta = new JITemporada();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JPanel getPanelMenu() throws Exception {
		if (panelMenu == null) {
			panelMenu = new JPanel();
			panelMenu.setBackground(null);
			GridBagLayout gbl_panelMenu = new GridBagLayout();
			gbl_panelMenu.columnWidths = new int[] { 289, 0 };
			gbl_panelMenu.rowHeights = new int[] { 23, 16, 0 };
			gbl_panelMenu.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
			gbl_panelMenu.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
			panelMenu.setLayout(gbl_panelMenu);
			GridBagConstraints gbc_menuBar = new GridBagConstraints();
			gbc_menuBar.fill = GridBagConstraints.HORIZONTAL;
			gbc_menuBar.anchor = GridBagConstraints.NORTH;
			gbc_menuBar.insets = new Insets(0, 0, 5, 0);
			gbc_menuBar.gridx = 0;
			gbc_menuBar.gridy = 0;
			panelMenu.add(getMenuBar_1(), gbc_menuBar);
			GridBagConstraints gbc_toolBar = new GridBagConstraints();
			gbc_toolBar.anchor = GridBagConstraints.WEST;
			gbc_toolBar.gridx = 0;
			gbc_toolBar.gridy = 1;
			panelMenu.add(getToolBar(), gbc_toolBar);
		}
		return panelMenu;
	}

	private JToolBar getToolBar() throws Exception {
		if (toolBar == null) {
			toolBar = new JToolBar();
			toolBar.setFloatable(false);
			toolBar.setAlignmentX(Component.LEFT_ALIGNMENT);
			toolBar.add(getBtnVentas());
			toolBar.add(getBtnCompras());
			toolBar.add(getBtnAyuda());
			toolBar.add(getBtnSalir());
		}
		return toolBar;
	}

	private JButtonPanambi getBtnVentas() throws Exception {
		if (btnVentas == null) {
			btnVentas = new JButtonPanambi("");
			btnVentas.setToolTipText("Registro de Ventas");
			BufferedImage buffimg = ImageIO.read(new File(JFramePanambiMain.class.getResource("/py/com/panambi/images/ventas.png").toURI()));
			ImageIcon icon = new ImageIcon(buffimg.getScaledInstance(40, 40, Image.SCALE_SMOOTH));
			btnVentas.setIcon(icon);
			btnVentas.setSize(new Dimension(50, 50));
			btnVentas.setPreferredSize(new Dimension(50, 50));
			btnVentas.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					doRegistrarVenta();

				}
			});
			btnVentas.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						btnVentas.doClick();
					}
					super.keyPressed(e);
				}
			});

		}
		return btnVentas;
	}

	private JButtonPanambi getBtnCompras() throws Exception {
		if (btnCompras == null) {
			btnCompras = new JButtonPanambi("");
			btnCompras.setSize(new Dimension(50, 50));
			btnCompras.setToolTipText("Registro de Compras");
			BufferedImage buffimg = ImageIO.read(new File(JFramePanambiMain.class.getResource("/py/com/panambi/images/compras.png").toURI()));
			ImageIcon icon = new ImageIcon(buffimg.getScaledInstance(40, 40, Image.SCALE_SMOOTH));
			btnCompras.setIcon(icon);
			btnCompras.setPreferredSize(new Dimension(50, 50));
			btnCompras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarCompra();
				}
			});
			btnCompras.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						btnCompras.doClick();
					}
					super.keyPressed(e);
				}
			});
		}
		return btnCompras;
	}

	private JMenuItem getMntmUsuarios() {
		if (mntmUsuarios == null) {
			mntmUsuarios = new JMenuItem("Usuarios");
			mntmUsuarios.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
			mntmUsuarios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doUsuarios();
				}
			});
		}
		return mntmUsuarios;
	}

	private void doUsuarios() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						JIUsuario jalta = new JIUsuario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}

			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doProductos() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIProducto jalta = new JIProducto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmPerfilesDeUsuarios() {
		if (mntmPerfilesDeUsuarios == null) {
			mntmPerfilesDeUsuarios = new JMenuItem("Perfiles de usuarios");
			mntmPerfilesDeUsuarios.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
			mntmPerfilesDeUsuarios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPerfilUsuario();
				}
			});
		}
		return mntmPerfilesDeUsuarios;
	}

	private void doPerfilUsuario() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPerfilUsuario jalta = new JIPerfilUsuario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmTiposDeDescuentos() {
		if (mntmTiposDeDescuentos == null) {
			mntmTiposDeDescuentos = new JMenuItem("Tipos de descuentos salariales");
			mntmTiposDeDescuentos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmTiposDeDescuentos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTipoDescuentoSalarios();

				}
			});
		}
		return mntmTiposDeDescuentos;
	}

	private void doTipoDescuentoSalarios() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JITipoDescuentoSalario jalta = new JITipoDescuentoSalario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnAyuda() {
		if (mnAyuda == null) {
			mnAyuda = new JMenu("Otros");
			mnAyuda.setMnemonic('O');
			mnAyuda.add(getMntmAyuda());
			mnAyuda.add(getMntmACercaDe());
			mnAyuda.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
		}
		return mnAyuda;
	}

	private void doHelp() {
		try {
			PanambiUtils.setWaitCursor(this);
			try {

				hb.enableHelpOnButton(mntmAyuda, "presentacion", helpset);
				hb.enableHelpKey(getRootPane(), "presentacion", helpset);

			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Error al cargar la ayuda: " + e);
			}
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmAyuda() {
		if (mntmAyuda == null) {
			mntmAyuda = new JMenuItem("Ayuda");
			mntmAyuda.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
			mntmAyuda.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doHelp();
				}
			});
		}
		return mntmAyuda;
	}

	private JMenu getMnEmpleados() {
		if (mnEmpleados == null) {
			mnEmpleados = new JMenu("Empleados");
			mnEmpleados.setMnemonic('E');
			mnEmpleados.add(getMntmGestionarEmpleados());
			mnEmpleados.add(getMntmAsignarsacarUsuario());
		}
		return mnEmpleados;
	}

	private JMenuItem getMntmGestionarEmpleados() {
		if (mntmGestionarEmpleados == null) {
			mntmGestionarEmpleados = new JMenuItem("Gestionar empleados");
			mntmGestionarEmpleados.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
			mntmGestionarEmpleados.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGestionarEmpleados();
				}
			});
		}
		return mntmGestionarEmpleados;
	}

	private JMenuItem getMntmAsignarsacarUsuario() {
		if (mntmAsignarsacarUsuario == null) {
			mntmAsignarsacarUsuario = new JMenuItem("Asignar/Quitar usuario");
			mntmAsignarsacarUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmAsignarsacarUsuario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAsignarUsuarioAEmpleado();
				}
			});
		}
		return mntmAsignarsacarUsuario;
	}

	private void doAsignarUsuarioAEmpleado() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIAsignarEmpleadoUsuario jalta = new JIAsignarEmpleadoUsuario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnReportes() {
		if (mnReportes == null) {
			mnReportes = new JMenu("Reportes");
			mnReportes.setMnemonic('R');
			mnReportes.add(getMntmReporteDeCompras());
			mnReportes.add(getMntmReporteDeVentas());
			mnReportes.add(getMntmReporteDeGastos());
			mnReportes.add(getMntmReporteDePagos());
			mnReportes.add(getMntmReporteIngresosEgresos());
			mnReportes.add(getMntmReporteDeFlujo());
			mnReportes.add(getMnCoutas());
			mnReportes.add(getMntmProductos());
			mnReportes.add(getMntmListarProveedores());
			mnReportes.add(getMntmListarProductosPerdidos());
			mnReportes.add(getMntmAuditoria());
			mnReportes.add(getMntmImpresionPagares());
		}
		return mnReportes;
	}

	private JMenuItem getMntmProductos() {
		if (mntmProductos == null) {
			mntmProductos = new JMenuItem("Listar productos");
			mntmProductos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmProductos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doListarProductos();
				}
			});
		}
		return mntmProductos;
	}

	private void doListarProductos() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultaProductos jalta = new JIConsultaProductos();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmListarProveedores() {
		if (mntmListarProveedores == null) {
			mntmListarProveedores = new JMenuItem("Listar proveedores");
			mntmListarProveedores.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmListarProveedores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarProveedores();
				}
			});
		}
		return mntmListarProveedores;
	}

	private void doConsultarProveedores() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultaProveedores jalta = new JIConsultaProveedores();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmGenerarCodigoDe() {
		if (mntmGenerarCodigoDe == null) {
			mntmGenerarCodigoDe = new JMenuItem("Generar codigo de barras");
			mntmGenerarCodigoDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
			mntmGenerarCodigoDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarCodigoBarra();
				}
			});
		}
		return mntmGenerarCodigoDe;
	}

	private void doGenerarCodigoBarra() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIGenerarCodigoBarra jalta = new JIGenerarCodigoBarra();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doPagoCuotas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPagoCuota jalta = new JIPagoCuota();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void doTipoGasto() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JITipoGasto jalta = new JITipoGasto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnGastos() {
		if (mnGastos == null) {
			mnGastos = new JMenu("Gastos");
			mnGastos.setMnemonic('G');
			mnGastos.add(getMntmRegistrarGasto());
			mnGastos.add(getMntmConsultaranularGastos());
			mnGastos.add(getMntmTiposDeGastos());
		}
		return mnGastos;
	}

	private JMenuItem getMntmRegistrarGasto() {
		if (mntmRegistrarGasto == null) {
			mntmRegistrarGasto = new JMenuItem("Registrar gasto");
			mntmRegistrarGasto.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
			mntmRegistrarGasto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGasto();
				}
			});
		}
		return mntmRegistrarGasto;
	}

	private JMenuItem getMntmTiposDeGastos() {
		if (mntmTiposDeGastos == null) {
			mntmTiposDeGastos = new JMenuItem("Tipos de gastos");
			mntmTiposDeGastos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
			mntmTiposDeGastos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTipoGasto();
				}
			});
		}
		return mntmTiposDeGastos;
	}

	private JMenu getMnProductoPerdido() {
		if (mnProductoPerdido == null) {
			mnProductoPerdido = new JMenu("Productos dados de baja");
			mnProductoPerdido.setMnemonic('B');
			mnProductoPerdido.add(getMntmRegistrarProductoPerdido());
			mnProductoPerdido.add(getMntmConsultarProductosPerdidos());
		}
		return mnProductoPerdido;
	}

	private JMenuItem getMntmRegistrarProductoPerdido() {
		if (mntmRegistrarProductoPerdido == null) {
			mntmRegistrarProductoPerdido = new JMenuItem("Registrar producto dado de baja");
			mntmRegistrarProductoPerdido.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
			mntmRegistrarProductoPerdido.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarProductoPerdido();
				}
			});
		}
		return mntmRegistrarProductoPerdido;
	}

	private void doRegistrarProductoPerdido() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						JIProductoPerdido jalta = new JIProductoPerdido();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultarProductosPerdidos() {
		if (mntmConsultarProductosPerdidos == null) {
			mntmConsultarProductosPerdidos = new JMenuItem("Consultar productos dados de baja");
			mntmConsultarProductosPerdidos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmConsultarProductosPerdidos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarProductoPerdido();
				}
			});
		}
		return mntmConsultarProductosPerdidos;
	}

	private void doConsultarProductoPerdido() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularProductoPerdido jalta = new JIConsultarAnularProductoPerdido();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmListarProductosPerdidos() {
		if (mntmListarProductosPerdidos == null) {
			mntmListarProductosPerdidos = new JMenuItem("Listar productos dados de baja");
			mntmListarProductosPerdidos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmListarProductosPerdidos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRptConsultarProductoPerdido();
				}
			});
		}
		return mntmListarProductosPerdidos;
	}

	private void doRptConsultarProductoPerdido() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarRegistroProductoPerdido jalta = new JIConsultarRegistroProductoPerdido();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnCoutas() {
		if (mnCoutas == null) {
			mnCoutas = new JMenu("Cuotas");
			mnCoutas.setMnemonic('C');
			mnCoutas.add(getMntmGenerarExtractoDe());
			mnCoutas.add(getMntmListarCuotasAtrasadas());
		}
		return mnCoutas;
	}

	private JMenuItem getMntmListarCuotasAtrasadas() {
		if (mntmListarCuotasAtrasadas == null) {
			mntmListarCuotasAtrasadas = new JMenuItem("Listar cuotas atrasadas");
			mntmListarCuotasAtrasadas.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmListarCuotasAtrasadas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doRptCuotasAtrasadas();
				}
			});
		}
		return mntmListarCuotasAtrasadas;
	}

	private void doRptCuotasAtrasadas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarCuotasAtrasadas jalta = new JIConsultarCuotasAtrasadas();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmGenerarExtractoDe() {
		if (mntmGenerarExtractoDe == null) {
			mntmGenerarExtractoDe = new JMenuItem("Generar extracto de cuotas");
			mntmGenerarExtractoDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmGenerarExtractoDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doGenerarExtracto();
				}
			});
		}
		return mntmGenerarExtractoDe;
	}

	private void doGenerarExtracto() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIGenerarExtractoCuotas jalta = new JIGenerarExtractoCuotas();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmProveedores() {
		if (mntmProveedores == null) {
			mntmProveedores = new JMenuItem("Proveedores");
			mntmProveedores.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
			mntmProveedores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doProveedores();
				}
			});
		}
		return mntmProveedores;
	}

	private JMenuItem getMntmFormasDePago() {
		if (mntmFormasDePago == null) {
			mntmFormasDePago = new JMenuItem("Formas de pago");
			mntmFormasDePago.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
			mntmFormasDePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doFormasPagos();
				}
			});
		}
		return mntmFormasDePago;
	}

	private void doFormasPagos() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIFormaPago jalta = new JIFormaPago();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmSucursales() {
		if (mntmSucursales == null) {
			mntmSucursales = new JMenuItem("Sucursales");
			mntmSucursales.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
			mntmSucursales.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSucursales();
				}
			});
		}
		return mntmSucursales;
	}

	private void doSucursales() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JISucursal jalta = new JISucursal();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmBancos() {
		if (mntmBancos == null) {
			mntmBancos = new JMenuItem("Bancos");
			mntmBancos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBanco();
				}
			});
			mntmBancos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
		}
		return mntmBancos;
	}

	private void doBanco() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIBanco jalta = new JIBanco();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmPagoDeAguinaldos() {
		if (mntmPagoDeAguinaldos == null) {
			mntmPagoDeAguinaldos = new JMenuItem("Pago de aguinaldos");
			mntmPagoDeAguinaldos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagoAguinaldo();
				}
			});
			mntmPagoDeAguinaldos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmPagoDeAguinaldos;
	}

	private void doPagoAguinaldo() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPagoAguinaldo jalta = new JIPagoAguinaldo();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmPagoDeLiquidacion() {
		if (mntmPagoDeLiquidacion == null) {
			mntmPagoDeLiquidacion = new JMenuItem("Pago de liquidacion");
			mntmPagoDeLiquidacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagoLiquidacion();
				}
			});
			mntmPagoDeLiquidacion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmPagoDeLiquidacion;
	}

	private void doPagoLiquidacion() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPagoLiquidacion jalta = new JIPagoLiquidacion();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMenuPagoProveedores() {
		if (menuPagoProveedores == null) {
			menuPagoProveedores = new JMenuItem("Pago a Proveedores");
			menuPagoProveedores.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
			menuPagoProveedores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagoProveedores();
				}
			});
		}
		return menuPagoProveedores;
	}

	private void doPagoProveedores() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						JIPagoProveedores jipagoproveedores = new JIPagoProveedores();
						getDesktopPane().add(jipagoproveedores);
						jipagoproveedores.centerIt();
						jipagoproveedores.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnDevolucionDeProducto() {
		if (mnDevolucionDeProducto == null) {
			mnDevolucionDeProducto = new JMenu("Devoluciones");
			mnDevolucionDeProducto.setMnemonic('D');
			mnDevolucionDeProducto.add(getMntmRegistrarDevolucion());
			mnDevolucionDeProducto.add(getMntmConsultarDevoluciones());
			mnDevolucionDeProducto.add(getMntmConsultaranularDevolucion());
			mnDevolucionDeProducto.add(getMntmNotasDeCredito());
		}
		return mnDevolucionDeProducto;
	}

	private JMenuItem getMntmRegistrarDevolucion() {
		if (mntmRegistrarDevolucion == null) {
			mntmRegistrarDevolucion = new JMenuItem("Registrar devolucion");
			mntmRegistrarDevolucion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doRegistrarDevolucion();
				}
			});
			mntmRegistrarDevolucion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		}
		return mntmRegistrarDevolucion;
	}

	private void doRegistrarDevolucion() {
		try {
			PanambiUtils.setWaitCursor(this);

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JIDevolucionProducto jalta = new JIDevolucionProducto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});

		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultaranularDevolucion() {
		if (mntmConsultaranularDevolucion == null) {
			mntmConsultaranularDevolucion = new JMenuItem("Anular devolucion");
			mntmConsultaranularDevolucion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarAnularDevolucion();
				}
			});
			mntmConsultaranularDevolucion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularDevolucion;
	}

	private void doConsultarAnularDevolucion() {
		try {
			PanambiUtils.setWaitCursor(this);

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JIConsultarAnularDevolucionProducto jalta = new JIConsultarAnularDevolucionProducto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});

		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenu getMnCuotas() {
		if (mnCuotas == null) {
			mnCuotas = new JMenu("Cuotas");
			mnCuotas.setMnemonic('C');
			mnCuotas.add(getMntmRegistrarPagoDe());
			mnCuotas.add(getMntmConsultaranularPagoDe());
		}
		return mnCuotas;
	}

	private JMenuItem getMntmRegistrarPagoDe() {
		if (mntmRegistrarPagoDe == null) {
			mntmRegistrarPagoDe = new JMenuItem("Registrar pago de cuota");
			mntmRegistrarPagoDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
			mntmRegistrarPagoDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doPagoCuotas();
				}
			});
		}
		return mntmRegistrarPagoDe;
	}

	private JMenuItem getMntmConsultaranularPagoDe() {
		if (mntmConsultaranularPagoDe == null) {
			mntmConsultaranularPagoDe = new JMenuItem("Anular pago de cuota");
			mntmConsultaranularPagoDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarAnularPagoCuotas();
				}
			});
			mntmConsultaranularPagoDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularPagoDe;
	}

	private void doConsultarAnularPagoCuotas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularPagoCuota jalta = new JIConsultarAnularPagoCuota();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmAnularFactura() {
		if (mntmAnularFactura == null) {
			mntmAnularFactura = new JMenuItem("Anular factura");
			mntmAnularFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnularFactura();
				}
			});
			mntmAnularFactura.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		}
		return mntmAnularFactura;
	}

	private void doAnularFactura() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIAnularFactura jalta = new JIAnularFactura();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmAuditoria() {
		if (mntmAuditoria == null) {
			mntmAuditoria = new JMenuItem("Auditor\u00EDa");
			mntmAuditoria.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAuditoria();
				}
			});
			mntmAuditoria.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmAuditoria;
	}

	private void doAuditoria() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAuditoriaGeneral jalta = new JIConsultarAuditoriaGeneral();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmPermisos() {
		if (mntmPermisos == null) {
			mntmPermisos = new JMenuItem("Permisos");
			mntmPermisos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPermisos();
				}
			});
			mntmPermisos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmPermisos;
	}

	private void doPermisos() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIPermiso jalta = new JIPermiso();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmNotasDeCredito() {
		if (mntmNotasDeCredito == null) {
			mntmNotasDeCredito = new JMenuItem("Consultar notas de credito");
			mntmNotasDeCredito.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doNotasCredito();
				}
			});
			mntmNotasDeCredito.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmNotasDeCredito;
	}

	private void doNotasCredito() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarNotaCredito jalta = new JIConsultarNotaCredito();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultaranularGastos() {
		if (mntmConsultaranularGastos == null) {
			mntmConsultaranularGastos = new JMenuItem("Consultar/Anular gastos");
			mntmConsultaranularGastos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarAnularGasto();
				}
			});
			mntmConsultaranularGastos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularGastos;
	}

	private void doConsultarAnularGasto() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularGasto jalta = new JIConsultarAnularGasto();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmBonificaciones() {
		if (mntmBonificaciones == null) {
			mntmBonificaciones = new JMenuItem("Bonificaciones");
			mntmBonificaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBonificaciones();
				}
			});
			mntmBonificaciones.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmBonificaciones;
	}

	private void doBonificaciones() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIBonificacion jalta = new JIBonificacion();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultarVentas() {
		if (mntmConsultarVentas == null) {
			mntmConsultarVentas = new JMenuItem("Consultar ventas");
			mntmConsultarVentas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarVentas();
				}
			});
			mntmConsultarVentas.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultarVentas;
	}

	private void doConsultarVentas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarVenta jalta = new JIConsultarVenta();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultaranularCompras() {
		if (mntmConsultaranularCompras == null) {
			mntmConsultaranularCompras = new JMenuItem("Consultar/Anular compras");
			mntmConsultaranularCompras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarAnularCompra();
				}
			});
			mntmConsultaranularCompras.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularCompras;
	}

	private void doConsultarAnularCompra() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularCompra jalta = new JIConsultarAnularCompra();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JButtonPanambi getBtnAyuda() throws Exception {
		if (btnAyuda == null) {
			btnAyuda = new JButtonPanambi();
			btnAyuda.setSize(new Dimension(50, 50));
			btnAyuda.setPreferredSize(new Dimension(50, 50));

			btnAyuda.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doHelpIcon();
				}
			});

			btnAyuda.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doHelpIcon();
					}
				}
			});
			btnAyuda.setToolTipText("Ayuda");
			BufferedImage buffimg = ImageIO.read(new File(JFramePanambiMain.class.getResource("/py/com/panambi/images/ayuda.png").toURI()));
			ImageIcon icon = new ImageIcon(buffimg.getScaledInstance(40, 40, Image.SCALE_SMOOTH));
			btnAyuda.setIcon(icon);
		}
		return btnAyuda;
	}

	private void doHelpIcon() {
		try {
			PanambiUtils.setWaitCursor(this);
			mntmAyuda.doClick();
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JButtonPanambi getBtnSalir() throws Exception {
		if (btnSalir == null) {
			btnSalir = new JButtonPanambi();
			btnSalir.setSize(new Dimension(50, 50));
			btnSalir.setPreferredSize(new Dimension(50, 50));
			btnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});

			btnSalir.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						btnSalir.doClick();
					}
				}
			});

			btnSalir.setToolTipText("Salir del sistema");
			BufferedImage buffimg = ImageIO.read(new File(JFramePanambiMain.class.getResource("/py/com/panambi/images/salir.png").toURI()));
			ImageIcon icon = new ImageIcon(buffimg.getScaledInstance(40, 40, Image.SCALE_SMOOTH));
			btnSalir.setIcon(icon);
		}
		return btnSalir;
	}

	private JMenuItem getMntmReporteDeCompras() {
		if (mntmReporteDeCompras == null) {
			mntmReporteDeCompras = new JMenuItem("Reporte de compras");
			mntmReporteDeCompras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReporteCompras();
				}
			});
			mntmReporteDeCompras.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteDeCompras;
	}

	private void doReporteCompras() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReporteCompras jalta = new JIReporteCompras();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmReporteDeVentas() {
		if (mntmReporteDeVentas == null) {
			mntmReporteDeVentas = new JMenuItem("Reporte de ventas");
			mntmReporteDeVentas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReporteVentas();
				}
			});
			mntmReporteDeVentas.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteDeVentas;
	}

	private void doReporteVentas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReporteVentas jalta = new JIReporteVentas();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmImpresionPagares() {
		if (mntmImpresionPagares == null) {
			mntmImpresionPagares = new JMenuItem("Impresi\u00F3n de pagar\u00E9s");
			mntmImpresionPagares.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
			mntmImpresionPagares.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doPruebas();
				}
			});
		}
		return mntmImpresionPagares;
	}

	private void doPruebas() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIGenerarPagare jalta = new JIGenerarPagare();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmReporteIngresosEgresos() {
		if (mntmReporteIngresosEgresos == null) {
			mntmReporteIngresosEgresos = new JMenuItem("Reporte de ingresos y egresos");
			mntmReporteIngresosEgresos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReporteGanancias();
				}
			});
			mntmReporteIngresosEgresos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteIngresosEgresos;
	}

	private void doReporteGanancias() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReporteIngresosEgresos jalta = new JIReporteIngresosEgresos();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultarStock() {
		if (mntmConsultarStock == null) {
			mntmConsultarStock = new JMenuItem("Consultar stock");
			mntmConsultarStock.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarStock();
				}
			});
			mntmConsultarStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultarStock;
	}

	private void doConsultarStock() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarStock jalta = new JIConsultarStock();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmReporteDeFlujo() {
		if (mntmReporteDeFlujo == null) {
			mntmReporteDeFlujo = new JMenuItem("Reporte de flujo de caja");
			mntmReporteDeFlujo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReporteFlujoCaja();
				}
			});
			mntmReporteDeFlujo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteDeFlujo;
	}

	private void doReporteFlujoCaja() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReporteFlujoCaja jalta = new JIReporteFlujoCaja();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmConsultaranularPagosA() {
		if (mntmConsultaranularPagosA == null) {
			mntmConsultaranularPagosA = new JMenuItem("Consultar/Anular pagos a empleados");
			mntmConsultaranularPagosA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarAnularPagoEmpleado();
				}
			});
			mntmConsultaranularPagosA.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularPagosA;
	}

	private void doConsultarAnularPagoEmpleado() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularPagoSalario jalta = new JIConsultarAnularPagoSalario();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private JMenuItem getMntmACercaDe() {
		if (mntmACercaDe == null) {
			mntmACercaDe = new JMenuItem("Acerca de ");
			mntmACercaDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAcercaDe();
				}
			});
			mntmACercaDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		}
		return mntmACercaDe;
	}

	private void doAcercaDe() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						DlgAcercade jalta = new DlgAcercade();
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}

	private void poblarSucursales() throws Exception {
		ControladorSucursal controladorSucursal = new ControladorSucursal();
		sucursales = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
	}

	private JMenu getMnSucursalOperativa() throws Exception {
		if (mnSucursalOperativa == null) {
			mnSucursalOperativa = new JMenu("Sucursal Operativa");
			mnSucursalOperativa.setMnemonic('S');
			poblarSucursales();
			ButtonGroup bg = new ButtonGroup();
			final ControladorSucursal controladorSucursal = new ControladorSucursal();
			for (Sucursal sucursal : sucursales) {
				JRadioButtonMenuItem rbutton = new JRadioButtonMenuItem(sucursal.getNombre());
				rbutton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							Sucursal sucucheck = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), ((JRadioButtonMenuItem)e.getSource()).getText());
							if (JFramePanambiMain.session.getSucursalOperativa().getCodSucursal() != sucucheck.getCodSucursal()) {
								boolean validado = validarPorAdministrador();
								if (validado) {
									JFramePanambiMain.session.setSucursalOperativa(sucucheck);
									setMainTitle();
									disposeAllWindows();
								}else{
									Component[] components = getMnSucursalOperativa().getPopupMenu().getComponents();
									for (Component component : components) {
										if(((JRadioButtonMenuItem)component).getText().equals(JFramePanambiMain.session.getSucursalOperativa().getNombre())){
											((JRadioButtonMenuItem)component).setSelected(true);
										}
									}
								}
							}
						} catch (Exception e1) {
							logger.error(e1.getMessage(), e1);
						}
					}
				});
				bg.add(rbutton);
				if(JFramePanambiMain.session.getSucursalOperativa() == null && sucursal.getPrincipal()){
					rbutton.setSelected(true);
					JFramePanambiMain.session.setSucursalOperativa(sucursal);
					setMainTitle();
				}else if(JFramePanambiMain.session.getSucursalOperativa() != null &&  JFramePanambiMain.session.getSucursalOperativa().getNombre().equals(sucursal.getNombre())){
					rbutton.setSelected(true);
				}
				mnSucursalOperativa.add(rbutton);
			}
		}
		return mnSucursalOperativa;
	}

	private void disposeAllWindows() {
		Component[] components = getDesktopPane().getComponents();
		for (int i = 0; i < components.length; i++) {
			Component component = components[i];
			if (component instanceof JInternalFramePanambi) {
				((JInternalFramePanambi)component).dispose();
			}
		}
	}

	public void setMainTitle() {
		JFramePanambiMain.this.setTitle("Panambi Soft \t\t\t\t - Usuario: " + JFramePanambiMain.session.getUsuario().getUsuario() + " Sucursal: " + JFramePanambiMain.session.getSucursalOperativa().getNombre());		
	}
	
	public void markSucursal() throws Exception{
		Component[] components = getMnSucursalOperativa().getPopupMenu().getComponents();
		for (Component component : components) {
			if(((JRadioButtonMenuItem)component).getText().equals(JFramePanambiMain.session.getSucursalOperativa().getNombre())){
				((JRadioButtonMenuItem)component).setSelected(true);
			}
		}
	}

	private boolean validarPorAdministrador() {
		DlgLoginValidaAdministrador dlgLoginAdministrador = new DlgLoginValidaAdministrador(this);
		dlgLoginAdministrador.setVisible(true);
		return dlgLoginAdministrador.isValidada();
	}
	
	private JMenuItem getMntmCambiarContrasea() {
		if (mntmCambiarContrasea == null) {
			mntmCambiarContrasea = new JMenuItem("Cambiar contrase\u00F1a");
			mntmCambiarContrasea.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK));
			mntmCambiarContrasea.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doChangePass();
				}
			});
		}
		return mntmCambiarContrasea;
	}
	
	private void doChangePass(){
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					
					try {
						DlgChangePassword jalta = new DlgChangePassword();
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}
					
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}
	private JMenuItem getMntmConsultaranularDeudasDe() {
		if (mntmConsultaranularDeudasDe == null) {
			mntmConsultaranularDeudasDe = new JMenuItem("Consultar/Anular deudas de empleados");
			mntmConsultaranularDeudasDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarDeudasEmpleado();
				}
			});
			mntmConsultaranularDeudasDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		}
		return mntmConsultaranularDeudasDe;
	}
	
	private void doConsultarDeudasEmpleado() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarAnularDeudaEmpleado jalta = new JIConsultarAnularDeudaEmpleado();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}
	private JMenuItem getMntmConsultarDevoluciones() {
		if (mntmConsultarDevoluciones == null) {
			mntmConsultarDevoluciones = new JMenuItem("Consultar devoluciones");
			mntmConsultarDevoluciones.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
			mntmConsultarDevoluciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarDevoluciones();
				}
			});
		}
		return mntmConsultarDevoluciones;
	}
	
	private void doConsultarDevoluciones() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIConsultarDevolucion jalta = new JIConsultarDevolucion();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}
	private JMenuItem getMntmReporteDeGastos() {
		if (mntmReporteDeGastos == null) {
			mntmReporteDeGastos = new JMenuItem("Reporte de gastos");
			mntmReporteDeGastos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReporteGastos();
				}
			});
			mntmReporteDeGastos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteDeGastos;
	}
	
	private void doReporteGastos() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReporteGastos jalta = new JIReporteGastos();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	}
	private JMenuItem getMntmReporteDePagos() {
		if (mntmReporteDePagos == null) {
			mntmReporteDePagos = new JMenuItem("Reporte de pagos a empleados");
			mntmReporteDePagos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doReportePagoEmpleado();
				}
			});
			mntmReporteDePagos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		}
		return mntmReporteDePagos;
	}
	
	private void doReportePagoEmpleado() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIReportePagosEmpleados jalta = new JIReportePagosEmpleados();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	
	}
	private JMenuItem getMntmParametrosDelSistema() {
		if (mntmParametrosDelSistema == null) {
			mntmParametrosDelSistema = new JMenuItem("Parametros del sistema");
			mntmParametrosDelSistema.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doParametrosSistema();
				}
			});
			mntmParametrosDelSistema.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
		}
		return mntmParametrosDelSistema;
	}
	
	private void doParametrosSistema() {
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {

					try {
						JIParametros jalta = new JIParametros();
						getDesktopPane().add(jalta);
						jalta.centerIt();
						jalta.setVisible(true);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(JFramePanambiMain.this, e, DlgMessage.ERROR_MESSAGE);
					}

				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
	
	}
}
