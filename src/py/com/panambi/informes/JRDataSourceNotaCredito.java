package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceNotaCredito implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceNotaCredito(Connection conn, Integer coddevolucion ) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT nroitem,"
				+" ( SELECT descripcion FROM productos p WHERE p.codproducto = dd.codproducto ) AS descripcion, "
				+ "montounitario, cantidad, totalitem "
				+ "FROM detalledevoluciones dd "
				+ "WHERE coddevolucion = "+coddevolucion;
	
		sql += "order by nroitem ";
		//ps.setInt(1, coddevolucion);
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("nroitem")) {
				obj = rs.getInt("nroitem");
			}
			if (arg0.getName().equals("descripcion")) {
				obj = rs.getString("descripcion");
			}
			if (arg0.getName().equals("montounitario")) {
				obj = rs.getDouble("montounitario");
			}
			if (arg0.getName().equals("cantidad")) {
				obj = rs.getInt("cantidad");
			}
			if (arg0.getName().equals("totalitem")) {
				obj = rs.getDouble("totalitem");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
