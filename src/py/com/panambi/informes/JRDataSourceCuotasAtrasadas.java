package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceCuotasAtrasadas implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceCuotasAtrasadas(Connection conn, Integer cantidad, Integer codsucursal) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT v.nrocomprobante, c.nombres||' '||c.apellidos AS cliente, nrocuota, vencimiento, monto, current_date - vencimiento AS retraso "+
		"FROM planespagos pp ,ventas v , clientes c, sucursales s "+
		"WHERE pp.codventa = v.codventa "+ 
		"AND s.codsucursal = v.codsucursal "+
		"AND v.codcliente = c.codcliente "+
		"AND pp.estado = 'D' ";
		
		
		if (cantidad!=-1) {
			sql += "AND (current_date - vencimiento <= "+cantidad+") ";
		}
		
		if(codsucursal !=null){
			sql+="AND v.codsucursal = "+codsucursal;
		}
		
		sql += "AND (current_date - vencimiento > 0 ) ";
		sql += "order by cliente, fecha ;";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
	}
	
	public JRDataSourceCuotasAtrasadas(Connection conn, Integer codsucursal) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT v.nrocomprobante, c.nombres||' '||c.apellidos AS cliente, nrocuota, vencimiento, monto, current_date - vencimiento AS retraso "+
		"FROM planespagos pp ,ventas v , clientes c, sucursales s "+
		"WHERE pp.codventa = v.codventa "+ 
		"AND v.codcliente = c.codcliente "+
		"AND v.codsucursal = s.codsucursal "+
		"AND pp.estado = 'D' ";
		
		if(codsucursal !=null){
			sql+="AND v.codsucursal = "+codsucursal;
		}
		
		sql += "AND (current_date - vencimiento > 90) ";
		
		sql += "order by cliente, fecha ;";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codVenta")) {
				obj = rs.getInt("nrocomprobante");
			}
			if (arg0.getName().equals("cliente")) {
				obj = rs.getString("cliente");
			}
			if (arg0.getName().equals("nrocuota")) {
				obj = rs.getInt("nrocuota");
			}
			if (arg0.getName().equals("vencimiento")) {
				obj = rs.getDate("vencimiento");
			}
			
			if (arg0.getName().equals("monto")) {
				obj = rs.getBigDecimal("monto");
			}
			if (arg0.getName().equals("diasRetraso")) {
				obj = rs.getInt("retraso");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
