package py.com.panambi.informes;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIConsultaProveedores extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8443394787346483932L;
	private JPanel panel;
	private JRadioButton rdbtnTodos;
	private JRadioButton rdbtnSoloActivos;
	private JRadioButton rdbtnInactivos;
	private JButtonPanambi btnpnmbConsultar;
	
	
	public JIConsultaProveedores() throws Exception{
		initialize();
	}
	
	
	
	private void initialize() {
		setMaximizable(false);
		setTitle("Reporte de proveedores");
		setBounds(100,100,360,289);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(78, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbConsultar());
		setShortcuts(this);
		setHelp("listarproveedores");
		doLimpiar();
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Proveedores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(19)
						.addComponent(getRdbtnInactivos(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(getRdbtnSoloActivos())
						.addGap(15)
						.addComponent(getRdbtnTodos())
						.addContainerGap(24, Short.MAX_VALUE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getRdbtnInactivos())
							.addComponent(getRdbtnTodos())
							.addComponent(getRdbtnSoloActivos()))
						.addContainerGap(29, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectTodos();
				}
			});
		}
		return rdbtnTodos;
	}
	private JRadioButton getRdbtnSoloActivos() {
		if (rdbtnSoloActivos == null) {
			rdbtnSoloActivos = new JRadioButton("Solo Activos");
			rdbtnSoloActivos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectSoloActivos();
				}
			});
		}
		return rdbtnSoloActivos;
	}
	private JRadioButton getRdbtnInactivos() {
		if (rdbtnInactivos == null) {
			rdbtnInactivos = new JRadioButton("Inactivos");
			rdbtnInactivos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectInactivos();
					}
			});
		}
		return rdbtnInactivos;
	}
	
	private void doSelectInactivos(){
		getRdbtnInactivos().setSelected(true);
		getRdbtnSoloActivos().setSelected(false);
		getRdbtnTodos().setSelected(false);
	}
	
	private void doSelectSoloActivos(){
		getRdbtnInactivos().setSelected(false);
		getRdbtnSoloActivos().setSelected(true);
		getRdbtnTodos().setSelected(false);
	}
	
	private void doSelectTodos(){
		doLimpiar();
	}
	
	private JButtonPanambi getBtnpnmbConsultar() {
		if (btnpnmbConsultar == null) {
			btnpnmbConsultar = new JButtonPanambi();
			btnpnmbConsultar.setMnemonic('C');
			btnpnmbConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultar();
				}
			});
			btnpnmbConsultar.setText("Consultar");
		}
		return btnpnmbConsultar;
	}
	
	private void doLimpiar(){
		getRdbtnTodos().setSelected(true);
		getRdbtnInactivos().setSelected(false);
		getRdbtnSoloActivos().setSelected(false);
	}
	private void doConsultar() {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultaProveedores.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			String tipoReporte = "";
			if(getRdbtnInactivos().isSelected()){
				parameters.put("TIPOREPORTE", "PROVEEDORES INACTIVOS");
				tipoReporte = "I";
			}
			
			if(getRdbtnSoloActivos().isSelected()){
				parameters.put("TIPOREPORTE", "PROVEEDORES ACTIVOS");
				tipoReporte = "A";
			}
			if(getRdbtnTodos().isSelected()){
				parameters.put("TIPOREPORTE", "TODOS LOS PROVEEDORES");
				tipoReporte = "T";
			}
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			URL url = JIConsultaProveedores.class.getResource("/py/com/panambi/informes/reports/JRProveedores.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JFramePanambiMain.session.getConn());
			
			JasperPrint jasperPrint;
			
			if(getRdbtnTodos().isSelected()){
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceProveedores(JFramePanambiMain.session.getConn(), tipoReporte ));
			}else {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceProveedores(JFramePanambiMain.session.getConn(), tipoReporte));
			}
			
			List<?> pages = jasperPrint.getPages();
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}
}
