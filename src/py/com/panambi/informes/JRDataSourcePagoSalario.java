package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourcePagoSalario implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;
	
	public JRDataSourcePagoSalario(Connection conn , Integer nrorecibo) throws Exception {
		String sql = "SELECT "+nrorecibo+" as nrorecibo" ;
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("nrorecibo")) {
				obj = rs.getInt("nrorecibo");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
