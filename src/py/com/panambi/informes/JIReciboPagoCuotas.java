package py.com.panambi.informes;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import py.com.panambi.controller.ControladorSecuencias;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.NumberToLetterConverter;
import py.com.panambi.utils.PanambiUtils;

public class JIReciboPagoCuotas extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3116528323291060770L;
	private JLabelPanambi lblpnmbANombreDe;
	private JTextFieldUpper jTxtNombre;
	private JLabelPanambi lblpnmbMonto;
	private JTextFieldDouble jTxtMonto;
	private JLabelPanambi lblpnmbDescripcion;
	private JTextAreaUpper jTxtDescripcion;
	private JButtonPanambi btnpnmbGenerarRecibo;
	private JScrollPane scrollPane;
	public JIReciboPagoCuotas() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Generar Recibo de Pago de Cuotas");
		getJPanelSouth().add(getBtnpnmbGenerarRecibo());
		setBounds(100, 100, 419, 312);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbANombreDe(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtNombre(), GroupLayout.PREFERRED_SIZE, 253, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)))
					.addGap(112))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbANombreDe(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
	}
	private JLabelPanambi getLblpnmbANombreDe() {
		if (lblpnmbANombreDe == null) {
			lblpnmbANombreDe = new JLabelPanambi();
			lblpnmbANombreDe.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbANombreDe.setText("A nombre de : ");
		}
		return lblpnmbANombreDe;
	}
	private JTextFieldUpper getJTxtNombre() {
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
		}
		return jTxtNombre;
	}
	private JLabelPanambi getLblpnmbMonto() {
		if (lblpnmbMonto == null) {
			lblpnmbMonto = new JLabelPanambi();
			lblpnmbMonto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbMonto.setText("Monto : ");
		}
		return lblpnmbMonto;
	}
	private JTextFieldDouble getJTxtMonto() {
		if (jTxtMonto == null) {
			jTxtMonto = new JTextFieldDouble();
		}
		return jTxtMonto;
	}
	private JLabelPanambi getLblpnmbDescripcion() {
		if (lblpnmbDescripcion == null) {
			lblpnmbDescripcion = new JLabelPanambi();
			lblpnmbDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDescripcion.setText("Descripcion : ");
		}
		return lblpnmbDescripcion;
	}
	private JTextAreaUpper getJTxtDescripcion() {
		if (jTxtDescripcion == null) {
			jTxtDescripcion = new JTextAreaUpper();
		}
		return jTxtDescripcion;
	}
	private JButtonPanambi getBtnpnmbGenerarRecibo() {
		if (btnpnmbGenerarRecibo == null) {
			btnpnmbGenerarRecibo = new JButtonPanambi();
			btnpnmbGenerarRecibo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doGenerarReciboPagoCuotas();
				}
			});
			btnpnmbGenerarRecibo.setText("Generar recibo");
		}
		return btnpnmbGenerarRecibo;
	}
	

	private void doGenerarReciboPagoCuotas(){
	try {
			Integer ret = null;
			if(getJTxtNombre().getText().length()== 0 ||
				getJTxtMonto().getText().length()== 0 ||
				getJTxtDescripcion().getText().length()==0){
				ret = JOptionPane.showInternalConfirmDialog(this,"Exiten campos en blanco \nDesea generar de todas formas el recibo?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); 
			}else{
				ret= JOptionPane.YES_OPTION;
			}
			if(ret == JOptionPane.YES_OPTION){
				PanambiUtils.setWaitCursor(getOwner());
				Map<String, Object> parameters = new HashMap<String, Object>();
				javax.swing.ImageIcon icon = new ImageIcon(JIReciboPagoCuotas.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
				Image image = icon.getImage();
				
				parameters.put("LOGO", image);
				parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
				if(getJTxtMonto().getText().length()==0){
					parameters.put("MONTOTOTAL", null);
					parameters.put("MONTOLETRAS", "............................................................................................................");
					
				}else if ((Double)getJTxtMonto().getValue()==0){
					parameters.put("MONTOTOTAL", null);
					parameters.put("MONTOLETRAS", "............................................................................................................");
				}else{
					parameters.put("MONTOTOTAL", (Double)getJTxtMonto().getValue());
					parameters.put("MONTOLETRAS", NumberToLetterConverter.convertNumberToLetter((Double)getJTxtMonto().getValue()));
				}
				
				if(getJTxtNombre().getText().length()==0){
					parameters.put("CLIENTE", "............................................................................................................");
				}else{
					parameters.put("CLIENTE",getJTxtNombre().getText().toUpperCase());
				}
				
				if(getJTxtDescripcion().getText().length()==0){
					parameters.put("DESCRIPCION", "...................................................................................................");
				}else{
					parameters.put("DESCRIPCION",getJTxtDescripcion().getText().toUpperCase());
				}
				
		
				URL url = JIReciboPagoCuotas.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
				
				JasperPrint jasperPrint;
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceReciboPagoCuota(JFramePanambiMain.session.getConn()));
				
				
				List<?> pages = jasperPrint.getPages();
				if (!pages.isEmpty()) {
					JasperViewer.viewReport(jasperPrint, false);

					new ControladorSecuencias().aumentarNroReciboPagoCuotas(JFramePanambiMain.session.getConn());
				} else {
					DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
				}
			}
			
	} catch (Exception e) {
		DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
	} finally {
		PanambiUtils.setDefaultCursor(getOwner());
	}
	
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTxtDescripcion());
		}
		return scrollPane;
	}
}
