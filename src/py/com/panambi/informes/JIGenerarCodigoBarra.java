 package py.com.panambi.informes;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Producto;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.prompts.PromptProductos;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIGenerarCodigoBarra extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8705548234046710393L;
	private JLabelPanambi lblpnmbProducto;
	private JComboBoxPanambi jCmbProducto;
	private JButtonPanambi btnpnmbGenerar;
	private JButtonPanambi JbtnExaminar;
	private Producto producto;
	private List<String> productos = new ArrayList<String>();
	private ControladorProducto controladorProducto = new ControladorProducto();
	private JLabelPanambi lblpnmbPresioneFPara;
	
	public JIGenerarCodigoBarra() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Generar Codigo de Barras");
		setMaximizable(false);
		setBounds(100, 100, 561, 250);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbProducto(), GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getJCmbProducto(), GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(getJbtnExaminar(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
							.addGap(18))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJbtnExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(28))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbGenerar());
		setShortcuts(this);
		setHelp("codigosbarras");
		doLimpiar();
	}
	private JLabelPanambi getLblpnmbProducto() {
		if (lblpnmbProducto == null) {
			lblpnmbProducto = new JLabelPanambi();
			lblpnmbProducto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProducto.setText("Producto : ");
		}
		return lblpnmbProducto;
	}
	private JComboBoxPanambi getJCmbProducto() {
		if (jCmbProducto == null) {
			jCmbProducto = new JComboBoxPanambi();
			jCmbProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SelectProducto();
				}
			});
		}
		return jCmbProducto;
	}
	
	
	private void doPrompProducto() {
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}
	
//	private void doBuscar() {
//		try {
//			doLimpiar();
//			String[] columnNames = { "C�digo", "Descripcion", "Precio"/*
//																	 * ,
//																	 * "Maximo Descuento"
//																	 * ,
//																	 * "Stock Minimo"
//																	 * ,
//																	 * "Temporada"
//																	 */};
//			String[] pks = { "codProducto" };
//			String sSelect = "SELECT codproducto, descripcion, precio ";
//			sSelect += "FROM productos WHERE estado = 'A' ";
//			sSelect += "ORDER BY descripcion";
//			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Producto", pks);
//			jb.setVisible(true);
//		} catch (Exception e) {
//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//		}
//	}
	
	private void SelectProducto() {
		try {
			Producto prod = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), (String) jCmbProducto.getSelectedItem());
			if (prod != null) {
				setValues(prod, null);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar() {
		try {
			poblarLista();
			getJCmbProducto().setModel(new ListComboBoxModel<String>(productos));
			getJCmbProducto().setSelectedItem(null);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void poblarLista() throws Exception {
		productos = new ArrayList<String>();
		List<Producto> listaprod = controladorProducto.getProductos(JFramePanambiMain.session.getConn());

		for (Producto prod : listaprod) {
			productos.add(prod.getDescripcion());
		}
	}

	
	private JButtonPanambi getBtnpnmbGenerar() {
		if (btnpnmbGenerar == null) {
			btnpnmbGenerar = new JButtonPanambi();
			btnpnmbGenerar.setMnemonic('G');
			btnpnmbGenerar.setToolTipText("Generar codigo de barra");
			btnpnmbGenerar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarCodigoBarra();
				}
			});
			btnpnmbGenerar.setText("Generar");
		}
		return btnpnmbGenerar;
	}
	
	private void doGenerarCodigoBarra() {
		try {
			if(producto != null){
				
				
				PanambiUtils.setWaitCursor(getOwner());
				Map<String, Object> parameters = new HashMap<String, Object>();
				javax.swing.ImageIcon icon = new ImageIcon(JIGenerarCodigoBarra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
				Image image = icon.getImage();
				
				parameters.put("LOGO", image);
				parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
				parameters.put("CODIGO", producto.getCodProducto());
				URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRGenerarCodigoBarra.jasper");
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
	//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JFramePanambiMain.session.getConn());
				
				JasperPrint jasperPrint;
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceGenerarCodigoBarra(JFramePanambiMain.session.getConn(), producto ));
				
				
				List<?> pages = jasperPrint.getPages();
				if (!pages.isEmpty()) {
					JRPdfExporter exporter = new JRPdfExporter();
					File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.exportReport();
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(filetmp);
					}
				} else {
					DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
				}
			}else{
				DlgMessage.showMessage(getOwner(), "No se selecciono ning�n producto.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}


	}
	
	
	private JButtonPanambi getJbtnExaminar() {
		if (JbtnExaminar == null) {
			JbtnExaminar = new JButtonPanambi();
			JbtnExaminar.setMnemonic('E');
			JbtnExaminar.setToolTipText("Buscar Producto");
			JbtnExaminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//doBuscar();
					doPrompProducto();
				}
			});
			JbtnExaminar.setText("Examinar ...");
		}
		return JbtnExaminar;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		this.producto = (Producto) obj;
		getJCmbProducto().setSelectedPanambiObject(((Producto) (obj)));
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
