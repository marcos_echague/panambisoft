package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceProductos implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceProductos(Connection conn, Integer codsucursal, boolean stockMenor) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT sucu.nombre Sucursal, prod.codproducto, prod.descripcion, prod.precio, prod.stockminimo, st.cantidad ";
		sql += "FROM productos prod ";
		sql += "JOIN stockproductos st on prod.codproducto = st.codproducto_productos ";
		sql += "JOIN sucursales sucu on sucu.codsucursal = st.codsucursal_sucursales ";
		if (codsucursal != null) {
			sql += "WHERE codsucursal = ? ";
			if(stockMenor){
				sql += "AND st.cantidad < prod.stockminimo ";
			}
		}else if (stockMenor){
			sql += "WHERE st.cantidad < prod.stockminimo ";
		}
		sql += "order by sucursal, codproducto ";
		ps = conn.prepareStatement(sql);
		if (codsucursal != null) {
			ps.setInt(1, codsucursal);
		}
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("sucursal")) {
				obj = rs.getString("sucursal");
			}
			if (arg0.getName().equals("codproducto")) {
				obj = rs.getInt("codproducto");
			}
			if (arg0.getName().equals("cantidad")) {
				obj = rs.getInt("cantidad");
			}
			if (arg0.getName().equals("descripcion")) {
				obj = rs.getString("descripcion");
			}
			if (arg0.getName().equals("precio")) {
				obj = rs.getBigDecimal("precio");
			}
			if (arg0.getName().equals("stockminimo")) {
				obj = rs.getInt("stockminimo");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
