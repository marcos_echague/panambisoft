package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.main.JFramePanambiMain;

public class JRDataSourceIngresosEgresos implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceIngresosEgresos(Connection conn, Date fechaDesde , Date fechaHasta , Sucursal sucursal ,List<String> descripciones, List<Double> ingresos ,List<Double> egresos ) throws Exception {
		// aqui se prepara y ejecuta el sql
						
//		String sql  = "SELECT codpago AS codpago , 0 AS codpagoempleado , fecha AS fecha FROM pagos p WHERE p.estado = 'A' ";
//				if(fechaDesde!=null && fechaHasta!=null){
//					sql+=" AND p.fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
//				}else if (fechaDesde ==null && fechaHasta !=null){
//					sql+=" AND p.fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
//				}else if (fechaDesde !=null && fechaHasta ==null){
//					sql+=" AND p.fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
//				}
//				sql+= "UNION "
//				+ "SELECT 0 AS codpado , codpagoempleado, fechapago AS fecha FROM pagosempleados pe WHERE pe.estado = 'A' ";
//				if(fechaDesde!=null && fechaHasta!=null){
//					sql+=" AND pe.fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
//				}else if (fechaDesde ==null && fechaHasta !=null){
//					sql+=" AND pe.fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
//				}else if (fechaDesde !=null && fechaHasta ==null){
//					sql+=" AND pe.fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
//				}
//				sql+="ORDER BY fecha ";
		String sql  = "SELECT * FROM pagos p WHERE p.estado = 'A' ";
		
		if(sucursal!=null){
			sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
		}
		
		if(fechaDesde!=null && fechaHasta!=null){
			sql+=" AND p.fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
		}else if (fechaDesde ==null && fechaHasta !=null){
			sql+=" AND p.fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
		}else if (fechaDesde !=null && fechaHasta ==null){
			sql+=" AND p.fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
		}
		
		sql+="ORDER BY fecha ";

		ps = conn.prepareStatement(sql);
		
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		
		Object obj = null;
		try {
			Pago pago = null;
			String descripcion = "";
			Double debe = 0.0;
			Double haber = 0.0;
			Object fecha = null;
			if(rs.getInt("codpago")!=0){
				pago = new ControladorPago().getPago(JFramePanambiMain.session.getConn(),rs.getInt("codpago"));
			}
			
			                  
			if(pago!=null){
				if(pago.getCompra()!=null){
		 			if(pago.getCompra().getFacturanro().length()!=0){
						descripcion = "Pago por compra con factura nro "+pago.getCompra().getFacturanro()+".";
					}else{
							descripcion = "Pago por compra sin numero de factura.";
					}
					debe = pago.getMonto();
					haber = 0.0;
				}
				if(pago.getGasto()!=null){
					if(pago.getGasto().getNroComprobante().length()!=0){
						descripcion = "Pago por gasto en concepto de "+pago.getGasto().getTipoGasto().getConcepto()+" con comprobante nro "+pago.getGasto().getNroComprobante()+".";
						}else{
						descripcion = "Pago por gasto en concepto de "+pago.getGasto().getTipoGasto().getConcepto()+"sin numero de comprbante.";
					}
					debe  = pago.getMonto();
					haber = 0.0;
				}
				if(pago.getVenta()!=null){
					if(pago.getNroRecibo()!=0){
						List <PlanPago> cuotasPagadas = new ControladorPlanPago().getCuotasPagadas(JFramePanambiMain.session.getConn(), pago);
						if(cuotasPagadas.size()>1){
							descripcion = "Cobro de cuotas nro";
							
						}else{
							descripcion = "Cobro de cuota nro";
						}
						
						for(int i = 0; i<cuotasPagadas.size();i++){
							descripcion += " "+cuotasPagadas.get(i).getNroCuota();
							if(i!=cuotasPagadas.size()-1){
								descripcion+=" y";
							}
						}
						descripcion += " de factura nro "+pago.getVenta().getNroComprobante()+". Recibo nro "+ pago.getNroRecibo();
					}else if(pago.getVenta().getNroComprobante()!=null){
						descripcion = "Cobro por venta con factura nro "+pago.getVenta().getNroComprobante();
					}else{
						descripcion = "Cobro por venta sin n�mero de factura.";
					}
					haber = pago.getMonto();
					debe =  0.0;
					
				}
				fecha = pago.getFecha();
				
			}
			

			if(pago.getPagoSalario()!=null){
				
//				String patron = "dd/MM/yyyy";
//				SimpleDateFormat formato = new SimpleDateFormat(patron);
//				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getPagoSalario().getCodempleado());
//				descripcion = "Pago de salario al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
//				+formato.format(pago.getPagoSalario().getFechaDesde())+" a "+formato.format(pago.getPagoSalario().getFechaHasta());
//				if(pago.getNroRecibo()!=null){
//					descripcion+=" con recibo nro "+pago.getNroRecibo();
//				}
//				
//				
//				debe = pago.getMonto();
//				haber =  0.0;
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getPagoSalario().getCodempleado());
				
				if(pago.getPagoSalario().getTipo().equals("S")){
					
					descripcion = "Pago de salario al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
							+formato.format(pago.getPagoSalario().getFechaDesde())+" a "+formato.format(pago.getPagoSalario().getFechaHasta());
				}else if (pago.getPagoSalario().getTipo().equals("A")){
					
					descripcion = "Pago de aguinaldo al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
							+formato.format(pago.getPagoSalario().getFechaDesde())+" a "+formato.format(pago.getPagoSalario().getFechaHasta());
				}else{
					
					descripcion = "Pago de liquidacion al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" en fecha "+formato.format(pago.getPagoSalario().getFechapago());
				}
				
				if(pago.getNroRecibo()!=null){
					descripcion+=" con recibo nro "+pago.getNroRecibo();
				}
				
				
				debe = pago.getMonto();
				haber =  0.0;
				
			}
			
			if(pago.getDeudaEmpleado()!=null){
				
				Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getDeudaEmpleado().getPagoEmpleado().getCodempleado());
				descripcion = "Cobro al empleado "+emp.getNombre()+""+emp.getApellido()+"por deuda de "+ pago.getDeudaEmpleado().getConcepto();
				haber = pago.getMonto();
				debe =  0.0;
				
			}
			
//			if(pagoEmpleado!=null){
//				
//				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodempleado());
//				descripcion = "Pago de salario a empleado "+empleado.getNombre()+" "+empleado.getApellido();
//
//				String patron = "dd/MM/yyyy";
//				SimpleDateFormat formato = new SimpleDateFormat(patron);
//				if(pagoEmpleado.getFechaDesde()!=null && pagoEmpleado.getFechaHasta()!=null){
//					descripcion+=" periodo "+formato.format(pagoEmpleado.getFechaDesde())+" a "+formato.format(pagoEmpleado.getFechaHasta());
//				}else{
//					descripcion = " en fecha"+formato.format(pagoEmpleado.getFechapago());
//				}
//				debe = pagoEmpleado.getTotalpagado();
//				haber = 0.0;
//				fecha = pagoEmpleado.getFechapago();
//			}
			
			if (arg0.getName().equals("descripcion")) {
				obj = descripcion;
			}
			
			if (arg0.getName().equals("fecha")) {
				obj = fecha;
			}
			
			if (arg0.getName().equals("montoingreso")) {
				obj = haber;
			}
			if (arg0.getName().equals("montoegreso")) {
				obj = debe;
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
