package py.com.panambi.informes;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIReporteIngresosEgresos extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private JButtonPanambi jBtnGenerarReporte;
	private ControladorPago controladorPago= new ControladorPago();
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbSucursal;
	private List<String> sucursales = new ArrayList<String>();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	
	
	public JIReporteIngresosEgresos() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Reporte de ingresos y egresos");
		setBounds(100,100, 551, 298);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnGenerarReporte());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addGap(6))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(getJDPFechaDesde(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(getJCmbSucursal(), GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(123)
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(17, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 45, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(25)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(31))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setShortcuts(this);
		setHelp("reporteingresosegresos");
	}
	

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			
			poblarListaSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSucursal= controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					//getJCmbConcepto().requestFocus();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha comienzo");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnGenerarReporte() {
		if (jBtnGenerarReporte == null) {
			jBtnGenerarReporte = new JButtonPanambi();
			jBtnGenerarReporte.setMnemonic('G');
			jBtnGenerarReporte.setToolTipText("Generar reporte de ingresos y egresos");
			jBtnGenerarReporte.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doReporteGanancias();
				}
			});
			jBtnGenerarReporte.setText("Generar reporte");
		}
		return jBtnGenerarReporte;
	}
	
	@SuppressWarnings("deprecation")
	private void doReporteGanancias(){
		try{
			Date fechaDesde ;
			Date fechaHasta ;
			Sucursal sucursal = null;
//			Double ganancias = 0.0;
			List<Pago> pagos = new ArrayList<Pago>();
			List<PagoSalario> pagosSalarios = new ArrayList<PagoSalario>();
			
			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			String nombreSucursal = "";
			nombreSucursal= (String)getJCmbSucursal().getSelectedItem(); 
			
			if(!nombreSucursal.equals("TODAS")){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}else sucursal = null;
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			pagos = controladorPago.getPagosActivos(JFramePanambiMain.session.getConn(), fechaDesde, fechaHasta, sucursal);
			//pagosSalarios = new ControladorPagoSalario().getPagosSalariosActivos(JFramePanambiMain.session.getConn(), fechaDesde, fechaHasta);
			
			List <String> descripciones = new ArrayList<String>();
			List <Double> ingresos= new ArrayList<Double>();
			List <Double> egresos= new ArrayList<Double>();
			
			for(int i = 0;i<pagos.size();i++){
				if(pagos.get(i)!=null){
					if(pagos.get(i).getCompra()!=null){
						if(pagos.get(i).getCompra().getFacturanro().length()!=0){
							descripciones.add("Compra con factura nro = "+pagos.get(i).getCompra().getFacturanro());
						}else{
							descripciones.add("Compra sin numero de factura.");
						}
						egresos.add(pagos.get(i).getMonto());
						ingresos.add(0.0);
//						ganancias = ganancias - pagos.get(i).getMonto();
					}
					if(pagos.get(i).getGasto()!=null){
						if(pagos.get(i).getGasto().getNroComprobante().length()!=0){
							descripciones.add("Gasto con comprobante nro = "+pagos.get(i).getGasto().getNroComprobante());
						}else{
							descripciones.add("Gasto sin numero de comprbante.");
						}
						egresos.add(pagos.get(i).getMonto());
						ingresos.add(0.0);
//						ganancias = ganancias - pagos.get(i).getMonto();
					}
					if(pagos.get(i).getVenta()!=null){
						if(pagos.get(i).getVenta().getNroComprobante()!=0){
							descripciones.add("Venta con factura nro = "+pagos.get(i).getVenta().getNroComprobante());
						}else{
							descripciones.add("Venta sin numero de factura.");
						}
						ingresos.add(pagos.get(i).getMonto());
						egresos.add(0.0);
//						ganancias = ganancias + pagos.get(i).getMonto();
					}
					if(pagos.get(i).getPagoSalario()!=null){
						String patron = "dd/MM/yyyy";
						SimpleDateFormat formato = new SimpleDateFormat(patron);
						Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagos.get(i).getPagoSalario().getCodempleado());
						String des = "";
						des = "Pago de salario al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
						+formato.format(pagos.get(i).getPagoSalario().getFechaDesde())+" a "+formato.format(pagos.get(i).getPagoSalario().getFechaHasta());
						if(pagos.get(i).getNroRecibo()!=null){
							des+=" con recibo nro "+pagos.get(i).getNroRecibo();
						}
						descripciones.add(des);
						egresos.add(pagos.get(i).getMonto());
						ingresos.add(0.0);
//						ganancias = ganancias + pagos.get(i).getMonto();
					}
					
					if(pagos.get(i).getDeudaEmpleado()!=null){
						Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagos.get(i).getDeudaEmpleado().getPagoEmpleado().getCodempleado());
						String descripcion = "Cobro al empleado "+emp.getNombre()+""+emp.getApellido()+" por deuda de "+ pagos.get(i).getDeudaEmpleado().getConcepto();
						descripciones.add(descripcion);
						egresos.add(0.0);
						ingresos.add(pagos.get(i).getMonto());
						
					}
				}
			}
			
			for(int i = 0; i<pagosSalarios.size();i++){
				egresos.add(pagosSalarios.get(i).getTotalpagado());
				ingresos.add(0.0);
			}
			
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				PanambiUtils.setWaitCursor(getOwner());
				Map<String, Object> parameters = new HashMap<String, Object>();
				javax.swing.ImageIcon icon = new ImageIcon(JIReporteCompras.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
				Image image = icon.getImage();

				
				String periodo = "DESDE ";
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				if(getJDPFechaDesde().getDate()!=null){
					periodo+=formato.format(getJDPFechaDesde().getDate());
				}else{
					periodo+="INICIO";
				}
				
				periodo+=" HASTA ";
				
				if(getJDPFechaHasta().getDate()!=null){
					periodo+=formato.format(getJDPFechaHasta().getDate());
				}else{
					periodo+="FINAL.";
				}
				
				parameters.put("PERIODO", periodo);
				parameters.put("SUCURSAL", (String)getJCmbSucursal().getSelectedItem());
				parameters.put("LOGO", image);
				parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
				
				Double totalIngresos = 0.0;
				Double totalEgresos = 0.0;
				Double ganancias = 0.0;
				
				for(int i = 0;i<ingresos.size();i++){
					totalIngresos +=ingresos.get(i);
				}
				for(int i = 0;i<egresos.size();i++){
					totalEgresos+=egresos.get(i);
				}
				
				ganancias = totalIngresos - totalEgresos;
				
				parameters.put("TOTALINGRESO", totalIngresos);
				parameters.put("TOTALEGRESO", totalEgresos);
				parameters.put("TOTALGANANCIA", ganancias);
				
				URL url = JIReporteIngresosEgresos.class.getResource("/py/com/panambi/informes/reports/JRReporteIngresosEgresos.jasper");
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
				
				JasperPrint jasperPrint = null;
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceIngresosEgresos(JFramePanambiMain.session.getConn(),fechaDesde,fechaHasta, sucursal, descripciones, ingresos, egresos));
				
				List<?> pages = jasperPrint.getPages();
				if (!pages.isEmpty()) {
					JRPdfExporter exporter = new JRPdfExporter();
					File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.exportReport();
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(filetmp);
					}
				} else {
					DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
				}
			}

		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
		
	}
		
	 private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
	
	@Override
	public void setValues(Object obj, Integer source) {
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
}
