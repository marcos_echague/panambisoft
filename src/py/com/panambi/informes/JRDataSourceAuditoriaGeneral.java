package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class JRDataSourceAuditoriaGeneral implements JRDataSource {
	
	ResultSet rs = null;
	Integer cantidadColumnas;
	
	public JRDataSourceAuditoriaGeneral(Connection conn, ResultSet rs, Integer cantidadColumnas) throws Exception{
		try{
			this.cantidadColumnas = cantidadColumnas+3;
			this.rs = rs;
			
		}catch (Exception e ){
			throw e;
		}finally{
			//ConnectionManager.closeStatments(ps);
		}
	}
	
	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = "";
		try {
			if (arg0.getName().equals("registroAuditado")) {
				
				for(int i = 1; i<=cantidadColumnas; i++){;
					String cadenaTemporal="";
					try{
						cadenaTemporal = rs.getObject(i).toString().trim(); 
					}catch(Exception e){
						cadenaTemporal = "N/A";
					}
					obj += cadenaTemporal+",   ";
				}
					
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}	
	
}
