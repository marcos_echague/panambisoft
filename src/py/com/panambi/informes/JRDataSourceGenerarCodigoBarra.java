package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Producto;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceGenerarCodigoBarra implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceGenerarCodigoBarra(Connection conn, Producto producto) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT codproducto, descripcion, precio ";
		sql += "FROM productos ";
		sql += "WHERE codproducto = ? ";
		ps = conn.prepareStatement(sql);
		ps.setInt(1, producto.getCodProducto());
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codproducto")) {
				obj = rs.getInt("codproducto");
			}
			if (arg0.getName().equals("descripcion")) {
				obj = rs.getString("descripcion");
			}
			if (arg0.getName().equals("precio")) {
				obj = rs.getBigDecimal("precio");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
