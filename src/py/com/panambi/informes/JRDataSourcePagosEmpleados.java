package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPagoSalario;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.main.JFramePanambiMain;

public class JRDataSourcePagosEmpleados implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;
	Connection conn = null;

	public JRDataSourcePagosEmpleados(Connection conn, Empleado empleado , String tipoPagoSalario , Sucursal sucursal,String estado, Date fechaDesde, Date fechaHasta ) throws Exception {
		this.conn = conn;
		String sql = "";
		
//			sql = "SELECT pe.codpagoempleado AS codpagoempleado, "
//					+ "pe.fechapago AS fechapago, p.nrorecibo AS nrorecibo,"
//					+ "codempleado, fechadesde, fechahasta, tipo, p.estado AS estadopago, "
//					+ "pe.estado AS estadopagoempleado, salarionominal, "
//					+ "totaldescuento, totalpagado, codsucursal "
//					+ "FROM pagosempleados pe ,  pagos p "
//					+ "WHERE pe.codpagoempleado = p.codpagoempleado ";
		sql = "SELECT * FROM pagosempleados ";
		sql+="WHERE 1 = 1 ";
		
			if(empleado!=null){
				sql+=" AND codempleado=  "+empleado.getCodEmpleado()+" ";
			}
			if(tipoPagoSalario!=null){
				sql+=" AND tipo =  '"+tipoPagoSalario+"' ";
			}
		
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(estado!=null){
				if(estado.equals("P")){
					sql+=" AND estado =  'P' ";
				}else if (estado.equals("I")){
					sql+=" AND estado =  'NP' ";
				}
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY fechapago DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codigo")) {
				obj = rs.getInt("codpagoempleado");
			}
			if (arg0.getName().equals("fecha")) {
				obj = rs.getTimestamp("fechapago");
			}
			
			if (arg0.getName().equals("empleado")) {
				PagoSalario pagoEmpleado = new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(), rs.getInt("codpagoempleado"));
				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodempleado());
				obj = empleado.getNombre()+" "+empleado.getApellido();
			}
			
			if (arg0.getName().equals("concepto")) {
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				PagoSalario pagoEmpleado = new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(), rs.getInt("codpagoempleado"));
				String concepto ="";
				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodempleado());
				
				if(pagoEmpleado.getTipo().equals("S")){
					
					concepto= "Pago de salario al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
							+formato.format(pagoEmpleado.getFechaDesde())+" a "+formato.format(pagoEmpleado.getFechaHasta());
				}else if (pagoEmpleado.getTipo().equals("A")){
					
					concepto = "Pago de aguinaldo al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
							+formato.format(pagoEmpleado.getFechaDesde())+" a "+formato.format(pagoEmpleado.getFechaHasta());
				}else{
					
					concepto = "Pago de liquidacion al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" en fecha "+formato.format(pagoEmpleado.getFechapago());
				}
				
				obj = concepto;
			}
			
			if (arg0.getName().equals("recibo")) {
				if(rs.getInt("nrorecibo")!=0){
					obj = rs.getInt("nrorecibo");
				}
				
			}
			
			if (arg0.getName().equals("sucursal")) {
				PagoSalario pagoEmpleado = new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(), rs.getInt("codpagoempleado"));
				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodempleado());
				obj = new ControladorSucursal().getSucursal(conn, empleado.getSucursal().getNombre());
			}
			
//			if (arg0.getName().equals("total")) {
//				obj = rs.getDouble("importe");
//			}
			
			String estado = rs.getString("estado");
			if(estado.equals("P")){
				estado = "Pagado";
			}else {
				estado = "No pagado";
			}
			
			if (arg0.getName().equals("estadoPago")) {
				obj = estado;
			}
			if (arg0.getName().equals("totalnominal")) {
				obj = rs.getDouble("salarionominal");
			}
			if (arg0.getName().equals("totaldescuento")) {
				obj = rs.getDouble("totaldescuento");
			}
			if (arg0.getName().equals("apagar")) {
				obj = rs.getDouble("totalpagado");
			}
			Double totalPagado = 0.0;
			if(estado.equals("Pagado")){
				totalPagado = rs.getDouble("totalpagado");
			}
			if (arg0.getName().equals("totalpagado")) {
				obj = totalPagado;
			}
			
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
