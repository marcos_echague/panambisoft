package py.com.panambi.informes;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Usuario;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.prompts.PromptProductos;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIConsultarRegistroProductoPerdido extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3023195950354627593L;
	private JLabelPanambi lblpnmbUsuario;
	private JLabelPanambi lblpnmbSucursal;
	private JPanel panel;
	private JLabelPanambi lblpnmbCodifo;
	private JLabelPanambi lblpnmbDescripcion;
	private JTextFieldInteger jTxtCodProducto;
	private JComboBoxPanambi jCmbProducto;
	private JComboBoxPanambi jCmbUsuario;
	private JComboBoxPanambi jCmbSucursal;
	private ControladorUsuario controladorUsuario = new ControladorUsuario();
	private Usuario usuario;
	private List<String> usuarios = new ArrayList<String>();
	private ControladorProducto controladorProducto= new ControladorProducto();
	private Producto producto;
	private List<String> productos = new ArrayList<String>();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private Sucursal sucursal ;
	private List<String> sucursales = new ArrayList<String>();
	private JRadioButton rdbtnTodosLosProductos;
	private JButton btnSu;
	private JButton btnSs;
	private JButton btnSp;
	private JButtonPanambi btnpnmbLimpiar;
	private JRadioButton rdbtnHabilitarFiltros;
	private JButtonPanambi btnpnmbConsultar;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JLabelPanambi lblpnmbFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechaHasta;
	
	public JIConsultarRegistroProductoPerdido() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar productos dado de baja");
		setBounds(100, 100, 801, 461);
		getJPanelSouth().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbConsultar());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(635)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(54)
					.addComponent(getRdbtnTodosLosProductos(), GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addComponent(getRdbtnHabilitarFiltros(), GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
							.addGap(1)
							.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
							.addGap(1)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getBtnSu(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnSs(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 384, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
					.addGap(117)
					.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addComponent(getRdbtnTodosLosProductos())
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getRdbtnHabilitarFiltros())
							.addGap(30)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(40)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(52)
							.addComponent(getBtnSu())
							.addGap(38)
							.addComponent(getBtnSs()))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(19)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		decorate();
		setShortcuts(this);
		setHelp("listarproductosdadosdebaja");
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuario.setText("Usuario : ");
		}
		return lblpnmbUsuario;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal : ");
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbSucursal;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Producto", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(4)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getLblpnmbCodifo(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJCmbProducto(), GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getBtnSp()))))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(14)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodifo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(23)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJCmbProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getBtnSp())))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbCodifo() {
		if (lblpnmbCodifo == null) {
			lblpnmbCodifo = new JLabelPanambi();
			lblpnmbCodifo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodifo.setText("Codigo : ");
		}
		return lblpnmbCodifo;
	}
	private JLabelPanambi getLblpnmbDescripcion() {
		if (lblpnmbDescripcion == null) {
			lblpnmbDescripcion = new JLabelPanambi();
			lblpnmbDescripcion.setText("Descripcion : ");
			lblpnmbDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbDescripcion;
	}
	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
			jTxtCodProducto.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() != null) {
						Integer codproducto = ((JTextFieldInteger) e.getComponent()).getValor();
						setProductoFromCod(codproducto);
					}
					super.focusLost(e);
				}
			});
		}
		return jTxtCodProducto;
	}
	
	public void setProductoFromCod(Integer codproducto) {
		try {
			if (codproducto == null) {
				throw new ValidException();
			}
			Producto producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), codproducto);
			if (producto != null) {
				setValues(producto, null);
			} else {
				setNoValues(new Producto(), null);
			}
		} catch (ValidException e) {
			doPrompProducto();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doPrompProducto() {
		getJTxtCodProducto().setValor(null);
		getJCmbProducto().setSelectedItem(null);
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}
	
	private JComboBoxPanambi getJCmbProducto() {
		if (jCmbProducto == null) {
			jCmbProducto = new JComboBoxPanambi();
			jCmbProducto.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusProducto();
				}
			});
		}
		return jCmbProducto;
	}
	
	private void lostFocusProducto(){
		try {
			Producto pro = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), (String) jCmbProducto.getSelectedItem());
			if (pro != null) {
				getJTxtCodProducto().setValor(pro.getCodProducto());
			} 
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JComboBoxPanambi getJCmbUsuario() {
		if (jCmbUsuario == null) {
			jCmbUsuario = new JComboBoxPanambi();
		}
		return jCmbUsuario;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			usuario = null;
			producto = null;
			sucursal = null;
			
			poblarListaUsuarios();
			getJCmbUsuario().setModel(new ListComboBoxModel<>(usuarios));
			getJCmbUsuario().setSelectedItem(null);
			
			poblarListaProductos();
			getJCmbProducto().setModel(new ListComboBoxModel<>(productos));
			getJCmbProducto().setSelectedItem(null);
			
			poblarListaSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
				getBtnSs().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
				getBtnSs().setEnabled(false);
			}
			
			getRdbtnTodosLosProductos().setSelected(true);
			
			getJTxtCodProducto().setValor(null);
			getJDPFechaDesde().setDate(null);
			getJDPFechaHasta().setDate(null);
			
			getRdbtnHabilitarFiltros().setSelected(false);
			deshabilitarFiltros();
			
		}catch(Exception e ){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void poblarListaUsuarios() throws Exception{
		usuarios = new ArrayList<String>();
		List<Usuario> listausu = controladorUsuario.getUsuarios(JFramePanambiMain.session.getConn());

		for (Usuario usu : listausu) {
			usuarios.add(usu.getUsuario());
		}
	}
	
	private void poblarListaProductos() throws Exception {
		productos = new ArrayList<String>();
		List<Producto> listaprod = controladorProducto.getProductos(JFramePanambiMain.session.getConn());

		for (Producto prod : listaprod) {
			productos.add(prod.getDescripcion());
		}
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales= new ArrayList<String>();
		List<Sucursal> listasuc = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());

		for (Sucursal suc : listasuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = (Producto) obj;
			getJTxtCodProducto().setValor(producto.getCodProducto());
			getJCmbProducto().setSelectedItem(producto.getDescripcion());
		}
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = null;
			getJCmbProducto().setSelectedItem(null);
			getJTxtCodProducto().setValor(null);
		}
		
	}
	private JRadioButton getRdbtnTodosLosProductos() {
		if (rdbtnTodosLosProductos == null) {
			rdbtnTodosLosProductos = new JRadioButton("Todos los productos dados de baja");
			rdbtnTodosLosProductos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTodosProductosPerdidos();
				}
			});
		}
		return rdbtnTodosLosProductos;
	}
	
	private void doTodosProductosPerdidos(){
		doLimpiar();
	}
	
	private void deshabilitarFiltros(){
		
		getJCmbUsuario().setEnabled(false);
		getJCmbSucursal().setEnabled(false);
		getJCmbProducto().setEnabled(false);
		
		getBtnSu().setEnabled(false);
		getBtnSs().setEnabled(false);
		getBtnSp().setEnabled(false);
		
		getJDPFechaDesde().setEnabled(false);
		getJDPFechaHasta().setEnabled(false);
		
		getJTxtCodProducto().setEnabled(false);
	}
	
	private JButton getBtnSu() {
		if (btnSu == null) {
			btnSu = new JButton("S/U");
			btnSu.setToolTipText("Sin usuario");
			btnSu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSinUsuario();
				}
			});
		}
		return btnSu;
	}
	
	private void doSinUsuario(){
		getJCmbUsuario().setSelectedItem(null);
	}
	private void doSinSucursal(){
		getJCmbSucursal().setSelectedItem(null);
	}
	private void doSinProducto(){
		getJCmbProducto().setSelectedItem(null);
		getJTxtCodProducto().setValor(null);
	}
	
	private JButton getBtnSs() {
		if (btnSs == null) {
			btnSs = new JButton("S/S");
			btnSs.setToolTipText("Sin sucursal");
			btnSs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSinSucursal();
				}
			});
		}
		return btnSs;
	}
	private JButton getBtnSp() {
		if (btnSp == null) {
			btnSp = new JButton("S/P");
			btnSp.setToolTipText("Sin producto");
			btnSp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSinProducto();
				}
			});
		}
		return btnSp;
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbProducto());
	}
	
	@SuppressWarnings("deprecation")
	private void doConsultar() {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultaProveedores.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			Integer filtroUsuario = null;
			Integer filtroSucursal = null;
			Integer filtroProducto = null;
			Date fechaDesde ;
			Date fechaHasta ;
			
			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			if(getJCmbUsuario().getSelectedIndex()!=-1){
				usuario = controladorUsuario.getUsuario(JFramePanambiMain.session.getConn(), (String)getJCmbUsuario().getSelectedItem());
				filtroUsuario = usuario.getCodUsuario();
				parameters.put("USUARIOPRODUCTOPERDIDO", usuario.getUsuario());
			}else {
				parameters.put("USUARIOPRODUCTOPERDIDO", "TODOS");
			}
			if(getJCmbSucursal().getSelectedIndex()!=-1){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String) getJCmbSucursal().getSelectedItem());
				filtroSucursal = sucursal.getCodSucursal();
				parameters.put("SUCURSAL", sucursal.getNombre());
			}else{
				parameters.put("SUCURSAL", "TODAS");
			}
			if(getJCmbProducto().getSelectedIndex()!=-1){
				producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), (String) getJCmbProducto().getSelectedItem());
				filtroProducto = producto.getCodProducto();
				parameters.put("PRODUCTO", producto.getDescripcion());
			}else{
				parameters.put("PRODUCTO", "TODOS");
			}
			
			
			URL url = JIConsultaProveedores.class.getResource("/py/com/panambi/informes/reports/JRProductosPerdidos.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceRegistroProductoPerdido(JFramePanambiMain.session.getConn(),filtroUsuario,filtroSucursal,filtroProducto, fechaDesde, fechaHasta));
			
			List<?> pages = jasperPrint.getPages();
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
				
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}
	
	private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.DATE, dias);
        return new java.sql.Date(cal.getTimeInMillis());
   }
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JRadioButton getRdbtnHabilitarFiltros() {
		if (rdbtnHabilitarFiltros == null) {
			rdbtnHabilitarFiltros = new JRadioButton("Habilitar Filtros");
			rdbtnHabilitarFiltros.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doHabilitarFiltros();
				}
			});
		}
		return rdbtnHabilitarFiltros;
	}
	
	private void doHabilitarFiltros(){
		getRdbtnTodosLosProductos().setSelected(false);
		getRdbtnHabilitarFiltros().setSelected(true);
		getJDPFechaDesde().setDate(new java.util.Date());
		getJDPFechaHasta().setDate(new java.util.Date());
		habilitarFiltros();
	}
	
	private void habilitarFiltros(){
		
		getJCmbUsuario().setEnabled(true);
		getJCmbSucursal().setEnabled(true);
		getJCmbProducto().setEnabled(true);
		
		getBtnSu().setEnabled(true);
		getBtnSs().setEnabled(true);
		getBtnSp().setEnabled(true);
		
		getJDPFechaDesde().setEnabled(true);
		getJDPFechaHasta().setEnabled(true);
		
		getJTxtCodProducto().setEnabled(true);
	}
	private JButtonPanambi getBtnpnmbConsultar() {
		if (btnpnmbConsultar == null) {
			btnpnmbConsultar = new JButtonPanambi();
			btnpnmbConsultar.setMnemonic('C');
			btnpnmbConsultar.setToolTipText("Generar reporte");
			btnpnmbConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			btnpnmbConsultar.setText("Consultar");
		}
		return btnpnmbConsultar;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JLabelPanambi getLblpnmbFechaDesde() {
		if (lblpnmbFechaDesde == null) {
			lblpnmbFechaDesde = new JLabelPanambi();
			lblpnmbFechaDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDesde.setText("Fecha desde : ");
		}
		return lblpnmbFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.setToolTipText("Fecha de inicio");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
}
