package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.MovimientoProducto;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceNotaRemision implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceNotaRemision(Connection conn, MovimientoProducto movimiento) throws Exception {
		String sql = "SELECT dm.nroitem, dm.cantidad AS cantidad , dm.codproducto AS codigo, "
				+ " (SELECT descripcion FROM productos p WHERE p.codproducto = dm.codproducto) AS descripcion  "
				+ " FROM detallemovimientos dm WHERE codmovimientoproducto = ? " ;
		sql += "order by nroitem ";
		ps = conn.prepareStatement(sql);
		ps.setInt(1, movimiento.getCodMovimientoProducto());
		rs = ps.executeQuery();
	}
	
	public JRDataSourceNotaRemision(Connection conn, Integer codmovimiento) throws Exception {
		String sql = "SELECT dm.nroitem, dm.cantidad AS cantidad , dm.codproducto AS codigo, "
				+ " (SELECT descripcion FROM productos p WHERE p.codproducto = dm.codproducto) AS descripcion  "
				+ " FROM detallemovimientos dm WHERE codmovimientoproducto = ? " ;
		sql += "order by nroitem ";
		ps = conn.prepareStatement(sql);
		ps.setInt(1, codmovimiento);
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("cantidad")) {
				obj = rs.getInt("cantidad");
			}
			if (arg0.getName().equals("descripcion")) {
				obj = rs.getString("descripcion");
			}
			if (arg0.getName().equals("codigo")) {
				obj = rs.getInt("codigo");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
