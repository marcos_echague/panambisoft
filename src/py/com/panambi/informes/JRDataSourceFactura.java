package py.com.panambi.informes;

import java.text.DecimalFormat;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Venta;

public class JRDataSourceFactura implements JRDataSource {
	private Venta venta = null;
	private Iterator<DetalleVenta> detalle = null;
	private DetalleVenta detalleVenta = null;
	private Integer itemnro = 0;
	public static final Integer MAXITEMS = 8;
	private DecimalFormat frmFactura = new DecimalFormat("0000000");
	
	public JRDataSourceFactura(Venta venta) throws Exception {
		this.venta = venta;
		this.detalle = venta.getDetalle().iterator();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (detalleVenta != null) {
				if (arg0.getName().equals("descripcion")) {
					obj = detalleVenta.getProducto().getDescripcion();
				}
				if (arg0.getName().equals("cantidad")) {
					obj = detalleVenta.getCantidad();
				}
				if (arg0.getName().equals("preciounitario")) {
					obj = detalleVenta.getPrecioUnitario();
				}
				if (arg0.getName().equals("descuentounitario")) {
					obj = detalleVenta.getDescuentoUnitario();
				}
				if (arg0.getName().equals("totalitem")) {
					obj = detalleVenta.getTotalItem();
				}
				if (arg0.getName().equals("comprobantenro")) {
					obj = venta.getNroComprobante();
				}
				if (arg0.getName().equals("fecha")) {
					obj = venta.getFecha();
				}
				if (arg0.getName().equals("facturanro")) {
					obj = "001-001-" + frmFactura.format(venta.getNroComprobante());
				}
				if (arg0.getName().equals("tipoventa")) {
					obj = venta.getTipoVenta();
				}
				if (arg0.getName().equals("ruc")) {
					obj = venta.getCliente().getNroDocumento();
				}
				if (arg0.getName().equals("razonsocial")) {
					obj = venta.getCliente().getNombres();
					if (venta.getCliente().getApellidos() != null) {
						obj += " " + venta.getCliente().getApellidos();
					}
				}
				if (arg0.getName().equals("direccion")) {
					obj = venta.getCliente().getDireccion();
				}
				if (arg0.getName().equals("telefono")) {
					obj = venta.getCliente().getTelefono();
				}
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			itemnro++;
			ret = (detalle.hasNext() || itemnro <= MAXITEMS);
			if (detalle.hasNext()) {
				detalleVenta = detalle.next();
			} else {
				detalleVenta = null;
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
