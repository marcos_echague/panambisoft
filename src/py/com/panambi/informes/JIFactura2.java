package py.com.panambi.informes;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIFactura2 extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4244313499878531446L;
	private JTextFieldInteger textFieldInteger;
	private JButtonPanambi btnpnmbGenerar;
	private JButtonPanambi btnpnmbGenerar_1;
	public JIFactura2() throws Exception{
		initialize();
	}
	private void initialize() {
		getJPanelCentral().setLayout(null);
		getJPanelCentral().add(getTextFieldInteger());
		getJPanelSouth().add(getBtnpnmbGenerar());
		getJPanelSouth().add(getBtnpnmbGenerar_1());
	}
	private JTextFieldInteger getTextFieldInteger() {
		if (textFieldInteger == null) {
			textFieldInteger = new JTextFieldInteger();
			textFieldInteger.setBounds(184, 23, 196, 21);
		}
		return textFieldInteger;
	}
	private JButtonPanambi getBtnpnmbGenerar() {
		if (btnpnmbGenerar == null) {
			btnpnmbGenerar = new JButtonPanambi();
			btnpnmbGenerar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerar();
				}
			});
			btnpnmbGenerar.setText("Generar");
		}
		return btnpnmbGenerar;
	}
	
	private void doGenerar(){
		
			try {		
				Venta venta = new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), getTextFieldInteger().getValor());
					PanambiUtils.setWaitCursor(getOwner());
					Map<String, Object> parameters = new HashMap<String, Object>();
					javax.swing.ImageIcon icon = new ImageIcon(JIGenerarCodigoBarra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
					Image image = icon.getImage();
					
					parameters.put("LOGO", image);
					parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
					parameters.put("FECHA", venta.getFecha());
					parameters.put("FACTURA", venta.getNroComprobante());
					if(venta.getTipoVenta().equals("C")){
						parameters.put("CREDITO", "X");
						parameters.put("CONTADO", " ");
					}else{
						parameters.put("CREDITO", " ");
						parameters.put("CONTADO", "X");
					}
					
					parameters.put("RUC", venta.getCliente().getNroDocumento());
					parameters.put("RAZONSOCIAL", venta.getCliente().getNombres()+" "+venta.getCliente().getApellidos());
					parameters.put("DIRECCION", venta.getCliente().getDireccion());
					parameters.put("TELEFONO", venta.getCliente().getTelefono());
					
					parameters.put("TOTALLETRAS", venta.getTotalLetras());
					parameters.put("IVA", venta.getIva());
					parameters.put("TOTAL", venta.getTotal());
					parameters.put("SUBTOTAL", venta.getTotal()-venta.getIva());
					
					
					URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRFacturaVenta.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					
					JasperPrint jasperPrint;
					
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceFactura(JFramePanambiMain.session.getConn(), venta));
					
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JasperViewer.viewReport(jasperPrint, false);
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}
				
			} catch (Exception e) {
				DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
			} finally {
				PanambiUtils.setDefaultCursor(getOwner());
			}
	}
	
	private void doGenerarPagare(){
		
		try {		
			Venta venta = new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), getTextFieldInteger().getValor());
				PanambiUtils.setWaitCursor(getOwner());
				Map<String, Object> parameters = new HashMap<String, Object>();
				
				parameters.put("PAGARENRO", venta.getNroPagare());
				
				String patronDia = "dd";
				SimpleDateFormat formatoDia = new SimpleDateFormat(patronDia);
				parameters.put("DIA", formatoDia.format(venta.getFecha()));
				
				String patronMes = "MM";
				SimpleDateFormat formatoMes = new SimpleDateFormat(patronMes);
				String numMes = formatoMes.format(venta.getFecha()); 
				String mes = "";
				
				if(numMes.equals("01")){
					mes = "ENERO";
				}else if(numMes.equals("02")){
					mes = "FEBRERO";
				}else if(numMes.equals("03")){
					mes = "MARZO";
				}else if(numMes.equals("04")){
					mes = "ABRIL";
				}else if(numMes.equals("05")){
					mes = "MAYO";
				}else if(numMes.equals("06")){
					mes = "JUNIO";
				}else if(numMes.equals("07")){
					mes = "JULIO";
				}else if(numMes.equals("08")){
					mes = "AGOSTO";
				}else if(numMes.equals("09")){
					mes = "SETIEMBRE";
				}else if(numMes.equals("10")){
					mes = "OCTUBRE";
				}else if(numMes.equals("11")){
					mes = "NOVIEMBRE";
				}else {
					mes = "DICIEMBRE";
				}
				
				parameters.put("MES", mes);
				
				String patronAnho = "yyyy";
				SimpleDateFormat formatoAnho= new SimpleDateFormat(patronAnho);
				
				parameters.put("ANHO", formatoAnho.format(venta.getFecha()));
				
				URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRPagareCuotas.jasper");
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
				
				JasperPrint jasperPrint;
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagare(JFramePanambiMain.session.getConn(), venta));
				
				
				List<?> pages = jasperPrint.getPages();
				if (!pages.isEmpty()) {
					JasperViewer.viewReport(jasperPrint, false);
				} else {
					DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
				}
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
}
	
	private JButtonPanambi getBtnpnmbGenerar_1() {
		if (btnpnmbGenerar_1 == null) {
			btnpnmbGenerar_1 = new JButtonPanambi();
			btnpnmbGenerar_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarPagare();
				}
			});
			btnpnmbGenerar_1.setText("Generar 2 pagare");
		}
		return btnpnmbGenerar_1;
	}
}
