package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.TipoGasto;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorTipoGasto;

public class JRDataSourceGastos implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;
	Connection conn = null;

	public JRDataSourceGastos(Connection conn, TipoGasto tipoGasto , Sucursal sucursal,String estado, Date fechaDesde, Date fechaHasta ) throws Exception {
		this.conn = conn;
		String sql = "";
		
			sql = "SELECT g.codgasto AS codigoGasto, g.fechapago AS fechapago, g.codtipogasto AS tipogasto, g.nrocomprobante AS nrocomprobante, "
					+ "codsucursal, importe, g.estado AS estadoGasto, "
					+ "(SELECT SUM(p.monto) FROM pagos p WHERE p.codgasto = g.codgasto AND p.estado = 'A' ) AS pagado "
					+ " FROM gastos g ";
			sql+=" WHERE 1 = 1 ";
			if(tipoGasto!=null){
				sql+=" AND codtipogasto =  "+tipoGasto.getCodigo()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
		
			if(estado!=null){
				if(estado.equals("A")){
					sql+=" AND estado =  'A' ";
				}else if (estado.equals("I")){
					sql+=" AND estado =  'I' ";
				}
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY codigoGasto DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codigo")) {
				obj = rs.getInt("codigoGasto");
			}
			if (arg0.getName().equals("fecha")) {
				obj = rs.getTimestamp("fechapago");
			}
			if (arg0.getName().equals("concepto")) {
				obj = new ControladorTipoGasto().getTipoGasto(conn, rs.getInt("tipogasto")).getConcepto();
			}
			
			if (arg0.getName().equals("factura")) {
				obj = rs.getString("nrocomprobante");
			}
			
			if (arg0.getName().equals("sucursal")) {
				obj = new ControladorSucursal().getSucursal(conn, rs.getInt("codsucursal")).getNombre();
			}
			
			if (arg0.getName().equals("total")) {
				obj = rs.getDouble("importe");
			}
			
			String estado = rs.getString("estadoGasto");
			if(estado.equals("I")){
				estado = "Anulado";
			}else {
				estado = "Activo";
			}
			
			if (arg0.getName().equals("estado")) {
				obj = estado;
			}
			if (arg0.getName().equals("totalpagado")) {
				obj = rs.getDouble("pagado");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
