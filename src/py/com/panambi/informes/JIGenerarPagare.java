package py.com.panambi.informes;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIGenerarPagare extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4244313499878531446L;
	private JTextFieldInteger jTxtNroFactura;
	private JButtonPanambi btnpnmbGenerarPagare;
	private JLabelPanambi lblpnmbIngreseElNumero;
	private JLabelPanambi lblpnmbPresioneFPara;

	public JIGenerarPagare() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Impresion de pagares");
		setBounds(100, 100, 448, 236);
		getJPanelSouth().add(getBtnpnmbGenerarPagare());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(32, Short.MAX_VALUE)
					.addComponent(getLblpnmbIngreseElNumero(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
					.addGap(18))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbIngreseElNumero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(10, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("impresionpagare");
		//doLimpiar();
	}
	
//	private void doLimpiar(){
//		cliente =null;
//		venta = null;
//		getJTxtNroFactura().setValor(null);
//		getJTxtNroCedula().setText("");
//		getJTxtNombreApellido().setText("");
//
//		if(getTablePanambi().getRowCount()!=0){
//			getTablePanambi().resetData(0);
//		}
//	}
	
//	private void lostFocusNroCedula(){
//		
//		try{
//			Integer longitud = 0;
//			try{
//				longitud = getJTxtNroCedula().getText().length();
//			}catch(Exception e){
//				longitud = 0;
//			}
//			
//			if(longitud==0){
//				if(venta!=null){
//					setValues(venta, null);
//				}
//			}else{
//				
//				cliente= new ControladorVenta().get(JFramePanambiMain.session.getConn(), getJTxtCodigo().getValor());
//				
//				if(prod != null){
//					setValues(prod, null);
//					//getRdbtnTodos().setSelected(false);
//				}else{
//					producto=null;
//					getJTxtCodigo().setValor(null);
//					getJTxtDescripcion().setText("");
//					getRdbtnTodos().setSelected(true);
//					doPrompProducto();
//				}
//			}
//		}catch(Exception e){
//			logger.error(e.getMessage(),e);
//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//		}
//		
//	}
	
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
//			jTxtNroFactura.addFocusListener(new FocusAdapter() {
//				@Override
//				public void focusLost(FocusEvent arg0) {
//					lostFocusNroFactura();
//				}
//			});
		}
		return jTxtNroFactura;
	}
	
	
//	private void lostFocusNroFactura(){
//		try{
//			Integer nroFactura;
//			try{
//				nroFactura = getJTxtNroFactura().getValor();
//			}catch(Exception e){
//				nroFactura  = 0;
//			}
//			Venta ven = new ControladorVenta().getVentaPorFactura(JFramePanambiMain.session.getConn(), nroFactura);
//			if(ven == null){
//				DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nLa factura no existe." , DlgMessage.ERROR_MESSAGE);
//			}else{
//				if(ven.getTipoVenta().equals("D")){
//					DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nFactura contado, debe ingresar una factura a cr�dito." , DlgMessage.ERROR_MESSAGE);
//					getJTxtNroFactura().setValor(null);
//				}else{
//					setValues(ven, null);
//				}
//			}
//		}catch(Exception e){
//			logger.error(e.getMessage(), e);
//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//		}
//	}
	
	
	private void doGenerarPagare(){
		
		try {	
			Integer nroFactura;
			Venta venta = null;
			try{
				nroFactura = getJTxtNroFactura().getValor();
				venta = new ControladorVenta().getVentaPorFactura(JFramePanambiMain.session.getConn(), nroFactura);
			}catch(Exception e){
				nroFactura  = 0;
			}
			
			if(venta!=null){
				if(venta.getTipoVenta().equals("C")){
					PanambiUtils.setWaitCursor(getOwner());
					Map<String, Object> parameters = new HashMap<String, Object>();
					
					parameters.put("PAGARENRO", venta.getNroPagare());
					
					String patronDia = "dd";
					SimpleDateFormat formatoDia = new SimpleDateFormat(patronDia);
					parameters.put("DIA", formatoDia.format(venta.getFecha()));
					
					String patronMes = "MM";
					SimpleDateFormat formatoMes = new SimpleDateFormat(patronMes);
					String numMes = formatoMes.format(venta.getFecha()); 
					String mes = "";
					
					if(numMes.equals("01")){
						mes = "ENERO";
					}else if(numMes.equals("02")){
						mes = "FEBRERO";
					}else if(numMes.equals("03")){
						mes = "MARZO";
					}else if(numMes.equals("04")){
						mes = "ABRIL";
					}else if(numMes.equals("05")){
						mes = "MAYO";
					}else if(numMes.equals("06")){
						mes = "JUNIO";
					}else if(numMes.equals("07")){
						mes = "JULIO";
					}else if(numMes.equals("08")){
						mes = "AGOSTO";
					}else if(numMes.equals("09")){
						mes = "SETIEMBRE";
					}else if(numMes.equals("10")){
						mes = "OCTUBRE";
					}else if(numMes.equals("11")){
						mes = "NOVIEMBRE";
					}else {
						mes = "DICIEMBRE";
					}
					
					parameters.put("MES", mes);
					
					String patronAnho = "yyyy";
					SimpleDateFormat formatoAnho= new SimpleDateFormat(patronAnho);
					
					parameters.put("ANHO", formatoAnho.format(venta.getFecha()));
					
					URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRPagareCuotas.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					
					JasperPrint jasperPrint;
					
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagare(JFramePanambiMain.session.getConn(), venta));
					
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}

				}else{
					DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nFactura contado, debe ingresar una factura a cr�dito." , DlgMessage.ERROR_MESSAGE);
				}
			}else{
				DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nLa factura no existe." , DlgMessage.ERROR_MESSAGE);
			}
							
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
}
	
	private JButtonPanambi getBtnpnmbGenerarPagare() {
		if (btnpnmbGenerarPagare == null) {
			btnpnmbGenerarPagare = new JButtonPanambi();
			btnpnmbGenerarPagare.setToolTipText("Imprimir pagare");
			btnpnmbGenerarPagare.setMnemonic('G');
			btnpnmbGenerarPagare.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarPagare();
				}
			});
			btnpnmbGenerarPagare.setText("Generar pagare");
		}
		return btnpnmbGenerarPagare;
	}
	private JLabelPanambi getLblpnmbIngreseElNumero() {
		if (lblpnmbIngreseElNumero == null) {
			lblpnmbIngreseElNumero = new JLabelPanambi();
			lblpnmbIngreseElNumero.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbIngreseElNumero.setText("N\u00FAmero de factura : ");
		}
		return lblpnmbIngreseElNumero;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	@Override
	public void setValues(Object obj, Integer source) {
//		if(obj instanceof Venta){
//			this.venta = (Venta) obj;
//			this.cliente = venta.getCliente();
//			getJTxtNroCedula().setText(cliente.getNroDocumento());
//			getJTxtNombreApellido().setText(cliente.getNombres()+" "+cliente.getApellidos());
//		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
}
