package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceRegistroProductoPerdido implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceRegistroProductoPerdido(Connection conn, Integer codusuario, Integer codsucursal, Integer codproducto, Date fechaDesde , Date fechaHasta) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT codproductoperdido, fecha, cantidad, comentario,"
				+ "(SELECT descripcion FROM productos p WHERE p.codproducto = pp.codproducto) producto , "
				+ "(SELECT usuario FROM usuarios u WHERE u.codusuario= pp.codusuario) usuario, "
				+ "(SELECT nombre FROM sucursales s WHERE s.codsucursal= pp.codsucursal) sucursal  ";
		sql += "FROM productosperdidos pp ";
		sql += "WHERE 1 = 1 ";
		
		if(codusuario!=null ){
			sql+=" AND pp.codusuario = "+codusuario;
		}
		
		if(codsucursal!=null ){
			sql+=" AND pp.codsucursal= "+codsucursal;
		}
		if(codproducto!=null ){
			sql+=" AND pp.codproducto = "+codproducto;
		}
		
		if(fechaDesde!=null && fechaHasta!=null){
			sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
		}else if (fechaDesde ==null && fechaHasta !=null){
			sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
		}else if (fechaDesde !=null && fechaHasta ==null){
			sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
		}
		
		
//		if(codusuario!=null && codsucursal!=null && codproducto!=null){
//			sql+="WHERE pp.codusuario = "+codusuario
//				+ "AND pp.codsucursal = "+codsucursal
//				+ "AND pp.codproducto = "+codproducto;
//		}else if(codusuario!=null && codsucursal!=null && codproducto==null){
//			sql+="WHERE pp.codusuario = "+codusuario
//			    + "AND pp.codsucursal = "+codsucursal;
//		}else if(codusuario!=null && codsucursal==null && codproducto!=null){
//			sql+="WHERE pp.codusuario = "+codusuario
//			     +"AND pp.codproducto = "+codproducto;
//		}else if(codusuario==null && codsucursal!=null && codproducto!=null){
//			sql+="WHERE pp.codsucursal = "+codsucursal
//				  + "AND pp.codproducto = "+codproducto;
//		}else if(codusuario!=null && codsucursal==null && codproducto==null){
//			sql+="WHERE pp.codusuario= "+codusuario;
//		}else if(codusuario==null && codsucursal!=null && codproducto==null){
//			sql+="WHERE pp.codsucursal = "+codsucursal;
//		}else if(codusuario==null && codsucursal==null && codproducto!=null){
//			sql+="WHERE pp.codproducto= "+codproducto;
//		}
			
		sql += "ORDER BY fecha ";
		ps = conn.prepareStatement(sql);
		
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codproductoperdido")) {
				obj = rs.getInt("codproductoperdido");
			}
			if (arg0.getName().equals("fecha")) {
				obj = rs.getDate("fecha");
			}
			if (arg0.getName().equals("cantidad")) {
				obj = rs.getInt("cantidad");
			}
			if (arg0.getName().equals("comentario")) {
				obj = rs.getString("comentario");
			}
			if (arg0.getName().equals("producto")) {
				obj = rs.getString("producto");
			}
			if (arg0.getName().equals("usuario")) {
				obj = rs.getString("usuario");
			}
			if (arg0.getName().equals("sucursal")) {
				obj = rs.getString("sucursal");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
