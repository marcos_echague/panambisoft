package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorBanco;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorFormaPago;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.main.JFramePanambiMain;

public class JRDataSourceFlujoDeCaja implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceFlujoDeCaja(Connection conn, Date fechaDesde , Date fechaHasta, Sucursal sucursal) throws Exception {
		// aqui se prepara y ejecuta el sql
//		String sql = "SELECT p.codpago AS codpago, 0 AS codpagoempleado, p.codventa AS venta, p.codcompra AS compra, p.codgasto AS gasto, 0 AS pagoempleado,  dp.codformapago AS formapago, p.monto, p.fecha AS fecha "+
//				"FROM pagos p , detallepagos dp "+ 
//				"WHERE p.codpago = dp.codpago "+ 
//				"AND p.estado = 'A' ";
//				if(fechaDesde!=null && fechaHasta!=null){
//					sql+=" AND p.fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
//				}else if (fechaDesde ==null && fechaHasta !=null){
//					sql+=" AND p.fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
//				}else if (fechaDesde !=null && fechaHasta ==null){
//					sql+=" AND p.fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
//				}
//				sql+="UNION "+
//				"SELECT 0 AS codpago, pe.codpagoempleado, 0, 0, 0 ,pe.codpagoempleado AS pagoempleado ,1 AS formapago , pe.totalpagado, pe.fechapago AS fecha "+
//				"FROM  pagosempleados pe, detallepagosempleados dpe "+
//				"WHERE pe.codpagoempleado = dpe.codpagoempleado "+
//				"AND pe.estado = 'A' ";
//				if(fechaDesde!=null && fechaHasta!=null){
//					sql+=" AND pe.fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
//				}else if (fechaDesde ==null && fechaHasta !=null){
//					sql+=" AND pe.fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
//				}else if (fechaDesde !=null && fechaHasta ==null){
//					sql+=" AND pe.fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
//				}
//				sql+="ORDER BY formapago, fecha";
		String sql = "SELECT nroitem, codbanco, chequenro, bouchernro, notacredito, dp.codpago AS codpago, dp.monto AS monto, codformapago , fecha "+
				"FROM pagos p , detallepagos dp "+ 
				"WHERE p.codpago = dp.codpago "+ 
				"AND p.estado = 'A' ";
		
				if(sucursal!=null){
					sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
				}
		
				if(fechaDesde!=null && fechaHasta!=null){
					sql+=" AND p.fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
				}else if (fechaDesde ==null && fechaHasta !=null){
					sql+=" AND p.fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
				}else if (fechaDesde !=null && fechaHasta ==null){
					sql+=" AND p.fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
				}
				sql+="ORDER BY codformapago, fecha";

		

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			Pago pago = null;
			DetallePago detallePago = new DetallePago();
			detallePago.setNroitem(rs.getInt("nroitem"));
			detallePago.setMonto(rs.getDouble("monto"));
			detallePago.setFormaPago(new ControladorFormaPago().getFormaPago(JFramePanambiMain.session.getConn(), rs.getInt("codformapago")));
			
			if (rs.getInt("codbanco")!=0){
				detallePago.setBanco(new ControladorBanco().getBanco(JFramePanambiMain.session.getConn(), rs.getInt("codbanco")));
			}
			
			if(rs.getInt("notacredito")!=0){
				detallePago.setNotacredito(rs.getInt("notacredito"));
			}
			
			if(rs.getString("chequenro")!=null){
				detallePago.setChequenro(rs.getString("chequenro"));
			}
			
			if(rs.getString("bouchernro")!=null){
				detallePago.setBouchernro(rs.getString("bouchernro"));
			}
			
			String descripcion = "";
			String formaPago="";
			Double debe = 0.0;
			Double haber = 0.0;
			Object fecha = null;
			formaPago = detallePago.getFormaPago().getNombre();
			if(rs.getInt("codpago")!=0){
				pago = new ControladorPago().getPago(JFramePanambiMain.session.getConn(),rs.getInt("codpago"));
			}
			                  
			if(pago!=null){
				if(pago.getCompra()!=null){
		 			if(pago.getCompra().getFacturanro().length()!=0){
						descripcion = "Pago por compra con factura nro "+pago.getCompra().getFacturanro();
					}else{
							descripcion = "Pago por compra sin numero de factura";
					}
					debe = detallePago.getMonto();
					haber = 0.0;
				}
				if(pago.getGasto()!=null){
					if(pago.getGasto().getNroComprobante().length()!=0){
						descripcion = "Pago por gasto en concepto de "+pago.getGasto().getTipoGasto().getConcepto()+" con comprobante nro "+pago.getGasto().getNroComprobante();
						}else{
						descripcion = "Pago por gasto en concepto de "+pago.getGasto().getTipoGasto().getConcepto()+"sin numero de comprbante";
					}
					debe  = detallePago.getMonto();
					haber = 0.0;
				}
				if(pago.getVenta()!=null){
					if(pago.getNroRecibo()!=0){
						descripcion = "Cobro de cuota de venta con factura nro "+pago.getVenta().getNroComprobante()+" con recibo nro "+ pago.getNroRecibo();
					}else if(pago.getVenta().getNroComprobante()!=null){
						descripcion = "Cobro por venta con factura nro "+pago.getVenta().getNroComprobante();
					}else{
						descripcion = "Cobro por venta sin numero de factura";
					}
					haber = detallePago.getMonto();
					debe =  0.0;
					
				}
				
				if(pago.getPagoSalario()!=null){
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getPagoSalario().getCodempleado());
					
					if(pago.getPagoSalario().getTipo().equals("S")){
						
						descripcion = "Pago de salario al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
								+formato.format(pago.getPagoSalario().getFechaDesde())+" a "+formato.format(pago.getPagoSalario().getFechaHasta());
					}else if (pago.getPagoSalario().getTipo().equals("A")){
						
						descripcion = "Pago de aguinaldo al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" periodo de "
								+formato.format(pago.getPagoSalario().getFechaDesde())+" a "+formato.format(pago.getPagoSalario().getFechaHasta());
					}else{
						
						descripcion = "Pago de liquidacion al empleado "+empleado.getNombre()+" "+empleado.getApellido()+" en fecha "+formato.format(pago.getPagoSalario().getFechapago());
					}
					
					if(pago.getNroRecibo()!=null){
						descripcion+=" con recibo nro "+pago.getNroRecibo();
					}
					
					
					debe = detallePago.getMonto();
					haber =  0.0;
					
				}
				
				if(pago.getDeudaEmpleado()!=null){
					
					Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getDeudaEmpleado().getPagoEmpleado().getCodempleado());
					descripcion = "Cobro al empleado "+emp.getNombre()+""+emp.getApellido()+"por deuda de "+ pago.getDeudaEmpleado().getConcepto();
					haber = detallePago.getMonto();
					debe =  0.0;
					
				}
				
				fecha = pago.getFecha();
				if(detallePago.getBanco()!=null){
					descripcion+=". Banco "+detallePago.getBanco().getDescripcion();
				}
				if(detallePago.getChequenro()!=null){
					descripcion+=". Cheque nro "+detallePago.getChequenro();
				}
				if(detallePago.getBouchernro()!=null){
					descripcion+=". Boucher nro "+detallePago.getBouchernro();
				}
				if(detallePago.getNotacredito()!=null){
					descripcion+=". Nota de cr�dito nro "+detallePago.getNotacredito();
				}
				
			}
			
//			if(pagoEmpleado!=null){
//				
//				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodempleado());
//				descripcion = "Pago de salario a empleado "+empleado.getNombre()+" "+empleado.getApellido();
//
//				String patron = "dd/MM/yyyy";
//				SimpleDateFormat formato = new SimpleDateFormat(patron);
//				if(pagoEmpleado.getFechaDesde()!=null && pagoEmpleado.getFechaHasta()!=null){
//					descripcion+=" periodo "+formato.format(pagoEmpleado.getFechaDesde())+" a "+formato.format(pagoEmpleado.getFechaHasta());
//				}else{
//					descripcion = " en fecha"+formato.format(pagoEmpleado.getFechapago());
//				}
//				debe = pagoEmpleado.getTotalpagado();
//				haber = 0.0;
//				fecha = pagoEmpleado.getFechapago();
//			}
			
			
			if (arg0.getName().equals("formapago")) {
				obj = formaPago;
			}
			
			if (arg0.getName().equals("concepto")) {
				obj = descripcion;
			}
			
			if (arg0.getName().equals("fecha")) {
				obj = fecha;
			}
			
			if (arg0.getName().equals("debe")) {
				obj = debe;
			}
			if (arg0.getName().equals("haber")) {
				obj = haber;
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
