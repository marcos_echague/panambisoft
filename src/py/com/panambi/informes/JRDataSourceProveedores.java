package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceProveedores implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceProveedores(Connection conn,String tipoReporte) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT codproveedor, razonsocial, direccion, telefono, nrodocumento, estado ";
		sql += "FROM proveedores ";
		
		if (tipoReporte.equals("A")) {
			sql += "WHERE estado = 'A' ";
			
		}else if (tipoReporte.equals("I")){
			sql += "WHERE estado = 'I' ";
		} 
		sql += "order by razonsocial, codproveedor ";
		ps = conn.prepareStatement(sql);
		
		rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codproveedor")) {
				obj = rs.getInt("codproveedor");
			}
			if (arg0.getName().equals("razonsocial")) {
				obj = rs.getString("razonsocial");
			}
			if (arg0.getName().equals("direccion")) {
				obj = rs.getString("direccion");
			}
			if (arg0.getName().equals("telefono")) {
				obj = rs.getString("telefono");
			}
			if (arg0.getName().equals("nrodocumento")) {
				obj = rs.getString("nrodocumento");
			}
			if (arg0.getName().equals("estado")) {
				String est = rs.getString("estado");
				if(est.equals("A")){
					est = "Activo";
				}else{
					est = "Inactivo";
				}
				obj = est;
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
