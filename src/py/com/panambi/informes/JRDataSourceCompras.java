package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.controller.ControladorSucursal;

public class JRDataSourceCompras implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;
	Connection conn = null;

	public JRDataSourceCompras(Connection conn, Proveedor proveedor, Sucursal sucursal,boolean pagados, boolean pendientes, boolean anulados, Date fechaDesde, Date fechaHasta ) throws Exception {
		this.conn = conn;
		String sql = "";
		
			sql = "SELECT c.codcompra AS codigoCompra, c.fecha AS fechaCompra, codproveedor, facturanro, codsucursal, total, c.estado AS estadoCompra, "
					+ "(SELECT SUM(p.monto) FROM pagos p WHERE p.codcompra = c.codcompra AND p.estado = 'A' ) AS pagado "
//					+ " (SELECT p.razonsocial FROM proveedores p WHERE c.codproveedor = p.codproveedor) AS proveedor, "
//					+ " c.facturanro, "
//					+ " (SELECT s.nombre FROM sucursales s WHERE c.codsucursal = s.codsucursal) AS sucursal, "
//					+ " c.total, "; 
//					+ "(SELECT CASE WHEN  c.estado = 'I' THEN 'ANULADO' WHEN c.estado = 'P' THEN 'PAGADO' ESLE 'PENDIENTE DE PAGO' END) AS estado, "
//					+ " (SELECT SUM(monto) FROM pagos p WHERE p.codcompra = c.codcompra) AS pagado "
					+ " FROM compras c ";
			sql+=" WHERE 1 = 1 ";
			if(proveedor!=null){
				sql+=" AND codproveedor =  "+proveedor.getCodProveedor()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(pagados && !pendientes && !anulados){
				sql+=" AND estado =  'P' ";
			}else if(!pagados && pendientes && !anulados){
				sql+=" AND estado =  'N' ";
			}else if(!pagados && !pendientes && anulados){
				sql+=" AND estado =  'I' ";
			}else if(pagados && pendientes && !anulados){
				sql+=" AND (estado = 'P' OR estado = 'N') ";
			}else if(pagados && !pendientes && anulados){
				sql+=" AND (estado =  'P' OR estado = 'I') ";
			}else if(!pagados && pendientes && anulados){
				sql+=" AND (estado =  'N' OR estado = 'I') ";
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY codigoCompra DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codigo")) {
				obj = rs.getInt("codigoCompra");
			}
			if (arg0.getName().equals("fecha")) {
				obj = rs.getTimestamp("fechaCompra");
			}
			if (arg0.getName().equals("proveedor")) {
				obj = new ControladorProveedor().getProveedor(conn, rs.getInt("codproveedor")).getRazonSocial();
			}
			
			if (arg0.getName().equals("factura")) {
				obj = rs.getString("facturanro");
			}
			
			if (arg0.getName().equals("sucursal")) {
				obj = new ControladorSucursal().getSucursal(conn, rs.getInt("codsucursal")).getNombre();
			}
			
			if (arg0.getName().equals("total")) {
				obj = rs.getDouble("total");
			}
			
			String estado = rs.getString("estadoCompra");
			if(estado.equals("I")){
				estado = "Anulado";
			}else if(estado.equals("P")){
				estado = "Pagado";
			}else {
				estado = "Pendiente de pago";
			}
			
			if (arg0.getName().equals("estado")) {
				obj = estado;
			}
			if (arg0.getName().equals("totalpagado")) {
				obj = rs.getDouble("pagado");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
