package py.com.panambi.informes;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorCompra;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIReporteCompras extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JComboBoxPanambi jCmbProveedor;
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbProveedor;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private ControladorProveedor controladorProveedor = new ControladorProveedor();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private List<String> proveedores= new ArrayList<String>();
	private List<String> sucursales= new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private JButtonPanambi jBtnGenerarReporte;
	private JCheckBoxPanambi chckbxpnmbTodos;
	private JCheckBoxPanambi chckbxpnmbPagados;
	private JCheckBoxPanambi chckbxpnmbPendientesDePago;
	private JCheckBoxPanambi chckbxpnmbAnulados;
	private JLabelPanambi lblpnmbSucursal;
	private ControladorCompra controladorCompra = new ControladorCompra();
	
	
	public JIReporteCompras() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Reporte de compras");
		setBounds(100,100, 756, 315);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnGenerarReporte());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(getChckbxpnmbTodos(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
							.addGap(22)
							.addComponent(getChckbxpnmbPagados(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addGap(38)
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(88)
							.addComponent(getChckbxpnmbAnulados(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
							.addGap(22)
							.addComponent(getChckbxpnmbPendientesDePago(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(51)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getChckbxpnmbTodos(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getChckbxpnmbPagados(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(3)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(7)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getChckbxpnmbAnulados(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(getChckbxpnmbPendientesDePago(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(183))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setShortcuts(this);
		setHelp("reportecompras");
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			proveedores = null;
			sucursales = null;
			poblarListaProveedores();
			poblarListaSucursales();
			poblarListaEstados();
			getJCmbProveedor().setModel(new ListComboBoxModel<String>(proveedores));
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
//			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
//			poblarListaEstados();
			getChckbxpnmbTodos().setSelected(true);
			getChckbxpnmbPagados().setSelected(false);
			getChckbxpnmbPendientesDePago().setSelected(false);
			getChckbxpnmbAnulados().setSelected(false);
			getJDPFechaDesde().setDate(null);
			
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			getJCmbProveedor().setSelectedItem("TODOS");
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaProveedores() throws Exception {
		proveedores = new ArrayList<String>();
		List<Proveedor> listaProveedor= controladorProveedor.getProveedores(JFramePanambiMain.session.getConn());
		proveedores.add("TODOS");
		for (Proveedor prov : listaProveedor) {
			proveedores.add(prov.getRazonSocial());
		}
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSucursal= controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("ACTIVOS/PAGADOS");
		estados.add("PENDIENTES POR PAGAR");
		estados.add("ANULADOS");
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbProveedor().requestFocus();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JComboBoxPanambi getJCmbProveedor() {
		if (jCmbProveedor == null) {
			jCmbProveedor = new JComboBoxPanambi();
		}
		return jCmbProveedor;
	}
	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProveedor.setText("Proveedor : ");
		}
		return lblpnmbProveedor;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnGenerarReporte() {
		if (jBtnGenerarReporte == null) {
			jBtnGenerarReporte = new JButtonPanambi();
			jBtnGenerarReporte.setMnemonic('G');
			jBtnGenerarReporte.setToolTipText("Generar reporte de compra");
			jBtnGenerarReporte.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarReporte();
				}
			});
			jBtnGenerarReporte.setText("Generar reporte");
		}
		return jBtnGenerarReporte;
	}
	
	@SuppressWarnings("deprecation")
	private void doGenerarReporte(){
		try{
			Proveedor proveedor = null;
			String razonSocial  = "";
			
			Sucursal sucursal = null;
			String nombreSucursal = "";
			boolean pagados = false;
			boolean pendientes = false;
			boolean anulados = false;
			Date fechaDesde ;
			Date fechaHasta ;

			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			//fechaHasta = sumarFechasDias(fechaHasta, 1);
		
			razonSocial = (String)getJCmbProveedor().getSelectedItem();
			
			if(!razonSocial.equals("TODOS")){
				proveedor = controladorProveedor.getProveedor(JFramePanambiMain.session.getConn(), razonSocial);
			}else proveedor = null;
			
			nombreSucursal= (String)getJCmbSucursal().getSelectedItem(); 
			
			if(!nombreSucursal.equals("TODAS")){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}else sucursal = null;
			
				
			if(getChckbxpnmbPagados().isSelected()){
				pagados = true;
			}
			
			if(getChckbxpnmbPendientesDePago().isSelected()){
				pendientes = true;
			}
			
			if(getChckbxpnmbAnulados().isSelected()){
				anulados = true;
			}
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				
				try {
					PanambiUtils.setWaitCursor(getOwner());
					Map<String, Object> parameters = new HashMap<String, Object>();
					javax.swing.ImageIcon icon = new ImageIcon(JIReporteCompras.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
					Image image = icon.getImage();

					if(getJCmbSucursal().getSelectedIndex()!=-1){
						parameters.put("SUCURSAL", getJCmbSucursal().getSelectedItem());
						sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String)getJCmbSucursal().getSelectedItem());
					}else{
						parameters.put("SUCURSAL", "TODAS");
					}
					
					if(getJCmbProveedor().getSelectedIndex()!=-1){
						parameters.put("PROVEEDOR", getJCmbProveedor().getSelectedItem());
					}else{
						parameters.put("PROVEEDOR", "TODOS");
					}
					
					String periodo = "DESDE ";
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					if(getJDPFechaDesde().getDate()!=null){
						periodo+=formato.format(getJDPFechaDesde().getDate());
					}else{
						periodo+="INICIO";
					}
					
					periodo+=" HASTA ";
					
					if(getJDPFechaHasta().getDate()!=null){
						periodo+=formato.format(getJDPFechaHasta().getDate());
					}else{
						periodo+="FINAL.";
					}
					parameters.put("PERIODO", periodo);
					
					String estado = "";
					if(getChckbxpnmbTodos().isSelected()){
						estado = "PAGADOS, PENDIENTES DE PAGO Y ANULADOS";
					}else if(getChckbxpnmbPagados().isSelected() && !getChckbxpnmbPendientesDePago().isSelected() && !getChckbxpnmbAnulados().isSelected()){
						estado = "PAGADOS";
					}else if(!getChckbxpnmbPagados().isSelected() && getChckbxpnmbPendientesDePago().isSelected() && !getChckbxpnmbAnulados().isSelected()){
						estado = "PENDIENTES DE PAGO";
					}else if(!getChckbxpnmbPagados().isSelected() && !getChckbxpnmbPendientesDePago().isSelected() && getChckbxpnmbAnulados().isSelected()){
						estado = "ANULADOS";
					}else if(getChckbxpnmbPagados().isSelected() && getChckbxpnmbPendientesDePago().isSelected() && !getChckbxpnmbAnulados().isSelected()){
						estado = "PAGADOS Y PENDIENTES DE PAGO";
					}else if(getChckbxpnmbPagados().isSelected() && !getChckbxpnmbPendientesDePago().isSelected() && getChckbxpnmbAnulados().isSelected()){
						estado = "PAGADOS Y ANULADOS";
					}else if(!getChckbxpnmbPagados().isSelected() && getChckbxpnmbPendientesDePago().isSelected() && getChckbxpnmbAnulados().isSelected()){
						estado = "PENDIENTES DE PAGO Y ANULADOS";
					}
					parameters.put("ESTADO", estado);
					parameters.put("LOGO", image);
					parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
					
					
					
					
					Double montoFacturado =  controladorCompra.getMontoFacturado(JFramePanambiMain.session.getConn(),proveedor, sucursal, pagados, pendientes, anulados, fechaDesde, fechaHasta);
					parameters.put("TOTALFACTURADO", montoFacturado);
					Double montoPagado = controladorCompra.getMontoPagado(JFramePanambiMain.session.getConn(),proveedor, sucursal, pagados, pendientes, anulados, fechaDesde, fechaHasta);
					parameters.put("TOTALPAGADO", montoPagado);
					URL url = JIConsultarCuotasAtrasadas.class.getResource("/py/com/panambi/informes/reports/JRCompras.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					
					JasperPrint jasperPrint = null;
					
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceCompras(JFramePanambiMain.session.getConn(),proveedor, sucursal, pagados, pendientes, anulados, fechaDesde, fechaHasta));
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}
				} catch (Exception e) {
					DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
				} finally {
					PanambiUtils.setDefaultCursor(getOwner());
				}
				
				
				
//				List<Compra> listaCompra= controladorCompra.getCompras(JFramePanambiMain.session.getConn(), proveedor, sucursal,pagados, pendientes, anulados, fechaDesde, fechaHasta);
//				Iterator<Compra> iteratorCompra= listaCompra.listIterator();
//				while (iteratorCompra.hasNext()) {
//					
//					getTablePanambi().addRow();
//					
//					Compra comp= (Compra) iteratorCompra.next();
//					//"Nro. de factura","Proveedor", "Fecha de compra", "Total","Sucursal","Estado","Data"};
//					getTablePanambi().setValueAt(comp.getFacturanro(), getTablePanambi().getRowCount()-1, 0);
//					getTablePanambi().setValueAt(comp.getProveedor().getRazonSocial(), getTablePanambi().getRowCount()-1, 1);
//					
//					String patron = "dd/MM/yyyy";
//					SimpleDateFormat formato = new SimpleDateFormat(patron);
//					
//					
//					getTablePanambi().setValueAt(formato.format(comp.getFecha()), getTablePanambi().getRowCount()-1, 2);
//					getTablePanambi().setValueAt(comp.getTotal(), getTablePanambi().getRowCount()-1, 3);
//					getTablePanambi().setValueAt(comp.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 4);
//					if(comp.getEstado().equals("P")){
//						getTablePanambi().setValueAt("Pagado", getTablePanambi().getRowCount()-1, 5);
//					}else if(comp.getEstado().equals("N")){
//						getTablePanambi().setValueAt("Pendiente de pago", getTablePanambi().getRowCount()-1, 5);
//					}else if (comp.getEstado().equals("I")){
//						getTablePanambi().setValueAt("Anulado", getTablePanambi().getRowCount()-1, 5);
//					}
//					
//					getTablePanambi().setValueAt(comp, getTablePanambi().getRowCount()-1, 6);
//				}	
			}
			
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	 @SuppressWarnings("unused")
	private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
		
		
	
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
		
	}
	private JCheckBoxPanambi getChckbxpnmbTodos() {
		if (chckbxpnmbTodos == null) {
			chckbxpnmbTodos = new JCheckBoxPanambi();
			chckbxpnmbTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosEstados();
				}
			});
			chckbxpnmbTodos.setText("Todos");
		}
		return chckbxpnmbTodos;
	}
	
	private void doSelectTodosEstados(){
		getChckbxpnmbTodos().setSelected(true);
		getChckbxpnmbPagados().setSelected(false);
		getChckbxpnmbPendientesDePago().setSelected(false);
		getChckbxpnmbAnulados().setSelected(false);
	}
	private JCheckBoxPanambi getChckbxpnmbPagados() {
		if (chckbxpnmbPagados == null) {
			chckbxpnmbPagados = new JCheckBoxPanambi();
			chckbxpnmbPagados.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbPagados.setText("Pagados");
		}
		return chckbxpnmbPagados;
	}
	
	private void doSelectEstados(){
		getChckbxpnmbTodos().setSelected(false);
		
		if(!getChckbxpnmbPagados().isSelected() && !getChckbxpnmbPendientesDePago().isSelected() && !getChckbxpnmbAnulados().isSelected()){
			getChckbxpnmbTodos().setSelected(true);
		}
		if(getChckbxpnmbPagados().isSelected() && getChckbxpnmbPendientesDePago().isSelected() && getChckbxpnmbAnulados().isSelected()){
			getChckbxpnmbTodos().setSelected(true);
			getChckbxpnmbPagados().setSelected(false);
			getChckbxpnmbPendientesDePago().setSelected(false);
			getChckbxpnmbAnulados().setSelected(false);
		}
		
	}
	
	private JCheckBoxPanambi getChckbxpnmbPendientesDePago() {
		if (chckbxpnmbPendientesDePago == null) {
			chckbxpnmbPendientesDePago = new JCheckBoxPanambi();
			chckbxpnmbPendientesDePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbPendientesDePago.setText("Pendientes de pago");
		}
		return chckbxpnmbPendientesDePago;
	}
	private JCheckBoxPanambi getChckbxpnmbAnulados() {
		if (chckbxpnmbAnulados == null) {
			chckbxpnmbAnulados = new JCheckBoxPanambi();
			chckbxpnmbAnulados.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbAnulados.setText("Anulados");
		}
		return chckbxpnmbAnulados;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal : ");
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbSucursal;
	}
}
