package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Venta;
import py.com.panambi.connect.ConnectionManager;

public class JRDataSourceExtractoCuotas implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;

	public JRDataSourceExtractoCuotas(Connection conn, Venta venta ) throws Exception {
		// aqui se prepara y ejecuta el sql
		String sql = "SELECT codplanpago, nrocuota, monto, vencimiento, fechapago , "
				+ "CASE WHEN estado = 'D' THEN 'PENDIENTES' "
				+ "     WHEN estado = 'P' THEN 'PAGADAS' "
				+ "END AS estado "
				+ "FROM planespagos "
				+ "WHERE codventa = "+venta.getCodVenta()
				+ "ORDER BY estado, codplanpago ";
				

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("codplanpago")) {
				obj = rs.getInt("codplanpago");
			}
			if (arg0.getName().equals("nrocuota")) {
				obj = rs.getInt("nrocuota");
			}
			if (arg0.getName().equals("monto")) {
				obj = rs.getDouble("monto");
			}
			if (arg0.getName().equals("vencimiento")) {
				obj = rs.getDate("vencimiento");
			}
			if (arg0.getName().equals("fechapago")) {
				obj = rs.getDate("fechapago");
			}
			if (arg0.getName().equals("estado")) {
				obj = rs.getString("estado");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
