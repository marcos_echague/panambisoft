package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class JRDataSourceAuditoriaGeneralDetalles implements JRDataSource {
	
	ResultSet rs = null;
	Integer cantidadColumnas;
	Integer cantidadRegistros;
	//String tabla;
	List<ResultSet> rsDetalleList = null;
	
	public JRDataSourceAuditoriaGeneralDetalles(Connection conn, ResultSet rs, Integer cantidadColumnas) throws Exception{
		try{
			//this.tabla = tabla;
			this.cantidadColumnas = cantidadColumnas+3;
			this.rs = rs;
			//this.cantidadRegistros = rs.size();
			
		}catch (Exception e ){
			throw e;
		}finally{
			//ConnectionManager.closeStatments(ps);
		}
	}
	
	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("registroCabecera")) {
				obj = rs.getString("cabecera");
			}
			if (arg0.getName().equals("registroDetalle")) {
				obj = rs.getString("detalle");
			}
				
//				for(int i = 1; i<=cantidadColumnas; i++){;
//					String cadenaTemporalCabecera="";
//					try{
//						cadenaTemporalCabecera = rsCabecera.getObject(i).toString().trim(); 
//						
//					}catch(Exception e){
//						cadenaTemporalCabecera = "N/A";
//					}
//					obj += cadenaTemporalCabecera+",   ";
//				}
//				
//				ResultSet rsDetalle = rsDetalleList.get(1);
//				if(arg0.getName().equals("registroDetalle"));{
//					String cadenaTemporalDetalle = "";
//					for(int j = 1 ; j <=3;j++){
//						
//						try{
//							cadenaTemporalDetalle = rsDetalle.getObject(j).toString().trim(); 
//							
//						}catch(Exception e){
//							cadenaTemporalDetalle = "N/A";
//						}
//						obj += cadenaTemporalDetalle+",   ";
//						
//					}
//				}
//				
				
//				PreparedStatement ps = null;
//				ResultSet rsDetalle = null;
//				String sqlDetalle = "";
//				sqlDetalle ="";
//				sqlDetalle = "SELECT au_usuario, au_operacion, au_fecha, ";
//				
//				List<String> columnasDetalles = new ControladorAuditoria().getColumnNames(JFramePanambiMain.session.getConn(), "detalle"+tabla);
//				
//				
//				for (int i = 0;i<columnasDetalles.size();i++){
//					
//					//JOptionPane.showMessageDialog(null, columnasDetalles.get(i));
//					
//					sqlDetalle+=columnasDetalles.get(i)+"";
//					
//					if(i!=columnasDetalles.size()-1){
//						sqlDetalle+=", ";
//					}else sqlDetalle+=" ";
//				}
//				
//				sqlDetalle+=" FROM au_detalle"+tabla;
//				sqlDetalle+=" WHERE codventa  = "+rs.getObject(4);
//				//JOptionPane.showMessageDialog(null, sqlDetalle);
				
			//}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}	
	
}
