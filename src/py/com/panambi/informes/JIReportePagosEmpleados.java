package py.com.panambi.informes;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIReportePagosEmpleados extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JComboBoxPanambi jCmbEstado;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private List<String> tiposPagosEmpleados = new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private List<String> sucursales= new ArrayList<String>();
	private JButtonPanambi jBtnConsultar;
	private ControladorEmpleado controladorEmpleado= new ControladorEmpleado();
	private JLabelPanambi lblpnmbTipoDePago;
	private JComboBoxPanambi jCmbTipoPago;
	private JPanel panel;
	private JTextFieldUpper jTxtNombreApellido;
	private JLabelPanambi lblpnmbEmpleado;
	private JLabelPanambi labelPanambi_1;
	private JTextField jTxtNroCedula;
	private JRadioButton rdbtnTodos;
	private Empleado empleado;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private JPanel panel_1;
	
	public JIReportePagosEmpleados() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Reporte de pago a empleados");
		setBounds(100,100, 810, 394);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnSalir());
		setHelp("consultarAnularPagoEmpleado");
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addGap(51)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 171, Short.MAX_VALUE)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
							.addGap(69))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addGap(20)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 359, GroupLayout.PREFERRED_SIZE)
									.addGap(34)
									.addComponent(getPanel_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
					.addGap(20))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(7)
					.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
						.addComponent(getPanel_1(), GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(3)
									.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(1)
									.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
					.addGap(1))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setHelp("reportepagosempleados");
		setShortcuts(this);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			empleado = null;
			tiposPagosEmpleados = null;
			estados=null;
			poblarListaTiposPagosEmpleados();
			poblarListaEstados();
			poblarListaSucursales();
			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
			getJCmbTipoPago().setModel(new ListComboBoxModel<>(tiposPagosEmpleados) );
			getJCmbSucursal().setModel(new ListComboBoxModel<>(sucursales) );
			getJTxtNroCedula().setText("");
			getJTxtNombreApellido().setText("");
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			getRdbtnTodos().setSelected(true);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaTiposPagosEmpleados() throws Exception {
		tiposPagosEmpleados = new ArrayList<String>();
		tiposPagosEmpleados.add("TODOS");
		tiposPagosEmpleados.add("SALARIOS");
		tiposPagosEmpleados.add("AGUINALDOS");
		tiposPagosEmpleados.add("LIQUIDACIONES");
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("PAGADOS");
		estados.add("NO PAGADOS");
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSucursal= controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('G');
			jBtnConsultar.setToolTipText("Generar reporte de pagos a empleados");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doReportePagoEmpleados();
				}
			});
			jBtnConsultar.setText("Generar reporte");
		}
		return jBtnConsultar;
	}
	
	
	@SuppressWarnings("deprecation")
	private void doReportePagoEmpleados(){
	
		try{
			String tipoPagoEmpleado  = "";
			String estado ;
			Date fechaDesde ;
			Date fechaHasta ;
			Sucursal sucursal = null;
			String nombreSucursal = "";
			
			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			estado = (String)getJCmbEstado().getSelectedItem();

			if(estado.equals("TODOS")){
				estado = null;
			}else if(estado.equals("PAGADOS")){
				estado = "P";
			}else{
				estado = "NP";
			}
			
			nombreSucursal= (String)getJCmbSucursal().getSelectedItem(); 
			
			if(!nombreSucursal.equals("TODAS")){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}else sucursal = null;
			
			tipoPagoEmpleado = (String)getJCmbTipoPago().getSelectedItem();
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
			}
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				try {
					PanambiUtils.setWaitCursor(getOwner());
					Map<String, Object> parameters = new HashMap<String, Object>();
					javax.swing.ImageIcon icon = new ImageIcon(JIReporteGastos.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
					Image image = icon.getImage();
	
					if(getJCmbSucursal().getSelectedIndex()!=-1){
						parameters.put("SUCURSAL", getJCmbSucursal().getSelectedItem());
						sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String)getJCmbSucursal().getSelectedItem());
					}else{
						parameters.put("SUCURSAL", "TODAS");
					}
					
					String periodo = "DESDE ";
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					if(getJDPFechaDesde().getDate()!=null){
						periodo+=formato.format(getJDPFechaDesde().getDate());
					}else{
						periodo+="INICIO";
					}
					
					periodo+=" HASTA ";
					
					if(getJDPFechaHasta().getDate()!=null){
						periodo+=formato.format(getJDPFechaHasta().getDate());
					}else{
						periodo+="FINAL.";
					}
					parameters.put("PERIODO", periodo);
					
					if(tipoPagoEmpleado.equals("TODOS")){
						parameters.put("TIPOPAGOEMPLEADO", "SALARIO, AGUINALDO Y LIQUIDACION");
					}else{
						parameters.put("TIPOPAGOEMPLEADO", tipoPagoEmpleado);
					}
					
					if(empleado != null){
						parameters.put("EMPLEADO", empleado.getNombre()+" "+empleado.getApellido());
					}else{
						parameters.put("EMPLEADO", "TODOS");
					}	
					
					
					if(tipoPagoEmpleado.equals("TODOS")){
						tipoPagoEmpleado = null;
					}else if (tipoPagoEmpleado.equals("SALARIOS")){
						tipoPagoEmpleado = "S";
					}else if (tipoPagoEmpleado.equals("AGUINALDOS")){
						tipoPagoEmpleado = "A";
					}else if (tipoPagoEmpleado.equals("LIQUIDACIONES")){
						tipoPagoEmpleado = "L";
					}
					
					parameters.put("ESTADO", (String)getJCmbEstado().getSelectedItem());
					parameters.put("LOGO", image);
					parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
					
					parameters.put("TIPOPAGOEMPLEADO", (String)getJCmbTipoPago().getSelectedItem());
					
	//				if(tipoPagoEmpleado!=null){
	//					if(tipoPagoEmpleado.equals("S")){
	//						parameters.put("TIPOPAGOEMPLEADO", "PAGO DE SALARIOS");
	//					}else if (tipoPagoEmpleado.equals("L")){
	//					
	//					}else {
	//						
	//					}
	//				}	
					
	//				Double montoFacturado =  controladorCompra.getMontoFacturado(JFramePanambiMain.session.getConn(),tipoGasto, sucursal, pagados, pendientes, anulados, fechaDesde, fechaHasta);
	//				parameters.put("TOTALFACTURADO", montoFacturado);
	//				Double montoPagado = controladorCompra.getMontoPagado(JFramePanambiMain.session.getConn(),tipoGasto, sucursal, pagados, pendientes, anulados, fechaDesde, fechaHasta);
	//				parameters.put("TOTALPAGADO", montoPagado);
					URL url = JIConsultarCuotasAtrasadas.class.getResource("/py/com/panambi/informes/reports/JRPagosEmpleados.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					
					JasperPrint jasperPrint = null;
					
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagosEmpleados(JFramePanambiMain.session.getConn(),empleado , tipoPagoEmpleado, sucursal, estado, fechaDesde, fechaHasta));
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}
			}catch (Exception e) {
				DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
			} finally {
				PanambiUtils.setDefaultCursor(getOwner());
			}
		}
		
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
		
	 private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
		
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
		}
		return jCmbEstado;
		
	}
	private JLabelPanambi getLblpnmbTipoDePago() {
		if (lblpnmbTipoDePago == null) {
			lblpnmbTipoDePago = new JLabelPanambi();
			lblpnmbTipoDePago.setText("Tipo de pago a empleado : ");
			lblpnmbTipoDePago.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbTipoDePago;
	}
	private JComboBoxPanambi getJCmbTipoPago() {
		if (jCmbTipoPago == null) {
			jCmbTipoPago = new JComboBoxPanambi();
		}
		return jCmbTipoPago;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Informacion del empleado");
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(10)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
								.addGap(9)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
								.addGap(24)
								.addComponent(getRdbtnTodos(), GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(20)
								.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 302, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(74, Short.MAX_VALUE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getRdbtnTodos())
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setToolTipText("Nombre y apellido del empleado");
			jTxtNombreApellido.setText("");
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setFocusable(false);
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return jTxtNombreApellido;
	}
	private JLabelPanambi getLblpnmbEmpleado() {
		if (lblpnmbEmpleado == null) {
			lblpnmbEmpleado = new JLabelPanambi();
			lblpnmbEmpleado.setText("Empleado");
			lblpnmbEmpleado.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbEmpleado.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbEmpleado;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("Nro. Cedula : ");
			labelPanambi_1.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return labelPanambi_1;
	}
	private JTextField getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextField();
			jTxtNroCedula.setText("");
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtNroCedula.setColumns(10);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroCedula();
				}
			});

		}
		return jTxtNroCedula;
	}
	
private void lostFocusNroCedula(){
		
		try{
			Integer longitud = 0;
			try{
				longitud = getJTxtNroCedula().getText().length();
			}catch(Exception e){
				longitud = 0;
			}
			if(longitud==0){
				empleado = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Empleado emp =  controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), getJTxtNroCedula().getText());
				
				if(emp != null){
					setValues(emp, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					doBuscarEmpleados();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doBuscarEmpleados(){
			try {
				getJTxtNroCedula().setText("");
				getJTxtNombreApellido().setText("");
				String[] columnNames = { "C�digo", "Nro Documento", "Nombre","Apellido", "Cargo","Sucursal","Estado"};
				String[] pks = { "codEmpleado" };
				String sSelect = "SELECT codempleado, nrodocumento, nombres, apellidos, ";
				sSelect += "(SELECT descripcion FROM cargosempleados WHERE empleados.codcargoempleado = cargosempleados.codcargoempleado),  ";
				sSelect += "(SELECT nombre FROM sucursales WHERE empleados.codsucursal= sucursales.codsucursal), ";
				sSelect += " (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
				sSelect += "FROM empleados ";
				sSelect += "ORDER BY nombres";
				JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Empleado", pks);
				jb.setVisible(true);
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
	}

	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosEmpleados();
				}
			});
			rdbtnTodos.setToolTipText("Todos los empleados");
			rdbtnTodos.setSelected(true);
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodosEmpleados(){
		getRdbtnTodos().setSelected(true);
		empleado = null;
		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText(null);
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Empleado) {
			this.empleado= (Empleado)obj;
			getRdbtnTodos().setSelected(false);
			getJTxtNombreApellido().setText(empleado.getNombre()+" "+empleado.getApellido());
			getJTxtNroCedula().setText(empleado.getNroDocumento());
			
		}
	}
	
	public void setNoValues(Object obj, Integer source) {
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(46)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(79)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJCmbEstado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addComponent(getLblpnmbTipoDePago(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJCmbTipoPago(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGap(29))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(11)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(11)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(11)
								.addComponent(getLblpnmbTipoDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addComponent(getJCmbTipoPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
			);
			panel_1.setLayout(gl_panel_1);
		}
		return panel_1;
	}
}
