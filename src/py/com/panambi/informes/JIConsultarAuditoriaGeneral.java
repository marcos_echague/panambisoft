package py.com.panambi.informes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.JXDatePicker;

import py.com.panambi.controller.ControladorAuditoria;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIConsultarAuditoriaGeneral extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3449615672767290822L;
	private JButtonPanambi jBtnConsultar;
	private JXDatePicker jDatePickerFechaDesde;
	private JXDatePicker jDatePickerFechaHasta;
	private JLabelPanambi lblpnmbFechaDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private JComboBoxPanambi jCmbTablaAudit;
	private JLabelPanambi lblpnmbAuditoriaDe;
	private JCheckBoxPanambi jChInserciones;
	private JCheckBoxPanambi jChModificaciones;
	private JCheckBoxPanambi jChEliminaciones;
	private JCheckBoxPanambi jChTodas;
	private JLabelPanambi lblpnmbOperacion;
	private ControladorAuditoria controladorAuditoria = new ControladorAuditoria();
	private JLabelPanambi lblpnmbPresioneFPara;
	private JButtonPanambi btnpnmbLimpiar;
	public JIConsultarAuditoriaGeneral() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Reporte de auditoria general");
		setMaximizable(false);
		setBounds(100, 100, 686, 328);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(532)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addComponent(getLblpnmbAuditoriaDe(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJCmbTablaAudit(), GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)
					.addGap(32)
					.addComponent(getLblpnmbOperacion(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(getJChTodas(), GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
					.addGap(15)
					.addComponent(getJChInserciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(443)
					.addComponent(getJChModificaciones(), GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(getJChEliminaciones(), GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addGap(8)
					.addComponent(getJDatePickerFechaDesde(), GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
					.addGap(111)
					.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJDatePickerFechaHasta(), GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(6)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(7)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbAuditoriaDe(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getJCmbTablaAudit(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbOperacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJChTodas(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJChInserciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addGap(2)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJChModificaciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJChEliminaciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getJDatePickerFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJDatePickerFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setShortcuts(this);
		setHelp("auditoriageneral");
		getJCmbTablaAudit().requestFocus();
	}

	
	private void doLimpiar(){
		
		getJChTodas().setSelected(true);
		getJChInserciones().setSelected(false);
		getJChModificaciones().setSelected(false);
		getJChEliminaciones().setSelected(false);
		
		
		
		getJCmbTablaAudit().setSelectedItem("PRODUCTOS");
		
	}
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Generar reporte de auditoria");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	
	private void doConsultar() {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultaProductos.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("ELEMENTOAUDITADO", (String)getJCmbTablaAudit().getSelectedItem());//getJCmbTablaAudit().getSelectedItem()+"");
			
			String tabla = "";
			
			tabla = getNombreTabla(getJCmbTablaAudit().getSelectedItem().toString().toLowerCase());
			
			String operaciones = "";
			
			boolean insercion = false;
			boolean modificacion = false;
			boolean eliminacion = false;
			
			if(getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
				operaciones = "INSERCIONES ";
				insercion = true;
			}else if(!getJChInserciones().isSelected() && getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
				operaciones  = "MODIFICACIONES";
				modificacion=true;
			}else if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
				operaciones = "ELIMINACIONES";
				eliminacion = true;
			}else if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
				operaciones = "INSERCIONES Y MODIFICACIONES";
				insercion = true;
				modificacion=true;
			}else if(getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
				operaciones = "INSERCIONES Y ELIMINACIONES";
				insercion=true;
				eliminacion = true;
			}else if(!getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
				operaciones = "MODIFICACIONES Y ELIMINACIONES";
				modificacion=true;
				eliminacion=true;
				
			}
			
			if(getJChTodas().isSelected()){
				operaciones = "INSERCIONES, MODIFICACIONES Y ELIMNACIONES";
				insercion=true;
				modificacion=true;
				eliminacion=true;
			}
			
			parameters.put("OPERACIONES", operaciones);
			
			if(getJDatePickerFechaDesde().getDate()!=null){
				parameters.put("FECHAINICIO", getJDatePickerFechaDesde().getDate());
			}else{
				parameters.put("FECHAINICIO", "PRINCIPIO DE OPERACIONES");
			}
			
			if(getJDatePickerFechaHasta().getDate()!=null){
				parameters.put("FECHAFIN", getJDatePickerFechaHasta().getDate());
			}else{
				parameters.put("FECHAFIN", "FINAL DE OPERACIONES");
			}
			
			
			java.sql.Date fechaDesde = null;
			java.sql.Date fechaHasta = null;		
			
			//**Obteniendo datos de fechas.
			try{
				fechaDesde = new java.sql.Date(getJDatePickerFechaDesde().getDate().getTime());
			}catch(Exception e ){
				fechaDesde = null;
			}
			
			try{
				fechaHasta = new java.sql.Date (getJDatePickerFechaHasta().getDate().getTime());
			}catch(Exception e){
				fechaHasta =null;
			}
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			
			String columnasAuditadas="USUARIO, OPERACION, FECHA, ";
			List<String> columNames = controladorAuditoria.getColumnNames(JFramePanambiMain.session.getConn(), tabla);
			for(int i = 0 ; i < columNames.size(); i++){
				columnasAuditadas+=columNames.get(i)+", ";
			}
			
			parameters.put("DATOSAUDITADOS",columnasAuditadas);
			URL url = JIConsultaProductos.class.getResource("/py/com/panambi/informes/reports/JRAuditoriaGeneral.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
				getJDatePickerFechaDesde().requestFocus();
			}else{
				ResultSet resultSet = new ControladorAuditoria().getResultSetAudit(JFramePanambiMain.session.getConn(), tabla, insercion, modificacion, eliminacion, fechaDesde, fechaHasta);
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceAuditoriaGeneral(JFramePanambiMain.session.getConn(),resultSet,columNames.size()));
				
				List<?> pages = jasperPrint.getPages();
				if (!pages.isEmpty()) {
					JRPdfExporter exporter = new JRPdfExporter();
					File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.exportReport();
					if (Desktop.isDesktopSupported()) {
						Desktop.getDesktop().open(filetmp);
					}
					resultSet.close();
					
				} else {
					DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
				}
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
	}
	
	private String getNombreTabla(String seleccion){
		String tabla = "";
		
		switch (seleccion.toLowerCase()) {
		case "cargos de empleados" : tabla = "cargosempleados";
			break;
		case "tipos de gastos" : tabla = "tiposgastos";
			break;
		case "formas de pago" : tabla = "formaspagos";
			break;
		case "pagos de empleados" : tabla = "pagosempleados";
			break;	
		case "descuentos salariales" : tabla = "descuentossalarios";
			break;
		case "tipos de descuentos salariales" : tabla = "tiposdescuentossalarios";
			break;
		case "movimiento de productos" : tabla = "movimientosproductos";
			break;
		case "planes de pago" : tabla = "planespagos";
			break;
		case "productos dados de baja" : tabla = "productosperdidos";
			break;
		case "stock de productos" : tabla = "stockproductos";
			break;
		case "asignacion de perfiles a usuarios": tabla = "usuarios_perfiles";
			break;
		case "detalle de compras": tabla = "detallecompras";
			break;
		case "detalle de ventas": tabla = "detalleventas";
			break;
		case "detalle de devoluciones": tabla = "detalledevoluciones";
			break;
		case "detalle de movimientos": tabla = "detallemovimientos";
			break;
		case "detalle de pagos": tabla = "detallepagos";
			break;
		case "detalle de pagos de empleados": tabla = "detallepagosempleados";
			break;
		case "imagenes de productos": tabla = "imagenesproductos";
			break;
		case "relacion de pagos con planes de pago": tabla = "pagosplanespagos";
			break;
		case "relacion de perfiles con programas": tabla = "perfiles_programas";
			break;
		case "relacion usuarios con perfiles": tabla = "usuarios_perfiles";
			break;
		case "deudas de empleados": tabla = "deudasempleado";
			break;
		case "parametros del sistema": tabla = "parametros";
			break;
		default: tabla = seleccion.toLowerCase();
			break;
		}
		return tabla;
	}
	
	
	private JXDatePicker getJDatePickerFechaDesde() {
		if (jDatePickerFechaDesde == null) {
			jDatePickerFechaDesde = new JXDatePicker();
			jDatePickerFechaDesde.setToolTipText("Fecha de inicio de auditoria");
			jDatePickerFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDatePickerFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDatePickerFechaDesde;
	}
	private JXDatePicker getJDatePickerFechaHasta() {
		if (jDatePickerFechaHasta == null) {
			jDatePickerFechaHasta = new JXDatePicker();
			jDatePickerFechaHasta.setToolTipText("Fecha tope de auditoria");
			jDatePickerFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDatePickerFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDatePickerFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechaDesde() {
		if (lblpnmbFechaDesde == null) {
			lblpnmbFechaDesde = new JLabelPanambi();
			lblpnmbFechaDesde.setText("Fecha desde :");
		}
		return lblpnmbFechaDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta :");
		}
		return lblpnmbFechaHasta;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBoxPanambi getJCmbTablaAudit() {
		if (jCmbTablaAudit == null) {
			jCmbTablaAudit = new JComboBoxPanambi();
			jCmbTablaAudit.setToolTipText("Elemento a auditar");
			jCmbTablaAudit.setModel(new DefaultComboBoxModel(new String[] {"PRODUCTOS", "COMPRAS","VENTAS","CLIENTES", "EMPLEADOS","CARGOS DE EMPLEADOS","USUARIOS","PERFILES","SUCURSALES","PROVEEDORES",
					"TEMPORADAS","GASTOS","TIPOS DE GASTOS","DEVOLUCIONES","PAGOS","BONIFICACIONES","FORMAS DE PAGO","PAGOS DE EMPLEADOS","BANCOS","DESCUENTOS SALARIALES","TIPOS DE DESCUENTOS SALARIALES",
					"MOVIMIENTO DE PRODUCTOS","PLANES DE PAGO","PRODUCTOS DADOS DE BAJA","STOCK DE PRODUCTOS","ASIGNACION DE PERFILES A USUARIOS","DEUDAS DE EMPLEADOS",
							"DETALLE DE COMPRAS","DETALLE DE VENTAS","DETALLE DE DEVOLUCIONES","DETALLE DE MOVIMIENTOS","DETALLE DE PAGOS","DETALLE DE PAGOS DE EMPLEADOS","IMAGENES DE PRODUCTOS",
							"RELACION DE PAGOS CON PLANES DE PAGO","RELACION DE PERFILES CON PROGRAMAS","RELACION USUARIOS CON PERFILES","PARAMETROS DEL SISTEMA"}));
			jCmbTablaAudit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					//
				}
			});
			jCmbTablaAudit.setAlignmentY(Component.CENTER_ALIGNMENT);
			jCmbTablaAudit.setAlignmentX(Component.CENTER_ALIGNMENT);
		}
		return jCmbTablaAudit;
	}
	private JLabelPanambi getLblpnmbAuditoriaDe() {
		if (lblpnmbAuditoriaDe == null) {
			lblpnmbAuditoriaDe = new JLabelPanambi();
			lblpnmbAuditoriaDe.setText("Auditoria de :");
		}
		return lblpnmbAuditoriaDe;
	}
	private JCheckBoxPanambi getJChInserciones() {
		if (jChInserciones == null) {
			jChInserciones = new JCheckBoxPanambi();
			jChInserciones.setToolTipText("Inserciones");
			jChInserciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectInserciones();
				}
			});
			jChInserciones.setText("Insercionces");
		}
		return jChInserciones;
	}
	
	private void doSelectInserciones(){
		if(getJChInserciones().isSelected()){
			getJChInserciones().setSelected(true);
			getJChTodas().setSelected(false);
		}else{
			getJChInserciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChModificaciones() {
		if (jChModificaciones == null) {
			jChModificaciones = new JCheckBoxPanambi();
			jChModificaciones.setToolTipText("Modificaciones");
			jChModificaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectModificaciones();
				}
			});
			jChModificaciones.setText("Modificaciones");
		}
		return jChModificaciones;
	}
	
	private void doSelectModificaciones(){
		if(getJChModificaciones().isSelected()){
			getJChModificaciones().setSelected(true);
			getJChTodas().setSelected(false);
			
		}else{
			getJChModificaciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChEliminaciones() {
		if (jChEliminaciones == null) {
			jChEliminaciones = new JCheckBoxPanambi();
			jChEliminaciones.setToolTipText("Eliminaciones");
			jChEliminaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEliminaciones();
				}
			});
			jChEliminaciones.setText("Eliminaciones");
		}
		return jChEliminaciones;
	}
	
	private void doSelectEliminaciones(){
		if(getJChEliminaciones().isSelected()){
			getJChEliminaciones().setSelected(true);
			getJChTodas().setSelected(false);
		}else{
			getJChEliminaciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChTodas() {
		if (jChTodas == null) {
			jChTodas = new JCheckBoxPanambi();
			jChTodas.setToolTipText("Todas las operaciones");
			jChTodas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodas();
				}
			});
			jChTodas.setText("Todas");
		}
		return jChTodas;
	}
	
	private void doSelectTodas(){
		if(getJChTodas().isSelected()){
			//JOptionPane.showMessageDialog(null, "no esta seleccionado");
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false); 
		}else{
			
			//JOptionPane.showMessageDialog(null, "esta seleccionado");
			getJChTodas().setSelected(false);
			getJChInserciones().setSelected(true);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
		
	}
	
	private JLabelPanambi getLblpnmbOperacion() {
		if (lblpnmbOperacion == null) {
			lblpnmbOperacion = new JLabelPanambi();
			lblpnmbOperacion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbOperacion.setText("Operacion : ");
		}
		return lblpnmbOperacion;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJCmbTablaAudit().requestFocus();
				}
			});
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
}
