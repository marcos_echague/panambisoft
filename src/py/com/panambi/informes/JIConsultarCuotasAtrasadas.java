package py.com.panambi.informes;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIConsultarCuotasAtrasadas extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9022396874235454369L;
	private JLabelPanambi lblpnmbDiasDeAtraso;
	private JComboBoxPanambi jCmbAtraso;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;
	private JButtonPanambi btnConsultar;
	private Sucursal sucursal ;
	private List<String> sucursales = new ArrayList<String>();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private final Integer ultimoIndice = 6;
	
	private final Integer selOpcion5 = 0;
	private final Integer selOpcion90 = 5;
	private final Integer selOpcionMay90 = 6;
	private JRadioButton jRdbtnTodasSucursales;
	private JRadioButton jRdbtnTodosAtrasos;
	
	
	public JIConsultarCuotasAtrasadas() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar cuotas retrasadas");
		setBounds(100, 100, 380, 300);
		getJPanelSouth().add(getBtnConsultar());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(getLblpnmbDiasDeAtraso(), GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbAtraso(), GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(getJRdbtnTodosAtrasos(), GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(getJRdbtnTodasSucursales(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addContainerGap(51, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJRdbtnTodasSucursales())))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbDiasDeAtraso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbAtraso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJRdbtnTodosAtrasos()))))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setShortcuts(this);
		setHelp("cuotasatrasadas");
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			sucursal = null;
			
			poblarListaSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<>(sucursales));
			//getJRdbtnTodasSucursales().setSelected(true);
			
			poblarListaDiasAtraso();
			getJCmbAtraso().setSelectedItem(null);
			getJRdbtnTodosAtrasos().setSelected(true);
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
				getJRdbtnTodasSucursales().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
				getJRdbtnTodasSucursales().setEnabled(false);
			}
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales= new ArrayList<String>();
		List<Sucursal> listasuc = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());

		for (Sucursal suc : listasuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	@SuppressWarnings("unchecked")
	private void poblarListaDiasAtraso(){
		getJCmbAtraso().addItem(5);
		getJCmbAtraso().addItem(15);
		getJCmbAtraso().addItem(30);
		getJCmbAtraso().addItem(45);
		getJCmbAtraso().addItem(60);
		getJCmbAtraso().addItem(90);
		getJCmbAtraso().addItem("MAYOR A 90");
	}
	
	
	private JLabelPanambi getLblpnmbDiasDeAtraso() {
		if (lblpnmbDiasDeAtraso == null) {
			lblpnmbDiasDeAtraso = new JLabelPanambi();
			lblpnmbDiasDeAtraso.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDiasDeAtraso.setText("Dias de atraso");
		}
		return lblpnmbDiasDeAtraso;
	}
	private JComboBoxPanambi getJCmbAtraso() {
		if (jCmbAtraso == null) {
			jCmbAtraso = new JComboBoxPanambi();
			jCmbAtraso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionDiasAtraso();
				}
			});
		}
		return jCmbAtraso;
	}
	
	private void doSeleccionDiasAtraso(){
		try {
			getJRdbtnTodosAtrasos().setSelected(false);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal ");
		}
		return lblpnmbSucursal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
			jCmbSucursal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionSucursal();
				}
			});
		}
		return jCmbSucursal;
	}
	
	private void doSeleccionSucursal(){
		try {
			sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String) getJCmbSucursal().getSelectedItem());
			getJRdbtnTodasSucursales().setSelected(false);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JButtonPanambi getBtnConsultar() {
		if (btnConsultar == null) {
			btnConsultar = new JButtonPanambi();
			btnConsultar.setMnemonic('C');
			btnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			btnConsultar.setText("Consultar");
		}
		return btnConsultar;
	}
	
	private void doConsultar(){
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultaProductos.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();

			if(getJCmbSucursal().getSelectedIndex()!=-1){
				parameters.put("SUCURSAL", getJCmbSucursal().getSelectedItem());
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String)getJCmbSucursal().getSelectedItem());
			}else{
				parameters.put("SUCURSAL", "Todas");
				sucursal = null;
			}
			if(getJCmbAtraso().getSelectedIndex()==-1){
				
				parameters.put("DIASDEATRASO", "Todos los atrasos");
				
			}else if(getJCmbAtraso().getSelectedIndex()!=ultimoIndice){
				parameters.put("DIASDEATRASO", "Hasta "+getJCmbAtraso().getSelectedItem()+" d�as");
			}else{
				parameters.put("DIASDEATRASO", getJCmbAtraso().getSelectedItem()+" d�as");
			}
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			URL url = JIConsultarCuotasAtrasadas.class.getResource("/py/com/panambi/informes/reports/JRCuotasAtrasadas.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JFramePanambiMain.session.getConn());
			
			JasperPrint jasperPrint = null;
			Integer codSucursal = null;
			if(sucursal != null){
				codSucursal = sucursal.getCodSucursal();
			}
			
			if(getJCmbAtraso().getSelectedIndex()==-1){
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceCuotasAtrasadas(JFramePanambiMain.session.getConn(),-1,codSucursal));
			} else if(getJCmbAtraso().getSelectedIndex()>= selOpcion5 && getJCmbAtraso().getSelectedIndex() <= selOpcion90 ){
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceCuotasAtrasadas(JFramePanambiMain.session.getConn(),(Integer)getJCmbAtraso().getSelectedItem(),codSucursal));
			}else if(getJCmbAtraso().getSelectedIndex() == selOpcionMay90){
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceCuotasAtrasadas(JFramePanambiMain.session.getConn(),codSucursal));
			}
			
			List<?> pages = jasperPrint.getPages();
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
	}
	
	private void doTodosAtrasos(){
		getJCmbAtraso().setSelectedItem(null);
		getJRdbtnTodosAtrasos().setSelected(true);
	}
	
	private void doTodasSucursales(){
		getJCmbSucursal().setSelectedItem(null);
		getJRdbtnTodasSucursales().setSelected(true);
	}
	private JRadioButton getJRdbtnTodasSucursales() {
		if (jRdbtnTodasSucursales == null) {
			jRdbtnTodasSucursales = new JRadioButton("Todas");
			jRdbtnTodasSucursales.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTodasSucursales();
				}
			});
		}
		return jRdbtnTodasSucursales;
	}
	private JRadioButton getJRdbtnTodosAtrasos() {
		if (jRdbtnTodosAtrasos == null) {
			jRdbtnTodosAtrasos = new JRadioButton("Todos");
			jRdbtnTodosAtrasos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doTodosAtrasos();
				}
			});
		}
		return jRdbtnTodosAtrasos;
	}
}
