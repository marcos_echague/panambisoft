package py.com.panambi.informes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.main.JFramePanambiMain;

public class JRDataSourceVentasPorVendedor implements JRDataSource {
	ResultSet rs = null;
	PreparedStatement ps = null;
	Connection conn = null;

	public JRDataSourceVentasPorVendedor(Connection conn, Cliente cliente, Sucursal sucursal, String estado, String tipoVenta, Date fechaDesde, Date fechaHasta ) throws Exception {
		this.conn = conn;
		String sql = "";
		
			sql = "SELECT v.codventa AS codigoVenta,nrocomprobante, v.fecha AS fechaVenta, codcliente, codsucursal, tipoventa, total, v.estado AS estadoVenta, codusuario, "
					+ "(SELECT SUM(p.monto) FROM pagos p WHERE p.codventa = v.codventa AND p.estado = 'A' ) AS recaudado "
//					+ " (SELECT p.razonsocial FROM proveedores p WHERE c.codproveedor = p.codproveedor) AS proveedor, "
//					+ " c.facturanro, "
//					+ " (SELECT s.nombre FROM sucursales s WHERE c.codsucursal = s.codsucursal) AS sucursal, "
//					+ " c.total, "; 
//					+ "(SELECT CASE WHEN  c.estado = 'I' THEN 'ANULADO' WHEN c.estado = 'P' THEN 'PAGADO' ESLE 'PENDIENTE DE PAGO' END) AS estado, "
//					+ " (SELECT SUM(monto) FROM pagos p WHERE p.codcompra = c.codcompra) AS pagado "
					+ " FROM ventas v ";
			sql+=" WHERE 1 = 1 ";
			if(cliente!=null){
				sql+=" AND codcliente =  "+cliente.getCodCliente()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(tipoVenta!=null){
				if(tipoVenta.equals("C")){
					sql+=" AND tipoventa =  'C' ";
				}else if (tipoVenta.equals("D")){
					sql+=" AND tipoventa =  'D' ";
				}
			}
			
			if(estado!=null){
				if(estado.equals("A")){
					sql+=" AND estado =  'A' ";
				}else if (estado.equals("I")){
					sql+=" AND estado =  'I' ";
				}
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY codigoVenta DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			
	}

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object obj = null;
		try {
			if (arg0.getName().equals("vendedor")) {
				Usuario usuario = new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));	
				obj = usuario.getUsuario();
			}
			
			if (arg0.getName().equals("factura")) {
				obj = rs.getString("nrocomprobante");
			}
			if (arg0.getName().equals("fecha")) {
				obj = rs.getTimestamp("fechaVenta");
			}
			if (arg0.getName().equals("cliente")) {
				obj = new ControladorCliente().getCliente(conn, rs.getInt("codcliente")).getNombres()+" "+new ControladorCliente().getCliente(conn, rs.getInt("codcliente")).getApellidos();
			}
			
			if (arg0.getName().equals("sucursal")) {
				obj = new ControladorSucursal().getSucursal(conn, rs.getInt("codsucursal")).getNombre();
			}
			
			if (arg0.getName().equals("total")) {
				obj = rs.getDouble("total");
			}
			
			String estado = rs.getString("estadoVenta");
			if(estado.equals("I")){
				estado = "Anulado";
			}else if(estado.equals("A")){
				estado = "Activo";
			}
			
			String tipoventa = rs.getString("tipoventa");
			if(tipoventa.equals("C")){
				tipoventa = "Cr�dito";
			}else {
				tipoventa= "Contado";
			}
			
			if (arg0.getName().equals("tipoventa")) {
				obj = tipoventa;
			}
			
			if (arg0.getName().equals("estado")) {
				obj = estado;
			}
			if (arg0.getName().equals("totalpagado")) {
				obj = rs.getDouble("recaudado");
			}
			
		} catch (Exception e) {
			throw new JRException(e);
		}
		return obj;
	}

	@Override
	public boolean next() throws JRException {
		boolean ret = false;
		try {
			ret = rs.next();
			if (!ret) {
				ConnectionManager.closeStatments(ps);
			}
		} catch (Exception e) {
			throw new JRException(e);
		}
		return ret;
	}

}
