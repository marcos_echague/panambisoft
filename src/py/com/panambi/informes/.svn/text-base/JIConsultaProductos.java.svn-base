package py.com.panambi.informes;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.utils.PanambiUtils;

//import com.toedter.calendar.JDateChooser;

public class JIConsultaProductos extends JInternalFramePanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6138458345510452835L;
	private JButtonPanambi btnpnmbConsultar;
	private JComboBoxPanambi jCmbSucursal;
	private JRadioButton rdbtnTodos;
	private Sucursal sucursal;
	private List<String> sucursales = new ArrayList<String>();
	private boolean stockMinimo;
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private JPanel panel;
	private JCheckBox chckbxCantidadInferior;

	/**
	 * Create the frame.
	 */
	public JIConsultaProductos() throws Exception{

		initialize();
	}

	private void initialize() {
		setTitle("Reporte de productos");
		setMaximizable(false);
		setBounds(100, 100, 401, 306);
		getJPanelSouth().add(getBtnpnmbConsultar());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
						.addComponent(getChckbxCantidadInferior()))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(158, Short.MAX_VALUE)
					.addComponent(getChckbxCantidadInferior())
					.addGap(18)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addGap(25))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setHelp("listarproductos");
		setShortcuts(this);
	}

	private JButtonPanambi getBtnpnmbConsultar() {
		if (btnpnmbConsultar == null) {
			btnpnmbConsultar = new JButtonPanambi();
			btnpnmbConsultar.setMnemonic('C');
			btnpnmbConsultar.setToolTipText("Gerera el reporte");
			btnpnmbConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			btnpnmbConsultar.setText("Consultar");
		}
		return btnpnmbConsultar;
	}

	@SuppressWarnings({ "unchecked" })
	private void doLimpiar(){
		try{
			sucursal = null;
			poblarLista();

			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursal().setSelectedItem(null);
			getRdbtnTodos().setSelected(true);
			
		} catch (Exception e) {
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarLista() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listasuc = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
		
		for (Sucursal suc : listasuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void doConsultar() {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultaProductos.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			if(getChckbxCantidadInferior().isSelected()){
				stockMinimo = true;
			}else stockMinimo = false;
			if(stockMinimo){
				parameters.put("STOCKMENOR", "PRODUCTOS POR REPONER");
			}else{
				parameters.put("STOCKMENOR", "");
			}
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			URL url = JIConsultaProductos.class.getResource("/py/com/panambi/informes/reports/JRProductos.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JFramePanambiMain.session.getConn());
			
			JasperPrint jasperPrint;
			
			if(getRdbtnTodos().isSelected()){
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceProductos(JFramePanambiMain.session.getConn(), null, stockMinimo));
			}else {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceProductos(JFramePanambiMain.session.getConn(), sucursal.getCodSucursal(),stockMinimo));
			}
			
			List<?> pages = jasperPrint.getPages();
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
			jCmbSucursal.setToolTipText("filtra sucursal");
			jCmbSucursal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionSucursal();
				}
			});
			
		}
		return jCmbSucursal;
	}
	
	private void doSeleccionSucursal(){
		try {
			sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), (String) getJCmbSucursal().getSelectedItem());
			getRdbtnTodos().setSelected(false);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todas");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodos();
				}
			});
			rdbtnTodos.setToolTipText("todas las sucursales");
			rdbtnTodos.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodos(){
		
		getJCmbSucursal().setSelectedItem(null);
		getRdbtnTodos().setSelected(true);
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Sucursales", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
						.addGap(19)
						.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 208, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
						.addComponent(getRdbtnTodos())
						.addContainerGap())
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getRdbtnTodos()))
						.addContainerGap(18, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JCheckBox getChckbxCantidadInferior() {
		if (chckbxCantidadInferior == null) {
			chckbxCantidadInferior = new JCheckBox("Cantidad inferior al stock minimo");
		}
		return chckbxCantidadInferior;
	}
	
//	private void doLimpiarFechas(){
//		getJDCDesde().setDate(null);
//		getJDCHasta().setDate(null);
//	}
}
