/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBox;

import py.com.panambi.swing.font.FontPanambi;

/**
 * @author hugo
 *
 */
public class JCheckBoxPanambi extends JCheckBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5223214187784112889L;

	/**
	 * 
	 */
	public JCheckBoxPanambi() {
		initialize();
	}

	/**
	 * @param icon
	 */
	public JCheckBoxPanambi(Icon icon) {
		super(icon);
		initialize();
	}

	/**
	 * @param text
	 */
	public JCheckBoxPanambi(String text) {
		super(text);
		initialize();
	}

	/**
	 * @param a
	 */
	public JCheckBoxPanambi(Action a) {
		super(a);
		initialize();
	}

	/**
	 * @param icon
	 * @param selected
	 */
	public JCheckBoxPanambi(Icon icon, boolean selected) {
		super(icon, selected);
		initialize();
	}

	/**
	 * @param text
	 * @param selected
	 */
	public JCheckBoxPanambi(String text, boolean selected) {
		super(text, selected);
		initialize();
	}

	/**
	 * @param text
	 * @param icon
	 */
	public JCheckBoxPanambi(String text, Icon icon) {
		super(text, icon);
		initialize();
	}

	/**
	 * @param text
	 * @param icon
	 * @param selected
	 */
	public JCheckBoxPanambi(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		initialize();
	}

	private void initialize() {
		setFont(FontPanambi.getLabelInstance());
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JCheckBoxPanambi.this.transferFocus();
				}
			}
		});
	}
}
