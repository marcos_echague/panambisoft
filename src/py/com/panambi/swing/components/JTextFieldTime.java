/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.sql.Time;
import java.text.Format;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.text.MaskFormatter;

import py.com.panambi.swing.font.FontPanambi;

/**
 *
 */
public class JTextFieldTime extends JFormattedTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4740457748368398139L;
	private Border originalborder = getBorder();

	/**
	 * 
	 */
	public JTextFieldTime() {
		super();
		initialize();
	}

	/**
	 * @param value
	 */
	public JTextFieldTime(Object value) {
		super(value);
		initialize();
	}

	/**
	 * @param format
	 */
	public JTextFieldTime(Format format) {
		super(format);
		initialize();
	}

	/**
	 * @param formatter
	 */
	public JTextFieldTime(AbstractFormatter formatter) {
		super(formatter);
		initialize();
	}

	/**
	 * @param factory
	 */
	public JTextFieldTime(AbstractFormatterFactory factory) {
		super(factory);
		initialize();
	}

	/**
	 * @param factory
	 * @param currentValue
	 */
	public JTextFieldTime(AbstractFormatterFactory factory, Object currentValue) {
		super(factory, currentValue);
		initialize();
	}

	private void initialize() {
		try {
			GregorianCalendar gc = new GregorianCalendar();
			MaskFormatter mask = new MaskFormatter("##:##");
			mask.setValueClass(Time.class);
			mask.setCommitsOnValidEdit(true);
			mask.setAllowsInvalid(false);
			mask.setOverwriteMode(true);
			mask.install(this);
			setText(mask.valueToString(new Time(gc.getTimeInMillis())));
			setPreferredSize(new Dimension(40, getPreferredSize().height));
			setHorizontalAlignment(JTextField.CENTER);
			setFont(FontPanambi.getLabelInstance());
			addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						e.getComponent().transferFocus();
					}
				}
			});
			addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusGained(java.awt.event.FocusEvent e) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							JTextFieldTime.this.selectAll();
						}
					});
				}
			});

			this.setInputVerifier(new ValidadorHora());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class ValidadorHora extends InputVerifier {

		String message = "";

		@Override
		public boolean shouldYieldFocus(JComponent input) {
			boolean ret = verify(input);
			if (!ret) {
				JTextFieldTime.this.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.red, 2));
				JTextFieldTime.this.selectAll();
			} else {
				JTextFieldTime.this.setBorder(originalborder);
			}

			return ret;
		}

		@Override
		public boolean verify(JComponent input) {
			boolean ret = true;
			try {
				String texto = ((JTextField) input).getText();
				StringTokenizer stoke = new StringTokenizer(texto, ":");
				String hh = stoke.nextToken();
				String mm = stoke.nextToken();
				Integer ih = new Integer(hh);
				Integer im = new Integer(mm);
				if (ih < 0 || ih > 24) {
					message = "Hora fuera de rango";
					ret = false;
				}
				if (im < 0 || im > 59) {
					message = "Minuto fuera de rango";
					ret = false;
				}
			} catch (NumberFormatException e) {
				ret = false;
			}
			return ret;
		}
	}

	public Time getHora() {
		Time ret = null;
		String texto = this.getText().trim();
		if (!texto.equals("")) {
			StringTokenizer stoke = new StringTokenizer(texto, ":");
			String hh = stoke.nextToken();
			String mm = stoke.nextToken();
			Integer ih = new Integer(hh);
			Integer im = new Integer(mm);
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(Calendar.HOUR_OF_DAY, ih);
			gc.set(Calendar.MINUTE, im);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			ret = new Time(gc.getTimeInMillis());
		}
		return ret;
	}

}
