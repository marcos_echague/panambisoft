/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

import py.com.panambi.swing.font.FontPanambi;

/**
 * 
 */
public class JButtonPanambi extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5813052686419127446L;

	/**
	 * 
	 */
	public JButtonPanambi() {
		super();
		initialize();
	}

	/**
	 * @param icon
	 */
	public JButtonPanambi(Icon icon) {
		super(icon);
		initialize();
	}

	/**
	 * @param text
	 */
	public JButtonPanambi(String text) {
		super(text);
		initialize();
	}

	/**
	 * @param a
	 */
	public JButtonPanambi(Action a) {
		super(a);
		initialize();
	}

	/**
	 * +
	 * 
	 * @param text
	 * @param icon
	 */
	public JButtonPanambi(String text, Icon icon) {
		super(text, icon);
		initialize();
	}

	private void initialize() {
		this.setFont(FontPanambi.getLabelInstance());
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//					JButtonPanambi.this.doClick();
				}
			}
		});
	}

}
