/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

import py.com.panambi.swing.font.FontPanambi;

/**
 *
 */
public class JTextAreaUpper extends JTextArea implements ClipboardOwner {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3321207221102427309L;

	/**
	 * 
	 */
	public JTextAreaUpper() {
		super();
		initialize();
	}

	/**
	 * @param text
	 */
	public JTextAreaUpper(String text) {
		super(text);
		initialize();
	}

	/**
	 * @param doc
	 */
	public JTextAreaUpper(Document doc) {
		super(doc);
		initialize();
	}

	/**
	 * @param rows
	 * @param columns
	 */
	public JTextAreaUpper(int rows, int columns) {
		super(rows, columns);
		initialize();
	}

	/**
	 * @param text
	 * @param rows
	 * @param columns
	 */
	public JTextAreaUpper(String text, int rows, int columns) {
		super(text, rows, columns);
		initialize();
	}

	/**
	 * @param doc
	 * @param text
	 * @param rows
	 * @param columns
	 */
	public JTextAreaUpper(Document doc, String text, int rows, int columns) {
		super(doc, text, rows, columns);
		initialize();

	}

	private void initialize() {
		this.setFont(FontPanambi.getLabelInstance());
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(new StringSelection(JTextAreaUpper.this.getText()), JTextAreaUpper.this);
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							e.getComponent().dispatchEvent(new FocusEvent(e.getComponent(), FocusEvent.FOCUS_GAINED));
						}
					});
				}
				super.mouseClicked(e);
			}
		});
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
				}
			}
		});
		this.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent e) {
				if (!e.isTemporary() && e.getComponent().isEnabled()) {
					JTextAreaUpper.this.selectAll();
				}
			}
		});
		((AbstractDocument) this.getDocument()).setDocumentFilter((new DocumentSizeFilter()));
	}

	private class DocumentSizeFilter extends DocumentFilter {

		/**
		 * Allows unlimited text.
		 */
		public static final int UNLIMITED_TEXT = -1;

		private int maxCharacters;

		public DocumentSizeFilter() {
			this(UNLIMITED_TEXT);
		}

		public DocumentSizeFilter(int maxChars) {
			setMaxSize(maxChars);
		}

		public void setMaxSize(int maxChars) {
			maxCharacters = maxChars;
		}

		public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
			if (maxCharacters == UNLIMITED_TEXT || (fb.getDocument().getLength() + str.length()) <= maxCharacters)
				super.insertString(fb, offs, str.toUpperCase(), a);
			else
				Toolkit.getDefaultToolkit().beep();
		}

		public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {
			if (maxCharacters == UNLIMITED_TEXT || (fb.getDocument().getLength() + str.length() - length) <= maxCharacters)
				super.replace(fb, offs, length, (str != null ? str.toUpperCase() : null), a);
			else
				Toolkit.getDefaultToolkit().beep();
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		setDisabledTextColor(Color.BLACK);
		super.setEnabled(enabled);
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
	}

}
