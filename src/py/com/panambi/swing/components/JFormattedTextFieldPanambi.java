/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Dimension;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.text.Format;

import javax.swing.JFormattedTextField;

import py.com.panambi.swing.font.FontPanambi;

/**
 *
 */
public class JFormattedTextFieldPanambi extends JFormattedTextField implements ClipboardOwner {

	/**
	 * 
	 */
	private static final long serialVersionUID = 662915613895495303L;

	/**
	 * 
	 */
	public JFormattedTextFieldPanambi() {
		super();
		initialize();
	}

	/**
	 * @param value
	 */
	public JFormattedTextFieldPanambi(Object value) {
		super(value);
		initialize();
	}

	/**
	 * @param format
	 */
	public JFormattedTextFieldPanambi(Format format) {
		super(format);
		initialize();
	}

	/**
	 * @param formatter
	 */
	public JFormattedTextFieldPanambi(AbstractFormatter formatter) {
		super(formatter);
		initialize();
	}

	/**
	 * @param factory
	 */
	public JFormattedTextFieldPanambi(AbstractFormatterFactory factory) {
		super(factory);
		initialize();
	}

	/**
	 * @param factory
	 * @param currentValue
	 */
	public JFormattedTextFieldPanambi(AbstractFormatterFactory factory, Object currentValue) {
		super(factory, currentValue);
		initialize();
	}

	private void initialize() {
		this.setFont(FontPanambi.getLabelInstance());
		this.setPreferredSize(new Dimension(80, getPreferredSize().height));
		this.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent e) {
				if (!e.isTemporary() && e.getComponent().isEnabled()) {
					JFormattedTextFieldPanambi.this.selectAll();
				}
			}
		});
		this.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JFormattedTextFieldPanambi.this.transferFocus();
				}
			}
		});

	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
	}

}
