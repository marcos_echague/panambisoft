package py.com.panambi.swing.components.table;

import java.text.DecimalFormat;

import javax.swing.SwingConstants;

public class DoubleTableCellRenderer extends PanambiTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -290858200455239434L;
	String pattern = "#,##0.00";
	DecimalFormat formatter = null;

	public DoubleTableCellRenderer() {
		super();
		initialize();

	}

	public DoubleTableCellRenderer(String pattern) {
		super();
		setPattern(pattern);
		initialize();
	}

	private void initialize() {
		setHorizontalAlignment(SwingConstants.RIGHT);
		formatter = new DecimalFormat(getPattern());
	}

	public void setValue(Object value) {
		setText((value == null) ? "" : formatter.format(value));
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}
