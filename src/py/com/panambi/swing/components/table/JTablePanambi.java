/**
 * 
 */
package py.com.panambi.swing.components.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterEvent.Type;
import javax.swing.event.RowSorterListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.JTextComponent;

import py.com.panambi.swing.font.FontPanambi;

/**
 * 
 */
public class JTablePanambi extends JTable implements ClipboardOwner {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7830926907731818297L;
	private SimpleDateFormat sdftime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private DecimalFormat decf = new DecimalFormat("#,##0.00");
	private PanambiTableRowSorter sorter = null;
	RowFilter<Object, Object> filter = null;
	private HashMap<Integer, Color> hashModeltemp;

	/**
	 * @param columnNames
	 */
	public JTablePanambi(String[] columnNames) {
		super(new TableModelPanambi(columnNames));
		initialize();
	}

	/**
	 * @param columnNames
	 */
	public JTablePanambi(String[] columnNames, Integer[] editableColumns) {
		super(new TableModelPanambi(columnNames, editableColumns));
		initialize();
	}

	/**
	 * @param columnNames
	 */
	public JTablePanambi(String[] columnNames, Integer[] editableColumns, HashMap<Integer, Class<?>> dataTypes) {
		super(new TableModelPanambi(columnNames, editableColumns, dataTypes));
		initialize();
	}

	/**
	 * @param columnNames
	 */
	public JTablePanambi(String[] columnNames, HashMap<Integer, Class<?>> dataTypes) {
		super(new TableModelPanambi(columnNames, dataTypes));
		initialize();
	}

	private void initialize() {
		setFont(FontPanambi.getTableInstance());
		getTableHeader().setFont(FontPanambi.getLabelInstance());
		this.setDefaultRenderer(String.class, new PanambiTableCellRenderer());
		this.setDefaultRenderer(Double.class, new DoubleTableCellRenderer());
		this.setDefaultRenderer(Timestamp.class, new TimestampTableCellRenderer());
		this.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		this.setDefaultRenderer(Integer.class, new IntegerTableCellRenderer());
		this.setDefaultRenderer(Boolean.class, new BooleanTableCellRenderer());
		this.setDefaultEditor(String.class, new PanambiTableCellEditor(new JTextField()));
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_F6) {
					if (JTablePanambi.this.getSelectedRow() > -1 && JTablePanambi.this.getSelectedColumn() > -1) {
						String valor = "";
						Object obj = JTablePanambi.this.getValueAt(JTablePanambi.this.getSelectedRow(), JTablePanambi.this.getSelectedColumn());
						if (obj != null) {
							if (obj instanceof Date) {
								valor = sdf.format(obj);
							} else if (obj instanceof Timestamp) {
								valor = sdftime.format(obj);
							} else if (obj instanceof Double) {
								valor = decf.format(obj);
							} else {
								valor = obj.toString();
							}
						}
						StringSelection stringSelection = new StringSelection(valor);
						Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, JTablePanambi.this);
					}
				}
			}
		});
		setRowSorter(getSorter());
		this.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.<KeyStroke> emptySet());
		this.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, Collections.<KeyStroke> emptySet());
		Action controlTab = new AbstractAction() {
			private static final long serialVersionUID = 199097039881855943L;

			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) e.getSource();

				if (table.isEditing() && !table.getCellEditor().stopCellEditing()) {
					return;
				}
				table.transferFocus();
			}
		};
		Action shiftControlTab = new AbstractAction() {

			private static final long serialVersionUID = -5375225782332546682L;

			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) e.getSource();

				if (table.isEditing() && !table.getCellEditor().stopCellEditing()) {
					return;
				}
				table.transferFocusBackward();
			}
		};

		KeyStroke controlTabKeyStroke = KeyStroke.getKeyStroke("control TAB");
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(controlTabKeyStroke, "controlTab");
		this.getActionMap().put("controlTab", controlTab);

		KeyStroke shiftControlTabKeyStroke = KeyStroke.getKeyStroke("shift control TAB");
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(shiftControlTabKeyStroke, "shiftControlTab");
		this.getActionMap().put("shiftControlTab", shiftControlTab);
	}

	private PanambiTableRowSorter getSorter() {
		// if (sorter == null) {
		sorter = new PanambiTableRowSorter(getModel());
		sorter.removeRowSorterListener(new RowSorterListener() {
			@Override
			public void sorterChanged(RowSorterEvent e) {
				mySorterChanged(e);
			}
		});
		sorter.addRowSorterListener(new RowSorterListener() {
			@Override
			public void sorterChanged(RowSorterEvent e) {
				mySorterChanged(e);
			}
		});
		// }
		return sorter;
	}

	public boolean editCellAt(int row, int column, EventObject e) {
		boolean result = super.editCellAt(row, column, e);
		Component editor = getEditorComponent();
		if (editor != null) {
			editor.setFont(FontPanambi.getTableInstance());
			if (editor instanceof JComboBox)
				editor = ((JComboBox<?>) editor).getEditor().getEditorComponent();
			if (editor instanceof JTextComponent) {
				((JTextComponent) editor).selectAll();
			}
		}
		return result;
	}

	// public void changeSelection(final int row, final int column, boolean
	// toggle, boolean extend) {
	// super.changeSelection(row, column, toggle, extend);
	// SwingUtilities.invokeLater(new Runnable() {
	// public void run() {
	// JTablePanambi.this.dispatchEvent(new KeyEvent(JTablePanambi.this,
	// KeyEvent.KEY_PRESSED, 0, 0, KeyEvent.VK_F2, KeyEvent.CHAR_UNDEFINED));
	// }
	// });
	// }

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		// TODO Auto-generated method stub
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			JComponent jc = (JComponent) c;
			Object valor = getValueAt(row, column);
			String text = "";
			if (valor != null && valor instanceof String) {
				text = (String) valor;
			}
			if (valor != null && valor instanceof Double) {
				text = decf.format((Double) valor);
			}
			if (valor != null && valor instanceof Integer) {
				text = valor.toString();
			}
			if (valor != null && valor instanceof Date) {
				text = sdf.format((Date) valor);
			}
			if (valor != null && valor instanceof Timestamp) {
				text = sdftime.format((Timestamp) valor);
			}
			int colWidth = this.getColumnModel().getColumn(column).getWidth();
			Font font = jc.getFont();
			FontMetrics fontMetrics = jc.getFontMetrics(font);
			int textWidth = SwingUtilities.computeStringWidth(fontMetrics, text);
			if ((textWidth + 2) >= colWidth) {
				jc.setToolTipText(text);
			} else {
				jc.setToolTipText(null);
			}
		}
		return c;
	};

	/**
	 * @param foreColorRows
	 *            the foreColorRows to set Cambia el color de la fuente para la
	 *            fila recibida
	 */
	public void changeRowColor(Integer row, Color color) {
		for (int i = 0; i < getColumnCount(); i++) {
			if (getRowCount() > 0 && (getCellRenderer(row, i) instanceof PanambiTableCellRenderer)) {
				((PanambiTableCellRenderer) getCellRenderer(row, i)).setForeColorRow(row, color);
			}
		}
		this.repaint();

	}
	
	/**
	 * @param foreColorRows
	 *            the foreColorRows to set Cambia el color de la fuente para la
	 *            celda recibida
	 */
	public void changeCellColor(Integer row, Integer column, Color color) {
		((PanambiTableCellRenderer) getCellRenderer(row, column)).setForeColorRow(row, color);
		this.repaint();

	}


	public void addRow() {
		setRowSorter(null);
		((TableModelPanambi) getModel()).addRow();
		changeSelection(getRowCount() - 1, 0, false, false);
		setRowSorter(getSorter());
	}

	public void resetData(int rowcount) {
		setRowSorter(null);
		((TableModelPanambi) getModel()).resetData(rowcount);
		((PanambiTableCellRenderer) getDefaultRenderer(String.class)).resetRowColor();
		((PanambiTableCellRenderer) getDefaultRenderer(Integer.class)).resetRowColor();
		((PanambiTableCellRenderer) getDefaultRenderer(Date.class)).resetRowColor();
		((PanambiTableCellRenderer) getDefaultRenderer(Timestamp.class)).resetRowColor();
		((PanambiTableCellRenderer) getDefaultRenderer(Double.class)).resetRowColor();
		setRowSorter(getSorter());
	}

	@SuppressWarnings("unchecked")
	public void removeRow(int rowindex) {
		changeRowColor(rowindex, null);
		((TableModelPanambi) getModel()).removeRow(rowindex);

		HashMap<Integer, Color> hashclone = (HashMap<Integer, Color>) ((PanambiTableCellRenderer) getDefaultRenderer(String.class)).getForeColorRows().clone();
		Set<Integer> keys = hashclone.keySet();
		for (Integer key : keys) {
			if (key > rowindex) {
				if (key > 0) {
					changeRowColor((key - 1), hashclone.get(key));
				}
				changeRowColor(key, null);
			}
		}
	}

	public void removeRows(Integer[] rowindexs) {
		// setRowSorter(null);
		List<Integer> idxModel = new ArrayList<Integer>();
		for (int i = rowindexs.length - 1; i >= 0; i--) {
			idxModel.add(convertRowIndexToModel(rowindexs[i]));

		}
		Collections.sort(idxModel, new Comparator<Integer>() {
			@Override
			public int compare(Integer o2, Integer o1) {
				return o1.compareTo(o2);
			}
		});

		for (Integer idx : idxModel) {
			removeRow(idx);
		}
		// setRowSorter(sorter);
	}

	@SuppressWarnings("unchecked")
	private void mySorterChanged(RowSorterEvent e) {
		HashMap<Integer, Color> hashtemp = new HashMap<Integer, Color>();
		if (((PanambiTableCellRenderer) getDefaultRenderer(String.class)).getForeColorRows() != null) {
			hashtemp = (HashMap<Integer, Color>) ((PanambiTableCellRenderer) getDefaultRenderer(String.class)).getForeColorRows().clone();
		}
		try {
			if (e.getType() == Type.SORT_ORDER_CHANGED) {
				hashModeltemp = new HashMap<Integer, Color>();
				Set<Integer> keys = hashtemp.keySet();
				for (Integer key : keys) {
					hashModeltemp.put(convertRowIndexToModel(key), hashtemp.get(key));
				}
			} else if (e.getType() == Type.SORTED) {
				if (hashModeltemp != null) {
					for (int newindex = 0; newindex < getRowCount(); newindex++) {
						int oldindex = convertRowIndexToModel(newindex);
						if (hashModeltemp.containsKey(oldindex)) {
							changeRowColor(newindex, hashModeltemp.get(oldindex));
						} else {
							changeRowColor(newindex, null);
						}
					}
				}
			}
		} catch (IndexOutOfBoundsException e3) {
		}
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		// TODO Auto-generated method stub

	}

	public void setRowFilter(RowFilter<Object, Object> filter) {
		this.filter = filter;
		sorter.setRowFilter(filter);
	}

}
