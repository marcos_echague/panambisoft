package py.com.panambi.swing.components.table;

import java.awt.Color;
import java.util.HashMap;

import javax.swing.table.DefaultTableCellRenderer;

public class PanambiTableCellRenderer extends DefaultTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8779221221143547190L;
	private HashMap<Integer, Color> foreColorRows;

	/**
	 * @return the foreColorRows
	 */
	public HashMap<Integer, Color> getForeColorRows() {
		return foreColorRows;
	}

	/**
	 * @param foreColorRows the foreColorRows to set
	 */
	public void setForeColorRow(Integer row, Color color) {
		if (getForeColorRows() == null) {
			foreColorRows = new HashMap<Integer, Color>();
		}
		foreColorRows.put(row, color);
	}
	
	public void resetRowColor() {
		foreColorRows = new HashMap<Integer, Color>();
	}
}
