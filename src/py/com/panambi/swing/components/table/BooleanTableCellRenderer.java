package py.com.panambi.swing.components.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class BooleanTableCellRenderer extends PanambiTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6722583510971348277L;

	protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

	private static final Border SAFE_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		JCheckBox component = new JCheckBox();
		component.setHorizontalAlignment(SwingConstants.CENTER);
		if (!isSelected) {
			component.setBackground(UIManager.getColor("Table.background"));
		}
		if (isSelected && !hasFocus) {
			component.setBackground(UIManager.getColor("Table.selectionBackground"));
		}
		if (hasFocus) {
			Border border = null;
			if (isSelected) {
				border = UIManager.getBorder("Table.focusSelectedCellHighlightBorder");
			}
			if (border == null) {
				border = UIManager.getBorder("Table.focusCellHighlightBorder");
			}
			component.setBorder(border);

			if (!isSelected && table.isCellEditable(row, column)) {
				Color col;
				col = UIManager.getColor("Table.focusCellForeground");
				if (col != null) {
					component.setForeground(col);
				}
				col = UIManager.getColor("Table.editableBackground");
				if (col != null) {
					component.setBackground(col);
				}
			}
		} else {
			component.setBorder(getNoFocusBorder());
		}
		if (value != null) {
			component.setSelected((Boolean) value);
		}
		return component;
	}

	private static Border getNoFocusBorder() {
		if (System.getSecurityManager() != null) {
			return SAFE_NO_FOCUS_BORDER;
		} else {
			return noFocusBorder;
		}
	}

}