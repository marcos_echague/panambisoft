/**
 * 
 */
package py.com.panambi.swing.components.table;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 */
public class BooleanTableCellEditor extends DefaultCellEditor {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2935949540767908917L;

	private boolean debug = false;

    
    /** Creates new BooleanCellEditor */
    public BooleanTableCellEditor(JTextField textField) {
        super(textField);
    }
    
    public BooleanTableCellEditor(JCheckBox checkBox) {
        super(checkBox);
        checkBox.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    
    public BooleanTableCellEditor(JComboBox comboBox) {
        super(comboBox);
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column){
        if(debug){System.out.println("BooleanCellEditor:shouldSelectCell(EventObject)");}
        return(super.getTableCellEditorComponent(table, value, isSelected, row, column));
    }
    
    public boolean shouldSelectCell(EventObject anEvent){
        if(debug){System.out.println("BooleanCellEditor:shouldSelectCell(EventObject)");}
        boolean result = super.shouldSelectCell(anEvent);
/*
        Component tempComponent = getComponent();
        if (tempComponent instanceof JCheckBox) {
            JCheckBox tempJCheckBox = (JCheckBox) tempComponent;
            if (tempJCheckBox != null){
                boolean tempBoolean = tempJCheckBox.isSelected();
                if(debug){System.out.println("BooleanCellEditor:shouldSelectCell(): tempBoolean = " + tempBoolean);}
                try{
                    originalValue = new Integer(tempString);
                    if(debug){System.out.println("BooleanCellEditor:shouldSelectCell(): tempDouble = " + originalValue);}
                } catch (Exception e){
                    result = false;
                }
            }
        }
 */
        return(result);
    }
    
    public boolean stopCellEditing(){
        if(debug){System.out.println("BooleanCellEditor:stopCellEditing()");}
        boolean result = super.stopCellEditing();
/*        
        Component tempComponent = getComponent();
        if( tempComponent instanceof JTextField){
            JTextField tempJTextField = (JTextField) tempComponent;
            if (tempJTextField!=null){
                String tempString = tempJTextField.getText();
                if(debug){System.out.println("BooleanCellEditor:stopCellEditing(): tempString = " + tempString);}
                try{
                    Integer tempInteger = new Integer(tempString);
                    intDataTableModel = (DefaultTableModel)intJTable.getModel();
                    if (tempInteger.intValue()<0){
                        tempInteger = new Integer(0);
                    }
                    if (tempInteger.intValue()>255){
                        tempInteger = new Integer(255);
                    }
                    intDataTableModel.setValueAt(tempInteger, intRow, intColumn);
                    tempJTextField.setText(tempInteger.toString());
                    if(debug){System.out.println("BooleanCellEditor:stopCellEditing(): tempInteger = " + tempInteger);}
                } catch (Exception e){
                    if(originalValue!=null){
                        if(debug){System.out.println("BooleanCellEditor:stopCellEditing():restore originalValue = " + originalValue + ", intRow = " + intRow + ", intColumn = " + intColumn);}                        
                        intDataTableModel = (DefaultTableModel)intJTable.getModel();
                        intDataTableModel.setValueAt(originalValue, intRow, intColumn);
                        tempJTextField.setText(originalValue.toString());
                    } else {
                        if(debug){System.out.println("BooleanCellEditor:stopCellEditing():restore originalValue = null");}                        
                    }
                    cancelCellEditing();
                    result = false;
                }
            }
        }
 */
        return(result);
    }
}