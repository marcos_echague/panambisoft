package py.com.panambi.swing.components.table;

import java.text.SimpleDateFormat;

import javax.swing.SwingConstants;

public class DateTableCellRenderer extends PanambiTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4574406966556618566L;

	private String pattern = "dd-MM-yyyy";
	SimpleDateFormat formatter = null;

	public DateTableCellRenderer() {
		super();
		initialize();
	}

	public DateTableCellRenderer(String pattern) {
		super();
		setPattern(pattern);
		initialize();
	}

	private void initialize() {
		formatter = new SimpleDateFormat(getPattern());
	}

	public void setValue(Object value) {
		setText((value == null) ? "" : formatter.format(value));
		setHorizontalAlignment(SwingConstants.CENTER);
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}
