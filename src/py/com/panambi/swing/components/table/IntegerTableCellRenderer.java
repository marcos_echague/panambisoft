package py.com.panambi.swing.components.table;

import java.text.DecimalFormat;

import javax.swing.SwingConstants;

public class IntegerTableCellRenderer extends PanambiTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1781772720982897287L;
	String pattern = "#0";
	DecimalFormat formatter = null;

	public IntegerTableCellRenderer() {
		initialize();

	}

	public IntegerTableCellRenderer(String pattern) {
		setPattern(pattern);
		initialize();
	}

	private void initialize() {
		setHorizontalAlignment(SwingConstants.RIGHT);
		formatter = new DecimalFormat(getPattern());
	}

	public void setValue(Object value) {
		setText((value == null) ? "" : formatter.format(value));
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}
