package py.com.panambi.swing.components.table;

/**
 * 
 */
import java.text.SimpleDateFormat;

import javax.swing.SwingConstants;

/**
 */
public class TimestampTableCellRenderer extends PanambiTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8825283531499154810L;
	private String pattern = "dd-MM-yyyy HH:mm:ss";
	SimpleDateFormat formatter = null;

	public TimestampTableCellRenderer() {
		super();
		initialize();
	}

	public TimestampTableCellRenderer(String pattern) {
		super();
		setPattern(pattern);
		initialize();
	}

	private void initialize() {
		formatter = new SimpleDateFormat(getPattern());
	}

	public void setValue(Object value) {
		setText((value == null) ? "" : formatter.format(value));
		setHorizontalAlignment(SwingConstants.CENTER);
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
