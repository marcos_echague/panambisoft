package py.com.panambi.swing.components.table;

import java.text.Collator;
import java.util.Comparator;

import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class PanambiTableRowSorter extends TableRowSorter<TableModel> {

	private static final Comparator<?> COMPARABLE_COMPARATOR = new ComparableComparator();

	/**
	 * 
	 */
	public PanambiTableRowSorter() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param model
	 */
	public PanambiTableRowSorter(TableModel model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	public Comparator<?> getComparator(int column) {
		Class<?> columnClass = getModel().getColumnClass(column);
		if (columnClass == String.class) {
			//            return Collator.getInstance();
			return new PanambiStringComparator();
		}
		Comparator<?> comparator = super.getComparator(column);
		if (comparator != null) {
			return comparator;
		}
		if (Comparable.class.isAssignableFrom(columnClass)) {
			return COMPARABLE_COMPARATOR;
		}
		return Collator.getInstance();
	}

	private static class ComparableComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			return ((Comparable) o1).compareTo(o2);
		}
	}

	class PanambiStringComparator implements Comparator<String> {
		@Override
		public int compare(String o1, String o2) {
			if (o1 != null) {
				return o1.compareTo(o2);
			}
			return 0;
		}

	}

}
