/**
 * 
 */
package py.com.panambi.swing.components.table;

import java.util.HashMap;

import javax.swing.table.DefaultTableModel;

/**
 * 
 */
public class TableModelPanambi extends DefaultTableModel {

	private String columnsNames[];
	private Object rowData[][];
	private Integer[] editableColumns;
	private HashMap<Integer, Class<?>> dataTypes = new HashMap<Integer, Class<?>>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 996395261000039310L;

	/**
	 * 
	 */
	public TableModelPanambi(String[] columnNames) {
		this.columnsNames = columnNames;
		initialize();
	}

	/**
	 * 
	 */
	public TableModelPanambi(String[] columnNames, HashMap<Integer, Class<?>> dataTypes) {
		this.columnsNames = columnNames;
		this.dataTypes = dataTypes;
		initialize();
	}

	/**
	 * 
	 */
	public TableModelPanambi(String[] columnNames, Integer[] editableColumns, HashMap<Integer, Class<?>> dataTypes) {
		this.columnsNames = columnNames;
		this.editableColumns = editableColumns;
		this.dataTypes = dataTypes;
		initialize();
	}

	/**
	 * 
	 */
	public TableModelPanambi(String[] columnNames, Integer[] editableColumns) {
		this.columnsNames = columnNames;
		this.editableColumns = editableColumns;
		initialize();
	}

	private void initialize() {
		resetData(0);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		boolean ret = false;
		if (editableColumns != null) {
			for (int i = 0; i < editableColumns.length; i++) {
				Integer colindex = editableColumns[i];
				if (colindex == column) {
					ret = true;
					break;
				}

			}
		}
		return ret;
	}

	public String getColumnName(int col) {
		return columnsNames[col].toString();
	}

	public Class<?> getColumnClass(int c) {
		Class<?> ret = String.class;
		Object obj = getValueAt(0, c);
		if (obj != null) {
			ret = getValueAt(0, c).getClass();
		} else {
			if (dataTypes.get(c) != null) {
				ret = dataTypes.get(c);
			}
		}
		return ret;
	}

	public int getRowCount() {
		if (rowData != null) {
			return rowData.length;
		} else {
			return 0;
		}
	}

	public int getColumnCount() {
		return columnsNames.length;
	}

	public Object getValueAt(int row, int col) {
		return rowData[row][col];
	}

	public void setValueAt(Object value, int row, int col) {
		rowData[row][col] = value;
		fireTableCellUpdated(row, col);
	}

	public void addRow() {
		Object[][] obj = new Object[rowData.length + 1][columnsNames.length];
		for (int i = 0; i < rowData.length; i++) {
			for (int j = 0; j < rowData[0].length; j++) {
				obj[i][j] = rowData[i][j];
			}
		}
		rowData = obj;
		fireTableRowsInserted(rowData.length - 1, rowData.length - 1);
	}

	public void resetData(int rowcount) {
		rowData = new Object[rowcount][columnsNames.length];
		fireTableDataChanged();
	}

	public void removeRow(int row) {
		System.out.println("Row count despues MODEL " + getRowCount());
		Object[][] obj = new Object[rowData.length - 1][columnsNames.length];
		System.out.println("removingg.... indice: " + row + " dato " + rowData[row][0]);
		for (int i = 0; i < rowData.length; i++) {
			if (i != row) {
				for (int j = 0; j < rowData[0].length; j++) {
					if (i > row) {
						obj[i - 1][j] = rowData[i][j];
					} else {
						obj[i][j] = rowData[i][j];
					}
				}
			}
		}
		rowData = obj;
		fireTableRowsDeleted(row, row);
		System.out.println("Row count despues MODEL " + getRowCount());
	}

}
