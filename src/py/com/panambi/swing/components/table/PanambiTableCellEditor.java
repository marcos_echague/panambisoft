package py.com.panambi.swing.components.table;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class PanambiTableCellEditor extends DefaultCellEditor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4303930259401704359L;

	/**
	 * @param textField
	 */
	public PanambiTableCellEditor(JTextField textField) {
		super(textField);
		((AbstractDocument)textField.getDocument()).setDocumentFilter(new DocumentUpperFilter());
		initialize();
	}

	public PanambiTableCellEditor(JCheckBox checkBox) {
		super(checkBox);
		initialize();
	}

	/**
	 * @param comboBox
	 */
	public PanambiTableCellEditor(JComboBox comboBox) {
		super(comboBox);
		initialize();
	}
	
	

	private void initialize() {
		
	}



	private class DocumentUpperFilter extends DocumentFilter {

		public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
			super.insertString(fb, offs, str.toUpperCase(), a);
		}

		public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {
			super.replace(fb, offs, length, (str != null ? str.toUpperCase() : null), a);
		}
	}

}
