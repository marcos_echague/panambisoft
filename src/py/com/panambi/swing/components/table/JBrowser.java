/**
 * 
 */
package py.com.panambi.swing.components.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import py.com.panambi.interfaces.Browseable;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;
import py.com.panambi.utils.PanambiUtils;

/**
 * 
 */
public class JBrowser extends JDialogPanambi {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelTittle = null;
	private JPanel jPanelCenter = null;
	private JPanel jPanelFilter = null;
	private JScrollPane jScrollPane = null;
	private JTablePanambi jTable = null;
	private String[] columnNames;
	private Connection conn;
	private Boolean autoPopulate;
	private String sSelect;
	private Browseable browseable;
	private String bean;
	private String[] pks;
	private Object[] selectParams;
	private JLabelPanambi jLabelFiltro = null;
	private JTextField jTxtFiltro = null;
	private JButtonPanambi jBtnCleanFilter = null;
	private boolean withData = true;
	RowFilter<Object, Object> filter = null;
	private Class<?>[] columnClasses;
	private Boolean selectWithScape = true;
	private JLabel lblNewLabel;

	/**
	 * @param browseable
	 */
	public JBrowser(Connection conn, Browseable browseable, String[] columnNames, String sSelect, Boolean autoPopulate, String bean, String[] pks) {
		this(conn, browseable, columnNames, sSelect, autoPopulate, bean, pks, null);
	}

	/**
	 * @param browseable
	 */
	public JBrowser(Connection conn, Browseable browseable, String[] columnNames, String sSelect, Boolean autoPopulate, String bean, String[] pks, Object[] selectParams) {
		this(conn, browseable, columnNames, sSelect, autoPopulate, bean, pks, selectParams, null);

	}

	/**
	 * @param browseable
	 */
	public JBrowser(Connection conn, Browseable browseable, String[] columnNames, String sSelect, Boolean autoPopulate, String bean, String[] pks, Object[] selectParams, Class<?>[] columnClasses) {
		this(conn, browseable, columnNames, sSelect, autoPopulate, bean, pks, selectParams, columnClasses, true);
	}

	/**
	 * @param browseable
	 */
	public JBrowser(Connection conn, Browseable browseable, String[] columnNames, String sSelect, Boolean autoPopulate, String bean, String[] pks, Object[] selectParams, Class<?>[] columnClasses, Boolean selectWithScape) {
		super((JFrame) ((JInternalFrame) browseable).getParent().getParent().getParent().getParent().getParent());
		this.columnNames = new String[columnNames.length + 1];
		for (int i = 0; i < columnNames.length; i++) {
			this.columnNames[i] = columnNames[i];
		}
		this.columnNames[columnNames.length] = "Data";
		this.sSelect = sSelect;
		this.autoPopulate = autoPopulate;
		this.browseable = browseable;
		this.bean = bean;
		this.pks = pks;
		this.selectParams = selectParams;
		this.columnClasses = columnClasses;
		this.selectWithScape = selectWithScape;
		this.conn = conn;
		initialize();
	}

	/**
	 * @param owner
	 * @wbp.parser.constructor
	 */
	public JBrowser(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		try {
			this.setSize(646, 440);
			this.setContentPane(getJContentPane());
			PanambiUtils iu = new PanambiUtils();
			centerIt();
			if (autoPopulate) {
				populate();
				iu.resizeColumns(this, getJTable());
			}
			if (getJTable().getRowCount() > 0) {
				getJTable().changeSelection(0, 0, false, false);
				getJScrollPane().getVerticalScrollBar().setValue(0);
			}
			setListeners(getRootPane());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	// private void populate() throws Exception {
	// PreparedStatement ps = conn.prepareStatement(sSelect);
	// if (selectParams != null) {
	// for (int i = 0; i < selectParams.length; i++) {
	// Object param = selectParams[i];
	// if (param instanceof Integer) {
	// ps.setInt((i + 1), (Integer) param);
	// }
	// if (param instanceof String) {
	// ps.setString((i + 1), (String) param);
	// }
	// if (param instanceof Date) {
	// ps.setDate((i + 1), (Date) param);
	// }
	// if (param instanceof Timestamp) {
	// ps.setTimestamp((i + 1), (Timestamp) param);
	// }
	// }
	// }
	// ResultSet rs = ps.executeQuery();
	// int j = 0;
	// while (rs.next()) {
	// j++;
	// getJTable().addRow();
	// Class<?> cls = Class.forName(bean);
	// Constructor<?> constructor = null;
	// Object obj = null;
	// if (pks.length == 1) {
	// constructor = cls.getConstructor(Connection.class, Integer.class);
	// obj = constructor.newInstance(conn, rs.getInt(pks[0]));
	// }
	// if (pks.length == 2) {
	// constructor = cls.getConstructor(Connection.class, Integer.class,
	// Integer.class);
	// obj = constructor.newInstance(conn, rs.getInt(pks[0]),
	// rs.getInt(pks[1]));
	// }
	// if (pks.length == 3) {
	// constructor = cls.getConstructor(Connection.class, Integer.class,
	// Integer.class, Integer.class);
	// obj = constructor.newInstance(conn, rs.getInt(pks[0]), rs.getInt(pks[1]),
	// rs.getInt(pks[2]));
	// }
	// for (int i = 0; i < columnNames.length - 1; i++) {
	// if (columnClasses != null) {
	// if (columnClasses[i].equals(Integer.class)) {
	// getJTable().setValueAt(rs.getInt((i + 1)), getJTable().getRowCount() - 1,
	// i);
	// }
	// if (columnClasses[i].equals(Date.class)) {
	// getJTable().setValueAt(rs.getDate((i + 1)), getJTable().getRowCount() -
	// 1, i);
	// }
	// if (columnClasses[i].equals(String.class)) {
	// getJTable().setValueAt(rs.getString((i + 1)), getJTable().getRowCount() -
	// 1, i);
	// }
	// if (columnClasses[i].equals(Timestamp.class)) {
	// getJTable().setValueAt(rs.getTimestamp((i + 1)),
	// getJTable().getRowCount() - 1, i);
	// }
	// if (columnClasses[i].equals(Double.class)) {
	// getJTable().setValueAt(rs.getDouble((i + 1)), getJTable().getRowCount() -
	// 1, i);
	// }
	// } else {
	// getJTable().setValueAt(rs.getString((i + 1)), getJTable().getRowCount() -
	// 1, i);
	// }
	// }
	// getJTable().setValueAt(obj, getJTable().getRowCount() - 1,
	// columnNames.length - 1);
	// }
	// rs.close();
	// ps.close();
	// if (j == 0) {
	// withData = false;
	// DlgMessage.showMessage(this, "No se encuentran registros",
	// DlgMessage.WARNING_MESSAGE);
	// }
	// }

	private void populate() throws Exception {
		PreparedStatement ps = conn.prepareStatement(sSelect);
		if (selectParams != null) {
			for (int i = 0; i < selectParams.length; i++) {
				Object param = selectParams[i];
				if (param instanceof Integer) {
					ps.setInt((i + 1), (Integer) param);
				}
				if (param instanceof String) {
					ps.setString((i + 1), (String) param);
				}
				if (param instanceof Date) {
					ps.setDate((i + 1), (Date) param);
				}
				if (param instanceof Timestamp) {
					ps.setTimestamp((i + 1), (Timestamp) param);
				}
			}
		}
		ResultSet rs = ps.executeQuery();
		int j = 0;
		while (rs.next()) {
			j++;
			getJTable().addRow();
			Class<?> cls = Class.forName(bean);
			Class<?> clsControler = Class.forName(getControlerName());
			Constructor<?> constructor = null;
			Object obj = new Object();
			Constructor<?> controllerConstructor = clsControler.getConstructor();
			Object objController = controllerConstructor.newInstance();
			try {
				if (pks.length == 1) {
					constructor = cls.getConstructor(Connection.class, Integer.class);
					obj = constructor.newInstance(conn, rs.getInt(pks[0]));
				}
				if (pks.length == 2) {
					constructor = cls.getConstructor(Connection.class, Integer.class, Integer.class);
					obj = constructor.newInstance(conn, rs.getInt(pks[0]), rs.getInt(pks[1]));
				}
				if (pks.length == 3) {
					constructor = cls.getConstructor(Connection.class, Integer.class, Integer.class, Integer.class);
					obj = constructor.newInstance(conn, rs.getInt(pks[0]), rs.getInt(pks[1]), rs.getInt(pks[2]));
				}
			} catch (NoSuchMethodException e) {
				Method met = null;
				if (pks.length == 1) {
					met = clsControler.getMethod("get" + cls.getSimpleName(), Connection.class, Integer.class);
					obj = met.invoke(objController, conn, rs.getInt(pks[0]));
				}
				if (pks.length == 2) {
					met = clsControler.getMethod("get" + cls.getSimpleName(), Connection.class, Integer.class, Integer.class);
					obj = met.invoke(objController, conn, rs.getInt(pks[0]), rs.getInt(pks[1]));
				}
				if (pks.length == 3) {
					met = clsControler.getMethod("get" + cls.getSimpleName(), Connection.class, Integer.class, Integer.class, Integer.class);
					obj = met.invoke(objController, conn, rs.getInt(pks[0]), rs.getInt(pks[1]), rs.getInt(pks[2]));
				}
			}
			for (int i = 0; i < columnNames.length - 1; i++) {
				if (columnClasses != null) {
					if (columnClasses[i].equals(Integer.class)) {
						getJTable().setValueAt(rs.getInt((i + 1)), getJTable().getRowCount() - 1, i);
					}
					if (columnClasses[i].equals(Date.class)) {
						getJTable().setValueAt(rs.getDate((i + 1)), getJTable().getRowCount() - 1, i);
					}
					if (columnClasses[i].equals(String.class)) {
						getJTable().setValueAt(rs.getString((i + 1)), getJTable().getRowCount() - 1, i);
					}
					if (columnClasses[i].equals(Timestamp.class)) {
						getJTable().setValueAt(rs.getTimestamp((i + 1)), getJTable().getRowCount() - 1, i);
					}
					if (columnClasses[i].equals(Double.class)) {
						getJTable().setValueAt(rs.getDouble((i + 1)), getJTable().getRowCount() - 1, i);
					}
				} else {
					getJTable().setValueAt(rs.getString((i + 1)), getJTable().getRowCount() - 1, i);
				}
			}
			getJTable().setValueAt(obj, getJTable().getRowCount() - 1, columnNames.length - 1);
		}
		rs.close();
		ps.close();
		if (j == 0) {
			withData = false;
			DlgMessage.showMessage(this, "No se encuentran registros", DlgMessage.WARNING_MESSAGE);
		}
	}

	private String getControlerName() {
		String ret = "";
		int idx = bean.lastIndexOf(".");
		String pack1 = bean.substring(0, idx);
		int idx2 = pack1.lastIndexOf(".");
		String pack = bean.substring(0, idx2) + ".controller.";
		String name = bean.substring(idx + 1, bean.length());
		ret = pack + "Controlador" + name;
		return ret;
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelTittle(), BorderLayout.NORTH);
			jContentPane.add(getJPanelCenter(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelTittle
	 * 
	 * @return Panambi.utils.JPanelTittle
	 */
	private JPanel getJPanelTittle() {
		if (jPanelTittle == null) {
			jPanelTittle = new JPanel();
			jPanelTittle.add(getLblNewLabel());
		}
		return jPanelTittle;
	}

	/**
	 * This method initializes jPanelCenter
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelCenter() {
		if (jPanelCenter == null) {
			jPanelCenter = new JPanel();
			jPanelCenter.setLayout(new BorderLayout(0, 0));
			jPanelCenter.add(getJPanelFilter(), BorderLayout.NORTH);
			jPanelCenter.add(getJScrollPane());
		}
		return jPanelCenter;
	}

	/**
	 * This method initializes jPanelFilter
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelFilter() {
		if (jPanelFilter == null) {
			jLabelFiltro = new JLabelPanambi();
			jLabelFiltro.setText("Filtrar Texto");
			jPanelFilter = new JPanel();
			jPanelFilter.setAlignmentX(Component.RIGHT_ALIGNMENT);
			jPanelFilter.setMinimumSize(new Dimension(0, 40));
			jPanelFilter.setMaximumSize(new Dimension(1920, 40));
			jPanelFilter.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			GroupLayout gl_jPanelFilter = new GroupLayout(jPanelFilter);
			gl_jPanelFilter.setHorizontalGroup(
				gl_jPanelFilter.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPanelFilter.createSequentialGroup()
						.addContainerGap()
						.addComponent(jLabelFiltro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJTxtFiltro(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJBtnCleanFilter(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(281))
			);
			gl_jPanelFilter.setVerticalGroup(
				gl_jPanelFilter.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPanelFilter.createSequentialGroup()
						.addGap(10)
						.addGroup(gl_jPanelFilter.createParallelGroup(Alignment.BASELINE)
							.addComponent(jLabelFiltro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFiltro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJBtnCleanFilter(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			jPanelFilter.setLayout(gl_jPanelFilter);
		}
		return jPanelFilter;
	}

	/**
	 * This method initializes jScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTable());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTablePanambi getJTable() {
		if (jTable == null) {
			jTable = new JTablePanambi(columnNames);
			jTable.getColumnModel().getColumn(columnNames.length - 1).setMinWidth(0);
			jTable.getColumnModel().getColumn(columnNames.length - 1).setPreferredWidth(0);
			jTable.getColumnModel().getColumn(columnNames.length - 1).setMaxWidth(0);
			jTable.addKeyListener(new java.awt.event.KeyAdapter() {

				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						seleccionar();
					} else if ((e.getKeyCode() == KeyEvent.VK_UP) && (getJTable().getSelectedRow() == 0)) {
						getJTable().changeSelection(getJTable().getRowCount() - 1, 0, false, false);
						e.consume();
					} else if ((e.getKeyCode() == KeyEvent.VK_DOWN) && (getJTable().getSelectedRow() == getJTable().getRowCount() - 1)) {
						getJTable().changeSelection(0, 0, false, false);
						e.consume();
					}
				}
			});
			jTable.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					if (e.getClickCount() > 1) {
						seleccionar();
					}
				}
			});
			// Apply row filter to JTable
			filter = new RowFilter<Object, Object>() {
				public boolean include(Entry<?, ?> entry) {
					boolean ret = false;
					for (int i = 0; i < jTable.getColumnModel().getColumnCount(); i++) {
						Object value = entry.getValue(i);
						if (value != null) {
							String str = value.toString();
							str = str.toLowerCase();
							ret = str.contains(getJTxtFiltro().getText().trim().toLowerCase());
							if (ret) {
								break;
							}
						}
					}
					return ret;
				}
			};

		}
		return jTable;
	}

	private void seleccionar() {
		if (getJTable().getSelectedRow() > -1) {
			browseable.setValues(getJTable().getValueAt(getJTable().getSelectedRow(), columnNames.length - 1), null);
		}
		this.dispose();
	}

	private void setListeners(JComponent component) {
		component.addKeyListener(new KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					if (selectWithScape) {
						seleccionar();
					} else {
						JBrowser.this.dispose();
					}
				}
//				if (e.getKeyCode() == KeyEvent.VK_F3) {
//					doNoSelection();
//				}

			}
		});
		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setListeners(comp);
				}
			}
		}
	}

	/**
	 * This method initializes jTxtFiltro
	 * 
	 */
	private JTextField getJTxtFiltro() {
		if (jTxtFiltro == null) {
			jTxtFiltro = new JTextField();
			jTxtFiltro.addKeyListener(new KeyAdapter() {

				@Override
				public void keyReleased(KeyEvent e) {
					if (!jTxtFiltro.getText().trim().equals("")) {
						getJTable().setRowFilter(filter);
					} else {
						getJTable().setRowFilter(null);
					}
				}
			});
		}
		return jTxtFiltro;
	}

	/**
	 * This method initializes jBtnCleanFilter
	 * 
	 */
	private JButtonPanambi getJBtnCleanFilter() {
		if (jBtnCleanFilter == null) {
			jBtnCleanFilter = new JButtonPanambi();
			jBtnCleanFilter.setText("Limpiar Filtro");
			jBtnCleanFilter.setMnemonic(KeyEvent.VK_L);
			jBtnCleanFilter.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					getJTxtFiltro().setText("");
					getJTable().setRowFilter(null);
					getJTxtFiltro().requestFocus();
				}
			});
		}
		return jBtnCleanFilter;
	}

	/**
	 * @return the withData
	 */
	public boolean isWithData() {
		return withData;
	}

	/**
	 * @param withData
	 *            the withData to set
	 */
	public void setWithData(boolean withData) {
		this.withData = withData;
	}

	@Override
	public void setVisible(boolean b) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				if (!isWithData()) {
					Object ret = null;
					try {
						Class<?> classbean = Class.forName(bean);
						Constructor<?> cons = classbean.getConstructor();
						ret = cons.newInstance();
					} catch (Exception e) {
					}
					browseable.setNoValues(ret, null);
					JBrowser.this.dispose();

				}
			}
		});
		super.setVisible(b);
	}

	private void doNoSelection() {
		try {
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		}
		this.dispose();
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("");
			lblNewLabel.setIcon(new ImageIcon(JBrowser.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
		}
		return lblNewLabel;
	}
}