/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.Format;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import py.com.panambi.swing.font.FontPanambi;

/**
 * @author hcano
 * 
 */
public class JTextFieldInteger extends JFormattedTextFieldPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7587452146674015605L;
	private int maxChars = 10;

	/**
	 * 
	 */
	public JTextFieldInteger() {
		initialize();
	}

	/**
	 * @param value
	 */
	public JTextFieldInteger(Object value) {
		super(value);
		initialize();
	}

	/**
	 * @param format
	 */
	public JTextFieldInteger(Format format) {
		super(format);
		initialize();
	}

	/**
	 * @param formatter
	 */
	public JTextFieldInteger(AbstractFormatter formatter) {
		super(formatter);
		initialize();
	}

	/**
	 * @param factory
	 */
	public JTextFieldInteger(AbstractFormatterFactory factory) {
		super(factory);
		initialize();
	}

	/**
	 * @param factory
	 * @param currentValue
	 */
	public JTextFieldInteger(AbstractFormatterFactory factory, Object currentValue) {
		super(factory, currentValue);
		initialize();
	}

	private void initialize() {
		this.setDocument(new LimitadorCaracteres());
		this.setFont(FontPanambi.getLabelInstance());
		this.setPreferredSize(new Dimension(60, this.getPreferredSize().height));
		this.addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				if (!e.isTemporary() && e.getComponent().isEnabled()) {
					selectAll();
				}
			}

		});
		this.addKeyListener(new KeyAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
			 */
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JTextFieldInteger.this.transferFocus();
				}
			}

		});
		
		setDisabledTextColor(Color.black);
	}

	private class LimitadorCaracteres extends PlainDocument {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8226023882599400309L;

		/**
		 * M�todo al que llama el editor cada vez que se intenta insertar caracteres. S�lo debemos verificar arg1, que es la cadena que se quiere insertar en el JTextField
		 */
		public void insertString(int arg0, String arg1, AttributeSet arg2) throws BadLocationException {
			for (int i = 0; i < arg1.length(); i++) {
				if (this.getLength() >= getCantCaracteres()) {
					return;
				}
				if (!Character.isDigit(arg1.charAt(i))) {
					return;
				}
			}
			super.insertString(arg0, arg1, arg2);
		}
	}

	public int getCantCaracteres() {
		return maxChars;
	}

	public void setMaxChars(int cantCaracteres) {
		this.maxChars = cantCaracteres;
		setDocument(new LimitadorCaracteres());
	}

	public Integer getValor() {
		if (!getText().equals("")) {
			return new Integer(getText());
		} else {
			return null;
		}
	}

	public void setValor(Integer valor) {
		if (valor == null) {
			setText("");
		} else {
			if (!getText().equals(valor.toString())) {
				setText(valor.toString());
			}
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		setDisabledTextColor(Color.BLACK);
		super.setEnabled(enabled);
	}
	

}
