/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.Format;

import javax.swing.JFormattedTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import py.com.panambi.swing.font.FontPanambi;

/**
 * 
 */
public class JTextFieldPercent extends JFormattedTextFieldPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 768309070125724687L;
	private String mask = "#,##0.0#";
	private int cantDecimals = 2;
	private int cantEnteros = 2;

	/**
	 * 
	 */
	public JTextFieldPercent() {
		super();
		initialize();
	}

	public JTextFieldPercent(String mask) {
		super();
		this.mask = mask;
		initialize();
	}

	public JTextFieldPercent(Object value) {
		super(value);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param format
	 */
	public JTextFieldPercent(Format format) {
		super(format);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param formatter
	 */
	public JTextFieldPercent(AbstractFormatter formatter) {
		super(formatter);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param factory
	 */
	public JTextFieldPercent(AbstractFormatterFactory factory) {
		super(factory);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param factory
	 * @param currentValue
	 */
	public JTextFieldPercent(AbstractFormatterFactory factory, Object currentValue) {
		super(factory, currentValue);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		java.util.Locale.setDefault(java.util.Locale.UK);
		setMask();
		this.setFont(FontPanambi.getLabelInstance());
		this.setPreferredSize(new Dimension(80, getPreferredSize().height));
		this.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent e) {
				selectItLater(e.getComponent());
			}
		});
		this.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_DOWN) {
					e.getComponent().transferFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					e.getComponent().transferFocusBackward();
				}
				if (e.getKeyCode() == KeyEvent.VK_DELETE || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					String sel = JTextFieldPercent.this.getSelectedText();
					if (sel != null && sel.equals(JTextFieldPercent.this.getText())) {
						JTextFieldPercent.this.setText("0.0");
					}
				}
			}
		});
	}

	private void setMask() {
		DecimalFormat df = new DecimalFormat(mask);
		if (mask.contains(".")) {
			df.setGroupingSize(3);
			df.setGroupingUsed(true);
		}
		df.setMaximumIntegerDigits(cantEnteros);
		df.setMaximumFractionDigits(cantDecimals);
		DefaultFormatter fmt = new NumberFormatter(df);
		fmt.setValueClass(Double.class);
		fmt.setOverwriteMode(false);
		fmt.setCommitsOnValidEdit(true);
		fmt.setAllowsInvalid(false);
		DefaultFormatterFactory fmtFactory = new DefaultFormatterFactory(fmt, fmt, fmt, fmt);
		fmtFactory.setEditFormatter(fmt);
		this.setFormatterFactory(fmtFactory);
	}

	// Workaround for formatted text field focus side effects.
	protected void selectItLater(Component c) {
		if (c instanceof JFormattedTextField) {
			final JFormattedTextField ftf = (JFormattedTextField) c;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					ftf.selectAll();
				}
			});
		}
	}

	public int getCantDecimals() {
		return cantDecimals;
	}

	public void setCantDecimals(int cantDecimals) {
		this.cantDecimals = cantDecimals;
		setMask();
	}

	public int getCantEnteros() {
		return cantEnteros;
	}

	public void setCantEnteros(int cantEnteros) {
		this.cantEnteros = cantEnteros;
		setMask();
	}

}
