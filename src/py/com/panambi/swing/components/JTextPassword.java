/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.swing.JPasswordField;
import javax.swing.text.Document;

/**
 *
 */
public class JTextPassword extends JPasswordField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3249166123829529376L;

	/**
	 * 
	 */
	public JTextPassword() {
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param text
	 */
	public JTextPassword(String text) {
		super(text);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param columns
	 */
	public JTextPassword(int columns) {
		super(columns);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param text
	 * @param columns
	 */
	public JTextPassword(String text, int columns) {
		super(text, columns);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param doc
	 * @param txt
	 * @param columns
	 */
	public JTextPassword(Document doc, String txt, int columns) {
		super(doc, txt, columns);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		this.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent e) {
				JTextPassword.this.selectAll();
			}
		});

		this.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JTextPassword.this.transferFocus();
				}
			}
		});
		this.setSize(new Dimension(100, getPreferredSize().height));
	}

}
