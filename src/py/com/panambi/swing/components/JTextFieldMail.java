package py.com.panambi.swing.components;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import py.com.panambi.swing.font.FontPanambi;

public class JTextFieldMail extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4115021043249399633L;

	public JTextFieldMail() {
		super();
		addVerifier();
		init();
	}

	public JTextFieldMail(int columns) {
		super(columns);
		addVerifier();
		init();
	}

	private void init() {
		setFont(FontPanambi.getLabelInstance());
		this.addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				JTextFieldMail.this.selectAll();
				super.focusGained(e);
			}
		});
	}

	private void addVerifier() {
		this.setInputVerifier(new EmailVerifier());
	}

	class EmailVerifier extends InputVerifier {

		public boolean verify(JComponent input) {
			boolean ret = true;
			String regx = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
			String text = JTextFieldMail.this.getText();
			if (text.equals("")) {
				return true;
			}

			if (text.indexOf(";") > -1) {
				String[] singlestxt = text.split(";");
				for (int i = 0; i < singlestxt.length; i++) {
					String singletxt = singlestxt[i].trim();
					if (!singletxt.matches(regx)) {
						JOptionPane.showMessageDialog(null, "Formato de e-mail inv�lido.[" + singletxt + "]");
						return false;
					}
				}
			} else {
				if (!text.matches(regx)) {
					JOptionPane.showMessageDialog(null, "Formato de e-mail inv�lido.[" + text + "]");
					return false;
				}
			}
			return ret;

			// String regx = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
			// String text = JTextFieldMail.this.getText();
			// if (text.equals("")){
			// return true;
			// }
			// if (text.matches(regx)) {
			// return true;
			// } else {
			// JOptionPane.showMessageDialog(null,
			// "Formato de e-mail inv�lido.");
			// return false;
			// }
		}
	}
}
