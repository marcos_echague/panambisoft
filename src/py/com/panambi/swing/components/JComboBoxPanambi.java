package py.com.panambi.swing.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import py.com.panambi.bean.PanambiBean;
import py.com.panambi.swing.font.FontPanambi;

@SuppressWarnings("rawtypes")
public class JComboBoxPanambi extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4907718438144763159L;

	public JComboBoxPanambi() {
		initialize();
	}

	@SuppressWarnings({ "serial", "unchecked" })
	private void initialize() {
		getEditor().getEditorComponent().setFont(FontPanambi.getLabelInstance());
		setFont(FontPanambi.getLabelInstance());
		setRenderer(new DefaultListCellRenderer() {
		        @Override
		        public void paint(Graphics g) {
		            setForeground(Color.BLACK);
		            super.paint(g);
		        }
		});
		((JTextField) getEditor().getEditorComponent()).setDocument(new javax.swing.text.PlainDocument() {
			public void insertString(int theOffset, String theString, AttributeSet theAttributes) throws BadLocationException {
				if (theString != null) {
					theString = theString.toUpperCase();
				}
				super.insertString(theOffset, theString, theAttributes);
			}
		});

		getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.getComponent().transferFocus();
				}
			}
		});
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JComboBoxPanambi.this.transferFocus();
				}
			}
		});

	}

	public void setSelectedPanambiObject(PanambiBean anObject) {
		for (int i = 0; i < getItemCount(); i++) {
			String descripcion = getItemAt(i).toString();
			if (descripcion != null && anObject != null && descripcion.equals(anObject.toString())) {
				setSelectedIndex(i);
				break;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public JComboBoxPanambi(ComboBoxModel aModel) {
		super(aModel);
		initialize();
	}

	@SuppressWarnings("unchecked")
	public JComboBoxPanambi(Object[] items) {
		super(items);
		initialize();
	}

	@SuppressWarnings("unchecked")
	public JComboBoxPanambi(Vector items) {
		super(items);
		initialize();
	}

}
