package py.com.panambi.swing.components;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.text.MaskFormatter;

import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.windows.JInternalFramePanambi;
import py.com.panambi.swing.windows.MessageWindow;

public class JTextFieldDate extends JFormattedTextFieldPanambi {

	private static final long serialVersionUID = 6778907973133793007L;
	private MaskFormatter mf = null;
	private Border original;
	private boolean requerida;
	String messageerror = null;
	MessageWindow mw = new MessageWindow("");
	private Collection<Component> componentes = new ArrayList<Component>(); // @jve:decl-index=0:

	public JTextFieldDate() {
		super();
		initialize();
	}

	public JTextFieldDate(AbstractFormatter formatter) {
		super(formatter);
		initialize();
	}

	public JTextFieldDate(AbstractFormatterFactory factory, Object currentValue) {
		super(factory, currentValue);
		initialize();
	}

	public JTextFieldDate(AbstractFormatterFactory factory) {
		super(factory);
		initialize();
	}

	public JTextFieldDate(Format format) {
		super(format);
		initialize();
	}

	public JTextFieldDate(Object value) {
		super(value);
		initialize();
	}

	private void initialize() {
		try {
			this.setRequerida(false);
			this.setFocusLostBehavior(JFormattedTextField.PERSIST);
			this.setInputVerifier(new FormattedTextDateVerifier());
			this.setPreferredSize(new Dimension(80, getPreferredSize().height));
			this.setFont(FontPanambi.getLabelInstance());
			this.setHorizontalAlignment(SwingConstants.CENTER);
			mf = new MaskFormatter("##/##/####");
			mf.setCommitsOnValidEdit(true);
			mf.setAllowsInvalid(false);
			mf.setOverwriteMode(true);
			mf.install(this);
			original = getBorder();
			this.addFocusListener(new FocusAdapter() {

				public void focusLost(FocusEvent e) {
					mf.install(JTextFieldDate.this);
					super.focusLost(e);
				}

				public void focusGained(FocusEvent e) {
					if (!e.isTemporary() && e.getComponent().isEnabled()) {
						mf.install(JTextFieldDate.this);
						((JFormattedTextField) e.getComponent()).selectAll();
						if (!JTextFieldDate.this.getText().contains("/")) {
							Robot robot;
							try {
								robot = new Robot();
								robot.keyPress(KeyEvent.VK_ENTER);
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
						super.focusGained(e);
					}
				}
			});
			this.addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						e.getComponent().transferFocus();
					}
					if (((JTextFieldDate)e.getComponent()).isEditable()) {
						if (e.getKeyCode() == KeyEvent.VK_UP && !e.isControlDown()) {
							if (getFecha() == null) {
								setFecha(new Date(System.currentTimeMillis()));
							} else {
								GregorianCalendar gc = new GregorianCalendar();
								gc.setTimeInMillis(getFecha().getTime());
								gc.add(Calendar.DAY_OF_MONTH, 1);
								setFecha(new Date(gc.getTimeInMillis()));
							}
						}
						if (e.getKeyCode() == KeyEvent.VK_DOWN && !e.isControlDown()) {
							if (getFecha() == null) {
								setFecha(new Date(System.currentTimeMillis()));
							} else {
								GregorianCalendar gc = new GregorianCalendar();
								gc.setTimeInMillis(getFecha().getTime());
								gc.add(Calendar.DAY_OF_MONTH, -1);
								setFecha(new Date(gc.getTimeInMillis()));
							}
						}
						if (e.getKeyCode() == KeyEvent.VK_UP && e.isControlDown()) {
							if (getFecha() != null) {
								GregorianCalendar gc = new GregorianCalendar();
								gc.setTimeInMillis(getFecha().getTime());
								gc.add(Calendar.MONTH, 1);
								setFecha(new Date(gc.getTimeInMillis()));
							}
						}
						if (e.getKeyCode() == KeyEvent.VK_DOWN && e.isControlDown()) {
							if (getFecha() != null) {
								GregorianCalendar gc = new GregorianCalendar();
								gc.setTimeInMillis(getFecha().getTime());
								gc.add(Calendar.MONTH, -1);
								setFecha(new Date(gc.getTimeInMillis()));
							}
						}
						if (e.getKeyCode() == KeyEvent.VK_DELETE) {
							if (JTextFieldDate.this.getSelectedText() == null) {
								e.consume();
								JTextFieldDate.this.dispatchEvent(new KeyEvent(JTextFieldDate.this, KeyEvent.KEY_PRESSED, 0, 0, KeyEvent.VK_RIGHT, KeyEvent.CHAR_UNDEFINED));
								JTextFieldDate.this.dispatchEvent(new KeyEvent(JTextFieldDate.this, KeyEvent.KEY_PRESSED, 0, 0, KeyEvent.VK_BACK_SPACE, KeyEvent.CHAR_UNDEFINED));
							}
						}
					}
					if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						if (!JTextFieldDate.this.getText().contains("/")) {
							Robot robot;
							try {
								robot = new Robot();
								robot.keyPress(KeyEvent.VK_ENTER);
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					}
				}

			});
			this.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getPropertyName().equals("enabled")) {
						if (!JTextFieldDate.this.isEnabled()) {
							JTextFieldDate.this.setBorder(original);
							if (mw != null && mw.isVisible()) {
								mw.setVisible(false);
							}
						}
					}
				}

			});
			GregorianCalendar gcc = new GregorianCalendar();
			setFecha(new Date(gcc.getTimeInMillis()));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setFecha(java.util.Date fecha) {
		if (fecha != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			setValue(df.format(fecha));
		} else {
			setValue(null);
		}
		mf.install(this);
	}

	public void setFecha(Timestamp fecha) {
		if (fecha != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			setValue(df.format(fecha));
		} else {
			setValue(null);
		}
		mf.install(this);

	}

	public Date getFecha() {
		String text = (String) getValue();
		if (text == null) {
			return null;
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setLenient(false);
		int i1 = text.indexOf('/');
		int i2 = text.indexOf('/', i1 + 1);
		String sdia = text.substring(0, i1);
		String smes = text.substring(i1 + 1, i2);
		String sanho = text.substring(i2 + 1, text.length());
		if (sdia.trim().equals("") || smes.equals("") || sanho.equals("")) {
			return null;
		} else {
			int dia = Integer.valueOf(sdia.replaceAll(" ", ""));
			int mes = Integer.valueOf(smes.replaceAll(" ", ""));
			int anho = Integer.valueOf(sanho.replaceAll(" ", ""));
			if (anho >= 1 && anho < 100) {
				anho = 2000 + anho;
			}
			gc.set(Calendar.DAY_OF_MONTH, dia);
			gc.set(Calendar.MONTH, mes - 1);
			gc.set(Calendar.YEAR, anho);
			gc.set(Calendar.HOUR_OF_DAY, 0);
			gc.set(Calendar.MINUTE, 0);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			Date d = new Date(gc.getTimeInMillis());
			return d;
		}
	}

	class FormattedTextDateVerifier extends InputVerifier {

		public boolean verify(JComponent input) {
			boolean ret = true;
			if (input instanceof JFormattedTextField) {
				setArrayComponents(JTextFieldDate.this.getRootPane().getParent());
				JFormattedTextField ftf = (JFormattedTextField) input;
				String text = ftf.getText();
				ret = verificarFecha(text);
				setEnabledComponents(ret);
			}
			return ret;
		}

		public boolean shouldYieldFocus(JComponent input) {
			if (!input.isEnabled()) {
				return true;
			}
			boolean ret = verify(input);
			if (!ret && !messageerror.equals("")) {
				mw.setLocation(getLocationOnScreen().x + getWidth() + 3, getLocationOnScreen().y);
				mw.setMessage(messageerror);
				mw.pack();
				mw.setVisible(true);
				mw.setFocusable(false);
				mw.setFocusableWindowState(false);
				setMessageToParent();
			} else {
				mf.install(JTextFieldDate.this);
				if (mw != null) {
					mw.setVisible(false);
				}
			}
			return ret;
		}
	}

	private boolean verificarFecha(String text) {
		boolean ret = true;
		try {
			messageerror = null;
			Calendar gc = Calendar.getInstance();
			gc.setLenient(false);
			int i1 = text.indexOf('/');
			int i2 = text.indexOf('/', i1 + 1);
			String sdia = text.substring(0, i1);
			String smes = text.substring(i1 + 1, i2);
			String sanho = text.substring(i2 + 1, text.length());
			if (sdia.trim().equals("") || smes.trim().equals("") || sanho.trim().equals("")) {
				if (isRequerida()) {
					messageerror = "La fecha es requerida";
					this.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.red, 2));
				} else {
					setValue(null);
					this.setBorder(original);
				}
			} else {
				int dia = Integer.valueOf(sdia.replaceAll(" ", ""));
				int mes = Integer.valueOf(smes.replaceAll(" ", ""));
				int anho = Integer.valueOf(sanho.replaceAll(" ", ""));
				if (anho >= 0 && anho < 100) {
					anho = 2000 + anho;
				} else if (anho >= 100 && anho < 1000) {
					anho = 1000 + anho;
				}
				gc.set(Calendar.DAY_OF_MONTH, dia);
				gc.set(Calendar.MONTH, (mes - 1));
				gc.set(Calendar.YEAR, anho);
				gc.set(Calendar.HOUR_OF_DAY, 0);
				gc.set(Calendar.MINUTE, 0);
				gc.set(Calendar.SECOND, 0);
				gc.set(Calendar.MILLISECOND, 0);
				Date d = new Date(gc.getTimeInMillis());
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				messageerror = valid(d);
				if (messageerror == null) {
					setValue(df.format(d));
					this.setBorder(original);
				}
			}
			if (messageerror != null) {
				this.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.red, 2));
				this.selectAll();
				ret = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			messageerror = "Fecha inv�lida";
			getToolkit().beep();
			this.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.red, 2));
			mf.install(this);
				if (!this.getText().contains("/")) {
					JTextFieldDate.this.dispatchEvent(new KeyEvent(JTextFieldDate.this, KeyEvent.KEY_PRESSED, 0, 0, KeyEvent.VK_ENTER, KeyEvent.CHAR_UNDEFINED));
				}
			return false;
		}
		return ret;
	}

	public void setMessageToParent() {
		((JInternalFramePanambi) ((JPanel) getParent()).getRootPane().getParent()).setMessageWindow(mw);
	}

	public boolean isRequerida() {
		return requerida;
	}

	public void setRequerida(boolean requerida) {
		this.requerida = requerida;
	}

	

	public String valid(Date fecha) {
		return null;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}

	private void setEnabledComponents(boolean b) {
		for (Component comp : componentes) {
			comp.setEnabled(b);
		}
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	public void setArrayComponents(Container c) {
		Component Component[] = c.getComponents();
		for (int i = 0; i < Component.length; i++) {
			Component comp = Component[i];
			if (!comp.equals(this) && comp instanceof JButton) {
				if (comp.isEnabled()) {
					componentes.add(comp);
				}
			}
			if (Component[i] instanceof java.awt.Container) {
				setArrayComponents((Container) Component[i]);
			}
		}
	}
	/**
	 * Setea la fecha del d�a de la pc cliente
	 */
	public void setCurrentDate(){
		setFecha(new java.util.Date());
	}

}