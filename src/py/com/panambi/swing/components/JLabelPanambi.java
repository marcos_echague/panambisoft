/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.JLabel;

import py.com.panambi.swing.font.FontPanambi;

/**
 * @author Administrador
 * 
 */
public class JLabelPanambi extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2496685866454105255L;

	/**
	 * 
	 */
	public JLabelPanambi() {
		initialize();
	}

	/**
	 * @param text
	 */
	public JLabelPanambi(String text) {
		super(text);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param image
	 */
	public JLabelPanambi(Icon image) {
		super(image);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param text
	 * @param horizontalAlignment
	 */
	public JLabelPanambi(String text, int horizontalAlignment) {
		super(text, horizontalAlignment);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param image
	 * @param horizontalAlignment
	 */
	public JLabelPanambi(Icon image, int horizontalAlignment) {
		super(image, horizontalAlignment);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param text
	 * @param icon
	 * @param horizontalAlignment
	 */
	public JLabelPanambi(String text, Icon icon, int horizontalAlignment) {
		super(text, icon, horizontalAlignment);
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		this.setFont(FontPanambi.getLabelInstance());
		this.setPreferredSize(new Dimension(100, 20));
	}

	@Override
	public void setPreferredSize(Dimension d) {
		super.setPreferredSize(d);
		this.setMinimumSize(d);
	}

}
