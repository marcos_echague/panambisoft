/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * 
 */
public class JPanelTittle extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel lblLabelLeft;
	private JLabel lblLabelCenter;
	private JLabel lblLabelRight;

	/**
	 * 
	 */
	public JPanelTittle() {
		// TODO Auto-generated constructor stub
		super();
		initialize();
	}

	/**
	 * @param layout
	 */
	public JPanelTittle(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param isDoubleBuffered
	 */
	public JPanelTittle(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public JPanelTittle(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(1920, 100));
		setMaximumSize(new Dimension(1920, 100));
		setLayout(new BorderLayout(0, 0));
		add(getLblLabelLeft(), BorderLayout.WEST);
		add(getLblLabelCenter(), BorderLayout.CENTER);
		add(getLblLabelRight(), BorderLayout.EAST);
	}

	private JLabel getLblLabelLeft() {
		if (lblLabelLeft == null) {
			lblLabelLeft = new JLabel("");
			lblLabelLeft.setIcon(new ImageIcon(JPanelTittle.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		}
		return lblLabelLeft;
	}
	private JLabel getLblLabelCenter() {
		if (lblLabelCenter == null) {
			lblLabelCenter = new JLabel("");
			lblLabelCenter.setHorizontalAlignment(SwingConstants.CENTER);
			lblLabelCenter.setIcon(new ImageIcon(JPanelTittle.class.getResource("/py/com/panambi/images/imagenCentral.png")));
		}
		return lblLabelCenter;
	}
	private JLabel getLblLabelRight() {
		if (lblLabelRight == null) {
			lblLabelRight = new JLabel("");
			lblLabelRight.setIcon(new ImageIcon(JPanelTittle.class.getResource("/py/com/panambi/images/mariposaDerecha.png")));
			lblLabelRight.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblLabelRight;
	}
} 
