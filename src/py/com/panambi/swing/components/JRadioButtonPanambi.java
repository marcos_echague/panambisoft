/**
 * 
 */
package py.com.panambi.swing.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JRadioButton;

import py.com.panambi.swing.font.FontPanambi;

/**
 * @author Administrador
 * 
 */
public class JRadioButtonPanambi extends JRadioButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4604333918778786612L;

	/**
	 * 
	 */
	public JRadioButtonPanambi() {
		super();
		initialize();
	}

	/**
	 * @param icon
	 */
	public JRadioButtonPanambi(Icon icon) {
		super(icon);
		initialize();
	}

	/**
	 * @param a
	 */
	public JRadioButtonPanambi(Action a) {
		super(a);
		initialize();
	}

	/**
	 * @param text
	 */
	public JRadioButtonPanambi(String text) {
		super(text);
		initialize();
	}

	/**
	 * @param icon
	 * @param selected
	 */
	public JRadioButtonPanambi(Icon icon, boolean selected) {
		super(icon, selected);
		initialize();
	}

	/**
	 * @param text
	 * @param selected
	 */
	public JRadioButtonPanambi(String text, boolean selected) {
		super(text, selected);
		initialize();
	}

	/**
	 * @param text
	 * @param icon
	 */
	public JRadioButtonPanambi(String text, Icon icon) {
		super(text, icon);
		initialize();
	}

	/**
	 * @param text
	 * @param icon
	 * @param selected
	 */
	public JRadioButtonPanambi(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		initialize();
	}

	private void initialize() {
		this.setFont(FontPanambi.getLabelInstance());
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					JRadioButtonPanambi.this.transferFocus();
				}
			}
		});

	}

}
