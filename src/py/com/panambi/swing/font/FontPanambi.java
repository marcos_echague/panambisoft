/**
 * 
 */
package py.com.panambi.swing.font;

import java.awt.Font;

/**
 *
 */
public class FontPanambi {

	public static Font getLabelInstance() {
		return new Font("Dialog", Font.PLAIN, 11);
	}
	
	public static Font getTableInstance() {
		return new Font("Dialog", Font.PLAIN, 10);
	}
	
	public static Font getTittleInstance() {
		return new Font("Dialog", Font.BOLD, 11);
	}
	
	public static Font getLinkInstance() {
		return new Font("Dialog", Font.BOLD, 10);
	}

}
