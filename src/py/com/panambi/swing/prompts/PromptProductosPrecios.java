package py.com.panambi.swing.prompts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Producto;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;
import java.awt.Toolkit;

public class PromptProductosPrecios extends JDialogPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8951887626144995739L;
	private final JPanel contentPanel = new JPanel();
	private JLabelPanambi lblpnmbCdigo;
	private JTextFieldInteger jTxtCodProducto;
	private JLabelPanambi lblpnmbNombreProducto;
	private JTextFieldUpper jTxtNombreProducto;
	private JScrollPane scrollPane;
	private JTablePanambi table;
	private JButtonPanambi jBtnBuscar;
	private JCheckBoxPanambi jChkActivos;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private Browseable browseable;

	/**
	 * Create the dialog.
	 */
	public PromptProductosPrecios(Browseable browseable) {
		this.browseable = browseable;
		initialize();
	}

	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptProductosPrecios.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Prompt Productos");
		setBounds(100, 100, 852, 595);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getJChkActivos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(249)
							.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(102))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtNombreProducto(), GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)))
							.addGap(89))
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 781, GroupLayout.PREFERRED_SIZE))
					.addGap(31))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJChkActivos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addComponent(getScrollPane(), 0, 0, Short.MAX_VALUE)
					.addGap(71))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		{
			JPanel panelTitle = new JPanel();
			panelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(panelTitle, BorderLayout.NORTH);
			{
				JLabel lblNewLabel = new JLabel("");
				lblNewLabel.setIcon(new ImageIcon(PromptProductosPrecios.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
				panelTitle.add(lblNewLabel);
			}
		}
		doCheckDisponibles();
		getJChkActivos().setSelected(true);
		centerIt();
		setShortcutsHere((JComponent) getContentPane());
	}

	private JLabelPanambi getLblpnmbCdigo() {
		if (lblpnmbCdigo == null) {
			lblpnmbCdigo = new JLabelPanambi();
			lblpnmbCdigo.setText("C\u00F3digo Producto");
		}
		return lblpnmbCdigo;
	}

	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
		}
		return jTxtCodProducto;
	}

	private JLabelPanambi getLblpnmbNombreProducto() {
		if (lblpnmbNombreProducto == null) {
			lblpnmbNombreProducto = new JLabelPanambi();
			lblpnmbNombreProducto.setText("Nombre Producto");
		}
		return lblpnmbNombreProducto;
	}

	private JTextFieldUpper getJTxtNombreProducto() {
		if (jTxtNombreProducto == null) {
			jTxtNombreProducto = new JTextFieldUpper();
		}
		return jTxtNombreProducto;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTablePanambi getTable() {
		if (table == null) {
			String[] columns = { "Codigo", "Producto", "Precio de costo", "Cod. Proveedor","Razon Social"};
			table = new JTablePanambi(columns);
			table.getColumnModel().getColumn(0).setPreferredWidth(100);
			table.getColumnModel().getColumn(1).setPreferredWidth(300);
			table.getColumnModel().getColumn(2).setPreferredWidth(100);
			table.getColumnModel().getColumn(3).setPreferredWidth(100);
			table.getColumnModel().getColumn(4).setPreferredWidth(300);
						
		}
		return table;
	}

	private JButtonPanambi getJBtnBuscar() {
		if (jBtnBuscar == null) {
			jBtnBuscar = new JButtonPanambi();
			jBtnBuscar.setText("Buscar (F5)");
			jBtnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
		}
		return jBtnBuscar;
	}

	private void doBuscar() {
		try {
			getTable().resetData(0);
			List<String[]> productos = controladorProducto.getPreciosProductosByFilters(JFramePanambiMain.session.getConn(), getJTxtCodProducto().getValor(), (!getJTxtNombreProducto().getText().trim().isEmpty() ? getJTxtNombreProducto().getText().trim() : null), getJChkActivos().isSelected());
			int i = 0;
			
			for (String[] producto : productos) {
				getTable().addRow();
				getTable().setValueAt(producto[0], i, 0);
				getTable().setValueAt(producto[1], i, 1);
				getTable().setValueAt(producto[2], i, 2);
				getTable().setValueAt(producto[3], i, 3);
				getTable().setValueAt(producto[4], i, 4);
				i++;
			}
			if (getTable().getRowCount() > 0) {
				getTable().requestFocus();
				getTable().changeSelection(0, 0, false, false);
			}else{
				DlgMessage.showMessage(getOwner(), "No existe el producto de acuerdo a los parámetros ingresados", DlgMessage.INFORMATION_MESSAGE);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((Producto)getTable().getValueAt(getTable().getSelectedRow(), 3), null);
		}
		this.dispose();
	}

	private JCheckBoxPanambi getJChkActivos() {
		if (jChkActivos == null) {
			jChkActivos = new JCheckBoxPanambi();
			jChkActivos.setMnemonic('A');
			jChkActivos.setText("Solo Activos");
		}
		return jChkActivos;
	}

	private void doCheckDisponibles() {
		getJChkActivos().setSelected(true);
	}
}
