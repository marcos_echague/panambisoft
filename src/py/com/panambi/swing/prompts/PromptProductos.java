package py.com.panambi.swing.prompts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;
import javax.swing.SwingConstants;
import java.awt.Toolkit;

public class PromptProductos extends JDialogPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8951887626144995739L;
	private final JPanel contentPanel = new JPanel();
	private JLabelPanambi lblpnmbCdigo;
	private JTextFieldInteger jTxtCodProducto;
	private JLabelPanambi lblpnmbNombreProducto;
	private JTextFieldUpper jTxtNombreProducto;
	private JScrollPane scrollPane;
	private JTablePanambi table;
	private JPanel panelSeleccion;
	private JLabel lblImage;
	private JLabelPanambi lblpnmbDisponibleEn;
	private JButtonPanambi jBtnBuscar;
	private JCheckBoxPanambi jChkActivos;
	private JCheckBoxPanambi jChkDisponibles;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private JComboBoxPanambi jCmbSucursal;
	private Browseable browseable;
	private JLabelPanambi jLabelDisponibleEn;

	/**
	 * Create the dialog.
	 */
	public PromptProductos(Browseable browseable) {
		this.browseable = browseable;
		initialize();
	}

	private void initialize() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptProductos.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Prompt Productos");
		setBounds(100, 100, 985, 595);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtNombreProducto(), GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(getJChkDisponibles(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJChkActivos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPanel.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addGap(12))
										.addGroup(gl_contentPanel.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
							.addGap(89))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 621, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addComponent(getPanel_1_1(), GroupLayout.PREFERRED_SIZE, 274, GroupLayout.PREFERRED_SIZE)
					.addGap(19))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(49)
							.addComponent(getPanel_1_1(), GroupLayout.PREFERRED_SIZE, 298, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(15)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJChkDisponibles(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(8)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJChkActivos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getScrollPane(), 0, 0, Short.MAX_VALUE)))
					.addGap(71))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		{
			JPanel panelTitle = new JPanel();
			panelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(panelTitle, BorderLayout.NORTH);
			{
				JLabel lblNewLabel = new JLabel("");
				lblNewLabel.setIcon(new ImageIcon(PromptProductos.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
				panelTitle.add(lblNewLabel);
			}
		}
		doCheckDisponibles();
		getJChkActivos().setSelected(true);
		centerIt();
		setShortcutsHere((JComponent) getContentPane());
		getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa());
		getJChkDisponibles().setSelected(true);
		getJCmbSucursal().setEnabled(true);
	}

	private JLabelPanambi getLblpnmbCdigo() {
		if (lblpnmbCdigo == null) {
			lblpnmbCdigo = new JLabelPanambi();
			lblpnmbCdigo.setText("C\u00F3digo Producto");
		}
		return lblpnmbCdigo;
	}

	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
		}
		return jTxtCodProducto;
	}

	private JLabelPanambi getLblpnmbNombreProducto() {
		if (lblpnmbNombreProducto == null) {
			lblpnmbNombreProducto = new JLabelPanambi();
			lblpnmbNombreProducto.setText("Nombre Producto");
		}
		return lblpnmbNombreProducto;
	}

	private JTextFieldUpper getJTxtNombreProducto() {
		if (jTxtNombreProducto == null) {
			jTxtNombreProducto = new JTextFieldUpper();
		}
		return jTxtNombreProducto;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTablePanambi getTable() {
		if (table == null) {
			String[] columns = { "Codigo", "Producto", "Precio", "Disponible En", "Data" };
			table = new JTablePanambi(columns);
			table.getColumnModel().getColumn(0).setPreferredWidth(60);
			table.getColumnModel().getColumn(1).setPreferredWidth(300);
			table.getColumnModel().getColumn(2).setPreferredWidth(100);
			table.getColumnModel().getColumn(3).setPreferredWidth(300);
			table.getColumnModel().getColumn(4).setPreferredWidth(0);
			table.getColumnModel().getColumn(4).setMinWidth(0);
			table.getColumnModel().getColumn(4).setMaxWidth(0);
			table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (table.getSelectedRow() > -1) {
						actualizarDetalleProducto((Producto) getTable().getValueAt(getTable().getSelectedRow(), 4));
					}

				}
			});
		}
		return table;
	}

	private void actualizarDetalleProducto(Producto producto) {
		// Actualiza imagen
		try {
			if (producto != null) {
				InputStream in = null;
				if (producto.getImagenProducto() != null) {
					try {
						in = new ByteArrayInputStream(producto.getImagenProducto().getImagen());
						BufferedImage image = ImageIO.read(in);
						getLblImage().setIcon(new ImageIcon(image.getScaledInstance(getLblImage().getWidth(), getLblImage().getHeight(), Image.SCALE_SMOOTH)));
						
					} catch (Exception e) {
						e.printStackTrace();
						getLblImage().setIcon(null);
						JOptionPane.showMessageDialog(this, "No se pudo obtener imagen");
					}
				} else {
					getLblImage().setIcon(null);
				}
				// Actualiza disponibilidad
				List<Sucursal> sucursales = controladorProducto.getSucursalesDisponibles(JFramePanambiMain.session.getConn(), producto);
				String disponibles = "";
				for (Sucursal sucursal : sucursales) {
					disponibles += sucursal.getNombre() + ", ";
				}
				getJLabelDisponibleEn().setText(disponibles.substring(0, disponibles.length() - 2));
			}else{
				getLblImage().setIcon(null);
				getJLabelDisponibleEn().setText(null);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private JPanel getPanel_1_1() {
		if (panelSeleccion == null) {
			panelSeleccion = new JPanel();
			panelSeleccion.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Producto Seleccionado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panelSeleccion = new GroupLayout(panelSeleccion);
			gl_panelSeleccion.setHorizontalGroup(
				gl_panelSeleccion.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelSeleccion.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panelSeleccion.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelSeleccion.createSequentialGroup()
								.addGap(10)
								.addComponent(getJLabelDisponibleEn(), GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE))
							.addComponent(getLblpnmbDisponibleEn(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblImage(), GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE))
						.addContainerGap())
			);
			gl_panelSeleccion.setVerticalGroup(
				gl_panelSeleccion.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelSeleccion.createSequentialGroup()
						.addContainerGap()
						.addComponent(getLblImage(), GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getLblpnmbDisponibleEn(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJLabelDisponibleEn(), GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			panelSeleccion.setLayout(gl_panelSeleccion);
		}
		return panelSeleccion;
	}

	private JLabel getLblImage() {
		if (lblImage == null) {
			lblImage = new JLabel("");
			lblImage.setBorder(new LineBorder(Color.GRAY));
		}
		return lblImage;
	}

	private JLabelPanambi getLblpnmbDisponibleEn() {
		if (lblpnmbDisponibleEn == null) {
			lblpnmbDisponibleEn = new JLabelPanambi();
			lblpnmbDisponibleEn.setText("Disponible en:");
		}
		return lblpnmbDisponibleEn;
	}

	private JButtonPanambi getJBtnBuscar() {
		if (jBtnBuscar == null) {
			jBtnBuscar = new JButtonPanambi();
			jBtnBuscar.setText("Buscar (F5)");
			jBtnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
		}
		return jBtnBuscar;
	}

	private void doBuscar() {
		try {
			getTable().resetData(0);
			Integer codsucursal = null;
			if (getJCmbSucursal().isEnabled() && getJCmbSucursal().getSelectedIndex() > 0) {
				codsucursal = ((Sucursal) getJCmbSucursal().getSelectedItem()).getCodSucursal();
			}
			List<Producto> productos = controladorProducto.getProductosByFilters(JFramePanambiMain.session.getConn(), getJTxtCodProducto().getValor(), (!getJTxtNombreProducto().getText().trim().isEmpty() ? getJTxtNombreProducto().getText().trim() : null), getJChkDisponibles().isSelected(),
					codsucursal, getJChkActivos().isSelected());
			int i = 0;
			for (Producto producto : productos) {
				getTable().addRow();
				getTable().setValueAt(producto.getCodProducto(), i, 0);
				getTable().setValueAt(producto.getDescripcion(), i, 1);
				getTable().setValueAt(producto.getPrecio(), i, 2);
				List<Sucursal> sucursalesDisponibles = controladorProducto.getSucursalesDisponibles(JFramePanambiMain.session.getConn(), producto);
				String sucurDisponibles = "";
				for (Sucursal sucursal : sucursalesDisponibles) {
					sucurDisponibles += sucursal.getNombre() + ", ";
				}
				if (!sucursalesDisponibles.isEmpty()) {
					sucurDisponibles = sucurDisponibles.substring(0, sucurDisponibles.length() - 2);
				}else{
					sucurDisponibles = "NINGUNA";
				}
				getTable().setValueAt(sucurDisponibles, i, 3);
				getTable().setValueAt(producto, i, 4);
				i++;
			}
			if (getTable().getRowCount() > 0) {
				getTable().requestFocus();
				getTable().changeSelection(0, 0, false, false);
			}else{
				actualizarDetalleProducto(null);
				DlgMessage.showMessage(getOwner(), "No existe el producto de acuerdo a los parámetros ingresados", DlgMessage.INFORMATION_MESSAGE);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((Producto)getTable().getValueAt(getTable().getSelectedRow(), 4), null);
		}
		this.dispose();
	}

	private JCheckBoxPanambi getJChkActivos() {
		if (jChkActivos == null) {
			jChkActivos = new JCheckBoxPanambi();
			jChkActivos.setMnemonic('A');
			jChkActivos.setText("Solo Activos");
		}
		return jChkActivos;
	}

	private JCheckBoxPanambi getJChkDisponibles() {
		if (jChkDisponibles == null) {
			jChkDisponibles = new JCheckBoxPanambi();
			jChkDisponibles.setMnemonic('D');
			jChkDisponibles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCheckDisponibles();
				}
			});
			jChkDisponibles.setText("Solo Disponibles en");
		}
		return jChkDisponibles;
	}

	private void doCheckDisponibles() {
		getJCmbSucursal().setSelectedIndex(0);
		getJCmbSucursal().setEnabled(getJChkDisponibles().isSelected());
		getJChkActivos().setEnabled(!getJChkDisponibles().isSelected());
		getJChkActivos().setSelected(true);
	}

	@SuppressWarnings("all")
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			try {
				jCmbSucursal = new JComboBoxPanambi();
				List<Sucursal> sucursales = new ArrayList<Sucursal>();
				Sucursal sucursal = new Sucursal();
				sucursal.setNombre("TODAS");
				sucursal.setCodSucursal(0);
				sucursal.setEstado("A");
				sucursal.setDireccion("NA");
				sucursales.add(sucursal);
				ControladorSucursal controladorSucursal = new ControladorSucursal();
				sucursales.addAll(controladorSucursal.getSucursalesActivas(JFramePanambiMain.session.getConn()));
				jCmbSucursal.setModel(new ListComboBoxModel(sucursales));
				
				
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			jCmbSucursal.setSelectedIndex(0);

		}
		return jCmbSucursal;
	}

	private JLabelPanambi getJLabelDisponibleEn() {
		if (jLabelDisponibleEn == null) {
			jLabelDisponibleEn = new JLabelPanambi();
			jLabelDisponibleEn.setVerticalAlignment(SwingConstants.TOP);
		}
		return jLabelDisponibleEn;
	}
}
