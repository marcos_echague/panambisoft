package py.com.panambi.swing.prompts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.MovimientoProducto;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorMovimientoProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;

import java.awt.Toolkit;

public class PromptMovimientos extends JDialogPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8951887626144995739L;
	private final JPanel contentPanel = new JPanel();
	private JScrollPane scrollPane;
	private JTablePanambi table;
	private JButtonPanambi jBtnBuscar;
	private ControladorMovimientoProducto controladorMovimiento = new ControladorMovimientoProducto();
	private Browseable browseable;
	private JComboBoxPanambi JCmbSucursalOrigen;
	private JLabelPanambi lblpnmbSucursalOrigen;
	private JLabelPanambi lblpnmbSucursalDestino;
	private JComboBoxPanambi JCmbSucursalDestino;
	private JComboBoxPanambi JCmbEstado;
	private JLabelPanambi lblpnmbEstado;
	private JTextFieldDate JTxtFechaDesde;
	private JTextFieldDate JTxtFechaHasta;
	private JCheckBoxPanambi chckbxpnmbFechaDesde;
	private JCheckBoxPanambi chckbxpnmbFechaHasta;

	/**
	 * Create the dialog.
	 */
	public PromptMovimientos(Browseable browseable) {
		this.browseable = browseable;
		initialize();
	}

	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptMovimientos.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Prompt Movimiento de productos");
		setBounds(100, 100, 852, 595);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
						.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJCmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getChckbxpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getJTxtFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(184)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getChckbxpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(getJCmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(getJTxtFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getChckbxpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getChckbxpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(19)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(14)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(6)
							.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(48, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		{
			JPanel panelTitle = new JPanel();
			panelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(panelTitle, BorderLayout.NORTH);
			{
				JLabel lblNewLabel = new JLabel("");
				lblNewLabel.setIcon(new ImageIcon(PromptMovimientos.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
				panelTitle.add(lblNewLabel);
			}
		}

		centerIt();
		setShortcutsHere((JComponent) getContentPane());
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTablePanambi getTable() {
		if (table == null) {
			String[] columns = { "Nota remision", "Fecha","Estado","Sucursal de origen","Sucursal de destino", "Comentarios", "Data" };
			table = new JTablePanambi(columns);
			table.getColumnModel().getColumn(0).setPreferredWidth(30);
			table.getColumnModel().getColumn(1).setPreferredWidth(150);
			table.getColumnModel().getColumn(2).setPreferredWidth(50);
			table.getColumnModel().getColumn(3).setPreferredWidth(100);
			table.getColumnModel().getColumn(4).setPreferredWidth(100);
			table.getColumnModel().getColumn(5).setPreferredWidth(100);
			table.getColumnModel().getColumn(6).setPreferredWidth(0);
			table.getColumnModel().getColumn(6).setMinWidth(0);
			table.getColumnModel().getColumn(6).setMaxWidth(0);
		/*	table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (table.getSelectedRow() > -1) {
					}

				}
			});*/
		}
		return table;
	}

	private JButtonPanambi getJBtnBuscar() {
		if (jBtnBuscar == null) {
			jBtnBuscar = new JButtonPanambi();
			jBtnBuscar.setText("Buscar (F5)");
			jBtnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
		}
		return jBtnBuscar;
	}

	private void doBuscar() {
		try {
			getTable().resetData(0);
			Integer codsucursalOrigen = null;
			Integer codsucursalDestino = null;
			String estado = "";
			String fechaDesde = "";
			String fechaHasta = "";
			
			if (getJCmbSucursalOrigen().isEnabled() && getJCmbSucursalOrigen().getSelectedIndex() > 0) {
				codsucursalOrigen = ((Sucursal) getJCmbSucursalOrigen().getSelectedItem()).getCodSucursal();
			}
			
			//if (getJCmbSucursalDestino().isEnabled() && getJCmbSucursalDestino().getSelectedIndex() > 0) {
			if (getJCmbSucursalDestino().getSelectedIndex() > 0) {
				codsucursalDestino = ((Sucursal) getJCmbSucursalDestino().getSelectedItem()).getCodSucursal();
			}
			
			if (getJTxtFechaDesde().isEnabled()) {
				fechaDesde = getJTxtFechaDesde().getText();
			}
			
			if (getJTxtFechaHasta().isEnabled()) {
				fechaHasta = getJTxtFechaHasta().getText();
			}
			
			if (getJCmbEstado().getSelectedIndex() > 0) {
				estado = getJCmbEstado().getSelectedItem().toString();
				
				if(estado == "TODOS"){
					estado = "";
				}else if(estado == "EN TRANSITO"){
					estado = "ET";
				}else if(estado == "ACEPTADO"){
					estado = "AC";
				}else if(estado == "RECHAZADO"){
					estado = "RE";
				}else if(estado == "ANULADO"){
					estado = "AN";
				}
			}
			
			List<MovimientoProducto> movimientos = controladorMovimiento.getMovimientosByFilters(JFramePanambiMain.session.getConn(), codsucursalOrigen,codsucursalDestino, estado, fechaDesde, fechaHasta);
			int i = 0;
			for (MovimientoProducto movimiento : movimientos) {
				getTable().addRow();
				getTable().setValueAt(movimiento.getNotaRemision(), i, 0);
				getTable().setValueAt(movimiento.getFecha(), i, 1);
				
				String estadoMov = "";
				
				if(movimiento.getEstado().equals("ET")){
					estadoMov = "EN TRANSITO";
				}else if (movimiento.getEstado().equals("AC")){
					estadoMov = "ACEPTADO";
				}else if (movimiento.getEstado().equals("RE")){
					estadoMov = "RECHAZADO";
				}else if (movimiento.getEstado().equals("AN")){
					estadoMov = "ANULADO";
				}else {
					estadoMov = "NO SE RECONOCE EL ESTADO";
				}
				getTable().setValueAt(estadoMov, i, 2);
				getTable().setValueAt(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), movimiento.getCodSucursalOrigen()).getNombre(), i, 3);
				getTable().setValueAt(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),movimiento.getCodSucursalDestino()).getNombre(), i, 4);
				getTable().setValueAt(movimiento.getComentarios(), i, 5);
				getTable().setValueAt(movimiento, i, 6);
				i++;
			}
			if (getTable().getRowCount() > 0) {
				getTable().requestFocus();
				getTable().changeSelection(0, 0, false, false);
			}else{
				DlgMessage.showMessage(getOwner(), "No se han encontrado resultados", DlgMessage.INFORMATION_MESSAGE);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((MovimientoProducto)getTable().getValueAt(getTable().getSelectedRow(), 6), null);
		}
		this.dispose();
	}
	
	@SuppressWarnings("all")
	private JComboBoxPanambi getJCmbSucursalOrigen() {
		if (JCmbSucursalOrigen == null) {
			try {
				JCmbSucursalOrigen = new JComboBoxPanambi();
				List<Sucursal> sucursales = new ArrayList<Sucursal>();
				Sucursal sucursal = new Sucursal();
				sucursal.setCodSucursal(0);
				sucursal.setNombre("TODAS");
				sucursal.setDireccion("NA");
				sucursal.setEstado("A");
				sucursal.setTelefono("0000");
				sucursal.setPrincipal(false);
				sucursales.add(sucursal);
				ControladorSucursal controladorSucursal = new ControladorSucursal();
				sucursales.addAll(controladorSucursal.getSucursales(JFramePanambiMain.session.getConn()));
				JCmbSucursalOrigen.setModel(new ListComboBoxModel(sucursales));
				JCmbSucursalOrigen.setSelectedIndex(0);
				JCmbSucursalOrigen.setEnabled(true);
	
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}

		}
		return JCmbSucursalOrigen;
	}
	private JLabelPanambi getLblpnmbSucursalOrigen() {
		if (lblpnmbSucursalOrigen == null) {
			lblpnmbSucursalOrigen = new JLabelPanambi();
			lblpnmbSucursalOrigen.setText("Sucursal Origen");
		}
		return lblpnmbSucursalOrigen;
	}
	private JLabelPanambi getLblpnmbSucursalDestino() {
		if (lblpnmbSucursalDestino == null) {
			lblpnmbSucursalDestino = new JLabelPanambi();
			lblpnmbSucursalDestino.setText("Sucursal Destino");
		}
		return lblpnmbSucursalDestino;
	}
	
	@SuppressWarnings("all")
	private JComboBoxPanambi getJCmbSucursalDestino() {
		if (JCmbSucursalDestino == null) {
			try {
				JCmbSucursalDestino = new JComboBoxPanambi();
				List<Sucursal> sucursales = new ArrayList<Sucursal>();
				Sucursal sucursal = new Sucursal();
				sucursal.setCodSucursal(0);
				sucursal.setNombre("TODAS");
				sucursal.setDireccion("NA");
				sucursal.setEstado("A");
				sucursal.setTelefono("0000");
				sucursales.add(sucursal);
				Sucursal sucursalOrigen = JFramePanambiMain.session.getSucursalOperativa();
				ControladorSucursal controladorSucursal = new ControladorSucursal();
				sucursales.addAll(controladorSucursal.getSucursales(JFramePanambiMain.session.getConn()));
				JCmbSucursalDestino.setModel(new ListComboBoxModel(sucursales));
				JCmbSucursalDestino.setSelectedItem(JFramePanambiMain.session.getSucursalOperativa());
				//JCmbSucursalDestino.setSelectedItem(sucursalOrigen);
				
				boolean admin = false;
				List<PerfilUsuario> perfilesUsuario = null;
				if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
					perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
				}
				
				if(perfilesUsuario!=null){
					for(int i = 0;i<perfilesUsuario.size();i++){
						PerfilUsuario perfil = perfilesUsuario.get(i);
						if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
							admin = true;
						}
					}
				}
				
				if(admin){
					JCmbSucursalDestino.setEnabled(true);
				}else{
					JCmbSucursalDestino.setEnabled(false);
				}
				
	
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}

		}
		return JCmbSucursalDestino;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JComboBoxPanambi getJCmbEstado() {
		if (JCmbEstado == null) {
				JCmbEstado = new JComboBoxPanambi();
				List<String> estados = new ArrayList<String>();
				estados.add("TODOS");
				estados.add("ACEPTADO");
				estados.add("RECHAZADO");
				estados.add("EN TRANSITO");
				JCmbEstado.setModel(new ListComboBoxModel(estados));
				JCmbEstado.setSelectedIndex(0);
		}
		return JCmbEstado;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setText("Estado");
		}
		return lblpnmbEstado;
	}
	private JTextFieldDate getJTxtFechaDesde() {
		if (JTxtFechaDesde == null) {
			JTxtFechaDesde = new JTextFieldDate();
			JTxtFechaDesde.setEnabled(false);
		}
		return JTxtFechaDesde;
	}
	private JTextFieldDate getJTxtFechaHasta() {
		if (JTxtFechaHasta == null) {
			JTxtFechaHasta = new JTextFieldDate();
			JTxtFechaHasta.setEnabled(false);
		}
		return JTxtFechaHasta;
	}
	private JCheckBoxPanambi getChckbxpnmbFechaDesde() {
		if (chckbxpnmbFechaDesde == null) {
			chckbxpnmbFechaDesde = new JCheckBoxPanambi();
			chckbxpnmbFechaDesde.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCheckFechaDesde();
				}
			});
			chckbxpnmbFechaDesde.setText("Fecha desde");
		}
		return chckbxpnmbFechaDesde;
	}
	
	private void doCheckFechaDesde() {
		getJTxtFechaDesde().setEnabled(getChckbxpnmbFechaDesde().isSelected());
		
	}
	
	private JCheckBoxPanambi getChckbxpnmbFechaHasta() {
		if (chckbxpnmbFechaHasta == null) {
			chckbxpnmbFechaHasta = new JCheckBoxPanambi();
			chckbxpnmbFechaHasta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCheckFechaHasta();
				}
			});
			chckbxpnmbFechaHasta.setText("Fecha Hasta");
		}
		return chckbxpnmbFechaHasta;
	}
	
	private void doCheckFechaHasta() {
		getJTxtFechaHasta().setEnabled(getChckbxpnmbFechaHasta().isSelected());
	}
}
