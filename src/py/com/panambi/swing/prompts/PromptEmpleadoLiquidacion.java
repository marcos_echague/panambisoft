package py.com.panambi.swing.prompts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;


public class PromptEmpleadoLiquidacion extends JDialogPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID =  8951887626144995739L;
	private final JPanel contentPanel = new JPanel();
	private JLabelPanambi lblpnmbCdigo;
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi lblpnmbNombreProducto;
	private JTextFieldUpper jTxtNroDocumento;
	private JScrollPane scrollPane;
	private JTablePanambi table;
	private JButtonPanambi jBtnBuscar;
	private Browseable browseable;
	private ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
	ControladorSucursal controladorSucursal = new ControladorSucursal();
	private JTextFieldUpper jTxtNombre;
	private JLabelPanambi lblpnmbNombre;
	private JButton btnLimpiar;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;

	/**
	 * Create the dialog.
	 */
	public PromptEmpleadoLiquidacion(Browseable browseable) {
		this.browseable = browseable;
		initialize();
	}

	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptEmpleadoLiquidacion.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Prompt Empleados");
		setBounds(100, 100, 852, 595);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtNroDocumento(), GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
								.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 781, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(getBtnLimpiar())))
							.addContainerGap(35, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
							.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getJTxtNombre(), GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)))
							.addGap(188))))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbNombreProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(28)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getBtnLimpiar())))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(16)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(26)
					.addComponent(getScrollPane(), 0, 0, Short.MAX_VALUE)
					.addGap(71))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		{
			JPanel panelTitle = new JPanel();
			panelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			getContentPane().add(panelTitle, BorderLayout.NORTH);
			{
				JLabel lblNewLabel = new JLabel("");
				lblNewLabel.setIcon(new ImageIcon(PromptProductosPrecios.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
				panelTitle.add(lblNewLabel);
			}
		}
		centerIt();
		setShortcutsHere((JComponent) getContentPane());
	}

	private JLabelPanambi getLblpnmbCdigo() {
		if (lblpnmbCdigo == null) {
			lblpnmbCdigo = new JLabelPanambi();
			lblpnmbCdigo.setText("C\u00F3digo");
		}
		return lblpnmbCdigo;
	}

	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
		}
		return jTxtCodigo;
	}

	private JLabelPanambi getLblpnmbNombreProducto() {
		if (lblpnmbNombreProducto == null) {
			lblpnmbNombreProducto = new JLabelPanambi();
			lblpnmbNombreProducto.setText("Nro. Documento");
		}
		return lblpnmbNombreProducto;
	}

	private JTextFieldUpper getJTxtNroDocumento() {
		if (jTxtNroDocumento == null) {
			jTxtNroDocumento = new JTextFieldUpper();
		}
		return jTxtNroDocumento;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTablePanambi getTable() {
		if (table == null) {
			table = new JTablePanambi(new String[] {"Codigo", "Documento", "Nombre", "Apellido", "Cargo", "Sucursal","Estado","Data"});
			table.getColumnModel().getColumn(0).setPreferredWidth(15);
			table.getColumnModel().getColumn(1).setPreferredWidth(20);
			table.getColumnModel().getColumn(2).setPreferredWidth(60);
			table.getColumnModel().getColumn(3).setPreferredWidth(60);
			table.getColumnModel().getColumn(4).setPreferredWidth(40);
			table.getColumnModel().getColumn(5).setPreferredWidth(40);
			table.getColumnModel().getColumn(6).setPreferredWidth(40);
			table.getColumnModel().getColumn(7).setPreferredWidth(0);
			table.getColumnModel().getColumn(7).setMinWidth(0);
			table.getColumnModel().getColumn(7).setMaxWidth(0);
						
		}
		return table;
	}

	private JButtonPanambi getJBtnBuscar() {
		if (jBtnBuscar == null) {
			jBtnBuscar = new JButtonPanambi();
			jBtnBuscar.setText("Buscar (F5)");
			jBtnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
		}
		return jBtnBuscar;
	}

	private void doBuscar() {
		try {
			getTable().resetData(0);
			int i = 0;
			String documentoempleado = getJTxtNroDocumento().getText();
			Integer codigoempleado = getJTxtCodigo().getValor();
			String nombreempleado = getJTxtNombre().getText();
			Integer codsucursal = null;
			if (getJCmbSucursal().isEnabled() && getJCmbSucursal().getSelectedIndex() > 0) {
				codsucursal = ((Sucursal) getJCmbSucursal().getSelectedItem()).getCodSucursal();
			}
			
			List<Empleado> empleadoLista =  controladorEmpleado.getEmpleados(JFramePanambiMain.session.getConn(), documentoempleado,codigoempleado,nombreempleado,codsucursal); 
			
			for (Empleado empleado : empleadoLista) {
					getTable().addRow();
					getTable().setValueAt(empleado.getCodEmpleado(), i, 0);
					getTable().setValueAt(empleado.getNroDocumento(), i, 1);
					getTable().setValueAt(empleado.getNombre(), i, 2);
					getTable().setValueAt(empleado.getApellido(), i, 3);
					getTable().setValueAt(empleado.getCargoEmpleado().getCodCargoEmpleado()+"-"+empleado.getCargoEmpleado().getDescripcion(), i, 4);
					getTable().setValueAt(empleado.getSucursal(), i, 5);
					if(empleado.getEstado().equals("A")){
						getTable().setValueAt("Activo", i, 6);
					}else{
						getTable().setValueAt("Inactivo", i, 6);
					}
					
					getTable().setValueAt(empleado, i, 7);
					i++;
				
			}
			if (getTable().getRowCount() > 0) {
				getTable().requestFocus();
				getTable().changeSelection(0, 0, false, false);
			}else{
				DlgMessage.showMessage(getOwner(), "No existe empleado acorde a los parámetros ingresados", DlgMessage.INFORMATION_MESSAGE);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((Empleado)getTable().getValueAt(getTable().getSelectedRow(), 7), null);
		}
		this.dispose();
	}
	private JTextFieldUpper getJTxtNombre() {
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
		}
		return jTxtNombre;
	}
	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setText("Nombre");
		}
		return lblpnmbNombre;
	}
	private JButton getBtnLimpiar() {
		if (btnLimpiar == null) {
			btnLimpiar = new JButton("Limpiar");
			
			btnLimpiar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					doLimpiar();
					
				}
			});
		}
		return btnLimpiar;
	}
	
	private void doLimpiar(){
		getJTxtCodigo().setText(null);
		getJTxtNombre().setText("");
		getJTxtNroDocumento().setText("");
		getTable().resetData(0);
		getJCmbSucursal().setSelectedIndex(0);
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal");
		}
		return lblpnmbSucursal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		
		if (jCmbSucursal == null) {
			try {
				jCmbSucursal = new JComboBoxPanambi();
				List<Sucursal> sucursales = new ArrayList<Sucursal>();
				Sucursal sucursal = new Sucursal();
				sucursal.setNombre("TODAS");
				sucursales.add(sucursal);
				sucursales.addAll(controladorSucursal.getSucursales(JFramePanambiMain.session.getConn()));
				jCmbSucursal.setModel(new ListComboBoxModel(sucursales));
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			jCmbSucursal.setSelectedIndex(0);

		}
		return jCmbSucursal;
	}
}
