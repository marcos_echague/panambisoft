package py.com.panambi.swing.prompts;

import java.awt.Container;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.apache.velocity.runtime.parser.node.SetExecutor;

import py.com.panambi.bean.Devolucion;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;

public class PromptClientes extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6512808815357953517L;
	private JScrollPane scrollPane;
	private JButtonPanambi jBtnBuscar;
	private JLabelPanambi lblpnmbNumero;
	private JTextFieldInteger jTxtNumeroNotaCredito;
	private Browseable browseable;
	private JTablePanambi table;
	private ControladorCliente controladorCliente= new ControladorCliente();

	/**
	 * Create the dialog.
	 */
	public PromptClientes(Browseable browseable) {
		this.browseable = browseable;
		initialize();
	}
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptClientes.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Busqueda de notas de credito");
		setBounds(100, 100, 696, 360);
		centerIt();
		setShortcutsHere((JComponent) getContentPane());
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 612, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbNumero(), GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
							.addGap(2)
							.addComponent(getJTxtNumeroNotaCredito(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(39))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(49)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbNumero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJTxtNumeroNotaCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJBtnBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(21)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
					.addGap(93))
		);
		getContentPane().setLayout(groupLayout);
		setShortcutsHere((JComponent) getContentPane());
	}
	
	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});
		
		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}
	
	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((Devolucion)getTable().getValueAt(getTable().getSelectedRow(), 3), null);
		}
		this.dispose();
	}
	
	private void doBuscar() {
		try {
			getTable().resetData(0);
			if(getJTxtNumeroNotaCredito().getValor()==null){
				List<Devolucion> devoluciones = controladorCliente.getCliente(JFramePanambiMain.session.getConn());
				int i = 0;
				for (Devolucion devolucion : devoluciones) {
					getTable().addRow();
					getTable().setValueAt(devolucion.getNotaCredito(), i, 0);
					getTable().setValueAt(devolucion.getTotal(), i, 1);
					getTable().setValueAt(devolucion.getFecha(), i, 2);
					getTable().setValueAt(devolucion, i, 3);
					i++;
				}
				if (getTable().getRowCount() > 0) {
					getTable().requestFocus();
					getTable().changeSelection(0, 0, false, false);
				}else{
					//actualizarDetalleProducto(null);
					DlgMessage.showMessage(getOwner(), "No existen nota de cr�dito activas de acuerdo a los par�metros ingresados", DlgMessage.INFORMATION_MESSAGE);
				}
			}else{
				Devolucion devolucion = controladorDevolucion.getNotaCreditoActiva(JFramePanambiMain.session.getConn(), getJTxtNumeroNotaCredito().getValor());
				if(devolucion!=null){
					getTable().addRow();
					getTable().setValueAt(devolucion.getNotaCredito(), getTable().getRowCount()-1, 0);
					getTable().setValueAt(devolucion.getTotal(), getTable().getRowCount()-1, 1);
					getTable().setValueAt(devolucion.getFecha(), getTable().getRowCount()-1, 2);
					getTable().setValueAt(devolucion, getTable().getRowCount()-1, 3);
				}else{
					DlgMessage.showMessage(getOwner(), "No existe nota de cr�dito activas de acuerdo a los par�metros ingresados", DlgMessage.INFORMATION_MESSAGE);
				}
			}
			

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	
	private JTablePanambi getTable(){
		if (table == null) {
			String[] columns = { "Nota", "Valor", "Fecha de generacion", "Data" };
			table = new JTablePanambi(columns);
			table.getColumnModel().getColumn(0).setPreferredWidth(50);
			table.getColumnModel().getColumn(1).setPreferredWidth(100);
			table.getColumnModel().getColumn(2).setPreferredWidth(100);
			table.getColumnModel().getColumn(3).setPreferredWidth(0);
			table.getColumnModel().getColumn(3).setMinWidth(0);
			table.getColumnModel().getColumn(3).setMaxWidth(0);
		}
		return table;
	}
	
	private JButtonPanambi getJBtnBuscar() {
		if (jBtnBuscar == null) {
			jBtnBuscar = new JButtonPanambi();
			jBtnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			jBtnBuscar.setText("Buscar (F5)");
		}
		return jBtnBuscar;
	}
	private JLabelPanambi getLblpnmbNumero() {
		if (lblpnmbNumero == null) {
			lblpnmbNumero = new JLabelPanambi();
			lblpnmbNumero.setText("Numero : ");
		}
		return lblpnmbNumero;
	}
	private JTextFieldInteger getJTxtNumeroNotaCredito() {
		if (jTxtNumeroNotaCredito == null) {
			jTxtNumeroNotaCredito = new JTextFieldInteger();
		}
		return jTxtNumeroNotaCredito;
	}
	
}
