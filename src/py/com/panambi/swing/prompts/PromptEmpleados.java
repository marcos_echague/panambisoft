package py.com.panambi.swing.prompts;

import java.awt.Container;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;

public class PromptEmpleados extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6512808815357953517L;
	private JScrollPane scrollPane;
	private Browseable browseable;
	private JTablePanambi table;
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	private Cliente cliente;

	/**
	 * Create the dialog.
	 */
	public PromptEmpleados(Browseable browseable, Cliente cliente) {
		this.browseable = browseable;
		this.cliente = cliente;
		initialize();
	}
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(PromptEmpleados.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Promp Empleados");
		setBounds(100, 100, 479, 285);
		centerIt();
		setShortcutsHere((JComponent) getContentPane());
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(21, Short.MAX_VALUE)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE)
					.addGap(19))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(87, Short.MAX_VALUE)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
					.addGap(24))
		);
		getContentPane().setLayout(groupLayout);
		setShortcutsHere((JComponent) getContentPane());
		cargarListaNotasCredito();
		getTable().changeSelection(0, 0, false, false);
	}
	
	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					seleccionar();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				super.keyPressed(e);
			}
		});
		
		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}
	
	private void seleccionar() {
		if (getTable().getSelectedRow() > -1) {
			browseable.setValues((Devolucion)getTable().getValueAt(getTable().getSelectedRow(), 3), null);
		}
		this.dispose();
	}
	
	private void cargarListaNotasCredito() {
		
		try {
			List<Devolucion> devoluciones = controladorDevolucion.getNotasCreditoActivasCliente(JFramePanambiMain.session.getConn(), cliente);
			int i = 0 ;
			for (Devolucion devolucion : devoluciones) {
				getTable().addRow();
				getTable().setValueAt(devolucion.getNotaCredito(), i, 0);
				getTable().setValueAt(devolucion.getTotal(), i, 1);
				getTable().setValueAt(devolucion.getFecha(), i, 2);
				getTable().setValueAt(devolucion, i, 3);
				i++;
			}
//			getTable().resetData(0);
//			if(getJTxtNumeroNotaCredito().getValor()==null){
//				List<Devolucion> devoluciones = controladorDevolucion.getNotasCreditoActivasCliente(JFramePanambiMain.session.getConn(), cliente);
//				int i = 0;
//				for (Devolucion devolucion : devoluciones) {
//					getTable().addRow();
//					getTable().setValueAt(devolucion.getNotaCredito(), i, 0);
//					getTable().setValueAt(devolucion.getTotal(), i, 1);
//					getTable().setValueAt(devolucion.getFecha(), i, 2);
//					getTable().setValueAt(devolucion, i, 3);
//					i++;
//				}
//				if (getTable().getRowCount() > 0) {
//					getTable().requestFocus();
//					getTable().changeSelection(0, 0, false, false);
//				}else{
//					//actualizarDetalleProducto(null);
//					DlgMessage.showMessage(getOwner(), "No existen nota de cr�dito activas de acuerdo a los par�metros ingresados", DlgMessage.INFORMATION_MESSAGE);
//				}
//			}else{
////				//Devolucion devolucion = controladorDevolucion.getNotaCreditoActiva(JFramePanambiMain.session.getConn(), getJTxtNumeroNotaCredito().getValor());
////				if(devolucion!=null){
////					getTable().addRow();
////					getTable().setValueAt(devolucion.getNotaCredito(), getTable().getRowCount()-1, 0);
////					getTable().setValueAt(devolucion.getTotal(), getTable().getRowCount()-1, 1);
////					getTable().setValueAt(devolucion.getFecha(), getTable().getRowCount()-1, 2);
////					getTable().setValueAt(devolucion, getTable().getRowCount()-1, 3);
//				}else{
//					DlgMessage.showMessage(getOwner(), "No existe nota de cr�dito activas de acuerdo a los par�metros ingresados", DlgMessage.INFORMATION_MESSAGE);
//				}
//			}
			

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}

	}

	
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	
	private JTablePanambi getTable(){
		if (table == null) {
			String[] columns = { "C�digo", "Nombres", "Apellidos", "Nro. de c�dula", };
			table = new JTablePanambi(columns);
			table.getColumnModel().getColumn(0).setPreferredWidth(50);
			table.getColumnModel().getColumn(1).setPreferredWidth(100);
			table.getColumnModel().getColumn(2).setPreferredWidth(100);
			table.getColumnModel().getColumn(3).setPreferredWidth(0);
			table.getColumnModel().getColumn(3).setMinWidth(0);
			table.getColumnModel().getColumn(3).setMaxWidth(0);
		}
		return table;
	}
}
