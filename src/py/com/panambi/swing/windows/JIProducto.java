package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.MaskFormatter;

import org.apache.commons.io.FileUtils;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.ImagenProducto;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Temporada;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorTemporada;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.prompts.PromptProductosPrecios;

public class JIProducto extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6868130936207867210L;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbDescripcion;
	private JLabelPanambi lblpnmbPrecio;
	private JLabelPanambi lblpnmbMaximoDescuento;
	private JLabelPanambi lblpnmbStockMinimo;
	private JTextFieldInteger jTxtCodProducto;
	private JTextFieldDouble jTxtPrecio;
	private JTextFieldInteger jTxtStockMinimo;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JComboBoxPanambi jCmbDescripcion;
	private Producto producto;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private ControladorTemporada controladorTemporada = new ControladorTemporada();
	private List<String> productos = new ArrayList<String>();
	private List<String> temporadas = new ArrayList<String>();
	private JLabelPanambi lblpnmbTemporada;
	private JComboBox<String> jCmbTemporada;
	private JLabelPanambi lblpnmbImagen;
	private JCheckBox jChxbxEstado;
	private JButtonPanambi btnpnmbBuscarImagen;
	private JFileChooser jfcExaminarImagen;
	private JPanel panel;
	private JLabelPanambi jLblImagen;
	private JButtonPanambi btnpnmbLimpiarImagen;
	private ImagenProducto imagenProducto;
	private File fileImagen;
	private byte[] imagenBytes;
	private JButtonPanambi btnpnmbEliminar;
	private JFormattedTextField jFtTxtMexDescuento;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi labelPanambi_1;
	private JLabelPanambi labelPanambi_4;
	private JButtonPanambi JbtnpnmbConsultarPrecios;

	public JIProducto() throws Exception{
		super();
		initialize();
	}

	private void initialize() {
		setMaximizable(false);
		setTitle("Productos");
		setBounds(100, 100, 732, 505);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getJbtnpnmbConsultarPrecios());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		setShortcuts(this);
		decorate();
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbPrecio(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbStockMinimo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbMaximoDescuento(), GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbTemporada(), GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJCmbTemporada(), GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelPanambi_4(), GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJChxbxEstado())
						.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtStockMinimo(), GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJFtTxtMexDescuento(), 144, 144, 144)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJTxtPrecio(), GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelPanambi_1_1(), GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(53)
							.addComponent(getLblpnmbImagen(), GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getBtnpnmbBuscarImagen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getBtnpnmbLimpiarImagen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(27)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJChxbxEstado())
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbImagen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getBtnpnmbBuscarImagen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getBtnpnmbLimpiarImagen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(39)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
											.addComponent(getJTxtPrecio(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbPrecio(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addComponent(getLabelPanambi_1_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(getJTxtStockMinimo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbStockMinimo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(getJFtTxtMexDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbMaximoDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getLabelPanambi_4(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJCmbTemporada(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbTemporada(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))))
					.addGap(66))
		);
		getJPanelCentral().setLayout(groupLayout);

		setHelp("productos");
		decorate();
		setShortcuts(this);
		doLimpiar();
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { getJCmbDescripcion(), getJTxtPrecio(), getJTxtStockMinimo(), getJFtTxtMexDescuento(), getJCmbTemporada(), getJChxbxEstado(), getBtnpnmbBuscarImagen(), getBtnpnmbLimpiarImagen(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(),
				getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir() }));
	}

	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}

	private JLabelPanambi getLblpnmbDescripcion() {
		if (lblpnmbDescripcion == null) {
			lblpnmbDescripcion = new JLabelPanambi();
			lblpnmbDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDescripcion.setText("Descripcion : ");
		}
		return lblpnmbDescripcion;
	}

	private JLabelPanambi getLblpnmbPrecio() {
		if (lblpnmbPrecio == null) {
			lblpnmbPrecio = new JLabelPanambi();
			lblpnmbPrecio.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPrecio.setText("Precio de venta: ");
		}
		return lblpnmbPrecio;
	}

	private JLabelPanambi getLblpnmbMaximoDescuento() {
		if (lblpnmbMaximoDescuento == null) {
			lblpnmbMaximoDescuento = new JLabelPanambi();
			lblpnmbMaximoDescuento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbMaximoDescuento.setText("Maximo Descuento(%) : ");
		}
		return lblpnmbMaximoDescuento;
	}

	private JLabelPanambi getLblpnmbStockMinimo() {
		if (lblpnmbStockMinimo == null) {
			lblpnmbStockMinimo = new JLabelPanambi();
			lblpnmbStockMinimo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbStockMinimo.setText("Stock Minimo : ");
		}
		return lblpnmbStockMinimo;
	}

	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
			jTxtCodProducto.setEnabled(false);
			jTxtCodProducto.setEditable(false);
		}
		return jTxtCodProducto;
	}

	private JTextFieldDouble getJTxtPrecio() {
		if (jTxtPrecio == null) {
			jTxtPrecio = new JTextFieldDouble();
		}
		return jTxtPrecio;
	}

	private JTextFieldInteger getJTxtStockMinimo() {
		if (jTxtStockMinimo == null) {
			jTxtStockMinimo = new JTextFieldInteger();
		}
		return jTxtStockMinimo;
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}

	private void doSalir() {
		this.dispose();
	}

	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Graba producto");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}

	private void doGrabar() {
		boolean camposObligatorios = true;
		boolean temporadaSeleccionada = true;
		try {
			if (producto == null) {

				producto = new Producto();

				producto.setDescripcion((String) getJCmbDescripcion().getSelectedItem());

				if (getJTxtPrecio().getValue() == null || (double) getJTxtPrecio().getValue() == 0.0) {
					// producto.setPrecio(0.0);
					camposObligatorios = false;
				} else {
					producto.setPrecio((double) getJTxtPrecio().getValue());
				}
				if (getJFtTxtMexDescuento().getValue() == null) {
					producto.setMaxDescuento(0);
				} else {
					producto.setMaxDescuento(Float.parseFloat((getJFtTxtMexDescuento().getValue().toString())));
				}
				if (getJTxtStockMinimo().getValor() == null) {
					producto.setStockMinimo(0);
				} else {
					producto.setStockMinimo((getJTxtStockMinimo().getValor()));
				}
				if ((String) jCmbTemporada.getSelectedItem() == null) {
					temporadaSeleccionada = false;
				} else {
					producto.setCodTemporada(controladorTemporada.getTemporada(JFramePanambiMain.session.getConn(), (String) jCmbTemporada.getSelectedItem()).getCodTemporada());
				}

				if (getJChxbxEstado().isSelected()) {
					producto.setEstado("A");
				} else {
					producto.setEstado("I");
				}

				if (imagenBytes != null) {

					imagenProducto = new ImagenProducto();
					imagenProducto.setDescripcion("");
					imagenProducto.setImagen(imagenBytes);

				} else {
					imagenProducto = null;
				}

				producto.setImagenProducto(imagenProducto);

				if (camposObligatorios) {
					if (temporadaSeleccionada) {
						controladorProducto.insertarProducto(JFramePanambiMain.session.getConn(), producto);
						DlgMessage.showMessage(getOwner(), "Operacion exitosa", DlgMessage.INFORMATION_MESSAGE);
						producto = null;
						doLimpiar();
						getJCmbDescripcion().requestFocus();
					} else {
						DlgMessage.showMessage(getOwner(), "Debe seleccionar una temporada para el producto", DlgMessage.ERROR_MESSAGE);
						producto = null;
						getJCmbTemporada().requestFocus();
					}
				} else {
					DlgMessage.showMessage(getOwner(), "Debe completar los campos obligatorios", DlgMessage.ERROR_MESSAGE);
					producto = null;
					getJTxtPrecio().requestFocus();
				}
				producto = null;

			} else {

				producto.setDescripcion((String) getJCmbDescripcion().getSelectedItem());

				if (getJTxtPrecio().getValue() == null) {
					camposObligatorios = false;
				} else {
					producto.setPrecio((double) getJTxtPrecio().getValue());
				}

				if (getJFtTxtMexDescuento().getValue() == null) {
					producto.setMaxDescuento(0);
				} else {
					producto.setMaxDescuento((Float.parseFloat(getJFtTxtMexDescuento().getValue().toString())));
				}
				if (getJTxtStockMinimo().getValor() == null) {
					producto.setStockMinimo(0);
				} else {
					producto.setStockMinimo((getJTxtStockMinimo().getValor()));
				}

				if (getJChxbxEstado().isSelected()) {
					producto.setEstado("A");
				} else {
					producto.setEstado("I");
				}
				if ((String) jCmbTemporada.getSelectedItem() == null) {
					temporadaSeleccionada = false;
				} else {
					producto.setCodTemporada(controladorTemporada.getTemporada(JFramePanambiMain.session.getConn(), (String) jCmbTemporada.getSelectedItem()).getCodTemporada());
				}

				// validarTemporada((String) jCmbTemporada.getSelectedItem());

				if (imagenBytes != null) {

					imagenProducto = new ImagenProducto();
					imagenProducto.setDescripcion("");
					imagenProducto.setImagen(imagenBytes);

				} else {
					imagenProducto = null;
				}

				producto.setImagenProducto(imagenProducto);

				if (camposObligatorios) {
					if (temporadaSeleccionada) {
						Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea modificar los datos del producto?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (ret == JOptionPane.YES_OPTION) {
							controladorProducto.modificarProducto(JFramePanambiMain.session.getConn(), producto);
							DlgMessage.showMessage(getOwner(), "Operacion exitosa", DlgMessage.INFORMATION_MESSAGE);
							producto = null;
							doLimpiar();
						}

						getJCmbDescripcion().requestFocus();
					} else {
						DlgMessage.showMessage(getOwner(), "Debe seleccionar una temporada para el producto", DlgMessage.ERROR_MESSAGE);
						producto = null;
						getJCmbTemporada().requestFocus();
					}
				} else {
					DlgMessage.showMessage(getOwner(), "Debe completar los campos obligatorios", DlgMessage.ERROR_MESSAGE);
					producto = null;
					getJTxtPrecio().requestFocus();

				}
				producto = null;
			}

		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	/*
	 * private boolean cargarValores(){ boolean resul; boolean
	 * camposObligatorios; producto.setDescripcion((String)
	 * getJCmbDescripcion().getSelectedItem().trim());
	 * producto.setPrecio((Double)getJTxtPrecio().getValue());
	 * producto.setStockMinimo(getJTxtStockMinimo().getValor());
	 * producto.setMaxDescuento((Float)getJFtTxtMexDescuento().getValue());
	 * cliente.setEmail(getJTxtEmail().getText());
	 * cliente.setDireccion(getJTxtDireccion().getText());
	 * cliente.setFechaIngreso(getJTxtFecha().getFecha());
	 * 
	 * if(getChckbxActivo().isSelected()){ cliente.setEstado("A"); }else
	 * cliente.setEstado("I"); }
	 */
	/**
	 * Valida la asignacion de el atributo de temporada al producto
	 * 
	 * @param tempo
	 * 
	 */
	/*
	 * private void validarTemporada(String tempo){
	 * 
	 * if(tempo==null){
	 * 
	 * try { Temporada t = new Temporada(); ControladorTemporada ct = new
	 * ControladorTemporada(); tempo = "S/T";
	 * t=ct.getTemporada(JFramePanambiMain.session.getConn(), tempo);
	 * 
	 * if(t == null){
	 * 
	 * Temporada t2 = new Temporada(); t2.setNombre(tempo);
	 * ct.insertarTemporada(JFramePanambiMain.session.getConn(), t2); t2 =
	 * ct.getTemporada(JFramePanambiMain.session.getConn(), tempo);
	 * producto.setCodTemporada(t2.getCodTemporada());
	 * 
	 * }else{ producto.setCodTemporada(t.getCodTemporada()); }
	 * 
	 * 
	 * } catch (Exception e) {
	 * 
	 * DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE); }
	 * 
	 * }else{ Temporada tExistente = new Temporada(); try { tExistente = new
	 * ControladorTemporada().getTemporada(JFramePanambiMain.session.getConn(),
	 * tempo); producto.setCodTemporada(tExistente.getCodTemporada()); } catch
	 * (Exception e) { DlgMessage.showMessage(getOwner(), e,
	 * DlgMessage.ERROR_MESSAGE); }
	 * 
	 * }
	 * 
	 * }
	 */

	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar productos");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}

	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Descripcion", "Precio", "Maximo Descuento", "Stock Minimo", "Temporada" };
			String[] pks = { "codProducto" };
			String sSelect = "SELECT codproducto, descripcion, precio, maxdescuento, stockminimo, ";
			sSelect += "(SELECT nombre FROM temporadas WHERE productos.codtemporada = temporadas.codtemporada) ";
			sSelect += "FROM productos ";
			sSelect += "ORDER BY descripcion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Producto", pks);
			jb.setVisible(true);
			getJCmbDescripcion().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Prepara la pantalla para un nuevo registro");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar() {

		try {

			producto = null;
			poblarLista();
			poblarListaTemporada();
			getJCmbDescripcion().setModel(new ListComboBoxModel<String>(productos));
			getJCmbDescripcion().setSelectedItem(null);
			getJCmbTemporada().setModel(new ListComboBoxModel<String>(temporadas));
			getJCmbTemporada().setSelectedItem(null);
			getJTxtCodProducto().setValor(null);
			getJTxtPrecio().setValue(null);
			getJTxtStockMinimo().setValor(0);
			getJFtTxtMexDescuento().setValue("00.00");
			jLblImagen.setIcon(null);
			btnpnmbEliminar.setEnabled(false);
			btnpnmbGrabar.setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void poblarLista() throws Exception {
		productos = new ArrayList<String>();
		List<Producto> listaprod = controladorProducto.getProductos(JFramePanambiMain.session.getConn());

		for (Producto prod : listaprod) {
			productos.add(prod.getDescripcion());
		}
	}

	private void poblarListaTemporada() throws Exception {

		temporadas = new ArrayList<String>();
		List<Temporada> listatem = controladorTemporada.getTemporadas(JFramePanambiMain.session.getConn());

		for (Temporada tem : listatem) {
			temporadas.add(tem.getNombre());
		}
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbDescripcion() {
		if (jCmbDescripcion == null) {
			jCmbDescripcion = new JComboBoxPanambi();
			jCmbDescripcion.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusDescripcion();
				}
			});

			jCmbDescripcion.setEditable(true);
			jCmbDescripcion.setModel(new ListComboBoxModel<String>(productos));
			jCmbDescripcion.setSelectedItem(null);

		}
		return jCmbDescripcion;
	}

	private JLabelPanambi getLblpnmbTemporada() {
		if (lblpnmbTemporada == null) {
			lblpnmbTemporada = new JLabelPanambi();
			lblpnmbTemporada.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTemporada.setText("Temporada : ");
		}
		return lblpnmbTemporada;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JComboBox getJCmbTemporada() {
		if (jCmbTemporada == null) {
			jCmbTemporada = new JComboBox();
		}
		return jCmbTemporada;
	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			this.producto = (Producto) obj;
			getJTxtCodProducto().setValor(producto.getCodProducto());
			getJCmbDescripcion().setSelectedItem(((Producto) (obj)).getDescripcion());
			getJTxtPrecio().setValue(producto.getPrecio());
			getJTxtStockMinimo().setValor(producto.getStockMinimo());

			if (producto.getMaxDescuento() == 0.0) {
				getJFtTxtMexDescuento().setValue("00.00");
			} else {
				getJFtTxtMexDescuento().setValue(producto.getMaxDescuento());
			}

			if (producto.getEstado().equals("A")) {
				getJChxbxEstado().setSelected(true);
			} else {
				getJChxbxEstado().setSelected(false);
			}

			try {
				getJCmbTemporada().setSelectedItem((new Temporada(JFramePanambiMain.session.getConn(), producto.getCodTemporada())).getNombre());
			} catch (Exception e) {
				getJCmbTemporada().setSelectedItem(null);
			}

			InputStream in = null;
			if (producto.getImagenProducto() != null) {
				try {
					in = new ByteArrayInputStream(producto.getImagenProducto().getImagen());
					BufferedImage image = null;
					image = ImageIO.read(in);
					jLblImagen.setIcon(new ImageIcon(image.getScaledInstance(jLblImagen.getWidth(), jLblImagen.getHeight(), Image.SCALE_DEFAULT)));

				} catch (Exception e) {
					e.printStackTrace();
					jLblImagen.setIcon(null);
					JOptionPane.showMessageDialog(this, "No se pudo obtener imagen");
				}
			} else {
				jLblImagen.setIcon(null);
			}

			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);

		}
	}

	@Override
	public void setNoValues(Object obj, Integer source) {

	}

	private void lostFocusDescripcion() {
		try {
			Producto prod = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), (String) jCmbDescripcion.getSelectedItem());
			if (prod != null) {
				setValues(prod, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void doNuevoFromLostFocus() {
		producto = null;
		getJTxtCodProducto().setValor(null);
		getJCmbTemporada().setSelectedItem(null);
		getJTxtPrecio().setValue(null);
		getJFtTxtMexDescuento().setValue("00.00");
		getJTxtStockMinimo().setValor(0);
		jChxbxEstado.setSelected(true);
		jLblImagen.setIcon(null);
		btnpnmbEliminar.setEnabled(false);
		if (getJCmbDescripcion().getSelectedItem().toString().length() != 0) {
			btnpnmbGrabar.setEnabled(true);
		} else
			btnpnmbGrabar.setEnabled(false);

	}

	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbDescripcion());
	}


	private JLabelPanambi getLblpnmbImagen() {
		if (lblpnmbImagen == null) {
			lblpnmbImagen = new JLabelPanambi();
			lblpnmbImagen.setText("Imagen");
		}
		return lblpnmbImagen;
	}

	private JCheckBox getJChxbxEstado() {
		if (jChxbxEstado == null) {
			jChxbxEstado = new JCheckBox("Activo");
			jChxbxEstado.setSelected(true);
		}
		return jChxbxEstado;
	}

	private JButtonPanambi getBtnpnmbBuscarImagen() {
		if (btnpnmbBuscarImagen == null) {
			btnpnmbBuscarImagen = new JButtonPanambi();
			btnpnmbBuscarImagen.setMnemonic('E');
			btnpnmbBuscarImagen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						doExaminarImagen();
					} catch (IOException e) {

						JOptionPane.showMessageDialog(null, "No se puede visualizar examinador");
					}
				}
			});
			btnpnmbBuscarImagen.setToolTipText("Examinar imagen(Alt + E)");
			btnpnmbBuscarImagen.setText("...");
		}
		return btnpnmbBuscarImagen;
	}

	private void doExaminarImagen() throws IOException {

		jfcExaminarImagen = new JFileChooser();

		FileNameExtensionFilter filtroJpg = new FileNameExtensionFilter("JPG", "jpg");

		jfcExaminarImagen.setDialogTitle("Seleccion de Imagenes para Productos");
		jfcExaminarImagen.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfcExaminarImagen.setFileFilter(filtroJpg);
		jfcExaminarImagen.setMultiSelectionEnabled(false);
		jfcExaminarImagen.setAcceptAllFileFilterUsed(false);

		int returnVal = jfcExaminarImagen.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {

			fileImagen = jfcExaminarImagen.getSelectedFile();
			String ruta = fileImagen.getPath();
			ImageIcon imagen = new ImageIcon(ruta);
			Icon icono = new ImageIcon(imagen.getImage().getScaledInstance(jLblImagen.getWidth(), jLblImagen.getHeight(), Image.SCALE_DEFAULT));
			jLblImagen.setIcon(icono);
			byte[] bytesimagen = FileUtils.readFileToByteArray(fileImagen);
			imagenBytes = bytesimagen;

		} else
			imagenBytes = null;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBackground(Color.WHITE);
			panel.setLayout(null);
			panel.add(getLabelPanambi_1());
		}
		return panel;
	}

	private JLabelPanambi getLabelPanambi_1() {
		if (jLblImagen == null) {
			jLblImagen = new JLabelPanambi();
			jLblImagen.setBounds(0, 0, 280, 275);
		}
		return jLblImagen;
	}

	private JButtonPanambi getBtnpnmbLimpiarImagen() {
		if (btnpnmbLimpiarImagen == null) {
			btnpnmbLimpiarImagen = new JButtonPanambi();
			btnpnmbLimpiarImagen.setMnemonic('I');
			btnpnmbLimpiarImagen.setToolTipText("Borra imagen del producto");
			btnpnmbLimpiarImagen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiarImagen();
				}
			});
			btnpnmbLimpiarImagen.setText("Limpiar Imagen");
		}
		return btnpnmbLimpiarImagen;
	}

	private void doLimpiarImagen() {
		this.jLblImagen.setIcon(null);
		imagenBytes = null;
		producto.setImagenProducto(null);
	}

	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Elimina registro");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}

	private void doEliminar() {

		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea eliminar de forma \npermanente los datos del producto?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (ret == JOptionPane.YES_OPTION) {
			try {
				controladorProducto.borrarProducto(JFramePanambiMain.session.getConn(), producto);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbDescripcion().requestFocus();
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		producto = null;
		jCmbDescripcion.requestFocus();
	}

	private JFormattedTextField getJFtTxtMexDescuento() {
		if (jFtTxtMexDescuento == null) {
			try {
				MaskFormatter mascara = new MaskFormatter("##.##");
				jFtTxtMexDescuento = new JFormattedTextField(mascara);
				jFtTxtMexDescuento.setHorizontalAlignment(SwingConstants.LEFT);
				jFtTxtMexDescuento.setValue("00.00");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}

		}
		return jFtTxtMexDescuento;
	}

	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}

	private JLabelPanambi getLabelPanambi_1_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}

	private JLabelPanambi getLabelPanambi_4() {
		if (labelPanambi_4 == null) {
			labelPanambi_4 = new JLabelPanambi();
			labelPanambi_4.setText("*");
		}
		return labelPanambi_4;
	}
	private JButtonPanambi getJbtnpnmbConsultarPrecios() {
		if (JbtnpnmbConsultarPrecios == null) {
			JbtnpnmbConsultarPrecios = new JButtonPanambi();
			JbtnpnmbConsultarPrecios.setMnemonic('C');
			JbtnpnmbConsultarPrecios.setToolTipText("Consulta precios de costo");
			JbtnpnmbConsultarPrecios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPromptConsultarProducto();
				}
			});
			JbtnpnmbConsultarPrecios.setText("Consultar precios de costo");
		}
		return JbtnpnmbConsultarPrecios;
	}
	
	private void doPromptConsultarProducto() {
		PromptProductosPrecios prompt = new PromptProductosPrecios(this);
		prompt.setVisible(true);
	}
}
