package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.DetalleDevolucion;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class DlgDetalleDevoluciones extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbNroFactura;
	private JLabelPanambi lblpnmbFecha;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbCliente;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbTotal;
	private JLabelPanambi lblpnmbUsuario;
	private JLabelPanambi lblpnmbFechaAnulacin;
	private JLabelPanambi lblpnmbObservaciones;
	private JTextAreaUpper jTxtObservaciones;
	private JTextFieldInteger jTxtCodigo;
	private JFormattedTextFieldPanambi jTxtNroFactura;
	private JFormattedTextFieldPanambi jTxtCliente;
	private JTextFieldDouble jTxtTotal;
	private JLabelPanambi lblpnmbDetalleDeVenta;
	private ControladorDevolucion controladorDevolucion= new ControladorDevolucion();
	private Devolucion devolucion;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JFormattedTextFieldPanambi jTxtFecha;
	private JFormattedTextFieldPanambi jtxtUsuario;
	private JFormattedTextFieldPanambi jTxtSucursal;
	private JFormattedTextFieldPanambi jTxtEstadoDevolucion;
	private JFormattedTextFieldPanambi jTxtFechaAnulacion;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JButtonPanambi jBtnVerUso ;
	private JFormattedTextFieldPanambi jTxtEstadoNotaCredito;
	private JLabelPanambi lblpnmbEstadoNotaCrdito;
	private JLabelPanambi lblpnmbNotaDeCrdito;
	private JPanel panel_3;
	private JPanel panel_4;
	private JTextFieldInteger jTxtNotaCredito;
	public DlgDetalleDevoluciones(Devolucion devolucion) {
		this.devolucion= devolucion;
		initialize();
		
	}
	private void initialize() {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetalleDevoluciones.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 817, 614);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		getBtnpnmbAceptar().requestFocus();
		if(devolucion.getEstadoNotaCredito().equals("U")){
			getJBtnVerUso().setVisible(true);
		}else{
			getJBtnVerUso().setVisible(false);
		}
		
	}

	private void cargarValores(){
		try{
			
			getJTxtCodigo().setText(devolucion.getCodDevolucion()+"");
			getJTxtNroFactura().setText(devolucion.getVenta().getNroComprobante()+"");
			getJTxtNotaCredito().setValor(devolucion.getNotaCredito());
			getJTxtCliente().setText(devolucion.getCliente().getNombres()+" "+devolucion.getCliente().getApellidos());
			getJTxtTotal().setValue(devolucion.getTotal());
			
			String  patronFecha = "dd/MM/yyyy hh:mm:ss";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
			getJTxtFecha().setText(formatoFecha.format(devolucion.getFecha()));
			getJtxtUsuario().setText(devolucion.getUsuario().getUsuario());
			
			getJTxtSucursal().setText(devolucion.getSucursal().getNombre());
			
			String estadoDevolucion = "";
			if(devolucion.getEstadoDevolucion().equals("A")){
				estadoDevolucion= "ACTIVO";
			}else{
				estadoDevolucion = "ANULADO";
			}
			getJTxtEstadoDevolucion().setText(estadoDevolucion);
			
			String estadoNotaCredito = "";
			if(devolucion.getEstadoNotaCredito().equals("A")){
				estadoNotaCredito= "ACTIVO";
			}else if(devolucion.getEstadoNotaCredito().equals("U")){
				estadoNotaCredito= "USADO";
			}else{
				estadoNotaCredito= "ANULADO";
			}
			getJTxtEstadoNotaCredito().setText(estadoNotaCredito);
						
			if(devolucion.getFechaAnulacion()!=null){
				getJTxtFechaAnulacion().setText(formatoFecha.format(devolucion.getFechaAnulacion()));
			}else{
				getJTxtFechaAnulacion().setVisible(false);
				getLblpnmbFechaAnulacin().setVisible(false);
			}
			
			if(devolucion.getObservaciones()!=null){
				getJTxtObservaciones().setText(devolucion.getObservaciones().trim());
			}
			
			Iterator<DetalleDevolucion> iteratorDetalle=  devolucion.getDetalleDevolucion().listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				DetalleDevolucion det= (DetalleDevolucion) iteratorDetalle.next();
				//"Nro. Item","Descripcion del producto", "Monto unitario", "Cantidad","Total item"};
				getTablePanambi().setValueAt(det.getNroItem(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(det.getProducto().getDescripcion(), getTablePanambi().getRowCount()-1, 1);
					
				getTablePanambi().setValueAt(det.getMontoUnitario(), getTablePanambi().getRowCount()-1, 2);
				getTablePanambi().setValueAt(det.getCantidad(), getTablePanambi().getRowCount()-1, 3);
				getTablePanambi().setValueAt(det.getTotalItem(), getTablePanambi().getRowCount()-1, 4);
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 801, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLES DE DEVOLUCION");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 801, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro. Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha : ");
		}
		return lblpnmbFecha;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado devoluci\u00F3n : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Cliente : ");
		}
		return lblpnmbCliente;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Total : ");
		}
		return lblpnmbTotal;
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuario.setText("Usuario : ");
		}
		return lblpnmbUsuario;
	}
	private JLabelPanambi getLblpnmbFechaAnulacin() {
		if (lblpnmbFechaAnulacin == null) {
			lblpnmbFechaAnulacin = new JLabelPanambi();
			lblpnmbFechaAnulacin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacin.setText("Fecha anulaci\u00F3n : ");
		}
		return lblpnmbFechaAnulacin;
	}
	private JLabelPanambi getLblpnmbObservaciones() {
		if (lblpnmbObservaciones == null) {
			lblpnmbObservaciones = new JLabelPanambi();
			lblpnmbObservaciones.setBounds(24, 198, 100, 20);
			lblpnmbObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbObservaciones.setText("Observaciones ");
		}
		return lblpnmbObservaciones;
	}
	private JTextAreaUpper getJTxtObservaciones() {
		if (jTxtObservaciones == null) {
			jTxtObservaciones = new JTextAreaUpper();
			jTxtObservaciones.setBounds(34, 229, 378, 59);
			jTxtObservaciones.setFocusable(false);
			jTxtObservaciones.setEditable(false);
			jTxtObservaciones.setFont(new Font("Dialog", Font.PLAIN, 10));
		}
		return jTxtObservaciones;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JFormattedTextFieldPanambi getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JFormattedTextFieldPanambi();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.setFocusable(false);
			jTxtNroFactura.setEditable(false);
		}
		return jTxtNroFactura;
	}
	private JFormattedTextFieldPanambi getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JFormattedTextFieldPanambi();
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setFocusable(false);
			jTxtCliente.setEditable(false);
		}
		return jTxtCliente;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setFocusable(false);
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbDetalleDeVenta() {
		if (lblpnmbDetalleDeVenta == null) {
			lblpnmbDetalleDeVenta = new JLabelPanambi();
			lblpnmbDetalleDeVenta.setBounds(42, 294, 124, 20);
			lblpnmbDetalleDeVenta.setText("Detalle de devoluci\u00F3n");
		}
		return lblpnmbDetalleDeVenta;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(42, 325, 726, 150);
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. Item","Descripcion del producto", "Monto unitario", "Cantidad","Total item"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(2, Double.class);
				types.put(3, Integer.class);
				types.put(4, Double.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
//				tablePanambi.getColumnModel().getColumn(0).setMinWidth(0);
//				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(0);
//				tablePanambi.getColumnModel().getColumn(0).setMaxWidth(0);
//				
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(300);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
				
				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JFormattedTextFieldPanambi getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JFormattedTextFieldPanambi();
			jTxtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFecha.setFocusable(false);
			jTxtFecha.setEditable(false);
		}
		return jTxtFecha;
	}
	private JFormattedTextFieldPanambi getJtxtUsuario() {
		if (jtxtUsuario == null) {
			jtxtUsuario = new JFormattedTextFieldPanambi();
			jtxtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			jtxtUsuario.setFocusable(false);
			jtxtUsuario.setEditable(false);
		}
		return jtxtUsuario;
	}
	private JFormattedTextFieldPanambi getJTxtSucursal() {
		if (jTxtSucursal == null) {
			jTxtSucursal = new JFormattedTextFieldPanambi();
			jTxtSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSucursal.setFocusable(false);
			jTxtSucursal.setEditable(false);
		}
		return jTxtSucursal;
	}
	private JFormattedTextFieldPanambi getJTxtEstadoDevolucion() {
		if (jTxtEstadoDevolucion == null) {
			jTxtEstadoDevolucion = new JFormattedTextFieldPanambi();
			jTxtEstadoDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstadoDevolucion.setFocusable(false);
			jTxtEstadoDevolucion.setEditable(false);
		}
		return jTxtEstadoDevolucion;
	}
	private JFormattedTextFieldPanambi getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JFormattedTextFieldPanambi();
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
		}
		return jTxtFechaAnulacion;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 801, 498);
			panel_1.setLayout(null);
			panel_1.add(getScrollPane());
			panel_1.add(getLblpnmbObservaciones());
			panel_1.add(getLblpnmbDetalleDeVenta());
			panel_1.add(getJTxtObservaciones());
			panel_1.add(getPanel_3());
			panel_1.add(getPanel_4());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 538, 801, 38);
			panel_2.add(getBtnpnmbAceptar());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	
	private void doVerUsoNotaCredito(){
		try{
			Venta venta = controladorDevolucion.getVentaUsoNota(JFramePanambiMain.session.getConn(), devolucion);
			if(venta == null ){
				DlgMessage.showMessage(getOwner(),"No se ha encontrado el uso de la nota de cr�dito.", DlgMessage.WARNING_MESSAGE);
			}else{
				
				DlgDetalleVentas detalleVenta= new DlgDetalleVentas(venta);
				detalleVenta.centerIt();
				detalleVenta.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
		
		
		private JButtonPanambi getJBtnVerUso() {
		if (jBtnVerUso == null) {
			jBtnVerUso = new JButtonPanambi();
			jBtnVerUso.setMnemonic('V');
			jBtnVerUso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doVerUsoNotaCredito();
				}
			});
			jBtnVerUso.setText("Ver uso");
			jBtnVerUso.setToolTipText("");
		}
		return jBtnVerUso;
	
	}
		
	private JFormattedTextFieldPanambi getJTxtEstadoNotaCredito() {
		if (jTxtEstadoNotaCredito == null) {
			jTxtEstadoNotaCredito = new JFormattedTextFieldPanambi();
			jTxtEstadoNotaCredito.setFocusable(false);
			jTxtEstadoNotaCredito.setEditable(false);
			jTxtEstadoNotaCredito.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtEstadoNotaCredito;
	}
	private JLabelPanambi getLblpnmbEstadoNotaCrdito() {
		if (lblpnmbEstadoNotaCrdito == null) {
			lblpnmbEstadoNotaCrdito = new JLabelPanambi();
			lblpnmbEstadoNotaCrdito.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstadoNotaCrdito.setText("Estado nota cr\u00E9dito : ");
		}
		return lblpnmbEstadoNotaCrdito;
	}
	private JLabelPanambi getLblpnmbNotaDeCrdito() {
		if (lblpnmbNotaDeCrdito == null) {
			lblpnmbNotaDeCrdito = new JLabelPanambi();
			lblpnmbNotaDeCrdito.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNotaDeCrdito.setText("Nota de cr\u00E9dito : ");
		}
		return lblpnmbNotaDeCrdito;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setBorder(null);
			panel_3.setBounds(432, 13, 359, 285);
			GroupLayout gl_panel_3 = new GroupLayout(panel_3);
			gl_panel_3.setHorizontalGroup(
				gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(25)
						.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(25)
						.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJtxtUsuario(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(25)
						.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(25)
						.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtEstadoDevolucion(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(10)
						.addComponent(getLblpnmbEstadoNotaCrdito(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtEstadoNotaCredito(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
						.addGap(19)
						.addComponent(getJBtnVerUso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(25)
						.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
			);
			gl_panel_3.setVerticalGroup(
				gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(11)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(17)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJtxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(17)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(17)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstadoDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(16)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbEstadoNotaCrdito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtEstadoNotaCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJBtnVerUso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(16)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			panel_3.setLayout(gl_panel_3);
		}
		return panel_3;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.setBounds(24, 13, 390, 181);
			GroupLayout gl_panel_4 = new GroupLayout(panel_4);
			gl_panel_4.setHorizontalGroup(
				gl_panel_4.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_4.createSequentialGroup()
						.addGap(23)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_4.createSequentialGroup()
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_4.createSequentialGroup()
								.addComponent(getLblpnmbNotaDeCrdito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtNotaCredito(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_4.createSequentialGroup()
								.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_4.createSequentialGroup()
								.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))))
			);
			gl_panel_4.setVerticalGroup(
				gl_panel_4.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_4.createSequentialGroup()
						.addGap(7)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbNotaDeCrdito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtNotaCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_4.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			panel_4.setLayout(gl_panel_4);
		}
		return panel_4;
	}
	private JTextFieldInteger getJTxtNotaCredito() {
		if (jTxtNotaCredito == null) {
			jTxtNotaCredito = new JTextFieldInteger();
			jTxtNotaCredito.setFocusable(false);
			jTxtNotaCredito.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNotaCredito.setEditable(false);
		}
		return jTxtNotaCredito;
	}
}
