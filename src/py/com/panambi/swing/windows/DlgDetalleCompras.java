package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.DetalleCompra;
import py.com.panambi.bean.Pago;
import py.com.panambi.controller.ControladorCompra;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;

public class DlgDetalleCompras extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbNroFactura;
	private JLabelPanambi lblpnmbFecha;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbProveedor;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbTotal;
	private JLabelPanambi lblpnmbFechaAnulacin;
	private JLabelPanambi lblpnmbObservaciones;
	private JTextAreaUpper jTxtComentarios;
	private JTextFieldInteger jTxtCodigo;
	private JFormattedTextFieldPanambi jTxtNroFactura;
	private JFormattedTextFieldPanambi jTxtProveedor;
	private JTextFieldDouble jTxtTotal;
	private JLabelPanambi lblpnmbDetalleDeVenta;
	private Compra compra;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JFormattedTextFieldPanambi jTxtFecha;
	private JFormattedTextFieldPanambi jTxtSucursal;
	private JFormattedTextFieldPanambi jTxtEstado;
	private JFormattedTextFieldPanambi jTxtFechaAnulacion;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JLabelPanambi lblpnmbPagoALos;
	private JTextFieldInteger jTxtPagaderoDias;
	private JLabelPanambi lblpnmbDas;
	private JButtonPanambi btnpnmbDetallesDePago;
	public DlgDetalleCompras(Compra compra) {
		this.compra = compra;
		initialize();
		
	}
	private void initialize() {
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetalleCompras.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 817, 599);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		getBtnpnmbAceptar().requestFocus();
	}

	private void cargarValores(){
		try{
			
			getJTxtCodigo().setText(compra.getCodcompra()+"");
			getJTxtNroFactura().setText(compra.getFacturanro()+"");
			getJTxtProveedor().setText(compra.getProveedor().getRazonSocial());
			getJTxtTotal().setValue(compra.getTotal());
			
			String  patronFecha = "dd/MM/yyyy hh:mm:ss";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
			getJTxtFecha().setText(formatoFecha.format(compra.getFecha()));
//			String tipoVenta = "";
//			if(c.getTipoVenta().equals("C")){
//				tipoVenta = "CREDITO";
//			}else{
//				tipoVenta = "CONTADO";
//			}
			
			getJTxtSucursal().setText(compra.getSucursal().getNombre());
			
			getJTxtPagaderoDias().setValor(compra.getPagaderodias());
			
			String estado = "";
			if(compra.getEstado().equals("P")){
				estado= "PAGADOS";
			}else if (compra.getEstado().equals("N")){
				estado = "PENDIENTE DE PAGO";
			}else{
				estado = "ANULADO";
			}
			getJTxtEstado().setText(estado);
			
			String  patronFechaAnulacion = "dd/MM/yyyy";
			SimpleDateFormat formatoFechaAnulacion = new SimpleDateFormat(patronFechaAnulacion);
			if(compra.getFechaAnulacion()!=null){
				getJTxtFechaAnulacion().setText(formatoFechaAnulacion.format(compra.getFechaAnulacion()));
			}else{
				getJTxtFechaAnulacion().setVisible(false);
				getLblpnmbFechaAnulacin().setVisible(false);
			}
			
			if(compra.getComentarios()!=null){
				getJTxtComentarios().setText(compra.getComentarios().trim());
			}
			
//			List<DetalleCompra> detalleCompra = new ArrayList<DetalleCompra>();
//			detalleCompra = compra.getDetalleCompra();
			Iterator<DetalleCompra> iteratorDetalle=  compra.getDetalleCompra().listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				DetalleCompra det= (DetalleCompra) iteratorDetalle.next();
				//"Nro. Item","Descripcion del producto", "Costo unitario", "Cantidad","Total item"};
				getTablePanambi().setValueAt(det.getNroitem(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(det.getProducto().getDescripcion(), getTablePanambi().getRowCount()-1, 1);
					
				getTablePanambi().setValueAt(det.getCostounitario(), getTablePanambi().getRowCount()-1, 2);
				getTablePanambi().setValueAt(det.getCantidad(), getTablePanambi().getRowCount()-1, 3);
				getTablePanambi().setValueAt(det.getCostounitario()*det.getCantidad(), getTablePanambi().getRowCount()-1, 4);
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 801, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLES DE COMPRAS");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 801, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro. Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha : ");
		}
		return lblpnmbFecha;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProveedor.setText("Proveedor : ");
		}
		return lblpnmbProveedor;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Total : ");
		}
		return lblpnmbTotal;
	}
	private JLabelPanambi getLblpnmbFechaAnulacin() {
		if (lblpnmbFechaAnulacin == null) {
			lblpnmbFechaAnulacin = new JLabelPanambi();
			lblpnmbFechaAnulacin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacin.setText("Fecha anulaci\u00F3n : ");
		}
		return lblpnmbFechaAnulacin;
	}
	private JLabelPanambi getLblpnmbObservaciones() {
		if (lblpnmbObservaciones == null) {
			lblpnmbObservaciones = new JLabelPanambi();
			lblpnmbObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbObservaciones.setText("Comentarios : ");
		}
		return lblpnmbObservaciones;
	}
	private JTextAreaUpper getJTxtComentarios() {
		if (jTxtComentarios == null) {
			jTxtComentarios = new JTextAreaUpper();
			jTxtComentarios.setFocusable(false);
			jTxtComentarios.setEditable(false);
			jTxtComentarios.setFont(new Font("Dialog", Font.PLAIN, 10));
		}
		return jTxtComentarios;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JFormattedTextFieldPanambi getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JFormattedTextFieldPanambi();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.setFocusable(false);
			jTxtNroFactura.setEditable(false);
		}
		return jTxtNroFactura;
	}
	private JFormattedTextFieldPanambi getJTxtProveedor() {
		if (jTxtProveedor == null) {
			jTxtProveedor = new JFormattedTextFieldPanambi();
			jTxtProveedor.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtProveedor.setFocusable(false);
			jTxtProveedor.setEditable(false);
		}
		return jTxtProveedor;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setFocusable(false);
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbDetalleDeVenta() {
		if (lblpnmbDetalleDeVenta == null) {
			lblpnmbDetalleDeVenta = new JLabelPanambi();
			lblpnmbDetalleDeVenta.setText("Detalle de compra");
		}
		return lblpnmbDetalleDeVenta;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. Item","Descripcion del producto", "Costo unitario", "Cantidad","Total item"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(2, Double.class);
				types.put(3, Integer.class);
				types.put(4, Double.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(300);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JFormattedTextFieldPanambi getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JFormattedTextFieldPanambi();
			jTxtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFecha.setFocusable(false);
			jTxtFecha.setEditable(false);
		}
		return jTxtFecha;
	}
	private JFormattedTextFieldPanambi getJTxtSucursal() {
		if (jTxtSucursal == null) {
			jTxtSucursal = new JFormattedTextFieldPanambi();
			jTxtSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSucursal.setFocusable(false);
			jTxtSucursal.setEditable(false);
		}
		return jTxtSucursal;
	}
	private JFormattedTextFieldPanambi getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JFormattedTextFieldPanambi();
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}
	private JFormattedTextFieldPanambi getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JFormattedTextFieldPanambi();
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
		}
		return jTxtFechaAnulacion;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 801, 483);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGap(216)
								.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtProveedor(), GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
								.addGap(67)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
								.addGap(200)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGap(214)
								.addComponent(getLblpnmbPagoALos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtPagaderoDias(), GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getLblpnmbDas(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(24)
								.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtComentarios(), GroupLayout.PREFERRED_SIZE, 304, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(355)
								.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(41)
								.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 726, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(30, Short.MAX_VALUE))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbPagoALos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtPagaderoDias(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbDas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(9)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtComentarios(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)))
						.addGap(11)
						.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
						.addGap(27))
			);
			panel_1.setLayout(gl_panel_1);
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 523, 801, 38);
			panel_2.add(getBtnpnmbAceptar());
			panel_2.add(getBtnpnmbDetallesDePago());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbPagoALos() {
		if (lblpnmbPagoALos == null) {
			lblpnmbPagoALos = new JLabelPanambi();
			lblpnmbPagoALos.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPagoALos.setText("Pago a los  : ");
		}
		return lblpnmbPagoALos;
	}
	private JTextFieldInteger getJTxtPagaderoDias() {
		if (jTxtPagaderoDias == null) {
			jTxtPagaderoDias = new JTextFieldInteger();
			jTxtPagaderoDias.setEditable(false);
			jTxtPagaderoDias.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtPagaderoDias.setFocusable(false);
		}
		return jTxtPagaderoDias;
	}
	private JLabelPanambi getLblpnmbDas() {
		if (lblpnmbDas == null) {
			lblpnmbDas = new JLabelPanambi();
			lblpnmbDas.setText("d\u00EDas");
		}
		return lblpnmbDas;
	}
	private JButtonPanambi getBtnpnmbDetallesDePago() {
		if (btnpnmbDetallesDePago == null) {
			btnpnmbDetallesDePago = new JButtonPanambi();
			btnpnmbDetallesDePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doDetallePago();
				}
			});
			btnpnmbDetallesDePago.setToolTipText("Detalles del pago relacionado a la compra");
			btnpnmbDetallesDePago.setText("Detalles de pago");
		}
		return btnpnmbDetallesDePago;
	}
	
	private void doDetallePago(){
		try{
			Pago pago = new Pago();
			pago = new ControladorCompra().getPagoDeCompra(JFramePanambiMain.session.getConn(), compra);
			
			if(pago!=null){
				DlgDetallePagos detallePago = new DlgDetallePagos(pago);
				detallePago.centerIt();
				detallePago.setVisible(true);
			}else{
				DlgMessage.showMessage(getOwner(), "No existe pagos relacionados a la compra.", DlgMessage.ERROR_MESSAGE);
			}
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(),e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
}
