package py.com.panambi.swing.windows;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.DetalleCompra;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.controller.ControladorCompra;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import java.awt.Color;
import javax.swing.UIManager;

public class JIPagoProveedores extends JInternalFramePanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8203287115512554962L;
	private JLabelPanambi lblpnmbFechaAPagar;
	private JTextFieldDate jTxtDesde;
	private JLabelPanambi lblpnmbA;
	private JTextFieldDate jTxtHasta;
	private JLabelPanambi lblpnmbFacturaNro;
	private JTextFieldUpper jTxtFacturaNro;
	private JLabelPanambi lblpnmbProveedor;
	private JComboBoxPanambi comboBoxPanambi;
	private JButtonPanambi btnpnmbBuscar;
	private JScrollPane scrollPane;
	private JTablePanambi jTable;
	private List<Proveedor> proveedores = new ArrayList<Proveedor>();
	private ControladorProveedor controladorProveedor = new ControladorProveedor();
	private ControladorCompra controladorCompra = new ControladorCompra();
	private JScrollPane scrollPane_1;
	private JTablePanambi jTableDetalle;
	private JButtonPanambi jBtnPagar;
	private JLabelPanambi lblpnmbPresioneFPara;

	/**
	 * Create the frame.
	 */
	public JIPagoProveedores() throws Exception {
		initialize();
	}

	private void initialize() throws Exception {
		setMaximizable(false);
		setTitle("Pago a Proveedores");
		setBounds(100, 100, 746, 505);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.LEADING)
												.addGroup(
														groupLayout
																.createSequentialGroup()
																.addGroup(
																		groupLayout.createParallelGroup(Alignment.TRAILING, false).addComponent(getLblpnmbProveedor(), Alignment.LEADING, 0, 0, Short.MAX_VALUE).addComponent(getLblpnmbFacturaNro(), Alignment.LEADING, 0, 0, Short.MAX_VALUE)
																				.addComponent(getLblpnmbFechaAPagar(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 84, Short.MAX_VALUE))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(
																		groupLayout
																				.createParallelGroup(Alignment.LEADING)
																				.addGroup(
																						groupLayout.createSequentialGroup().addComponent(getJTxtDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(getLblpnmbA(), GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(getJTxtHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
																								.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE).addGap(9))
																				.addGroup(
																						groupLayout
																								.createSequentialGroup()
																								.addGroup(
																										groupLayout
																												.createParallelGroup(Alignment.LEADING)
																												.addComponent(getJTxtFacturaNro(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
																												.addGroup(
																														groupLayout.createSequentialGroup().addComponent(getComboBoxPanambi(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
																																.addComponent(getBtnpnmbBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
																																.addComponent(getJBtnPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
																								.addPreferredGap(ComponentPlacement.RELATED, 107, Short.MAX_VALUE))).addGap(11))
												.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup().addComponent(getScrollPane_1(), GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE).addContainerGap())
												.addGroup(groupLayout.createSequentialGroup().addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE).addContainerGap()))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addGroup(
												groupLayout
														.createSequentialGroup()
														.addGap(21)
														.addGroup(
																groupLayout
																		.createParallelGroup(Alignment.LEADING)
																		.addGroup(
																				groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbA(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																						.addComponent(getJTxtHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																		.addGroup(
																				groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbFechaAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																						.addComponent(getJTxtDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(
																groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbFacturaNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(getJTxtFacturaNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addComponent(getBtnpnmbBuscar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(
												groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(getComboBoxPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addComponent(getJBtnPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addGap(18)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(getScrollPane_1(), GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE).addContainerGap()));
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setShortcutsHere(this);
		setHelp("pagarCompra");
		doNuevo();
	}

	@SuppressWarnings("unchecked")
	private void doNuevo() {
		try {
			getJTxtFacturaNro().setText(null);
			poblarProveedores();
			getComboBoxPanambi().setModel(new ListComboBoxModel<Proveedor>(proveedores));
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JLabelPanambi getLblpnmbFechaAPagar() {
		if (lblpnmbFechaAPagar == null) {
			lblpnmbFechaAPagar = new JLabelPanambi();
			lblpnmbFechaAPagar.setText("Fecha a Pagar");
		}
		return lblpnmbFechaAPagar;
	}

	private JTextFieldDate getJTxtDesde() {
		if (jTxtDesde == null) {
			jTxtDesde = new JTextFieldDate();
		}
		return jTxtDesde;
	}

	private JLabelPanambi getLblpnmbA() {
		if (lblpnmbA == null) {
			lblpnmbA = new JLabelPanambi();
			lblpnmbA.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbA.setText("a");
		}
		return lblpnmbA;
	}

	private JTextFieldDate getJTxtHasta() {
		if (jTxtHasta == null) {
			jTxtHasta = new JTextFieldDate();
		}
		return jTxtHasta;
	}

	private JLabelPanambi getLblpnmbFacturaNro() {
		if (lblpnmbFacturaNro == null) {
			lblpnmbFacturaNro = new JLabelPanambi();
			lblpnmbFacturaNro.setText("Factura Nro.");
		}
		return lblpnmbFacturaNro;
	}

	private JTextFieldUpper getJTxtFacturaNro() {
		if (jTxtFacturaNro == null) {
			jTxtFacturaNro = new JTextFieldUpper();
		}
		return jTxtFacturaNro;
	}

	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setText("Proveedor");
		}
		return lblpnmbProveedor;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getComboBoxPanambi() {
		if (comboBoxPanambi == null) {
			comboBoxPanambi = new JComboBoxPanambi();
			comboBoxPanambi.setEditable(false);
			comboBoxPanambi.setModel(new ListComboBoxModel<Proveedor>(proveedores));
			comboBoxPanambi.setSelectedItem(null);
			comboBoxPanambi.setEditable(false);

		}
		return comboBoxPanambi;
	}

	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar (F5)");
		}
		return btnpnmbBuscar;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Factura", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setViewportView(getJTable());
		}
		return scrollPane;
	}

	private JTablePanambi getJTable() {
		if (jTable == null) {
			String[] columnNames = { "Fecha Compra", "Fecha a Pagar", "Proveedor", "Factura Nro.", "Total", "data" };
			jTable = new JTablePanambi(columnNames);
			jTable.getColumnModel().getColumn(0).setPreferredWidth(120);
			jTable.getColumnModel().getColumn(1).setPreferredWidth(100);
			jTable.getColumnModel().getColumn(2).setPreferredWidth(300);
			jTable.getColumnModel().getColumn(3).setPreferredWidth(100);
			jTable.getColumnModel().getColumn(4).setPreferredWidth(80);
			jTable.getColumnModel().getColumn(5).setMinWidth(0);
			jTable.getColumnModel().getColumn(5).setPreferredWidth(0);
			jTable.getColumnModel().getColumn(5).setMaxWidth(0);
			jTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (jTable.getSelectedRow() > -1) {
						setDetalle((Compra) jTable.getValueAt(jTable.getSelectedRow(), 5));
					}

				}
			});
		}
		return jTable;
	}

	private void setShortcutsHere(JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_F5) {
					doBuscar();
				}
				super.keyPressed(e);
			}
		});
		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}

	}

	private void doBuscar() {
		try {
			getJTable().resetData(0);
			getJTableDetalle().resetData(0);
			Proveedor proveedor = null;
			if (getComboBoxPanambi().getSelectedIndex() > 0) {
				proveedor = (Proveedor) getComboBoxPanambi().getSelectedItem();
			}
			List<Compra> compras = controladorCompra.listarComprasPendientes(JFramePanambiMain.session.getConn(), getJTxtDesde().getFecha(), getJTxtHasta().getFecha(), proveedor, getJTxtFacturaNro().getText());
			for (Compra compra : compras) {
				getJTable().addRow();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(compra.getFecha().getTime());
				gc.add(Calendar.DAY_OF_MONTH, compra.getPagaderodias());
				getJTable().setValueAt(compra.getFecha(), getJTable().getRowCount() - 1, 0);
				getJTable().setValueAt(new Date(gc.getTimeInMillis()), getJTable().getRowCount() - 1, 1);
				getJTable().setValueAt(compra.getProveedor().getRazonSocial(), getJTable().getRowCount() - 1, 2);
				getJTable().setValueAt(compra.getFacturanro(), getJTable().getRowCount() - 1, 3);
				getJTable().setValueAt(compra.getTotal(), getJTable().getRowCount() - 1, 4);
				getJTable().setValueAt(compra, getJTable().getRowCount() - 1, 5);

			}
			getJTable().changeSelection(0, 0, false, false);
			getJTableDetalle().changeSelection(0, 0, false, false);
			getJTable().requestFocus();
		} catch (ValidException e) {
			DlgMessage.showMessage(getOwner(), e.getMessage(), DlgMessage.INFORMATION_MESSAGE);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void poblarProveedores() throws Exception {
		proveedores = new ArrayList<Proveedor>();
		Proveedor proveedor = new Proveedor();
		proveedor.setRazonSocial("TODOS");
		List<Proveedor> listaprov = controladorProveedor.getProveedores(JFramePanambiMain.session.getConn());
		proveedores.add(proveedor);
		for (Proveedor prov : listaprov) {
			proveedores.add(prov);
		}
	}

	private void setDetalle(Compra compra) {
		getJTableDetalle().resetData(0);
		if (compra != null) {
			List<DetalleCompra> detalle = compra.getDetalleCompra();
			for (DetalleCompra detalleCompra : detalle) {
				getJTableDetalle().addRow();
				getJTableDetalle().setValueAt(detalleCompra.getNroitem(), getJTableDetalle().getRowCount() - 1, 0);
				getJTableDetalle().setValueAt(detalleCompra.getCantidad(), getJTableDetalle().getRowCount() - 1, 1);
				getJTableDetalle().setValueAt(detalleCompra.getProducto().getDescripcion(), getJTableDetalle().getRowCount() - 1, 2);
				getJTableDetalle().setValueAt(detalleCompra.getCostounitario(), getJTableDetalle().getRowCount() - 1, 3);
				getJTableDetalle().setValueAt(detalleCompra.getCostounitario() * detalleCompra.getCantidad(), getJTableDetalle().getRowCount() - 1, 4);
			}
		}
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Detalle Factura", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane_1.setViewportView(getJTableDetalle());
		}
		return scrollPane_1;
	}

	private JTablePanambi getJTableDetalle() {
		if (jTableDetalle == null) {
			String[] columnNames = { "Item", "Cantidad", "Producto", "Costo Unitario", "Costo Total" };
			jTableDetalle = new JTablePanambi(columnNames);
			jTableDetalle.getColumnModel().getColumn(0).setPreferredWidth(60);
			jTableDetalle.getColumnModel().getColumn(1).setPreferredWidth(60);
			jTableDetalle.getColumnModel().getColumn(2).setPreferredWidth(300);
			jTableDetalle.getColumnModel().getColumn(3).setPreferredWidth(150);
			jTableDetalle.getColumnModel().getColumn(4).setPreferredWidth(150);
		}
		return jTableDetalle;
	}

	private JButtonPanambi getJBtnPagar() {
		if (jBtnPagar == null) {
			jBtnPagar = new JButtonPanambi();
			jBtnPagar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagar();
				}
			});
			jBtnPagar.setText("Registrar Pago");
			jBtnPagar.setMnemonic('P');
		}
		return jBtnPagar;
	}

	private void doPagar() {
		try {
			if (getJTable().getSelectedRow() > -1) {
				Compra compra = (Compra) getJTable().getValueAt(getJTable().getSelectedRow(), 5);
				DlgPagos dlgPagos = new DlgPagos(getOwner(), compra.getTotal(), 5, 6);
				dlgPagos.centerIt();
				dlgPagos.setVisible(true);
				if (dlgPagos.getDetallePagos() == null) {
					throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
				}
				Pago pago = dlgPagos.getPago();
				pago.setCompra(compra);
				controladorCompra.registrarPagoCompra(JFramePanambiMain.session.getConn(), pago);
				DlgMessage.showMessage(getOwner(), "Su pago se registr� en forma satisfactoria", DlgMessage.INFORMATION_MESSAGE);
				doBuscar();
			} else {
				DlgMessage.showMessage(getOwner(), "Debe seleccionar una factura a pagar", DlgMessage.WARNING_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
