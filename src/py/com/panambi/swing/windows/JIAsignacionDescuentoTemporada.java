package py.com.panambi.swing.windows;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Temporada;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorTemporada;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class JIAsignacionDescuentoTemporada extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -825331484208368271L;
	private JLabelPanambi lblpnmbTemporada;
	private JLabelPanambi lblpnmbDescuento;
	private JComboBoxPanambi jCmbTemporada;
	private JFormattedTextField jFmTxtDescuento;
	private JButtonPanambi btnpnmbAplicar;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbLimpiar;
	private List<String> temporadas = new ArrayList<String>();
	private ControladorProducto controladorProducto = new ControladorProducto();
	private ControladorTemporada controladorTemporada = new ControladorTemporada();
	private Temporada temporada;
	private JLabelPanambi labelPanambi;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	private JLabel lblPresioneFPara;
	
	public JIAsignacionDescuentoTemporada() throws Exception {		
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setBounds(100, 100, 320, 296);
		setTitle("Asignacion de descuento por temporada");
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(23, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblPresioneFPara())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblpnmbDescuento(), 0, 0, Short.MAX_VALUE)
								.addComponent(getLblpnmbTemporada(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(getJFmTxtDescuento())
								.addComponent(getJCmbTemporada(), GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
							.addGap(6)))
					.addGap(30))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addComponent(getLblPresioneFPara())
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbTemporada(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbTemporada(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJFmTxtDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(33, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbAplicar());
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbSalir());

		  try {
	        	// Carga el fichero de ayuda 
	            fichero = new File("./bin/py/com/panambi/help/help_set.hs"); 
	        	hsURL = fichero.toURI().toURL();
				helpset = new HelpSet(getClass().getClassLoader(), hsURL);
	    	    hb = helpset.createHelpBroker();
	    	    // Pone ayuda a item de menu al pulsar F1. mntmIndice es el JMenuitem  
	    	    hb.enableHelpKey(getRootPane(),"asignardescuentostemporadas",helpset);
			} catch (Exception e) {
				//logger.error(getOwner(),e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
	      setShortcuts(this);
		  doLimpiar();
	}
	
	private JLabelPanambi getLblpnmbTemporada() {
		if (lblpnmbTemporada == null) {
			lblpnmbTemporada = new JLabelPanambi();
			lblpnmbTemporada.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTemporada.setText("Temporada : ");
		}
		return lblpnmbTemporada;
	}
	private JLabelPanambi getLblpnmbDescuento() {
		if (lblpnmbDescuento == null) {
			lblpnmbDescuento = new JLabelPanambi();
			lblpnmbDescuento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDescuento.setText("Descuento : ");
		}
		return lblpnmbDescuento;
	}
	private JComboBoxPanambi getJCmbTemporada() {
		if (jCmbTemporada == null) {
			jCmbTemporada = new JComboBoxPanambi();
		}
		return jCmbTemporada;
	}
	
	
	
	private JFormattedTextField getJFmTxtDescuento() {
		if (jFmTxtDescuento == null) {
			
			try{
				MaskFormatter mascara = new MaskFormatter("##.##");
				jFmTxtDescuento = new JFormattedTextField(mascara);
				jFmTxtDescuento.setToolTipText("Valor de descuento para la temporada seleccionada");
				jFmTxtDescuento.setHorizontalAlignment(SwingConstants.RIGHT);
				jFmTxtDescuento.setValue("00.00");
			}catch( Exception e ){
				logger.error(e.getMessage(),e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
			
			
		}
		return jFmTxtDescuento;
	}
	
	private void poblarListaTemporada() throws Exception {
		
		temporadas = new ArrayList<String>();
		List<Temporada> listatem= controladorTemporada.getTemporadasActivas(JFramePanambiMain.session.getConn());
		
		for (Temporada tem : listatem) {
			temporadas.add(tem.getNombre());
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try {
			poblarListaTemporada();

			getJCmbTemporada().setModel(new ListComboBoxModel<String>(temporadas));
			getJCmbTemporada().setSelectedItem(null);
			getJFmTxtDescuento().setValue("00.00");
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e,DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	private JButtonPanambi getBtnpnmbAplicar() {
		if (btnpnmbAplicar == null) {
			btnpnmbAplicar = new JButtonPanambi();
			btnpnmbAplicar.setMnemonic('A');
			btnpnmbAplicar.setToolTipText("Aplica descuento a temporada seleccionada");
			btnpnmbAplicar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAplicarDescuento();
				}
			});
			btnpnmbAplicar.setText("Aplicar");
		}
		return btnpnmbAplicar;
	}
	
	private void doAplicarDescuento() {
		try{
			
			Float descuento = Float.parseFloat((getJFmTxtDescuento().getValue().toString()))*1;
			
			if(jCmbTemporada.getSelectedItem()==null){
				DlgMessage.showMessage(getOwner(), "Debe seleccionar una temporada", DlgMessage.WARNING_MESSAGE);
			}else{
				
				String nombreTemporada =  jCmbTemporada.getSelectedItem().toString();
				int returnValue=JOptionPane.showConfirmDialog(this,"Confirma aplicar el "+descuento+" % de descuento a todos los productos con temporada "+nombreTemporada+" ?", 
						null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
				
				if (returnValue == JOptionPane.YES_OPTION){
					temporada = new ControladorTemporada().getTemporada(JFramePanambiMain.session.getConn(), nombreTemporada);
					//producto.setCodTemporada(temporada.getCodTemporada());
					controladorProducto.aplicarDescuentoTemporada(JFramePanambiMain.session.getConn(),  descuento , temporada.getCodTemporada()) ;
					DlgMessage.showMessage(getOwner(), "Operacion Exitosa", DlgMessage.INFORMATION_MESSAGE);
					//DlgMessage.showMessage(getOwner(), "Se aplico el descuento del "+descuento+" % a los productos con temporada "+temporada.getNombre(), DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
				}
				
			}
			
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Prepara pantalla para nueva operacion");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
}
