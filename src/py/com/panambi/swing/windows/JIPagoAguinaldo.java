package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.controller.ControladorPagoSalario;
import py.com.panambi.informes.JRDataSourceReciboPagoCuota;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.interfaces.ReciboBrowser;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.utils.PanambiUtils;


public class JIPagoAguinaldo extends JInternalFramePanambi implements Browseable, ReciboBrowser{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3311645412692147066L;
	private ControladorPagoSalario controladorPagoSalario = new ControladorPagoSalario();
	private List<PagoSalario> pagos;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
    private JTextFieldDate txtFechaDesde;
    private JLabelPanambi lblpnmbPeriodo;
    private JTextFieldDate txtFechaHasta;
    private JScrollPane scrollPane;
    private JTablePanambi table;
    private JButtonPanambi btnpnmbPagar;
    private JTextFieldInteger jTxtPagado;
    private JTextFieldInteger jTxtTotal;
    private JLabelPanambi lblpnmbPagados;
    private JLabelPanambi lblpnmbDe;
    private JLabelPanambi labelPanambi;

	/**
	 * Create the frame.
	 */
	public JIPagoAguinaldo() throws Exception{

		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Pago de aguinaldos");
		setBounds(100, 100, 889, 505);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 849, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPeriodo(), GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
							.addGap(29)
							.addComponent(getTxtFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(51)
							.addComponent(getTxtFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(137)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbPagados(), Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbDe(), GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
							.addGap(25)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(getJTxtTotal(), 0, 0, Short.MAX_VALUE)
								.addComponent(getJTxtPagado(), GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE))
							.addGap(100)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
							.addGap(46)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblpnmbPeriodo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getTxtFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getTxtFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbPagados(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbDe(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(7)
					.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbGenerar());

		doLimpiar();
		setShortcuts(this);
		setHelp("generarPagoAguinaldo");
		decorate();
		
		getJPanelSouth().add(getBtnpnmbPagar());
		getJPanelSouth().add(getBtnpnmbSalir());
		getJPanelCentral().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getTxtFechaDesde(), getTxtFechaHasta(), getJTxtTotal(), getJTxtPagado(), getTable(), getScrollPane(), getLblpnmbPeriodo(), getLblpnmbPagados(), getLblpnmbDe()}));
	}
	
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setToolTipText("Salir");
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	
	private JButtonPanambi getBtnpnmbGenerar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setToolTipText("Genera planilla");
			btnpnmbBuscar.setMnemonic('G');
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerar();
				}
			});
			btnpnmbBuscar.setText("Generar");
		}
		return btnpnmbBuscar;
	}
	
	private void doGenerar(){
		//Preguntar si ya hay para el periodo seleccionado, si ya hay, mostrar eso, sino, crear
		Date fechaDesde = getTxtFechaDesde().getFecha();
		Date fechaHasta = getTxtFechaHasta().getFecha();
		
		String texto = " generar ";
		
		if(getBtnpnmbGenerar().getText().equals("Actualizar")){
			texto = " actualizar ";
		}
			
		try {
			int ret = JOptionPane.showConfirmDialog(getOwner(), "<html>Est&aacute seguro que desea "+texto+" la planilla de aguinaldos?.", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if(ret == JOptionPane.YES_OPTION){
				 controladorPagoSalario.generarPagoAguinaldo(JFramePanambiMain.session.getConn(),fechaDesde, fechaHasta);
				 doLimpiar();
			}
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	private void doLimpiar(){
		try {
			Date fechaDesde = getTxtFechaDesde().getFecha();
			Date fechaHasta = getTxtFechaHasta().getFecha();
			Integer contadorTotal = 0;
			Integer contadorPagados =0;
			pagos = controladorPagoSalario.getPagosSalario(JFramePanambiMain.session.getConn(),fechaDesde, fechaHasta, "A");
			
			contadorTotal = pagos.size();
			getTable().resetData(0);
			Integer i=1;
			for (PagoSalario pago : pagos) {
				getBtnpnmbGenerar().setText("Actualizar");
				getBtnpnmbPagar().setEnabled(true);
				getTable().addRow();
				getTable().setValueAt(i, getTable().getRowCount() - 1, 0);
				getTable().setValueAt(pago.getCodempleado(), getTable().getRowCount() - 1, 1);
				getTable().setValueAt(pago.getNrodocumento(), getTable().getRowCount() - 1, 2);
				getTable().setValueAt(pago.getNombre(), getTable().getRowCount() - 1, 3);
				getTable().setValueAt(pago.getSalarionominal(), getTable().getRowCount() - 1, 4);
				getTable().setValueAt(pago.getTotalpagado(), getTable().getRowCount() - 1, 5);
				String estado = "No pagado";
				if(pago.getEstado().equals("P")){
					estado = "Pagado";
				}
				getTable().setValueAt(estado, getTable().getRowCount() - 1, 6);
				
				if(pago.getEstado().equals("P")){
					contadorPagados++;
				}
				i++;
			}
			
			getJTxtPagado().setValor(contadorPagados);
			getJTxtTotal().setValor(contadorTotal);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof PerfilUsuario) {
		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	
	private void decorate() {
	}
	private JTextFieldDate getTxtFechaDesde() {
		if (txtFechaDesde == null) {
			txtFechaDesde = new JTextFieldDate();
			txtFechaDesde.setOpaque(false);
			txtFechaDesde.setFont(new Font("Dialog", Font.BOLD, 11));
			txtFechaDesde.setEditable(false);
			doCargarFechaDesde();
		}
		return txtFechaDesde;
	}
	
	private void doCargarFechaDesde() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDateOfPreviousMonth = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fechaDesde = df.format(firstDateOfPreviousMonth);
		
		getTxtFechaDesde().setText(fechaDesde);
		
	}
	private JLabelPanambi getLblpnmbPeriodo() {
		if (lblpnmbPeriodo == null) {
			lblpnmbPeriodo = new JLabelPanambi();
			lblpnmbPeriodo.setText("Periodo: ");
			lblpnmbPeriodo.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbPeriodo;
	}
	private JTextFieldDate getTxtFechaHasta() {
		if (txtFechaHasta == null) {
			txtFechaHasta = new JTextFieldDate();
			txtFechaHasta.setForeground(UIManager.getColor("Button.foreground"));
			txtFechaHasta.setFont(new Font("Dialog", Font.BOLD, 11));
			txtFechaHasta.setEditable(false);
			doCargarFechaHasta();
		}
		return txtFechaHasta;
	}
	
	private void doCargarFechaHasta() {
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		Date lastDateOfPreviousMonth = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHasta = df.format(lastDateOfPreviousMonth);
		getTxtFechaHasta().setText(fechaHasta);
		
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Planilla de aguinaldos a pagar", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	private JTablePanambi getTable() {
		if (table == null) {
			
			String[] columnNames = {"Numero", "Cod. Empleado", "Documento", "Nombre", "Salario Nominal", "Total a pagar", "Estado"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, Integer.class);
			types.put(5, Integer.class);
			Integer[] editable = { 0,5 };
			
			table = new JTablePanambi(columnNames, editable, types);
			table.setRowHeight(20);
			table.getColumnModel().getColumn(0).setPreferredWidth(5);
			table.getColumnModel().getColumn(1).setPreferredWidth(5);
			table.getColumnModel().getColumn(2).setPreferredWidth(10);
			table.getColumnModel().getColumn(3).setPreferredWidth(150);
			table.getColumnModel().getColumn(4).setPreferredWidth(80);
			table.getColumnModel().getColumn(5).setPreferredWidth(40);
			table.getColumnModel().getColumn(6).setPreferredWidth(40);
			
		}
		return table;
	}
	
	private JButtonPanambi getBtnpnmbPagar() {
		if (btnpnmbPagar == null) {
			btnpnmbPagar = new JButtonPanambi();
			btnpnmbPagar.setToolTipText("Realizar Pagos");
			btnpnmbPagar.setMnemonic('R');
			btnpnmbPagar.setEnabled(false);
			btnpnmbPagar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRealizarPagos();
				}
			});
			btnpnmbPagar.setText("Realizar Pagos");
		}
		return btnpnmbPagar;
	}
	
	private void doRealizarPagos(){
		DlgRealizarPagos dlgRealizarPagos = new DlgRealizarPagos(pagos, "aguinaldo", this);
		dlgRealizarPagos.addWindowListener(new WindowAdapter() {
			
			public void windowClosed(WindowEvent e){
				doLimpiar();
			}
		});
		dlgRealizarPagos.centerIt();
		dlgRealizarPagos.setVisible(true);
	}
	private JTextFieldInteger getJTxtPagado() {
		if (jTxtPagado == null) {
			jTxtPagado = new JTextFieldInteger();
			jTxtPagado.setEditable(false);
		}
		return jTxtPagado;
	}
	private JTextFieldInteger getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldInteger();
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbPagados() {
		if (lblpnmbPagados == null) {
			lblpnmbPagados = new JLabelPanambi();
			lblpnmbPagados.setText("Total");
			lblpnmbPagados.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblpnmbPagados;
	}
	private JLabelPanambi getLblpnmbDe() {
		if (lblpnmbDe == null) {
			lblpnmbDe = new JLabelPanambi();
			lblpnmbDe.setText("Pagados");
			lblpnmbDe.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblpnmbDe;
	}
	
	public void doRecibo(boolean generar, String titular, String concepto, String montoLetras, Double monto, Integer nrorecibo, Double vuelto) {
		// TODO Auto-generated method stub
		
		if(generar ==true){
			try{
			PanambiUtils.setWaitCursor(getOwner());
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularCompra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", monto);
		
			parameters.put("CLIENTE", titular);

			parameters.put("DESCRIPCION", concepto);
			parameters.put("MONTOLETRAS", montoLetras);
			
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
	
			JasperPrint jasperPrint;
	
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceReciboPagoCuota(JFramePanambiMain.session.getConn(),nrorecibo));
		
			List<?> pages = jasperPrint.getPages();
		
			if (!pages.isEmpty()) {
				String pdffilename = System.getProperty("java.io.tmpdir") + "\\tmp_panambi_" + System.nanoTime() + ".pdf";
				JasperExportManager.exportReportToPdfFile(jasperPrint, pdffilename);
				Desktop desk = Desktop.getDesktop();
				desk.open(new File(pdffilename));
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
		}
		
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Presione F1 para Ayuda");
			labelPanambi.setForeground(Color.GRAY);
		}
		return labelPanambi;
	}
}
