package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorDeudaEmpleado;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarAnularDeudaEmpleado extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnAnular;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JComboBoxPanambi jCmbEstado;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private List<String> sucursales = new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private JPopupMenu popupMenu;
	private JMenuItem menuItemDetallePago;
	private JMenuItem menuItemLiquidacion;
	private JMenuItem menuItemAnular;
	private JButtonPanambi jBtnConsultar;
	private ControladorEmpleado controladorEmpleado= new ControladorEmpleado();
	private JPanel panel;
	private JTextFieldUpper jTxtNombreApellido;
	private JLabelPanambi lblpnmbEmpleado;
	private JLabelPanambi labelPanambi_1;
	private JTextField jTxtNroCedula;
	private JRadioButton rdbtnTodos;
	private Empleado empleado;
	private JButtonPanambi btnpnmbExaminar;
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbSucursal;
	
	public JIConsultarAnularDeudaEmpleado() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar/Anular deudas de empleados");
		setBounds(100,100, 852, 505);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 792, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 392, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 155, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
									.addGap(33))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
									.addGap(21)))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(19)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
					.addGap(20))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		agregarMenuPoput();
		setShortcuts(this);
		setHelp("consultarAnularDeudaEmpleado");
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
		popupMenu.add(getMenuItemLiquidacion());
		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			empleado = null;
			estados=null;
			poblarListaEstados();
			poblarListaSucursales();
			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJTxtNroCedula().setText("");
			getJTxtNombreApellido().setText("");
			
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			
			if(getTablePanambi().getRowCount()>0){
				getTablePanambi().resetData(0);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("PAGADOS");
		estados.add("NO PAGADOS");
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		sucursales.add("TODAS");
		List<Sucursal> listaSucursal = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetallePago == null) {
			menuItemDetallePago = new JMenuItem("Ver detalles del pago...");
			menuItemDetallePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetallesPago();
				}
			});
		}
		return menuItemDetallePago; 
	}
	
	private JMenuItem getMenuItemLiquidacion(){
		if (menuItemLiquidacion == null) {
			menuItemLiquidacion = new JMenuItem("Ver detalle de liquidacion asociada...");
			menuItemLiquidacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetallesLiquidacion();
				}
			});
		}
		return menuItemLiquidacion; 
	}
	
	
	private void doVerDetallesLiquidacion(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro.", DlgMessage.WARNING_MESSAGE);
			}else{
				//"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total pagado", "Estado","Data"};
				DeudaEmpleado deudaDet = (DeudaEmpleado)getTablePanambi().getValueAt(fila, 5);
				
				DlgDetallePagoEmpleado dlgDetalleLiquidacion= new DlgDetallePagoEmpleado(deudaDet.getPagoEmpleado());
				dlgDetalleLiquidacion.centerIt();
				dlgDetalleLiquidacion.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private void doVerDetallesPago(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro.", DlgMessage.WARNING_MESSAGE);
			}else{
				//"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total pagado", "Estado","Data"};
				DeudaEmpleado deudaDet= (DeudaEmpleado)getTablePanambi().getValueAt(fila, 5);
				Pago pagoDet = new ControladorPago().getPagoDeuda(JFramePanambiMain.session.getConn(), deudaDet);
				if(deudaDet.getEstado().equals("NP")){
					DlgMessage.showMessage(getOwner(), "No es posible visualizar detalles de un pago a empleado con estado NO PAGADO.", DlgMessage.ERROR_MESSAGE);
				}else{
					DlgDetallePagos dlgDetallePago= new DlgDetallePagos(pagoDet);
					dlgDetallePago.centerIt();
					dlgDetallePago.setVisible(true);
				}
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JMenuItem getMenuItemAnular(){
		if (menuItemAnular == null) {
			menuItemAnular = new JMenuItem("Anular");
			menuItemAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
		}
		return menuItemAnular; 
	}
	
	private JButtonPanambi getJBtnAnular() {
		if (jBtnAnular == null) {
			jBtnAnular = new JButtonPanambi();
			jBtnAnular.setMnemonic('A');
			jBtnAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
			jBtnAnular.setToolTipText("Anular el pago de la deuda de empleado");
			jBtnAnular.setText("Anular pago de deuda");
		}
		return jBtnAnular;
	}
	
	
	private void doAnular(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Imposible anular.\nDebe seleccionar un regitro", DlgMessage.ERROR_MESSAGE);
			}else{
				DeudaEmpleado deudaNul= (DeudaEmpleado)getTablePanambi().getValueAt(fila, 5);
				
				if(deudaNul.getEstado().equals("NP")){
					DlgMessage.showMessage(getOwner(),"Imposible anular el pago de la deuda del empleado.\nLa deuda no posee pagos.", DlgMessage.ERROR_MESSAGE);
				}else{
					Pago pago = new ControladorPago().getPagoDeuda(JFramePanambiMain.session.getConn(), deudaNul);
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					if (!sdf.format(new java.util.Date()).equals(sdf.format(pago.getFecha()))){
						DlgMessage.showMessage(getOwner(), "Imposible anular pago de la deuda del empleado.\nS�lo es posible anularlo el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
					}else{
							
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la anulaci�n del pago de la deuda del empleado ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (ret == JOptionPane.YES_OPTION) {
							new ControladorDeudaEmpleado().anularPagoDeudaEmpleado(JFramePanambiMain.session.getConn(),deudaNul);
							DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa.", DlgMessage.INFORMATION_MESSAGE);
							doConsultarDeudaEmpleado();
							}
						}
					}
				}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			
			//tablePanambi = new JTablePanambi((String[]) null);
			String[] columnNames = {"C�digo","Empleado", "Concepto", "Monto","Estado de deuda","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(3, Double.class);
			Integer[] editable = { };
			tablePanambi= new JTablePanambi(columnNames,editable,types);
//			tablePanambi.addMouseListener(new MouseAdapter() {
//				@Override
//				public void mouseClicked(MouseEvent e) {
//					doMenuEnableDisnable();
//				}
//			});			
			tablePanambi.setRowHeight(20);
						
			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(200);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(150);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(40);
			
			tablePanambi.getColumnModel().getColumn(5).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(5).setMaxWidth(0);
			
			tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
		}
		return tablePanambi;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Consultar deudas de emplados");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarDeudaEmpleado();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	private void doConsultarDeudaEmpleado(){
	
		try{
			String estado ;
			String nombreSucursal ;
			
			Sucursal sucursal = null;
			
			nombreSucursal = (String)getJCmbSucursal().getSelectedItem();
			
			estado = (String)getJCmbEstado().getSelectedItem();
			
			if(nombreSucursal.equals("TODAS")){
				sucursal = null;
			}else{
				sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}
			
			if(estado.equals("TODOS")){
				estado = null;
			}else if(estado.equals("PAGADOS")){
				estado = "P";
			}else{
				estado = "NP";
			}	
			
			List<DeudaEmpleado> listaDeudasEmpleados= new ControladorDeudaEmpleado().getDeudasEmpleados(JFramePanambiMain.session.getConn(), empleado, sucursal , estado);
				if(listaDeudasEmpleados.size()==0){
					DlgMessage.showMessage(getOwner(), "Ning�n pago encontrado", DlgMessage.INFORMATION_MESSAGE);
				}
				
				Iterator<DeudaEmpleado> iteratorDeudaEmpleado= listaDeudasEmpleados.listIterator();
//				
				if(getTablePanambi().getRowCount()!=0){
					getTablePanambi().resetData(0);
				}
				
				while (iteratorDeudaEmpleado.hasNext()) {
					
					getTablePanambi().addRow();
					DeudaEmpleado deuda = (DeudaEmpleado) iteratorDeudaEmpleado.next();
					//"C�digo","Empleado", "Concepto", "Monto","Estado de deuda","Data"};
					Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), deuda.getPagoEmpleado().getCodempleado());
					getTablePanambi().setValueAt(deuda.getCodDeudaEmpleado(), getTablePanambi().getRowCount()-1, 0);
					getTablePanambi().setValueAt(emp.getNombre()+" "+emp.getApellido(), getTablePanambi().getRowCount()-1, 1);
					getTablePanambi().setValueAt(deuda.getConcepto(), getTablePanambi().getRowCount()-1, 2);
					getTablePanambi().setValueAt(deuda.getMonto(), getTablePanambi().getRowCount()-1, 3);
					
					String est = "";
					if(deuda.getEstado().equals("P")){
						est = "Pagado";
					}else{
						est = "No pagado";
					}
					getTablePanambi().setValueAt(est, getTablePanambi().getRowCount()-1, 4);
					getTablePanambi().setValueAt(deuda, getTablePanambi().getRowCount()-1, 5);

				}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	
		
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
		}
		return jCmbEstado;
		
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Informacion del empleado");
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(10)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
								.addGap(9)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(getRdbtnTodos(), GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(20)
								.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 347, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(24, Short.MAX_VALUE))
					.addComponent(getLblpnmbEmpleado(), GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getRdbtnTodos())
								.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(6)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setToolTipText("Nombre y apellido del empleado");
			jTxtNombreApellido.setText("");
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setFocusable(false);
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return jTxtNombreApellido;
	}
	private JLabelPanambi getLblpnmbEmpleado() {
		if (lblpnmbEmpleado == null) {
			lblpnmbEmpleado = new JLabelPanambi();
			lblpnmbEmpleado.setText("Empleado");
			lblpnmbEmpleado.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbEmpleado.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbEmpleado;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("Nro. Cedula : ");
			labelPanambi_1.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return labelPanambi_1;
	}
	private JTextField getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextField();
			jTxtNroCedula.setText("");
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtNroCedula.setColumns(10);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroCedula();
				}
			});

		}
		return jTxtNroCedula;
	}
	
private void lostFocusNroCedula(){
		
		try{
			Integer longitud = 0;
			try{
				longitud = getJTxtNroCedula().getText().length();
			}catch(Exception e){
				longitud = 0;
			}
			if(longitud==0){
				empleado = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Empleado emp =  controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), getJTxtNroCedula().getText());
				
				if(emp != null){
					setValues(emp, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					doBuscarEmpleados();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doBuscarEmpleados(){
			try {
				getJTxtNroCedula().setText("");
				getJTxtNombreApellido().setText("");
				String[] columnNames = { "C�digo", "Nro Documento", "Nombre","Apellido", "Cargo","Sucursal","Estado"};
				String[] pks = { "codEmpleado" };
				String sSelect = "SELECT codempleado, nrodocumento, nombres, apellidos, ";
				sSelect += "(SELECT descripcion FROM cargosempleados WHERE empleados.codcargoempleado = cargosempleados.codcargoempleado),  ";
				sSelect += "(SELECT nombre FROM sucursales WHERE empleados.codsucursal= sucursales.codsucursal), ";
				sSelect += " (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
				sSelect += "FROM empleados ";
				sSelect += "ORDER BY nombres";
				JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Empleado", pks);
				jb.setVisible(true);
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
	}

	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosEmpleados();
				}
			});
			rdbtnTodos.setToolTipText("Todos los empleados");
			rdbtnTodos.setSelected(true);
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodosEmpleados(){
		getRdbtnTodos().setSelected(true);
		empleado = null;
		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText(null);
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Empleado) {
			this.empleado= (Empleado)obj;
			getRdbtnTodos().setSelected(false);
			getJTxtNombreApellido().setText(empleado.getNombre()+" "+empleado.getApellido());
			getJTxtNroCedula().setText(empleado.getNroDocumento());
			
		}
	}
	
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JButtonPanambi getBtnpnmbExaminar() {
		if (btnpnmbExaminar == null) {
			btnpnmbExaminar = new JButtonPanambi();
			btnpnmbExaminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doBuscarEmpleados();
				}
			});
			btnpnmbExaminar.setToolTipText("Examinar empleado");
			btnpnmbExaminar.setText("Examinar ...");
		}
		return btnpnmbExaminar;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal : ");
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbSucursal;
	}
}
