package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarDevolucion extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JComboBoxPanambi jCmbEstado;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private List<String> estados = new ArrayList<String>();
	private List<String> sucursales = new ArrayList<String>();
	private JPopupMenu popupMenu;
	private JMenuItem menuItemDetalles;
	//private JMenuItem menuItemAnular;
	private JButtonPanambi jBtnConsultar;
	private JPanel panel;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi labelPanambi_1;
	private JTextField jTxtNroCedula;
	private JRadioButton rdbtnTodos;
	private JTextFieldUpper jTxtNombreApellido;
	private JLabelPanambi lblpnmbSucuesal;
	private JComboBoxPanambi jCmbSucursal;
	private Cliente cliente;
	private ControladorCliente controladorCliente = new ControladorCliente();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private ControladorDevolucion controladorDevolucion= new ControladorDevolucion();
	
	
	public JIConsultarDevolucion() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar devoluciones");
		setBounds(100,100, 869, 505);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucuesal(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getJCmbSucursal(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbEstado(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
							.addGap(28)
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(28, Short.MAX_VALUE))))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(71)
					.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 190, Short.MAX_VALUE)
					.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addGap(132))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucuesal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
					.addGap(23))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		agregarMenuPoput();
		setShortcuts(this);
		setHelp("consultarDevolucion");
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
//		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			poblarListaEstados();
			poblarListaSucursales();
			
			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
			getJCmbEstado().setSelectedItem("TODOS");
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			cliente = null;
			getRdbtnTodos().setSelected(true);
			getJTxtNroCedula().setText("");
			getJTxtNombreApellido().setText("");
			
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			
			if(getTablePanambi().getRowCount()>0){
				getTablePanambi().resetData(0);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaSucursales() throws Exception{
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSuc = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("ACTIVOS");
		estados.add("ANULADOS");
	}
	
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver mas detalles...");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetalles();
				}
			});
		}
		return menuItemDetalles; 
	}
	
	private void doVerDetalles(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
			}else{
				
				Devolucion devolucionDet = (Devolucion)getTablePanambi().getValueAt(fila, 7);
				
				DlgDetalleDevoluciones dlgDetalleDevolucion = new DlgDetalleDevoluciones(devolucionDet);
				dlgDetalleDevolucion.centerIt();
				dlgDetalleDevolucion.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			//tablePanambi = new JTablePanambi((String[]) null);
			String[] columnNames = {"Nro. Factura","Cliente", "Fecha devoluci�n","Monto ","Sucursal","Estado","Nota cr�dito","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(2, Date.class);
			types.put(3, Double.class);
			types.put(6, Double.class);
			Integer[] editable = { };
			tablePanambi= new JTablePanambi(columnNames,editable,types);
						
			tablePanambi.setRowHeight(20);
		
			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(30);
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(150);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(50);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(40);
			
			tablePanambi.getColumnModel().getColumn(7).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setMaxWidth(0);
//			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(2);
//			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(250);
//			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
//			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha comienzo");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Consultar devoluciones");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarVentas();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	@SuppressWarnings("deprecation")
	private void doConsultarVentas(){
		try{
			Sucursal sucu;
			String sucursal;
			String estado ;
			Date fechaDesde ;
			Date fechaHasta ;

			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			sucursal = (String) getJCmbSucursal().getSelectedItem(); 
			
			if(!sucursal.equals("TODAS")){
				sucu = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), sucursal);
			}else sucu = null;
			
			
			estado = (String)getJCmbEstado().getSelectedItem();
				
			if(estado.equals("TODOS")){
				estado = null;
			}else if(estado.equals("ACTIVOS")){
				estado = "A";
			}else{
				estado = "I";
			}
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}	
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				List<Devolucion> listaDevoluciones= controladorDevolucion.getDevoluciones(JFramePanambiMain.session.getConn(), cliente, sucu, estado, fechaDesde, fechaHasta);
				if(listaDevoluciones.size()==0){
					DlgMessage.showMessage(getOwner(), "Ninguna devoluci�n encontrada.", DlgMessage.INFORMATION_MESSAGE);
				}
				Iterator<Devolucion> iteratorDevoluciones= listaDevoluciones.listIterator();
				if(getTablePanambi().getRowCount()!=0){
					getTablePanambi().resetData(0);
				}
				
				while (iteratorDevoluciones.hasNext()) {
					
					getTablePanambi().addRow();
					
					Devolucion dev = (Devolucion) iteratorDevoluciones.next();
					//"Nro. Factura","Cliente", "Fecha devoluci�n","Monto ","Sucursal","Estado","Nota cr�dito","Data"};
					getTablePanambi().setValueAt(dev.getVenta().getNroComprobante(), getTablePanambi().getRowCount()-1, 0);
					getTablePanambi().setValueAt(dev.getCliente().getNombres()+" "+dev.getCliente().getApellidos(), getTablePanambi().getRowCount()-1, 1);
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					
					//getJTxtFechaAnulacion().setText(formato.format(gasto.getFechaAnulacion()));
					
					getTablePanambi().setValueAt(formato.format(dev.getFecha()), getTablePanambi().getRowCount()-1, 2);
					
					getTablePanambi().setValueAt(dev.getTotal(), getTablePanambi().getRowCount()-1, 3);
					getTablePanambi().setValueAt(dev.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 4);
					
					if(dev.getEstadoDevolucion().equals("A")){
						getTablePanambi().setValueAt("Activo", getTablePanambi().getRowCount()-1, 5);
					}else{
						getTablePanambi().setValueAt("Anulado", getTablePanambi().getRowCount()-1, 5);
					}
					
					getTablePanambi().setValueAt(dev.getNotaCredito(), getTablePanambi().getRowCount()-1, 6);
					
					getTablePanambi().setValueAt(dev, getTablePanambi().getRowCount()-1, 7);
				}	
			}
			
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	 private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
		}
		return jCmbEstado;
		
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Informacion del cliente");
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGap(0, 330, Short.MAX_VALUE)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(getLabelPanambi(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
							.addGap(10)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
							.addGap(9)
							.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(24)
							.addComponent(getRdbtnTodos(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getRdbtnTodos())
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Cliente");
			labelPanambi.setHorizontalAlignment(SwingConstants.CENTER);
			labelPanambi.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return labelPanambi;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("Nro. Cedula : ");
			labelPanambi_1.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return labelPanambi_1;
	}
	private JTextField getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextField();
			jTxtNroCedula.setText("");
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtNroCedula.setColumns(10);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroCedula();
				}
			});
		}
		return jTxtNroCedula;
	}
	
	private void lostFocusNroCedula(){
		
		try{
			Integer longitud = 0;
			try{
				longitud = getJTxtNroCedula().getText().length();
			}catch(Exception e){
				longitud = 0;
			}
			if(longitud==0){
				cliente = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Cliente cli =  controladorCliente.getCliente(JFramePanambiMain.session.getConn(), getJTxtNroCedula().getText());
				
				if(cli != null){
					setValues(cli, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					doBuscarCliente();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doBuscarCliente(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nro. de c�dula", "Nombres", "Apellidos" };
			String[] pks = { "codCliente" };
			String sSelect = "SELECT codcliente, nrodocumento, nombres, apellidos ";
			sSelect += "FROM clientes ";
			sSelect += "WHERE codcliente in (SELECT codcliente FROM devoluciones)";
			sSelect += "ORDER BY nrodocumento";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Cliente", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectTodosClientes();
				}
			});
			
			rdbtnTodos.addKeyListener(new java.awt.event.KeyAdapter() {

				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER ) {
						doSelectTodosClientes();
					}
				}
			});
			
			rdbtnTodos.setToolTipText("Todos los clientes");
			rdbtnTodos.setSelected(true);
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodosClientes(){
		getRdbtnTodos().setSelected(true);
		cliente = null;
		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText(null);
	}
	
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setToolTipText("Nombre y apellido del cliente");
			jTxtNombreApellido.setText("");
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setFocusable(false);
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return jTxtNombreApellido;
	}
	private JLabelPanambi getLblpnmbSucuesal() {
		if (lblpnmbSucuesal == null) {
			lblpnmbSucuesal = new JLabelPanambi();
			lblpnmbSucuesal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucuesal.setText("Sucural : ");
		}
		return lblpnmbSucuesal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Cliente) {
			this.cliente = (Cliente)obj;
			getRdbtnTodos().setSelected(false);
			getJTxtNombreApellido().setText(cliente.getNombres()+" "+cliente.getApellidos());
			getJTxtNroCedula().setText(cliente.getNroDocumento());
			
		}
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
}
