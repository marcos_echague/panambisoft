
package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.TipoDescuentoSalario;
import py.com.panambi.controller.ControladorTipoDescuentoSalario;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldFloat;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;

public class JITipoDescuentoSalario extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7543484558078226942L;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbNombre;
	private JTextFieldInteger jTxtCodTipoDescuento;
	private JComboBoxPanambi jCmbTipoDescuento;
	private TipoDescuentoSalario tipoDescuento;
	private List<String> tiposDescuentos = new ArrayList<>();
	private ControladorTipoDescuentoSalario controladorTipoDescuentoSalario = new ControladorTipoDescuentoSalario();
	private JCheckBox chckbxActivo;
	private JRadioButton rdbtnPorcentual;
	private JRadioButton rdbtnFijo;
	private JLabel lblPresioneFPara;
    private JTextFieldDouble jTxtMontoFijo;
    private JLabelPanambi lblpnmbFormaDeAplicacion;
    private JRadioButton rdbtnMasiva;
    private JRadioButton rdbtnPersonal;
    private JTextFieldFloat jTxtPorcentaje;

	/**
	 * Create the frame.
	 */
	public JITipoDescuentoSalario() throws Exception{
		super();
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Tipo de Descuento Salarial");
		setBounds(100, 100, 366, 415);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		
		
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getJTxtCodTipoDescuento(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addGap(60)
					.addComponent(getChckbxActivo()))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getJCmbTipoDescuento(), GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addComponent(getLblpnmbFormaDeAplicacion(), GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(getRdbtnMasiva())
					.addGap(12)
					.addComponent(getRdbtnPersonal()))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addComponent(getRdbtnPorcentual())
					.addGap(18)
					.addComponent(getJTxtPorcentaje(), GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(146, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addComponent(getRdbtnFijo())
					.addGap(52)
					.addComponent(getJTxtMontoFijo(), GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(193)
					.addComponent(getLblPresioneFPara(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(38))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getLblPresioneFPara(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(19)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodTipoDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getChckbxActivo()))
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbTipoDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbFormaDeAplicacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getRdbtnMasiva())
						.addComponent(getRdbtnPersonal()))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getRdbtnPorcentual())
						.addComponent(getJTxtPorcentaje(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getRdbtnFijo())
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getJTxtMontoFijo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(33))
		);
		getJPanelCentral().setLayout(groupLayout);
		decorate();
		setHelp("tiposdescuentossalarios");
		setShortcuts(this);
		doLimpiar();
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar tipo de descuento salarial");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	

	private void doEliminar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la eliminacion de forma permantente\n del tipo de descuento salarial? ", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				if(!controladorTipoDescuentoSalario.comprobarReferencia(JFramePanambiMain.session.getConn(), tipoDescuento)){
					controladorTipoDescuentoSalario.borrarTipoDescuento(JFramePanambiMain.session.getConn(), tipoDescuento);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbTipoDescuento().requestFocus();
					
				} else DlgMessage.showMessage(getOwner(), "Imposible eliminar tipo de descuento /nEl tipo de descuento esta siendo utilizado", DlgMessage.ERROR_MESSAGE);
				
			}
			
			getJCmbTipoDescuento().requestFocus();
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar tipo de descuento salarial");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar(){

		try{
			if (tipoDescuento== null) {
				tipoDescuento= new TipoDescuentoSalario();

				
				if(getRdbtnMasiva().isSelected() && getRdbtnPorcentual().isSelected() && (Float)getJTxtPorcentaje().getValue() == (float)0){
					
					getJTxtPorcentaje().requestFocus();
					DlgMessage.showMessage(getOwner(),"No se admite porcentaje del 0.0 % para un descuento porcentual\n"
							+ "Favor ingrese un porcentaje diferente del 0.0 % ", DlgMessage.WARNING_MESSAGE);
					
					tipoDescuento = null;
				}else if(getRdbtnMasiva().isSelected() && getRdbtnFijo().isSelected() && (Double)getJTxtMontoFijo().getValue()==0.0){
					
					getJTxtMontoFijo().requestFocus();
					DlgMessage.showMessage(getOwner(),"No se admite monto del 0 Gs. \n"
							+ "Favor ingrese un monto fijo diferente de 0", DlgMessage.WARNING_MESSAGE);
					
					tipoDescuento = null;
				}else{
					cargarValores();
					controladorTipoDescuentoSalario.insertarTipoDescuento(JFramePanambiMain.session.getConn(), tipoDescuento);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
				}
					
				} else {
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la modificacion de los datos "
				    		+ "del tipo de desucuento salarial ?", "Confirmacion", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				    if(ret == JOptionPane.YES_OPTION){
				    	
				    	if(getRdbtnMasiva().isSelected() && getRdbtnPorcentual().isSelected() && (Float)getJTxtPorcentaje().getValue() == (float)0){
							
							getJTxtPorcentaje().requestFocus();
							DlgMessage.showMessage(getOwner(),"No se admite porcentaje del 0.0 % para un descuento porcentual\n"
									+ "Favor ingrese un porcentaje diferente del 0.0 % ", DlgMessage.WARNING_MESSAGE);
							
							tipoDescuento = null;
						}else if(getRdbtnMasiva().isSelected() && getRdbtnFijo().isSelected() && (Double)getJTxtMontoFijo().getValue()==0.0){
							
							getJTxtMontoFijo().requestFocus();
							DlgMessage.showMessage(getOwner(),"No se admite monto del 0 Gs. \n"
									+ "Favor ingrese un monto fijo diferente de 0", DlgMessage.WARNING_MESSAGE);
							
							tipoDescuento = null;
						}else{
							cargarValores();
							controladorTipoDescuentoSalario.modificarTipoDescuento(JFramePanambiMain.session.getConn(), tipoDescuento);
							DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
							doLimpiar();
							getJCmbTipoDescuento().requestFocus();
						}
				    		
						   	
					    }else{
					    	tipoDescuento=null;
					    }
					}	
					
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private void cargarValores(){
		
		
		if(getJTxtCodTipoDescuento().getValor()!=null){
			tipoDescuento.setCodTipoDescuento(getJTxtCodTipoDescuento().getValor());
		}
		
		tipoDescuento.setNombre((String) getJCmbTipoDescuento().getSelectedItem());
		
		//** carga valores para el estado del tipo de descuento.
		if (chckbxActivo.isSelected()){
			tipoDescuento.setEstado("A");
		}else{
			tipoDescuento.setEstado("I");
		}
		//**carga forma de aplicacion de descuentos, junto con sus descuentos porcentuales o fijos.
		if(getRdbtnPersonal().isSelected()){
			tipoDescuento.setFormaAplicacion("P");
			
			if(getRdbtnPorcentual().isSelected()){
				tipoDescuento.setTipoDescuento("P");

			}else{
				tipoDescuento.setTipoDescuento("F");
			}
			tipoDescuento.setMontoFijo(0.0);
			tipoDescuento.setPorcentaje((float)0);
		}else{
			tipoDescuento.setFormaAplicacion("M");
			if(getRdbtnPorcentual().isSelected()){
				tipoDescuento.setTipoDescuento("P");
				tipoDescuento.setPorcentaje((Float)getJTxtPorcentaje().getValue());
				tipoDescuento.setMontoFijo(0.0);
			}else{
				tipoDescuento.setTipoDescuento("F");
				tipoDescuento.setPorcentaje((float)0);
				tipoDescuento.setMontoFijo((Double)getJTxtMontoFijo().getValue());
			}
			
		}
		
	}
	
	
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar tipo de descuento");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			String[] columnNames = { "C�digo", "Nombre","Aplicacion", "Tipo","Porcentaje","Monto fijo","Estado" };
			String[] pks = { "codtipodescuentosalario" };
			String sSelect = "SELECT codtipodescuentosalario, nombre, "
					+ "(CASE WHEN formaaplicacion = 'M' THEN 'Masivo' WHEN formaaplicacion = 'P' THEN 'Personal' ELSE 'Desconocido' END), "
					+ "(CASE WHEN tipo = 'P' THEN 'Porcentual' WHEN tipo = 'F' THEN 'Fijo' ELSE 'Desconocido' END), porcentaje, montofijo , estado ";
			sSelect += "FROM tiposdescuentossalarios ts ";
			sSelect += "ORDER BY nombre";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.TipoDescuentoSalario", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbTipoDescuento().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try {
			tipoDescuento= null;
			poblarLista();
			getJTxtCodTipoDescuento().setValor(null);
			getJCmbTipoDescuento().setModel(new ListComboBoxModel<String>(tiposDescuentos));
			getJCmbTipoDescuento().setSelectedItem(null);
			
			getChckbxActivo().setSelected(true);
			
			getRdbtnFijo().setSelected(false);
			getJTxtMontoFijo().setValue(null);
			getJTxtMontoFijo().setEnabled(false);
			
			getRdbtnPorcentual().setSelected(true);
			getJTxtPorcentaje().setValue(null);
			getRdbtnMasiva().setSelected(true);
			getRdbtnPersonal().setSelected(false);
			
			btnpnmbEliminar.setEnabled(false);
			btnpnmbGrabar.setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void poblarLista() throws Exception {
		tiposDescuentos= new ArrayList<String>();
		List<TipoDescuentoSalario> listatipodes = controladorTipoDescuentoSalario.getTiposDescuentos(JFramePanambiMain.session.getConn());
		for (TipoDescuentoSalario tipodes : listatipodes) {
			tiposDescuentos.add(tipodes.getNombre());
		}
	}
	
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNombre.setText("Nombre : ");
		}
		return lblpnmbNombre;
	}
	private JTextFieldInteger getJTxtCodTipoDescuento() {
		if (jTxtCodTipoDescuento == null) {
			jTxtCodTipoDescuento = new JTextFieldInteger();
			jTxtCodTipoDescuento.setEnabled(false);
			jTxtCodTipoDescuento.setEditable(false);
		}
		return jTxtCodTipoDescuento;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbTipoDescuento() {
			if (jCmbTipoDescuento== null) {
				jCmbTipoDescuento = new JComboBoxPanambi();
				jCmbTipoDescuento.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
						lostFocusNombre();
					}
				});
				jCmbTipoDescuento.setEditable(true);
				jCmbTipoDescuento.setModel(new ListComboBoxModel<String>(tiposDescuentos));
				jCmbTipoDescuento.setSelectedItem(null);
			}
			return jCmbTipoDescuento;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof TipoDescuentoSalario) {
			
			this.tipoDescuento = (TipoDescuentoSalario) obj;
			
			getJTxtCodTipoDescuento().setValor(tipoDescuento.getCodTipoDescuento());
			getJCmbTipoDescuento().setSelectedItem(((TipoDescuentoSalario) (obj)).getNombre());
			
			if (tipoDescuento.getEstado().equals("A")) {
				getChckbxActivo().setSelected(true);
			} else {
				getChckbxActivo().setSelected(false);
			}
			
			if(tipoDescuento.getFormaAplicacion().equals("P")){
				getRdbtnMasiva().setSelected(false);
				getRdbtnPersonal().setSelected(true);
				if(tipoDescuento.getTipoDescuento().equals("P")){
					getRdbtnPorcentual().setSelected(true); 
					getRdbtnFijo().setSelected(false);
				}else{
					getRdbtnPorcentual().setSelected(false);
					getRdbtnFijo().setSelected(true);
				}
				getJTxtPorcentaje().setValue(null);
				getJTxtPorcentaje().setEnabled(false);
				getJTxtMontoFijo().setValue(null);
				getJTxtMontoFijo().setEnabled(false);
				
			}else{
				getRdbtnPersonal().setSelected(false);
				getRdbtnMasiva().setSelected(true);
				
				if (tipoDescuento.getTipoDescuento().equals("P")){
					getRdbtnPorcentual().setSelected(true);
					getRdbtnFijo().setSelected(false);
					
					getJTxtPorcentaje().setValue(tipoDescuento.getPorcentaje());
					getJTxtPorcentaje().setEnabled(true);
					
					getJTxtMontoFijo().setEnabled(false);
					getJTxtMontoFijo().setValue(null);

				}else{
					getRdbtnPorcentual().setSelected(false);
					getRdbtnFijo().setSelected(true);
					
					getJTxtMontoFijo().setEnabled(true);
					getJTxtMontoFijo().setValue(tipoDescuento.getMontoFijo());
					
					getJTxtPorcentaje().setEnabled(false);
					getJTxtPorcentaje().setValue(null);

				}
				
			}
		
		btnpnmbEliminar.setEnabled(true);
		btnpnmbGrabar.setEnabled(true);
		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	
	private void lostFocusNombre() {
		try {
			TipoDescuentoSalario tipodes = controladorTipoDescuentoSalario.getTipoDescuento(JFramePanambiMain.session.getConn(), (String) jCmbTipoDescuento.getSelectedItem());
			if (tipodes != null) {
				setValues(tipodes, null);
				
			} else {
				//if(getJCmbTipoDescuento().getSelectedItem()!=null){
					//doNuevoFromLostFocus();
				//}else{
					//doLimpiar();
				//}
				doNuevoFromLostFocus();				
//				if(getJCmbTipoDescuento().getSelectedItem().toString().length()>0){
//					doNuevoFromLostFocus();
//				}else{
//					doLimpiar();
//				}
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		
		tipoDescuento = null;
		getJTxtCodTipoDescuento().setValor(null);
		getJTxtPorcentaje().setValue((float)0);
		getJTxtPorcentaje().setEnabled(true);
		getRdbtnPorcentual().setSelected(true);
		getRdbtnFijo().setSelected(false);
		getJTxtMontoFijo().setValue(null);
		chckbxActivo.setSelected(true);
		
		if(getJCmbTipoDescuento().getSelectedItem()!=null){
			if(getJCmbTipoDescuento().getSelectedItem().toString().length()!=0){
				getBtnpnmbGrabar().setEnabled(true);
			}else getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbEliminar().setEnabled(false);
		}else{
			
			getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbEliminar().setEnabled(false);
		}
			
	}

	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbTipoDescuento());
	}
	private JCheckBox getChckbxActivo(){
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
	private JRadioButton getRdbtnPorcentual() {
		if (rdbtnPorcentual == null) {
			rdbtnPorcentual = new JRadioButton("Porcentual");
			rdbtnPorcentual.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doSeleccionDescuentoPorcentual();
					}
				}
			});
			rdbtnPorcentual.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionDescuentoPorcentual();
				}
			});
			rdbtnPorcentual.setSelected(true);
		}
		return rdbtnPorcentual;
	}
	
	private void doSeleccionDescuentoPorcentual(){
	
		getRdbtnPorcentual().setSelected(true);
		
		if(getRdbtnMasiva().isSelected()){
			getJTxtPorcentaje().setEnabled(true);
			
			if(getJTxtPorcentaje().getValue()==null){
				getJTxtPorcentaje().setValue((float)0);
			}
		}else{
			getJTxtPorcentaje().setEnabled(false);
			getJTxtPorcentaje().setValue(null);
		}
		
		
		
		getRdbtnFijo().setSelected(false);
		getJTxtMontoFijo().setValue(null);
		getJTxtMontoFijo().setEnabled(false);

	}
	
	private JRadioButton getRdbtnFijo() {
		if (rdbtnFijo == null) {
			rdbtnFijo = new JRadioButton("Fijo");
			rdbtnFijo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doSeleccionDescuentoFijo();
					}
				}
			});
			rdbtnFijo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionDescuentoFijo();
				}
			});
		}
		return rdbtnFijo;
	}
	
	private void doSeleccionDescuentoFijo(){
		
		getRdbtnFijo().setSelected(true);
		
		
		if(getRdbtnMasiva().isSelected()){
			getJTxtMontoFijo().setEnabled(true);
			
			if(getJTxtMontoFijo().getValue()==null){
				getJTxtMontoFijo().setValue((float)0);
			}
		}else{
			getJTxtMontoFijo().setEnabled(false);
			getJTxtMontoFijo().setValue(null);
		}
		
		getRdbtnPorcentual().setSelected(false);
		getJTxtPorcentaje().setValue(null);
		getJTxtPorcentaje().setEnabled(false);

	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
	private JTextFieldDouble getJTxtMontoFijo() {
		if (jTxtMontoFijo == null) {
			jTxtMontoFijo = new JTextFieldDouble();
			jTxtMontoFijo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		}
		return jTxtMontoFijo;
	}
	private JLabelPanambi getLblpnmbFormaDeAplicacion() {
		if (lblpnmbFormaDeAplicacion == null) {
			lblpnmbFormaDeAplicacion = new JLabelPanambi();
			lblpnmbFormaDeAplicacion.setText("Forma de aplicacion");
		}
		return lblpnmbFormaDeAplicacion;
	}
	private JRadioButton getRdbtnMasiva() {
		if (rdbtnMasiva == null) {
			rdbtnMasiva = new JRadioButton("Masiva");
			rdbtnMasiva.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doFormaMasiva();
					}
				}
			});
			rdbtnMasiva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doFormaMasiva();
				}
			});
		}
		return rdbtnMasiva;
	}
	
	private void doFormaMasiva(){
		getRdbtnMasiva().setSelected(true);
		getRdbtnPersonal().setSelected(false);
		
		
		if(getRdbtnFijo().isSelected()){
			getJTxtMontoFijo().setEnabled(true);
			getJTxtPorcentaje().setEnabled(false);
			getJTxtPorcentaje().setValue(null);
			getJTxtMontoFijo().setValue(0.0);
		}else{
			getJTxtMontoFijo().setEnabled(false);
			getJTxtPorcentaje().setEnabled(true);
			getJTxtPorcentaje().setValue((float)0);
			getJTxtMontoFijo().setValue(null);
		}
	}
	
	private JRadioButton getRdbtnPersonal() {
		if (rdbtnPersonal == null) {
			rdbtnPersonal = new JRadioButton("Personal");
			rdbtnPersonal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doFormaPersonal();
					}
				}
			});
			rdbtnPersonal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doFormaPersonal();
				}
			});
		}
		return rdbtnPersonal;
	}
	
	private void doFormaPersonal(){
		getRdbtnPersonal().setSelected(true);
		getRdbtnMasiva().setSelected(false);
		
		getJTxtMontoFijo().setEnabled(false);
		getJTxtMontoFijo().setValue(null);
		
		getJTxtPorcentaje().setEnabled(false);
		getJTxtPorcentaje().setValue(null);
		
	}
	private JTextFieldFloat getJTxtPorcentaje() {
		if (jTxtPorcentaje == null) {
			jTxtPorcentaje = new JTextFieldFloat();
			jTxtPorcentaje.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					doComprobarPorcentaje();
				}
			});
			
		}
		return jTxtPorcentaje;
	}
	
	private void doComprobarPorcentaje(){
		float porcentaje = 0;
		try{
			porcentaje = (Float)getJTxtPorcentaje().getValue();
		}catch(Exception e){
			getJTxtPorcentaje().setValue((float)0);
			porcentaje = 0;
		}
		if(porcentaje>(float)100){
			getJTxtPorcentaje().setValue((float)0);
		}
	}
}
