/**
 * 
 */
package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.apache.log4j.Logger;



/**
 *
 */
public class JDialogPanambi extends JDialog {

	private static final long serialVersionUID = 1L;
	protected static Logger logger = Logger.getLogger(JDialogPanambi.class);
	private JPanel jContentPane = null;

	/**
	 * 
	 */
	public JDialogPanambi() {
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 */
	public JDialogPanambi(Frame owner) {
		super(owner);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 */
	public JDialogPanambi(Dialog owner) {
		super(owner);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 */
	public JDialogPanambi(Window owner) {
		super(owner);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modal
	 */
	public JDialogPanambi(Frame owner, boolean modal) {
		super(owner, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public JDialogPanambi(Frame owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modal
	 */
	public JDialogPanambi(Dialog owner, boolean modal) {
		super(owner, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public JDialogPanambi(Dialog owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modalityType
	 */
	public JDialogPanambi(Window owner, ModalityType modalityType) {
		super(owner, modalityType);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public JDialogPanambi(Window owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 */
	public JDialogPanambi(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 */
	public JDialogPanambi(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modalityType
	 */
	public JDialogPanambi(Window owner, String title, ModalityType modalityType) {
		super(owner, title, modalityType);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @param gc
	 */
	public JDialogPanambi(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @param gc
	 */
	public JDialogPanambi(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modalityType
	 * @param gc
	 */
	public JDialogPanambi(Window owner, String title, ModalityType modalityType, GraphicsConfiguration gc) {
		super(owner, title, modalityType, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(354, 232);
		this.setContentPane(getJContentPane());
		this.setModal(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
		}
		return jContentPane;
	}

	public void centerIt() {
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension ventana = getSize();
		setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
	}

}