package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Banco;
import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.FormaPago;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorBanco;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorFormaPago;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.interfaces.ReciboBrowser;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.prompts.PromptNotasCreditos;
import py.com.panambi.utils.NumerosALetras;

public class DlgPagoCuota extends JDialogPanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5934988469506679227L;
	private JButtonPanambi jBtnEfectuarPago;
	private JButtonPanambi jBtnCancelar;
	private JPanel panel;
	private JTextFieldDouble jTxtTotal;
	private JPanel panel_1;
	private JPanel panel_2;
	private DefaultTableModel dtmPagoCuota;
	private JScrollPane scrollPane;
	private JTable jTblPagoCuota;
	private Double total;
	private JLabelPanambi lblpnmbVuelto;
	private JTextFieldDouble jTxtVuelto;
	private ControladorPago controladorPago = new ControladorPago();
	private boolean pagoEfectuado = false;
	private ControladorFormaPago controladorFormaPago = new ControladorFormaPago();
	private JLabel lblFormaDePago;
	private JComboBoxPanambi jCmbFormaPago;
	private JLabelPanambi lblpnmbTotal;
	private ControladorVenta controladorVenta= new ControladorVenta();
	private String conceptoRecibo;
	private Integer numeroRecibo;
	private JTextFieldDouble jTxtImporteFormaPago;
	private JButtonPanambi jBtnAgregarPago;
	private JPanel jPnlPagos;
	private JScrollPane scrollPane_1;
	private JButtonPanambi jBtnQuitarPago;
	private JTablePanambi jTblPagos;
	private JLabel jLblImporte;
	private JLabelPanambi lblpnmbPendiente;
	private JTextFieldDouble jTxtPendientePorPagar;
	private JLabelPanambi lblpnmbTotalAcumulado;
	private JTextFieldDouble jTxtTotalAcumulado;
	private JLabel jLblNumero;
	private JTextFieldUpper jTxtNumero;
	private JLabel jLblBanco;
	private JComboBoxPanambi jCmbBanco;
	private Cliente cliente;
	private List<String> bancos = new ArrayList<String>();
	private ControladorBanco controladorBanco = new ControladorBanco();
	private JLabelPanambi lblpnmbPagoDeCuotas;
	private ReciboBrowser reciboBrowser;
	private JButtonPanambi jBtnExaminar;
	
	
	public  DlgPagoCuota(DefaultTableModel dtm, Cliente cli, ReciboBrowser reciboBrowser) {
		this.dtmPagoCuota = dtm;
		this.cliente = cli;
		this.reciboBrowser = reciboBrowser;
		initialize();
		getJTxtImporteFormaPago().requestFocus();
	}
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgPagoCuota.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setResizable(false);
		setBounds(100, 100, 796,643);
		try{
			doLimpiar();
			poblarListaBancos();
			setLocationRelativeTo(null);
			getContentPane().setLayout(null);
			getContentPane().add(getPanel_1());
			getContentPane().add(getPanel_2());
			getContentPane().add(getPanel());
			
		}catch (Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private void doLimpiar(){
		getJTblPagos().resetData(0);
		getJTxtTotalAcumulado().setValue(0.0);
		getJTxtVuelto().setValue(0.0);
		calcularTotal();
		doPoblarListaFormaPago();
		getJTxtTotal().setValue(total);
		calcularPendiente();
		getJLblNumero().setVisible(false);
		getJTxtNumero().setVisible(false);
		getJLblBanco().setVisible(false);
		getJCmbBanco().setVisible(false);
		getJBtnExaminar().setVisible(false);
		getJCmbFormaPago().setSelectedItem("EFECTIVO");;
		
	}
	
	private void calcularPendiente(){
		if(total > (Double)getJTxtTotalAcumulado().getValue()){
			getJTxtPendientePorPagar().setValue(total - (Double)getJTxtTotalAcumulado().getValue());
			getJTxtVuelto().setValue(0.0);
		}else{
			getJTxtPendientePorPagar().setValue(0.0);
			getJTxtVuelto().setValue((Double)getJTxtTotalAcumulado().getValue()-(Double)getJTxtTotal().getValue());
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void doPoblarListaFormaPago(){
		try{
			 List<String> formasPagos = new ArrayList<String>();
			 List<FormaPago> listafp = new ControladorFormaPago().getFormasPagosActivas(JFramePanambiMain.session.getConn());
			 for (FormaPago form : listafp) {
				if(form.getNombre().equals("NOTA DE CREDITO")){
					List<Devolucion> notasCreditoCliente = new ControladorDevolucion().getNotasCreditoActivasCliente(JFramePanambiMain.session.getConn(), cliente);
					 if(notasCreditoCliente.size()!=0){
						 formasPagos.add(form.getNombre());
					 }
					 
				}else {
					formasPagos.add(form.getNombre());
				}
			}
			 
			 getJCmbFormaPago().setModel(new ListComboBoxModel<String>(formasPagos));
			 getJCmbFormaPago().setSelectedItem("EFECTIVO");
			 
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getJBtnEfectuarPago() {
		if (jBtnEfectuarPago == null) {
			jBtnEfectuarPago = new JButtonPanambi();
			jBtnEfectuarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doEfectuarPago();
				}
			});
			jBtnEfectuarPago.setText("Registrar Pago");
		}
		return jBtnEfectuarPago;
	}
	
	private void doEfectuarPago(){
		conceptoRecibo = "";
		if((Double)getJTxtTotalAcumulado().getValue()==0.0){
			
			DlgMessage.showMessage(getOwner(), "Debe cargar montos en el importe.", DlgMessage.WARNING_MESSAGE);
			
		}else if((Double)getJTxtTotalAcumulado().getValue() < total){
			
			DlgMessage.showMessage(getOwner(), "El monto de importe no puede ser menor al monto de la deuda total.", DlgMessage.WARNING_MESSAGE);	
			getJTxtImporteFormaPago().requestFocus();
			
		}else{
 				Integer ret = JOptionPane.showConfirmDialog(this,"Confirma el pago de cuotas ? ", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
 				
 				if(ret == JOptionPane.YES_OPTION){
 					try {
// 						clientes =null;
// 						clientes = new ArrayList<Cliente>();
// 						clientes.add(controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(),(Integer)dtmPagoCuota.getValueAt(0, 1)).getCliente());
// 						
 						
 						if(dtmPagoCuota.getRowCount()>1){
 							conceptoRecibo ="Pago de cuotas";
 						}else{
 							conceptoRecibo ="Pago de cuota";
 						}
 						
 						Integer factura = (Integer) dtmPagoCuota.getValueAt(0, 1);
 						boolean primero = true;
 						for (int i = 0;i<dtmPagoCuota.getRowCount();i++){
// 							Cliente clienteAux = new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(),(Integer)dtmPagoCuota.getValueAt(i, 1)).getCliente();
// 							
// 							boolean existeCliente = false;
// 							for (int j = 0; j<clientes.size();j++){
// 								if(clientes.get(j).getCodCliente()==clienteAux.getCodCliente()){
// 									existeCliente = true;
// 								}
// 							}
// 							
// 							if(!existeCliente){
// 								clientes.add(clienteAux);
// 							}
 							 
 							 
// 							 if(factura != dtmPagoCuota.getValueAt(i, 1)){
// 								// conceptoRecibo+=" de factura n�mero "+factura+" y cuota";
// 								 //factura = (Integer)dtmPagoCuota.getValueAt(i, 1);
// 								conceptoRecibo+=" "+dtmPagoCuota.getValueAt(i, 2);
// 								primero = true;
// 							 }else{
 							if(!primero){
 								conceptoRecibo+=" y "+dtmPagoCuota.getValueAt(i, 2);
 							 }else{
 								conceptoRecibo+=" "+dtmPagoCuota.getValueAt(i, 2);
 								primero = false;
 							 }
 								
// 							}
 							pagoEfectuado = true;
						}
 						
 						conceptoRecibo +=" de factura n�mero "+factura+".";
 						
 						} catch (Exception e) {
							DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
						}
 					//***Registrando pago efectuado.
 					if(pagoEfectuado){
 						try {
 							
 							numeroRecibo =  new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn());
 							
 							//for(int i=0;i<getJTblPagoCuota().getRowCount();i++){
 								
 								Pago pago = new Pago();
 								pago. setMonto (total);//(Double)getJTblPagoCuota().getValueAt(i, 3));
 								pago. setVenta(controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), (Integer) getJTblPagoCuota().getValueAt(0, 1)));
 							//	pago. setPlanPago(new ControladorPlanPago().getCuota(JFramePanambiMain.session.getConn(), (Integer) getJTblPagoCuota().getValueAt(i, 0)));
 								
 								
 								
 								List<DetallePago> detalle = new ArrayList<DetallePago>();
 								
 								for (int j = 0; j < getJTblPagos().getRowCount(); j++) {
//									DetallePago detallePago = new DetallePago();
//									
//									//detallePago. setNroItem(j+1);
//									detallePago. setMonto((Double)getJTblPagos().getValueAt(j, 2));
//									detallePago. setFormaPago(new ControladorFormaPago().getFormaPago(JFramePanambiMain.session.getConn(), (String)getJTblPagos().getValueAt(j, 1)));
//									//detallePago. setPago(pago);
									
									//detalle.add(detallePago);
									detalle.add((DetallePago)getJTblPagos().getValueAt(j, 3));
									
								}
 								
 								//**Comprobando si es posible generar vuelto en efectivo.
 								boolean vueltoCorrecto = false;
 								String mensajeVuelvoIncorrecto = "";
 								if((Double)getJTxtVuelto().getValue()!=0.0){
 									Integer indiceEfectivo = null;
 									for (int j = 0; j<getJTblPagos().getRowCount();j++){
 										if(getJTblPagos().getValueAt(j, 1).equals("EFECTIVO")){
 											indiceEfectivo = j;
 										}
 									}
 									DetallePago detallePagoTemp = new DetallePago();
 									if(indiceEfectivo !=null){
 										if((Double)getJTblPagos().getValueAt(indiceEfectivo, 2)>=(Double)getJTxtVuelto().getValue()){
 											vueltoCorrecto = true;
 	 										//detallePagoTemp .setNroitem(indiceEfectivo+1);
 	 										detallePagoTemp .setFormaPago(controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "EFECTIVO"));
 	 										detallePagoTemp .setMonto((Double)getJTblPagos().getValueAt(indiceEfectivo, 2)-(Double)getJTxtVuelto().getValue());
 	 										int indiceBorrado = indiceEfectivo;
 	 	 									detalle.remove(indiceBorrado);
 	 	 									detalle.add(detallePagoTemp);
 										}else{
 											mensajeVuelvoIncorrecto = "El importe en efectivo no puede ser menor al vuelto.";
 											vueltoCorrecto = false;
 										}
 										
 									}else{
 										mensajeVuelvoIncorrecto = "No se encuentra ninguna forma de pago efectivo.";
 										vueltoCorrecto = false;
 									}	
 								}else{
 									vueltoCorrecto = true;
 								}
 								
 								
 								pago.setDetallePago(detalle);
 								
 								pago.setSucursal(JFramePanambiMain.session.getSucursalOperativa());
 								
 								List<Integer> cuotasPagadas = new ArrayList<Integer>();
 								for(int j = 0 ; j < getJTblPagoCuota().getRowCount();j++){
 									cuotasPagadas.add((Integer)getJTblPagoCuota().getValueAt(j, 0));
 								}
 								
 								if(vueltoCorrecto){
 									controladorPago.registrarPagoPlanPago(JFramePanambiMain.session.getConn(), pago, numeroRecibo,cuotasPagadas);
 		 							
 		 							//}
 		 							
 		 							jTxtVuelto.setValue((Double)getJTxtTotalAcumulado().getValue()-total);	
 			 						DlgMessage.showMessage(getOwner(), "Pago registrado exitosamente\nSe generar� un recibo de pago de cuotas", DlgMessage.INFORMATION_MESSAGE);
 										
 									getJBtnEfectuarPago().setFocusable(false);
 									getJTxtImporteFormaPago().setFocusable(false);
 									getJTxtVuelto().setFocusable(false);
 									getJCmbFormaPago().setFocusable(false);
 									getJCmbFormaPago().setEditable(false);
 									
 									doGenerarRecibo();
// 									reciboBrowser.doRecibo(true, "el titu", "el concept", "el monto", 0.0);
 									this.dispose();
 									
 								}else{
 									DlgMessage.showMessage(getOwner(), "Error en el vuelto del pago.\n"+mensajeVuelvoIncorrecto, DlgMessage.ERROR_MESSAGE);
 									getJTxtImporteFormaPago().requestFocus();
 								}
								
						} catch (Exception e) {
							logger.error(e.getMessage(), e);
							DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
						}
 						
 				}
 			}	
 		}
		
	}
	
	private JButtonPanambi getJBtnCancelar() {
		if (jBtnCancelar == null) {
			jBtnCancelar = new JButtonPanambi();
			jBtnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doCancelar();
				}
			});
			jBtnCancelar.setText("Salir");
		}
		return jBtnCancelar;
	}
	
	private void doCancelar(){
		this.dispose();
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 45, 790, 535);
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
								.addGap(19)
								.addComponent(getScrollPane_1()))
							.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
								.addGap(24)
								.addComponent(getJPnlPagos(), GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(12)
										.addComponent(getLblpnmbPendiente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(26)
										.addComponent(getLblpnmbVuelto(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)))
								.addGap(10)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtPendientePorPagar(), GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtVuelto(), GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))))
						.addGap(7))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(9)
						.addComponent(getScrollPane_1(), GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(97)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
										.addGap(24)
										.addComponent(getLblpnmbPendiente(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
										.addGap(29)
										.addComponent(getLblpnmbVuelto(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
										.addGap(21)
										.addComponent(getJTxtPendientePorPagar(), GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
										.addGap(26)
										.addComponent(getJTxtVuelto(), GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)))
								.addGap(28))
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getJPnlPagos(), GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
								.addContainerGap())))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setBackground(Color.BLUE);
			jTxtTotal.setForeground(Color.WHITE);
			jTxtTotal.setToolTipText("Total a pagar");
			jTxtTotal.setFont(new Font("Tahoma", Font.BOLD, 22));
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setEditable(false);
			jTxtTotal.setFocusable(false);
		}
		return jTxtTotal;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBounds(0, 582, 790, 33);
			panel_1.add(getJBtnEfectuarPago());
			panel_1.add(getJBtnCancelar());
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBackground(new Color(0, 0, 139));
			panel_2.setForeground(Color.DARK_GRAY);
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 0, 800, 44);
			panel_2.setLayout(null);
			panel_2.add(getLblpnmbPagoDeCuotas());
		}
		return panel_2;
	}
	private JScrollPane getScrollPane_1() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblPagoCuota());
		}
		return scrollPane;
	}
	private JTable getJTblPagoCuota() {
		if (jTblPagoCuota == null) {
			jTblPagoCuota = new JTable();
			jTblPagoCuota.setRowSelectionAllowed(false);
			jTblPagoCuota.setFont(new Font("Tahoma", Font.PLAIN, 15));
			jTblPagoCuota.setModel(dtmPagoCuota);
			jTblPagoCuota.setFocusable(false);
			

		}
		return jTblPagoCuota;
	}
	private void calcularTotal(){
		total = 0.0;
		for (int i = 0;i<dtmPagoCuota.getRowCount();i++){
			total+= (Double)dtmPagoCuota.getValueAt(i, 3);
		}
	}
	private JLabelPanambi getLblpnmbVuelto() {
		if (lblpnmbVuelto == null) {
			lblpnmbVuelto = new JLabelPanambi();
			lblpnmbVuelto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbVuelto.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblpnmbVuelto.setText("Vuelto");
		}
		return lblpnmbVuelto;
	}
	private JTextFieldDouble getJTxtVuelto() {
		if (jTxtVuelto == null) {
			jTxtVuelto = new JTextFieldDouble();
			jTxtVuelto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtVuelto.setToolTipText("Vuelto del cliente");
			jTxtVuelto.setForeground(Color.RED);
			jTxtVuelto.setEditable(false);
			jTxtVuelto.setFont(new Font("Tahoma", Font.BOLD, 22));
			jTxtVuelto.setFocusable(false);
		}
		return jTxtVuelto;
	}
	private JLabel getLblFormaDePago() {
		if (lblFormaDePago == null) {
			lblFormaDePago = new JLabel("Forma de Pago");
			lblFormaDePago.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblFormaDePago;
	}
	private JComboBoxPanambi getJCmbFormaPago() {
		if (jCmbFormaPago == null) {
			jCmbFormaPago = new JComboBoxPanambi();
			jCmbFormaPago.setToolTipText("Seleccione forma de pago");
			jCmbFormaPago.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			jCmbFormaPago.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					doSelectFormaPago();
				}
			});
		}
		return jCmbFormaPago;
	}
	
	private void doSelectFormaPago(){
//		if(getJCmbFormaPago().getSelectedItem().equals("NOTA DE CREDITO")){
//			PromptNotasCreditos prompt = new PromptNotasCreditos(this);
//			prompt.setVisible(true);
//		}else if(getJCmbFormaPago().getSelectedIndex()==-1){
//			getJCmbFormaPago().requestFocus();
//		}
		
		int cheque;
		int tarjetaDebito ;
		int tarjetaCredito;
		int notaCredito;

		try{
			int codformapago = controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(),(String)getJCmbFormaPago().getSelectedItem()).getCodFormaPago();
			
			cheque = controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "CHEQUE").getCodFormaPago();
			tarjetaDebito= controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "TARJETA DE DEBITO").getCodFormaPago();
			tarjetaCredito= controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "TARJETA DE CREDITO").getCodFormaPago();
			notaCredito= controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "NOTA DE CREDITO").getCodFormaPago();
			
			
			if(codformapago==tarjetaCredito){
				
				getJLblNumero().setText("Boucher Nro.");
				getJLblNumero().setVisible(true);
				getJTxtNumero().setEditable(true);
				getJTxtNumero().setVisible(true);
				getJLblBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				getJBtnExaminar().setVisible(false);
				
			}else if(codformapago==tarjetaDebito){
				getJLblNumero().setText("Boucher Nro.");
				getJLblNumero().setVisible(true);
				getJTxtNumero().setEditable(true);
				getJTxtNumero().setVisible(true);
				getJLblBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				getJBtnExaminar().setVisible(false);
				
			}else if (codformapago == cheque){
				getJLblNumero().setText("Cheque Nro.");
				getJLblNumero().setVisible(true);
				getJTxtNumero().setEditable(true);
				getJTxtNumero().setVisible(true);
				getJLblBanco().setVisible(true);
				getJCmbBanco().setVisible(true);				
				getJBtnExaminar().setVisible(false);
				
			}else if(codformapago==notaCredito){
				
				boolean poseeNotas = new ControladorDevolucion().poseeNotaSinUso(JFramePanambiMain.session.getConn(), cliente);
				if(poseeNotas){
					//doPrompNotaCredito();
					getJLblNumero().setText("Nota Nro.");
					getJLblNumero().setVisible(true);
					getJTxtNumero().setEditable(true);
					getJTxtNumero().setVisible(true);
					getJLblBanco().setVisible(false);
					getJCmbBanco().setVisible(false);
					getJBtnExaminar().setVisible(true);
				}else{
					DlgMessage.showMessage(getOwner(), "El cliente no posee ninguna nota de cr�dito que pueda utilizar como medio de pago.", DlgMessage.INFORMATION_MESSAGE);
					getJLblNumero().setVisible(false);
					getJTxtNumero().setVisible(false);
					getJTxtNumero().setText(null);
					getJLblBanco().setVisible(false);
					getJCmbBanco().setVisible(false);
					getJCmbFormaPago().setSelectedItem("EFECTIVO");
					getJTxtImporteFormaPago().requestFocus();
				}
				
			}else{
				getJLblNumero().setVisible(false);
				getJTxtNumero().setVisible(false);
				getJTxtNumero().setText(null);
				getJLblBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				getJBtnExaminar().setVisible(false);
			}
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void doPrompNotaCredito(){
		try{
			Venta venta = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 1));
			PromptNotasCreditos prompt = new PromptNotasCreditos(this,venta.getCliente());
			prompt.setVisible(true);
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblpnmbTotal.setText("Total a pagar");
		}
		return lblpnmbTotal;
	}
	
	
	private void doGenerarRecibo() {
		try {
			String titular = "";
			String concepto = "";
			Double montoTotal =0.0;
			String montoLetras = "";
			Double ultimoVuelto = 0.0;
			
			
			titular = cliente.getNombres()+" "+cliente.getApellidos();
			concepto = conceptoRecibo;
			montoTotal = (Double)getJTxtTotal().getValue();
			ultimoVuelto = (Double)getJTxtVuelto().getValue();
			montoLetras = "GUARAN�ES "+ NumerosALetras.getLetras((Double)getJTxtTotal().getValue());
	
			this.dispose();
			reciboBrowser.doRecibo(true, titular, concepto, montoLetras, montoTotal, numeroRecibo, ultimoVuelto);
				
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JTextFieldDouble getJTxtImporteFormaPago() {
		if (jTxtImporteFormaPago == null) {
			jTxtImporteFormaPago = new JTextFieldDouble();
			jTxtImporteFormaPago.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtImporteFormaPago.setToolTipText("Ingrese importe");
			jTxtImporteFormaPago.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return jTxtImporteFormaPago;
	}
	private JButtonPanambi getJBtnAgregarPago() {
		if (jBtnAgregarPago == null) {
			jBtnAgregarPago = new JButtonPanambi();
			jBtnAgregarPago.setToolTipText("Agregar importe");
			jBtnAgregarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarPago();
				}
			});
			jBtnAgregarPago.setFont(new Font("Dialog", Font.PLAIN, 12));
			jBtnAgregarPago.setText("Agregar");
		}
		return jBtnAgregarPago;
	}
	
	private void doAgregarPago(){
		
		try{
			
		if(getJCmbFormaPago().getSelectedIndex()!=-1){
			
			if(getJTxtImporteFormaPago().getValue()!=null && (Double)getJTxtImporteFormaPago().getValue()!=0.0){
				
				Double totalAcumulado = 0.0;
				if(getJTblPagos().getRowCount()!=0){	
					
					for(int i = 0 ; i<getJTblPagos().getRowCount();i++){
						totalAcumulado+=(Double)jTblPagos.getValueAt(i, 2);
					}
					
					if(totalAcumulado<(Double)getJTxtTotal().getValue()){
						
						boolean efectivoEnTabla = false;
						boolean cargaCorrecta = false;
						
						if(getJCmbFormaPago().getSelectedItem().toString().equals("EFECTIVO")){
							
							for(int i = 0 ; i<getJTblPagos().getRowCount();i++){
								//Acumula efectivo en el caso que agregue varias veces.
								if(getJTblPagos().getValueAt(i, 1).equals("EFECTIVO")){
									efectivoEnTabla = true;
									
									getJTblPagos(). setValueAt((Double)getJTxtImporteFormaPago().getValue()+(Double)getJTblPagos(). getValueAt(i, 2), i, 2);
									DetallePago detallePago = new DetallePago();
									detallePago.setMonto((Double)getJTblPagos().getValueAt(i, 2));
									detallePago.setFormaPago(controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), "EFECTIVO"));
									totalAcumulado+=(Double)getJTxtImporteFormaPago().getValue();
									i= getJTblPagos().getRowCount()+1;
									getJTxtTotalAcumulado().setValue(totalAcumulado);
									calcularPendiente();
									posicionInicial();
								}
							}
						}
						
						if(!efectivoEnTabla){

							getJTblPagos(). addRow();
							getJTblPagos(). setValueAt(Boolean.FALSE, getJTblPagos().getRowCount()-1, 0);
							getJTblPagos(). setValueAt(getJCmbFormaPago().getSelectedItem(), getJTblPagos().getRowCount()-1, 1);
							getJTblPagos(). setValueAt(getJTxtImporteFormaPago().getValue(), getJTblPagos().getRowCount()-1, 2);
							totalAcumulado+=(Double)getJTxtImporteFormaPago().getValue();
							
							DetallePago detallePago = new DetallePago();
							detallePago.setMonto((Double) getJTxtImporteFormaPago().getValue());
							detallePago.setFormaPago(controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), (String) getJCmbFormaPago().getSelectedItem()));
							
							 
							
							if(getJCmbFormaPago().getSelectedItem().toString().equals("CHEQUE")){
								
								if(getJTxtNumero().getText().trim().length()!=0){
									cargaCorrecta = true;
									detallePago. setChequenro(getJTxtNumero().getText().trim());
									detallePago. setBanco(new ControladorBanco().getBanco(JFramePanambiMain.session.getConn(), (String)getJCmbBanco().getSelectedItem()));
								}else{
									getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
									DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de cheque.", DlgMessage.ERROR_MESSAGE);
									getJTxtNumero().requestFocus();
								}
								
							}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE DEBITO")){
								
								if(getJTxtNumero().getText().trim().length()!=0){
									cargaCorrecta = true;
									detallePago. setBouchernro(getJTxtNumero().getText().trim());
								}else{
									getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
									DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de boucher.", DlgMessage.ERROR_MESSAGE);
									getJTxtNumero().requestFocus();
								}
								
							}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE CREDITO")){

								if(getJTxtNumero().getText().trim().length()!=0){
									cargaCorrecta = true;
									detallePago. setBouchernro(getJTxtNumero().getText().trim());
								}else{
									getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
									DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de boucher.", DlgMessage.ERROR_MESSAGE);
									getJTxtNumero().requestFocus();
								}
								
							}else if (getJCmbFormaPago().getSelectedItem().toString().equals("NOTA DE CREDITO")){

								Devolucion nota =null;
								try{
									nota = new ControladorDevolucion().getNotaCredito(JFramePanambiMain.session.getConn(), Integer.parseInt(getJTxtNumero().getText()));
								}catch(Exception e){
									nota = null;
								}
														
								if(nota !=null){
									
									if(nota.getTotal()+1-1!=(Double)getJTxtImporteFormaPago().getValue()+1-1){
										cargaCorrecta = false;
										getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
										DlgMessage.showMessage(getOwner(), "Datos incorrectos de la nota de cr�dito.\nFavor verifique.", DlgMessage.ERROR_MESSAGE);
										getJTxtImporteFormaPago().setValue(nota.getTotal());
										getJTxtNumero().requestFocus();
									}else{
										boolean yaUtilizado = false;
										for(int i = 0;i<getJTblPagos().getRowCount();i++){
											Integer nroAgregado;
											try{
												nroAgregado = ((DetallePago) getJTblPagos().getValueAt(i, 3)).getNotacredito();
											}catch(Exception e ){
												nroAgregado = 0;
											}
											if(nota.getNotaCredito() ==nroAgregado){
												yaUtilizado = true;
											}
										}
										
										if(!yaUtilizado){
											cargaCorrecta = true;
											detallePago. setNotacredito((Integer)getJTxtNumero().getValue());
										}else{
											cargaCorrecta = false;
											getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
											DlgMessage.showMessage(getOwner(), "Nota de cr�dito ya agregada.", DlgMessage.ERROR_MESSAGE);
											getJTxtNumero().requestFocus();
										}
									}
								}else{
									cargaCorrecta = false;
									getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
									DlgMessage.showMessage(getOwner(), "Datos incorrectos de la nota de cr�dito.\nFavor verifique.", DlgMessage.ERROR_MESSAGE);
								}
					
								
							}else{
								cargaCorrecta = true;
								//en efectivo u otra forma de pago no hay accion adicional.
							}
							
							if(cargaCorrecta){
								getJTblPagos(). setValueAt(detallePago, getJTblPagos().getRowCount()-1, 3);
								getJTxtTotalAcumulado().setValue(totalAcumulado);
								calcularPendiente();
								posicionInicial();
							}
						}
					}else{
						DlgMessage.showMessage(getOwner(), "El monto acumulado de importe ya exede al total a pagar.\nDebe eliminar formas de pagos si desea agregar mas importes", DlgMessage.WARNING_MESSAGE);
					}
					
				}else{
					getJTblPagos(). addRow();
					getJTblPagos(). setValueAt(Boolean.FALSE, getJTblPagos().getRowCount()-1, 0);
					getJTblPagos(). setValueAt(getJCmbFormaPago().getSelectedItem(), getJTblPagos().getRowCount()-1, 1);
					getJTblPagos(). setValueAt(getJTxtImporteFormaPago().getValue(), getJTblPagos().getRowCount()-1, 2);
					
					DetallePago detallePago = new DetallePago();
					detallePago.setMonto((Double) getJTxtImporteFormaPago().getValue());
					detallePago.setFormaPago(controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), (String) getJCmbFormaPago().getSelectedItem()));
					
					boolean cargaCorrecta = false; 
					
					if(getJCmbFormaPago().getSelectedItem().toString().equals("CHEQUE")){
						
						if(getJTxtNumero().getText().trim().length()!=0){
							cargaCorrecta = true;
							detallePago. setChequenro(getJTxtNumero().getText().trim());
							detallePago. setBanco(new ControladorBanco().getBanco(JFramePanambiMain.session.getConn(), (String)getJCmbBanco().getSelectedItem()));
						}else{
							getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
							DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de cheque.", DlgMessage.ERROR_MESSAGE);
							getJTxtNumero().requestFocus();
						}
						
					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE DEBITO")){
						
						if(getJTxtNumero().getText().trim().length()!=0){
							cargaCorrecta = true;
							detallePago. setBouchernro(getJTxtNumero().getText().trim());
						}else{
							getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
							DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de boucher.", DlgMessage.ERROR_MESSAGE);
							getJTxtNumero().requestFocus();
						}
						
					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE CREDITO")){
						
						if(getJTxtNumero().getText().trim().length()!=0){
							cargaCorrecta = true;
							detallePago. setBouchernro(getJTxtNumero().getText().trim());
						}else{
							getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
							DlgMessage.showMessage(getOwner(), "Debe ingresar el n�mero de boucher.", DlgMessage.ERROR_MESSAGE);
							getJTxtNumero().requestFocus();
						}
						
					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("NOTA DE CREDITO")){
						Devolucion nota =null;
						try{
							nota = new ControladorDevolucion().getNotaCredito(JFramePanambiMain.session.getConn(), Integer.parseInt(getJTxtNumero().getText()));
						}catch(Exception e){
							nota = null;
						}
												
						if(nota !=null){
							
//							JOptionPane.showMessageDialog(null, nota.getTotal()+1);
//							JOptionPane.showMessageDialog(null, (Double)getJTxtImporteFormaPago().getValue()+1);
//							
//							if((Double)nota.getTotal()+1-1==(Double)getJTxtImporteFormaPago().getValue()+1-1){
//								JOptionPane.showMessageDialog(null, "iguales");
//							}else{
//								JOptionPane.showMessageDialog(null, "diferentes");
//							}
							if(nota.getTotal()+1-1!=(Double)getJTxtImporteFormaPago().getValue()+1-1){
								cargaCorrecta = false;
								getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
								DlgMessage.showMessage(getOwner(), "Datos incorrectos de la nota de cr�dito.\nFavor verifique.", DlgMessage.ERROR_MESSAGE);
								getJTxtImporteFormaPago().setValue(nota.getTotal());
								getJTxtNumero().requestFocus();
							}else{
								cargaCorrecta = true;
								detallePago. setNotacredito((Integer)getJTxtNumero().getValue());
							}
						}else{
							cargaCorrecta = false;
							getJTblPagos().removeRow(getJTblPagos().getRowCount()-1);
							DlgMessage.showMessage(getOwner(), "Datos incorrectos de la nota de cr�dito.\nFavor verifique.", DlgMessage.ERROR_MESSAGE);
						}
					}else{
						cargaCorrecta = true;
						//en efectivo u otra forma de pago no hay accion adicional.
					}
					
					if(cargaCorrecta){
						getJTblPagos(). setValueAt(detallePago, getJTblPagos().getRowCount()-1, 3);
						getJTxtTotalAcumulado().setValue(detallePago.getMonto());
						calcularPendiente();
						posicionInicial();
					}
					
					
//					if(getJCmbFormaPago().getSelectedItem().toString().equals("CHEQUE")){
//						JOptionPane.showMessageDialog(null, "es cheque");
//						detallePago. setChequenro(getJTxtNumero().getText().trim());
//						detallePago. setBanco(new ControladorBanco().getBanco(JFramePanambiMain.session.getConn(), (String)getJCmbBanco().getSelectedItem()));
//					
//					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE DEBITO")){
//						JOptionPane.showMessageDialog(null, "t de debito");
//						detallePago. setBouchernro(getJTxtNumero().getText().trim());
//					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("TARJETA DE CREDITO")){
//						JOptionPane.showMessageDialog(null, "tr de credito");
//						detallePago. setBouchernro(getJTxtNumero().getText().trim());
//					}else if (getJCmbFormaPago().getSelectedItem().toString().equals("NOTA DE CREDITO")){
//						JOptionPane.showMessageDialog(null, "nota cer");
//						detallePago. setNotacredito((Integer)getJTxtNumero().getValue());
//					}else{
//						JOptionPane.showMessageDialog(null, "efectivo");
//						//en efectivo u otra forma de pago no hay accion adicional.
//					}
//					
//					getJTblPagos(). setValueAt(detallePago, getJTblPagos().getRowCount()-1, 3);
//					
//					totalAcumulado = (Double)getJTxtImporteFormaPago().getValue();
//					getJTxtTotalAcumulado().setValue(totalAcumulado);
//					calcularPendiente();
//					posicionInicial();
//					
				}
				
			}else{
				Toolkit.getDefaultToolkit().beep();
				getJTxtImporteFormaPago().requestFocus();
			}
		}else{
			Toolkit.getDefaultToolkit().beep();
			getJCmbFormaPago().requestFocus();
		}
		}catch(Exception e ){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Carga los valores del combo para Bancos
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void poblarListaBancos(){
		try{
			 List<String> bancos = new ArrayList<String>();
			 List<Banco> listaba = controladorBanco.getBancosActivos(JFramePanambiMain.session.getConn());
			 for (Banco ban : listaba) {
					bancos.add(ban.getDescripcion());
				}
			 getJCmbBanco().setModel(new ListComboBoxModel<String>(bancos));
			 getJCmbBanco().setSelectedIndex(0);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void posicionInicial(){
		getJCmbFormaPago().setSelectedItem("EFECTIVO");
		getJTxtImporteFormaPago().setValue(0.0);
		
		if((Double)getJTxtTotalAcumulado().getValue()>=(Double)getJTxtTotal().getValue()){
			getJBtnEfectuarPago().requestFocus();
		}else getJTxtImporteFormaPago().requestFocus();
		getJLblNumero().setVisible(false);
		getJCmbBanco().setVisible(false);
	}

	
	private JPanel getJPnlPagos() {
		if (jPnlPagos == null) {
			jPnlPagos = new JPanel();
			jPnlPagos.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_jPnlPagos = new GroupLayout(jPnlPagos);
			gl_jPnlPagos.setHorizontalGroup(
				gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlPagos.createSequentialGroup()
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(18)
								.addComponent(getJLblImporte(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtImporteFormaPago(), GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(18)
								.addComponent(getLblFormaDePago())
								.addGap(10)
								.addComponent(getJCmbFormaPago(), GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(18)
								.addComponent(getJLblNumero(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtNumero(), GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getJBtnExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(18)
								.addComponent(getJLblBanco(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJCmbBanco(), GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(109)
								.addComponent(getJBtnAgregarPago(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
								.addGap(26)
								.addComponent(getButtonPanambi_1_1(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(22)
								.addComponent(getScrollPane_1_1(), GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(88)
								.addComponent(getLblpnmbTotalAcumulado(), GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtTotalAcumulado(), GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(19, Short.MAX_VALUE))
			);
			gl_jPnlPagos.setVerticalGroup(
				gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlPagos.createSequentialGroup()
						.addGap(13)
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addComponent(getJLblImporte(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtImporteFormaPago(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addGap(14)
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblFormaDePago(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(2)
								.addComponent(getJCmbFormaPago(), GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGap(15)
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
									.addComponent(getJLblNumero(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtNumero(), GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
								.addGap(13)
								.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_jPnlPagos.createSequentialGroup()
										.addGap(2)
										.addComponent(getJLblBanco(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
									.addComponent(getJCmbBanco(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
							.addComponent(getJBtnExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(14)
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(1)
								.addComponent(getJBtnAgregarPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getButtonPanambi_1_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(15)
						.addComponent(getScrollPane_1_1(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
						.addGap(25)
						.addGroup(gl_jPnlPagos.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbTotalAcumulado(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_jPnlPagos.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtTotalAcumulado(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
			);
			jPnlPagos.setLayout(gl_jPnlPagos);
		}
		return jPnlPagos;
	}
	private JScrollPane getScrollPane_1_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setViewportView(getJTblPagos());
			
			
		}
		return scrollPane_1;
	}
	private JButtonPanambi getButtonPanambi_1_1() {
		if (jBtnQuitarPago == null) {
			jBtnQuitarPago = new JButtonPanambi();
			jBtnQuitarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doQuitarImporte();
				}
			});
			jBtnQuitarPago.setToolTipText("Quitar importe");
			jBtnQuitarPago.setText("Quitar");
			jBtnQuitarPago.setFont(new Font("Dialog", Font.PLAIN, 12));
		}
		return jBtnQuitarPago;
	}
	
	private void doQuitarImporte() {
		try {
			ArrayList<Integer> toRemove = new ArrayList<Integer>();
			for (int i = 0; i < getJTblPagos().getRowCount(); i++) {
				Boolean selected = (Boolean) getJTblPagos().getValueAt(i, 0);
				if (selected) {
					toRemove.add(i);
				}
			}
			if (toRemove.isEmpty()) {
				Toolkit.getDefaultToolkit().beep();
				//throw new ValidException("Debe seleccionar el producto que desea quitar");
			}
			Integer[] toRemoveInt = new Integer[toRemove.size()];
			toRemove.toArray(toRemoveInt);
			getJTblPagos().removeRows(toRemoveInt);
			
			Double totalAcumulado =0.0;
			
			if(getJTblPagos().getRowCount()!=0){
				for(int i = 0 ; i<getJTblPagos().getRowCount();i++){
					totalAcumulado+=(Double)jTblPagos.getValueAt(i, 2);
				}
			}
							
			getJTxtTotalAcumulado().setValue(totalAcumulado);
			calcularPendiente();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JTablePanambi getJTblPagos() {
		if (jTblPagos == null) {
			String[] columnNames = { "Seleccion", "Forma de pago", "Monto","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, String.class);
			types.put(2, Double.class);
			Integer[] editable = {0};
			jTblPagos= new JTablePanambi(columnNames, editable, types);
			jTblPagos.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jTblPagos.setRowHeight(20);
			jTblPagos.getColumnModel().getColumn(0).setPreferredWidth(10);
			jTblPagos.getColumnModel().getColumn(1).setPreferredWidth(60);
			jTblPagos.getColumnModel().getColumn(2).setPreferredWidth(60);
		//	jTblPagos.getColumnModel().getColumn(3).setPreferredWidth(0);
			
			jTblPagos.getColumnModel().getColumn(3).setMinWidth(0);
			jTblPagos.getColumnModel().getColumn(3).setPreferredWidth(0);
			jTblPagos.getColumnModel().getColumn(3).setMaxWidth(0);
			
		}
		return jTblPagos;
	}
	private JLabel getJLblImporte() {
		if (jLblImporte == null) {
			jLblImporte = new JLabel("Importe");
			jLblImporte.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return jLblImporte;
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			Devolucion dev = (Devolucion) obj;
			
			getJTxtImporteFormaPago().setValue(dev.getTotal());
			getJTxtNumero().setValue(dev.getNotaCredito());
			
			getJBtnAgregarPago().requestFocus();
		}
		
	}
	
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			JOptionPane.showMessageDialog(null, "entro en set no values");
			getJCmbFormaPago().setSelectedItem("EFECTIVO");
			getJTxtImporteFormaPago().setValue(0.0);
			posicionInicial();
		}
	}
	private JLabelPanambi getLblpnmbPendiente() {
		if (lblpnmbPendiente == null) {
			lblpnmbPendiente = new JLabelPanambi();
			lblpnmbPendiente.setText("Pendiente");
			lblpnmbPendiente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPendiente.setFont(new Font("Tahoma", Font.BOLD, 16));
		}
		return lblpnmbPendiente;
	}
	private JTextFieldDouble getJTxtPendientePorPagar() {
		if (jTxtPendientePorPagar == null) {
			jTxtPendientePorPagar = new JTextFieldDouble();
			jTxtPendientePorPagar.setForeground(Color.BLACK);
			jTxtPendientePorPagar.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtPendientePorPagar.setEditable(false);
			jTxtPendientePorPagar.setFont(new Font("Tahoma", Font.BOLD, 22));
			jTxtPendientePorPagar.setFocusable(false);
		}
		return jTxtPendientePorPagar;
	}
	private JLabelPanambi getLblpnmbTotalAcumulado() {
		if (lblpnmbTotalAcumulado == null) {
			lblpnmbTotalAcumulado = new JLabelPanambi();
			lblpnmbTotalAcumulado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotalAcumulado.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblpnmbTotalAcumulado.setText("Total acumulado");
		}
		return lblpnmbTotalAcumulado;
	}
	private JTextFieldDouble getJTxtTotalAcumulado() {
		if (jTxtTotalAcumulado == null) {
			jTxtTotalAcumulado = new JTextFieldDouble();
			jTxtTotalAcumulado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalAcumulado.setForeground(Color.BLACK);
			jTxtTotalAcumulado.setFont(new Font("Tahoma", Font.BOLD, 22));
			jTxtTotalAcumulado.setFocusable(false);
		}
		return jTxtTotalAcumulado;
	}
	private JLabel getJLblNumero() {
		if (jLblNumero == null) {
			jLblNumero = new JLabel("Nro. Cheque");
			jLblNumero.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return jLblNumero;
	}
	private JTextFieldUpper getJTxtNumero() {
		if (jTxtNumero == null) {
			jTxtNumero = new JTextFieldUpper();
			jTxtNumero.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtNumero.setFont(new Font("Tahoma", Font.BOLD, 14));
			jTxtNumero.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostNumero();
				}
			});
		}
		return jTxtNumero;
	}
	
	private void lostNumero(){
		if(getJCmbFormaPago().getSelectedItem().equals("NOTA DE CREDITO")){
			//JOptionPane.showMessageDialog(null, "Esta seleccionado nota de credito");
			Integer nroNota = 0;
			Devolucion nota = null;
			try{
//				JOptionPane.showMessageDialog(null, "valor");
//				JOptionPane.showMessageDialog(null, valor);
				nroNota = Integer.parseInt(getJTxtNumero().getText());
				
			}catch(Exception e){
				nroNota = 0;
			}
			
			if(nroNota !=0){
				try{
					nota = new ControladorDevolucion().getNotaCreditoActiva(JFramePanambiMain.session.getConn(), nroNota);
					
					if(getJTxtNumero().getText()==null){
						
					}else if(nota==null && getJTxtNumero().getText()!=null){
						getJTxtImporteFormaPago().setValue(0.0);
						getJTxtNumero().setValue(null);
						doPrompNotaCredito();
					}else if (nota !=null){
						getJTxtImporteFormaPago().setValue(nota.getTotal());
						getJTxtNumero().setValue(nota.getNotaCredito());
					}
					
				}catch(Exception e){
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			}else{
				//JOptionPane.showMessageDialog(null, "entro como valor 0 tiene que asignar 0 a importe y nada a texto numero");
				getJTxtImporteFormaPago().setValue(0.0);
				getJTxtNumero().setValue(0);
			}
			
			
		}
	}
	
	private JLabel getJLblBanco() {
		if (jLblBanco == null) {
			jLblBanco = new JLabel("Banco");
			jLblBanco.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return jLblBanco;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbBanco() {
		if (jCmbBanco == null) {
			jCmbBanco = new JComboBoxPanambi();
			jCmbBanco.setModel(new ListComboBoxModel<String>(bancos));
			jCmbBanco.setFont(new Font("Tahoma", Font.BOLD, 12));
			
		}
		return jCmbBanco;
	}
	private JLabelPanambi getLblpnmbPagoDeCuotas() {
		if (lblpnmbPagoDeCuotas == null) {
			lblpnmbPagoDeCuotas = new JLabelPanambi();
			lblpnmbPagoDeCuotas.setForeground(Color.WHITE);
			lblpnmbPagoDeCuotas.setFont(new Font("Tahoma", Font.BOLD, 21));
			lblpnmbPagoDeCuotas.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbPagoDeCuotas.setText("PAGO DE CUOTAS");
			lblpnmbPagoDeCuotas.setBounds(0, 0, 790, 44);
		}
		return lblpnmbPagoDeCuotas;
	}
	private JButtonPanambi getJBtnExaminar() {
		if (jBtnExaminar == null) {
			jBtnExaminar = new JButtonPanambi();
			jBtnExaminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doPrompNotaCredito();
				}
			});
			jBtnExaminar.setToolTipText("Buscar nota de credito");
			jBtnExaminar.setMnemonic('B');
			jBtnExaminar.setText("... ");
		}
		return jBtnExaminar;
	}
}
