package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;

public class DlgDetallesNotaCredito extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4763917049064814670L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetalleDeNota;
	private JPanel panel_1;
	private JLabelPanambi lblpnmbNmeroDeNota;
	private JLabelPanambi lblpnmbFechaGenerada;
	private JLabelPanambi lblpnmbValor;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbFacturaDevuelta;
	private JLabelPanambi lblpnmbUsuarioRegistrador;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbCliente;
	private JLabelPanambi lblpnmbFechaAnulacion;
	private JLabelPanambi lblpnmbFechaUtilizada;
	private JLabelPanambi lblpnmbFacturaUtilizada;
	private JLabelPanambi lblpnmbComentarios;
	private JTextFieldInteger jTxtNroNota;
	private JTextField jTxtCliente;
	private JTextFieldDouble jTxtValor;
	private JTextField jTxtFacturaDevuelta;
	private JTextField jTxtUsuario;
	private JTextAreaUpper jTxtComentarios;
	private JTextField jTxtFechaGenerada;
	private JTextField jTxtSucursal;
	private JTextField jTxtEstado;
	private JTextField jTxtFechaAnulacion;
	private JTextField jTxtFechaUtilizada;
	private JTextField jTxtFacturaUtilizada;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private Devolucion notaCredito;
	private JLabelPanambi lblpnmbEstadoDeFactura;
	private JTextField jTxtEstadoFacturaUso;
	
	public DlgDetallesNotaCredito(Devolucion nota) {
		this.notaCredito = nota;
		initialize();
	}
	private void initialize() {
		setBounds(100, 100, 630, 518);
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetallesNotaCredito.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		getContentPane().setLayout(null);
		getContentPane().add(getPanel_1());
		getContentPane().add(getPanel());
		getContentPane().add(getPanel_2());
		cargarValores();
	}
	
	private void cargarValores(){
		try{
			getJTxtNroNota().setValor(notaCredito.getNotaCredito());
			getJTxtCliente().setText(notaCredito.getCliente().getNombres()+" "+notaCredito.getCliente().getApellidos());
			getJTxtValor().setValue(notaCredito.getTotal());
			getJTxtFacturaDevuelta().setText(notaCredito.getVenta().getNroComprobante()+"");
			getJTxtUsuario().setText(notaCredito.getUsuario().getUsuario());
			String patronFechas = "dd/MM/yyyy";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFechas);
			getJTxtFechaGenerada().setText(formatoFecha.format(notaCredito.getFecha()));
			getJTxtSucursal().setText(notaCredito.getSucursal().getNombre());
			String estado="";
			if(notaCredito.getEstadoNotaCredito().equals("A")){
				estado = "Activo";
			}else if(notaCredito.getEstadoNotaCredito().equals("I")){
				estado = "Anulado";
			}else if(notaCredito.getEstadoNotaCredito().equals("U")){
				estado = "Usado";
			}
			getJTxtEstado().setText(estado);
			
			if(notaCredito.getFechaAnulacion()!=null){
				getJTxtFechaAnulacion().setText(formatoFecha.format(notaCredito.getFechaAnulacion()));
				getLblpnmbFechaUtilizada().setVisible(false);
				getJTxtFechaUtilizada().setVisible(false);
				getLblpnmbFacturaUtilizada().setVisible(false);
				getJTxtFacturaUtilizada().setVisible(false);
				getLblpnmbEstadoDeFactura().setVisible(false);
				getJTxtEstadoFacturaUso().setVisible(false);
			}else{
				getLblpnmbFechaAnulacion().setVisible(false);
				getJTxtFechaAnulacion().setVisible(false);
			}
			
			if(estado != "Anulado"){
				Venta ventaUsada = new Venta();
				ventaUsada = new ControladorDevolucion().getVentaNotaUsada(JFramePanambiMain.session.getConn(), notaCredito);
				if(ventaUsada!=null){
					getJTxtFechaUtilizada().setText(formatoFecha.format(ventaUsada.getFecha()));
					getJTxtFacturaUtilizada().setText(ventaUsada.getNroComprobante()+"");
					String estadoFacturaUso ="";
					if(ventaUsada.getEstado().equals("A")){
						estadoFacturaUso = "Activo";
					}else if (ventaUsada.getEstado().equals("I")){
						estadoFacturaUso = "Anulado";
					}
					
					getJTxtEstadoFacturaUso().setText(estadoFacturaUso);
					
				}else{
					getLblpnmbFechaUtilizada().setVisible(false);
					getJTxtFechaUtilizada().setVisible(false);
					getLblpnmbFacturaUtilizada().setVisible(false);
					getJTxtFacturaUtilizada().setVisible(false);
					getLblpnmbEstadoDeFactura().setVisible(false);
					getJTxtEstadoFacturaUso().setVisible(false);
				}
			}
			
			getJTxtComentarios().setText(notaCredito.getObservaciones().trim());
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
		
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 616, 40);
			panel.setBackground(new Color(0,0,139));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addComponent(getLblpnmbDetalleDeNota(), GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addComponent(getLblpnmbDetalleDeNota(), GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetalleDeNota() {
		if (lblpnmbDetalleDeNota == null) {
			lblpnmbDetalleDeNota = new JLabelPanambi();
			lblpnmbDetalleDeNota.setForeground(Color.WHITE);
			lblpnmbDetalleDeNota.setText("DETALLE DE NOTA DE CREDITO");
			lblpnmbDetalleDeNota.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblpnmbDetalleDeNota.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblpnmbDetalleDeNota;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 41, 616, 400);
			panel_1.setLayout(null);
			panel_1.add(getLblpnmbNmeroDeNota());
			panel_1.add(getJTxtNroNota());
			panel_1.add(getLblpnmbFechaGenerada());
			panel_1.add(getJTxtFechaGenerada());
			panel_1.add(getLblpnmbCliente());
			panel_1.add(getJTxtCliente());
			panel_1.add(getLblpnmbSucursal());
			panel_1.add(getJTxtSucursal());
			panel_1.add(getLblpnmbValor());
			panel_1.add(getJTxtValor());
			panel_1.add(getLblpnmbEstado());
			panel_1.add(getJTxtEstado());
			panel_1.add(getLblpnmbFacturaDevuelta());
			panel_1.add(getJTxtFacturaDevuelta());
			panel_1.add(getLblpnmbFechaAnulacion());
			panel_1.add(getJTxtFechaAnulacion());
			panel_1.add(getLblpnmbUsuarioRegistrador());
			panel_1.add(getJTxtUsuario());
			panel_1.add(getLblpnmbFechaUtilizada());
			panel_1.add(getJTxtFechaUtilizada());
			panel_1.add(getLblpnmbComentarios());
			panel_1.add(getJTxtComentarios());
			panel_1.add(getLblpnmbFacturaUtilizada());
			panel_1.add(getJTxtFacturaUtilizada());
			panel_1.add(getLblpnmbEstadoDeFactura());
			panel_1.add(getJTxtEstadoFacturaUso());
		}
		return panel_1;
	}
	private JLabelPanambi getLblpnmbNmeroDeNota() {
		if (lblpnmbNmeroDeNota == null) {
			lblpnmbNmeroDeNota = new JLabelPanambi();
			lblpnmbNmeroDeNota.setBounds(22, 24, 104, 20);
			lblpnmbNmeroDeNota.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNmeroDeNota.setText("N\u00FAmero de nota : ");
		}
		return lblpnmbNmeroDeNota;
	}
	private JLabelPanambi getLblpnmbFechaGenerada() {
		if (lblpnmbFechaGenerada == null) {
			lblpnmbFechaGenerada = new JLabelPanambi();
			lblpnmbFechaGenerada.setBounds(319, 24, 100, 20);
			lblpnmbFechaGenerada.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaGenerada.setText("Fecha generada : ");
		}
		return lblpnmbFechaGenerada;
	}
	private JLabelPanambi getLblpnmbValor() {
		if (lblpnmbValor == null) {
			lblpnmbValor = new JLabelPanambi();
			lblpnmbValor.setBounds(22, 108, 104, 20);
			lblpnmbValor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbValor.setText("Valor : ");
		}
		return lblpnmbValor;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setBounds(60, 232, 66, 20);
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbFacturaDevuelta() {
		if (lblpnmbFacturaDevuelta == null) {
			lblpnmbFacturaDevuelta = new JLabelPanambi();
			lblpnmbFacturaDevuelta.setBounds(22, 150, 104, 20);
			lblpnmbFacturaDevuelta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFacturaDevuelta.setText("Factura devuelta : ");
		}
		return lblpnmbFacturaDevuelta;
	}
	private JLabelPanambi getLblpnmbUsuarioRegistrador() {
		if (lblpnmbUsuarioRegistrador == null) {
			lblpnmbUsuarioRegistrador = new JLabelPanambi();
			lblpnmbUsuarioRegistrador.setBounds(22, 193, 104, 20);
			lblpnmbUsuarioRegistrador.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuarioRegistrador.setText("Usuario registrador : ");
		}
		return lblpnmbUsuarioRegistrador;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setBounds(353, 67, 66, 20);
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setBounds(22, 66, 104, 20);
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Cliente : ");
		}
		return lblpnmbCliente;
	}
	private JLabelPanambi getLblpnmbFechaAnulacion() {
		if (lblpnmbFechaAnulacion == null) {
			lblpnmbFechaAnulacion = new JLabelPanambi();
			lblpnmbFechaAnulacion.setBounds(319, 108, 100, 20);
			lblpnmbFechaAnulacion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacion.setText("Fecha anulacion : ");
		}
		return lblpnmbFechaAnulacion;
	}
	private JLabelPanambi getLblpnmbFechaUtilizada() {
		if (lblpnmbFechaUtilizada == null) {
			lblpnmbFechaUtilizada = new JLabelPanambi();
			lblpnmbFechaUtilizada.setBounds(319, 151, 100, 20);
			lblpnmbFechaUtilizada.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaUtilizada.setText("Fecha utilizada : ");
		}
		return lblpnmbFechaUtilizada;
	}
	private JLabelPanambi getLblpnmbFacturaUtilizada() {
		if (lblpnmbFacturaUtilizada == null) {
			lblpnmbFacturaUtilizada = new JLabelPanambi();
			lblpnmbFacturaUtilizada.setBounds(319, 193, 100, 20);
			lblpnmbFacturaUtilizada.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFacturaUtilizada.setText("Factura utilizada : ");
		}
		return lblpnmbFacturaUtilizada;
	}
	private JLabelPanambi getLblpnmbComentarios() {
		if (lblpnmbComentarios == null) {
			lblpnmbComentarios = new JLabelPanambi();
			lblpnmbComentarios.setBounds(22, 282, 104, 20);
			lblpnmbComentarios.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbComentarios.setText("Comentarios : ");
		}
		return lblpnmbComentarios;
	}
	private JTextFieldInteger getJTxtNroNota() {
		if (jTxtNroNota == null) {
			jTxtNroNota = new JTextFieldInteger();
			jTxtNroNota.setBounds(136, 24, 112, 21);
			jTxtNroNota.setFocusable(false);
			jTxtNroNota.setEditable(false);
			jTxtNroNota.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtNroNota;
	}
	private JTextField getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JTextField();
			jTxtCliente.setBounds(136, 68, 182, 21);
			jTxtCliente.setFocusable(false);
			jTxtCliente.setEditable(false);
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtCliente.setColumns(10);
		}
		return jTxtCliente;
	}
	private JTextFieldDouble getJTxtValor() {
		if (jTxtValor == null) {
			jTxtValor = new JTextFieldDouble();
			jTxtValor.setBounds(136, 108, 110, 21);
			jTxtValor.setFocusable(false);
			jTxtValor.setEditable(false);
			jTxtValor.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtValor;
	}
	private JTextField getJTxtFacturaDevuelta() {
		if (jTxtFacturaDevuelta == null) {
			jTxtFacturaDevuelta = new JTextField();
			jTxtFacturaDevuelta.setBounds(136, 150, 109, 21);
			jTxtFacturaDevuelta.setFocusable(false);
			jTxtFacturaDevuelta.setEditable(false);
			jTxtFacturaDevuelta.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFacturaDevuelta.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFacturaDevuelta.setColumns(10);
		}
		return jTxtFacturaDevuelta;
	}
	private JTextField getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextField();
			jTxtUsuario.setBounds(136, 193, 108, 21);
			jTxtUsuario.setFocusable(false);
			jTxtUsuario.setEditable(false);
			jTxtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtUsuario.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtUsuario.setColumns(10);
		}
		return jTxtUsuario;
	}
	private JTextAreaUpper getJTxtComentarios() {
		if (jTxtComentarios == null) {
			jTxtComentarios = new JTextAreaUpper();
			jTxtComentarios.setBounds(144, 283, 412, 94);
			jTxtComentarios.setFocusable(false);
			jTxtComentarios.setEditable(false);
		}
		return jTxtComentarios;
	}
	private JTextField getJTxtFechaGenerada() {
		if (jTxtFechaGenerada == null) {
			jTxtFechaGenerada = new JTextField();
			jTxtFechaGenerada.setBounds(429, 25, 128, 21);
			jTxtFechaGenerada.setFocusable(false);
			jTxtFechaGenerada.setEditable(false);
			jTxtFechaGenerada.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaGenerada.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFechaGenerada.setColumns(10);
		}
		return jTxtFechaGenerada;
	}
	private JTextField getJTxtSucursal() {
		if (jTxtSucursal == null) {
			jTxtSucursal = new JTextField();
			jTxtSucursal.setBounds(136, 232, 112, 21);
			jTxtSucursal.setFocusable(false);
			jTxtSucursal.setEditable(false);
			jTxtSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSucursal.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtSucursal.setColumns(10);
		}
		return jTxtSucursal;
	}
	private JTextField getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextField();
			jTxtEstado.setBounds(429, 66, 112, 21);
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtEstado.setColumns(10);
		}
		return jTxtEstado;
	}
	private JTextField getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JTextField();
			jTxtFechaAnulacion.setBounds(429, 109, 128, 21);
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFechaAnulacion.setColumns(10);
		}
		return jTxtFechaAnulacion;
	}
	private JTextField getJTxtFechaUtilizada() {
		if (jTxtFechaUtilizada == null) {
			jTxtFechaUtilizada = new JTextField();
			jTxtFechaUtilizada.setBounds(429, 152, 128, 21);
			jTxtFechaUtilizada.setFocusable(false);
			jTxtFechaUtilizada.setEditable(false);
			jTxtFechaUtilizada.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaUtilizada.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFechaUtilizada.setColumns(10);
		}
		return jTxtFechaUtilizada;
	}
	private JTextField getJTxtFacturaUtilizada() {
		if (jTxtFacturaUtilizada == null) {
			jTxtFacturaUtilizada = new JTextField();
			jTxtFacturaUtilizada.setBounds(429, 194, 112, 21);
			jTxtFacturaUtilizada.setFocusable(false);
			jTxtFacturaUtilizada.setEditable(false);
			jTxtFacturaUtilizada.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFacturaUtilizada.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFacturaUtilizada.setColumns(10);
		}
		return jTxtFacturaUtilizada;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 440, 616, 40);
			panel_2.add(getBtnpnmbAceptar());
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	
	private JLabelPanambi getLblpnmbEstadoDeFactura() {
		if (lblpnmbEstadoDeFactura == null) {
			lblpnmbEstadoDeFactura = new JLabelPanambi();
			lblpnmbEstadoDeFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstadoDeFactura.setText("Estado de Factura de uso : ");
			lblpnmbEstadoDeFactura.setBounds(279, 232, 140, 20);
		}
		return lblpnmbEstadoDeFactura;
	}
	private JTextField getJTxtEstadoFacturaUso() {
		if (jTxtEstadoFacturaUso == null) {
			jTxtEstadoFacturaUso = new JTextField();
			jTxtEstadoFacturaUso.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstadoFacturaUso.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtEstadoFacturaUso.setFocusable(false);
			jTxtEstadoFacturaUso.setEditable(false);
			jTxtEstadoFacturaUso.setColumns(10);
			jTxtEstadoFacturaUso.setBounds(429, 233, 112, 21);
		}
		return jTxtEstadoFacturaUso;
	}
}
