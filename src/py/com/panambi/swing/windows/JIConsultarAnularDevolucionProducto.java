package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import py.com.panambi.bean.DetalleDevolucion;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.components.JTextAreaUpper;


public class JIConsultarAnularDevolucionProducto extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5264254496284968813L;
	private JPanel cabecera;
	private JLabelPanambi lblpnmbNroFactura;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbFechaDeVenta;
	private JPanel jPnlCliente;
	private JLabelPanambi lblpnmbCliente;
	private JTextFieldInteger jTxtCodCliente;
	private JTextFieldUpper jTxtNombreApellido;
	private JPanel jPnlSucursal;
	private JLabelPanambi jLblSucursal;
	private JTextFieldInteger jTxtCodSucursal;
	private JTextFieldUpper jTxtNombreSucursal;
	private JScrollPane scrollPane;
	private JTablePanambi jTblDetalleDevolucion;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JTextField jTxtFechaDevolucion;
	//private Venta venta;
	private JLabelPanambi lblpnmbDevolucion;
	private JTextFieldDouble jTxtTotalDevuelto;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private ControladorVenta controladorVenta = new ControladorVenta();
	private Devolucion devolucion;
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbAnular;
	private JLabelPanambi lblpnmbCodDevolucion;
	private JTextFieldInteger jTxtCodDevolucion;
	private JLabelPanambi lblpnmbEstadoDeDevolucion;
	private JTextField jTxtEstadoDevolucion;
	private JLabelPanambi lblpnmbNotaDeCredito;
	private JTextFieldInteger jTxtNroNotaCredito;
	private JLabelPanambi lblpnmbEstadoDeNota;
	private JTextField jTxtEstadoNotaCredito;
	private JLabelPanambi lblpnmbUsuario;
	private JTextFieldUpper jTxtUsuario;
	private JLabelPanambi lblpnmbComentarios;
	private JTextAreaUpper jTxtObservaciones;
	
	public JIConsultarAnularDevolucionProducto(Devolucion dev) throws Exception {
		initialize();
		this.devolucion = dev;
		setValues(devolucion, null);
		getJTxtNroFactura().requestFocus();
	}
	
	public JIConsultarAnularDevolucionProducto() throws Exception {
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setBounds(100, 100, 953, 505);
		setTitle("Anulacion de devoluciones");
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getCabecera(), GroupLayout.DEFAULT_SIZE, 913, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
							.addGap(27))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 913, Short.MAX_VALUE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(getCabecera(), GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
					.addGap(16))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("consultarAnularDevolucion");
		doLimpiar();
	}
	
	private void doLimpiar(){
		devolucion = null;
		getJTxtNroFactura().setValor(null);
		getJTxtFechaDevolucion().setText("");
		getJTxtCodDevolucion().setValor(null);
		getJTxtEstadoDevolucion().setText("");
		getJTxtNroNotaCredito().setValor(null);
		getJTxtEstadoNotaCredito().setText(null);
		getJTxtUsuario().setText("");
		getJTxtCodCliente().setValor(null);
		getJTxtNombreApellido().setText("");
		getJTxtCodSucursal().setValor(null);
		getJTxtNombreSucursal().setText("");
		getJTxtObservaciones().setText("");
		getJTxtTotalDevuelto().setValue(0.0);
		
		if(getJTblDetalleDevolucion().getRowCount()!=0){
			getJTblDetalleDevolucion().resetData(0);
		}
	}
	
	private JPanel getCabecera() {
		if (cabecera == null) {
			cabecera = new JPanel();
			cabecera.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_cabecera = new GroupLayout(cabecera);
			gl_cabecera.setHorizontalGroup(
				gl_cabecera.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(8)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbNotaDeCredito(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(27)
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)))
						.addGap(4)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getJTxtNroNotaCredito(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
						.addGap(28)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbEstadoDeNota(), GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(25)
								.addComponent(getLblpnmbFechaDeVenta(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
						.addGap(6)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getJTxtEstadoNotaCredito(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaDevolucion(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
						.addGap(33)
						.addComponent(getJPnlCliente(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(24)
						.addComponent(getLblpnmbCodDevolucion(), GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtCodDevolucion(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
						.addGap(45)
						.addComponent(getLblpnmbEstadoDeDevolucion(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
						.addGap(6)
						.addComponent(getJTxtEstadoDevolucion(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(18)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbComentarios(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(4)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
								.addGap(43)
								.addComponent(getLblpnmbDevolucion(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtTotalDevuelto(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtObservaciones(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGap(33)
						.addComponent(getJPnlSucursal(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
			);
			gl_cabecera.setVerticalGroup(
				gl_cabecera.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(9)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addComponent(getLblpnmbNotaDeCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(12)
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addComponent(getJTxtNroNotaCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(11)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(5)
								.addComponent(getLblpnmbEstadoDeNota(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(9)
								.addComponent(getLblpnmbFechaDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtEstadoNotaCredito(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(11)
								.addComponent(getJTxtFechaDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJPnlCliente(), GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
						.addGap(5)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtCodDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(3)
								.addComponent(getLblpnmbEstadoDeDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtEstadoDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(6)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(4)
								.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(11)
								.addComponent(getLblpnmbComentarios(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJPnlSucursal(), GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_cabecera.createSequentialGroup()
										.addGap(4)
										.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
											.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
									.addComponent(getJTxtTotalDevuelto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(11)
								.addComponent(getJTxtObservaciones(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)))
						.addGap(14))
			);
			cabecera.setLayout(gl_cabecera);
			doLimpiar();
			setShortcuts(this);
		}
		return cabecera;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.setToolTipText("Numero de Factura");
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
		}
		return jTxtNroFactura;
	}
	
	private void lostFocusCodDevolucion(){
		
		if(getJTxtCodDevolucion().getValor()==null){
			//getJTxtCod().requestFocus();
			doLimpiar();
		}else{
			try {
				Devolucion dev = controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), getJTxtCodDevolucion().getValor());
				if (dev!= null) {
					setValues(dev, null);
				} else {
					doLimpiar();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	
	private void lostFocusNroFactura(){
		
		if(getJTxtNroFactura().getValor()==null){
			doLimpiar();
		}else{
			try {
				
				Venta ven = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				if(ven!= null){
					Devolucion dev = controladorDevolucion.getDevolucionPorVenta(JFramePanambiMain.session.getConn(), ven.getCodVenta());
					if (dev!= null) {
						if(controladorDevolucion.cantidadDevoluciones(JFramePanambiMain.session.getConn(), ven)>1&&getJTxtNroNotaCredito().getValor()== null){
//							DlgMessage.showMessage(getOwner(), "M�s de una devoluci�n para la factura n�mero "+getJTxtNroFactura().getValor()+".\n"
//									+ "Se deplegara la opci�n de b�squeda para seleccionar el registro espec�fico.", DlgMessage.WARNING_MESSAGE);
							doLimpiar();
							doBuscar(ven.getCodVenta());
							getJTxtCodDevolucion().requestFocus();
						}else {
//							dev = controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), dev.getCodDevolucion());
							setValues(dev, null);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No existe devoluci�n para la factura n�mero "+getJTxtNroFactura().getValor(), DlgMessage.WARNING_MESSAGE);
						doLimpiar();
					}
				}else{
					doLimpiar();
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	private JLabelPanambi getLblpnmbFechaDeVenta() {
		if (lblpnmbFechaDeVenta == null) {
			lblpnmbFechaDeVenta = new JLabelPanambi();
			lblpnmbFechaDeVenta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDeVenta.setText("Fecha de devolucion : ");
		}
		return lblpnmbFechaDeVenta;
	}
	private JPanel getJPnlCliente() {
		if (jPnlCliente == null) {
			jPnlCliente = new JPanel();
			jPnlCliente.setToolTipText("Informacion del cliente");
			jPnlCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_jPnlCliente = new GroupLayout(jPnlCliente);
			gl_jPnlCliente.setHorizontalGroup(
				gl_jPnlCliente.createParallelGroup(Alignment.LEADING)
					.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_jPnlCliente.createSequentialGroup()
						.addGap(12)
						.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
						.addGap(9)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))
			);
			gl_jPnlCliente.setVerticalGroup(
				gl_jPnlCliente.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlCliente.createSequentialGroup()
						.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(5)
						.addGroup(gl_jPnlCliente.createParallelGroup(Alignment.LEADING)
							.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			jPnlCliente.setLayout(gl_jPnlCliente);
		}
		return jPnlCliente;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setText("Cliente");
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.CENTER);
			
			lblpnmbCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbCliente;
	}
	private JTextFieldInteger getJTxtCodCliente() {
		if (jTxtCodCliente == null) {
			jTxtCodCliente = new JTextFieldInteger();
			jTxtCodCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodCliente.setToolTipText("Codigo del cliente");
			jTxtCodCliente.setEditable(false);
			jTxtCodCliente.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodCliente.setFocusable(false);
		}
		return jTxtCodCliente;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setToolTipText("Nombre del cliente");
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreApellido.setFocusable(false);
		}
		return jTxtNombreApellido;
	}
	private JPanel getJPnlSucursal() {
		if (jPnlSucursal == null) {
			jPnlSucursal = new JPanel();
			jPnlSucursal.setToolTipText("Informacion de sucursal");
			jPnlSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_jPnlSucursal = new GroupLayout(jPnlSucursal);
			gl_jPnlSucursal.setHorizontalGroup(
				gl_jPnlSucursal.createParallelGroup(Alignment.LEADING)
					.addComponent(getJLblSucursal(), GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_jPnlSucursal.createSequentialGroup()
						.addGap(12)
						.addComponent(getJTxtCodSucursal(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
						.addGap(9)
						.addComponent(getJTxtNombreSucursal(), GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))
			);
			gl_jPnlSucursal.setVerticalGroup(
				gl_jPnlSucursal.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlSucursal.createSequentialGroup()
						.addComponent(getJLblSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(5)
						.addGroup(gl_jPnlSucursal.createParallelGroup(Alignment.LEADING)
							.addComponent(getJTxtCodSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNombreSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			jPnlSucursal.setLayout(gl_jPnlSucursal);
		}
		return jPnlSucursal;
	}
	private JLabelPanambi getJLblSucursal() {
		if (jLblSucursal == null) {
			jLblSucursal = new JLabelPanambi();
			jLblSucursal.setText("Sucursal");
			jLblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jLblSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return jLblSucursal;
	}
	private JTextFieldInteger getJTxtCodSucursal() {
		if (jTxtCodSucursal == null) {
			jTxtCodSucursal = new JTextFieldInteger();
			jTxtCodSucursal.setToolTipText("Codigo de sucursal");
			jTxtCodSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodSucursal.setEditable(false);
			jTxtCodSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodSucursal.setFocusable(false);
		}
		return jTxtCodSucursal;
	}
	private JTextFieldUpper getJTxtNombreSucursal() {
		if (jTxtNombreSucursal == null) {
			jTxtNombreSucursal = new JTextFieldUpper();
			jTxtNombreSucursal.setToolTipText("Nombre de la sucursal");
			jTxtNombreSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreSucursal.setEditable(false);
			jTxtNombreSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreSucursal.setFocusable(false);
		}
		return jTxtNombreSucursal;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblDetalleDevolucion());
			scrollPane.setFocusable(false);
		}
		return scrollPane;
	}

	private JTablePanambi getJTblDetalleDevolucion() {
		if (jTblDetalleDevolucion == null) {
			String[] columnNames = { "Item", "Producto", "Monto unitario","Cantidad devuelta","Total"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(2, Integer.class);
			types.put(3, Integer.class);
			types.put(4, Double.class);
			types.put(5, Double.class);
			Integer[] editable = {};
			jTblDetalleDevolucion = new JTablePanambi(columnNames,editable,types);
			
			anadeListenerAlModelo(jTblDetalleDevolucion);
			
			jTblDetalleDevolucion.setRowHeight(20);
			jTblDetalleDevolucion.getColumnModel().getColumn(0).setPreferredWidth(2);
			jTblDetalleDevolucion.getColumnModel().getColumn(1).setPreferredWidth(250);
			jTblDetalleDevolucion.getColumnModel().getColumn(2).setPreferredWidth(10);
			jTblDetalleDevolucion.getColumnModel().getColumn(3).setPreferredWidth(10);
			jTblDetalleDevolucion.getColumnModel().getColumn(4).setPreferredWidth(60);
			jTblDetalleDevolucion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTblDetalleDevolucion.setCellSelectionEnabled(true);
			jTblDetalleDevolucion.setFocusable(false);
//			// configura el campo de texto
//			  JFormattedTextField formatoCantidadDevuelta= null;
//			  try {
//			   // s�lo admite 4 n�meros
//			   formatoCantidadDevuelta = new JFormattedTextField(new MaskFormatter("####"));
//			  }catch (Exception e) {
//					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//				}
//			  TableCellEditor editor =  new DefaultCellEditor(formatoCantidadDevuelta);
//			  jTblDetalleDevolucion.getColumnModel().getColumn(4).setCellEditor(editor);
		}
		return jTblDetalleDevolucion;
	}
	
	/**
     * Se a�ade el listener al model
     */
    private void anadeListenerAlModelo(JTablePanambi tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent evento) {
            	doCheckDevolucion();
            }
            
        });
    }
    
    private void doCheckDevolucion(){
    	Integer rowSelect = getJTblDetalleDevolucion().getSelectedRow();
    	if(rowSelect!=-1){
    		Integer cantidadFacturada =(Integer)getJTblDetalleDevolucion().getValueAt(rowSelect, 2);
    		Integer cantidadDevuelta = (Integer)getJTblDetalleDevolucion().getValueAt(rowSelect, 3);
    		
	    	if(cantidadDevuelta>cantidadFacturada){
	    		DlgMessage.showMessage(getOwner(), "Cantidad mayor a la facturaci�n\nFavor modifique la cantidad a devolver", DlgMessage.WARNING_MESSAGE);
	    		getJTblDetalleDevolucion().setValueAt(0, rowSelect, 3);
	    	}else{
	    		Double montoDevuelto = 0.0;
	        	for(int i = 0;i<getJTblDetalleDevolucion().getRowCount();i++){
	        		montoDevuelto+=(Double)getJTblDetalleDevolucion().getValueAt(i, 4)*(Integer)getJTblDetalleDevolucion().getValueAt(i, 3);
	        	}
	        	getJTxtTotalDevuelto().setValue(montoDevuelto);
	        }
    	}
    }
	
//	private void doGuardarDevolucion(){
//		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Est� seguro que quiere guardar esta devoluci�n?\n"
//				+ "Sucursal : "+venta.getSucursal().getNombre()+" \n"
//						+ "Devoluci�n : Gs. "+getJTxtDevolucion().getValue(), "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//		
//		if (ret == JOptionPane.YES_OPTION) {
//			
//			try{
//				
//				devolucion = new Devolucion();
//				devolucion.setTotal((Double)getJTxtDevolucion().getValue());
//				devolucion.setVenta(venta);
//				devolucion.setUsuario(JFramePanambiMain.session.getUsuario());
//				devolucion.setSucursal(venta.getSucursal());
//				devolucion.setCliente(venta.getCliente());
//				
//				List<DetalleDevolucion> detalle = new ArrayList<DetalleDevolucion>();
//				Integer item = 1;
//				for(int i = 0;i<getJTblDetalleVenta().getRowCount();i++){
//					DetalleDevolucion detalleDevolucion;
//					Integer cantidadDevuelta;
//					
//					if(getJTblDetalleVenta().getValueAt(i, 3)==null){
//						cantidadDevuelta = 0;
//					}else{
//						cantidadDevuelta = (Integer) getJTblDetalleVenta().getValueAt(i, 3);
//					}
//				
//					if(cantidadDevuelta!=0){
//						detalleDevolucion = new DetalleDevolucion();
//						detalleDevolucion.setNroItem(item);
//						detalleDevolucion.setCantidad(cantidadDevuelta);
//						detalleDevolucion.setMontoUnitario((Double)getJTblDetalleVenta().getValueAt(i, 4));
//						detalleDevolucion.setTotalItem((Double)getJTblDetalleVenta().getValueAt(i, 4)*cantidadDevuelta);
//						detalleDevolucion.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), (String)getJTblDetalleVenta().getValueAt(i, 1)));
//						item+=1;
//						detalle.add(detalleDevolucion);
//					}
//				}
//				
//				devolucion.setDetalleDevolucion(detalle);
//				
//				if(devolucion.getVenta().getEstado().equals("A")){
//					
//					if(!controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), devolucion)){
//						Integer codDevolucionGenerado = controladorDevolucion.guardarDevolucion(JFramePanambiMain.session.getConn(), devolucion);
//						DlgMessage.showMessage(getOwner(), "Devoluci�n registrada exitosamente\nSe ha generado una nota de credito para la devolucion", DlgMessage.INFORMATION_MESSAGE);
//						doLimpiar();
//						getJTxtNroFactura().requestFocus();
//						doGenerarNotaCredito(controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), codDevolucionGenerado));
//						
//					}else{
//						DlgMessage.showMessage(getOwner(), "Ya se ha registrado una devolucion para la factura nro "+devolucion.getVenta().getNroComprobante(), DlgMessage.WARNING_MESSAGE);
//					}
//				}else{
//					DlgMessage.showMessage(getOwner(), "La venta con factura nro "+devolucion.getVenta().getNroComprobante()+" se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
//				}
//				
//			} catch (SQLException e) {
//				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
//					DlgMessage.showMessage(getOwner(), "Error de integridad.\nYa se ha registrado una devolucion para esta facturacion.", DlgMessage.ERROR_MESSAGE);
//				} else {
//					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//				}	
//			}catch(Exception e){
//				logger.error(e.getMessage(), e);
//				DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
//			}
//		}
//	}
	
//	private void doGenerarNotaCredito(Devolucion d) {
//		try {
//			PanambiUtils.setWaitCursor(getOwner());
//			Map<String, Object> parameters = new HashMap<String, Object>();
//			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularDevolucionProducto.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
//			Image image = icon.getImage();
//			parameters.put("LOGO", image);
//			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
//			
//			parameters.put("NRONOTACREDITO", d.getNotaCredito());
//			parameters.put("CLIENTE", d.getCliente().getNombres()+" "+d.getCliente().getApellidos());
//			parameters.put("TOTALCREDITO",d.getTotal());
//			
//			
//			URL url = JIConsultarAnularDevolucionProducto.class.getResource("/py/com/panambi/informes/reports/JRNotaCredito.jasper");
//			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
//			
//			JasperPrint jasperPrint;
//			
//			
//			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceNotaCredito(JFramePanambiMain.session.getConn(),d.getCodDevolucion()));
//			
//			List<?> pages = jasperPrint.getPages();
//			if (!pages.isEmpty()) {
//				JasperViewer.viewReport(jasperPrint, false);
//				
//			} else {
//				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
//			}
//		} catch (Exception e) {
//			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
//		} finally {
//			PanambiUtils.setDefaultCursor(getOwner());
//		}
//
//	}
	
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JTextField getJTxtFechaDevolucion() {
		if (jTxtFechaDevolucion == null) {
			jTxtFechaDevolucion = new JTextField();
			jTxtFechaDevolucion.setToolTipText("Fecha de registro de devolucion");
			jTxtFechaDevolucion.setEditable(false);
			jTxtFechaDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaDevolucion.setColumns(10);
			jTxtFechaDevolucion.setFocusable(false);
		}
		return jTxtFechaDevolucion;
	}
	
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			try{	
				this.devolucion = (Devolucion) obj;
				
					String patron = "dd/MM/yyyy";
				    SimpleDateFormat formato = new SimpleDateFormat(patron);
					getJTxtFechaDevolucion().setText(formato.format(devolucion.getFecha()));
					
					getJTxtNroFactura().setValor(devolucion.getVenta().getNroComprobante());
					getJTxtCodDevolucion().setValor(devolucion.getCodDevolucion());
					
					if(devolucion.getEstadoDevolucion().equals("A")){
						getJTxtEstadoDevolucion().setText("Activo");
					}else{
						getJTxtEstadoDevolucion().setText("Anulado");
					}
					
					if(devolucion.getEstadoNotaCredito().equals("A")){
						getJTxtEstadoNotaCredito().setText("Activo");
					}else if (devolucion.getEstadoNotaCredito().equals("U")){
						getJTxtEstadoNotaCredito().setText("Usado");
					}else{
						getJTxtEstadoNotaCredito().setText("Anulado");
					}
					
					getJTxtNroNotaCredito().setValor(devolucion.getNotaCredito());
					getJTxtUsuario().setText(devolucion.getUsuario().getUsuario());
					
					getJTxtCodCliente().setValor(devolucion.getCliente().getCodCliente());
					getJTxtNombreApellido().setText(devolucion.getCliente().getNombres().toString()+" "+devolucion.getCliente().getApellidos().toString());
					getJTxtCodSucursal().setValor(devolucion.getSucursal().getCodSucursal());
					getJTxtNombreSucursal().setText(devolucion.getSucursal().getNombre());
					
					getJTxtTotalDevuelto().setValue(devolucion.getTotal());
					getJTxtObservaciones().setText(devolucion.getObservaciones());
					//dtmDetalleVenta = new DefaultTableModel();
					List<DetalleDevolucion> detallesDevolucion = new ArrayList<DetalleDevolucion>();
					detallesDevolucion = devolucion.getDetalleDevolucion();
					Iterator<DetalleDevolucion> iteratorDetalleDevolucion = detallesDevolucion.listIterator();
					if(getJTblDetalleDevolucion().getRowCount()!=0){
						getJTblDetalleDevolucion().resetData(0);
					}
					//getJTblDetalleDevolucion().resetData(0);
					
					while (iteratorDetalleDevolucion.hasNext()) {
						getJTblDetalleDevolucion().addRow();
						DetalleDevolucion dd = (DetalleDevolucion) iteratorDetalleDevolucion.next();
						
						getJTblDetalleDevolucion().setValueAt(dd.getNroItem(), getJTblDetalleDevolucion().getRowCount()-1, 0);
						getJTblDetalleDevolucion().setValueAt(dd.getProducto().getDescripcion(), getJTblDetalleDevolucion().getRowCount()-1, 1);
						getJTblDetalleDevolucion().setValueAt(dd.getMontoUnitario(), getJTblDetalleDevolucion().getRowCount()-1, 2);
						getJTblDetalleDevolucion().setValueAt(dd.getCantidad(), getJTblDetalleDevolucion().getRowCount()-1, 3);
						getJTblDetalleDevolucion().setValueAt(dd.getTotalItem(), getJTblDetalleDevolucion().getRowCount()-1, 4);
					}
					getJTxtTotalDevuelto().setValue(devolucion.getTotal());
				
			}catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}

	public void setNoValues(Object obj, Integer source) {

	}
	
	private JLabelPanambi getLblpnmbDevolucion() {
		if (lblpnmbDevolucion == null) {
			lblpnmbDevolucion = new JLabelPanambi();
			lblpnmbDevolucion.setText("Monto de devolucion : ");
			lblpnmbDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbDevolucion;
	}
	private JTextFieldDouble getJTxtTotalDevuelto() {
		if (jTxtTotalDevuelto == null) {
			jTxtTotalDevuelto = new JTextFieldDouble();
			jTxtTotalDevuelto.setForeground(Color.RED);
			jTxtTotalDevuelto.setFont(new Font("Dialog", Font.BOLD, 13));
			jTxtTotalDevuelto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalDevuelto.setEditable(false);
			jTxtTotalDevuelto.setFocusable(false);
		}
		return jTxtTotalDevuelto;
	}
	
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJTxtNroNotaCredito().requestFocus();
				}
			});
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	

	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar devolucion");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(Integer codventa) {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nro. factura", "Fecha", "Nota de cr�dito", "Monto total","Estado de devoluci�n"};
			String[] pks = { "codDevolucion" };
			String sSelect = "SELECT coddevolucion, ";
			sSelect += "(SELECT nrocomprobante FROM ventas v WHERE v.codventa = d.codventa), ";
			sSelect += "to_char(fecha,'dd/MM/yyyy'), notacredito, total, CASE WHEN estadodevolucion = 'A' THEN 'Activo' ELSE 'Anulado' END ";
			sSelect += "FROM devoluciones d ";
			sSelect += "WHERE codventa = "+codventa+" ";
			sSelect += "ORDER BY coddevolucion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Devolucion", pks);
			jb.setVisible(true);
			getJTxtNroFactura().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nro. factura", "Fecha", "Nota de cr�dito", "Monto total","Estado de devoluci�n"};
			String[] pks = { "codDevolucion" };
			String sSelect = "SELECT coddevolucion, ";
			sSelect += "(SELECT nrocomprobante FROM ventas v WHERE v.codventa = d.codventa), ";
			sSelect += "to_char(fecha,'dd/MM/yyyy'), notacredito, total, CASE WHEN estadodevolucion = 'A' THEN 'Activo' ELSE 'Anulado' END ";
			sSelect += "FROM devoluciones d ";
			sSelect += "ORDER BY coddevolucion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Devolucion", pks);
			jb.setVisible(true);
			getJTxtNroFactura().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbAnular() {
		if (btnpnmbAnular == null) {
			btnpnmbAnular = new JButtonPanambi();
			btnpnmbAnular.setMnemonic('A');
			btnpnmbAnular.setToolTipText("Anular devolucion");
			btnpnmbAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnularDevolucion();
				}
			});
			btnpnmbAnular.setText("Anular");
		}
		return btnpnmbAnular;
	}
	
	private void doAnularDevolucion(){
		try{
			if(devolucion == null){
				DlgMessage.showMessage(getOwner(),"Imposible anular devoluci�n.\nuDebe completar los campos.", DlgMessage.ERROR_MESSAGE);
				getJTxtNroNotaCredito().requestFocus();
			}else if(devolucion.getEstadoDevolucion().equals("I")){
				DlgMessage.showMessage(getOwner(), "Imposible anular devoluci�n.\nLa devoluci�n ya se encuentra anulada", DlgMessage.ERROR_MESSAGE);
			}else if(devolucion.getEstadoNotaCredito().equals("U")){
				DlgMessage.showMessage(getOwner(), "Imposible anular devoluci�n.\nLa nota de cr�dito ya se ha utilizado como medio de pago.", DlgMessage.ERROR_MESSAGE);
				getJTxtNroNotaCredito().requestFocus();
//			}else if(controladorDevolucion.notaCreditoUsada(JFramePanambiMain.session.getConn(), devolucion.getNotaCredito())){
//				DlgMessage.showMessage(getOwner(), "Imposible anular devoluci�n.\nLa nota de cr�dito asocioada a la devoluci�n se ha utilizado como medio de pago.", DlgMessage.ERROR_MESSAGE)
				
			}else{
				java.util.Date fechaActual = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				
				java.util.Date fechaDevolucion = (java.util.Date)devolucion.getFecha();
				if (!sdf.format(fechaActual).equals(sdf.format(fechaDevolucion))){
					DlgMessage.showMessage(getOwner(), "Imposible anular la devoluci�n.\nS�lo puede anular una devoluci�n el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
				}else{
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea anular la devoluci�n ? ",
							"", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (ret == JOptionPane.YES_OPTION) {
						
						String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n de la devolucion", "");
						
						if(observaciones!=null){
							if(observaciones.trim().toUpperCase().equals("")){
								DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario de anulaci�n", DlgMessage.WARNING_MESSAGE);
							}else{
								
								if(devolucion.getObservaciones()==null){
									devolucion.setObservaciones("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" : "+observaciones);
								}else{
									devolucion.setObservaciones(devolucion.getObservaciones()+"\nANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" : "+observaciones);
								}
								controladorDevolucion.anularDevolucion(JFramePanambiMain.session.getConn(), devolucion);
								DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
								doLimpiar();
								getJTxtNroNotaCredito().requestFocus();
							}
							
						}
						//controladorDevolucion.anularNotaCredito(JFramePanambiMain.session.getConn(), nronotacredito);
						
						}
				}
				}
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(),e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
//	private void doConsultarAnularNotaCredito(final Devolucion dev) {
//		try {
//			PanambiUtils.setWaitCursor(this);
//			SwingUtilities.invokeLater(new Runnable() {
//				@Override
//				public void run() {
//					JIConsultarAnularNotaCredito jalta = new JIConsultarAnularNotaCredito(dev);
//					getDesktopPane().add(jalta);
//					jalta.centerIt();
//					jalta.setVisible(true);
//				}
//			});
//		} finally {
//			PanambiUtils.setDefaultCursor(this);
//		}
//	}
	
	private JLabelPanambi getLblpnmbCodDevolucion() {
		if (lblpnmbCodDevolucion == null) {
			lblpnmbCodDevolucion = new JLabelPanambi();
			lblpnmbCodDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodDevolucion.setText("Cod. Devolucion : ");
		}
		return lblpnmbCodDevolucion;
	}
	private JTextFieldInteger getJTxtCodDevolucion() {
		if (jTxtCodDevolucion == null) {
			jTxtCodDevolucion = new JTextFieldInteger();
			jTxtCodDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodDevolucion.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusCodDevolucion();
				}
			});
		}
		return jTxtCodDevolucion;
	}
	private JLabelPanambi getLblpnmbEstadoDeDevolucion() {
		if (lblpnmbEstadoDeDevolucion == null) {
			lblpnmbEstadoDeDevolucion = new JLabelPanambi();
			lblpnmbEstadoDeDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstadoDeDevolucion.setText("Estado de devolucion : ");
		}
		return lblpnmbEstadoDeDevolucion;
	}
	private JTextField getJTxtEstadoDevolucion() {
		if (jTxtEstadoDevolucion == null) {
			jTxtEstadoDevolucion = new JTextField();
			jTxtEstadoDevolucion.setEditable(false);
			jTxtEstadoDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstadoDevolucion.setColumns(10);
			jTxtEstadoDevolucion.setFocusable(false);
		}
		return jTxtEstadoDevolucion;
	}
	private JLabelPanambi getLblpnmbNotaDeCredito() {
		if (lblpnmbNotaDeCredito == null) {
			lblpnmbNotaDeCredito = new JLabelPanambi();
			lblpnmbNotaDeCredito.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNotaDeCredito.setText("Nota de credito nro : ");
		}
		return lblpnmbNotaDeCredito;
	}
	private JTextFieldInteger getJTxtNroNotaCredito() {
		if (jTxtNroNotaCredito == null) {
			jTxtNroNotaCredito = new JTextFieldInteger();
			jTxtNroNotaCredito.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroNotaCredito.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroNotaCredito();
				}
			});
		}
		return jTxtNroNotaCredito;
	}
	
	private void lostFocusNroNotaCredito(){
		if(getJTxtNroNotaCredito().getValor()==null){
			//getJTxtNroFactura().requestFocus();
			doLimpiar();
		}else{
			try {
				Devolucion dev = controladorDevolucion.getNotaCredito(JFramePanambiMain.session.getConn(), getJTxtNroNotaCredito().getValor());
				if (dev!= null) {
					setValues(dev, null);
				} else {
					doLimpiar();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	private JLabelPanambi getLblpnmbEstadoDeNota() {
		if (lblpnmbEstadoDeNota == null) {
			lblpnmbEstadoDeNota = new JLabelPanambi();
			lblpnmbEstadoDeNota.setText("Estado de nota de credito : ");
			lblpnmbEstadoDeNota.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbEstadoDeNota;
	}
	private JTextField getJTxtEstadoNotaCredito() {
		if (jTxtEstadoNotaCredito == null) {
			jTxtEstadoNotaCredito = new JTextField();
			jTxtEstadoNotaCredito.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstadoNotaCredito.setEditable(false);
			jTxtEstadoNotaCredito.setColumns(10);
			jTxtEstadoNotaCredito.setFocusable(false);
		}
		return jTxtEstadoNotaCredito;
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setText("Usuario : ");
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbUsuario;
	}
	private JTextFieldUpper getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextFieldUpper();
			jTxtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtUsuario.setEditable(false);
			jTxtUsuario.setFocusable(false);
		}
		return jTxtUsuario;
	}
	private JLabelPanambi getLblpnmbComentarios() {
		if (lblpnmbComentarios == null) {
			lblpnmbComentarios = new JLabelPanambi();
			lblpnmbComentarios.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbComentarios.setText("Comentarios : ");
		}
		return lblpnmbComentarios;
	}
	private JTextAreaUpper getJTxtObservaciones() {
		if (jTxtObservaciones == null) {
			jTxtObservaciones = new JTextAreaUpper();
			jTxtObservaciones.setEditable(false);
			jTxtObservaciones.setFocusable(false);
		}
		return jTxtObservaciones;
	}
}


