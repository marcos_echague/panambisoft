package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarNotaCredito extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2567812276734777881L;
	private JLabelPanambi lblpnmbEstado;
	private JComboBoxPanambi jCmbEstado;
	private JButtonPanambi btnpnmbConsultar;
	private JButtonPanambi btnpnmbLimpiar;
	private JButtonPanambi btnpnmbSalir;
	private JPanel panel;
	private JTextFieldUpper jTxtNombreApellido;
	private JLabelPanambi labelPanambi;
	private JTextField jTxtNroCedula;
	private JLabelPanambi lblpnmbNroCedula;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;
	private JScrollPane scrollPane;
	private JRadioButton rdbtnTodos;
	private JTablePanambi tablePanambi;
	private List<String> sucursales ;
	private ControladorCliente  controladorCliente = new ControladorCliente();
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	private Cliente cliente;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JPopupMenu popupMenu = new JPopupMenu();
	private JMenuItem menuItemDetalles;

	public JIConsultarNotaCredito() throws Exception {
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar notas de credito");
		setBounds(100, 100, 832, 462);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbConsultar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 763, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
							.addGap(28)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(61)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)))
							.addGap(7)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
									.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)))
							.addGap(19))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(27)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(20)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(19)
									.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))))
					.addGap(20)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
					.addGap(22))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("consultarNotaCredito");
		doLimpiar();
		agregarMenuPoput();
		getJTxtNombreApellido().requestFocus();
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		
		cliente = null;
		getRdbtnTodos().setSelected(true);
		getJCmbEstado().setSelectedItem("TODOS");
		poblarListaSucursales();
		getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
		getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
		
		boolean admin = false;
		List<PerfilUsuario> perfilesUsuario = null;
		if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
			perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
		}
		
		if(perfilesUsuario!=null){
			for(int i = 0;i<perfilesUsuario.size();i++){
				PerfilUsuario perfil = perfilesUsuario.get(i);
				if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
					admin = true;
				}
			}
		}
		
		if(admin){
			getJCmbSucursal().setEnabled(true);
		}else{
			getJCmbSucursal().setEnabled(false);
		}
		
		if(getTablePanambi().getRowCount()!=0){
			getTablePanambi().resetData(0);
		}

		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText("");
		
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver mas detalles...");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetalles();
				}
			});
		}
		return menuItemDetalles; 
	}
	
	private void doVerDetalles(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
			}else{
				Devolucion notaCredito = (Devolucion)getTablePanambi().getValueAt(fila, 7);
				DlgDetallesNotaCredito dlgDetalleNotaCredito= new DlgDetallesNotaCredito(notaCredito);
				dlgDetalleNotaCredito.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaSucursales(){
		List<Sucursal>sucur = new ArrayList <Sucursal>();
		try{
			sucursales = new ArrayList<String>();
			sucursales.add("TODAS");
			sucur = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
			for(Sucursal suc :sucur){
				sucursales.add(suc.getNombre().toUpperCase());
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado de nota de credito : ");
		}
		return lblpnmbEstado;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
			jCmbEstado.setToolTipText("Estado de la nota de credito");
			jCmbEstado.setModel(new DefaultComboBoxModel(new String[] {"TODOS","ACTIVOS", "USADOS", "ANULADOS"}));
		}
		return jCmbEstado;
	}
	private JButtonPanambi getBtnpnmbConsultar() {
		if (btnpnmbConsultar == null) {
			btnpnmbConsultar = new JButtonPanambi();
			btnpnmbConsultar.setMnemonic('C');
			btnpnmbConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			btnpnmbConsultar.setToolTipText("Consultar notas de credito");
			btnpnmbConsultar.setText("Consultar");
		}
		return btnpnmbConsultar;
	}
	
	private void doConsultar(){
		try{
			Sucursal suc = null;
			String est = null;
			if(getJCmbSucursal().getSelectedItem().equals("TODAS")){
				suc = null;
			}else{
				suc = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), getJCmbSucursal().getSelectedItem().toString());
			}
			
			if(getJCmbEstado().getSelectedItem().equals("TODOS")){
				est = null;
			}else if(getJCmbEstado().getSelectedItem().equals("ACTIVOS")){
				est ="'A'";
			}else if(getJCmbEstado().getSelectedItem().equals("USADOS")){
				est ="'U'";
			}else{
				est = "'I'";
			}
			
			List<Devolucion> notas = controladorDevolucion.getNotasCreditoForParameter(JFramePanambiMain.session.getConn(), cliente, suc, est);
			
			if(notas.size()==0){
				DlgMessage.showMessage(getOwner(), "Ninguna nota de cr�dito obtenida seg�n parametros ingresados.", DlgMessage.INFORMATION_MESSAGE);
				
				if(getTablePanambi().getRowCount()!=0){
					getTablePanambi().resetData(0);
				}
				
			}else{
				try{
					Iterator<Devolucion> iteratorNotas= notas.listIterator();
					getTablePanambi().resetData(0);
					while (iteratorNotas.hasNext()) {
						getTablePanambi().addRow();
						Devolucion not = (Devolucion) iteratorNotas.next();
						getTablePanambi().setValueAt(not. getNotaCredito(), getTablePanambi().getRowCount()-1, 0);
						getTablePanambi().setValueAt(not.getTotal(), getTablePanambi().getRowCount()-1, 1);
						String patronFecha = "dd/MM/yyyy";
						SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
						getTablePanambi().setValueAt(formatoFecha.format(not.getFecha()), getTablePanambi().getRowCount()-1, 2);
						getTablePanambi().setValueAt(not.getVenta().getNroComprobante(), getTablePanambi().getRowCount()-1, 3);
						getTablePanambi().setValueAt(not.getCliente().getNombres()+" "+not.getCliente().getApellidos(), getTablePanambi().getRowCount()-1, 4);
						getTablePanambi().setValueAt(not.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 5);
						String estado = "";
						
						switch (not.getEstadoNotaCredito()) {
						case "U": estado ="Usado";
							break;
						case "I": estado ="Anulado";
							break;
							
						default: estado ="Activo";
							break;
						}
						
						getTablePanambi().setValueAt(estado, getTablePanambi().getRowCount()-1, 6);
						
//						if(not.getEstadoNotaCredito().equals("U")){
//							Venta ventaUsada = new Venta();
//							ventaUsada = controladorDevolucion.getVentaNotaUsada(JFramePanambiMain.session.getConn(), not);
//							getTablePanambi().setValueAt(ventaUsada.getNroComprobante(), getTablePanambi().getRowCount()-1, 7);
//						}
//						if(not.getFechaAnulacion()!=null){
//							String patronFechaUso = "dd/MM/yyyy";
//							SimpleDateFormat formatoFechaUso = new SimpleDateFormat(patronFechaUso);
//							getTablePanambi().setValueAt(formatoFechaUso.format(not.getFechaAnulacion()), getTablePanambi().getRowCount()-1, 8);
//						}
						
//						getTablePanambi().setValueAt(not.getObservaciones(), getTablePanambi().getRowCount()-1, 9);
						getTablePanambi().setValueAt(not, getTablePanambi().getRowCount()-1, 7);
						
					}
				}catch(Exception e){
					logger.error(e.getMessage(),e);
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			}
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJTxtNroCedula().requestFocus();
				}
			});
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Informacion del cliente");
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(10)
						.addComponent(getLblpnmbNroCedula(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
						.addGap(9)
						.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
						.addGap(24)
						.addComponent(getRdbtnTodos(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getLblpnmbNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getRdbtnTodos())
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setToolTipText("Nombre y apellido del cliente");
			jTxtNombreApellido.setText("");
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setFocusable(false);
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return jTxtNombreApellido;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Cliente");
			labelPanambi.setHorizontalAlignment(SwingConstants.CENTER);
			labelPanambi.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return labelPanambi;
	}
	private JTextField getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextField();
			jTxtNroCedula.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.setColumns(10);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroCedula();
				}
			});
		}
		return jTxtNroCedula;
	}
	
	private void lostFocusNroCedula(){
		
		try{
		
			if(getJTxtNroCedula().getText().length()==0){
				cliente = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Cliente cli =  controladorCliente.getCliente(JFramePanambiMain.session.getConn(), getJTxtNroCedula().getText());
				
				if(cli != null){
					setValues(cli, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					doBuscarCliente();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doBuscarCliente(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nro. de c�dula", "Nombres", "Apellidos" };
			String[] pks = { "codCliente" };
			String sSelect = "SELECT codcliente, nrodocumento, nombres, apellidos ";
			sSelect += "FROM clientes ";
			sSelect += "WHERE codcliente in (SELECT codcliente FROM devoluciones)";
			sSelect += "ORDER BY nrodocumento";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Cliente", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JLabelPanambi getLblpnmbNroCedula() {
		if (lblpnmbNroCedula == null) {
			lblpnmbNroCedula = new JLabelPanambi();
			lblpnmbNroCedula.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroCedula.setText("Nro. Cedula : ");
		}
		return lblpnmbNroCedula;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.setToolTipText("Todos los clientes");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosClientes();
				}
			});
			
			rdbtnTodos.addKeyListener(new java.awt.event.KeyAdapter() {

				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER ) {
						doSelectTodosClientes();
					}
				}
			});
			
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodosClientes(){
		getRdbtnTodos().setSelected(true);
		cliente = null;
		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText(null);
	}
	
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			String[] columnNames = 
				{ "Nro", "Monto","Fecha generada","Factura asoc.","Cliente","Sucursal","Estado","Data"};          //,"Factura uso","Fecha anulacion","Comentarios","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(1, Double.class);
			types.put(2, Date.class);
			types.put(8, Date.class);
			Integer[] editable = {};
			tablePanambi = new JTablePanambi(columnNames,editable,types);
			tablePanambi .setRowHeight(20);
			tablePanambi .getColumnModel().getColumn(0).setPreferredWidth(40);
			tablePanambi .getColumnModel().getColumn(1).setPreferredWidth(50);
			tablePanambi .getColumnModel().getColumn(2).setPreferredWidth(50);
			tablePanambi .getColumnModel().getColumn(3).setPreferredWidth(60);
			tablePanambi .getColumnModel().getColumn(4).setPreferredWidth(150);
			tablePanambi .getColumnModel().getColumn(5).setPreferredWidth(50);
			tablePanambi .getColumnModel().getColumn(6).setPreferredWidth(30);
//			tablePanambi .getColumnModel().getColumn(7).setPreferredWidth(50);
//			tablePanambi .getColumnModel().getColumn(8).setPreferredWidth(60);
//			tablePanambi .getColumnModel().getColumn(9).setPreferredWidth(250);
			tablePanambi.getColumnModel().getColumn(7).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setMaxWidth(0);
			//tablePanambi .setFocusable(false);
		}
		return tablePanambi;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Cliente) {
			this.cliente = (Cliente)obj;
			getRdbtnTodos().setSelected(false);
			getJTxtNombreApellido().setText(cliente.getNombres()+" "+cliente.getApellidos());
			getJTxtNroCedula().setText(cliente.getNroDocumento());
			
		}
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
