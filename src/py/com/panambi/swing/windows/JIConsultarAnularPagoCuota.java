package py.com.panambi.swing.windows;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarAnularPagoCuota extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6031618100348340916L;
	private JLabelPanambi lblpnmbCliente;
	private JTextFieldUpper jTxtCliente;
	private JLabelPanambi lblpnmbNroFactura;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbNroRecibo;
	private JTextFieldInteger jTxtNroRecibo;
	private JLabelPanambi lblpnmbMonto;
	private JTextFieldDouble jTxtMonto;
	private JLabelPanambi lblpnmbFechaDePago;
	private JLabelPanambi lblpnmbEstado;
	private JScrollPane scrollPane;
	private JButtonPanambi btnpnmbLimpiar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbAnular;
	private JButtonPanambi btnpnmbSalir;
	private JTablePanambi jTblCuotas;
	private JLabelPanambi lblpnmbCodPago;
	private JTextFieldInteger jTxtCodPago;
	private JScrollPane scrollPane_1;
	private JTablePanambi jTblDetallePago;
	private ControladorPago controladorPago = new ControladorPago();
	private Pago pago;
	private ControladorPlanPago controladorPlanPago = new ControladorPlanPago();
	private JLabelPanambi lblpnmbObservaciones;
	private JTextAreaUpper jTxtObservaciones;
	private JTextFieldUpper jTxtFechaPago;
	private JTextField jTxtEstado;
	private JButtonPanambi jBtnVerCuotas;
	
	public JIConsultarAnularPagoCuota() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Anular pago de cuotas");
		getJPanelSouth().add(getJBtnVerCuotas());
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbAnular());
		getJPanelSouth().add(getBtnpnmbSalir());
		setBounds(100, 100, 936, 505);
		doLimpiar();
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(getLblpnmbNroRecibo(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJTxtNroRecibo(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getLblpnmbCodPago(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtCodPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getLblpnmbMonto(), GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getLblpnmbFechaDePago(), GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addGap(8)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
							.addGap(11)
							.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
							.addGap(7)
							.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtObservaciones(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 873, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(getScrollPane_1(), GroupLayout.PREFERRED_SIZE, 873, GroupLayout.PREFERRED_SIZE)))
					.addGap(20))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbNroRecibo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNroRecibo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbCodPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbFechaDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtObservaciones(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getScrollPane_1(), GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setHelp("consultarAnularCuota");
		getJTxtNroRecibo().requestFocus();
		setShortcuts(this);
	}
	private void doLimpiar(){
		pago=null;
		getJTxtNroRecibo().setValor(null);
		getJTxtCodPago().setValor(null);
		getJTxtMonto().setValue(null);
		getJTxtFechaPago().setText("");
		getJTxtEstado().setText("");
		getJTxtNroFactura().setValor(null);
		getJTxtCliente().setText("");
		getJTxtObservaciones().setText("");
		
		if(getJTblDetallePago().getRowCount()!=0){
			getJTblDetallePago().resetData(0);
		}
		if(getJTblCuotas().getRowCount()!=0){
			getJTblCuotas().resetData(0);
		}		
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Cliente : ");
		}
		return lblpnmbCliente;
	}
	private JTextFieldUpper getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JTextFieldUpper();
			jTxtCliente.setToolTipText("Cliente ");
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setEditable(false);
			jTxtCliente.setFocusable(false);
		}
		return jTxtCliente;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro. Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setToolTipText("Numero de facturacion");
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
			
		}
		return jTxtNroFactura;
	}
	
	private void lostFocusNroFactura(){
		if(getJTxtNroFactura().getValor()==null){
			doLimpiar();
			
		}else{
			try {
			Venta venta = new Venta();
			venta = new ControladorVenta().getVentaPagadaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
			if(venta!=null){
				Integer cantidadPagos = controladorPago.getCantidadPagosPorFactura(JFramePanambiMain.session.getConn(), venta);
				if(cantidadPagos ==0){
					doLimpiar();
				}else if(cantidadPagos ==1){
					Pago pag = controladorPago.getPagoPorVenta(JFramePanambiMain.session.getConn(), venta);
					if(pag!=null){
						setValues(pag, null);
					}else{
						doLimpiar();
					}
				}else{
					if(getJTxtNroRecibo().getValor()==null && getJTxtCodPago().getValor()==null){
						doBuscar(venta.getCodVenta()+"");
					}
				}
			}else{
				doLimpiar();
			}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	private void doBuscar(String restriccion) {
		try {
			doLimpiar();
			String[] columnNames = { "Cod. Pago", "Factura pagada","Nro. Recibo", "Monto", "Fecha de pago","Cliente","Estado" };
			String[] pks = { "codpago" };
			String sSelect = "SELECT codpago, ";
			sSelect +="(SELECT nrocomprobante FROM ventas WHERE pagos.codventa = ventas.codventa), ";
			sSelect += "nrorecibo, monto, fecha, ";
			sSelect +="(SELECT nombres||' '||apellidos FROM clientes, ventas "
					+ "WHERE ventas.codcliente = clientes.codcliente AND pagos.codventa = ventas.codventa), ";
			sSelect += "(SELECT CASE WHEN  estado = 'A' THEN 'Activo' ELSE 'Anulado' END) AS estado ";
			sSelect += "FROM pagos ";
			sSelect += "WHERE 1 = 1 ";
			sSelect += "AND nrorecibo is not null  ";
			if(restriccion!=null){
				sSelect+="AND codventa = "+restriccion+" ";
			}
			
			sSelect += "ORDER BY codpago ";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Pago", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JLabelPanambi getLblpnmbNroRecibo() {
		if (lblpnmbNroRecibo == null) {
			lblpnmbNroRecibo = new JLabelPanambi();
			lblpnmbNroRecibo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroRecibo.setText("Nro. Recibo : ");
		}
		return lblpnmbNroRecibo;
	}
	private JTextFieldInteger getJTxtNroRecibo() {
		if (jTxtNroRecibo == null) {
			jTxtNroRecibo = new JTextFieldInteger();
			jTxtNroRecibo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroRecibo.setToolTipText("Numero de recibo");
			jTxtNroRecibo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroRecibo();
				}
			});
		}
		return jTxtNroRecibo;
	}
	
	private void lostFocusNroRecibo(){
		if(getJTxtNroRecibo().getValor()==null){
			doLimpiar();
			//getJTxtCodPago().requestFocus();
		}else{
			try {
				Pago pag = controladorPago.getPagoPorRecibo(JFramePanambiMain.session.getConn(), getJTxtNroRecibo().getValor());
				if (pag!= null) {
					setValues(pag, null);
				} else {
					doLimpiar();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	
	
	private JLabelPanambi getLblpnmbMonto() {
		if (lblpnmbMonto == null) {
			lblpnmbMonto = new JLabelPanambi();
			lblpnmbMonto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbMonto.setText("Monto : ");
		}
		return lblpnmbMonto;
	}
	private JTextFieldDouble getJTxtMonto() {
		if (jTxtMonto == null) {
			jTxtMonto = new JTextFieldDouble();
			jTxtMonto.setToolTipText("Monto del pago");
			jTxtMonto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtMonto.setEditable(false);
			jTxtMonto.setFocusable(false);
		}
		return jTxtMonto;
	}
	private JLabelPanambi getLblpnmbFechaDePago() {
		if (lblpnmbFechaDePago == null) {
			lblpnmbFechaDePago = new JLabelPanambi();
			lblpnmbFechaDePago.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDePago.setText("Fecha de pago : ");
		}
		return lblpnmbFechaDePago;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblCuotas());
			scrollPane.setFocusable(false);
		}
		return scrollPane;
	}
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJTxtNroRecibo().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar pago pago de cuota");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar(null);
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	private JButtonPanambi getBtnpnmbAnular() {
		if (btnpnmbAnular == null) {
			btnpnmbAnular = new JButtonPanambi();
			btnpnmbAnular.setMnemonic('A');
			btnpnmbAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnularPagoCuota();
				}
			});
			btnpnmbAnular.setToolTipText("Anular pago de cuota");
			btnpnmbAnular.setText("Anular");
		}
		return btnpnmbAnular;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doAnularPagoCuota(){
		try{
			if(pago!=null){
				if(pago.getEstado().equals("A")){
					if(!pendientesPorAnular(pago)){
						java.util.Date fechaActual = new java.util.Date();
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						java.util.Date fechaPagoCuota = new java.util.Date(pago.getFecha().getTime());
						if (!sdf.format(fechaActual).equals(sdf.format(fechaPagoCuota))){
							DlgMessage.showMessage(getOwner(), "Imposible anular pago cuota.\nS�lo puede anular un pago de cuota el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
						}else{
							Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea anular el pago de cuota?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
							if (ret == JOptionPane.YES_OPTION) {
								String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n del pago de cuota", ""); 
								if(observaciones!=null){
									
									if(observaciones.equals("")){
										DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario", DlgMessage.INFORMATION_MESSAGE);
									}else{
										pago.setObservaciones("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" . "+observaciones);
										controladorPago.anularPagoCuota(JFramePanambiMain.session.getConn(), pago);
										DlgMessage.showMessage(getOwner(), "Operacion exitosa", DlgMessage.INFORMATION_MESSAGE);
										doLimpiar();
										getJTxtNroRecibo().requestFocus();
									}
								}
								
								
							}
						}
					}else{
						DlgMessage.showMessage(getOwner(), "Imposible anular. \nExisten pagos pendientes por anular para factura nro. "+pago.getVenta().getNroComprobante()+".",DlgMessage.WARNING_MESSAGE );
					}
					
				}else{
					DlgMessage.showMessage(getOwner(), "Imposible anular. \nEl pago de cuota ya se encuentra anulado.",DlgMessage.WARNING_MESSAGE );
				}
			}else{
				DlgMessage.showMessage(getOwner(), "Imposible anular. \nFavor cargar los datos del pago.",DlgMessage.WARNING_MESSAGE );
				getJTxtNroRecibo().requestFocus();
			}
		}catch(Exception e ){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	
	private boolean pendientesPorAnular(Pago pag){
		boolean ret = false;
		try{
			Integer mayorCuotaAnular =null;
			
			List <PlanPago> cuotasAnular = new ArrayList<PlanPago>();
			cuotasAnular = controladorPlanPago.getCuotas(JFramePanambiMain.session.getConn(), pag);
			
			mayorCuotaAnular = cuotasAnular.get(0).getNroCuota();
			
			if(cuotasAnular.size()>1){
				for (int i = 1;i<cuotasAnular.size();i++){
					if(cuotasAnular.get(i).getNroCuota()>mayorCuotaAnular){
						mayorCuotaAnular = cuotasAnular.get(i).getNroCuota();
					}
				}
			}
			
			Integer mayorPagado = controladorPlanPago.getMayorCuotaPagada(JFramePanambiMain.session.getConn(), pago.getVenta().getCodVenta());
			if(mayorCuotaAnular==mayorPagado){
				ret = false;
			}else{
				ret = true;
			}
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		return ret;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JTablePanambi getJTblCuotas() {
		if (jTblCuotas == null) {
			String[] columnNames = { "Cod. Cuota", "Descripcion", "Monto", "Vencimiento","Estado","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(1, String.class);
			types.put(2, Double.class);
			Integer[] editable = {  };
			jTblCuotas = new JTablePanambi(columnNames, editable, types);
			jTblCuotas.setRowHeight(20);
			jTblCuotas.getColumnModel().getColumn(0).setPreferredWidth(10);
			jTblCuotas.getColumnModel().getColumn(1).setPreferredWidth(10);
			jTblCuotas.getColumnModel().getColumn(2).setPreferredWidth(100);
			jTblCuotas.getColumnModel().getColumn(3).setPreferredWidth(40);
			jTblCuotas.getColumnModel().getColumn(4).setPreferredWidth(40);
			jTblCuotas.getColumnModel().getColumn(5).setPreferredWidth(0);
			jTblCuotas.getColumnModel().getColumn(5).setMinWidth(0);
			jTblCuotas.getColumnModel().getColumn(5).setMaxWidth(0);
			jTblCuotas.setFocusable(false);
			
		}
		return jTblCuotas;
	}
	private JLabelPanambi getLblpnmbCodPago() {
		if (lblpnmbCodPago == null) {
			lblpnmbCodPago = new JLabelPanambi();
			lblpnmbCodPago.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodPago.setText("Cod. pago : ");
		}
		return lblpnmbCodPago;
	}
	private JTextFieldInteger getJTxtCodPago() {
		if (jTxtCodPago == null) {
			jTxtCodPago = new JTextFieldInteger();
			jTxtCodPago.setToolTipText("Codigo de pago");
			jTxtCodPago.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodPago.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusCodPago();
				}
			});
		}
		return jTxtCodPago;
	}
	
	private void lostFocusCodPago(){
		if(getJTxtCodPago().getValor()==null){
			doLimpiar();
			//getJTxtNroFactura().requestFocus();
		}else{
			try {
				Pago pag = controladorPago.getPago(JFramePanambiMain.session.getConn(), getJTxtCodPago().getValor());
				if(pag.getNroRecibo()==0){
					pag = null;
				}
				if (pag!= null) {
					setValues(pag, null);
				} else {
					pago = null;
					doLimpiar();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	
	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setViewportView(getJTblDetallePago());
			scrollPane_1.setFocusable(false);
		}
		return scrollPane_1;
	}
	private JTablePanambi getJTblDetallePago() {
		if (jTblDetallePago == null) {
			String[] columnNames = { "Item","Forma de Pago","Monto","Cheque","Banco","Boucher","Nota de credito","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(1, String.class);
			types.put(2, Double.class);
			types.put(6, Integer.class);
			Integer[] editable = {  };
			jTblDetallePago = new JTablePanambi(columnNames, editable, types);
			jTblDetallePago.setRowHeight(20);
			jTblDetallePago.getColumnModel().getColumn(0).setPreferredWidth(10);
			jTblDetallePago.getColumnModel().getColumn(1).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(2).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(3).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(4).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(5).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(6).setPreferredWidth(40);
			jTblDetallePago.getColumnModel().getColumn(7).setPreferredWidth(0);
			jTblDetallePago.getColumnModel().getColumn(7).setMinWidth(0);
			jTblDetallePago.getColumnModel().getColumn(7).setMaxWidth(0);
			jTblDetallePago.setFocusable(false);
			
		}
		return jTblDetallePago;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Pago) {
			try{	
				this.pago = (Pago) obj;
					getJTxtNroRecibo().setValor(pago.getNroRecibo());

					getJTxtCodPago().setValor(pago.getCodPago());
					getJTxtFechaPago().setText(pago.getFecha().toString());
					getJTxtMonto().setValue(pago.getMonto());
					String patron = "dd/MM/yyyy HH:mm:ss";
				    SimpleDateFormat formato = new SimpleDateFormat(patron);
					getJTxtFechaPago().setText(formato.format(pago.getFecha()));
					
					if(pago.getEstado().equals("A")){
						getJTxtEstado().setText("Activo");
					}else{
						getJTxtEstado().setText("Anulado");
					}
					if(pago.getVenta()!=null){
						getJTxtNroFactura().setValor(pago.getVenta().getNroComprobante());
					}
					
					if(pago.getVenta()!=null){
						getJTxtCliente().setText(pago.getVenta().getCliente().getNombres()+" "+pago.getVenta().getCliente().getApellidos());
					}
					
					if(pago.getObservaciones()!=null){
						getJTxtObservaciones().setText(pago.getObservaciones().trim());
					} else getJTxtObservaciones().setText("");
					
					List<PlanPago> cuotasPagadas = new ArrayList<PlanPago>();
					cuotasPagadas = controladorPlanPago.getCuotas(JFramePanambiMain.session.getConn(), pago);
					Iterator<PlanPago> iteratorCuotas = cuotasPagadas.listIterator();
					getJTblCuotas().resetData(0);
					Integer cantidadCuotas = null;
					
					if(pago.getVenta()!=null){
						cantidadCuotas = controladorPlanPago.getCantidadCuotas(JFramePanambiMain.session.getConn(), pago.getVenta().getCodVenta());
					}
					
					while (iteratorCuotas.hasNext()) {
						getJTblCuotas().addRow();
						PlanPago pp = (PlanPago) iteratorCuotas.next();
						getJTblCuotas().setValueAt(pp.getCodPlanPago(), getJTblCuotas().getRowCount()-1, 0);
						
						getJTblCuotas().setValueAt(pp.getNroCuota()+" / "+cantidadCuotas,getJTblCuotas().getRowCount()-1, 1);
						getJTblCuotas().setValueAt(pp.getMonto(), getJTblCuotas().getRowCount()-1, 2);
						getJTblCuotas().setValueAt(pp.getVencimiento(), getJTblCuotas().getRowCount()-1, 3);
						
						
						if(pp.getEstado().equals("P")){
							getJTblCuotas().setValueAt("Pagado", getJTblCuotas().getRowCount()-1, 4);
						}else{
							getJTblCuotas().setValueAt("Debe", getJTblCuotas().getRowCount()-1, 4);
						}
						
						getJTblCuotas().setValueAt(pp, getJTblCuotas().getRowCount()-1, 5);
					}
					
					List<DetallePago> detallesPago= new ArrayList<DetallePago>();
					detallesPago = pago.getDetallePago();
					Iterator<DetallePago> iteratorDetallePago = detallesPago.listIterator();
					getJTblDetallePago().resetData(0);
					while (iteratorDetallePago.hasNext()) {
						getJTblDetallePago().addRow();
						DetallePago dp = (DetallePago) iteratorDetallePago.next();
												
						getJTblDetallePago().setValueAt(dp.getNroitem(), getJTblDetallePago().getRowCount()-1, 0);
						getJTblDetallePago().setValueAt(dp.getFormaPago().getNombre(), getJTblDetallePago().getRowCount()-1, 1);
						getJTblDetallePago().setValueAt(dp.getMonto(), getJTblDetallePago().getRowCount()-1, 2);
						
						if(dp.getChequenro()!=null){
							getJTblDetallePago().setValueAt(dp.getChequenro(), getJTblDetallePago().getRowCount()-1, 3);
						}
						
						if(dp.getBanco()!=null){
							getJTblDetallePago().setValueAt(dp.getBanco().getDescripcion(), getJTblDetallePago().getRowCount()-1, 4);
						}
						
						if(dp.getBouchernro()!=null){
							getJTblDetallePago().setValueAt(dp.getBouchernro(), getJTblDetallePago().getRowCount()-1, 5);
						}
						
						if(dp.getNotacredito()!=null){
							getJTblDetallePago().setValueAt(dp.getNotacredito().toString(), getJTblDetallePago().getRowCount()-1, 6);
						}
						
						if(dp.getNotacredito()==0){
							getJTblDetallePago().setValueAt("", getJTblDetallePago().getRowCount()-1, 6);
						}
						
						getJTblDetallePago().setValueAt(dp, getJTblDetallePago().getRowCount()-1, 7);
					}
					
									
			}catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLblpnmbObservaciones() {
		if (lblpnmbObservaciones == null) {
			lblpnmbObservaciones = new JLabelPanambi();
			lblpnmbObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbObservaciones.setText("Observaciones : ");
		}
		return lblpnmbObservaciones;
	}
	private JTextAreaUpper getJTxtObservaciones() {
		if (jTxtObservaciones == null) {
			jTxtObservaciones = new JTextAreaUpper();
			jTxtObservaciones.setEditable(false);
			jTxtObservaciones .setFocusable(false);
		}
		return jTxtObservaciones;
	}
	private JTextFieldUpper getJTxtFechaPago() {
		if (jTxtFechaPago == null) {
			jTxtFechaPago = new JTextFieldUpper();
			jTxtFechaPago.setToolTipText("dd/mm/aaaa hh:mm:ss");
			jTxtFechaPago.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaPago.setEditable(false);
			jTxtFechaPago.setFocusable(false);
		}
		return jTxtFechaPago;
	}
	private JTextField getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextField();
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setToolTipText("Estado del pago");
			jTxtEstado.setEditable(false);
			jTxtEstado.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtEstado.setColumns(10);
			jTxtEstado.setFocusable(false);
		}
		return jTxtEstado;
	}
	private JButtonPanambi getJBtnVerCuotas() {
		if (jBtnVerCuotas == null) {
			jBtnVerCuotas = new JButtonPanambi();
			jBtnVerCuotas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doVerDetalleCuotas();
				}
			});
			jBtnVerCuotas.setMnemonic('V');
			jBtnVerCuotas.setText("Ver todas las cuotas");
		}
		return jBtnVerCuotas;
	}
	
	private void doVerDetalleCuotas(){
		if(pago!=null){
			if(pago.getVenta()!=null){
				DlgDetalleCuotas detalleCuota = new DlgDetalleCuotas(pago.getVenta());
				detalleCuota.centerIt();
				detalleCuota.setVisible(true);
			}else{
				DlgMessage.showMessage(getOwner(), "Debe ingresar una cuota para ver todas las cuotas relacionadas", DlgMessage.ERROR_MESSAGE);
			}
		}else{
			DlgMessage.showMessage(getOwner(), "Debe ingresar una cuota para ver todas las cuotas relacionadas", DlgMessage.ERROR_MESSAGE);
		}
			
	}
}
