package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.CargoEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Usuario;
import py.com.panambi.controller.ControladorCargoEmpleado;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorParametro;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;

public class JIEmpleado extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 507698811340038412L;
	 private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi lblpnmbNombre;
	private JLabelPanambi lblpnmbApellido;
	private JLabelPanambi lblpnmbNroDeDocumento;
	private JLabelPanambi lblpnmbTelefono;
	private JLabelPanambi lblpnmbDireccion;
	private JLabelPanambi lblpnmbEmail;
	private JLabelPanambi lblpnmbSalario;
	private JLabelPanambi lblpnmbFechaIngreso;
	private JLabelPanambi lblpnmbFechaSalida;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbCargo;
	private JFormattedTextFieldPanambi jTxtEmail;
	private JComboBoxPanambi jCmbSucursal;
	private JComboBoxPanambi jCmbCargo;
	private JTextFieldDouble jTxtSalario;
	private JButtonPanambi btnpnmbLimpiar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbSalir;
	private JCheckBox chckbxActivo;
	private JComboBoxPanambi jCmbNroDocumento;
	private Empleado empleado;
	private ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private ControladorParametro controladorParametro = new ControladorParametro();
	private ControladorCargoEmpleado controladorCargoEmpleado = new ControladorCargoEmpleado();
	private List<String> sucursales = new ArrayList<String>();
	private List<String> cargos = new ArrayList<String>();
	private List<String> documentos = new ArrayList<String>();
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JTextFieldDate jTxtFechaIngreso;
	private JTextFieldDate jTxtFechaSalida;
	private JTextFieldUpper jTxtNombre;
	private JTextFieldUpper jTxtApellido;
	private JTextFieldUpper jTxtTelefono;
	private JTextFieldUpper jTxtDireccion;
	private JButtonPanambi btnpnmbAplicarDescuento;
	private JLabelPanambi lblpnmbPresioneFPara;
	
	public JIEmpleado() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Empleados");
		setBounds(100, 100, 735, 453);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(getLblpnmbEmail(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addComponent(getLblpnmbApellido(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(getLblpnmbCodigo(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(getLblpnmbNroDeDocumento(), Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
								.addComponent(getLblpnmbTelefono(), GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
								.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(getJTxtTelefono(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJTxtNombre(), GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
								.addComponent(getJTxtApellido(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLabel_3(), GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(getLabel_2(), Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(getLabel_1(), GroupLayout.PREFERRED_SIZE, 6, GroupLayout.PREFERRED_SIZE))
								.addComponent(getLabel()))
							.addGap(383))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(getChckbxActivo()))
								.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
							.addContainerGap(429, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(getJTxtEmail(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJTxtDireccion(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getBtnpnmbAplicarDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED, 117, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(getLblpnmbCargo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(getJCmbCargo(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(getJCmbSucursal(), GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getLabel_5(), GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
										.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
											.addComponent(getLabel_4(), GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
											.addGap(39))))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getLblpnmbFechaSalida(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getLblpnmbFechaIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getJTxtFechaSalida(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(getJTxtFechaIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)))
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getChckbxActivo())
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbFechaIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtFechaIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbNroDeDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabel()))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLabel_1())
								.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLabel_2())
									.addComponent(getLblpnmbApellido(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtApellido(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(27)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(10)
											.addComponent(getLabel_3()))
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
											.addComponent(getLblpnmbTelefono(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJTxtTelefono(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
							.addGap(7)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtEmail(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbEmail(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbFechaSalida(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtFechaSalida(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabel_4()))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbCargo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbCargo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabel_5()))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getBtnpnmbAplicarDescuento(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
					.addGap(48))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		decorate();
		setShortcuts(this);
		setHelp("empleados");
		doLimpiar();
		getJCmbNroDocumento().requestFocus();
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNroDocumento(), getJTxtNombre(), getJTxtApellido(), getJTxtTelefono(), getJTxtEmail(), getJTxtDireccion(), getChckbxActivo(), getJTxtFechaIngreso(), getJTxtFechaSalida(), getJCmbSucursal(), getJCmbCargo(), getJTxtSalario(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNombre.setText("Nombres : ");
		}
		return lblpnmbNombre;
	}
	private JLabelPanambi getLblpnmbApellido() {
		if (lblpnmbApellido == null) {
			lblpnmbApellido = new JLabelPanambi();
			lblpnmbApellido.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbApellido.setText("Apellidos : ");
		}
		return lblpnmbApellido;
	}
	private JLabelPanambi getLblpnmbNroDeDocumento() {
		if (lblpnmbNroDeDocumento == null) {
			lblpnmbNroDeDocumento = new JLabelPanambi();
			lblpnmbNroDeDocumento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroDeDocumento.setText("Nro. de Documento : ");
		}
		return lblpnmbNroDeDocumento;
	}
	private JLabelPanambi getLblpnmbTelefono() {
		if (lblpnmbTelefono == null) {
			lblpnmbTelefono = new JLabelPanambi();
			lblpnmbTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTelefono.setText("Telefono : ");
		}
		return lblpnmbTelefono;
	}
	private JLabelPanambi getLblpnmbDireccion() {
		if (lblpnmbDireccion == null) {
			lblpnmbDireccion = new JLabelPanambi();
			lblpnmbDireccion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDireccion.setText("Direccion : ");
		}
		return lblpnmbDireccion;
	}
	private JLabelPanambi getLblpnmbEmail() {
		if (lblpnmbEmail == null) {
			lblpnmbEmail = new JLabelPanambi();
			lblpnmbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEmail.setText("Email : ");
		}
		return lblpnmbEmail;
	}
	private JLabelPanambi getLblpnmbSalario() {
		if (lblpnmbSalario == null) {
			lblpnmbSalario = new JLabelPanambi();
			lblpnmbSalario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSalario.setText("Salario : ");
		}
		return lblpnmbSalario;
	}
	private JLabelPanambi getLblpnmbFechaIngreso() {
		if (lblpnmbFechaIngreso == null) {
			lblpnmbFechaIngreso = new JLabelPanambi();
			lblpnmbFechaIngreso.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaIngreso.setText("Fecha Ingreso : ");
		}
		return lblpnmbFechaIngreso;
	}
	private JLabelPanambi getLblpnmbFechaSalida() {
		if (lblpnmbFechaSalida == null) {
			lblpnmbFechaSalida = new JLabelPanambi();
			lblpnmbFechaSalida.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaSalida.setText("Fecha Salida : ");
		}
		return lblpnmbFechaSalida;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbCargo() {
		if (lblpnmbCargo == null) {
			lblpnmbCargo = new JLabelPanambi();
			lblpnmbCargo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCargo.setText("Cargo : ");
		}
		return lblpnmbCargo;
	}
	private JFormattedTextFieldPanambi getJTxtEmail() {
		if (jTxtEmail == null) {
			jTxtEmail = new JFormattedTextFieldPanambi();
		}
		return jTxtEmail;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
			jCmbSucursal.setModel(new ListComboBoxModel<String>(sucursales));
			jCmbSucursal.setSelectedItem(null);
		}
		return jCmbSucursal;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbCargo() {
		if (jCmbCargo == null) {
			jCmbCargo = new JComboBoxPanambi();
			jCmbCargo.setModel(new ListComboBoxModel<String>(cargos));
			jCmbCargo.setSelectedItem(null);
		}
		return jCmbCargo;
	}
	private JTextFieldDouble getJTxtSalario() {
		if (jTxtSalario == null) {
			jTxtSalario = new JTextFieldDouble();
		}
		return jTxtSalario;
	}
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Prepara pantalla para un nuevo registro");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJCmbNroDocumento().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNroDocumento() {
		if (jCmbNroDocumento== null) {
			jCmbNroDocumento = new JComboBoxPanambi();
			jCmbNroDocumento.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNroDocumento();
				}
			});
			jCmbNroDocumento.setEditable(true);
			jCmbNroDocumento.setModel(new ListComboBoxModel<String>(documentos));
			jCmbNroDocumento.setSelectedItem(null);
		}
		return jCmbNroDocumento;
	}
	
	private void lostFocusNroDocumento() {
		try {
			Empleado emp = new Empleado();
			emp = controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String) jCmbNroDocumento.getSelectedItem());
			if (emp != null) {
				setValues(emp, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		
		getJCmbSucursal().setSelectedItem(null);
		getJCmbCargo().setSelectedItem(null);
		getJTxtSalario().setText(null);
		
		empleado = null;
		getJTxtCodigo().setValor(null);
		getJTxtNombre().setText(null);
		getJTxtApellido().setText(null);
		getJTxtDireccion().setText(null);
		getJTxtTelefono().setText(null);
		getJTxtEmail().setText(null);
		getJTxtTelefono().setText(null);
		getJTxtSalario().setValue(0.0);
		getJTxtFechaIngreso().setFecha(new java.util.Date());
		getJTxtFechaSalida().setFecha(null);
		
		if(getJCmbNroDocumento().getSelectedItem()!=null){
			if(getJCmbNroDocumento().getSelectedItem().toString().length()!=0){
				btnpnmbGrabar.setEnabled(true);
			}else btnpnmbGrabar.setEnabled(false);
			btnpnmbEliminar.setEnabled(false);
			chckbxActivo.setSelected(true);
		}else{
			btnpnmbGrabar.setEnabled(false);
			btnpnmbEliminar.setEnabled(false);
			chckbxActivo.setSelected(true);
		}	
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Empleado) {
			this.empleado = (Empleado) obj;
			//doLimpiar();
			getJTxtCodigo().setValor(empleado.getCodEmpleado());
			getJCmbNroDocumento().setSelectedItem(((Empleado) (obj)).getNroDocumento());
			getJTxtNombre().setText(empleado.getNombre());
			getJTxtApellido().setText(empleado.getApellido());
			getJTxtTelefono().setText(empleado.getTelefono());
			getJTxtDireccion().setText(empleado.getDireccion());
			getJTxtEmail().setText(empleado.getEmail());
			getJTxtSalario().setValue(empleado.getSalario());
			DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			getJTxtFechaIngreso().setText(formatoFecha.format(empleado.getFechaIngreso()));
			try{
				if(empleado.getFechaSalido().toString().length()!=10){
					getJTxtFechaSalida().setFecha(null);;
				}else{
					getJTxtFechaSalida().setFecha(empleado.getFechaSalido());
				}
			}catch(Exception e ){
				getJTxtFechaSalida().setFecha(null);
			}

			if(empleado.getEstado().equals("A")){
				getChckbxActivo().setSelected(true);
			}else getChckbxActivo().setSelected(false);
			
			if(empleado.getSucursal()!=null){
				try {
					getJCmbSucursal().setSelectedItem(controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), empleado.getSucursal().getCodSucursal()).getNombre());
				} catch (Exception e) {
					DlgMessage.showMessage(getOwner(), "No se puedo seleccionar sucursal del empleado", DlgMessage.ERROR_MESSAGE);
				}
			}
			
			
			if(empleado.getCargoEmpleado()!=null){
				try {
					getJCmbCargo().setSelectedItem(new CargoEmpleado(JFramePanambiMain.session.getConn(), empleado.getCargoEmpleado().getCodCargoEmpleado()).getDescripcion());
				} catch (Exception e) {
					DlgMessage.showMessage(getOwner(), "No se puedo seleccionar cargo del empleado", DlgMessage.ERROR_MESSAGE);
				}
			}
			
			getBtnpnmbGrabar().setEnabled(true);
			getBtnpnmbEliminar().setEnabled(true);
			getBtnpnmbAplicarDescuento().setEnabled(true);
			
		}
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listasucurs = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		for (Sucursal suc : listasucurs) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void poblarListaEmpleados() throws Exception {
		documentos = new ArrayList<String>();
		List<Empleado> listaemp = controladorEmpleado.getEmpleados(JFramePanambiMain.session.getConn());
		
		for (Empleado emp : listaemp) {
			documentos.add(emp.getNroDocumento());
		}
	}
	
	
	private void poblarListaCargos() throws Exception {
		cargos = new ArrayList<String>();
		List<CargoEmpleado> listacar= controladorCargoEmpleado.getCargosEmpleados(JFramePanambiMain.session.getConn());
		for (CargoEmpleado car : listacar) {
			cargos.add(car.getDescripcion());
		}
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{	
			getJTxtCodigo().setText("");
			getJCmbNroDocumento().setSelectedItem(null);
			getJTxtNombre().setText("");
			getJTxtApellido().setText("");
			getJTxtDireccion().setText("");
			getJTxtTelefono().setText("");
			getJTxtEmail().setText("");
			getJTxtSalario().setValue(0.0);
			
			getJTxtFechaIngreso().setFecha(new java.util.Date());
			getJTxtFechaSalida().setFecha(null);
			getJCmbSucursal().setSelectedItem(null);
			getJCmbCargo().setSelectedItem(null);
			
			getBtnpnmbEliminar().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbAplicarDescuento().setEnabled(false);
			
			poblarListaCargos();
			getJCmbCargo().setModel(new ListComboBoxModel<String>(cargos));
			getJCmbCargo().setSelectedItem(null);
			
			poblarListaEmpleados();
			getJCmbNroDocumento().setModel(new ListComboBoxModel<String>(documentos));
			getJCmbNroDocumento().setSelectedItem(null);
			
			poblarListaSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Busca empleado");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			String[] columnNames = { "C�digo", "Nro Documento", "Nombre","Apellido", "Cargo","Salario","Sucursal","Estado"};
			String[] pks = { "codEmpleado" };
			String sSelect = "SELECT codempleado, nrodocumento, nombres, apellidos, ";
			sSelect += "(SELECT descripcion FROM cargosempleados WHERE empleados.codcargoempleado = cargosempleados.codcargoempleado), salario, ";
			sSelect += "(SELECT nombre FROM sucursales WHERE empleados.codsucursal= sucursales.codsucursal), ";
			sSelect += " (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
			sSelect += "FROM empleados ";
			sSelect += "ORDER BY nombres";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Empleado", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar empleado");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			
			if (empleado == null) {
				
				empleado = new Empleado();
				
				if(cargarValores()){
					controladorEmpleado.insertarEmpleado(JFramePanambiMain.session.getConn(), empleado);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNroDocumento().requestFocus();
				}
				
			} else {
				
				if(cargarValores()){
					String mensaje = "Esta seguro que desea modificar los campos del empleado?.";
					Usuario usuario = new ControladorEmpleado().getUsuarioDeEmpleado(JFramePanambiMain.session.getConn(), empleado.getNroDocumento());
					
					empleado.setUsuario(usuario);
					if(empleado.getEstado().equals("I")&& empleado.getUsuario()!=null){
						mensaje +=  "\nSe inactivar� y quitar� el usuario "+empleado.getUsuario().getUsuario()+" perteneciente al empleado.";
					}
					Integer ret = JOptionPane.showInternalConfirmDialog(this, mensaje , "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					
					if(ret == JOptionPane.YES_OPTION){	
						
						controladorEmpleado.modificarEmpleado(JFramePanambiMain.session.getConn(), empleado);
						
						DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
						doLimpiar();
					}
					getJCmbNroDocumento().requestFocus();
					
				}				
			}
			
		} catch (Exception e) {
			empleado = null;
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private boolean cargarValores(){
		
		try {
			boolean camposObligatorios = true;
			boolean emailCorrecto = true;
			boolean fechaCorrecta = true;
			boolean fechaOrden = true;
			boolean grabar = true;
			
			empleado.setNroDocumento((String) getJCmbNroDocumento().getSelectedItem());
			
			if(getJTxtNombre().getText().toString().length()==0){
				camposObligatorios = false;
				empleado.setNombre("");
			}else{
				empleado.setNombre(getJTxtNombre().getText());
			}
			
			if(getJTxtApellido().getText().toString().length() == 0){
				camposObligatorios = false;
				empleado.setApellido("");
			}else{
				empleado.setApellido(getJTxtApellido().getText());
			}
			
			if(getJTxtTelefono().getText().toString().length()==0){
				camposObligatorios = false;
				empleado.setTelefono("");
			}else empleado.setTelefono(getJTxtTelefono().getText());
			
			empleado.setDireccion(getJTxtDireccion().getText());
			
			if(getJTxtEmail().getText().toString().length()!=0){
				emailCorrecto = validarEmail(getJTxtEmail().getText());
			}else emailCorrecto = true;
			
			empleado.setEmail(getJTxtEmail().getText());
			
			if(getChckbxActivo().isSelected()){	
					empleado.setEstado("A");
				} else {
					empleado.setEstado("I");
			}
			
			if(getJTxtFechaIngreso().getText().toString().length() == 10){
				
				fechaCorrecta = validarFecha(getJTxtFechaIngreso().getText());
				if(fechaCorrecta){
					
					empleado.setFechaIngreso(getJTxtFechaIngreso().getFecha());
					
				}else empleado.setFechaIngreso(null);
			
			}else empleado.setFechaIngreso(null);

			
			if(getJTxtFechaSalida().getText().toString().length()!= 0){
				fechaCorrecta = validarFecha(getJTxtFechaSalida().getText());
				if(fechaCorrecta){
							empleado.setFechaSalido(getJTxtFechaSalida().getFecha());
					
				}else empleado.setFechaSalido(null);
			} else empleado.setFechaSalido(null);
			
		
			
			if (getJCmbSucursal().getSelectedItem()!=null){
				empleado.setSucursal(controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), getJCmbSucursal().getSelectedItem().toString()));
				
			}else {
				camposObligatorios = false;
				empleado.setSucursal(null);
			}
			
			if (getJCmbCargo().getSelectedItem()!=null){
				empleado.setCargoEmpleado(controladorCargoEmpleado.getCargoEmpleado(JFramePanambiMain.session.getConn(), getJCmbCargo().getSelectedItem().toString()));
			}else {
				camposObligatorios = false;
				empleado.setCargoEmpleado(null);
			}
			
			if(getJTxtSalario().toString().length() != 0){
				empleado.setSalario((Double)getJTxtSalario().getValue());
			}else empleado.setSalario(0.0);
			
			fechaOrden = fechaOrden(empleado.getFechaIngreso(),empleado.getFechaSalido());
			
			if(!camposObligatorios){
				DlgMessage.showMessage(getOwner(), "Debe completar los todos los campos obligatorios", DlgMessage.ERROR_MESSAGE);
				grabar = false;
				getJTxtNombre().requestFocus();
				empleado=controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String)getJCmbNroDocumento().getSelectedItem());
				
			}else if(getJTxtEmail().getText().toString().length() != 0 && !emailCorrecto){
				DlgMessage.showMessage(getOwner(), "El email introducido no es el correcto", DlgMessage.ERROR_MESSAGE);
				grabar = false;
				getJTxtEmail().requestFocus();
				empleado=controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String)getJCmbNroDocumento().getSelectedItem());
			}else if(!fechaCorrecta){
				DlgMessage.showMessage(getOwner(), "Las fechas introducidas no son correctas", DlgMessage.ERROR_MESSAGE);
				grabar = false;
				getJTxtFechaIngreso().requestFocus();
				empleado=controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String)getJCmbNroDocumento().getSelectedItem());
			}else if(!fechaOrden){
				DlgMessage.showMessage(getOwner(), "La fecha de salida debe ser mayor o igual a la fecha de entrada", DlgMessage.ERROR_MESSAGE);
				grabar = false;
				getJTxtFechaSalida().requestFocus();
				empleado=controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String)getJCmbNroDocumento().getSelectedItem());
			}else if(empleado.getSalario()==0.0){
				DlgMessage.showMessage(getOwner(), "El salario no puede ser cero.", DlgMessage.ERROR_MESSAGE);
				Double salario = controladorParametro.getSueldoMinimo(JFramePanambiMain.session.getConn());		
				getJTxtSalario().setValue(salario);
				getJTxtSalario().requestFocus();
				grabar = false;
				empleado=controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String)getJCmbNroDocumento().getSelectedItem());
			}
			
			return grabar;

		}catch (Exception e) {
			empleado = null;
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			return false;
		}
	}
	
	private boolean fechaOrden(java.util.Date fechaIng, java.util.Date fechaSal){
		
		try{
			if(fechaIng.equals(fechaSal)){
				return true;
			}else if(fechaIng.before(fechaSal)){
				return true;
			}else return false;
			
		}catch(Exception e){
			return true;
		}
		
	}
		
		    /**
		     * Valida el formato correcto del email.
		     * 
		     * @param email
		     *            
		     * @return true para email valido, en otro caso retorna false
		     */
		    public static boolean validarEmail(String email) {
		 
		        // Compiles the given regular expression into a pattern.
		        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
		 
		        // Match the given input against this pattern
		        Matcher matcher = pattern.matcher(email);
		        return matcher.matches();
		 
		    }
		
		public boolean validarFecha(String fechax) {
			
	        if(fechax.trim().equals("/  /")){
				return true;
			}else{
				try {
		            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		            formatoFecha.parse(fechax);
		        } catch (Exception e) {
		            return false;
		        }
			}
			return true;
	    }
	
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Elimina empleado");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar(){
		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea eliminar de forma permanente los datos del empleado?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(ret == JOptionPane.YES_OPTION){		
			try {
				controladorEmpleado.borrarEmpleado(JFramePanambiMain.session.getConn(), empleado);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbNroDocumento().requestFocus();
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		empleado = null;
		getJCmbNroDocumento().requestFocus();
	}
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
		private void decorate() {
			AutoCompleteDecorator.decorate(getJCmbNroDocumento());
		}
		@Override
		public void setNoValues(Object obj, Integer source) {
			
		}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("*");
		}
		return label;
	}
	private JLabel getLabel_1() {
		if (label_1 == null) {
			label_1 = new JLabel("*");
		}
		return label_1;
	}
	private JLabel getLabel_2() {
		if (label_2 == null) {
			label_2 = new JLabel("*");
		}
		return label_2;
	}
	private JLabel getLabel_3() {
		if (label_3 == null) {
			label_3 = new JLabel("*");
		}
		return label_3;
	}
	private JLabel getLabel_4() {
		if (label_4 == null) {
			label_4 = new JLabel("*");
		}
		return label_4;
	}
	private JLabel getLabel_5() {
		if (label_5 == null) {
			label_5 = new JLabel("*");
		}
		return label_5;
	}
	private JTextFieldDate getJTxtFechaIngreso() {
		if (jTxtFechaIngreso == null) {
			jTxtFechaIngreso = new JTextFieldDate();
			jTxtFechaIngreso.setEditable(false);
		}
		return jTxtFechaIngreso;
	}
	private JTextFieldDate getJTxtFechaSalida() {
		if (jTxtFechaSalida == null) {
			jTxtFechaSalida = new JTextFieldDate();
			jTxtFechaSalida.setText("");
		}
		return jTxtFechaSalida;
	}
	private JTextFieldUpper getJTxtNombre() {
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
		}
		return jTxtNombre;
	}
	private JTextFieldUpper getJTxtApellido() {
		if (jTxtApellido == null) {
			jTxtApellido = new JTextFieldUpper();
		}
		return jTxtApellido;
	}
	private JTextFieldUpper getJTxtTelefono() {
		if (jTxtTelefono == null) {
			jTxtTelefono = new JTextFieldUpper();
		}
		return jTxtTelefono;
	}
	private JTextFieldUpper getJTxtDireccion() {
		if (jTxtDireccion == null) {
			jTxtDireccion = new JTextFieldUpper();
		}
		return jTxtDireccion;
	}
	private JButtonPanambi getBtnpnmbAplicarDescuento() {
		if (btnpnmbAplicarDescuento == null) {
			btnpnmbAplicarDescuento = new JButtonPanambi();
			btnpnmbAplicarDescuento.setMnemonic('A');
			btnpnmbAplicarDescuento.setToolTipText("Aplicar descuento de salario");
			btnpnmbAplicarDescuento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAplicarDescuento();
				}
			});
			btnpnmbAplicarDescuento.setText("Aplicar descuento/Asignacion de salario");
		}
		return btnpnmbAplicarDescuento;
	}
	
	private void doAplicarDescuento(){
		Integer codigo = getJTxtCodigo().getValor();
		if(codigo != null){
			try {
				Empleado empleado = controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), codigo);
				DlgAplicar dlgAplicarDescuento = new DlgAplicar(empleado);
				dlgAplicarDescuento.addWindowListener(new WindowAdapter() {
				});
				dlgAplicarDescuento.setVisible(true);
			}  catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
		}else{
			DlgMessage.showMessage(getOwner(), "No ha seleccionado ningun vendedor", DlgMessage.INFORMATION_MESSAGE);
		}
	
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
