package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.FormaPago;
import py.com.panambi.controller.ControladorFormaPago;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;

public class JIFormaPago extends JInternalFramePanambi implements Browseable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 677744619977875757L;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbFormaPago;
	private JTextFieldInteger jTxtCodFormPago;
	private JComboBoxPanambi jCmbNombre;
	private FormaPago	formaPago;
	private List<String> formasPagos = new ArrayList<String>();
	private ControladorFormaPago controladorFormaPago = new ControladorFormaPago();
	private JCheckBox chckbxActivo;
	private JLabelPanambi labelPanambi;
	private JLabel lblPresioneFPara;
	/**
	 * Create the frame.
	 */
	public JIFormaPago() throws Exception{
		super();
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Formas de pago");
		setBounds(100, 100, 390, 290);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(getLblPresioneFPara())
							.addGap(21))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbFormaPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
								.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtCodFormPago(), GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
									.addComponent(getChckbxActivo())
									.addGap(29)))
							.addGap(13))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(getLblPresioneFPara())
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodFormPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getChckbxActivo()))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbFormaPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(33))
		);
		getJPanelCentral().setLayout(groupLayout);
		decorate();
		setShortcuts(this);
		setHelp("formaspagos");
		doLimpiar();
		getJCmbNombre().requestFocus();
		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNombre(), getChckbxActivo(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Elimina registro");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	

	private void doEliminar() {
		
		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea eliminar de forma \npermanente los datos la forma de pago?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(ret == JOptionPane.YES_OPTION){
			try {
				controladorFormaPago.borrarFormaPago(JFramePanambiMain.session.getConn(), formaPago);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbNombre().requestFocus();
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		formaPago=null;
		jCmbNombre.requestFocus();
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Graba el registro");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (formaPago == null) {
				formaPago = new FormaPago();
				formaPago.setNombre((String) getJCmbNombre().getSelectedItem());
				
				if(chckbxActivo.isSelected()){
					formaPago.setEstado("A");
				}else formaPago.setEstado("I");
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la creaci�n de la forma de pago ?", "", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE );
				if(ret == JOptionPane.YES_OPTION){
					controladorFormaPago.insertarFormaPago(JFramePanambiMain.session.getConn(), formaPago);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
				
			} else {
				formaPago.setNombre((String) getJCmbNombre().getSelectedItem());
				if(chckbxActivo.isSelected()){
					formaPago.setEstado("A");
				}else formaPago.setEstado("I");
				
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea modificar \nlos datos de la forma de pago?", "", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE );
				if(ret == JOptionPane.YES_OPTION){
					controladorFormaPago.modificarFormaPago(JFramePanambiMain.session.getConn(), formaPago);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
				formaPago=null;
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Busca registros");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}

	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = {"C�digo", "Nombre","Estado"};
			String[] pks = { "codFormaPago" };
			String sSelect = "SELECT codformapago, nombre, (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
			sSelect += "FROM formaspagos ";
			sSelect += "ORDER BY nombre";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.FormaPago", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}

	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbFormaPago() {
		if (lblpnmbFormaPago == null) {
			lblpnmbFormaPago = new JLabelPanambi();
			lblpnmbFormaPago.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFormaPago.setText("Forma de Pago : ");
		}
		return lblpnmbFormaPago;
	}
	private JTextFieldInteger getJTxtCodFormPago() {
		if (jTxtCodFormPago == null) {
			jTxtCodFormPago = new JTextFieldInteger();
			jTxtCodFormPago.setEnabled(false);
			jTxtCodFormPago.setEditable(false);
		}
		return jTxtCodFormPago;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
	
		try{
			formaPago = null;
			poblarLista();
			getJCmbNombre().setModel(new ListComboBoxModel<String>(formasPagos));
			getJCmbNombre().setSelectedItem(null);
			getJTxtCodFormPago().setText(null);
			chckbxActivo.setSelected(true);
			btnpnmbEliminar.setEnabled(false);
			btnpnmbGrabar.setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	
	
	private void poblarLista() throws Exception {
		formasPagos= new ArrayList<String>();
		
		List<FormaPago> listaform= controladorFormaPago.getFormasPagos(JFramePanambiMain.session.getConn());
		
		for (FormaPago form : listaform) {
			formasPagos.add(form.getNombre());
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNombre() {
		if (jCmbNombre == null) {
			jCmbNombre = new JComboBoxPanambi();
			jCmbNombre.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNombre();
				}
			});
			jCmbNombre.setEditable(true);
			jCmbNombre.setModel(new ListComboBoxModel<String>(formasPagos));
			jCmbNombre.setSelectedItem(null);
		}
		return jCmbNombre;
	}

	private void lostFocusNombre() {
		try {
			FormaPago form = controladorFormaPago.getFormaPago(JFramePanambiMain.session.getConn(), (String) jCmbNombre.getSelectedItem());
			if (form != null) {
				setValues(form, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	

	private void doNuevoFromLostFocus() {
		formaPago = null;
		getJTxtCodFormPago().setValor(null);
		chckbxActivo.setSelected(true);
		Integer longitud = 0;
		try{
			longitud = getJCmbNombre().getSelectedItem().toString().length();
		}catch(Exception e ){
			longitud = 0;
		}
		if(longitud!=0){
			btnpnmbGrabar.setEnabled(true);
		}else{
			btnpnmbGrabar.setEnabled(false);
		}
		btnpnmbEliminar.setEnabled(false);
		
	}

	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbNombre());
	}
	
	@Override
	public void setValues(Object obj, Integer source) {

		if (obj instanceof FormaPago ) {
			this.formaPago = (FormaPago) obj;
			getJTxtCodFormPago().setValor(formaPago.getCodFormaPago());
			getJCmbNombre().setSelectedItem(((FormaPago) (obj)).getNombre());
			
			if(formaPago.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			}else chckbxActivo.setSelected(false);
			
			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);
		}
	}
	
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
}
