package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Temporada;
import py.com.panambi.controller.ControladorTemporada;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;



public class JITemporada extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1903889233795568573L;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbNombre;
	private JComboBoxPanambi jCmbNombre;
	private JTextFieldInteger jTxtCodigo;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private List<String> temporadas = new ArrayList<String>();
	private Temporada temporada;
	private ControladorTemporada controladorTemporada = new ControladorTemporada();
	private JCheckBox chckbxActivo;
	private JLabel lblPresioneFPara;
	private JLabel label;

	/**
	 * Launch the application.
	 *
	 * Create the frame.
	 */
	public JITemporada() throws Exception{
		super();
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Temporadas");
		setBounds(100, 100, 390, 290);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(getChckbxActivo())))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(141))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(getLblPresioneFPara())
							.addGap(119))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(37)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getChckbxActivo()))
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblPresioneFPara())))
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabel()))
					.addContainerGap(42, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		decorate();
		
		doLimpiar();
		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNombre(), getChckbxActivo(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
		setShortcuts(this);
		setHelp("temporadas");
		getJCmbNombre().requestFocus();
		
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNombre.setText("Nombre : ");
		}
		return lblpnmbNombre;
	}
	
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setEnabled(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	public void doSalir(){
		this.dispose();	
	}
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar temporada");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la eliminaci�n de forma permantente de la temporada? ", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				boolean referencias = controladorTemporada.comprobarReferencia(JFramePanambiMain.session.getConn(), temporada);
				
				if(!referencias){
					controladorTemporada.borrarTemporada(JFramePanambiMain.session.getConn(), temporada);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}else DlgMessage.showMessage(getOwner(),"Imposible eliminar temporada \nLa informacion de la temporada "+temporada.getNombre()+"\nesta siendo utilizada", DlgMessage.ERROR_MESSAGE);
			}
			temporada = null;
			getJCmbNombre().requestFocus();
			
		} catch (SQLException e) {
			if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
				DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
			} else {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar temporada");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (temporada == null) {
				temporada = new Temporada();
				if(cargarValores()){
					controladorTemporada.insertarTemporada(JFramePanambiMain.session.getConn(), temporada);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}else{
					temporada=null;
				}
				
			} else 	{
				
				if(cargarValores()){
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la modificacion de los datos de la temporada ?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(ret == JOptionPane.YES_OPTION){
						controladorTemporada.modificarTemporada(JFramePanambiMain.session.getConn(), temporada);
						DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
						doLimpiar();
						getJCmbNombre().requestFocus();
					}
					
				}
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private boolean cargarValores(){
		boolean resul = true;
		boolean camposCompletos = true;
		
		temporada.setNombre((String) getJCmbNombre().getSelectedItem());
		if(getChckbxActivo().isSelected()){	
			temporada.setEstado("A");
		} else {
			temporada.setEstado("I");
		}

		
		if(temporada.getNombre().equals("")){
			camposCompletos = false;
		}
		
		if(!camposCompletos){
			DlgMessage.showMessage(getOwner(), "Debe completar los todos los campos obligatorios", DlgMessage.ERROR_MESSAGE);
			resul = false;
			getJCmbNombre().requestFocus();
		}
		
		return resul;
	}

	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar temporadas");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nombre","Estado"};
			String[] pks = { "codTemporada" };
			String sSelect = "SELECT codtemporada, nombre, estado ";
			sSelect += "FROM temporadas ";
			sSelect += "ORDER BY nombre";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Temporada", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar(){
		if (btnpnmbLimpiar == null){
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try {
			temporada = null;
			poblarLista();
			getJCmbNombre().setModel(new ListComboBoxModel<String>(temporadas));
			getJCmbNombre().setSelectedItem(null);
			getJTxtCodigo().setValor(null);
			getChckbxActivo().setSelected(true);
			getBtnpnmbEliminar().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(false);
			
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void poblarLista() throws Exception{
		temporadas = new ArrayList<String>();
		List<Temporada> listatemps = controladorTemporada.getTemporadas(JFramePanambiMain.session.getConn());
		for (Temporada temp : listatemps) {
			temporadas.add(temp.getNombre());
		}
		
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Temporada) {
			this.temporada = (Temporada) obj;
			getJTxtCodigo().setValor(temporada.getCodTemporada());
			getJCmbNombre().setSelectedItem(((Temporada) (obj)).getNombre());
			
			if(temporada.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			}else chckbxActivo.setSelected(false);
			
			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);
		}

	}
	
	

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNombre(){
		if (jCmbNombre == null) {
			jCmbNombre = new JComboBoxPanambi();
			jCmbNombre.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNombre();
				}
			});
			jCmbNombre.setEditable(true);
			jCmbNombre.setModel(new ListComboBoxModel<String>(temporadas));
			jCmbNombre.setSelectedItem(null);
		}
		return jCmbNombre;
	}
	
	private void lostFocusNombre() {
		try {
			Temporada temp = controladorTemporada.getTemporada(JFramePanambiMain.session.getConn(), (String) jCmbNombre.getSelectedItem());
			if (temp != null) {
				setValues(temp, null);
				
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		temporada = null;
		
		getJTxtCodigo().setValor(null);		
		chckbxActivo.setSelected(true);
		
		Integer longitud = 0;
		try{
			longitud = getJCmbNombre().getSelectedItem().toString().length();
		}catch (Exception e){
			longitud = 0;
		}
		
		if(longitud !=0){
			
			btnpnmbGrabar.setEnabled(true);
			
		}else {
			btnpnmbGrabar.setEnabled(false);
		}
		
		btnpnmbEliminar.setEnabled(false);		
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {

	}
	
	
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbNombre());
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
		}
		return chckbxActivo;
	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("*");
		}
		return label;
	}
}


