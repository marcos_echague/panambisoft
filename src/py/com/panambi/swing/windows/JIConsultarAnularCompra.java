package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorCompra;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarAnularCompra extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnAnular;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JComboBoxPanambi jCmbProveedor;
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbProveedor;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private ControladorCompra controladorCompra = new ControladorCompra();
	private ControladorProveedor controladorProveedor = new ControladorProveedor();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private List<String> proveedores= new ArrayList<String>();
	private List<String> sucursales= new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private JPopupMenu popupMenu;
	private JMenuItem menuItemDetalles;
	private JMenuItem menuItemAnular;
	private JButtonPanambi jBtnConsultar;
	private JCheckBoxPanambi chckbxpnmbTodos;
	private JCheckBoxPanambi chckbxpnmbPagados;
	private JCheckBoxPanambi chckbxpnmbPendientesDePago;
	private JCheckBoxPanambi chckbxpnmbAnulados;
	private JLabelPanambi lblpnmbSucursal;
	
	
	public JIConsultarAnularCompra() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar/Anular compras");
		setBounds(100,100, 756, 480);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(getChckbxpnmbTodos(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
							.addGap(22)
							.addComponent(getChckbxpnmbPagados(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addGap(38)
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(88)
							.addComponent(getChckbxpnmbAnulados(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
							.addGap(22)
							.addComponent(getChckbxpnmbPendientesDePago(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(51)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 686, GroupLayout.PREFERRED_SIZE)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getChckbxpnmbTodos(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addComponent(getChckbxpnmbPagados(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(3)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(7)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getChckbxpnmbAnulados(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(getChckbxpnmbPendientesDePago(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		agregarMenuPoput();
		setShortcuts(this);
		setHelp("consultarAnularCompra");
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			proveedores = null;
			sucursales = null;
			poblarListaProveedores();
			poblarListaSucursales();
			poblarListaEstados();
			getJCmbProveedor().setModel(new ListComboBoxModel<String>(proveedores));
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
//			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
//			poblarListaEstados();
			getChckbxpnmbTodos().setSelected(true);
			getChckbxpnmbPagados().setSelected(false);
			getChckbxpnmbPendientesDePago().setSelected(false);
			getChckbxpnmbAnulados().setSelected(false);
			getJDPFechaDesde().setDate(null);
			
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			getJCmbProveedor().setSelectedItem("TODOS");
			
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			
			if(getTablePanambi().getRowCount()>0){
				getTablePanambi().resetData(0);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaProveedores() throws Exception {
		proveedores = new ArrayList<String>();
		List<Proveedor> listaProveedor= controladorProveedor.getProveedores(JFramePanambiMain.session.getConn());
		proveedores.add("TODOS");
		for (Proveedor prov : listaProveedor) {
			proveedores.add(prov.getRazonSocial());
		}
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSucursal= controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("ACTIVOS/PAGADOS");
		estados.add("PENDIENTES POR PAGAR");
		estados.add("ANULADOS");
	}
	
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver mas detalles...");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetalles();
				}
			});
		}
		return menuItemDetalles; 
	}
	
	private void doVerDetalles(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
			}else{
				
				Compra compraDet = (Compra) getTablePanambi().getValueAt(fila, 6);
				DlgDetalleCompras dlgDetalleCompra= new DlgDetalleCompras(compraDet);
				dlgDetalleCompra.centerIt();
				dlgDetalleCompra.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JMenuItem getMenuItemAnular(){
		if (menuItemAnular == null) {
			menuItemAnular = new JMenuItem("Anular");
			menuItemAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
		}
		return menuItemAnular; 
	}
	
	private JButtonPanambi getJBtnAnular() {
		if (jBtnAnular == null) {
			jBtnAnular = new JButtonPanambi();
			jBtnAnular.setMnemonic('A');
			jBtnAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
			jBtnAnular.setToolTipText("Anular compra");
			jBtnAnular.setText("Anular");
		}
		return jBtnAnular;
	}
	
	
	private void doAnular(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Imposible anular.\nDebe seleccionar un regitro", DlgMessage.ERROR_MESSAGE);
			}else{
				Compra compNul = (Compra)getTablePanambi().getValueAt(fila, 6);
				
				if(compNul.getEstado().equals("I")){
					DlgMessage.showMessage(getOwner(), "Imposible anular compra.\nEl registro ya se encuentra anulado.", DlgMessage.ERROR_MESSAGE);
				}else{
					java.util.Date fechaActual = new java.util.Date();
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					compNul = (Compra)getTablePanambi().getValueAt(fila, 6);
					java.util.Date fechaCompra = new java.util.Date(compNul.getFecha().getTime());
					if (!sdf.format(fechaActual).equals(sdf.format(fechaCompra))){
						DlgMessage.showMessage(getOwner(), "Imposible anular compra.\nS�lo puede anular una compra el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
					}else{
						compNul = (Compra)getTablePanambi().getValueAt(fila, 6);
						Integer ret = null;
						if(compNul.getEstado().equals("N")){
							ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la anulaci�n de la compra?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						}else if (compNul.getEstado().equals("P")){
							ret = JOptionPane.showInternalConfirmDialog(this, "La compra posee un pago asociado.\nSe anular� el pago relacionado.\nConfirma la anulaci�n de la compra?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						}
						
						if (ret == JOptionPane.YES_OPTION) {
								if(compNul.getComentarios()!=null){
									compNul.setComentarios(compNul.getComentarios().trim()+"\nANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+".");	
								}else{
									compNul.setComentarios("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+".");
								}
								
								String comentariosAnulacion  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n de la compra", "");
								if(comentariosAnulacion!=null){
									
									if(comentariosAnulacion.equals("")){
										DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario de anulaci�n", DlgMessage.WARNING_MESSAGE);
									
									}else{
										compNul.setComentarios(compNul.getComentarios().trim()+" "+comentariosAnulacion);
										
										controladorCompra.anularCompra(JFramePanambiMain.session.getConn(), compNul);
										DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa.", DlgMessage.INFORMATION_MESSAGE);
										doConsultarCompras();
										getJCmbProveedor().requestFocus();
									}
							}
						}
					}
				}
			}
				
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbProveedor().requestFocus();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			//tablePanambi = new JTablePanambi((String[]) null);
			String[] columnNames = {"Nro. de factura","Proveedor", "Fecha de compra", "Total","Sucursal","Estado","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(2, Date.class);
			types.put(3, Double.class);
			Integer[] editable = { };
			tablePanambi= new JTablePanambi(columnNames,editable,types);
						
			tablePanambi.setRowHeight(20);

			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(50);
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(150);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(70);
			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(80);
			
			tablePanambi.getColumnModel().getColumn(6).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(6).setMaxWidth(0);
//			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(2);
//			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(250);
//			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
//			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JComboBoxPanambi getJCmbProveedor() {
		if (jCmbProveedor == null) {
			jCmbProveedor = new JComboBoxPanambi();
		}
		return jCmbProveedor;
	}
	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProveedor.setText("Proveedor : ");
		}
		return lblpnmbProveedor;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Consultar compra");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarCompras();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	@SuppressWarnings("deprecation")
	private void doConsultarCompras(){
		try{
			Proveedor proveedor = null;
			String razonSocial  = "";
			
			Sucursal sucursal = null;
			String nombreSucursal = "";
			boolean pagados = false;
			boolean pendientes = false;
			boolean anulados = false;
			Date fechaDesde ;
			Date fechaHasta ;

			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			//fechaHasta = sumarFechasDias(fechaHasta, 1);
		
			razonSocial = (String)getJCmbProveedor().getSelectedItem();
			
			if(!razonSocial.equals("TODOS")){
				proveedor = controladorProveedor.getProveedor(JFramePanambiMain.session.getConn(), razonSocial);
			}else proveedor = null;
			
			nombreSucursal= (String)getJCmbSucursal().getSelectedItem(); 
			
			if(!nombreSucursal.equals("TODAS")){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}else sucursal = null;
			
				
			if(getChckbxpnmbPagados().isSelected()){
				pagados = true;
			}
			
			if(getChckbxpnmbPendientesDePago().isSelected()){
				pendientes = true;
			}
			
			if(getChckbxpnmbAnulados().isSelected()){
				anulados = true;
			}
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				List<Compra> listaCompra= controladorCompra.getCompras(JFramePanambiMain.session.getConn(), proveedor, sucursal,pagados, pendientes, anulados, fechaDesde, fechaHasta);
				if(listaCompra.size()==0){
					DlgMessage.showMessage(getOwner(), "Ning�n registro encontrado.", DlgMessage.ERROR_MESSAGE );
				}
				Iterator<Compra> iteratorCompra= listaCompra.listIterator();
				getTablePanambi().resetData(0);
				while (iteratorCompra.hasNext()) {
					
					getTablePanambi().addRow();
					
					Compra comp= (Compra) iteratorCompra.next();
					//"Nro. de factura","Proveedor", "Fecha de compra", "Total","Sucursal","Estado","Data"};
					getTablePanambi().setValueAt(comp.getFacturanro(), getTablePanambi().getRowCount()-1, 0);
					getTablePanambi().setValueAt(comp.getProveedor().getRazonSocial(), getTablePanambi().getRowCount()-1, 1);
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					
					
					getTablePanambi().setValueAt(formato.format(comp.getFecha()), getTablePanambi().getRowCount()-1, 2);
					getTablePanambi().setValueAt(comp.getTotal(), getTablePanambi().getRowCount()-1, 3);
					getTablePanambi().setValueAt(comp.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 4);
					if(comp.getEstado().equals("P")){
						getTablePanambi().setValueAt("Pagado", getTablePanambi().getRowCount()-1, 5);
					}else if(comp.getEstado().equals("N")){
						getTablePanambi().setValueAt("Pendiente de pago", getTablePanambi().getRowCount()-1, 5);
					}else if (comp.getEstado().equals("I")){
						getTablePanambi().setValueAt("Anulado", getTablePanambi().getRowCount()-1, 5);
					}
					
					getTablePanambi().setValueAt(comp, getTablePanambi().getRowCount()-1, 6);
				}	
			}
			
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	 @SuppressWarnings("unused")
	private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
		
		
	
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
		
	}
	private JCheckBoxPanambi getChckbxpnmbTodos() {
		if (chckbxpnmbTodos == null) {
			chckbxpnmbTodos = new JCheckBoxPanambi();
			chckbxpnmbTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosEstados();
				}
			});
			chckbxpnmbTodos.setText("Todos");
		}
		return chckbxpnmbTodos;
	}
	
	private void doSelectTodosEstados(){
		getChckbxpnmbTodos().setSelected(true);
		getChckbxpnmbPagados().setSelected(false);
		getChckbxpnmbPendientesDePago().setSelected(false);
		getChckbxpnmbAnulados().setSelected(false);
	}
	private JCheckBoxPanambi getChckbxpnmbPagados() {
		if (chckbxpnmbPagados == null) {
			chckbxpnmbPagados = new JCheckBoxPanambi();
			chckbxpnmbPagados.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbPagados.setText("Pagados");
		}
		return chckbxpnmbPagados;
	}
	
	private void doSelectEstados(){
		getChckbxpnmbTodos().setSelected(false);
		
		if(!getChckbxpnmbPagados().isSelected() && !getChckbxpnmbPendientesDePago().isSelected() && !getChckbxpnmbAnulados().isSelected()){
			getChckbxpnmbTodos().setSelected(true);
		}
		if(getChckbxpnmbPagados().isSelected() && getChckbxpnmbPendientesDePago().isSelected() && getChckbxpnmbAnulados().isSelected()){
			getChckbxpnmbTodos().setSelected(true);
			getChckbxpnmbPagados().setSelected(false);
			getChckbxpnmbPendientesDePago().setSelected(false);
			getChckbxpnmbAnulados().setSelected(false);
		}
		
	}
	
	private JCheckBoxPanambi getChckbxpnmbPendientesDePago() {
		if (chckbxpnmbPendientesDePago == null) {
			chckbxpnmbPendientesDePago = new JCheckBoxPanambi();
			chckbxpnmbPendientesDePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbPendientesDePago.setText("Pendientes de pago");
		}
		return chckbxpnmbPendientesDePago;
	}
	private JCheckBoxPanambi getChckbxpnmbAnulados() {
		if (chckbxpnmbAnulados == null) {
			chckbxpnmbAnulados = new JCheckBoxPanambi();
			chckbxpnmbAnulados.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectEstados();
				}
			});
			chckbxpnmbAnulados.setText("Anulados");
		}
		return chckbxpnmbAnulados;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal : ");
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbSucursal;
	}
}
