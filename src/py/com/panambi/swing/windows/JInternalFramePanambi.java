package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.event.InternalFrameEvent;

import org.apache.log4j.Logger;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.PrivilegeException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JInternalFramePanambi extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2941746739096816546L;
	protected Logger logger = Logger.getLogger(JInternalFramePanambi.class.getName());
	private Container invoker = null;
	private JPanel jPanelTitle;
	private JPanel jPanelCentral;
	private JPanel jPanelSouth;
	private JLabel lblNewLabel;
	private MessageWindow mw;
	private boolean whithNorthBanner;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;

	/**
	 * Create the frame.
	 */
	public JInternalFramePanambi() throws Exception {
		this(true);
	}

	/**
	 * Constructor indicando el uso o no del banner superior
	 * 
	 * @param withNorthBanner
	 */
	public JInternalFramePanambi(boolean withNorthBanner) throws Exception {
		super();
		this.whithNorthBanner = withNorthBanner;
		initialize();
	}

	/**
	 */
	public JInternalFramePanambi(Container owner) {
		super();
		this.invoker = owner;
		try {
			initialize();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private void initialize() throws Exception {
		if (!checkPrivilegies()) {
			throw new PrivilegeException("No cuenta con los permisos necesarios. Comun�quese con su Administrador.");
		}
		setFrameIcon(new ImageIcon(JInternalFramePanambi.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 573, 431);
		if (isWhithNorthBanner()) {
			getContentPane().add(getJPanelTitle(), BorderLayout.NORTH);
		}
		getContentPane().add(getJPanelCentral(), BorderLayout.CENTER);
		getContentPane().add(getJPanelSouth(), BorderLayout.SOUTH);
		this.addInternalFrameListener(new javax.swing.event.InternalFrameAdapter() {

			public void internalFrameClosed(javax.swing.event.InternalFrameEvent e) {
				discardLostFocus(JInternalFramePanambi.this.getTopLevelAncestor());
				if (getInvoker() != null) {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							try {
								if (getInvoker() instanceof JInternalFrame) {
									((JInternalFrame) getInvoker()).setSelected(true);
								}
							} catch (PropertyVetoException e) {
								e.printStackTrace();
							}
						}
					});
				}
				if (mw != null) {
					mw.dispose();
				}

			}

			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				discardLostFocus(JInternalFramePanambi.this.getTopLevelAncestor());
				if (getInvoker() != null) {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							try {
								if (getInvoker() instanceof JInternalFrame) {
									((JInternalFrame) getInvoker()).setSelected(true);
								}
							} catch (PropertyVetoException e) {
								e.printStackTrace();
							}
						}
					});
				}
				if (mw != null) {
					mw.dispose();
				}
			}
		});

	}

	private JPanel getJPanelTitle() {
		if (jPanelTitle == null) {
			jPanelTitle = new JPanel();
			jPanelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPanelTitle.add(getLblNewLabel());
		}
		return jPanelTitle;
	}

	protected JPanel getJPanelCentral() {
		if (jPanelCentral == null) {
			jPanelCentral = new JPanel();
			jPanelCentral.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPanelCentral.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		}
		return jPanelCentral;
	}

	protected JPanel getJPanelSouth() {
		if (jPanelSouth == null) {
			jPanelSouth = new JPanel();
			jPanelSouth.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return jPanelSouth;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel();
			lblNewLabel.setIcon(new ImageIcon(JInternalFramePanambi.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
		}
		return lblNewLabel;
	}

	public void centerIt() {
		if (getParent() != null) {
			Dimension pantalla = getParent().getSize();
			Dimension ventana = getSize();
			setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
		}
	}

	public JFrame getOwner() {
		JFrame ret = null;
		if (getTopLevelAncestor() instanceof JFrame) {
			ret = (JFrame) getTopLevelAncestor();
		}
		if (getTopLevelAncestor() instanceof JInternalFramePanambi) {
			ret = ((JInternalFramePanambi) getTopLevelAncestor()).getOwner();
		}
		return ret;
	}

	protected void setShortcuts(JInternalFramePanambi ji) {
		setShortcutsHere(this);
	}

	private void setShortcutsHere(final JComponent component) {
		component.setFocusTraversalKeysEnabled(false);
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					Container container = ((JPanel) JInternalFramePanambi.this.getContentPane()).getTopLevelAncestor();
					discardLostFocus(JInternalFramePanambi.this);
					if (container instanceof JDialogPanambi) {
						((JDialogPanambi) container).dispose();
					}
					JInternalFramePanambi.this.dispose();
					setActiveAnotherWindow(container);
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocus();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB && e.isShiftDown() && !e.isControlDown()) {
					if (!(e.getComponent() instanceof JTablePanambi)) {
						e.getComponent().transferFocusBackward();
					}
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private void setActiveAnotherWindow(Container container) {
		JFramePanambiMain frame = null;
		if (container instanceof JFramePanambiMain) {
			frame = (JFramePanambiMain) container;
			frame.getDesktopPane();
			Object[] component = frame.getDesktopPane().getComponents();
			for (int i = component.length - 1; i >= 0; i--) {
				Object jComponent = component[i];
				if (jComponent instanceof JInternalFramePanambi) {
					try {
						((JInternalFramePanambi) jComponent).setSelected(true);
					} catch (Exception e) {
					}
				}
			}
		}
	}

	private void discardLostFocus(Container container) {
//		if (container != null) {
//			for (int i = 0; i < container.getComponents().length; i++) {
//				if (container.getComponents()[i] instanceof JComponent) {
//					FocusListener[] listeners = container.getComponents()[i].getFocusListeners();
//					for (int j = 0; j < listeners.length; j++) {
//						FocusListener focusListener = listeners[j];
//						container.getComponents()[i].removeFocusListener(focusListener);
//					}
//				}
//				if (container.getComponents()[i] instanceof Container) {
//					discardLostFocus((Container) container.getComponents()[i]);
//				}
//			}
//		}
	}

	public void setMessageWindow(MessageWindow mw) {
		this.mw = mw;
	}

	/**
	 * @return the invoker
	 */
	public Container getInvoker() {
		return invoker;
	}

	/**
	 * @param invoker
	 *            the invoker to set
	 */
	public void setInvoker(Container invoker) {
		this.invoker = invoker;
	}

	public boolean isWhithNorthBanner() {
		return whithNorthBanner;
	}

	public void setWhithNorthBanner(boolean whithNorthBanner) {
		this.whithNorthBanner = whithNorthBanner;
	}

	protected boolean checkPrivilegies() throws Exception {
		PreparedStatement psprograma = null;
		PreparedStatement psperfil = null;
		boolean ret = true;
		try {
			if (JFramePanambiMain.session != null) {
				List<PerfilUsuario> perfiles = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
				String sSelPrograma = "SELECT codprograma FROM programas p ";
				sSelPrograma += "WHERE p.clase = ? ";
				psprograma = JFramePanambiMain.session.getConn().prepareStatement(sSelPrograma);
				psprograma.setString(1, this.getClass().getName());
				ResultSet rsPrograma = psprograma.executeQuery();
				int codprograma = 0;
				if (rsPrograma.next()) {
					codprograma = rsPrograma.getInt("codprograma");
					ret = false;
				}
				rsPrograma.close();
				if (codprograma != 0) {
					String sSelPerfil = "SELECT codperfil_perfiles FROM perfiles_programas ";
					sSelPerfil += "WHERE codprograma_programas = ? ";
					sSelPerfil += "AND codperfil_perfiles = ? ";
					psperfil = JFramePanambiMain.session.getConn().prepareStatement(sSelPerfil);
					psperfil.setInt(1, codprograma);
					for (PerfilUsuario perfilUsuario : perfiles) {
						if (perfilUsuario.getEstado().equals("A")) {
							psperfil.setInt(2, perfilUsuario.getCodPerfil());
							ResultSet rsPerfil = psperfil.executeQuery();
							if (rsPerfil.next()) {
								ret = true;
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psprograma, psperfil);
		}
		return ret;
	}

	/**
	 * Setea ayuda en linea.
	 * 
	 * @param helpName
	 * @throws Exception
	 */
	protected void setHelp(String helpName) {
		try {
			// Carga el fichero de ayuda
			fichero = new File("./bin/py/com/panambi/help/help_set.hs");
			hsURL = fichero.toURI().toURL();
			helpset = new HelpSet(getClass().getClassLoader(), hsURL);
			hb = helpset.createHelpBroker();
			hb.enableHelpKey(getRootPane(), helpName, helpset);
			// Pone ayuda a item de menu al pulsar F1. mntmIndice es el
			// JMenuitem
		} catch (Exception e) {
			logger.error(getOwner(), e);
			//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
}
