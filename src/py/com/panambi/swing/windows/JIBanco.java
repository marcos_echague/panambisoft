package py.com.panambi.swing.windows;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Banco;
import py.com.panambi.controller.ControladorBanco;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;

public class JIBanco extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2367428734690394666L;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JComboBoxPanambi jCmbDescripcion;
	private JLabelPanambi lblpnmbBanco;
	private JCheckBoxPanambi chckbxpnmbActivo;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi lblpnmbPresioneFPara;
	private Banco banco;
	private List<String> bancos= new ArrayList<String>();
	private ControladorBanco controladorBanco = new ControladorBanco();
	private JLabelPanambi labelPanambi;
	
	public JIBanco() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Bancos");
		setMaximizable(false);
		setBounds(100, 100, 390, 290);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbBanco(), GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(133)
									.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
									.addGap(33)
									.addComponent(getChckbxpnmbActivo(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap(20, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getChckbxpnmbActivo(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbBanco(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
		);
		getJPanelCentral().setLayout(groupLayout);
		decorate();
		setShortcuts(this);
		doLimpiar();
		setHelp("bancos");
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
	
		try{
			banco = null;
			poblarLista();
			getJCmbDescripcion().setModel(new ListComboBoxModel<String>(bancos));
			getJCmbDescripcion().setSelectedItem(null);
			getJTxtCodigo().setText(null);
			getChckbxpnmbActivo().setSelected(true);
			btnpnmbEliminar.setEnabled(false);
			btnpnmbGrabar.setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void poblarLista() throws Exception {
		bancos= new ArrayList<String>();
		
		List<Banco> listaban= controladorBanco.getBancos(JFramePanambiMain.session.getConn());
		
		for (Banco banc: listaban) {
			bancos.add(banc.getDescripcion());
		}
		
	}
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar() {
		
		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la eliminaci�n permanentemente del banco?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(ret == JOptionPane.YES_OPTION){
			try {
				controladorBanco.borrarBanco(JFramePanambiMain.session.getConn(), banco);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbDescripcion().requestFocus();
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		banco=null;
		jCmbDescripcion.requestFocus();
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (banco== null) {
				banco = new Banco();
				banco.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				
				if(chckbxpnmbActivo.isSelected()){
					banco.setEstado("A");
				}else banco.setEstado("I");
				
				controladorBanco.insertarBanco(JFramePanambiMain.session.getConn(), banco);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbDescripcion().requestFocus();
			} else {
				banco.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				if(chckbxpnmbActivo.isSelected()){
					banco.setEstado("A");
				}else banco.setEstado("I");
				
				Integer ret = JOptionPane.showConfirmDialog(this, "Esta seguro que desea modificar \n datos del banco?", "",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.YES_OPTION){
					controladorBanco.modificarBanco(JFramePanambiMain.session.getConn(), banco);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = {"C�digo", "        Descripci�n        ","Estado"};
			String[] pks = { "codBanco" };
			String sSelect = "SELECT codbanco, descripcion, (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
			sSelect += "FROM bancos ";
			sSelect += "ORDER BY descripcion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Banco", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbDescripcion() {
		if (jCmbDescripcion == null) {
			jCmbDescripcion = new JComboBoxPanambi();
			jCmbDescripcion.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusDescripcion();
				}
			});
			jCmbDescripcion.setEditable(true);
			jCmbDescripcion.setModel(new ListComboBoxModel<String>(bancos));
			jCmbDescripcion.setSelectedItem(null);
			
		}
		return jCmbDescripcion;
	}
	
	private void lostFocusDescripcion() {
		try {
			Banco ban= controladorBanco.getBanco(JFramePanambiMain.session.getConn(), (String) jCmbDescripcion.getSelectedItem());
			if (ban != null) {
				setValues(ban, null);
//			}else if (ban == null &&getJCmbDescripcion().getSelectedItem()!=null){
				
			} else {
				doNuevoFromLostFocus();
			}
		}catch(NullPointerException ne){
			getJCmbDescripcion().requestFocus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		banco = null;
		getJTxtCodigo().setValor(null);
		chckbxpnmbActivo.setSelected(true);
		Integer longitud = 0;
		
		try{
			longitud = getJCmbDescripcion().getSelectedItem().toString().length();
		}catch(Exception e){
			longitud = 0;
		}
		if(longitud!=0){
			btnpnmbGrabar.setEnabled(true);
		}else{
			btnpnmbGrabar.setEnabled(false);
		}
		btnpnmbEliminar.setEnabled(false);
		
	}
	
	private JLabelPanambi getLblpnmbBanco() {
		if (lblpnmbBanco == null) {
			lblpnmbBanco = new JLabelPanambi();
			lblpnmbBanco.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbBanco.setText("Banco : ");
		}
		return lblpnmbBanco;
	}
	private JCheckBoxPanambi getChckbxpnmbActivo() {
		if (chckbxpnmbActivo == null) {
			chckbxpnmbActivo = new JCheckBoxPanambi();
			chckbxpnmbActivo.setText("Activo");
		}
		return chckbxpnmbActivo;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setEnabled(false);
			jTxtCodigo.setEditable(false);
			jTxtCodigo.setFocusable(false);
		}
		return jTxtCodigo;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbDescripcion());
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		
		if (obj instanceof Banco ) {
			this.banco = (Banco) obj;
			getJTxtCodigo().setValor(banco.getCodbanco());
			getJCmbDescripcion().setSelectedItem(((Banco) (obj)).getDescripcion());
			
			if(banco.getEstado().equals("A")){
				chckbxpnmbActivo.setSelected(true);
			}else chckbxpnmbActivo.setSelected(false);
			
			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);
		}
		
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
}
