package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Cliente;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldMail;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;


public class JICliente extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -813329916061571646L;
	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodCliente;
	private JLabelPanambi lblpnmbNombres;
	private JLabelPanambi lblpnmbApellidos;
	private JLabelPanambi lblpnmbNroDeDocumento;
	private JTextFieldUpper jTxtNombres;
	private JTextFieldUpper jTxtApellidos;
	private JLabelPanambi lblpnmbEmail;
	private JLabelPanambi lblpnmbFechaDeIngreso;
	private JComboBoxPanambi jCmbNroDocumento;
	private JTextFieldMail jTxtEmail;
	private JLabelPanambi lblpnmbDireccion;
	private JTextFieldUpper jTxtDireccion;
	private JLabelPanambi lblpnmbTelefono;
	private JTextFieldUpper jTxtTelefono;
	private JTextFieldDate jTxtFecha;
	private JCheckBox chckbxActivo;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi labelPanambi_1;
	private JLabelPanambi labelPanambi_2;
	private JLabelPanambi labelPanambi_3;
	
	private Cliente cliente;
	private List<String> clientes = new ArrayList<String>();
	private ControladorCliente controladorCliente = new ControladorCliente();
	private JLabelPanambi labelPanambi_4;
	private JLabelPanambi lblpnmbPresioneFPara;

	/**
	 * Create the frame.
	 */
	
	
	public JICliente() throws Exception {

		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Clientes");
		setBounds(100, 100, 650, 414);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbFechaDeIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(4)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
							.addGap(181)
							.addComponent(getChckbxActivo())
							.addGap(28)
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLabelPanambi_4(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getLabelPanambi_1_1(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
							.addGap(25)
							.addComponent(getLblpnmbEmail(), GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtEmail(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJTxtNombres(), GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getLabelPanambi_2_1(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
							.addGap(25)
							.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtDireccion(), GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJTxtApellidos(), GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getLabelPanambi_3_1(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
							.addGap(25)
							.addComponent(getLblpnmbTelefono(), GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getJTxtTelefono(), GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(getLabelPanambi_4_1(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(getLblpnmbFechaDeIngreso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(11)
							.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(27)
							.addComponent(getChckbxActivo()))
						.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLabelPanambi_4(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getLabelPanambi_1_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbEmail(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtEmail(), GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
					.addGap(16)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNombres(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_2_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(16)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtApellidos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_3_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getLblpnmbTelefono(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getJTxtTelefono(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getLabelPanambi_4_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("clientes");
		doLimpiar();
		decorate();
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNroDocumento(), getJTxtNombres(), getJTxtApellidos(), getChckbxActivo(), getJTxtEmail(), getJTxtDireccion(), getJTxtTelefono(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Elimina cliente");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar() {
		Integer ret = JOptionPane.showInternalConfirmDialog(this,"Esta seguro que desea eliminar de forma permantente al cliente?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(ret == JOptionPane.YES_OPTION){
			try {
				controladorCliente.borrarCliente(JFramePanambiMain.session.getConn(), cliente);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbNroDocumento().requestFocus();
			
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		cliente=null;
		getJCmbNroDocumento().requestFocus();
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Graba cliente");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	
	private void doGrabar(){
		try {
			if (cliente == null) {
				cliente = new Cliente();
				
				if(cargarValores()){
					controladorCliente.insertarCliente(JFramePanambiMain.session.getConn(), cliente);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNroDocumento().requestFocus();
				}else{
					cliente = null;
				}
				
			} else {
				if(cargarValores()){
					Integer ret = JOptionPane.showInternalConfirmDialog(this,"Esta seguro que desea modificar los campos del cliente?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(ret == JOptionPane.YES_OPTION){
						
						//***Comprueba si el cliente posee cuotas pendientes por pagar.
						if(cliente.getEstado().equals("I") && controladorCliente.getCantidadCuotasPendientes(JFramePanambiMain.session.getConn(), cliente)==0){
							DlgMessage.showMessage(getOwner(), "No se puede dar de baja al cliente.\nEl cliente posee cuotas pendientes por pagar.", DlgMessage.WARNING_MESSAGE);
						}else{
							controladorCliente.modificarCliente(JFramePanambiMain.session.getConn(), cliente);
							DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
							doLimpiar();
						}
						
					}
				}
				
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private boolean cargarValores(){
		boolean resul = true;
		boolean emailCorrecto = true;
		boolean camposCompletos = true;
		
		cliente.setNroDocumento((String) getJCmbNroDocumento().getSelectedItem());
		cliente.setNombres(getJTxtNombres().getText());
		cliente.setApellidos(getJTxtApellidos().getText());
		cliente.setTelefono(getJTxtTelefono().getText());
		cliente.setEmail(getJTxtEmail().getText());
		cliente.setDireccion(getJTxtDireccion().getText());
		cliente.setFechaIngreso(getJTxtFecha().getFecha());
		
		if(getChckbxActivo().isSelected()){
			cliente.setEstado("A");
		}else cliente.setEstado("I");

		
		if(cliente.getNroDocumento().equals("")){
			camposCompletos = false;
		}
		if(cliente.getNombres().equals("")){
			camposCompletos = false;
		}
		if(cliente.getApellidos().equals("")){
			camposCompletos = false;
		}
		if(cliente.getTelefono().equals("")){
			camposCompletos = false;
		}
		if(cliente.getDireccion().equals("")){
			camposCompletos = false;
		}
		
		if(cliente.getEmail().length()!=0){
			emailCorrecto = validarEmail(cliente.getEmail());
		}
		
		if(!camposCompletos){
			DlgMessage.showMessage(getOwner(), "Debe completar los todos los campos obligatorios", DlgMessage.ERROR_MESSAGE);
			resul = false;
			getJTxtNombres().requestFocus();
		}else if (!emailCorrecto){
			DlgMessage.showMessage(getOwner(), "El email introducido no es el correcto", DlgMessage.ERROR_MESSAGE);
			resul = false;
			getJTxtEmail().requestFocus();
		}
		
		return resul;
	}
	
	
	/**
     * Valida el formato correcto del email.
     * 
     * @param email
     *            
     * @return true para email valido, en otro caso retorna false
     */
    public static boolean validarEmail(String email) {
 
        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
 
        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
 
    }
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Busca clientes");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(){
		try {
			String[] columnNames = { "C�digo", "Nro. Documento", "Nombres","Apellidos","Estado" };
			String[] pks = { "codCliente" };
			String sSelect = "SELECT codcliente, nrodocumento, nombres, apellidos, estado ";
			sSelect += "FROM clientes ";
			sSelect += "ORDER BY nombres";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Cliente", pks);
			jb.setVisible(true);
			getJCmbNroDocumento().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Prepara pantalla para un nuevo registro");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		
		try {
			cliente = null;
			
			poblarLista();
			getJCmbNroDocumento().setModel(new ListComboBoxModel<String>(clientes));
			getJCmbNroDocumento().setSelectedItem(null);
			
			getJTxtCodCliente().setValor(null);
			getJTxtNombres().setText(null);
			getJTxtApellidos().setText(null);
			getJTxtTelefono().setText(null);
			getJTxtEmail().setText(null);
			getJTxtDireccion().setText(null);
			getJTxtFecha().setFecha(new java.util.Date());
			
			getChckbxActivo().setSelected(true);
			
			getBtnpnmbEliminar().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(false);
			
			getJCmbNroDocumento().requestFocus();
			
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarLista() throws Exception {
		clientes = new ArrayList<String>();
		List<Cliente> listacli = controladorCliente.getClientes(JFramePanambiMain.session.getConn());
		for (Cliente cli : listacli) {
			clientes.add(cli.getNroDocumento());
		}
	}
	
	private JLabelPanambi getLabelPanambi_1() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodCliente() {
		if (jTxtCodCliente == null) {
			jTxtCodCliente = new JTextFieldInteger();
			jTxtCodCliente.setEnabled(false);
			jTxtCodCliente.setEditable(false);
		}
		return jTxtCodCliente;
	}
	private JLabelPanambi getLabelPanambi_2() {
		if (lblpnmbNombres == null) {
			lblpnmbNombres = new JLabelPanambi();
			lblpnmbNombres.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNombres.setText("Nombres : ");
		}
		return lblpnmbNombres;
	}
	private JLabelPanambi getLabelPanambi_3() {
		if (lblpnmbApellidos == null) {
			lblpnmbApellidos = new JLabelPanambi();
			lblpnmbApellidos.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbApellidos.setText("Apellidos : ");
		}
		return lblpnmbApellidos;
	}
	private JLabelPanambi getLabelPanambi_4() {
		if (lblpnmbNroDeDocumento == null) {
			lblpnmbNroDeDocumento = new JLabelPanambi();
			lblpnmbNroDeDocumento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroDeDocumento.setText("Nro de Documento : ");
		}
		return lblpnmbNroDeDocumento;
	}
	private JTextFieldUpper getJTxtNombres() {
		if (jTxtNombres == null) {
			jTxtNombres = new JTextFieldUpper();
		}
		return jTxtNombres;
	}
	private JTextFieldUpper getJTxtApellidos() {
		if (jTxtApellidos == null) {
			jTxtApellidos = new JTextFieldUpper();
		}
		return jTxtApellidos;
	}
	private JLabelPanambi getLblpnmbEmail() {
		if (lblpnmbEmail == null) {
			lblpnmbEmail = new JLabelPanambi();
			lblpnmbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEmail.setText("Email : ");
		}
		return lblpnmbEmail;
	}
	private JLabelPanambi getLblpnmbFechaDeIngreso() {
		if (lblpnmbFechaDeIngreso == null) {
			lblpnmbFechaDeIngreso = new JLabelPanambi();
			lblpnmbFechaDeIngreso.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDeIngreso.setText("Fecha de Ingreso : ");
		}
		return lblpnmbFechaDeIngreso;
	}
	
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNroDocumento() {
		if (jCmbNroDocumento == null) {
			jCmbNroDocumento = new JComboBoxPanambi();
			jCmbNroDocumento.setToolTipText("");
			jCmbNroDocumento.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNroDocumento();
				}
			});
			jCmbNroDocumento.setEditable(true);
			jCmbNroDocumento.setModel(new ListComboBoxModel<String>(clientes));
			jCmbNroDocumento.setSelectedItem(null);
		}
		return jCmbNroDocumento;
	}
	
	private void lostFocusNroDocumento(){
		try {
			Cliente cli= controladorCliente.getCliente(JFramePanambiMain.session.getConn(), (String) jCmbNroDocumento.getSelectedItem());
			if (cli != null) {
				setValues(cli, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		cliente = null;
		getJTxtCodCliente().setValor(null);
		
		getJTxtNombres().setText(null);
		getJTxtApellidos().setText(null);
		getJTxtTelefono().setText(null);
		getJTxtEmail().setText(null);
		getJTxtDireccion().setText(null);
		getJTxtFecha().setFecha(new java.util.Date());
		getChckbxActivo().setEnabled(true);
		
		if(getJCmbNroDocumento().getSelectedItem()==null || getJCmbNroDocumento().getSelectedItem().toString().equals("")){
			getBtnpnmbGrabar().setEnabled(false);
			
		}else getBtnpnmbGrabar().setEnabled(true);
		
		getBtnpnmbEliminar().setEnabled(false);

	}
		
	private JTextFieldMail getJTxtEmail() {
		if (jTxtEmail == null) {
			jTxtEmail = new JTextFieldMail();
		}
		return jTxtEmail;
	}
	private JLabelPanambi getLblpnmbDireccion() {
		if (lblpnmbDireccion == null) {
			lblpnmbDireccion = new JLabelPanambi();
			lblpnmbDireccion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDireccion.setText("Direccion : ");
		}
		return lblpnmbDireccion;
	}
	private JTextFieldUpper getJTxtDireccion() {
		if (jTxtDireccion == null) {
			jTxtDireccion = new JTextFieldUpper();
		}
		return jTxtDireccion;
	}
	private JLabelPanambi getLblpnmbTelefono() {
		if (lblpnmbTelefono == null) {
			lblpnmbTelefono = new JLabelPanambi();
			lblpnmbTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTelefono.setText("Telefono : ");
		}
		return lblpnmbTelefono;
	}
	private JTextFieldUpper getJTxtTelefono() {
		if (jTxtTelefono == null) {
			jTxtTelefono = new JTextFieldUpper();
		}
		return jTxtTelefono;
	}
	private JTextFieldDate getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JTextFieldDate();
			jTxtFecha.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
		}
		return jTxtFecha;
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabelPanambi getLabelPanambi_1_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}
	private JLabelPanambi getLabelPanambi_2_1() {
		if (labelPanambi_2 == null) {
			labelPanambi_2 = new JLabelPanambi();
			labelPanambi_2.setText("*");
		}
		return labelPanambi_2;
	}
	private JLabelPanambi getLabelPanambi_3_1() {
		if (labelPanambi_3 == null) {
			labelPanambi_3 = new JLabelPanambi();
			labelPanambi_3.setText("*");
		}
		return labelPanambi_3;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Cliente) {
			this.cliente = (Cliente) obj;
			getJTxtCodCliente().setValor(cliente.getCodCliente());
			getJCmbNroDocumento().setSelectedItem(((Cliente) (obj)).getNroDocumento());
			getJTxtNombres().setText(cliente.getNombres());
			getJTxtApellidos().setText(cliente.getApellidos());
			getJTxtTelefono().setText(cliente.getTelefono());
			getJTxtEmail().setText(cliente.getEmail());
			getJTxtDireccion().setText(cliente.getDireccion());
			
			DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			String fecha = formatoFecha.format(cliente.getFechaIngreso());
			getJTxtFecha().setValue(fecha);
			
			if(cliente.getEstado().equals("A")){
				getChckbxActivo().setSelected(true);
			}else getChckbxActivo().setSelected(false);
			
			getBtnpnmbGrabar().setEnabled(true);
			getBtnpnmbEliminar().setEnabled(true);
		}
		
	}
	
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbNroDocumento());
	}
	private JLabelPanambi getLabelPanambi_4_1() {
		if (labelPanambi_4 == null) {
			labelPanambi_4 = new JLabelPanambi();
			labelPanambi_4.setText("*");
		}
		return labelPanambi_4;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
