package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Permiso;
import py.com.panambi.controller.ControladorPerfilUsuario;
import py.com.panambi.controller.ControladorPermiso;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;

public class JIPermiso extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7662605079937885146L;
	private JButtonPanambi btnpnmbGuardarCambios;
	private JComboBoxPanambi jCmbPermisos;
	private JLabelPanambi lblpnmbPermiso;
	private ControladorPerfilUsuario controladorPerfiles = new ControladorPerfilUsuario();
	private ControladorPermiso controladorPermiso= new ControladorPermiso();
	private List<String> perfiles;
	private List<JCheckBox> checkboxes = new ArrayList<JCheckBox>();
	private JButtonPanambi btnpnmbSeleccionarTodos;
	private JButtonPanambi btnpnmbQuitarTodos;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JPanel panel_1;

	
	public JIPermiso() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Gestionar permisos");
		setBounds(100, 100, 1078, 505);
		getJPanelSouth().add(getBtnpnmbSeleccionarTodos());
		getJPanelSouth().add(getBtnpnmbQuitarTodos());
		getJPanelSouth().add(getBtnpnmbGuardarCambios());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(getPanel_1(), GroupLayout.DEFAULT_SIZE, 1028, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPermiso(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addGap(7)
							.addComponent(getJCmbPermisos(), GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 649, Short.MAX_VALUE)
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbPermiso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJCmbPermisos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getPanel_1(), GroupLayout.PREFERRED_SIZE, 274, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(20, Short.MAX_VALUE))
		);
		
		getJPanelCentral().setLayout(groupLayout);
		setHelp("permisos");
		doCargarOpciones();
		doCargarListaCheckBoxes();
		doAgregarListenerCheckbox();
		setShortcuts(this);
		doLimpiar();
	}
	
	private void doCargarOpciones(){
		
		try{
			List<Permiso> permisos = controladorPermiso.getPermisos(JFramePanambiMain.session.getConn()); 
			getPanel_1().setLayout(new GridLayout(16,1));
			for (int i = 0 ; i<permisos.size();i++){
				getPanel_1().add(new JCheckBoxPanambi(permisos.get(i).getNombre()));
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		poblarListaPermisos();
		getJCmbPermisos().setModel(new ListComboBoxModel<String>(perfiles));
		getJCmbPermisos().setSelectedItem("ADMINISTRADOR");
		doSelectPerfil();
	}
	
	private void doCargarListaCheckBoxes(){

		for( Component comp : panel_1.getComponents() ) {
			   if( comp instanceof JCheckBox) 
				   checkboxes.add( (JCheckBox)comp );
			}
	}
	
	private void doAgregarListenerCheckbox(){
 
		for ( int i=0;i<checkboxes.size();i++){
			final int j = i;
			checkboxes.get(i).addKeyListener(new java.awt.event.KeyAdapter() {

				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER ) {
						doSelectCheck((JCheckBoxPanambi) checkboxes.get(j));
					}
				}
			});
			
		}
	}
	
	private void doSelectCheck(JCheckBoxPanambi check){
		if(check.isSelected()){
			check.setSelected(false);
		}else{
			check.setSelected(true);
		}
	}
	
	private void poblarListaPermisos(){
		try{
			perfiles = new ArrayList<String>();
			List<PerfilUsuario> listaPerfiles = new ArrayList<PerfilUsuario>();
			listaPerfiles = controladorPerfiles.getPerfiles(JFramePanambiMain.session.getConn());
			
			for(PerfilUsuario pu:listaPerfiles){
				perfiles.add(pu.getNombre());
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JButtonPanambi getBtnpnmbGuardarCambios() {
		if (btnpnmbGuardarCambios == null) {
			btnpnmbGuardarCambios = new JButtonPanambi();
			btnpnmbGuardarCambios.setMnemonic('G');
			btnpnmbGuardarCambios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGuardarCambios();
				}
			});
			btnpnmbGuardarCambios.setText("Guardar cambios");
		}
		return btnpnmbGuardarCambios;
	}
	
	private void doGuardarCambios(){
		try{
			boolean cambiosRegistrados = false;
			PerfilUsuario perf = controladorPerfiles.getPerfil(JFramePanambiMain.session.getConn(), (String)getJCmbPermisos().getSelectedItem());

			
			if(perf.getNombre().toUpperCase().equals("ADMINISTRADOR")){
				DlgMessage.showMessage(getOwner(), "Los permisos del perfil ADMINISTRADOR no pueden ser modificados", DlgMessage.WARNING_MESSAGE);
			}else{
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la modificaci�n de permisos del perfil "+perf.getNombre()+" ?","", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (ret == JOptionPane.YES_OPTION) {
					for(int i = 0 ; i<checkboxes.size();i++){
						Permiso perm = controladorPermiso.getPermiso(JFramePanambiMain.session.getConn(), checkboxes.get(i).getText().toUpperCase().trim());
						if(checkboxes.get(i).isSelected() && !controladorPermiso.permisoAsignado(JFramePanambiMain.session.getConn(), perf, perm)){
							cambiosRegistrados = true;
							controladorPermiso.asignarPermisoAPerfil(JFramePanambiMain.session.getConn(), perf, perm);
						}
						if(!checkboxes.get(i).isSelected() && controladorPermiso.permisoAsignado(JFramePanambiMain.session.getConn(), perf, perm)){
							cambiosRegistrados = true;
							controladorPermiso.sacarPermiso(JFramePanambiMain.session.getConn(), perf, perm);
						}
					}
					
					if(cambiosRegistrados){
						DlgMessage.showMessage(getOwner(), "Permisos cambiados exitosamente.", DlgMessage.INFORMATION_MESSAGE);
					}else{
						DlgMessage.showMessage(getOwner(), "Ning�n permiso modificado.", DlgMessage.INFORMATION_MESSAGE);
					}

				}
							
			}
				
			getJCmbPermisos().requestFocus();
			
		}catch(Exception e ){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JComboBoxPanambi getJCmbPermisos() {
		if (jCmbPermisos == null) {
			jCmbPermisos = new JComboBoxPanambi();
			jCmbPermisos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSelectPerfil();
				}
			});
		}
		return jCmbPermisos;
	}
	
	private void doSelectPerfil(){
		try{
			PerfilUsuario perfilUsuario = controladorPerfiles.getPerfil(JFramePanambiMain.session.getConn(), (String)getJCmbPermisos().getSelectedItem());
			List<Permiso> permisos = controladorPermiso.getPermisos(JFramePanambiMain.session.getConn(), perfilUsuario);
			doLimpiarChecks();
			for (int i = 0;i<permisos.size();i++){
				for(int j=0;j<checkboxes.size();j++){
					if(permisos.get(i).getNombre().toUpperCase().trim().equals(checkboxes.get(j).getText().toUpperCase().trim())){
						checkboxes.get(j).setSelected(true);
						//j=checkboxes.size()+1;
						//JOptionPane.showMessageDialog(null, checkboxes.get(j));
					}
				}
				
			}
			
//			for(int j=0;j<checkboxes.size();j++){
//				JOptionPane.showMessageDialog(null, checkboxes.get(j));
//			}
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doLimpiarChecks(){
		for (int i = 0 ; i<checkboxes.size();i++){
			checkboxes.get(i).setSelected(false);
		}
	}
	
	private JLabelPanambi getLblpnmbPermiso() {
		if (lblpnmbPermiso == null) {
			lblpnmbPermiso = new JLabelPanambi();
			lblpnmbPermiso.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPermiso.setText("Perfil : ");
		}
		return lblpnmbPermiso;
	}
	
	
	private JButtonPanambi getBtnpnmbSeleccionarTodos() {
		if (btnpnmbSeleccionarTodos == null) {
			btnpnmbSeleccionarTodos = new JButtonPanambi();
			btnpnmbSeleccionarTodos.setMnemonic('S');
			btnpnmbSeleccionarTodos.setToolTipText("Seleccionar todos los permisos");
			btnpnmbSeleccionarTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSeleccionarTodos();
				}
			});
			btnpnmbSeleccionarTodos.setText("Seleccionar todos");
		}
		return btnpnmbSeleccionarTodos;
	}
	
	private void doSeleccionarTodos(){
		for (int i = 0; i < checkboxes.size();i++){
			checkboxes.get(i).setSelected(true);
		}
	}
	
	private JButtonPanambi getBtnpnmbQuitarTodos() {
		if (btnpnmbQuitarTodos == null) {
			btnpnmbQuitarTodos = new JButtonPanambi();
			btnpnmbQuitarTodos.setMnemonic('Q');
			btnpnmbQuitarTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doQuitarTodos();
				}
			});
			btnpnmbQuitarTodos.setToolTipText("Quitar todos los permisos");
			btnpnmbQuitarTodos.setText("Quitar todos");
		}
		return btnpnmbQuitarTodos;
	}
	
	private void doQuitarTodos(){
		for (int i = 0;i<checkboxes.size();i++){
			checkboxes.get(i).setSelected(false);
		}
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setLayout(null);
		}
		return panel_1;
	}
}
