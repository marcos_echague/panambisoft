package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import py.com.panambi.bean.DetallePagoSalario;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPagoSalario;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.interfaces.ReciboBrowser;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.prompts.PromptEmpleadoSalario;
import py.com.panambi.utils.NumerosALetras;

public class DlgRealizarPagos extends JDialogPanambi implements Browseable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2398408365286756579L;
	private JPanel panelCentral;
	private JLabelPanambi lblpnmbAPagar;
	private JTextFieldInteger jTxtCodigo;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTablePanambi jTablePagos;
	private List<PagoSalario> pagosSalario = new ArrayList<PagoSalario>();
	private ControladorPagoSalario controladorPagoSalario = new ControladorPagoSalario();
	private JButtonPanambi jBtnAplicar;
	JPopupMenu popupMenu = new JPopupMenu();
	private JLabelPanambi lblpnmbNombre;
	private JLabelPanambi lblpnmbSalarioNominal;
	private JTextFieldDouble jTxtSalario;
	private JTextFieldUpper jTxtNombre;
	private JButtonPanambi btnpnmbAnterior;
	private JButtonPanambi btnpnmbSiguiente;
	private ListIterator<PagoSalario> pagoIterator;
	private List<PagoSalario> pagoSalarioLista;
	private JPanel panelDatosEmpleado;
	private JLabelPanambi lblpnmbNroDocumento;
	private JFormattedTextFieldPanambi jTxtDocumento;
	private JLabelPanambi lblpnmbEstado;
	private JTextFieldUpper jTxtEstado;
	private JLabelPanambi lblpnmbTotalDescuento;
	private JLabel lblNewLabel;
	private JTextFieldDouble jTxtTotalDescuento;
	private JTextFieldDouble jTxtTotalPagar;
	private JLabelPanambi lblpnmbTotalAPagar;
	private String from;
	private ReciboBrowser reciboBrowser;


	/**
	 * Create the dialog.
	 */
	public DlgRealizarPagos(List<PagoSalario> pagoSalario, String from, ReciboBrowser reciboBrowser) {
		this.pagosSalario = pagoSalario;
		this.reciboBrowser = reciboBrowser;
		pagoSalarioLista = pagoSalario;
		pagoIterator = pagoSalario.listIterator();
		this.from = from;
		initialize();
	}

	private void initialize() {
		setResizable(false);
		setModal(true);
		try {
			setTitle("Pago a Empleado");
			setBounds(100, 100, 850, 537);
			getContentPane().add(getPanelCentral(), BorderLayout.CENTER);
			doSiguiente();

		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GroupLayout gl_panelCentral = new GroupLayout(panelCentral);
			gl_panelCentral.setHorizontalGroup(
				gl_panelCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, gl_panelCentral.createSequentialGroup()
						.addGap(22)
						.addGroup(gl_panelCentral.createParallelGroup(Alignment.TRAILING)
							.addComponent(getPanel(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 810, Short.MAX_VALUE)
							.addComponent(getPanelDatosEmpleado(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 810, Short.MAX_VALUE)
							.addGroup(gl_panelCentral.createSequentialGroup()
								.addComponent(getBtnpnmbAnterior(), GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 569, Short.MAX_VALUE)
								.addComponent(getBtnpnmbSiguiente(), GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)))
						.addGap(25))
			);
			gl_panelCentral.setVerticalGroup(
				gl_panelCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelCentral.createSequentialGroup()
						.addGap(18)
						.addGroup(gl_panelCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getBtnpnmbAnterior(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getBtnpnmbSiguiente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addComponent(getPanelDatosEmpleado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 261, GroupLayout.PREFERRED_SIZE)
						.addGap(1))
			);
			panelCentral.setLayout(gl_panelCentral);
			panelCentral.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getBtnpnmbAnterior(), getJTxtCodigo(), getScrollPane(), getJBtnAplicar(), getPanel(), getJtxtNombre(), getJTxtSalario(), getJTxtDocumento(), getJTxtEstado(), getJTablePagos(), getPanelDatosEmpleado(), getLblpnmbAPagar(), getLblpnmbNombre(), getLblpnmbSalarioNominal(), getLblpnmbNroDocumento(), getLblpnmbEstado(), getBtnpnmbSiguiente()}));
		}
		return panelCentral;
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Detalle de pago de salario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(106)
								.addComponent(getJBtnAplicar(), GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 778, Short.MAX_VALUE)))
						.addContainerGap())
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(getJBtnAplicar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	
	private JPanel getPanelDatosEmpleado() {
		if (panelDatosEmpleado == null) {
			panelDatosEmpleado = new JPanel();
			panelDatosEmpleado.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos de empleado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDatosEmpleado.setName("");
			GroupLayout gl_panelDatosEmpleado = new GroupLayout(panelDatosEmpleado);
			gl_panelDatosEmpleado.setHorizontalGroup(
				gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(5)
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(5)
								.addComponent(getJtxtNombre(), GroupLayout.PREFERRED_SIZE, 326, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(5)
								.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getJTxtEstado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGap(73))
					.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
						.addComponent(getLblpnmbTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGap(28)
								.addComponent(getLblNewLabel()))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtTotalDescuento(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
						.addGap(51)
						.addComponent(getLblpnmbTotalAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtTotalPagar(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
						.addGap(314))
			);
			gl_panelDatosEmpleado.setVerticalGroup(
				gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJtxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addPreferredGap(ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
						.addComponent(getLblNewLabel())
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLblpnmbTotalAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotalPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			panelDatosEmpleado.setLayout(gl_panelDatosEmpleado);
		}
		return panelDatosEmpleado;
	}
	
	private JButtonPanambi getJBtnAplicar() {
		if (jBtnAplicar == null) {
			jBtnAplicar = new JButtonPanambi();
			jBtnAplicar.setMnemonic('P');
			jBtnAplicar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagar();
				}
			});
			jBtnAplicar.setText("Pagar");
		}
		return jBtnAplicar;
	}

	private void doPagar() {
		
		try{
			if(getJTablePagos().getValueAt(0, 0).equals(false)){
				getJTablePagos().setValueAt(Boolean.TRUE, 0, 0);
				throw new ValidException("La casilla correspondiente al salario necesariamente debe estar seleccionada.");
			}else if((Double)getJTxtTotalPagar().getValue()<1.0){
				throw new ValidException("El monto a pagar no puede ser negativo.");
			}else{
				PagoSalario pagoARealizar = (PagoSalario) getJTablePagos().getValueAt(getJTablePagos().getRowCount() - 1, 5);
				
				pagoARealizar.setTotalpagado((Double) getJTxtTotalPagar().getValue());
				pagoARealizar.setTotaldescuento((Double) getJTxtTotalDescuento().getValue());
				
				DlgPagos dlgPagos = new DlgPagos(getOwner(), pagoARealizar.getTotalpagado(), 2,3,5,6);
				dlgPagos.centerIt();
				dlgPagos.setVisible(true);
				if (dlgPagos.getDetallePagos() == null) {
					throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
				}

			
				List<DetallePagoSalario> detalles = new ArrayList<DetallePagoSalario>();
				for (int i = 0; i < getJTablePagos().getRowCount(); i++) {
					DetallePagoSalario detalle = (DetallePagoSalario) getJTablePagos().getValueAt(i, 6);
					if((boolean) getJTablePagos().getValueAt(i, 0) == false){
						detalle.setEstado("I");	
					}else{
						detalle.setEstado("A");	
					}
					detalles.add(detalle);	
				}
						
				Pago pago = dlgPagos.getPago();
				
				pagoARealizar.setDetallePagoSalario(detalles);
				pagoARealizar.setEstado("P");
				pagoARealizar.setTotaldescuento((Double) getJTxtTotalDescuento().getValue());
				pagoARealizar.setTotalpagado((Double) getJTxtTotalPagar().getValue());
				
				PagoSalario pagoRealizado = controladorPagoSalario.getRealizarPagoSalario(JFramePanambiMain.session.getConn(), pagoARealizar);
				pago.setPagoSalario(pagoRealizado);
				pago.setNroRecibo(new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn()));
				
				new ControladorPago().registrarPago(JFramePanambiMain.session.getConn(), pago);
				DlgMessage.showMessage(getOwner(), "Pago registrado exitosamente\nSe ha generado un recibo.", DlgMessage.INFORMATION_MESSAGE);
				
				doCargarDatos(pagoRealizado);
				doGenerarRecibo(pago, from);
			}
		} catch (Exception e) {
		
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
		private void doGenerarRecibo(Pago pago, String from) {
			try {
				String titular = "EMPRESA PANAMBI";
				String concepto = "";
				Double montoTotal =0.0;
				String montoLetras = "";
				Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getPagoSalario().getCodempleado());
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato  = new SimpleDateFormat(patron);
				Date fechaDesde = pago.getPagoSalario().getFechaDesde();
				Date fechaHasta = pago.getPagoSalario().getFechaHasta();
				
				if(from.equals("aguinaldo")){
					concepto = "PAGO DE AGUINALDO PERIODO "+formato.format(fechaDesde)+" A "+formato.format(fechaHasta);
				}else{
					concepto = "PAGO DE HONORARIOS PERIODO "+formato.format(fechaDesde)+" A "+formato.format(fechaHasta);
				}
				
				montoTotal = pago.getPagoSalario().getTotalpagado();
				montoLetras = "GUARAN�ES "+NumerosALetras.getLetras(montoTotal);
		
				reciboBrowser.doRecibo(true, titular, concepto, montoLetras, montoTotal, pago.getPagoSalario().getNumeroRecibo(), 0.0);
				
				doCargarDatos(pago.getPagoSalario());
			} catch (Exception e) {
				DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
			}
		}

	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setText("Nombre");
			lblpnmbNombre.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbNombre;
	}
	private JLabelPanambi getLblpnmbSalarioNominal() {
		if (lblpnmbSalarioNominal == null) {
			lblpnmbSalarioNominal = new JLabelPanambi();
			lblpnmbSalarioNominal.setText("Salario Nominal");
			lblpnmbSalarioNominal.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbSalarioNominal;
	}
	private JTextFieldDouble getJTxtSalario() {
		if (jTxtSalario == null) {
			jTxtSalario = new JTextFieldDouble();
			jTxtSalario.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtSalario.setEditable(false);
			jTxtSalario.setDisabledTextColor(Color.RED);
			jTxtSalario.setCantDecimals(0);
		}
		return jTxtSalario;
	}
	private JTextFieldUpper getJtxtNombre(){
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
			jTxtNombre.setEditable(false);
		}
		return jTxtNombre;
	}
	
	private JLabelPanambi getLblpnmbAPagar() {
		if (lblpnmbAPagar == null) {
			lblpnmbAPagar = new JLabelPanambi();
			lblpnmbAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
			lblpnmbAPagar.setText("Codigo");
		}
		return lblpnmbAPagar;
	}

	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			
			jTxtCodigo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodigo.getValor() != null) {
						
						Integer codEmpleado = jTxtCodigo.getValor();
						PagoSalario pagoSeleccionado = new PagoSalario();
						Boolean found = false;
						for (PagoSalario pago : pagoSalarioLista) {
							if(pago.getCodempleado().equals(codEmpleado)){
								pagoSeleccionado = pago;
								doCargarDatos(pagoSeleccionado);
								found = true;
								break;
							}
							
						}
						
						if(found == false){
							doPromptEmpleadoPagoSalario();
							
						}
					}
					super.focusLost(e);
				}
			});
			
			jTxtCodigo.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtCodigo.setDisabledTextColor(Color.RED);
		}
		return jTxtCodigo;
	}
	
	private void doPromptEmpleadoPagoSalario() {
		PromptEmpleadoSalario prompt = new PromptEmpleadoSalario(this, pagoSalarioLista);
		prompt.setVisible(true);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTablePagos());
		}
		return scrollPane;
	}
	
	private JTablePanambi getJTablePagos() {
		if (jTablePagos == null) {
			String[] columnNames = { "Sel.", "Nro. Item", "Concepto", "Porcentaje", "Monto" ,"Data Pago"," Data Detalle"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, Integer.class);
			types.put(5, Integer.class);
			Integer[] editable = { 0,0 };
			
			jTablePagos = new JTablePanambi(columnNames,editable, types);
			
			doAgregarListener(jTablePagos);
			
			if(from.equals("aguinaldo")){
				jTablePagos.getColumnModel().getColumn(0).setPreferredWidth(0);
				jTablePagos.getColumnModel().getColumn(0).setMinWidth(0);
				jTablePagos.getColumnModel().getColumn(0).setMaxWidth(0);
			}else{
				jTablePagos.getColumnModel().getColumn(0).setPreferredWidth(5);
			}
			
			jTablePagos.getColumnModel().getColumn(1).setPreferredWidth(10);
			jTablePagos.getColumnModel().getColumn(2).setPreferredWidth(200);
			jTablePagos.getColumnModel().getColumn(3).setPreferredWidth(10);
			jTablePagos.getColumnModel().getColumn(4).setPreferredWidth(30);
			jTablePagos.getColumnModel().getColumn(5).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(5).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(5).setMaxWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setMaxWidth(0);
			jTablePagos.setFocusable(false);
			
			jTablePagos.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTablePagos.rowAtPoint(e.getPoint());
						jTablePagos.changeSelection(r, 0, true, true);
						int[] selecteds = jTablePagos.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});
			jTablePagos.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					logger.debug("key on table");
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						//doEliminarDetalle();
					}
					super.keyPressed(e);
				}
			});

		}
		return jTablePagos;
	}
	
	/**
     * Se a�ade el listener al model
     */
    private void doAgregarListener(JTablePanambi tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent evento) {
            	doCheckDevolucion();
            }
            
        });
    }
    
    private void doCheckDevolucion(){
    	Integer rowSelect = getJTablePagos().getSelectedRow();
    	
    	if(rowSelect!=-1){
    		Double montoApagar = 0.0;
    		Double montoDescuento = 0.0;
    		
    		for (int i=0; i<getJTablePagos().getRowCount(); i++) {	
    			
    			Boolean estado = (Boolean) getJTablePagos().getValueAt(i, 0);    			
    			DetallePagoSalario detalleParaActualizar = (DetallePagoSalario) getJTablePagos().getValueAt(i, 6);
    			if(estado==true){
    				if(detalleParaActualizar.getCodtipodescuentosalario()==null || detalleParaActualizar.getCodtipodescuentosalario()==0){
    					montoApagar += detalleParaActualizar.getMonto();
    				}else{
    					
    					montoDescuento  += detalleParaActualizar.getMonto();
    				}
    				
    			}
    				
    		}
    		getJTxtTotalPagar().setValue(montoApagar-montoDescuento);
    		getJTxtTotalDescuento().setValue(montoDescuento);
    	
    	}
    	
    }
	
	private JButtonPanambi getBtnpnmbAnterior() {
		if (btnpnmbAnterior == null) {
			btnpnmbAnterior = new JButtonPanambi();
			btnpnmbAnterior.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					doAnterior();
					
				}
			});
			btnpnmbAnterior.setText("Anterior");
			btnpnmbAnterior.setMnemonic('A');
		}
		return btnpnmbAnterior;
	}
	
	private void doAnterior(){
		System.out.println("Antes "+pagoIterator.nextIndex());
		if(pagoIterator.hasPrevious()){			
			PagoSalario pago =	pagoIterator.previous();
			doCargarDatos(pago);
		}
		
		if(pagoIterator.previousIndex() == -1){
			pagoIterator.next();
			
		}
		
	}
	
	
	private JButtonPanambi getBtnpnmbSiguiente() {
		if (btnpnmbSiguiente == null) {
			btnpnmbSiguiente = new JButtonPanambi();
			btnpnmbSiguiente.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					doSiguiente();
					
				}
			});
			btnpnmbSiguiente.setText("Siguiente");
			btnpnmbSiguiente.setMnemonic('S');
		}
		return btnpnmbSiguiente;
	}
	

	private void doSiguiente(){
		
		if(pagoIterator.hasNext()){
			PagoSalario pago =	pagoIterator.next();
			doCargarDatos(pago);
		}
		
		if(pagoIterator.nextIndex() == pagoSalarioLista.size()){
			pagoIterator.previous();
			
		}
		
	}
	
	private void doCargarDatos(PagoSalario pago){
		try{
			
		getJTxtCodigo().setText(pago.getCodempleado().toString());
		Empleado empleado = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getCodempleado());
		getJtxtNombre().setText(pago.getNombre());
		getJTxtSalario().setText(pago.getSalarionominal().toString());
		getJTxtDocumento().setText(empleado.getNroDocumento());
		getJTxtTotalDescuento().setValue(pago.getTotaldescuento());
		getJTxtTotalPagar().setValue(pago.getTotalpagado());
		
		String estado = pago.getEstado();
		if(estado.equals("P")){
			estado = "Pagado";
			getJBtnAplicar().setEnabled(false);
			getJTablePagos().setEnabled(false);
		}else{
			estado= "No pagado";
			getJBtnAplicar().setEnabled(true);
			getJTablePagos().setEnabled(true);
		}
		getJTxtEstado().setText(estado);
		List<DetallePagoSalario> detalles = pago.getDetallePagoSalario();
		getJTablePagos().resetData(0);
		
		for (DetallePagoSalario detalle : detalles) {	
			
			getJTablePagos().addRow();

			if(detalle.getEstado().equals("I")){
				getJTablePagos().setValueAt(Boolean.FALSE, getJTablePagos().getRowCount() - 1, 0);
			}else{
				getJTablePagos().setValueAt(Boolean.TRUE, getJTablePagos().getRowCount() - 1, 0);
			}			
			
			getJTablePagos().setValueAt(detalle.getNroitem(), getJTablePagos().getRowCount() - 1, 1);
			getJTablePagos().setValueAt(detalle.getConcepto(), getJTablePagos().getRowCount() - 1, 2);
			getJTablePagos().setValueAt(detalle.getPorcentaje(), getJTablePagos().getRowCount() - 1, 3);
			getJTablePagos().setValueAt(detalle.getMonto(), getJTablePagos().getRowCount() - 1, 4);
			getJTablePagos().setValueAt(pago, getJTablePagos().getRowCount() - 1, 5);
			getJTablePagos().setValueAt(detalle, getJTablePagos().getRowCount() - 1, 6);
			
		}
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(),e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JLabelPanambi getLblpnmbNroDocumento() {
		if (lblpnmbNroDocumento == null) {
			lblpnmbNroDocumento = new JLabelPanambi();
			lblpnmbNroDocumento.setText("Nro. Documento");
			lblpnmbNroDocumento.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbNroDocumento;
	}
	
	private JFormattedTextFieldPanambi getJTxtDocumento() {
		if (jTxtDocumento == null) {
			jTxtDocumento = new JFormattedTextFieldPanambi();
			jTxtDocumento.setEditable(false);
			jTxtDocumento.setFont(new Font("Dialog", Font.PLAIN, 12));
			jTxtDocumento.setDisabledTextColor(Color.RED);
			
		}
		return jTxtDocumento;
	}
	
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setText("Estado");
			lblpnmbEstado.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbEstado;
	}
	private JTextFieldUpper getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextFieldUpper();
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}

	@Override
	public void setValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		if (obj instanceof PagoSalario) {
			PagoSalario pago = (PagoSalario) obj;
			 doCargarDatos(pago);
		}
		
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLblpnmbTotalDescuento() {
		if (lblpnmbTotalDescuento == null) {
			lblpnmbTotalDescuento = new JLabelPanambi();
			lblpnmbTotalDescuento.setText("Total Descuento");
			lblpnmbTotalDescuento.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbTotalDescuento;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("");
		}
		return lblNewLabel;
	}
	private JTextFieldDouble getJTxtTotalDescuento() {
		if (jTxtTotalDescuento == null) {
			jTxtTotalDescuento = new JTextFieldDouble();
			jTxtTotalDescuento.setEditable(false);
		}
		return jTxtTotalDescuento;
	}
	private JTextFieldDouble getJTxtTotalPagar() {
		if (jTxtTotalPagar == null) {
			jTxtTotalPagar = new JTextFieldDouble();
			jTxtTotalPagar.setEditable(false);
		}
		return jTxtTotalPagar;
	}
	private JLabelPanambi getLblpnmbTotalAPagar() {
		if (lblpnmbTotalAPagar == null) {
			lblpnmbTotalAPagar = new JLabelPanambi();
			lblpnmbTotalAPagar.setText("Total a Pagar");
			lblpnmbTotalAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbTotalAPagar;
	}

}
