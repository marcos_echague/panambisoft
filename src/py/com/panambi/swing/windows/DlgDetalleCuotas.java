package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;

public class DlgDetalleCuotas extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbFecha;
	private JTextFieldInteger jTxtFactura;
	private Venta venta;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JTextFieldDouble jTxtTotal;
	private JLabelPanambi lblpnmbTotalPagado;
	private JTextFieldDouble jTxtTotalPagado;
	private JLabelPanambi lblpnmbDeudaTotal;
	private JTextFieldDouble jTxtDeudaTotal;
	private JLabelPanambi lblpnmbCantidadCuotas;
	private JTextFieldInteger jTxtCantidadCuotas;
	private JLabelPanambi lblpnmbCantidadCuotasPagadas;
	private JTextFieldInteger jTxtCuotasPagadas;
	private JMenuItem menuItemDetalles;
	private JPopupMenu popupMenu;
	private List<PlanPago> cuotas = new ArrayList<PlanPago>();
	private JLabelPanambi lblpnmbPagarNro;
	private JTextFieldInteger jTxtPagare;
	
	public DlgDetalleCuotas(Venta venta) {
		try{
			this.venta = venta;
			cuotas = new ControladorPlanPago().getPlanesPagos(JFramePanambiMain.session.getConn(), venta.getCodVenta());
			initialize();
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	private void initialize() {
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetalleCuotas.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 738, 442);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		agregarMenuPoput();
		getBtnpnmbAceptar().requestFocus();
		
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
//		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	private void cargarValores(){
		try{
			
			getJTxtFactura().setText(venta.getNroComprobante()+"");
			getJTxtTotal().setValue(venta.getTotal());
			getJTxtPagare().setValor(venta.getNroPagare());
			getJTxtCantidadCuotas().setValor(cuotas.size());
			Integer cantidadPagada = 0;
			Double montoPagado = 0.0;
			
			for (int i=0;i<cuotas.size();i++){
				PlanPago cuota =cuotas.get(i); 
				if(cuota.getEstado().equals("P")){
					cantidadPagada+=1;
					montoPagado+=cuota.getMonto();
				}
			}
			getJTxtCuotasPagadas().setValor(cantidadPagada);
			getJTxtTotalPagado().setValue(montoPagado);
			getJTxtDeudaTotal().setValue(venta.getTotal()-montoPagado);
			
			Iterator<PlanPago> iteratorDetalle=  cuotas.listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				PlanPago cuota= (PlanPago) iteratorDetalle.next();
				//"Nro. cuota","Monto de cuota", "Vencimiento", "Estado","Fecha de pago"};
				getTablePanambi().setValueAt(cuota.getNroCuota(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(cuota.getMonto(), getTablePanambi().getRowCount()-1, 1);
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				getTablePanambi().setValueAt(formato.format(cuota.getVencimiento()), getTablePanambi().getRowCount()-1, 2);
				if(cuota.getEstado().equals("D")){
					getTablePanambi().setValueAt("Debe", getTablePanambi().getRowCount()-1, 3);
				}else{
					getTablePanambi().setValueAt("Pagado", getTablePanambi().getRowCount()-1, 3);
					getTablePanambi().setValueAt(formato.format(cuota.getFechaPago()), getTablePanambi().getRowCount()-1, 4);
				}
				
				Pago pago = new ControladorPago().getPago(JFramePanambiMain.session.getConn(), cuota);
				
				if(pago!=null){
					if(pago.getNroRecibo()!=0){
						if(cuota.getEstado().equals("P")){
							getTablePanambi().setValueAt(pago.getNroRecibo(), getTablePanambi().getRowCount()-1, 5);
						}
					}
				}
				
				getTablePanambi().setValueAt(cuota, getTablePanambi().getRowCount()-1, 6);
				
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 722, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLE DE CUOTAS");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 722, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Factura : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Total : ");
		}
		return lblpnmbFecha;
	}
	private JTextFieldInteger getJTxtFactura() {
		if (jTxtFactura == null) {
			jTxtFactura = new JTextFieldInteger();
			jTxtFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFactura.setFocusable(false);
			jTxtFactura.setEditable(false);
		}
		return jTxtFactura;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. cuota","Monto de cuota", "Vencimiento", "Estado","Fecha de pago","Nro. Recibo","Data"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(1, Double.class);
				types.put(5, Integer.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
				
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(60);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(6).setMinWidth(0);
				tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(0);
				tablePanambi.getColumnModel().getColumn(6).setMaxWidth(0);
				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 722, 327);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(66)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtFactura(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
						.addGap(101)
						.addComponent(getLblpnmbPagarNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtPagare(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(66)
						.addComponent(getLblpnmbCantidadCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtCantidadCuotas(), GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
						.addGap(126)
						.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(29)
						.addComponent(getLblpnmbCantidadCuotasPagadas(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtCuotasPagadas(), GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
						.addGap(126)
						.addComponent(getLblpnmbTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtTotalPagado(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(391)
						.addComponent(getLblpnmbDeudaTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(getJTxtDeudaTotal(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(24)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 671, GroupLayout.PREFERRED_SIZE))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(23)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbPagarNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtPagare(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCantidadCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCantidadCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(9)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCantidadCuotasPagadas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCuotasPagadas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(13)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbDeudaTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtDeudaTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(23)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
			);
			panel_1.setLayout(gl_panel_1);
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 366, 722, 38);
			panel_2.add(getBtnpnmbAceptar());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setFocusable(false);
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbTotalPagado() {
		if (lblpnmbTotalPagado == null) {
			lblpnmbTotalPagado = new JLabelPanambi();
			lblpnmbTotalPagado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotalPagado.setText("Total pagado : ");
		}
		return lblpnmbTotalPagado;
	}
	private JTextFieldDouble getJTxtTotalPagado() {
		if (jTxtTotalPagado == null) {
			jTxtTotalPagado = new JTextFieldDouble();
			jTxtTotalPagado.setFocusable(false);
			jTxtTotalPagado.setEditable(false);
			jTxtTotalPagado.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtTotalPagado;
	}
	private JLabelPanambi getLblpnmbDeudaTotal() {
		if (lblpnmbDeudaTotal == null) {
			lblpnmbDeudaTotal = new JLabelPanambi();
			lblpnmbDeudaTotal.setText("Deuda total  : ");
			lblpnmbDeudaTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbDeudaTotal;
	}
	private JTextFieldDouble getJTxtDeudaTotal() {
		if (jTxtDeudaTotal == null) {
			jTxtDeudaTotal = new JTextFieldDouble();
			jTxtDeudaTotal.setFocusable(false);
			jTxtDeudaTotal.setEditable(false);
			jTxtDeudaTotal.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtDeudaTotal;
	}
	private JLabelPanambi getLblpnmbCantidadCuotas() {
		if (lblpnmbCantidadCuotas == null) {
			lblpnmbCantidadCuotas = new JLabelPanambi();
			lblpnmbCantidadCuotas.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCantidadCuotas.setText("Cantidad cuotas : ");
		}
		return lblpnmbCantidadCuotas;
	}
	private JTextFieldInteger getJTxtCantidadCuotas() {
		if (jTxtCantidadCuotas == null) {
			jTxtCantidadCuotas = new JTextFieldInteger();
			jTxtCantidadCuotas.setFocusable(false);
			jTxtCantidadCuotas.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCantidadCuotas.setEditable(false);
		}
		return jTxtCantidadCuotas;
	}
	private JLabelPanambi getLblpnmbCantidadCuotasPagadas() {
		if (lblpnmbCantidadCuotasPagadas == null) {
			lblpnmbCantidadCuotasPagadas = new JLabelPanambi();
			lblpnmbCantidadCuotasPagadas.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCantidadCuotasPagadas.setText("Cantidad cuotas pagadas  : ");
		}
		return lblpnmbCantidadCuotasPagadas;
	}
	private JTextFieldInteger getJTxtCuotasPagadas() {
		if (jTxtCuotasPagadas == null) {
			jTxtCuotasPagadas = new JTextFieldInteger();
			jTxtCuotasPagadas.setFocusable(false);
			jTxtCuotasPagadas.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCuotasPagadas.setEditable(false);
		}
		return jTxtCuotasPagadas;
	}
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver detalles del pago");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetallesPago();
				}
			});
		}
		return menuItemDetalles; 
	}
	
	private void doVerDetallesPago(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar una cuota para ver el detalle de sus pago.", DlgMessage.ERROR_MESSAGE);
			}else{
				PlanPago cuotaSeleccionada = (PlanPago) getTablePanambi().getValueAt(fila, 6);
				if(cuotaSeleccionada.getEstado().equals("D")){
					DlgMessage.showMessage(getOwner(),"La cuota se encuentra en deuda, no posee pago alguno.", DlgMessage.ERROR_MESSAGE);
				}else{
					Pago pago = new ControladorPago().getUltimoPagoCuota(JFramePanambiMain.session.getConn(),((PlanPago)getTablePanambi().getValueAt(fila, 6)).getCodPlanPago());
					if(pago==null){
						DlgMessage.showMessage(getOwner(),"No se han encontrado pagos para la cuota seleccionada.", DlgMessage.ERROR_MESSAGE);
					}else{
						DlgDetallePagos dlgDetallePagos = new DlgDetallePagos(pago);
						dlgDetallePagos.centerIt();
						dlgDetallePagos.setVisible(true);
					}
				}
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	private JLabelPanambi getLblpnmbPagarNro() {
		if (lblpnmbPagarNro == null) {
			lblpnmbPagarNro = new JLabelPanambi();
			lblpnmbPagarNro.setText("Pagar\u00E9 nro : ");
			lblpnmbPagarNro.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbPagarNro;
	}
	private JTextFieldInteger getJTxtPagare() {
		if (jTxtPagare == null) {
			jTxtPagare = new JTextFieldInteger();
			jTxtPagare.setFocusable(false);
			jTxtPagare.setEditable(false);
			jTxtPagare.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtPagare;
	}
}
