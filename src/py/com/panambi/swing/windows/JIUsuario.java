package py.com.panambi.swing.windows;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.MD5;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;
import java.awt.Color;
//Para manejo de JavaMail

public class JIUsuario extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4370393386252130240L;
	// private Container invoker = null;
	private JButtonPanambi btnpnmbNuevo;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbSalir;
	private JTextFieldUpper txtNombre;
	private JLabelPanambi lblNombre;
	private JLabelPanambi lblUsuario;
	private Usuario usuario;
	private ControladorUsuario controladorUsuario = new ControladorUsuario();
	private JCheckBox chckbxEstado;
	private JLabel lblCodigoDeUsuario;
	private JTextFieldInteger txtCodUsuario;
	private JCheckBox chckbxReestablecer;
	private JScrollPane scrollPane;
	private JTablePanambi table;
	private JButtonPanambi btnAgregarPerfil;
	private JButtonPanambi btnQuitarPerfil;
	private JLabel lblCorreo;
	private JComboBoxPanambi JCmbUsuario;
	private List<String> usuarios = new ArrayList<String>();
	private JFormattedTextFieldPanambi txtCorreo;
	private JLabelPanambi labelPanambi;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	private JLabelPanambi labelPanambi_1;
	private JLabelPanambi labelPanambi_2;
	private JLabelPanambi lblpnmbPresioneFPara;

	/**
	 * Create the frame.
	 */
	public JIUsuario() throws Exception {
		super();
		initialize();
	}

	private void initialize() {
		getJPanelCentral().setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getJPanelCentral().setToolTipText("Prepara la pantalla para un nuevo registro");
		setMaximizable(false);
		setTitle("Usuario");
		setBounds(100, 100, 637, 489);
		getJPanelSouth().add(getBtnpnmbNuevo());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 597, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addGroup(groupLayout.createSequentialGroup()
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
													.addComponent(getLblUsuario(), 0, 0, Short.MAX_VALUE)
													.addComponent(getLblCodigoDeUsuario(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
												.addGap(31))
											.addGroup(groupLayout.createSequentialGroup()
												.addComponent(getLblNombre(), 0, 0, Short.MAX_VALUE)
												.addPreferredGap(ComponentPlacement.RELATED)))
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(getLblCorreo())
											.addPreferredGap(ComponentPlacement.RELATED)))
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getTxtCodUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(getTxtCorreo(), GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED))
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(getTxtNombre(), GroupLayout.PREFERRED_SIZE, 263, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
									.addPreferredGap(ComponentPlacement.RELATED, 22, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getBtnAgregarPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getBtnQuitarPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(getChckbxEstado())
										.addComponent(getChckbxReestablecer()))
									.addGap(14))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
									.addContainerGap())))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblCodigoDeUsuario())
								.addComponent(getTxtCodUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(getLblUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getTxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(getChckbxEstado())
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getChckbxReestablecer())))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblCorreo())
						.addComponent(getTxtCorreo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getBtnQuitarPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnAgregarPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(19, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		 try {
	        	// Carga el fichero de ayuda 
	            fichero = new File("./bin/py/com/panambi/help/help_set.hs"); 
	        	hsURL = fichero.toURI().toURL();
				helpset = new HelpSet(getClass().getClassLoader(), hsURL);
	    	    hb = helpset.createHelpBroker();
	    	    // Pone ayuda a item de menu al pulsar F1. mntmIndice es el JMenuitem  
	    	    hb.enableHelpKey(getRootPane(),"usuarios",helpset);
			} catch (Exception e) {
				//logger.error(getOwner(),e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}  
		decorate();
		setShortcuts(this);
		doLimpiar();
		getJCmbUsuario().requestFocus();
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setToolTipText("Cierra la pantalla.");
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}

	private void doSalir() {
		this.dispose();
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setToolTipText("Graba el registro actual.");
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}

	private void doGrabar() {
		try {

			if(getJCmbUsuario().getSelectedItem().toString().length()==0 ||
					getTxtNombre().getText().length()==0 ||
					getTxtCorreo().getText().length()==0){
					DlgMessage.showMessage(getOwner(), "Debe completar los campos obligatorios", DlgMessage.WARNING_MESSAGE);
					getJCmbUsuario().requestFocus();
			}else{
				
				String genericPass = "";
				boolean insert = false;
				
				genericPass = passwordGenerator();
				MD5 md5 = new MD5();
				md5.init(); 
				md5.update(genericPass.toCharArray(),
				genericPass.toCharArray().length); 
				md5.md5final(); 
				String encpasswd = new String(md5.toHexString());
				
				if (usuario == null) {
					usuario = new Usuario();
					insert = true;
					
				}else if(usuario.getCodUsuario() == null){
					insert = true;
				}
				
				usuario.setUsuario((String) getJCmbUsuario().getSelectedItem());
				usuario.setNombreCompleto(getTxtNombre().getText().trim());
				usuario.setCorreo(getTxtCorreo().getText().trim());
				usuario.setPasswd(encpasswd);

				List<PerfilUsuario> perfiles = new ArrayList<PerfilUsuario>();
				for (int i = 0; i < getTable().getRowCount(); i++) {
					PerfilUsuario perfil = (PerfilUsuario) getTable().getValueAt(i, 3);
					perfiles.add(perfil);
				}
				
				usuario.setPerfilesUsuario(perfiles);
				
				if (getChckbxEstado().isSelected()) {
					usuario.setEstado("A");
				} else {
					usuario.setEstado("I");
				}
				
				if (insert) {
					controladorUsuario.insertarUsuario(JFramePanambiMain.session.getConn(), usuario);
					DlgMessage.showMessage(getOwner(), "Se enviara un email a su direccion correo electronico", DlgMessage.INFORMATION_MESSAGE);
					if(enviarEmail(genericPass,usuario.getNombreCompleto(), usuario.getCorreo()) == true){
						DlgMessage.showMessage(getOwner(), "Correo enviado exitosamente.", DlgMessage.INFORMATION_MESSAGE);
					}else{
						DlgMessage.showMessage(getOwner(), "El correo no pudo ser enviado, verifique su conexion.", DlgMessage.ERROR_MESSAGE);
						
					}
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa, la contrase�a generada es: "+genericPass, DlgMessage.INFORMATION_MESSAGE);
				}else {
					int returnValue  = JOptionPane.showConfirmDialog(this,"Confirma la operacion actualizacion de los datos del usuario "+usuario.getUsuario(), 
							null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
					if (returnValue == JOptionPane.YES_OPTION){
						controladorUsuario.actualizarUsuario(JFramePanambiMain.session.getConn(), usuario, getChckbxReestablecer().isSelected());
						boolean reestablecer = getChckbxReestablecer().isSelected();
						if(reestablecer==true){
							if(enviarEmail(genericPass,usuario.getNombreCompleto(), usuario.getCorreo()) == true){
								DlgMessage.showMessage(getOwner(), "Correo enviado exitosamente.", DlgMessage.INFORMATION_MESSAGE);
							}else{
								DlgMessage.showMessage(getOwner(), "El correo no pudo ser enviado.", DlgMessage.ERROR_MESSAGE);
								
							}
						}
						DlgMessage.showMessage(getOwner(), "Operacion Exitosa.", DlgMessage.INFORMATION_MESSAGE);
					} else getTxtNombre().requestFocus();
					
				}
				
				doLimpiar();
				getJCmbUsuario().requestFocus();
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setToolTipText("Busca registros.");
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nombre Completo", "Usuario", "Estado", "Correo" };
			String[] pks = { "codUsuario" };
			String sSelect = "SELECT codusuario, nombrecompleto, usuario, estado, correo ";
			sSelect += "FROM usuarios ";
			sSelect += "ORDER BY usuario";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Usuario", pks);
			jb.setVisible(true);
			getJCmbUsuario().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private Component getBtnpnmbNuevo() {
		if (btnpnmbNuevo == null) {
			btnpnmbNuevo = new JButtonPanambi();
			btnpnmbNuevo.setToolTipText("Prepara la pantalla para un nuevo registro.");
			btnpnmbNuevo.setMnemonic('L');
			btnpnmbNuevo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbUsuario().requestFocus();
				}
			});
			btnpnmbNuevo.setText("Limpiar");
		}
		return btnpnmbNuevo;
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar() {
		try {
			usuario = null;
			poblarLista();
			getJCmbUsuario().setModel(new ListComboBoxModel<String>(usuarios));
			getJCmbUsuario().setSelectedItem(null);
			getTable().resetData(0);
			getTxtCodUsuario().setValor(null);
			getTxtNombre().setText(null);
			getTxtCorreo().setText(null);
			getChckbxEstado().setSelected(true);
			getChckbxReestablecer().setSelected(false);
			getChckbxReestablecer().setEnabled(false);
			//
			getBtnpnmbGrabar().setEnabled(false);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void poblarLista() throws Exception {
		usuarios = new ArrayList<String>();
		List<Usuario> listausuarios = controladorUsuario.getUsuarios(JFramePanambiMain.session.getConn());
		for (Usuario usu : listausuarios) {
			usuarios.add(usu.getUsuario());
		}
	}
	
	private JLabelPanambi getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabelPanambi("Nombre Completo");
			lblNombre.setBounds(12, 37, 103, 16);
		}
		return lblNombre;
	}

	private JTextFieldUpper getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextFieldUpper();
			txtNombre.setBounds(138, 34, 139, 22);
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}

	private JLabelPanambi getLblUsuario() {
		if (lblUsuario == null) {
			lblUsuario = new JLabelPanambi("Usuario");
			lblUsuario.setBounds(12, 66, 56, 16);
		}
		return lblUsuario;
	}
	
	@Override
	public void setNoValues(Object obj, Integer source) {

	}
	
	private boolean isPerfilDuplicado(PerfilUsuario perfil) {
		boolean ret = false;
		for (int i = 0; i < getTable().getRowCount(); i++) {
			PerfilUsuario perfilTable = (PerfilUsuario) getTable().getValueAt(i, 3);
			if (perfilTable.getCodPerfil().equals(perfil.getCodPerfil())) {
				ret = true;
				break;
			}
		}
		return ret;
	}

	private static String passwordGenerator() {
		String NUMEROS = "0123456789";
		String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
		String key = NUMEROS + MAYUSCULAS + MINUSCULAS;
		String pswd = "";
		int length = 8;
		for (int i = 0; i < length; i++) {
			pswd += (key.charAt((int) (Math.random() * key.length())));
		}
		return pswd;
	}

	private JCheckBox getChckbxEstado() {
		if (chckbxEstado == null) {
			chckbxEstado = new JCheckBox("Activo");
		}
		return chckbxEstado;
	}

	private JLabel getLblCodigoDeUsuario() {
		if (lblCodigoDeUsuario == null) {
			lblCodigoDeUsuario = new JLabel("Codigo de usuario");
			lblCodigoDeUsuario.setFont(new Font("Arial", Font.PLAIN, 11));
		}
		return lblCodigoDeUsuario;
	}

	private JTextFieldInteger getTxtCodUsuario() {
		if (txtCodUsuario == null) {
			txtCodUsuario = new JTextFieldInteger();
			txtCodUsuario.setEditable(false);
			txtCodUsuario.setEnabled(false);
		}
		return txtCodUsuario;
	}

	private JCheckBox getChckbxReestablecer() {
		if (chckbxReestablecer == null) {
			chckbxReestablecer = new JCheckBox("Reestablecer contrase\u00F1a");
			chckbxReestablecer.setEnabled(false);
		}
		return chckbxReestablecer;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Perfiles Asignados", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTablePanambi getTable() {
		if (table == null) {
			String[] columnNames = { "Seleccionar", "Codigo", "Perfil", "Bean" };
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, Integer.class);
			Integer[] editable = { 0 };
			table = new JTablePanambi(columnNames, editable, types);
			table.getColumnModel().getColumn(0).setPreferredWidth(20);
			table.getColumnModel().getColumn(1).setPreferredWidth(20);
			table.getColumnModel().getColumn(2).setPreferredWidth(65);
			table.getColumnModel().getColumn(3).setPreferredWidth(0);
			table.getColumnModel().getColumn(3).setMinWidth(0);
			table.getColumnModel().getColumn(3).setMaxWidth(0);
		}
		return table;
	}

	private JButtonPanambi getBtnAgregarPerfil() {
		if (btnAgregarPerfil == null) {
			btnAgregarPerfil = new JButtonPanambi();
			btnAgregarPerfil.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarPerfil();
				}
			});
			btnAgregarPerfil.setText("Agregar Perfil");
		}
		return btnAgregarPerfil;
	}

	private void doAgregarPerfil() {
		String[] columnNames = { "C�digo", "Perfil" };
		String[] pks = { "codPerfil" };
		String sSelect = "SELECT codperfil, nombre ";
		sSelect += "FROM perfiles ";
		sSelect += "ORDER BY nombre";
		JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.PerfilUsuario", pks);
		jb.setVisible(true);
	}

	private JButtonPanambi getBtnQuitarPerfil() {
		if (btnQuitarPerfil == null) {
			btnQuitarPerfil = new JButtonPanambi();
			btnQuitarPerfil.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doQuitarPerfil();
				}
			});
			btnQuitarPerfil.setText("Quitar Perfil");
		}
		return btnQuitarPerfil;
	}

	private void doQuitarPerfil() {
		try {
			ArrayList<Integer> toRemove = new ArrayList<Integer>();
			for (int i = 0; i < getTable().getRowCount(); i++) {
				Boolean selected = (Boolean) getTable().getValueAt(i, 0);
				if (selected) {
					toRemove.add(i);
				}
			}
			if (toRemove.isEmpty()) {
				throw new ValidException("Debe seleccionar el perfil que desea quitar");
			}
			Integer[] toRemoveInt = new Integer[toRemove.size()];
			toRemove.toArray(toRemoveInt);
			getTable().removeRows(toRemoveInt);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private boolean enviarEmail(String pass, String nombre , String correo) {
		try{
		// Propiedades
		Properties props = new Properties();
		props.setProperty("mail.smtp.host","smtp.gmail.com");
		props.setProperty("mail.smtp.starttls.enable", "true");
		props.setProperty("mail.smtp.port", "587");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.user", "cruzmil7");  
	        
		// Preparamos la sesion
		Session session = Session.getDefaultInstance(props);
		//Recoger los datos
		String str_De= "cruzmil7";
		String str_PwRemitente = "kioudthoayplhukf";//no se cambia
		String str_Asunto = "PanambiSoft-Contrase�a de acceso";
		String str_Mensaje = "Hola "+nombre+", tu contrasenha es "+pass+"\nIngresa al sistema y cambia tu contrase�a a una que recuerdes con facilidad.";
		
		String htmlText = "<!DOCTYPE html><html><body><H5>"+str_Mensaje+"</H5><img src='cid:my-image-id.png' width='100' height='50'></body></html>";
		//String htmlText = str_Mensaje;
		//Seteamos el contenido con la imagen
		MimeMessage message = new MimeMessage(session);
		//set the headers
		message.setSubject(str_Asunto);
		message.setFrom(new InternetAddress(str_De));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo)); 
		//Create your new message part
		BodyPart messageBodyPart  = new MimeBodyPart();
		messageBodyPart.setContent(htmlText, "text/html");
		//Create a related multi-part to combine the parts
		MimeMultipart multipart = new MimeMultipart("related");
		multipart.addBodyPart(messageBodyPart);
		// Create part for the image
		messageBodyPart = new MimeBodyPart();
		// Fetch the image and associate to part
		messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("c:/Java/eclipse/workspace/PanambiSoft/src/py/com/panambi/images/imagenCentral.png")));
		messageBodyPart.setHeader("Content-ID", "<my-image-id.png>");
		messageBodyPart.addHeader("Content-Type", "image/jpeg");
		messageBodyPart.addHeader("Content-Transfer-Encoding", "base64");
		messageBodyPart.setFileName("panambiSoft.png");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		// Add part to multi-part
		multipart.addBodyPart(messageBodyPart);
		// Associate multi-part with message
		message.setContent(multipart);
		//Envia el mail
		Transport t = session.getTransport("smtp");
		t.connect(str_De, str_PwRemitente);
		t.sendMessage(message, message.getAllRecipients());  
		// Cierre de la conexion
		t.close();
		return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}      
	}
	
	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo");
			lblCorreo.setFont(new Font("Arial", Font.PLAIN, 11));
		}
		return lblCorreo;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbUsuario() {
		if (JCmbUsuario == null) {
			JCmbUsuario = new JComboBoxPanambi();
			JCmbUsuario.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusUsuario();
				}
			});
			JCmbUsuario.setEditable(true);
			JCmbUsuario.setModel(new ListComboBoxModel<String>(usuarios));
			JCmbUsuario.setSelectedItem(null);
		}
		return JCmbUsuario;
	}
	
	private void lostFocusUsuario() {
		try {
			Usuario usu = controladorUsuario.getUsuario(JFramePanambiMain.session.getConn(), (String) JCmbUsuario.getSelectedItem());
		    
			if(getJCmbUsuario().getSelectedItem().toString().length()==0){
		    	getBtnpnmbGrabar().setEnabled(false);
		    }else getBtnpnmbGrabar().setEnabled(true);
		    
			if (usu != null) {
				setValues(usu, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		try {
			if (obj instanceof Usuario) {
				this.usuario = (Usuario) obj;
				getTxtCodUsuario().setValor(usuario.getCodUsuario());
				getJCmbUsuario().setSelectedItem(((Usuario) (obj)).getUsuario());
				getTxtNombre().setText(usuario.getNombreCompleto());
				getTxtCorreo().setText(usuario.getCorreo());
				if (usuario.getEstado().equals("A")) {
					getChckbxEstado().setSelected(true);
				} else {
					getChckbxEstado().setSelected(false);
				}
				getTxtNombre().requestFocus();
				getChckbxReestablecer().setEnabled(true);
				getChckbxReestablecer().setSelected(false);
				List<PerfilUsuario> perfiles = usuario.getPerfilesUsuario();
				getTable().resetData(0);
				for (PerfilUsuario perfil : perfiles) {	
					getTable().addRow();
					getTable().setValueAt(Boolean.FALSE, getTable().getRowCount() - 1, 0);
					getTable().setValueAt(perfil.getCodPerfil(), getTable().getRowCount() - 1, 1);
					getTable().setValueAt(perfil.getNombre(), getTable().getRowCount() - 1, 2);
					getTable().setValueAt(perfil, getTable().getRowCount() - 1, 3);
				}
			}
			if (obj instanceof PerfilUsuario) {
				PerfilUsuario perfil = (PerfilUsuario) obj;
				if (isPerfilDuplicado(perfil)) {
					throw new ValidException("El perfil seleccionado ya ha sido agregado");
				}
				
				getTable().addRow();
				getTable().setValueAt(Boolean.FALSE, getTable().getRowCount() - 1, 0);
				getTable().setValueAt(perfil.getCodPerfil(), getTable().getRowCount() - 1, 1);
				getTable().setValueAt(perfil.getNombre(), getTable().getRowCount() - 1, 2);
				getTable().setValueAt(perfil, getTable().getRowCount() - 1, 3);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		usuario = null;
		getTable().resetData(0);
		//getBtnpnmbGrabar().setEnabled(true);
		getChckbxReestablecer().setSelected(false);
		getChckbxReestablecer().setEnabled(false);
		getTxtCodUsuario().setValor(null);
		getTxtCorreo().setText(null);
		getTxtNombre().setText(null);
		getChckbxEstado().setSelected(true);
		
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbUsuario());
	}
	private JFormattedTextFieldPanambi getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JFormattedTextFieldPanambi();
		}
		return txtCorreo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}
	private JLabelPanambi getLabelPanambi_2() {
		if (labelPanambi_2 == null) {
			labelPanambi_2 = new JLabelPanambi();
			labelPanambi_2.setText("*");
		}
		return labelPanambi_2;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
