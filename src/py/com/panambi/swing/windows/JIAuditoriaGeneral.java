package py.com.panambi.swing.windows;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXDatePicker;

import py.com.panambi.controller.ControladorAuditoria;
import py.com.panambi.informes.JIConsultarAuditoriaGeneral;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.utils.PanambiUtils;

public class JIAuditoriaGeneral extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7436630284567764275L;
	private JLabelPanambi lblpnmbSeleccioneElemento;
	private JComboBoxPanambi jCmbTablaAudit;
	private JScrollPane scrollPane;
	private JButtonPanambi jBtnConsultar;
	private JButtonPanambi jBtnLimpiar;
	private JTablePanambi jTblAuditoriaGeneral;
	private ControladorAuditoria controladorAuditoria = new ControladorAuditoria();
	private JLabelPanambi lblpnmbFechaDesde;
	private JXDatePicker jDatePickerFechaHasta;
	private JLabelPanambi lblpnmbFechaHasta;
	private JLabelPanambi lblpnmbOperacion;
	private JCheckBoxPanambi jChInserciones;
	private JCheckBoxPanambi jChModificaciones;
	private JCheckBoxPanambi jChEliminaciones;
	private JCheckBoxPanambi jChTodas;
	private JXDatePicker jDatePickerFechaDesde;
	private JButtonPanambi btnpnmbIrAlReporte;
	
	
	public JIAuditoriaGeneral() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Auditoria general");
		setBounds(100, 100, 975, 505);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		fecha();
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 914, Short.MAX_VALUE)
					.addGap(19))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(67)
							.addComponent(getLblpnmbSeleccioneElemento(), GroupLayout.PREFERRED_SIZE, 86, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(44)
							.addComponent(getLblpnmbFechaDesde(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(10)))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJCmbTablaAudit(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJDatePickerFechaDesde(), GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE))
					.addGap(69)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbFechaHasta(), Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbOperacion(), Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getJChModificaciones(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJChTodas(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJChInserciones(), GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
									.addGap(2))
								.addComponent(getJChEliminaciones(), GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
							.addGap(0))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJDatePickerFechaHasta(), GroupLayout.PREFERRED_SIZE, 57, Short.MAX_VALUE)
							.addGap(146))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbSeleccioneElemento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbTablaAudit(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJChTodas(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbOperacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJChInserciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJChModificaciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJChEliminaciones(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addGap(19)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJDatePickerFechaDesde(), GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJDatePickerFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(18)
					.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
					.addGap(20))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		setShortcuts(this);
		getJPanelSouth().add(getBtnpnmbIrAlReporte());
	}
	
	private void doLimpiar(){
		
		getJChTodas().setSelected(true);
		getJChInserciones().setSelected(false);
		getJChModificaciones().setSelected(false);
		getJChEliminaciones().setSelected(false);
		
		
		
		getJCmbTablaAudit().setSelectedItem("PRODUCTOS");
		getJCmbTablaAudit().requestFocus();
		
	}
	
	
	private JLabelPanambi getLblpnmbSeleccioneElemento() {
		if (lblpnmbSeleccioneElemento == null) {
			lblpnmbSeleccioneElemento = new JLabelPanambi();
			lblpnmbSeleccioneElemento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSeleccioneElemento.setText("Auditoria de : ");
		}
		return lblpnmbSeleccioneElemento;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JComboBoxPanambi getJCmbTablaAudit() {
		if (jCmbTablaAudit == null) {
			jCmbTablaAudit = new JComboBoxPanambi();
			jCmbTablaAudit.setToolTipText("Seleccione elemento a auditar");
			jCmbTablaAudit.setModel(new DefaultComboBoxModel(new String[] {"PRODUCTOS", "CLIENTES", "EMPLEADOS", "GASTOS", "FORMAS DE PAGO"}));
			
			jCmbTablaAudit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					//
				}
			});
		}
		return jCmbTablaAudit;
	}
	
	private void fecha(){
	//pack(); 
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setToolTipText("");
			//scrollPane.setViewportView(getJTblAuditoriaGeneral());
		}
		return scrollPane;
	}
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultar();
				}
			});
			jBtnConsultar.setToolTipText("Consultar datos de auditoria");
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					if(getJTblAuditoriaGeneral().getRowCount()>0){
						getJTblAuditoriaGeneral().resetData(0);
					}
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	
	private JTablePanambi getJTblAuditoriaGeneral() {
		if (jTblAuditoriaGeneral == null) {
			jTblAuditoriaGeneral = new JTablePanambi((String[]) null);
		}
		return jTblAuditoriaGeneral;
	}
	
	private void doConsultar(){
		try{
			Date fechaDesde = null;
			Date fechaHasta = null;
			boolean inserciones = false;
			boolean modificaciones = false;
			boolean eliminaciones = false;
			
			//**Obteniendo nombre de tablas.
			String nombreTabla = getNombreTabla((String)getJCmbTablaAudit().getSelectedItem());
			
			//**Obteniendo datos de fechas.
			try{
				fechaDesde = new Date(getJDatePickerFechaDesde().getDate().getTime());
			}catch(Exception e ){
				fechaDesde = null;
			}
			
			try{
				fechaHasta = new Date (getJDatePickerFechaHasta().getDate().getTime());
			}catch(Exception e){
				fechaHasta =null;
			}
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
				getJDatePickerFechaDesde().requestFocus();
			}else{
				
				//**Obteniendo datos de operaciones requeridas.
				if(getJChInserciones().isSelected()){
					inserciones = true;
				}else inserciones = false;
				
				if(getJChModificaciones().isSelected()){
					modificaciones = true;
				}else modificaciones = false;
				
				if(getJChEliminaciones().isSelected()){
					eliminaciones = true;
				}else eliminaciones = false;
				
				if(getJChTodas().isSelected()){
					inserciones=true;
					modificaciones=true;
					eliminaciones = true;
				}
				
				//**Poniendo nombres a las columnas
				List <String> listaNombresTabla = controladorAuditoria.getColumnNames(JFramePanambiMain.session.getConn(), nombreTabla);
				String[] columnNames = new String [3+listaNombresTabla.size()];
				columnNames[0] = "USUARIO";
				columnNames[1] = "OPERCION";
				columnNames[2] = "FECHA AUDITADA";
				for (int i = 0 ; i < listaNombresTabla.size();i++){
					columnNames[3+i] = listaNombresTabla.get(i);
				}
				
				List<List<Object>> listaAuditada= controladorAuditoria.getInfoAudit(JFramePanambiMain.session.getConn(), nombreTabla, inserciones, modificaciones, eliminaciones, fechaDesde, fechaHasta);
				
				HashMap<Integer, Class<?>> types = new HashMap<>();
				Integer[] editable = {}; 
				jTblAuditoriaGeneral = new JTablePanambi(columnNames, editable, types);
				getJTblAuditoriaGeneral() .setRowHeight(20);
				
				getScrollPane().setViewportView(getJTblAuditoriaGeneral());
				
				Iterator<List<Object>> iteratorAuditoria= listaAuditada.listIterator();
				getJTblAuditoriaGeneral().resetData(0);
				while (iteratorAuditoria.hasNext()) {
					
					getJTblAuditoriaGeneral().addRow();
					
					List<Object> lo = (List<Object>) iteratorAuditoria.next();
					
					try{
						getJTblAuditoriaGeneral().setValueAt(lo.get(0), getJTblAuditoriaGeneral().getRowCount()-1, 0);
					}catch (Exception e){
						getJTblAuditoriaGeneral().setValueAt("POSTGRES", getJTblAuditoriaGeneral().getRowCount()-1, 0);
					}
					
					getJTblAuditoriaGeneral().setValueAt(lo.get(1), getJTblAuditoriaGeneral().getRowCount()-1, 1);
					getJTblAuditoriaGeneral().setValueAt(lo.get(2), getJTblAuditoriaGeneral().getRowCount()-1, 2);
					
					for(int i=3; i<lo.size();i++){
						getJTblAuditoriaGeneral().setValueAt(lo.get(i), getJTblAuditoriaGeneral().getRowCount()-1, i);
					}
				}	
				
			}
			
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}	
		
	}
	
	
	private String getNombreTabla(String descripcion){
		String nombre = null;
		descripcion = descripcion.toLowerCase();
		switch (descripcion) {
		case "formas de pago":
			nombre = "formaspagos";
			break;

		default:
			nombre = descripcion;
			break;
		}
		
		return nombre;
	}
	
	private JLabelPanambi getLblpnmbFechaDesde() {
		if (lblpnmbFechaDesde == null) {
			lblpnmbFechaDesde = new JLabelPanambi();
			lblpnmbFechaDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDesde.setText("Fecha desde :");
		}
		return lblpnmbFechaDesde;
	}
	private JXDatePicker getJDatePickerFechaHasta() {
		if (jDatePickerFechaHasta == null) {
			jDatePickerFechaHasta = new JXDatePicker();
			jDatePickerFechaHasta.setToolTipText("Fecha tope de auditoria");
			jDatePickerFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDatePickerFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDatePickerFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	private JLabelPanambi getLblpnmbOperacion() {
		if (lblpnmbOperacion == null) {
			lblpnmbOperacion = new JLabelPanambi();
			lblpnmbOperacion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbOperacion.setText("Operaciones: ");
		}
		return lblpnmbOperacion;
	}
	private JCheckBoxPanambi getJChInserciones() {
		if (jChInserciones == null) {
			jChInserciones = new JCheckBoxPanambi();
			jChInserciones.setToolTipText("Todas las inserciones");
			jChInserciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectInserciones();
				}
			});
			jChInserciones.setText("INSERCIONES");
		}
		return jChInserciones;
	}
	
	private void doSelectInserciones(){
		if(getJChInserciones().isSelected()){
			getJChInserciones().setSelected(true);
			getJChTodas().setSelected(false);
		}else{
			getJChInserciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChModificaciones() {
		if (jChModificaciones == null) {
			jChModificaciones = new JCheckBoxPanambi();
			jChModificaciones.setToolTipText("Todas las modificaciones");
			jChModificaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectModificaciones();
				}
			});
			jChModificaciones.setText("MODIFICACIONES");
		}
		return jChModificaciones;
	}
	
	private void doSelectModificaciones(){
		if(getJChModificaciones().isSelected()){
			getJChModificaciones().setSelected(true);
			getJChTodas().setSelected(false);
			
		}else{
			getJChModificaciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChEliminaciones() {
		if (jChEliminaciones == null) {
			jChEliminaciones = new JCheckBoxPanambi();
			jChEliminaciones.setToolTipText("Todas las eliminaciones");
			jChEliminaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectEliminaciones();
				}
			});
			jChEliminaciones.setText("ELIMINACIONES");
		}
		return jChEliminaciones;
	}
	
	private void doSelectEliminaciones(){
		if(getJChEliminaciones().isSelected()){
			getJChEliminaciones().setSelected(true);
			getJChTodas().setSelected(false);
		}else{
			getJChEliminaciones().setSelected(false);
		}
		
		if(!getJChInserciones().isSelected() && !getJChModificaciones().isSelected() && !getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
		}
		
		if(getJChInserciones().isSelected() && getJChModificaciones().isSelected() && getJChEliminaciones().isSelected()){
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
	}
	
	private JCheckBoxPanambi getJChTodas() {
		if (jChTodas == null) {
			jChTodas = new JCheckBoxPanambi();
			jChTodas.setToolTipText("Todas las operaciones");
			jChTodas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodas();
				}
			});
			jChTodas.setSelected(true);
			jChTodas.setText("TODAS");
		}
		return jChTodas;
	}
	
	private void doSelectTodas(){
		if(getJChTodas().isSelected()){
			//JOptionPane.showMessageDialog(null, "no esta seleccionado");
			getJChTodas().setSelected(true);
			getJChInserciones().setSelected(false);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false); 
		}else{
			
			//JOptionPane.showMessageDialog(null, "esta seleccionado");
			getJChTodas().setSelected(false);
			getJChInserciones().setSelected(true);
			getJChModificaciones().setSelected(false);
			getJChEliminaciones().setSelected(false);
		}
		
	}
	private JXDatePicker getJDatePickerFechaDesde() {
		if (jDatePickerFechaDesde == null) {
			jDatePickerFechaDesde = new JXDatePicker();
			jDatePickerFechaDesde.setToolTipText("Fecha de comienzo de auditoria");
			jDatePickerFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDatePickerFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDatePickerFechaDesde;
	}
	private JButtonPanambi getBtnpnmbIrAlReporte() {
		if (btnpnmbIrAlReporte == null) {
			btnpnmbIrAlReporte = new JButtonPanambi();
			btnpnmbIrAlReporte.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doIrReporte();
				}
			});
			btnpnmbIrAlReporte.setText("Ir al Reporte");
		}
		return btnpnmbIrAlReporte;
	}
	
	private void doIrReporte(){
		try {
			PanambiUtils.setWaitCursor(this);
			SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JIConsultarAuditoriaGeneral jalta = new JIConsultarAuditoriaGeneral();
				getDesktopPane().add(jalta);
				jalta.centerIt();
				jalta.setVisible(true);
				}
			});
		} finally {
			PanambiUtils.setDefaultCursor(this);
		}
				
	}
	
//	 private boolean checkInternalFrame(Object obj){
//	       
//	        JInternalFrame[] activos= getDesktopPane().getAllFrames();
//	       
//	        boolean cerrado = true;
//	       
//	        int i=0;
//	        while(i<activos.length && cerrado){
//	            if(activos[i] == obj)
//	                cerrado = false;
//	                i++;
//	                       
//	        } return cerrado;
//	       
//	    }
	
}
