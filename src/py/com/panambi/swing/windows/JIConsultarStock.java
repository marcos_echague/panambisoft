package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Producto;
import py.com.panambi.bean.StockProducto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorStockProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.prompts.PromptProductos;

public class JIConsultarStock extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8094363506566689965L;
	private JButtonPanambi btnpnmbSalirt;
	private JButtonPanambi btnpnmbConsultar;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbDescripcin;
	private JTextField jTxtDescripcion;
	private JRadioButton rdbtnTodos;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private Producto producto;
	private ControladorProducto controladorProducto =  new ControladorProducto();
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbSucursal;
	private List<String> sucursales = new ArrayList<String>();
	private JButtonPanambi btnpnmbExaminar;
	public JIConsultarStock() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Consutla de stock de productos");
		setMaximizable(false);
		setBounds(100, 100, 805, 475);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbConsultar());
		getJPanelSouth().add(getBtnpnmbSalirt());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(22, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
							.addGap(39)))
					.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 742, GroupLayout.PREFERRED_SIZE)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(25, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(8)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
							.addGap(18))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(60)))
					.addGap(8)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
					.addGap(20))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("consultarStock");
		doLimpiar();
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			this.producto = null;
			poblarListaSucursales();
			
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			getRdbtnTodos().setSelected(true);
			getJTxtCodigo().setValor(null);
			getJTxtDescripcion().setText("");
			
			if(getTablePanambi().getRowCount()!=0){
				getTablePanambi().resetData(0);
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
			
	}
	
	private void poblarListaSucursales() throws Exception{
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSuc = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JButtonPanambi getBtnpnmbSalirt() {
		if (btnpnmbSalirt == null) {
			btnpnmbSalirt = new JButtonPanambi();
			btnpnmbSalirt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalirt.setToolTipText("Cerrar ventana");
			btnpnmbSalirt.setMnemonic('S');
			btnpnmbSalirt.setText("Salir");
		}
		return btnpnmbSalirt;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JButtonPanambi getBtnpnmbConsultar() {
		if (btnpnmbConsultar == null) {
			btnpnmbConsultar = new JButtonPanambi();
			btnpnmbConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultar();
				}
			});
			btnpnmbConsultar.setMnemonic('C');
			btnpnmbConsultar.setToolTipText("Consultar stock de productos");
			btnpnmbConsultar.setText("Consultar");
		}
		return btnpnmbConsultar;
	}
	
	private void doConsultar(){
		try{
			Sucursal sucursal = null;
			String sucu = (String)getJCmbSucursal().getSelectedItem();
			
			if(!sucu.equals("TODAS")){
				sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), sucu);
			}
			List<StockProducto> listaStock = new ControladorStockProducto().consultarStock(JFramePanambiMain.session.getConn(), producto, sucursal);
			
			if(listaStock.size()==0){
				DlgMessage.showMessage(getOwner(), "Ning�n registro encontrado seg�n los par�metros introducidos.", DlgMessage.INFORMATION_MESSAGE);
			}
			Iterator<StockProducto> iteratorStock= listaStock.listIterator();
			if(getTablePanambi().getRowCount()!=0){
				getTablePanambi().resetData(0);
			}
			
			while (iteratorStock.hasNext()) {
				getTablePanambi().addRow();
				
				StockProducto stockProducto= (StockProducto) iteratorStock.next();
				//"Producto", "Sucursal","Stock m�nimo", "Stock actual
				getTablePanambi().setValueAt(stockProducto.getProducto().getDescripcion(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(stockProducto.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 1);
								
				getTablePanambi().setValueAt(stockProducto.getProducto().getStockMinimo(), getTablePanambi().getRowCount()-1, 2);
				getTablePanambi().setValueAt(stockProducto.getCantidad(), getTablePanambi().getRowCount()-1, 3);
				
			}	
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJTxtCodigo().requestFocus();
				}
			});
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusCodCliente();
				}
			});
		}
		return jTxtCodigo;
	}
	
	private void lostFocusCodCliente(){
		
		try{
			Integer longitud = 0;
			try{
				longitud = getJTxtCodigo().getText().length();
			}catch(Exception e){
				longitud = 0;
			}
			if(longitud==0){
				producto = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Producto prod=  controladorProducto.getProductoAllowNull(JFramePanambiMain.session.getConn(), getJTxtCodigo().getValor());
				
				if(prod != null){
					setValues(prod, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					producto=null;
					getJTxtCodigo().setValor(null);
					getJTxtDescripcion().setText("");
					getRdbtnTodos().setSelected(true);
					doPrompProducto();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doPrompProducto() {
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}
	
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbDescripcin() {
		if (lblpnmbDescripcin == null) {
			lblpnmbDescripcin = new JLabelPanambi();
			lblpnmbDescripcin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDescripcin.setText("Descripci\u00F3n : ");
		}
		return lblpnmbDescripcin;
	}
	private JTextField getJTxtDescripcion() {
		if (jTxtDescripcion == null) {
			jTxtDescripcion = new JTextField();
			jTxtDescripcion.setFocusable(false);
			jTxtDescripcion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtDescripcion.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtDescripcion.setEditable(false);
			jTxtDescripcion.setColumns(10);
		}
		return jTxtDescripcion;
	}
	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosProductos();
				}
			});
		}
		return rdbtnTodos;
	}
	private void doSelectTodosProductos(){
		getRdbtnTodos().setSelected(true);
		this.producto = null;
		getJTxtCodigo().setValor(null);
		getJTxtDescripcion().setText("");
	}
	
	private void doExaminar(){
		
		if(producto==null){
			producto=null;
			getJTxtCodigo().setValor(null);
			getJTxtDescripcion().setText("");
			getRdbtnTodos().setSelected(true);
			
		}
		doPrompProducto();
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Producto", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLblpnmbDescripcin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
								.addGap(37)
								.addComponent(getRdbtnTodos())
								.addGap(38)
								.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getJTxtDescripcion(), GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getRdbtnTodos()))
									.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(15)
								.addComponent(getJTxtDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(17)
								.addComponent(getLblpnmbDescripcin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			String[] columnNames = { "Producto", "Sucursal","Stock m�nimo", "Stock actual"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, String.class);
			types.put(1, String.class);
			types.put(2, Integer.class);
			types.put(3, Integer.class);
			Integer[] editable = { };
			tablePanambi = new JTablePanambi(columnNames,editable,types);
			tablePanambi.setRowHeight(20);
			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(300);
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(70);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(40);
			
		}
		return tablePanambi;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		if(obj instanceof Producto){
			this.producto=(Producto) obj;
			getRdbtnTodos().setSelected(false);
			getJTxtCodigo().setValor(producto.getCodProducto());
			getJTxtDescripcion().setText(producto.getDescripcion());
		}
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.TRAILING);
			lblpnmbSucursal.setText("Sucursal");
		}
		return lblpnmbSucursal;
	}
	private JButtonPanambi getBtnpnmbExaminar() {
		if (btnpnmbExaminar == null) {
			btnpnmbExaminar = new JButtonPanambi();
			btnpnmbExaminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doExaminar();
				}
			});
			btnpnmbExaminar.setToolTipText("Buscar producto");
			btnpnmbExaminar.setMnemonic('E');
			btnpnmbExaminar.setText("Examinar ...");
		}
		return btnpnmbExaminar;
	}
}
