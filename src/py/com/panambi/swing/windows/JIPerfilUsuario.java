package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.controller.ControladorPerfilUsuario;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;


public class JIPerfilUsuario extends JInternalFramePanambi implements Browseable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3311645412692147066L;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodPerfil;
	private JLabelPanambi lblpnmbNombreDelPerfil;
	private JComboBoxPanambi jCmbNombre;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private PerfilUsuario perfil;
	private List<String> perfiles = new ArrayList<>();
	private ControladorPerfilUsuario controladorPerfilUsuario = new ControladorPerfilUsuario();
	private JCheckBox chckbxActivo;
    private JLabelPanambi labelPanambi;
    private JLabel lblPresioneFPara;

	/**
	 * Create the frame.
	 */
	public JIPerfilUsuario() throws Exception{

		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Perfil de usuario");
		setBounds(100, 100, 390, 290);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getJTxtCodPerfil(), GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
									.addComponent(getChckbxActivo())
									.addGap(40))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbNombreDelPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(19)
									.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
							.addContainerGap())
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(getLblPresioneFPara())
							.addGap(21))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(getLblPresioneFPara())
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtCodPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getChckbxActivo()))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbNombreDelPerfil(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(34))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		doLimpiar();
		setShortcuts(this);
		setHelp("perfilesusuarios");
		decorate();
		getJCmbNombre().requestFocus();
		
		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNombre(), getChckbxActivo(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodPerfil() {
		if (jTxtCodPerfil == null) {
			jTxtCodPerfil = new JTextFieldInteger();
			jTxtCodPerfil.setEnabled(false);
			jTxtCodPerfil.setEditable(false);
		}
		return jTxtCodPerfil;
	}
	private JLabelPanambi getLblpnmbNombreDelPerfil() {
		if (lblpnmbNombreDelPerfil == null) {
			lblpnmbNombreDelPerfil = new JLabelPanambi();
			lblpnmbNombreDelPerfil.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNombreDelPerfil.setText("Nombre del Perfil : ");
		}
		return lblpnmbNombreDelPerfil;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNombre() {
		if (jCmbNombre == null) {
			jCmbNombre = new JComboBoxPanambi();
			jCmbNombre.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNombre();
				}
			});
			
			jCmbNombre.setEditable(true);
			jCmbNombre.setModel(new ListComboBoxModel<String>(perfiles));
			jCmbNombre.setSelectedItem(null);
		}
		return jCmbNombre;
	}
	
	private void lostFocusNombre() {
		try {
			PerfilUsuario perf= controladorPerfilUsuario.getPerfil(JFramePanambiMain.session.getConn(), (String) jCmbNombre.getSelectedItem());
			if (perf != null) {
				setValues(perf, null);
				btnpnmbGrabar.setEnabled(true);
				btnpnmbEliminar.setEnabled(true);
			} else {
				doNuevoFromLostFocus();
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		perfil = null;
		getJTxtCodPerfil().setValor(null);
		chckbxActivo.setSelected(true);
		Integer longitud = 0;
		try{
			longitud = getJCmbNombre().getSelectedItem().toString().length();
		}catch(Exception e){
			longitud = 0;
		}
		
		if(longitud!=0){
			btnpnmbGrabar.setEnabled(true);
			
		}else {
			btnpnmbGrabar.setEnabled(false);
		}
		btnpnmbEliminar.setEnabled(false);
	}
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma eliminar de forma \npermante los datos del perdil de usuario? ", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				controladorPerfilUsuario.borrarPerfil(JFramePanambiMain.session.getConn(), perfil);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbNombre().requestFocus();
			}
			perfil = null;
			getJCmbNombre().requestFocus();
		} catch (SQLException e) {
			if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
				DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
			} else {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (perfil == null) {
				perfil = new PerfilUsuario();
				perfil.setNombre((String) getJCmbNombre().getSelectedItem());
				if(getChckbxActivo().isSelected()){
					perfil.setEstado("A");
					
				} else perfil.setEstado("I");
				
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la creaci�n del nuevo perfil de usuario ? ", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.YES_OPTION){
					controladorPerfilUsuario.insertarPerfil(JFramePanambiMain.session.getConn(), perfil);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa.\nSi desea agregar permisos al perfil vaya a la ventana de permisos (Ctrl + Alt + P).", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
				
			} else {
				perfil.setNombre((String) getJCmbNombre().getSelectedItem());
				
				if(getChckbxActivo().isSelected()){
					perfil.setEstado("A");
					
				}else perfil.setEstado("I");
				
				Integer ret = JOptionPane.showConfirmDialog(this, "Esta seguro que desea modificar los datos del perfil de usuario? ", "", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.YES_OPTION){
					controladorPerfilUsuario.modificarPerfil(JFramePanambiMain.session.getConn(), perfil);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
				perfil = null;
				getJCmbNombre().requestFocus();
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nombre del Perfil", "Estado"};
			String[] pks = { "codPerfil" };
			String sSelect = "SELECT codperfil, nombre, estado ";
			sSelect += "FROM perfiles ";
			sSelect += "ORDER BY nombre";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.PerfilUsuario", pks);
			jb.setVisible(true);
			//getJCmbNombre().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbNombre().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try {
			perfil = null;
			poblarLista();
			getJCmbNombre().setModel(new ListComboBoxModel<String>(perfiles));
			getJCmbNombre().setSelectedItem("");
			getJTxtCodPerfil().setValor(null);
			chckbxActivo.setSelected(true);
			btnpnmbGrabar.setEnabled(false);
			btnpnmbEliminar.setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void poblarLista() throws Exception {
		perfiles = new ArrayList<String>();
		List<PerfilUsuario> listaperf = controladorPerfilUsuario.getPerfiles(JFramePanambiMain.session.getConn());
		for (PerfilUsuario perf : listaperf) {
			perfiles.add(perf.getNombre());
		}
	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof PerfilUsuario) {
			this.perfil = (PerfilUsuario) obj;
			getJTxtCodPerfil().setValor(perfil.getCodPerfil());
			getJCmbNombre().setSelectedItem(((PerfilUsuario) (obj)).getNombre());
			if(perfil.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			}else chckbxActivo.setSelected(false);
		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbNombre());
	}
	
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
		}
		return chckbxActivo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
}
