package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.DetallePagoSalario;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.TipoDescuentoSalario;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;

public class DlgDetallePagoEmpleado extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbFecha;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbProveedor;
	private JLabelPanambi lblpnmbTipoPago;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldInteger jTxtCodigo;
	private JFormattedTextFieldPanambi jTxtEmpleado;
	private JTextFieldDouble jTxtTotalNominal;
	private JLabelPanambi lblpnmbDetalleDeVenta;
	private PagoSalario pagoSalario;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JFormattedTextFieldPanambi jTxtFechaPago;
	private JFormattedTextFieldPanambi jTxtTipoPago;
	private JFormattedTextFieldPanambi jTxtEstado;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JLabelPanambi lblpnmbPagoALos;
	private JTextFieldInteger jTxtReciboNro;
	private JButtonPanambi btnpnmbDetallesDePago;
	private JTextFieldDouble jTxtTotalDescuento;
	private JLabelPanambi lblpnmbTotalDescuento;
	private JTextFieldDouble jTxtTotalPagado;
	private JLabelPanambi lblpnmbTotalPagado;
	public DlgDetallePagoEmpleado(PagoSalario pagoSalario) {
		this.pagoSalario = pagoSalario;
		initialize();
		
	}
	private void initialize() {
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetallePagoEmpleado.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 855, 531);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		getBtnpnmbAceptar().requestFocus();
	}

	private void cargarValores(){
		try{
			
			getJTxtCodigo().setText(pagoSalario.getCodpagosalario()+"");
			Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoSalario.getCodempleado());
			getJTxtEmpleado().setText(emp.getNombre()+" "+emp.getApellido());
			getJTxtTotalNominal().setValue(pagoSalario.getSalarionominal());
			getJTxtTotalDescuento().setValue(pagoSalario.getTotaldescuento());
			getJTxtTotalPagado().setValue(pagoSalario.getTotalpagado());
			
			String  patronFecha = "dd/MM/yyyy";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
			getJTxtFechaPago().setText(formatoFecha.format(pagoSalario.getFechapago()));
//			String tipoVenta = "";
//			if(c.getTipoVenta().equals("C")){
//				tipoVenta = "CREDITO";
//			}else{
//				tipoVenta = "CONTADO";
//			}
			String tipo = "";
			if(pagoSalario.getTipo().equals("S")){
				tipo = "Salario";
			}else if(pagoSalario.getTipo().equals("A")){
				tipo = "Aguinaldo";
			}else{
				tipo = "Liquidacion";
//				DeudaEmpleado deudaEmpleado = null;
//				deudaEmpleado = new ControladorDeudaEmpleado().getDeudaEmpleadoActivaPorPago(JFramePanambiMain.session.getConn(), pagoSalario.getCodempleado());
//				if(deudaEmpleado!=null){
//					
//				}
			}
			getJTxtTipoPago().setText(tipo);
			
			if(pagoSalario.getEstado().equals("P")){
				getJTxtEstado().setText("Pagado");
			}else if (pagoSalario.getEstado().equals("NP")){
				getJTxtEstado().setText("No pagado");
				getBtnpnmbDetallesDePago().setVisible(false);
			}else{
				getJTxtEstado().setText("Anulado");
				getBtnpnmbDetallesDePago().setVisible(false);
			}
			
			getJTxtReciboNro().setValor(pagoSalario.getNumeroRecibo());
			
			
//			List<DetalleCompra> detalleCompra = new ArrayList<DetalleCompra>();
//			detalleCompra = compra.getDetalleCompra();
			Iterator<DetallePagoSalario> iteratorDetalle=  pagoSalario.getDetallePagoSalario().listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				DetallePagoSalario det= (DetallePagoSalario) iteratorDetalle.next();
				//"Nro. Item","Concepto", "Monto", "Porcentaje","Tipo descuento","Estado"};
				getTablePanambi().setValueAt(det.getNroitem(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(det.getConcepto(), getTablePanambi().getRowCount()-1, 1);
					
				getTablePanambi().setValueAt(det.getMonto(), getTablePanambi().getRowCount()-1, 2);
				getTablePanambi().setValueAt(det.getPorcentaje(), getTablePanambi().getRowCount()-1, 3);
				
				TipoDescuentoSalario tipoDescuento ;
				
				if(det.getCodtipodescuentosalario()!=0){
					tipoDescuento = new TipoDescuentoSalario(JFramePanambiMain.session.getConn(), det.getCodtipodescuentosalario());
					getTablePanambi().setValueAt(tipoDescuento.getNombre(), getTablePanambi().getRowCount()-1, 4);
				}else{
					getTablePanambi().setValueAt(null, getTablePanambi().getRowCount()-1, 4);
				}
				
				
				getTablePanambi().setValueAt(det.getEstado(), getTablePanambi().getRowCount()-1, 5);
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 839, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLES DE PAGO A EMPLEADO");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 839, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha de pago : ");
		}
		return lblpnmbFecha;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProveedor.setText("Empleado : ");
		}
		return lblpnmbProveedor;
	}
	private JLabelPanambi getLblpnmbTipoPago() {
		if (lblpnmbTipoPago == null) {
			lblpnmbTipoPago = new JLabelPanambi();
			lblpnmbTipoPago.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTipoPago.setText("Tipo de pago : ");
		}
		return lblpnmbTipoPago;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Total Nominal : ");
		}
		return lblpnmbTotal;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JFormattedTextFieldPanambi getJTxtEmpleado() {
		if (jTxtEmpleado == null) {
			jTxtEmpleado = new JFormattedTextFieldPanambi();
			jTxtEmpleado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEmpleado.setFocusable(false);
			jTxtEmpleado.setEditable(false);
		}
		return jTxtEmpleado;
	}
	private JTextFieldDouble getJTxtTotalNominal() {
		if (jTxtTotalNominal == null) {
			jTxtTotalNominal = new JTextFieldDouble();
			jTxtTotalNominal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalNominal.setFocusable(false);
			jTxtTotalNominal.setEditable(false);
		}
		return jTxtTotalNominal;
	}
	private JLabelPanambi getLblpnmbDetalleDeVenta() {
		if (lblpnmbDetalleDeVenta == null) {
			lblpnmbDetalleDeVenta = new JLabelPanambi();
			lblpnmbDetalleDeVenta.setText("Detalles");
		}
		return lblpnmbDetalleDeVenta;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. Item","Concepto", "Monto", "Porcentaje","Tipo descuento","Estado"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(2, Double.class);
				types.put(3, Double.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(250);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(70);
				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JFormattedTextFieldPanambi getJTxtFechaPago() {
		if (jTxtFechaPago == null) {
			jTxtFechaPago = new JFormattedTextFieldPanambi();
			jTxtFechaPago.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaPago.setFocusable(false);
			jTxtFechaPago.setEditable(false);
		}
		return jTxtFechaPago;
	}
	private JFormattedTextFieldPanambi getJTxtTipoPago() {
		if (jTxtTipoPago == null) {
			jTxtTipoPago = new JFormattedTextFieldPanambi();
			jTxtTipoPago.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTipoPago.setFocusable(false);
			jTxtTipoPago.setEditable(false);
		}
		return jTxtTipoPago;
	}
	private JFormattedTextFieldPanambi getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JFormattedTextFieldPanambi();
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 839, 417);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGap(216)
								.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(22)
								.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtEmpleado(), GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
								.addGap(67)
								.addComponent(getLblpnmbTipoPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtTipoPago(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(18)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(6)
										.addComponent(getJTxtTotalDescuento(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(6)
										.addComponent(getJTxtTotalNominal(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(getJTxtTotalPagado(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
								.addGap(200)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(10)
										.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbPagoALos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(10)
										.addComponent(getJTxtReciboNro(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
						.addContainerGap(157, Short.MAX_VALUE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap(389, Short.MAX_VALUE)
						.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(346))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap(42, Short.MAX_VALUE)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 761, GroupLayout.PREFERRED_SIZE)
						.addGap(32))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbTipoPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTipoPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotalNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbPagoALos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
											.addComponent(getLblpnmbTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJTxtTotalDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
											.addComponent(getJTxtTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbTotalPagado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(11)
								.addComponent(getJTxtReciboNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(11)
						.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
						.addGap(119))
			);
			panel_1.setLayout(gl_panel_1);
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 456, 839, 37);
			panel_2.add(getBtnpnmbAceptar());
			panel_2.add(getBtnpnmbDetallesDePago());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbPagoALos() {
		if (lblpnmbPagoALos == null) {
			lblpnmbPagoALos = new JLabelPanambi();
			lblpnmbPagoALos.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPagoALos.setText("Recibo nro  : ");
		}
		return lblpnmbPagoALos;
	}
	private JTextFieldInteger getJTxtReciboNro() {
		if (jTxtReciboNro == null) {
			jTxtReciboNro = new JTextFieldInteger();
			jTxtReciboNro.setEditable(false);
			jTxtReciboNro.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtReciboNro.setFocusable(false);
		}
		return jTxtReciboNro;
	}
	private JButtonPanambi getBtnpnmbDetallesDePago() {
		if (btnpnmbDetallesDePago == null) {
			btnpnmbDetallesDePago = new JButtonPanambi();
			btnpnmbDetallesDePago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doDetallePago();
				}
			});
			btnpnmbDetallesDePago.setToolTipText("Detalles del pago relacionado a la compra");
			btnpnmbDetallesDePago.setText("Detalles de pago");
		}
		return btnpnmbDetallesDePago;
	}
	
	private void doDetallePago(){
		try{
			Pago pago = new Pago();
			pago = new ControladorPago().getPago(JFramePanambiMain.session.getConn(), pagoSalario);
			
			if(pago!=null){
				DlgDetallePagos detallePago = new DlgDetallePagos(pago);
				detallePago.centerIt();
				detallePago.setVisible(true);
			}else{
				DlgMessage.showMessage(getOwner(), "No existe pagos relacionados al pago hecho al empleado.", DlgMessage.ERROR_MESSAGE);
			}
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(),e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	private JTextFieldDouble getJTxtTotalDescuento() {
		if (jTxtTotalDescuento == null) {
			jTxtTotalDescuento = new JTextFieldDouble();
			jTxtTotalDescuento.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalDescuento.setFocusable(false);
			jTxtTotalDescuento.setEditable(false);
		}
		return jTxtTotalDescuento;
	}
	private JLabelPanambi getLblpnmbTotalDescuento() {
		if (lblpnmbTotalDescuento == null) {
			lblpnmbTotalDescuento = new JLabelPanambi();
			lblpnmbTotalDescuento.setText("Total Descuento : ");
			lblpnmbTotalDescuento.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbTotalDescuento;
	}
	private JTextFieldDouble getJTxtTotalPagado() {
		if (jTxtTotalPagado == null) {
			jTxtTotalPagado = new JTextFieldDouble();
			jTxtTotalPagado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalPagado.setFocusable(false);
			jTxtTotalPagado.setEditable(false);
		}
		return jTxtTotalPagado;
	}
	private JLabelPanambi getLblpnmbTotalPagado() {
		if (lblpnmbTotalPagado == null) {
			lblpnmbTotalPagado = new JLabelPanambi();
			lblpnmbTotalPagado.setText("Total Pagado : ");
			lblpnmbTotalPagado.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbTotalPagado;
	}
}
