package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class DlgAcercade extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi lblpnmbPanambisoft;
	private JLabelPanambi lblpnmbVersin;
	private JLabelPanambi lblpnmbCopyright;
	private JButtonPanambi btnpnmbAceptar_1;
	private JLabelPanambi lblpnmbDesarrolladoPor;
	public DlgAcercade() {
		initialize();
		
	}
	private void initialize() {
		setTitle("Acerca de PanambiSoft");
		getContentPane().setBackground(Color.WHITE);
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgAcercade.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 465, 352);
		getBtnpnmbAceptar().requestFocus();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 474, GroupLayout.PREFERRED_SIZE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addComponent(getLblpnmbPanambisoft(), GroupLayout.PREFERRED_SIZE, 371, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addComponent(getLblpnmbVersin(), GroupLayout.PREFERRED_SIZE, 371, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addComponent(getLblpnmbDesarrolladoPor(), GroupLayout.PREFERRED_SIZE, 371, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addComponent(getLblpnmbCopyright(), GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getBtnpnmbAceptar_1(), GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
				.addComponent(getPanel_2(), GroupLayout.PREFERRED_SIZE, 801, GroupLayout.PREFERRED_SIZE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getLblpnmbPanambisoft(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getLblpnmbVersin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getLblpnmbDesarrolladoPor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbCopyright(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(getBtnpnmbAceptar_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(250)
					.addComponent(getPanel_2(), GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
		);
		getContentPane().setLayout(groupLayout);
	}


	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.add(getBtnpnmbAceptar());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}

	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setHorizontalAlignment(SwingConstants.CENTER);
			labelPanambi.setIcon(new ImageIcon(DlgAcercade.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg")));
		}
		return labelPanambi;
	}
	private JLabelPanambi getLblpnmbPanambisoft() {
		if (lblpnmbPanambisoft == null) {
			lblpnmbPanambisoft = new JLabelPanambi();
			lblpnmbPanambisoft.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblpnmbPanambisoft.setText("Sistema de gesti\u00F3n comercial PanambiSoft ");
		}
		return lblpnmbPanambisoft;
	}
	private JLabelPanambi getLblpnmbVersin() {
		if (lblpnmbVersin == null) {
			lblpnmbVersin = new JLabelPanambi();
			lblpnmbVersin.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblpnmbVersin.setText("Versi\u00F3n 1.0 2014");
		}
		return lblpnmbVersin;
	}
	private JLabelPanambi getLblpnmbCopyright() {
		if (lblpnmbCopyright == null) {
			lblpnmbCopyright = new JLabelPanambi();
			lblpnmbCopyright.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblpnmbCopyright.setText("Copyright \u00A9 2014. Todos los derechos reservados");
		}
		return lblpnmbCopyright;
	}
	private JButtonPanambi getBtnpnmbAceptar_1() {
		if (btnpnmbAceptar_1 == null) {
			btnpnmbAceptar_1 = new JButtonPanambi();
			btnpnmbAceptar_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doCerrar();
				}
			});
			btnpnmbAceptar_1.setText("Aceptar");
		}
		return btnpnmbAceptar_1;
	}
	
	private void doCerrar(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbDesarrolladoPor() {
		if (lblpnmbDesarrolladoPor == null) {
			lblpnmbDesarrolladoPor = new JLabelPanambi();
			lblpnmbDesarrolladoPor.setText("Desarrollado por Maria Cruz Ben\u00EDtez, Hugo Cano y Marcos Echag\u00FCe");
			lblpnmbDesarrolladoPor.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblpnmbDesarrolladoPor;
	}
}
