package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.informes.JRDataSourceReciboPagoCuota;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.interfaces.ReciboBrowser;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.utils.NumerosALetras;
import py.com.panambi.utils.PanambiUtils;

public class JIPagoCuota extends JInternalFramePanambi implements Browseable , ReciboBrowser {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7191669451568741710L;
	private JLabelPanambi lblpnmbCliente;
	private JScrollPane jSPVentas;
	private JTable jTblVentas;
	private JScrollPane jSPCuotas;
	private JTable jTblCuotas;
	private Venta venta;
	private Cliente cliente;
	private ControladorPlanPago controladorPagoCuota = new ControladorPlanPago();
	private ControladorVenta controladorVenta = new ControladorVenta();
	private List<Integer> nrosFacturas = new ArrayList<Integer>();
	private JButtonPanambi btnpnmbLimpiar;
	private JPanel panel;
	private JLabelPanambi lblpnmbNroCedula;
	private JButtonPanambi btnpnmbSalir;
	private JPanel jPnlVenta;
	private JPanel jPnlCuotas;
	private JLabel lblFacturaNro;
	private JPanel panel_1;
	private JButtonPanambi buttonPanambi;
	private JButtonPanambi btnpnmbX;
	private JScrollPane scrollPane;
	private JTable jTblPagoCuota;
	private DefaultTableModel dtmVenta;
	private DefaultTableModel dtmPlanPago;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	@SuppressWarnings("serial")
	private DefaultTableModel dtmCuota = new DefaultTableModel() {

		@Override
		public boolean isCellEditable(int row, int column) {
			// all cells false
			return false;
		}
	};
	private JButtonPanambi btnpnmbGenerarPago;
	private JTextFieldUpper jTxtCliente;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldDouble jTxtTotal;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodCliente;
	private JTextFieldUpper jTxtNroCedula;
	private JButtonPanambi btnpnmbExaminar;

	public JIPagoCuota() throws Exception{
		initialize();
	}

	private void initialize() {
		setMaximizable(false);
		setTitle("Pagos de Cuotas");
		setBounds(100, 100, 1185, 505);
		
		
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbGenerarPago());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(966)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 544, GroupLayout.PREFERRED_SIZE)
					.addGap(61)
					.addComponent(getJPnlVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getJPnlCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getButtonPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnpnmbX(), GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addComponent(getPanel_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(754, Short.MAX_VALUE)
					.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
					.addGap(187))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJPnlVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJPnlCuotas(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(35)
							.addComponent(getButtonPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(11)
							.addComponent(getBtnpnmbX(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getPanel_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbTotal(), GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
						.addComponent(getJTxtTotal(), GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
					.addContainerGap())
		);
		try {
			// Carga el fichero de ayuda
			fichero = new File("./bin/py/com/panambi/help/help_set.hs");
			hsURL = fichero.toURI().toURL();
			helpset = new HelpSet(getClass().getClassLoader(), hsURL);
			hb = helpset.createHelpBroker();
			// Pone ayuda a item de menu al pulsar F1. mntmIndice es el
			// JMenuitem
			hb.enableHelpKey(getRootPane(), "pagocuotas", helpset);
		} catch (Exception e) {
			// logger.error(getOwner(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		doLimpiar();
		decorate();
		setShortcuts(this);
		getJPanelCentral().setLayout(groupLayout);
	}

	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Nombre y Apellido: ");
		}
		return lblpnmbCliente;
	}

	private JScrollPane getScrollPane_1() {
		if (jSPVentas == null) {
			jSPVentas = new JScrollPane();
			jSPVentas.setViewportView(getJTblVentas());
		}
		return jSPVentas;
	}

	private JTable getJTblVentas() {
		if (jTblVentas == null) {
			jTblVentas = new JTable();
			jTblVentas.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent e) {
			    	doSelectVenta();
				}
			});        // here goes your code "on cell update"
			
			
//			jTblVentas.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "C�digo", "Fecha", "Monto total", "Sucursal" }));
//			jTblVentas.addMouseListener(new MouseAdapter() {
//				@Override
//				public void mouseClicked(MouseEvent e) {
//					doSelectVenta();
//				}
//			});
//			jTblVentas.getColumnModel().getColumn(0).setPreferredWidth(80);
//			jTblVentas.getColumnModel().getColumn(1).setPreferredWidth(125);
//			jTblVentas.getColumnModel().getColumn(2).setPreferredWidth(108);
//			jTblVentas.getColumnModel().getColumn(3).setPreferredWidth(120);

		}
		return jTblVentas;
	}

	private void doSelectVenta() {

		if (jTblVentas.getSelectedRowCount() > 0) {
			Integer rowSelect = jTblVentas.getSelectedRow();
			// Integer ventaCod =
			// Date ventaFecha = (Date) jTblVentas.getValueAt(rowSelect, 1);
			// Double ventaTotal = (Double) jTblVentas.getValueAt(rowSelect, 2);
			// Sucursal ventaSucursal= (Sucursal)
			// jTblVentas.getValueAt(rowSelect, 3);
			// String ventaSucursal = (String) jTblVentas.getValueAt(rowSelect,
			// 3);

			doCargarPlanPago((Integer) jTblVentas.getValueAt(rowSelect, 0));
		}

	}

	@SuppressWarnings("serial")
	private void doCargarPlanPago(Integer ventaCod) {

		try {
			List<PlanPago> planesPagos = new ArrayList<PlanPago>();
			planesPagos = controladorPagoCuota.getPlanesPagos(JFramePanambiMain.session.getConn(), ventaCod);
			dtmPlanPago = new DefaultTableModel() {
				public boolean isCellEditable(int reg, int column) {
					return false;
				}
			};

			Object[] cabeceraPlanPago = { "C�digo", "Fecha de pago", "Importe", "Nro. de cuota", "Fecha de vencimiento", "Estado" };
			dtmPlanPago.setColumnIdentifiers(cabeceraPlanPago);
			Iterator<PlanPago> iteratorPlanPago = planesPagos.listIterator();
			
			String estadoPago = "";
			while (iteratorPlanPago.hasNext()) {

				PlanPago pp = (PlanPago) iteratorPlanPago.next();
				if(pp.getEstado().equals("P")){
					estadoPago = "Pagado";
				}else{
					estadoPago = "Debe";
				}
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				String fechaPago = null;
				if(pp.getFechaPago()!=null){
					fechaPago = formato.format(pp.getFechaPago());
				}
				String fechaVencimiento = null;
				if(pp.getVencimiento()!=null){
					fechaVencimiento = formato.format(pp.getVencimiento());
				}
				Object[] row = { pp.getCodPlanPago(), fechaPago, pp.getMonto(), pp.getNroCuota(), fechaVencimiento, estadoPago };
				dtmPlanPago.addRow(row);
			}

			getJTblCuotas().setModel(dtmPlanPago);
			getJTxtNroFactura().setValor(controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), ventaCod).getNroComprobante());
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private JScrollPane getScrollPane_2() {
		if (jSPCuotas == null) {
			jSPCuotas = new JScrollPane();
			jSPCuotas.setViewportView(getJTblCuotas());
		}
		return jSPCuotas;
	}

	@SuppressWarnings("serial")
	private JTable getJTblCuotas() {
		if (jTblCuotas == null) {
			jTblCuotas = new JTable();
			jTblCuotas.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "C�digo", "Fecha de pago", "Importe", "Nro. de cuota", "Fecha de vencimiento", "Estado" }) {
				boolean[] columnEditables = new boolean[] { false, true, true, true, true, true };

				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
			jTblCuotas.getColumnModel().getColumn(0).setPreferredWidth(50);
			jTblCuotas.getColumnModel().getColumn(1).setPreferredWidth(108);
			jTblCuotas.getColumnModel().getColumn(2).setPreferredWidth(103);
			jTblCuotas.getColumnModel().getColumn(3).setPreferredWidth(100);
			jTblCuotas.getColumnModel().getColumn(4).setPreferredWidth(116);
			jTblCuotas.getColumnModel().getColumn(5).setPreferredWidth(49);
		}
		return jTblCuotas;
	}

	private void poblarListas() throws Exception {
		
//		// clientesNA = new ArrayList<String>();
//		clientesNCed = new ArrayList<String>();
//		List<Cliente> listacli = controladorPagoCuota.getClientes(JFramePanambiMain.session.getConn());
//		for (Cliente cli : listacli) {
//			// clientesNA.add(cli.getApellidos()+" "+cli.getNombres());
//			clientesNCed.add(cli.getNroDocumento());
//		}

		nrosFacturas = new ArrayList<Integer>();
		List<Venta> listaven = controladorPagoCuota.getVentas(JFramePanambiMain.session.getConn());
		for (Venta ven : listaven) {
			nrosFacturas.add(ven.getNroComprobante());
		}
	}

	private void doLimpiar() {
		try {
			poblarListas();
			getJTxtCliente().setText("");
			getJTxtNroCedula().setText("");
			getJTxtCodCliente().setValor(null);
			getJTxtNroFactura().setValor(null);
			getJTxtNroCedula().setEnabled(true);
			getJTxtNroFactura().setEnabled(true);
			getJTblVentas().setEnabled(true);
			getBtnpnmbExaminar().setEnabled(true);
			
			dtmVenta = new DefaultTableModel(new Object[][] {}, new String[] { "Codigo", "Fecha", "Total", "Sucursal" });
			getJTblVentas().setModel(dtmVenta);
			getJTblVentas().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			dtmPlanPago = new DefaultTableModel(new Object[][] {}, new String[] { "C�digo", "Fecha de pago", "Importe", "Nro. de cuota", "Fecha de vencimiento", "Estado" });
			getJTblCuotas().setModel(dtmPlanPago);
			getJTblCuotas().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.WARNING_MESSAGE);
		}
	}

	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					doLimpiar();
					
					dtmCuota = new DefaultTableModel(new Object[][] {}, new String[] { "C�digo de cuota", "Descripci�n de couta", "Importe" });
					jTblPagoCuota.setModel(dtmCuota);
					jTblPagoCuota.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					
					getJTxtNroCedula().requestFocus();
					
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Datos del cliente");
			panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(64)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(18)
								.addComponent(getLblpnmbNroCedula(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
								.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, 263, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblpnmbNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getBtnpnmbExaminar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(32, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}

	private JLabelPanambi getLblpnmbNroCedula() {
		if (lblpnmbNroCedula == null) {
			lblpnmbNroCedula = new JLabelPanambi();
			lblpnmbNroCedula.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroCedula.setText("Nro. Cedula : ");
		}
		return lblpnmbNroCedula;
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Cliente) {
			this.cliente = (Cliente) obj;
			//doLimpiar();
			getJTxtCodCliente().setValor(cliente.getCodCliente());
			getJTxtNroCedula().setText(cliente.getNroDocumento());
			// getJCmbClientes().setSelectedItem(cliente.getApellidos()+" "+cliente.getNombres());
			getJTxtCliente().setText(cliente.getNombres() + " " + cliente.getApellidos());
			//getJTxtNroFactura().requestFocus();
			
			//**cargando tabla ventas.
			cargarTablaVentas(cliente);
			//**seleccionamos primera venta.
			seleccionarPrimeraVenta();
			//**cargando cuotas de la primera venta
			doCargarPlanPago(venta.getCodVenta());
			
			getJTxtNroFactura().requestFocus();
			
		}
		
		if (obj instanceof Venta) {
			
			this.venta = (Venta) obj;
			this.cliente = venta.getCliente();
			cargarCliente(cliente);
			//setValues(cliente,null);
		
			if(getJTblVentas().getRowCount()==0){
				agregarVenta(venta);
			}else{
				//**Verifica si la factura se encuentra entre los registros de la tabla ventas.
				boolean facturaEnLista = false;
				for(int i = 0 ; i < getJTblVentas().getRowCount();i++){
					if(getJTblVentas().getValueAt(i, 0)==venta.getCodVenta()){
						facturaEnLista = true;
						i= getJTblVentas().getRowCount()+1;
					}
				}
				
				if(facturaEnLista){
					seleccionarVenta(venta);
				}else{
					agregarVenta(venta);
				}
				
			}
			//se saco lo que era el prcedimiento de agregar una venta
			
			
//			getJCmbNroCedula().setSelectedItem(ven.getCliente().getNroDocumento());
//			getJTxtCliente().setText(cliente.getNombres() + " " + cliente.getApellidos());
		}
		
		if (obj instanceof String) {
			doGenerarRecibo((String)obj);
		}

	}
	
	private void cargarCliente (Cliente cli){
		getJTxtCodCliente().setValor(cli.getCodCliente());
		getJTxtNroCedula().setText(cli.getNroDocumento());
		getJTxtCliente().setText(cli.getNombres()+" "+cli.getApellidos());
	}
	
	@SuppressWarnings("serial")
	private void agregarVenta(Venta ven){
		
		dtmVenta = new DefaultTableModel() {
			public boolean isCellEditable(int reg, int column) {
				return false;
			}
		};

		Object[] cabecera = { "C�digo", "Fecha", "Monto total", "Sucursal" };
		dtmVenta.setColumnIdentifiers(cabecera);
		Object[] row = { ven.getCodVenta(), ven.getFecha(), ven.getTotal(), ven.getSucursal() };
		dtmVenta.addRow(row);

		getJTblVentas().setModel(dtmVenta);
		doCargarPlanPago(ven.getCodVenta());
		
	}
	
	private void seleccionarVenta(Venta ven){
		for (int i=0 ; i<getJTblVentas().getRowCount();i++){	
			if(getJTblVentas().getValueAt(i, 0)==ven.getCodVenta()){
				getJTblVentas().changeSelection(i, 0, false, false);
			};
		}
	}
	
	
//	public void setValuesVentas(Object obj, Integer source) {
//		
//
//	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Cliente) {
			this.cliente = (Cliente) obj;
			getJTxtCodCliente().requestFocus();
		}
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}

	private void doSalir() {
		this.dispose();
	}

	private JPanel getJPnlVenta() {
		if (jPnlVenta == null) {
			jPnlVenta = new JPanel();
			jPnlVenta.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Ventas a Cuotas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_jPnlVenta = new GroupLayout(jPnlVenta);
			gl_jPnlVenta.setHorizontalGroup(
				gl_jPnlVenta.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlVenta.createSequentialGroup()
						.addGroup(gl_jPnlVenta.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPnlVenta.createSequentialGroup()
								.addGap(120)
								.addComponent(getLblFacturaNro(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jPnlVenta.createSequentialGroup()
								.addContainerGap()
								.addComponent(getScrollPane_1(), GroupLayout.PREFERRED_SIZE, 498, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(20, Short.MAX_VALUE))
			);
			gl_jPnlVenta.setVerticalGroup(
				gl_jPnlVenta.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlVenta.createSequentialGroup()
						.addGroup(gl_jPnlVenta.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPnlVenta.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblFacturaNro(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getScrollPane_1(), GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			jPnlVenta.setLayout(gl_jPnlVenta);
		}
		return jPnlVenta;
	}

	private JPanel getJPnlCuotas() {
		if (jPnlCuotas == null) {
			jPnlCuotas = new JPanel();
			jPnlCuotas.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Cuotas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_jPnlCuotas = new GroupLayout(jPnlCuotas);
			gl_jPnlCuotas.setHorizontalGroup(
				gl_jPnlCuotas.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, gl_jPnlCuotas.createSequentialGroup()
						.addContainerGap()
						.addComponent(getScrollPane_2(), GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
						.addContainerGap())
			);
			gl_jPnlCuotas.setVerticalGroup(
				gl_jPnlCuotas.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPnlCuotas.createSequentialGroup()
						.addGap(1)
						.addComponent(getScrollPane_2(), GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
						.addContainerGap())
			);
			jPnlCuotas.setLayout(gl_jPnlCuotas);
		}
		return jPnlCuotas;
	}

	private JLabel getLblFacturaNro() {
		if (lblFacturaNro == null) {
			lblFacturaNro = new JLabel("Factura Nro : ");
			lblFacturaNro.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblFacturaNro;
	}

	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Cuotas a Pagar", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(4)
						.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
						.addContainerGap())
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
						.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
						.addContainerGap())
			);
			panel_1.setLayout(gl_panel_1);
		}
		return panel_1;
	}

	private JButtonPanambi getButtonPanambi() {
		if (buttonPanambi == null) {
			buttonPanambi = new JButtonPanambi();
			buttonPanambi.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						doAgregarPagoCuota();
					} catch (Exception e) {
						DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
			buttonPanambi.setText(">>");
		}
		return buttonPanambi;
	}

	private void doAgregarPagoCuota() throws Exception {
		try {
			Integer rowSelect = jTblCuotas.getSelectedRow();
			Integer cuotaCod = null;
			PlanPago cuota = null;
			Integer nextCuo = null;

			if (rowSelect < 0) {
				Toolkit.getDefaultToolkit().beep();
			} else {
				cuotaCod = (Integer) jTblCuotas.getValueAt(rowSelect, 0);
				cuota = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), cuotaCod);
				nextCuo = controladorPagoCuota.ultimoPagado(JFramePanambiMain.session.getConn(), cuota);
				PlanPago cuotaTemp = null;
				
				Venta facturaActual = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				Venta facturaAnterior = null;
				if(getJTblPagoCuota().getRowCount()>0){
					cuotaTemp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 0));
					facturaAnterior= controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), cuotaTemp.getCodVenta());
				}
				
				Cliente clienteActual = controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), cuota.getCodVenta()).getCliente();
				Cliente clienteAnterior = null;
				if(getJTblPagoCuota().getRowCount()>0){
					cuotaTemp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 0));
					clienteAnterior = controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), cuotaTemp.getCodVenta()).getCliente();
				}
				
				if (controladorPagoCuota.yaPagado(JFramePanambiMain.session.getConn(), cuotaCod)) {
					DlgMessage.showMessage(getOwner(), "Couta ya pagada. \n No puede agregar cuotas que ya se encuentran pagadas. ", DlgMessage.WARNING_MESSAGE);
				} else if (existe(cuotaCod)) {
					DlgMessage.showMessage(getOwner(), "Couta ya agregada. \n No puede agregar mas de una vez una misma cuota. ", DlgMessage.WARNING_MESSAGE);
				//***************************
				} else if (jTblPagoCuota.getRowCount()>0 && clienteAnterior !=null && clienteActual.getCodCliente() != clienteAnterior.getCodCliente()){ 
						
//						controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), cuota.getCodVenta()).getCliente().getCodCliente() !=
//						controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), cuotaTemp.getCodVenta()).getCliente().getCodCliente()){
					
					//sd
					DlgMessage.showMessage(getOwner(), "Imposible agregar cuota. \n Solo puede agregar una cuotas de un mismo cliente.", DlgMessage.WARNING_MESSAGE);
					//getJTxtNroCedula().requestFocus();
					setValues(clienteAnterior, null);
					getJTxtNroCedula().requestFocus();
						
				} else if (jTblPagoCuota.getRowCount()>0 && facturaAnterior !=null && facturaActual.getCodVenta()!= facturaAnterior.getCodVenta()){
					DlgMessage.showMessage(getOwner(), "Imposible agregar cuota. \n Solo puede agregar una cuotas de una misma factura.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().setValor(facturaAnterior.getNroComprobante());
					lostFocusNroFactura();
					getJTxtNroFactura().requestFocus();
				} else if (cuotaCod != nextCuo && !cuotaAnterior(cuotaCod)) {
					DlgMessage.showMessage(getOwner(), "Couta pendiente por agregar. \n Favor verifiuque cuota faltante y agregela.", DlgMessage.WARNING_MESSAGE);

				} else {
					Object[] row = { cuota.getCodPlanPago(), /*cuota.getCodVenta(),*/ cuota.getNroCuota()+" / "+controladorPagoCuota.getCantidadCuotas(JFramePanambiMain.session.getConn(),cuota.getCodVenta()),/*cuota.getFechaPago(),*/ cuota.getMonto() };
					dtmCuota.addRow(row);
					getJTblPagoCuota().setModel(dtmCuota);
					getJTxtNroCedula().setEnabled(false);
					getJTxtNroFactura().setEnabled(false);
					getJTblVentas().setEnabled(false);
					getBtnpnmbExaminar().setEnabled(false);
					//doChangePagoCuota();
				}
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	/**
	 * Verifica si se encuentra agregada la cuota anterior inmediata.
	 * 
	 * @param codCuota
	 * @return
	 */
	private boolean cuotaAnterior(Integer codCuota) {
		boolean anteriorAgregado = false;
		Integer codAnterior = codCuota - 1;
		for (int i = 0; i < dtmCuota.getRowCount(); i++) {

			if (dtmCuota.getValueAt(i, 0) == codAnterior) {
				anteriorAgregado = true;
			}
		}
		return anteriorAgregado;
	}

	private boolean existe(Integer cod) {
		boolean ex = false;
		for (int i = 0; i < dtmCuota.getRowCount(); i++) {
			if (dtmCuota.getValueAt(i, 0) == cod) {
				ex = true;
			}
		}
		return ex;
	}

	private JButtonPanambi getBtnpnmbX() {
		if (btnpnmbX == null) {
			btnpnmbX = new JButtonPanambi();
			btnpnmbX.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doQuitarElemento();
				}
			});
			btnpnmbX.setText("X");
		}
		return btnpnmbX;
	}

	private void doQuitarElemento() {
		try {
			Integer rowSelect = jTblPagoCuota.getSelectedRow();

			if (rowSelect < 0) {
				Toolkit.getDefaultToolkit().beep();

			} else {
				// si el elemento seleccionado no es la cuota mayor de un plan
				// de pagos debe dar error.
				boolean isMayorCuota = true;
				if (dtmCuota.getRowCount() == 1) {
					isMayorCuota = true;
				} else {
					PlanPago cuotaSelect = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer) dtmCuota.getValueAt(rowSelect, 0));
					//
					for (int i = 0; i < dtmCuota.getRowCount(); i++) {
						PlanPago pp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer) dtmCuota.getValueAt(i, 0));
						if (cuotaSelect.getCodVenta() == pp.getCodVenta() && cuotaSelect.getCodPlanPago() < pp.getCodPlanPago()) {
							isMayorCuota = false;

						}
					}
				}

				if (isMayorCuota) {
					dtmCuota.removeRow(rowSelect);
					getJTblPagoCuota().setModel(dtmCuota);
					if(getJTblPagoCuota().getRowCount()==0){
						getJTxtNroCedula().setEnabled(true);
						getJTxtNroFactura().setEnabled(true);
						getJTblVentas().setEnabled(true);
						getBtnpnmbExaminar().setEnabled(true);
					}
					//doChangePagoCuota();
				} else {
					DlgMessage.showMessage(getOwner(), "Solo puede eliminar la mayor cuota agregada a la lista de cada venta", DlgMessage.ERROR_MESSAGE);
				}

			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblPagoCuota());
		}
		return scrollPane;
	}

	@SuppressWarnings("serial")
	private JTable getJTblPagoCuota() {
		if (jTblPagoCuota == null) {
			jTblPagoCuota = new JTable();
			
			dtmCuota = new DefaultTableModel(new Object[][] {}, new String[] { "C�digo de cuota", "Descripci�n de couta", "Importe" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					// all cells false
					return false;
				}
			};
			
			jTblPagoCuota.setModel(dtmCuota);
			jTblPagoCuota.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			anadeListenerAlModelo(jTblPagoCuota);

		}
		return jTblPagoCuota;
	}

    
	
	/**
     * Se a�ade el listener al model
     */
    private void anadeListenerAlModelo(JTable tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent evento) {
            	//doCheckDevolucion();
            	doChangePagoCuota();
            }
            
        });
    
    }
    
    private void doChangePagoCuota(){
    	if(getJTblPagoCuota().getRowCount()==0){
    		getJTxtTotal().setValue(0.0);
    	}else{
    		Double montoAcumulado = 0.0;
    		for(int i = 0;i<getJTblPagoCuota().getRowCount();i++){
    			montoAcumulado+=(Double)getJTblPagoCuota().getValueAt(i, 2);
    		}
    		getJTxtTotal().setValue(montoAcumulado);
    	}
    }
	
	
	private void decorate() {
	}

	private JButtonPanambi getBtnpnmbGenerarPago() {
		if (btnpnmbGenerarPago == null) {
			btnpnmbGenerarPago = new JButtonPanambi();
			btnpnmbGenerarPago.setMnemonic('G');
			btnpnmbGenerarPago.setToolTipText("Generar pago de cuota");
			btnpnmbGenerarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						doGenerarPago();
					} catch (Exception ex) {
						DlgMessage.showMessage(getOwner(), ex, DlgMessage.ERROR_MESSAGE);
					}
				}
			});
			btnpnmbGenerarPago.setText("Generar Pago");
		}
		return btnpnmbGenerarPago;
	}

	@SuppressWarnings("serial")
	private void doGenerarPago() throws Exception {

		if (dtmCuota.getRowCount() == 0) {
			DlgMessage.showMessage(getOwner(), "Debe ingresar cuotas en el cuadro 'Cuotas a Pagar' para poder efectuar un pago.", DlgMessage.WARNING_MESSAGE);
		} else {
			//**modelo de tabla a pasar al DLG pago.
			DefaultTableModel dtm = new DefaultTableModel() {
				public boolean isCellEditable(int reg, int column) {
					return false;
				}
			};

			String[] cabecera = { "C�digo de cuota", "N�mero de facturaci�n", "Descripci�n de la cuota", "Importe" };
			dtm.setColumnIdentifiers(cabecera);

			PlanPago pp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)dtmCuota.getValueAt(0, 0));
			Venta ven = controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), pp.getCodVenta());
			
			for (int i = 0; i < dtmCuota.getRowCount(); i++) {

				Object[] row = { dtmCuota.getValueAt(i, 0), ven.getNroComprobante(),
						 dtmCuota.getValueAt(i, 1),
						dtmCuota.getValueAt(i, 2) };
				dtm.addRow(row);
			}
			
//				PlanPago ppTemp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 0));
//				setValues(controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(), ppTemp.getCodVenta()), null);
			
			DlgPagoCuota dlgPagoCuota = new DlgPagoCuota(dtm, cliente, this);
			dlgPagoCuota.setVisible(true);
			dlgPagoCuota.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					actualizarDatos();
				}
			});
			
		}

	}
	
	private void doGenerarRecibo(String conceptoRecibo) {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularProductoPerdido.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", (Double)getJTxtTotal().getValue());
			
//			String nombreRecibo;
//			nombreRecibo = clientes.get(0).getNombres()+" "+clientes.get(0).getApellidos();
//			if(clientes.size()>1){
//				for(int i = 1 ; i<clientes.size();i++){
//					nombreRecibo+= " Y "+clientes.get(i).getNombres()+" "+clientes.get(i).getApellidos();
//				}
//			}		
//			
			
//			String pagante = JOptionPane.showInputDialog("Ingrese el nombre y apellido de la persona a cargo del pago", "");
//			if(pagante.length()==0){
//				pagante = "............................................................................................................";
//			}
			
			parameters.put("CLIENTE", cliente.getNombres()+" "+cliente.getApellidos());
			
			parameters.put("DESCRIPCION", conceptoRecibo);
			parameters.put("MONTOLETRAS", "GUARAN�ES "+NumerosALetras.getLetras(10.0));//(Double)getJTxtTotal().getValue()));
			
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;
			Integer numeroRecibo =  new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn());
			//JOptionPane.showMessageDialog(null, numeroRecibo);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceReciboPagoCuota(JFramePanambiMain.session.getConn(),numeroRecibo));
			JasperViewer.viewReport(jasperPrint, false);
			
			List<?> pages = jasperPrint.getPages();
			this.dispose();
			if (!pages.isEmpty()) {
				
				JasperViewer.viewReport(jasperPrint, false);
				
				//new ControladorSecuencias().aumentarNroRecibo(JFramePanambiMain.session.getConn());
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
			
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
	
	}
	
	
	@SuppressWarnings("serial")
	private void actualizarDatos() {

		PlanPago planp;
		List<PlanPago> planesPagos = new ArrayList<PlanPago>();
		try {
			if(getJTblCuotas().getRowCount()>0){
				planp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer) dtmPlanPago.getValueAt(0, 0));
				planesPagos = controladorPagoCuota.getPlanesPagos(JFramePanambiMain.session.getConn(), planp.getCodVenta());
				dtmPlanPago = new DefaultTableModel() {
					public boolean isCellEditable(int reg, int column) {
						return false;
					}
				};
	
				Object[] cabeceraPlanPago = { "C�digo", "Fecha de pago", "Importe", "Nro. de cuota", "Fecha de Vencimiento", "Estado" };
				dtmPlanPago.setColumnIdentifiers(cabeceraPlanPago);
				Iterator<PlanPago> iteratorPlanPago = planesPagos.listIterator();
	
				while (iteratorPlanPago.hasNext()) {
	
					PlanPago pp = (PlanPago) iteratorPlanPago.next();
					String estado = "";
					if(pp.getEstado().equals("P")){
						estado = "Pagado";
					}else estado = "Debe";
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					String fecha = null;
					String fechaVencimiento = null;
					if( pp.getFechaPago()!=null){
						fecha = formato.format(pp.getFechaPago());
					}
					if( pp.getVencimiento()!=null){
						fechaVencimiento = formato.format(pp.getVencimiento());
					}
					Object[] row = { pp.getCodPlanPago(), fecha, pp.getMonto(), pp.getNroCuota(), fechaVencimiento, estado };
					dtmPlanPago.addRow(row);
				}
	
				getJTblCuotas().setModel(dtmPlanPago);
	
				for (int i = 0; i < dtmCuota.getRowCount(); i++) {
	
					PlanPago pp = controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer) dtmCuota.getValueAt(i, 0));
					if (pp.getEstado().equals("P")) {
						dtmCuota.removeRow(i);
	
					}
					if(getJTblPagoCuota().getRowCount()==0){
						getJTxtNroCedula().setEnabled(true);
						getJTxtNroFactura().setEnabled(true);
						getJTblVentas().setEnabled(true);
						getBtnpnmbExaminar().setEnabled(true);
					}
				}
	
				getJTblPagoCuota().setModel(dtmCuota);
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	
	private void lostFocusNroFactura() {

		try {
				Venta ven = controladorVenta.getVentaCreditoPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				//JOptionPane.showMessageDialog(null, getJTxtNroFactura().getValor());
				//Venta ven = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());//nroFactura);
				
				
				if(getJTblPagoCuota().getRowCount()>0){
					PlanPago cuotaAnterior= controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 0));
					Venta ventaAnterior= controladorVenta.getVenta(JFramePanambiMain.session.getConn(), cuotaAnterior.getCodVenta());
					if(ven!=null){
						if(ventaAnterior.getCodVenta()!=ven.getCodVenta()){
							throw new ValidException("Error en la selecci�n de la factura\nNo puede seleccionar cuotas de facturas diferentes.");
						}
					}else{
						this.venta = ventaAnterior;
					}
					
				}
				
				if (ven!= null) {
					if(ven.getEstado().equals("A")){
						if(controladorPagoCuota.getCantidadCuotasPendientes(JFramePanambiMain.session.getConn(), ven)!=0){
							setValues(ven, null);
						}else{
							DlgMessage.showMessage(getOwner(), "No existen cuotas pendientes por pagar para al factura.", DlgMessage.WARNING_MESSAGE);
							doLimpiar();
							getJTxtNroFactura().requestFocus();
						}
									
					}else{
						DlgMessage.showMessage(getOwner(), "La factura se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
						doLimpiar();
						getJTxtNroFactura().requestFocus();
					}
				}else{
					doLimpiar();
					getJTxtNroCedula().requestFocus();
				}
				
		}catch (NullPointerException ne){
			getJTxtNroCedula().requestFocus();
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	

	private JTextFieldUpper getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JTextFieldUpper();
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setEditable(false);
			jTxtCliente.setFocusable(false);
		}
		return jTxtCliente;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
			
			
		}
		return jTxtNroFactura;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setFont(new Font("Dialog", Font.BOLD, 14));
			lblpnmbTotal.setText("Total ");
		}
		return lblpnmbTotal;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setEditable(false);
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setForeground(Color.RED);
			jTxtTotal.setFont(new Font("Dialog", Font.BOLD, 14));
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodCliente() {
		if (jTxtCodCliente == null) {
			jTxtCodCliente = new JTextFieldInteger();
			jTxtCodCliente.setEditable(false);
			jTxtCodCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodCliente.setFocusable(false);
		}
		return jTxtCodCliente;
	}
	private JTextFieldUpper getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextFieldUpper();
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusNroCedula();
				}
			});
		}
		return jTxtNroCedula;
	}
	
	private void lostFocusNroCedula() {
		try {
			String nroced = getJTxtNroCedula().getText();
			Cliente cli = controladorPagoCuota.getClienteConCuotasActivas(JFramePanambiMain.session.getConn(), nroced);
			if(getJTblPagoCuota().getRowCount()>0){
				PlanPago cuotaAnterior= controladorPagoCuota.getCuota(JFramePanambiMain.session.getConn(), (Integer)getJTblPagoCuota().getValueAt(0, 0));
				Cliente clienteAnterior = (controladorVenta.getVenta(JFramePanambiMain.session.getConn(), cuotaAnterior.getCodVenta())).getCliente();
				if(clienteAnterior!=cli){
						throw new ValidException("Error en la selecci�n del cliente\nNo puede seleccionar cuotas de clientes diferentes.");
				}
			}
			
			if(getJTxtNroCedula().getText().length()!=0 && cli == null){
				doBuscarCliente();

			}else{
				if (cli != null) {
					doLimpiar();
					setValues(cli, null);
					
//					dtmPlanPago = new DefaultTableModel(new Object[][] {}, new String[] { "C�digo", "Fecha de pago", "Importe", "Nro. de cuota", "Fecha de vencimiento", "Estado" });
//					getJTblCuotas().setModel(dtmPlanPago);
//					getJTblCuotas().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					
//					dtmCuota = new DefaultTableModel(new Object[][] {}, new String[] { "C�digo de cuota", "Descripci�n de cuota", "Importe" }) {
//						@Override
//						public boolean isCellEditable(int row, int column) {
//							// all cells false
//							return false;
//						}
//					};
//					getJTblPagoCuota().setModel(dtmCuota);
//					getJTblPagoCuota().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				} else {
					doLimpiar();
					//getJTxtNroFactura().requestFocus();
				}

			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	@SuppressWarnings("serial")
	private void cargarTablaVentas(Cliente cli){
		try{
			//***cargando tabla de ventas
			ArrayList<Venta> ventasAux = controladorPagoCuota.getVentasActivas(JFramePanambiMain.session.getConn(), cli);
			dtmVenta = new DefaultTableModel() {
				public boolean isCellEditable(int reg, int column) {
					return false;
				}
			};
			Object[] cabecera = { "C�digo", "Fecha", "Total", "Sucursal" };
			dtmVenta.setColumnIdentifiers(cabecera);
			List<Venta> ventas = new ArrayList<Venta>();
			for (int i=0; i < ventasAux.size();i++){
				List<PlanPago> cuotas = new ArrayList<PlanPago>();
				cuotas = controladorPagoCuota.getPlanesPagos(JFramePanambiMain.session.getConn(), ventasAux.get(i).getCodVenta());
				boolean pendientes = false;
				for(int j = 0;j<cuotas.size();j++){
					if(cuotas.get(j).getEstado().equals("D")){
						pendientes = true;
					}
				}
				
				if(pendientes){
					ventas.add(ventasAux.get(i));
				}
			}
			
			Iterator<Venta> iterator = ventas.listIterator();
			
			 
			while (iterator.hasNext()) {
				Venta v = (Venta) iterator.next();
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				String fecha = null;
				if(v.getFecha()!=null){
					fecha = formato.format(v.getFecha());
				}
				Object[] row = { v.getCodVenta(), fecha, v.getTotal(), v.getSucursal() };
				dtmVenta.addRow(row);	
			}
			getJTblVentas().setModel(dtmVenta);
			getJTblVentas().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		

		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void seleccionarPrimeraVenta(){
		try{
			//**seleccionando el primer registro de las ventas.
			getJTblVentas().changeSelection(0, 0, false, false);
			venta = controladorVenta.getVentaPorCodVenta(JFramePanambiMain.session.getConn(),(Integer)getJTblVentas().getValueAt(0, 0));
			getJTxtNroFactura().setValor((Integer)venta.getNroComprobante());
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}

	private void doBuscarCliente(){
		try {
			String[] columnNames = { "C�digo", "Nro. Documento", "Nombres","Apellidos" };
			String[] pks = { "codCliente" };
			String sSelect = "SELECT DISTINCT c.codcliente, c.nrodocumento, c.nombres, c.apellidos ";
			sSelect += "FROM clientes c ";
			sSelect += "JOIN ventas v ";
			sSelect += "ON c.codcliente = v.codcliente ";
			sSelect += "WHERE v.tipoventa = 'C' ";
			sSelect += "AND v.estado = 'A' ";
			sSelect += "AND c.estado = 'A' ";
			sSelect += " AND 0 NOT IN (SELECT count(*) FROM planespagos pp ";
			sSelect += " WHERE pp.estado = 'D' ";
			sSelect += " AND pp.codventa = v.codventa) ";
			sSelect += "ORDER BY nombres";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Cliente", pks);
			jb.setVisible(true);
			//getJTxtNroCedula(). requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	@Override
	public void doRecibo(boolean generar, String titular, String concepto, String montoLetras, Double monto, Integer nrorecibo, Double vuelto) {
		// TODO Auto-generated method stub
		
		if(generar ==true){
			try{
			PanambiUtils.setWaitCursor(getOwner());
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularProductoPerdido.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", monto);
		
			parameters.put("CLIENTE", titular);

			parameters.put("DESCRIPCION", concepto);
			parameters.put("MONTOLETRAS", montoLetras);
			
			//Integer nroRecibo = new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn());
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
	
			JasperPrint jasperPrint;
	
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceReciboPagoCuota(JFramePanambiMain.session.getConn(),nrorecibo));
		
			List<?> pages = jasperPrint.getPages();
		
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
//			
//			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
		}
		
	}
	private JButtonPanambi getBtnpnmbExaminar() {
		if (btnpnmbExaminar == null) {
			btnpnmbExaminar = new JButtonPanambi();
			btnpnmbExaminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doBuscarCliente();
				}
			});
			btnpnmbExaminar.setMnemonic('E');
			btnpnmbExaminar.setText("Examinar ...");
		}
		return btnpnmbExaminar;
	}
}
