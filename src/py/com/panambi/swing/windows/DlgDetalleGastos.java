package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Gasto;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;

public class DlgDetalleGastos extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8670374730195306998L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetalleDeGasto;
	private JPanel panel_1;
	private JLabelPanambi lblpnmbCdigo;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbFechaDePago;
	private JLabelPanambi lblpnmbConcepto;
	private JLabelPanambi lblpnmbImporte;
	private JLabelPanambi lblpnmbVencimiento;
	private JLabelPanambi lblpnmbComprobante;
	private JLabelPanambi lblpnmbFechaAnulacion;
	private JTextFieldInteger jTxtCodigo;
	private JTextField jTxtConcepto;
	private JTextFieldDouble jTxtImporte;
	private JTextField jTxtComprobante;
	private JTextField jTxtFechaPago;
	private JTextField jTxtEstado;
	private JTextField jTxtVencimiento;
	private JTextField jTxtFechaAnulacion;
	private JLabelPanambi lblpnmbComentarios;
	private JTextAreaUpper jTxtComentarios;
	private JPanel panel_2;
	private JButtonPanambi jBtnAceptar;
	private Gasto gasto;
	
	
	public DlgDetalleGastos(Gasto gasto) {
		setResizable(false);
		setLocationRelativeTo(getParent());
		this.gasto = gasto;
		initialize();
		
		
	}
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetalleGastos.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 525, 408);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		getContentPane().add(getPanel_1());
		getContentPane().add(getPanel_2());
		cargarDatos();
		setListeners(getRootPane());
		getJBtnAceptar().requestFocus();
		
	}
	
	private void cargarDatos(){
		getJTxtCodigo().setValor(gasto.getCodGasto());
		getJTxtImporte().setValue(gasto.getImporte());
		getJTxtComprobante().setText(gasto.getNroComprobante());
		String patronFechaPago = "dd/MM/yyyy hh:mm:ss";
		SimpleDateFormat formatoFechaPago = new SimpleDateFormat(patronFechaPago);
		getJTxtFechaPago().setText(formatoFechaPago.format(gasto.getFechaPago()));
		getJTxtComentarios().setText(gasto.getComentarios());
		getJTxtConcepto().setText(gasto.getTipoGasto().getConcepto());
		
		if(gasto.getFechaVencimiento()!=null){
			String patronVencimiento  = "dd/MM/yyyy";
			SimpleDateFormat formatoVencimiento = new SimpleDateFormat(patronVencimiento);
			getJTxtVencimiento().setText(formatoVencimiento.format(gasto.getFechaVencimiento()));
		}
		
		if(gasto.getFechaAnulacion()!=null){
			String patronAnulacion  = "dd/MM/yyyy hh:mm:ss";
			SimpleDateFormat formatoAnulacion= new SimpleDateFormat(patronAnulacion);
			getJTxtFechaAnulacion().setText(formatoAnulacion.format(gasto.getFechaAnulacion()));
		}else{
			getLblpnmbFechaAnulacion().setVisible(false);
			getJTxtFechaAnulacion().setVisible(false);
		}
		
		if(gasto.getEstado().equals("A")){
			getJTxtEstado().setText("Activo");
		}else{
			getJTxtEstado().setText("Anulado");
		}
	}

	private void setListeners(JComponent component) {
		component.addKeyListener(new KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doAceptar();
				}

			}
		});
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setForeground(new Color(0, 0, 139));
			panel.setBackground(new Color(0,0,139));
			panel.setBounds(0, 0, 520, 34);
			panel.setLayout(null);
			panel.add(getLblpnmbDetalleDeGasto());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetalleDeGasto() {
		if (lblpnmbDetalleDeGasto == null) {
			lblpnmbDetalleDeGasto = new JLabelPanambi();
			lblpnmbDetalleDeGasto.setForeground(Color.WHITE);
			lblpnmbDetalleDeGasto.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetalleDeGasto.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetalleDeGasto.setText("DETALLE DEL GASTO");
			lblpnmbDetalleDeGasto.setBounds(0, 0, 520, 34);
		}
		return lblpnmbDetalleDeGasto;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 33, 520, 309);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
								.addGap(75)
								.addComponent(getLblpnmbFechaDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtConcepto(), GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
								.addGap(40)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addComponent(getLblpnmbComentarios(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtComentarios(), GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbComprobante(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
										.addGap(10)
										.addComponent(getJTxtComprobante()))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbImporte(), GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
										.addGap(10)
										.addComponent(getJTxtImporte(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
								.addGap(43)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbVencimiento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getJTxtVencimiento(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))))))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(30)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCdigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbFechaDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtFechaPago(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
						.addGap(11)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbImporte(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtImporte(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbVencimiento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtVencimiento(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
						.addGap(11)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbComprobante(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtComprobante(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtComentarios(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLblpnmbComentarios(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			panel_1.setLayout(gl_panel_1);
		}
		return panel_1;
	}
	private JLabelPanambi getLblpnmbCdigo() {
		if (lblpnmbCdigo == null) {
			lblpnmbCdigo = new JLabelPanambi();
			lblpnmbCdigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCdigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCdigo;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbFechaDePago() {
		if (lblpnmbFechaDePago == null) {
			lblpnmbFechaDePago = new JLabelPanambi();
			lblpnmbFechaDePago.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDePago.setText("Fecha de pago : ");
		}
		return lblpnmbFechaDePago;
	}
	private JLabelPanambi getLblpnmbConcepto() {
		if (lblpnmbConcepto == null) {
			lblpnmbConcepto = new JLabelPanambi();
			lblpnmbConcepto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbConcepto.setText("Concepto : ");
		}
		return lblpnmbConcepto;
	}
	private JLabelPanambi getLblpnmbImporte() {
		if (lblpnmbImporte == null) {
			lblpnmbImporte = new JLabelPanambi();
			lblpnmbImporte.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbImporte.setText("Importe : ");
		}
		return lblpnmbImporte;
	}
	private JLabelPanambi getLblpnmbVencimiento() {
		if (lblpnmbVencimiento == null) {
			lblpnmbVencimiento = new JLabelPanambi();
			lblpnmbVencimiento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbVencimiento.setText("Vencimiento : ");
		}
		return lblpnmbVencimiento;
	}
	private JLabelPanambi getLblpnmbComprobante() {
		if (lblpnmbComprobante == null) {
			lblpnmbComprobante = new JLabelPanambi();
			lblpnmbComprobante.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbComprobante.setText("Comprobante : ");
		}
		return lblpnmbComprobante;
	}
	private JLabelPanambi getLblpnmbFechaAnulacion() {
		if (lblpnmbFechaAnulacion == null) {
			lblpnmbFechaAnulacion = new JLabelPanambi();
			lblpnmbFechaAnulacion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacion.setText("Fecha anulaci\u00F3n : ");
		}
		return lblpnmbFechaAnulacion;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtCodigo;
	}
	private JTextField getJTxtConcepto() {
		if (jTxtConcepto == null) {
			jTxtConcepto = new JTextField();
			jTxtConcepto.setFocusable(false);
			jTxtConcepto.setEditable(false);
			jTxtConcepto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtConcepto.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtConcepto.setColumns(10);
		}
		return jTxtConcepto;
	}
	private JTextFieldDouble getJTxtImporte() {
		if (jTxtImporte == null) {
			jTxtImporte = new JTextFieldDouble();
			jTxtImporte.setFocusable(false);
			jTxtImporte.setEditable(false);
			jTxtImporte.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtImporte;
	}
	private JTextField getJTxtComprobante() {
		if (jTxtComprobante == null) {
			jTxtComprobante = new JTextField();
			jTxtComprobante.setFocusable(false);
			jTxtComprobante.setEditable(false);
			jTxtComprobante.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtComprobante.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtComprobante.setColumns(10);
		}
		return jTxtComprobante;
	}
	private JTextField getJTxtFechaPago() {
		if (jTxtFechaPago == null) {
			jTxtFechaPago = new JTextField();
			jTxtFechaPago.setFocusable(false);
			jTxtFechaPago.setEditable(false);
			jTxtFechaPago.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaPago.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFechaPago.setColumns(10);
		}
		return jTxtFechaPago;
	}
	private JTextField getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextField();
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtEstado.setColumns(10);
		}
		return jTxtEstado;
	}
	private JTextField getJTxtVencimiento() {
		if (jTxtVencimiento == null) {
			jTxtVencimiento = new JTextField();
			jTxtVencimiento.setFocusable(false);
			jTxtVencimiento.setEditable(false);
			jTxtVencimiento.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtVencimiento.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtVencimiento.setColumns(10);
		}
		return jTxtVencimiento;
	}
	private JTextField getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JTextField();
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtFechaAnulacion.setColumns(10);
		}
		return jTxtFechaAnulacion;
	}
	private JLabelPanambi getLblpnmbComentarios() {
		if (lblpnmbComentarios == null) {
			lblpnmbComentarios = new JLabelPanambi();
			lblpnmbComentarios.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbComentarios.setText("Comentarios : ");
		}
		return lblpnmbComentarios;
	}
	private JTextAreaUpper getJTxtComentarios() {
		if (jTxtComentarios == null) {
			jTxtComentarios = new JTextAreaUpper();
			jTxtComentarios.setFocusable(false);
			jTxtComentarios.setEditable(false);
		}
		return jTxtComentarios;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 343, 520, 37);
			panel_2.add(getJBtnAceptar());
		}
		return panel_2;
	}
	private JButtonPanambi getJBtnAceptar() {
		if (jBtnAceptar == null) {
			jBtnAceptar = new JButtonPanambi();
			jBtnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			jBtnAceptar.setText("Aceptar");
			
		}
		return jBtnAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
}
