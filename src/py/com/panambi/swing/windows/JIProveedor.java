package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Proveedor;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;

public class JIProveedor extends JInternalFramePanambi implements Browseable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1222465303188085562L;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodProveedor;
	private JLabelPanambi lblpnmbRazonSocial;
	private JComboBoxPanambi jCmbRazonSocial;
	private JLabelPanambi lblpnmbTelefono2;
	private JLabelPanambi lblpnmbDireccion;
	private JLabelPanambi lblpnmbNroDocumento;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private ControladorProveedor controladorProveedor = new ControladorProveedor();
	private Proveedor proveedor;
	private List<String> proveedores = new ArrayList<String>();
	private JTextFieldUpper jTxtDireccion;
	private JTextFieldUpper jTxtNroDocumento;
	private JTextFieldUpper jTxtTelefono;
	private JCheckBox chckbxActivo;
	private JLabelPanambi labelPanambi_1;
	private JLabelPanambi labelPanambi_2;
	private JLabelPanambi labelPanambi_3;
	private JLabelPanambi lblpnmbPresioneFPara;

	/**
	 * Launch the application.
	 */
	public JIProveedor() throws Exception{
		super();
		initialize();
	}
	
	public void initialize(){
		setMaximizable(false);
		setTitle("Proveedor");
		setBounds(100, 100, 382, 424);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(220)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJTxtCodProveedor(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(93)
					.addComponent(getChckbxActivo()))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addComponent(getLblpnmbRazonSocial(), GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJCmbRazonSocial(), GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJTxtNroDocumento(), GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addComponent(getLblpnmbTelefono2(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJTxtTelefono(), GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(33)
					.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJTxtDireccion(), GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(5)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(5)
							.addComponent(getJTxtCodProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getChckbxActivo()))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbRazonSocial(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbRazonSocial(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbTelefono2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtTelefono(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtDireccion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout); 
		 setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbRazonSocial(), getJTxtNroDocumento(), 
				 getJTxtTelefono(), getJTxtDireccion(), getChckbxActivo(), getBtnpnmbLimpiar(), 
				 getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()})); 
		decorate();
		setShortcuts(this);
		setHelp("proveedores");
		doLimpiar();
		getJCmbRazonSocial().requestFocus();
		
	}

	@Override
	public void setValues(Object obj, Integer source) {
		
		if (obj instanceof Proveedor) {
			this.proveedor = (Proveedor) obj;
			getJTxtCodProveedor().setValor(proveedor.getCodProveedor());
			getJCmbRazonSocial().setSelectedItem(((Proveedor) (obj)).getRazonSocial());
			getJTxtTelefono().setText(proveedor.getTelefono());
			getJTxtDireccion().setText(proveedor.getDireccion());
			getJTxtNroDocumento().setText(proveedor.getNroDocumento());
			if(proveedor.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			}else chckbxActivo.setSelected(false);
			getBtnpnmbGrabar().setEnabled(true);
			getBtnpnmbEliminar().setEnabled(true);
			
		}
		
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodProveedor() {
		if (jTxtCodProveedor == null) {
			jTxtCodProveedor = new JTextFieldInteger();
			jTxtCodProveedor.setEnabled(false);
			jTxtCodProveedor.setEditable(false);
		}
		return jTxtCodProveedor;
	}
	private JLabelPanambi getLblpnmbRazonSocial() {
		if (lblpnmbRazonSocial == null) {
			lblpnmbRazonSocial = new JLabelPanambi();
			lblpnmbRazonSocial.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbRazonSocial.setText("Razon Social : ");
		}
		return lblpnmbRazonSocial;
	}
	
	private JLabelPanambi getLblpnmbTelefono2() {
		if (lblpnmbTelefono2 == null) {
			lblpnmbTelefono2 = new JLabelPanambi();
			lblpnmbTelefono2.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTelefono2.setText("Telefono : ");
		}
		return lblpnmbTelefono2;
	}
	private JLabelPanambi getLblpnmbDireccion() {
		if (lblpnmbDireccion == null) {
			lblpnmbDireccion = new JLabelPanambi();
			lblpnmbDireccion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDireccion.setText("Direccion : ");
		}
		return lblpnmbDireccion;
	}
	private JLabelPanambi getLblpnmbNroDocumento() {
		if (lblpnmbNroDocumento == null) {
			lblpnmbNroDocumento = new JLabelPanambi();
			lblpnmbNroDocumento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroDocumento.setText("Nro Documento : ");
		}
		return lblpnmbNroDocumento;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar registro");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar registro");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doEliminar() {
		
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la eliminaci�n permanente del proveedor? ", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				controladorProveedor.borrarProveedor(JFramePanambiMain.session.getConn(), proveedor);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbRazonSocial().requestFocus();
			}else {
				proveedor = null;
				getJCmbRazonSocial().requestFocus();
			}
		} catch (SQLException e) {
			if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
				DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
			} else {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doGrabar() {
	
		try {
			
			if (proveedor == null) {
				boolean camposCompletos = true;
				proveedor = new Proveedor();
				proveedor.setRazonSocial((String) getJCmbRazonSocial().getSelectedItem());
				proveedor.setNroDocumento(getJTxtNroDocumento().getText().trim());
				proveedor.setDireccion(getJTxtDireccion().getText().trim());
				proveedor.setTelefono(getJTxtTelefono().getText().trim());
				
				
				if(getChckbxActivo().isSelected()){
					proveedor.setEstado("A");
				}else proveedor.setEstado("I");

				if(proveedor.getRazonSocial().equals("")){
					camposCompletos = false;
				}
				if(proveedor.getNroDocumento().equals("")){
					camposCompletos = false;
				}
				if(proveedor.getTelefono().equals("")){
					camposCompletos = false;
				}
			
				if(camposCompletos){
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la creaci�n del nuevo proveedor ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(ret == JOptionPane.YES_OPTION){
						controladorProveedor.insertarProveedor(JFramePanambiMain.session.getConn(), proveedor);
						DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
						doLimpiar();
						getJCmbRazonSocial().requestFocus();
					}
					
				}else {
					DlgMessage.showMessage(getOwner(), "Debe completar todos los campos obligatorios", DlgMessage.WARNING_MESSAGE);
					proveedor = null;
					getJTxtNroDocumento().requestFocus();
				}
				
			} else {
				boolean camposCompletos = true;
					proveedor.setRazonSocial((String) getJCmbRazonSocial().getSelectedItem());
					proveedor.setNroDocumento(getJTxtNroDocumento().getText().trim());
					proveedor.setDireccion(getJTxtDireccion().getText().trim());
					proveedor.setTelefono(getJTxtTelefono().getText().trim());
					
					if(getChckbxActivo().isSelected()){
						proveedor.setEstado("A");
					}else proveedor.setEstado("I");
					

					if(proveedor.getRazonSocial().equals("")){
						camposCompletos = false;
					}
					if(proveedor.getNroDocumento().equals("")){
						camposCompletos = false;
					}
					if(proveedor.getTelefono().equals("")){
						camposCompletos = false;
					}
					
						if(camposCompletos){
							Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea modificar los datos del proveedor?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
							if(ret == JOptionPane.YES_OPTION){
								controladorProveedor.modificarProveedor(JFramePanambiMain.session.getConn(), proveedor);
								DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
								doLimpiar();
								getJCmbRazonSocial().requestFocus();
							}else{
								getJTxtNroDocumento().requestFocus();
								}
							
						}else{
							DlgMessage.showMessage(getOwner(), "Debe completar todos los campos obligatorios", DlgMessage.WARNING_MESSAGE);
							getJTxtNroDocumento().requestFocus();
						}
				}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar proveedor");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Razon Social", "Nro de Documento", "Direccion","Telefono","Estado" };
			String[] pks = { "codproveedor" };
			String sSelect = "SELECT codproveedor, razonsocial, nrodocumento, direccion, telefono, estado ";
			sSelect += "FROM proveedores ";
			sSelect += "ORDER BY razonsocial";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Proveedor", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbRazonSocial().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		
		try {
			
			proveedor = null;
			poblarLista();
			getJCmbRazonSocial().setModel(new ListComboBoxModel<String>(proveedores));
			getJCmbRazonSocial().setSelectedItem(null);
			getJTxtCodProveedor().setValor(null);
			getJTxtTelefono().setText(null);
			getJTxtDireccion().setText(null);
			getJTxtNroDocumento().setText(null);
			getChckbxActivo().setSelected(true);
			getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbEliminar().setEnabled(false);
			
		} catch (Exception e) {

			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarLista() throws Exception {
		proveedores = new ArrayList<String>();
		List<Proveedor> listaprov = controladorProveedor.getProveedores(JFramePanambiMain.session.getConn());
		for (Proveedor prov : listaprov) {
			proveedores.add(prov.getRazonSocial());
		}
	}
	private JTextFieldUpper getJTxtDireccion() {
		if (jTxtDireccion == null) {
			jTxtDireccion = new JTextFieldUpper();
		}
		return jTxtDireccion;
	}
	private JTextFieldUpper getJTxtNroDocumento() {
		if (jTxtNroDocumento == null) {
			jTxtNroDocumento = new JTextFieldUpper();
		}
		return jTxtNroDocumento;
	}
	private JTextFieldUpper getJTxtTelefono() {
		if (jTxtTelefono == null) {
			jTxtTelefono = new JTextFieldUpper();
		}
		return jTxtTelefono;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbRazonSocial() {
		if (jCmbRazonSocial == null) {
			jCmbRazonSocial = new JComboBoxPanambi();
			jCmbRazonSocial.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusRazonSocial();
				}
			});
			jCmbRazonSocial.setEditable(true);
			jCmbRazonSocial.setModel(new ListComboBoxModel<String>(proveedores));
			jCmbRazonSocial.setSelectedItem(null);
		}
		return jCmbRazonSocial;
	}
	
	private void lostFocusRazonSocial() {
		try {
			Proveedor prov = controladorProveedor.getProveedor(JFramePanambiMain.session.getConn(), (String) jCmbRazonSocial.getSelectedItem());
			if (prov != null) {
				setValues(prov, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	
	private void doNuevoFromLostFocus() {
		proveedor = null;
		getJTxtCodProveedor().setValor(null);
		getJTxtNroDocumento().setText(null);
		getJTxtTelefono().setText(null);
		getJTxtDireccion().setText(null);
		getChckbxActivo().setSelected(true);
		Integer longitud = 0;
		
		try{
			longitud = getJCmbRazonSocial().getSelectedItem().toString().length();
		}catch(Exception e){
			longitud = 0;
		}
		
		if(longitud!=0){
			getBtnpnmbGrabar().setEnabled(true);
		}else{
			getBtnpnmbGrabar().setEnabled(false);
		}
		getBtnpnmbEliminar().setEnabled(false);
	
		
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbRazonSocial());
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}
	private JLabelPanambi getLabelPanambi_2() {
		if (labelPanambi_2 == null) {
			labelPanambi_2 = new JLabelPanambi();
			labelPanambi_2.setText("*");
		}
		return labelPanambi_2;
	}
	private JLabelPanambi getLabelPanambi_3() {
		if (labelPanambi_3 == null) {
			labelPanambi_3 = new JLabelPanambi();
			labelPanambi_3.setText("*");
		}
		return labelPanambi_3;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
