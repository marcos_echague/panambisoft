package py.com.panambi.swing.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import py.com.panambi.bean.PlanPago;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;

public class JIVentasPrueba extends JInternalFramePanambi implements Browseable{
	private JLabelPanambi lblpnmbCuotas;
	private JLabelPanambi lblpnmbVencimiento;
	private JTextFieldDouble jTxtInteres;
	private JButtonPanambi btnpnmbOk;
	private JTextFieldDouble jTxtTotal;
	private ControladorPlanPago controladorPlanPago;
	private JComboBoxPanambi comboBoxPanambi;
	private JComboBoxPanambi comboBoxPanambi_1;
	private JPanel panel;
	private JRadioButton jRbFijoMora;
	private JRadioButton jRbFijoPorcentual;
	private JTextFieldDouble jTxtInteresFijoMora;
	private JButtonPanambi jBtnSimular;
	private JPanel panel_1;
	private JTextFieldDouble jTxtInteresPorcentual;
	private JRadioButton jRbInteresFijo;
	private JRadioButton jRbInteresPorcentual;
	private JTextFieldDouble jTxtInteresFijo;
	private JLabelPanambi lblpnmbMontoDeVenta;
	private JTable table;
	
	public JIVentasPrueba() {
		initialize();
	}
	private void initialize() {
		getJPanelCentral().setLayout(null);
		getJPanelCentral().add(getLblpnmbCuotas());
		getJPanelCentral().add(getLblpnmbVencimiento());
		getJPanelCentral().add(getJTxtTotal());
		getJPanelCentral().add(getComboBoxPanambi());
		getJPanelCentral().add(getComboBoxPanambi_1());
		getJPanelCentral().add(getPanel());
		GroupLayout groupLayout = new GroupLayout(getJPanelSouth());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getBtnpnmbOk(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(629, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getBtnpnmbOk(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		getJPanelSouth().setLayout(groupLayout);
		getJPanelCentral().add(getJBtnSimular());
		getJPanelCentral().add(getPanel_1());
		getJPanelCentral().add(getLblpnmbMontoDeVenta());
		getJPanelCentral().add(getTable());
	}
	private JLabelPanambi getLblpnmbCuotas() {
		if (lblpnmbCuotas == null) {
			lblpnmbCuotas = new JLabelPanambi();
			lblpnmbCuotas.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCuotas.setText("Cuotas : ");
			lblpnmbCuotas.setBounds(48, 53, 54, 20);
		}
		return lblpnmbCuotas;
	}
	private JLabelPanambi getLblpnmbVencimiento() {
		if (lblpnmbVencimiento == null) {
			lblpnmbVencimiento = new JLabelPanambi();
			lblpnmbVencimiento.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbVencimiento.setText("Vencimiento : ");
			lblpnmbVencimiento.setBounds(29, 22, 73, 20);
		}
		return lblpnmbVencimiento;
	}
	private JTextFieldDouble getJTxtInteres() {
		if (jTxtInteres == null) {
			jTxtInteres = new JTextFieldDouble();
			jTxtInteres.setEnabled(false);
			jTxtInteres.setEditable(false);
			jTxtInteres.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtInteres.setBounds(101, 43, 98, 21);
			jTxtInteres.setText("0.00");
		}
		return jTxtInteres;
	}
	private JButtonPanambi getBtnpnmbOk() {
		if (btnpnmbOk == null) {
			btnpnmbOk = new JButtonPanambi();
			btnpnmbOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doVentaCredito();
				}
			});
			btnpnmbOk.setText("ok");
		}
		return btnpnmbOk;
	}
	
	private void doVentaCredito(){
		
		PlanPago planPago = new PlanPago();
		planPago.setCodVenta(1);
		planPago.setEstado("A");
		
		Date fecha = new Date();
		fecha.setDate(10);
		String fechaCuota ;
//		Integer mes = Calendar.MONTH+1;
//		Integer dia = 10;
		
		for (int i = 0; i <= 12 ; i++){
			fecha.setMonth(fecha.getMonth()+1);
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
			fechaCuota = formateador.format(fecha);
			JOptionPane.showMessageDialog(null,fechaCuota);
			
		}
		
		JOptionPane.showMessageDialog(null, fecha.getDay());//+"/"+fecha.getMonth()+"/"+fecha.getYear());
		
		Float interes = Float.parseFloat(getJTxtInteres().getValue().toString());
		Integer cantidadCuotas = getJTxtCuotas().getValor();
		Float total = Float.parseFloat(getJTxtTotal().getValue().toString());
		Float montoCuota;
		Integer diaVencimiento = getJTxtVencimiento().getValor();
		
		JOptionPane.showMessageDialog(null,  Calendar.MONTH+1);
		JOptionPane.showMessageDialog(null,  getJTxtInteres().getValue());
		montoCuota = Float.parseFloat(getJTxtInteres().getValue().toString());
		JOptionPane.showMessageDialog(null,  montoCuota+1);
		if(interes == 0 ){
			montoCuota = total/cantidadCuotas;
		}else{
			montoCuota = (total+(total*(interes/100)))/cantidadCuotas;
			JOptionPane.showMessageDialog(null,  "AHORA");
			JOptionPane.showMessageDialog(null,  montoCuota);
		}
		JOptionPane.showMessageDialog(null,  montoCuota);
		
		planPago.setMonto(montoCuota);
		@SuppressWarnings("deprecation")
		Date fechaVencimiento = new Date (diaVencimiento,Calendar.MONTH, Calendar.YEAR);
		planPago.setVencimiento(fechaVencimiento);
		try {
			controladorPlanPago.insertarPagoCuota(JFramePanambiMain.session.getConn(), planPago);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setText("1000000");
			jTxtTotal.setBounds(324, 21, 80, 21);
		}
		return jTxtTotal;
	}
	@Override
	public void setValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JComboBoxPanambi getComboBoxPanambi() {
		if (comboBoxPanambi == null) {
			comboBoxPanambi = new JComboBoxPanambi();
			comboBoxPanambi.setToolTipText("Dia de Vencimiento de cada mes");
			comboBoxPanambi.setMaximumRowCount(3);
			comboBoxPanambi.setModel(new DefaultComboBoxModel(new String[] {"5", "7", "12", "15"}));
			comboBoxPanambi.setBounds(112, 22, 70, 21);
		}
		return comboBoxPanambi;
	}
	private JComboBoxPanambi getComboBoxPanambi_1() {
		if (comboBoxPanambi_1 == null) {
			comboBoxPanambi_1 = new JComboBoxPanambi();
			comboBoxPanambi_1.setToolTipText("Cantidad de Cuotas");
			comboBoxPanambi_1.setMaximumRowCount(3);
			comboBoxPanambi_1.setModel(new DefaultComboBoxModel(new String[] {"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
			comboBoxPanambi_1.setBounds(112, 53, 70, 21);
		}
		return comboBoxPanambi_1;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Interes por Mora", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panel.setBounds(244, 85, 209, 84);
			panel.setLayout(null);
			panel.add(getJTxtInteres());
			panel.add(getJRbFijoMora());
			panel.add(getJRbFijoPorcentual());
			panel.add(getJTxtInteresFijoMora());
		}
		return panel;
	}
	private JRadioButton getJRbFijoMora() {
		if (jRbFijoMora == null) {
			jRbFijoMora = new JRadioButton("Fijo");
			jRbFijoMora.setSelected(true);
			jRbFijoMora.setBounds(6, 17, 49, 23);
		}
		return jRbFijoMora;
	}
	private JRadioButton getJRbFijoPorcentual() {
		if (jRbFijoPorcentual == null) {
			jRbFijoPorcentual = new JRadioButton("Porcentual");
			jRbFijoPorcentual.setBounds(6, 43, 89, 23);
		}
		return jRbFijoPorcentual;
	}
	private JTextFieldDouble getJTxtInteresFijoMora() {
		if (jTxtInteresFijoMora == null) {
			jTxtInteresFijoMora = new JTextFieldDouble();
			jTxtInteresFijoMora.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtInteresFijoMora.setText("0");
			jTxtInteresFijoMora.setBounds(101, 17, 98, 21);
		}
		return jTxtInteresFijoMora;
	}
	private JButtonPanambi getJBtnSimular() {
		if (jBtnSimular == null) {
			jBtnSimular = new JButtonPanambi();
			jBtnSimular.setBounds(241, 52, 73, 23);
			jBtnSimular.setText("Generar");
		}
		return jBtnSimular;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(null);
			panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Interes de Financiacion", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panel_1.setBounds(23, 85, 211, 84);
			panel_1.add(getJTxtInteresPorcentual());
			panel_1.add(getJRbInteresFijo());
			panel_1.add(getJRbInteresPorcentual());
			panel_1.add(getJTxtInteresFijo());
		}
		return panel_1;
	}
	private JTextFieldDouble getJTxtInteresPorcentual() {
		if (jTxtInteresPorcentual == null) {
			jTxtInteresPorcentual = new JTextFieldDouble();
			jTxtInteresPorcentual.setText("0.00");
			jTxtInteresPorcentual.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtInteresPorcentual.setEnabled(false);
			jTxtInteresPorcentual.setEditable(false);
			jTxtInteresPorcentual.setBounds(84, 43, 117, 21);
		}
		return jTxtInteresPorcentual;
	}
	private JRadioButton getJRbInteresFijo() {
		if (jRbInteresFijo == null) {
			jRbInteresFijo = new JRadioButton("Fijo");
			jRbInteresFijo.setSelected(true);
			jRbInteresFijo.setBounds(6, 17, 49, 23);
		}
		return jRbInteresFijo;
	}
	private JRadioButton getJRbInteresPorcentual() {
		if (jRbInteresPorcentual == null) {
			jRbInteresPorcentual = new JRadioButton("Porcentual");
			jRbInteresPorcentual.setBounds(6, 43, 77, 23);
		}
		return jRbInteresPorcentual;
	}
	private JTextFieldDouble getJTxtInteresFijo() {
		if (jTxtInteresFijo == null) {
			jTxtInteresFijo = new JTextFieldDouble();
			jTxtInteresFijo.setText("0");
			jTxtInteresFijo.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtInteresFijo.setBounds(84, 17, 117, 21);
		}
		return jTxtInteresFijo;
	}
	private JLabelPanambi getLblpnmbMontoDeVenta() {
		if (lblpnmbMontoDeVenta == null) {
			lblpnmbMontoDeVenta = new JLabelPanambi();
			lblpnmbMontoDeVenta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbMontoDeVenta.setText("Monto de Venta : ");
			lblpnmbMontoDeVenta.setBounds(213, 21, 101, 20);
		}
		return lblpnmbMontoDeVenta;
	}
	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			table.setModel(new DefaultTableModel(
				new Object[][] {
					{null, null, null},
				},
				new String[] {
					"Importe", "Descripcion", "Vencimiento"
				}
			) {
				boolean[] columnEditables = new boolean[] {
					false, true, true
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
			table.setBounds(60, 337, 393, -125);
		}
		return table;
	}
}
