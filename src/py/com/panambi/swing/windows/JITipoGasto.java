package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.TipoGasto;
import py.com.panambi.controller.ControladorTipoGasto;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;

public class JITipoGasto extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2736779504173054335L;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbConcepto;
	private JButtonPanambi btnpnmbLimpiar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbSalir;
	private JCheckBox chckbxActivo;
	private JComboBoxPanambi jCmbConcepto;
	private JLabelPanambi lblpnmbPresioneFPara;
	private List<String> tiposGastos = new ArrayList<>();
	private TipoGasto tipoGasto;
	private ControladorTipoGasto controladorTipoGasto = new ControladorTipoGasto();
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi labelPanambi;
	
	public JITipoGasto() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Tipo de Gasto");
		setBounds(100, 100, 390, 290);
		setMaximizable(false);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(229)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(117)
					.addComponent(getChckbxActivo()))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJCmbConcepto(), GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getChckbxActivo()))
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout);
		setHelp("tiposgastos");
		setShortcuts(this);
		decorate();
		doLimpiar();
		getJCmbConcepto().requestFocus();
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbConcepto() {
		if (lblpnmbConcepto == null) {
			lblpnmbConcepto = new JLabelPanambi();
			lblpnmbConcepto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbConcepto.setText("Concepto : ");
		}
		return lblpnmbConcepto;
	}
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJCmbConcepto().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		
		try {
			tipoGasto=null;
			poblarLista();
			getJCmbConcepto().setModel(new ListComboBoxModel<String>(tiposGastos));
			getJTxtCodigo().setValor(null);
			getJCmbConcepto().setSelectedItem(null);
			getChckbxActivo().setSelected(true);
			getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbEliminar().setEnabled(false);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
		
	}
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar tipo de gasto");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Tipo de Gasto", "Estado"};
			String[] pks = { "codTipoGasto" };
			String sSelect = "SELECT codtipogasto, concepto, (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END)  ";
			sSelect += "FROM tiposgastos ";
			sSelect += "ORDER BY concepto ";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.TipoGasto", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar gasto");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (tipoGasto == null) {
				tipoGasto = new TipoGasto();
				tipoGasto.setConcepto((String) getJCmbConcepto().getSelectedItem());
				if(getChckbxActivo().isSelected()){
					tipoGasto.setEstado("A");
					
				} else tipoGasto.setEstado("I");
				
				controladorTipoGasto.insertarTipoGasto(JFramePanambiMain.session.getConn(), tipoGasto);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbConcepto().requestFocus();
			} else {
				tipoGasto.setConcepto((String) getJCmbConcepto().getSelectedItem());
				
				if(getChckbxActivo().isSelected()){
					tipoGasto.setEstado("A");
					
				}else tipoGasto.setEstado("I");
				
				Integer ret = JOptionPane.showConfirmDialog(this, "Esta seguro que desea modificar los datos del tipo de gasto? ", "Ventana de Confirmacion", 1, JOptionPane.YES_NO_OPTION);
				if(ret == JOptionPane.YES_OPTION){
					controladorTipoGasto.modificarTipoGasto(JFramePanambiMain.session.getConn(), tipoGasto);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbConcepto().requestFocus();
				}
				tipoGasto= null;
				getJCmbConcepto().requestFocus();
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar gasto");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma eliminar de forma \npermante los datos del tipo de gasto? ", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				controladorTipoGasto.borrarTipoGasto(JFramePanambiMain.session.getConn(), tipoGasto);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbConcepto().requestFocus();
			}
			tipoGasto = null;
			getJCmbConcepto().requestFocus();
		} catch (SQLException e) {
			if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
				DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
			} else {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
			chckbxActivo.setSelected(true);
		}
		return chckbxActivo;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbConcepto() {
		if (jCmbConcepto == null) {
			jCmbConcepto = new JComboBoxPanambi();
			jCmbConcepto.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusConcepto();
				}
			});
			
			jCmbConcepto.setEditable(true);
			jCmbConcepto.setModel(new ListComboBoxModel<String>(tiposGastos));
			jCmbConcepto.setSelectedItem(null);
		}
		return jCmbConcepto;
	}
	
	private void lostFocusConcepto() {
		try {
			TipoGasto tipoG= controladorTipoGasto.getTipoGasto(JFramePanambiMain.session.getConn(), (String) jCmbConcepto.getSelectedItem());
			if (tipoG != null) {
				setValues(tipoG, null);
				getBtnpnmbGrabar().setEnabled(true);
				getBtnpnmbEliminar().setEnabled(true);
			} else {
				doNuevoFromLostFocus();
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		tipoGasto = null;
		getJTxtCodigo().setText("");
		getChckbxActivo().setSelected(true);
		
		Integer longitud = 0;
		
		try{
			longitud =getJCmbConcepto().getSelectedItem().toString().length(); 
		}catch(Exception e){
			longitud = 0 ;
		}
		
		
		if(longitud !=0){
			btnpnmbGrabar.setEnabled(true);
			
		}else {
			btnpnmbGrabar.setEnabled(false);
		}
		
		btnpnmbEliminar.setEnabled(false);
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof TipoGasto) {
			this.tipoGasto = (TipoGasto) obj;
			getJTxtCodigo().setValor(tipoGasto.getCodigo());
			getJCmbConcepto().setSelectedItem(((TipoGasto) (obj)).getConcepto());
			
			if(tipoGasto.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			}else chckbxActivo.setSelected(false);
		}
		
	}
	
	private void poblarLista() throws Exception {
		tiposGastos = new ArrayList<String>();
		List<TipoGasto> listagast = controladorTipoGasto.getTiposGastos(JFramePanambiMain.session.getConn());
		for (TipoGasto gast : listagast) {
			tiposGastos.add(gast.getConcepto());
		}
	}
	
	
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setEnabled(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbConcepto());
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
}
