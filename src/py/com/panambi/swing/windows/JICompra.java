package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.DetalleCompra;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorCompra;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorProveedor;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.prompts.PromptProductos;

public class JICompra extends JInternalFramePanambi implements Browseable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5692956630618496093L;
	private JLabelPanambi jLblPanambi;
	private JTextFieldDate jTxtFecha;
	private JLabelPanambi lblpnmbProveedor;
	private JComboBoxPanambi jCmbProveedor;
	private List<Proveedor> proveedores = new ArrayList<Proveedor>();
	private List<Sucursal> sucursales = new ArrayList<Sucursal>();
	private ControladorProveedor controladorProveedor = new ControladorProveedor();
	private JScrollPane jScrollPaneProductos;
	private JTablePanambi jTableDetalle;
	private JButtonPanambi btnpnmbAgregarItem;
	private JButtonPanambi jBtnRegistrarCompra;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbNuevaCompra;
	private JLabelPanambi labelProducto;
	private JTextFieldInteger jTxtCodProducto;
	private JTextFieldUpper jTxtProducto;
	private JLabelPanambi labelCantidad;
	private JTextFieldInteger jTxtCantidad;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private JLabelPanambi lblpnmbCostoUnit;
	private JTextFieldDouble jTxtCosto;
	private Producto producto;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldDouble jTxtTotal;
	JPopupMenu popupMenu = new JPopupMenu();
	private JLabelPanambi lblpnmbPagaderoALos;
	private JTextFieldInteger jTxtDias;
	private JLabelPanambi lblpnmbDias;
	private JLabelPanambi lblpnmbFacturaNro;
	private JTextFieldUpper jTxtFacturaNro;
	private JLabel jLblImage;

	/**
	 * Create the frame.
	 */
	public JICompra() throws Exception {
		initialize();
	}

	@SuppressWarnings("unchecked")
	private void doNuevo() {
		try {
			poblarProveedores();
			getJCmbProveedor().setModel(new ListComboBoxModel<Proveedor>(proveedores));
			poblarSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<Sucursal>(sucursales));
			getJTableDetalle().resetData(0);
			getJTxtCantidad().setValor(null);
			getJTxtCodProducto().setValor(null);
			getJTxtTotal().setValue(0.0);
			getJTxtDias().setValor(0);
			setSelectedSucursal(JFramePanambiMain.session.getSucursalOperativa());
			getJCmbSucursal().setEnabled(false);
			getJTxtFacturaNro().setText(null);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void setSelectedSucursal(Sucursal sucursalOperativa) {
		for (int i = 0; i < getJCmbSucursal().getItemCount(); i++) {
			Sucursal sucu = (Sucursal) getJCmbSucursal().getItemAt(i);	
			if (sucu.getCodSucursal() == sucursalOperativa.getCodSucursal()) {
				getJCmbSucursal().setSelectedIndex(i);
				break;
			}
		} 
	}

	private void poblarSucursales() throws Exception {
		ControladorSucursal controladorSucursal = new ControladorSucursal();
		sucursales = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
	}

	private void initialize() {
		setMaximizable(false);
		setTitle("Registro de Compras");
		setBounds(100, 100, 946, 520);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJLblPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getLblpnmbPagaderoALos(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtDias(), GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getLblpnmbDias(), GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getLblpnmbFacturaNro(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtFacturaNro(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
							.addGap(103)
							.addComponent(getJLblImage(), GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtProducto(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelCantidad(), GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtCantidad(), GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLblpnmbCostoUnit(), GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtCosto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getBtnpnmbAgregarItem(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(62))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(739)
					.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
					.addGap(12))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(12, Short.MAX_VALUE)
					.addComponent(getJScrollPaneProductos(), GroupLayout.DEFAULT_SIZE, 902, Short.MAX_VALUE)
					.addGap(12))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJLblPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbPagaderoALos(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtDias(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbDias(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbFacturaNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtFacturaNro(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(getJLblImage(), GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLabelProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCantidad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCosto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnpnmbAgregarItem(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbCostoUnit(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelCantidad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getJScrollPaneProductos(), GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(177))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnpnmbNuevaCompra());
		getJPanelSouth().add(getJBtnRegistrarCompra());
		getJPanelSouth().add(getBtnpnmbSalir());

		JMenuItem menuItem4 = new JMenuItem("Eliminar Item");
		menuItem4.setFont(FontPanambi.getLabelInstance());
		menuItem4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				doEliminarDetalle();
			}
		});
		popupMenu.add(menuItem4);
		this.setOpaque(true);
		this.addInternalFrameListener(new javax.swing.event.InternalFrameAdapter() {

			public void internalFrameClosing(javax.swing.event.InternalFrameEvent e) {
				cleanPopPups();
			}

			public void internalFrameClosed(javax.swing.event.InternalFrameEvent e) {
				cleanPopPups();
			}
		});
		setShortcuts(this);
		setHelp("registrarCompras");
		doNuevo();
	}

	/**
	 * Elimina item del detalle
	 */
	private void doEliminarDetalle() {
		if (getJTableDetalle().getSelectedRow() > -1) {
			int response = JOptionPane.showConfirmDialog(getOwner(), "<html>Eliminando el detalle de la compra.<br><br>Desea continuar?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.YES_OPTION) {
				getJTableDetalle().removeRow(getJTableDetalle().getSelectedRow());
			}
			actualizaTotal();
		}
	}

	private void cleanPopPups() {
		popupMenu.setVisible(false);
	}

	private JLabelPanambi getJLblPanambi() {
		if (jLblPanambi == null) {
			jLblPanambi = new JLabelPanambi();
			jLblPanambi.setText("Fecha Compra");
		}
		return jLblPanambi;
	}

	private JTextFieldDate getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JTextFieldDate();
		}
		return jTxtFecha;
	}

	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setText("Proveedor");
		}
		return lblpnmbProveedor;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbProveedor() {
		if (jCmbProveedor == null) {
			jCmbProveedor = new JComboBoxPanambi();
			jCmbProveedor.setModel(new ListComboBoxModel<Proveedor>(proveedores));
			jCmbProveedor.setSelectedItem(null);
			jCmbProveedor.setEditable(false);
		}
		return jCmbProveedor;
	}

	private void poblarProveedores() throws Exception {
		proveedores = new ArrayList<Proveedor>();
		List<Proveedor> listaprov = controladorProveedor.getProveedores(JFramePanambiMain.session.getConn());
		for (Proveedor prov : listaprov) {
			proveedores.add(prov);
		}
	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = (Producto) obj;
			getJTxtCodProducto().setValor(producto.getCodProducto());
			getJTxtProducto().setText(producto.getDescripcion());
			getJLblImage().setText(null);
			InputStream in = null;
			if (producto.getImagenProducto() != null) {
				try {
					in = new ByteArrayInputStream(producto.getImagenProducto().getImagen());
					BufferedImage image = null;
					image = ImageIO.read(in);
					getJLblImage().setIcon(new ImageIcon(image.getScaledInstance(getJLblImage().getWidth(), getJLblImage().getHeight(), Image.SCALE_SMOOTH)));

				} catch (Exception e) {
					e.printStackTrace();
					getJLblImage().setIcon(null);
					JOptionPane.showMessageDialog(this, "No se pudo obtener imagen");
				}
				getJTxtCantidad().requestFocus();
			}else{
				getJLblImage().setIcon(null);
			}
		}
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = null;
			getJTxtProducto().setText(null);
			getJTxtCodProducto().requestFocus();
			getJLblImage().setText(null);
			getJLblImage().setIcon(null);
		}
	}

	private JScrollPane getJScrollPaneProductos() {
		if (jScrollPaneProductos == null) {
			jScrollPaneProductos = new JScrollPane();
			jScrollPaneProductos.setViewportView(getJTableDetalle());
		}
		return jScrollPaneProductos;
	}

	private JTablePanambi getJTableDetalle() {
		if (jTableDetalle == null) {
			String[] columnNames = { "Cant.", "Producto", "Costo Unitario", "Total", "Data" };
			jTableDetalle = new JTablePanambi(columnNames);
			jTableDetalle.getColumnModel().getColumn(0).setPreferredWidth(20);
			jTableDetalle.getColumnModel().getColumn(1).setPreferredWidth(300);
			jTableDetalle.getColumnModel().getColumn(2).setPreferredWidth(100);
			jTableDetalle.getColumnModel().getColumn(3).setPreferredWidth(120);
			jTableDetalle.getColumnModel().getColumn(4).setMinWidth(0);
			jTableDetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
			jTableDetalle.getColumnModel().getColumn(4).setMaxWidth(0);
			jTableDetalle.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTableDetalle.rowAtPoint(e.getPoint());
						jTableDetalle.changeSelection(r, 0, true, true);
						int[] selecteds = jTableDetalle.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});
			jTableDetalle.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						doEliminarDetalle();
					}
					super.keyPressed(e);
				}
			});
		}
		return jTableDetalle;
	}

	private JButtonPanambi getBtnpnmbAgregarItem() {
		if (btnpnmbAgregarItem == null) {
			btnpnmbAgregarItem = new JButtonPanambi();
			btnpnmbAgregarItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarItem();
				}
			});
			btnpnmbAgregarItem.setMnemonic('I');
			btnpnmbAgregarItem.setText("Agregar Item");
		}
		return btnpnmbAgregarItem;
	}

	/**
	 * Agrega cada item de producto comprado
	 */
	private void doAgregarItem() {
		try {
			checkAddItem();
			getJTableDetalle().addRow();
			getJTableDetalle().setValueAt(getJTxtCantidad().getValor(), getJTableDetalle().getRowCount() - 1, 0);
			getJTableDetalle().setValueAt(producto.getDescripcion(), getJTableDetalle().getRowCount() - 1, 1);
			getJTableDetalle().setValueAt(getJTxtCosto().getValue(), getJTableDetalle().getRowCount() - 1, 2);
			double totallinea = getJTxtCantidad().getValor() * (Double) getJTxtCosto().getValue();
			getJTableDetalle().setValueAt(totallinea, getJTableDetalle().getRowCount() - 1, 3);
			DetalleCompra detalleCompra = new DetalleCompra();
			detalleCompra.setCantidad(getJTxtCantidad().getValor());
			detalleCompra.setCostounitario((Double) getJTxtCosto().getValue());
			detalleCompra.setNroitem(getJTableDetalle().getRowCount());
			detalleCompra.setProducto(producto);
			getJTableDetalle().setValueAt(detalleCompra, getJTableDetalle().getRowCount() - 1, 4);
			actualizaTotal();
			initializeItem();
		} catch (ValidException e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			if (e.getMessage().contains("producto")) {
				getJTxtCodProducto().requestFocus();
			}
			if (e.getMessage().contains("cantidad")) {
				getJTxtCantidad().requestFocus();
			}
			if (e.getMessage().contains("costo")) {
				getJTxtCosto().requestFocus();
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void actualizaTotal() {
		double total = 0.0;
		for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
			total += (Double) getJTableDetalle().getValueAt(i, 3);
		}
		getJTxtTotal().setValue(total);
	}

	private void initializeItem() {
		getJTxtCodProducto().setValor(null);
		getJTxtCantidad().setValor(null);
		getJTxtProducto().setText(null);
		getJTxtCosto().setValue(null);
		getJLblImage().setIcon(null);
		producto = null;
		getJTxtCodProducto().requestFocus();

	}

	private void checkAddItem() throws ValidException {
		if (producto == null) {
			throw new ValidException("Debe seleccionar un producto");
		}
		if (getJTxtCantidad().getValor() == null || getJTxtCantidad().getValor() < 1) {
			throw new ValidException("Debe ingresar una cantidad > 0");
		}
		if (getJTxtCosto().getValue() == null || ((Double) getJTxtCosto().getValue()) < 1) {
			throw new ValidException("Debe ingresar un costo > 0");
		}
		for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
			if (producto.getCodProducto().equals(((DetalleCompra) getJTableDetalle().getValueAt(i, 4)).getProducto().getCodProducto())) {
				throw new ValidException("El producto duplicado en la misma compra.");
			}
		}
	}

	private JButtonPanambi getJBtnRegistrarCompra() {
		if (jBtnRegistrarCompra == null) {
			jBtnRegistrarCompra = new JButtonPanambi();
			jBtnRegistrarCompra.setToolTipText("Registrar compra");
			jBtnRegistrarCompra.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarCompra();
				}
			});
			jBtnRegistrarCompra.setMnemonic('R');
			jBtnRegistrarCompra.setText("Registrar Compra");
		}
		return jBtnRegistrarCompra;
	}

	/**
	 * Registra la Compra
	 */
	private void doRegistrarCompra() {
		Connection conn = JFramePanambiMain.session.getConn();
		try {
			if(getJTableDetalle().getRowCount()!=0){
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma el registro de la compra?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (ret == JOptionPane.YES_OPTION) {
					ControladorCompra controladorCompra = new ControladorCompra();
					Compra compra = new Compra();
					compra.setFecha(new Timestamp(getJTxtFecha().getFecha().getTime()));
					compra.setProveedor((Proveedor) getJCmbProveedor().getSelectedItem());
					compra.setSucursal((Sucursal) getJCmbSucursal().getSelectedItem());
					compra.setTotal((Double) getJTxtTotal().getValue());
					compra.setPagaderodias(getJTxtDias().getValor() != null ? getJTxtDias().getValor() : 0);
					compra.setFacturanro(getJTxtFacturaNro().getText() != null ? ((String) getJTxtFacturaNro().getText()).trim() : null);
					List<DetalleCompra> detalle = new ArrayList<DetalleCompra>();
					for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
						DetalleCompra detalleCompra = (DetalleCompra) getJTableDetalle().getValueAt(i, 4);
						detalle.add(detalleCompra);
					}
					compra.setDetalleCompra(detalle);
					controladorCompra.registrarCompra(conn, compra);
					DlgMessage.showMessage(getOwner(), "Compra registrada en forma satisfactoria", DlgMessage.INFORMATION_MESSAGE);
					doNuevo();
					getJTxtDias().requestFocus();
				}
			}else{
				DlgMessage.showMessage(getOwner(), "Debe agregar items a la compra", DlgMessage.WARNING_MESSAGE);
			}
			

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}

	private void doSalir() {
		this.dispose();
	}

	private JButtonPanambi getBtnpnmbNuevaCompra() {
		if (btnpnmbNuevaCompra == null) {
			btnpnmbNuevaCompra = new JButtonPanambi();
			btnpnmbNuevaCompra.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doNuevo();
				}
			});
			btnpnmbNuevaCompra.setToolTipText("Limpiar pantalla");
			btnpnmbNuevaCompra.setMnemonic('L');
			btnpnmbNuevaCompra.setText("Limpiar");
		}
		return btnpnmbNuevaCompra;
	}

	private JLabelPanambi getLabelProducto() {
		if (labelProducto == null) {
			labelProducto = new JLabelPanambi();
			labelProducto.setText("Producto");
		}
		return labelProducto;
	}

	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
			jTxtCodProducto.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					try{
						if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() != null) {
							Integer codproducto = ((JTextFieldInteger) e.getComponent()).getValor();
							setProductoFromCod(codproducto);
						}
						if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() == null) {
							producto = null;
							getJTxtCodProducto().setValor(null);
							getJTxtProducto().setText("");
						}
						super.focusLost(e);
					}catch (Exception e1) {
						producto = null;
						getJTxtCodProducto().setValor(null);
						getJTxtProducto().setText("");
						doPromptProducto();
					}
						
				}
			});
		}
		return jTxtCodProducto;
	}

	protected void setProductoFromCod(Integer codproducto) {
		try {
			if (codproducto == null) {
				throw new ValidException();
			}
			Producto producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), codproducto);
			if (producto != null) {
				setValues(producto, null);
			} else {
				setNoValues(new Producto(), null);
			}
		} catch (ValidException e) {
			producto = null;
			getJTxtCodProducto().setValor(null);
			getJTxtProducto().setText("");
			doPromptProducto();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void doPromptProducto() {
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}

	private JTextFieldUpper getJTxtProducto() {
		if (jTxtProducto == null) {
			jTxtProducto = new JTextFieldUpper();
			jTxtProducto.setEnabled(false);
		}
		return jTxtProducto;
	}

	private JLabelPanambi getLabelCantidad() {
		if (labelCantidad == null) {
			labelCantidad = new JLabelPanambi();
			labelCantidad.setText("Cantidad");
		}
		return labelCantidad;
	}

	private JTextFieldInteger getJTxtCantidad() {
		if (jTxtCantidad == null) {
			jTxtCantidad = new JTextFieldInteger();
		}
		return jTxtCantidad;
	}

	private JLabelPanambi getLblpnmbCostoUnit() {
		if (lblpnmbCostoUnit == null) {
			lblpnmbCostoUnit = new JLabelPanambi();
			lblpnmbCostoUnit.setText("Costo Unit.");
		}
		return lblpnmbCostoUnit;
	}

	private JTextFieldDouble getJTxtCosto() {
		if (jTxtCosto == null) {
			jTxtCosto = new JTextFieldDouble();
		}
		return jTxtCosto;
	}

	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setText("Sucursal");
		}
		return lblpnmbSucursal;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
			jCmbSucursal.setEditable(false);
			jCmbSucursal.setModel(new ListComboBoxModel<Sucursal>(sucursales));
			jCmbSucursal.setSelectedItem(null);
		}
		return jCmbSucursal;
	}

	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setFont(new Font("Dialog", Font.BOLD, 11));
			lblpnmbTotal.setText("Total");
		}
		return lblpnmbTotal;
	}

	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtTotal.setFont(new Font("Dialog", Font.BOLD, 11));
			jTxtTotal.setEnabled(false);
			jTxtTotal.setDisabledTextColor(Color.BLACK);
			jTxtTotal.setValue(0.0);
		}
		return jTxtTotal;
	}

	private JLabelPanambi getLblpnmbPagaderoALos() {
		if (lblpnmbPagaderoALos == null) {
			lblpnmbPagaderoALos = new JLabelPanambi();
			lblpnmbPagaderoALos.setText("Pagadero a los");
		}
		return lblpnmbPagaderoALos;
	}

	private JTextFieldInteger getJTxtDias() {
		if (jTxtDias == null) {
			jTxtDias = new JTextFieldInteger();
			jTxtDias.setMaxChars(3);
		}
		return jTxtDias;
	}

	private JLabelPanambi getLblpnmbDias() {
		if (lblpnmbDias == null) {
			lblpnmbDias = new JLabelPanambi();
			lblpnmbDias.setText("dias.");
		}
		return lblpnmbDias;
	}

	private JLabelPanambi getLblpnmbFacturaNro() {
		if (lblpnmbFacturaNro == null) {
			lblpnmbFacturaNro = new JLabelPanambi();
			lblpnmbFacturaNro.setText("Factura Nro.");
		}
		return lblpnmbFacturaNro;
	}

	private JTextFieldUpper getJTxtFacturaNro() {
		if (jTxtFacturaNro == null) {
			jTxtFacturaNro = new JTextFieldUpper();
		}
		return jTxtFacturaNro;
	}

	private JLabel getJLblImage() {
		if (jLblImage == null) {
			jLblImage = new JLabel("");
		}
		return jLblImage;
	}
}
