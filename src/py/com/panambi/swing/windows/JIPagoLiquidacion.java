package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.joda.time.DateTime;
import org.joda.time.Months;

import py.com.panambi.bean.DetallePagoSalario;
import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.Usuario;
import py.com.panambi.controller.ControladorDeudaEmpleado;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPagoSalario;
import py.com.panambi.controller.ControladorPlanPago;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.informes.JRDataSourcePagoSalario;
import py.com.panambi.informes.JRDataSourceReciboPagoCuota;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.interfaces.ReciboBrowser;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.prompts.PromptEmpleadoLiquidacion;
import py.com.panambi.utils.NumerosALetras;
import py.com.panambi.utils.PanambiUtils;


public class JIPagoLiquidacion extends JInternalFramePanambi implements Browseable, ReciboBrowser{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3311645412692147066L;
	private ControladorPagoSalario controladorPagoSalario = new ControladorPagoSalario();
	private ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
	private ControladorDeudaEmpleado controladordeudaempleado = new  ControladorDeudaEmpleado();
    private JScrollPane scrollPane;
	private JLabelPanambi lblpnmbAPagar;
	private JTextFieldInteger jTxtCodigo;

	private JTablePanambi jTablePagos;
	private JButtonPanambi jBtnAplicar;
	JPopupMenu popupMenu = new JPopupMenu();
	private JLabelPanambi lblpnmbNombre;
	private JLabelPanambi lblpnmbSalarioNominal;
	private JTextFieldDouble jTxtSalario;
	private JTextFieldUpper jTxtNombre;
	private JPanel panelDatosEmpleado;
	private JLabelPanambi lblpnmbNroDocumento;
	private JFormattedTextFieldPanambi jTxtDocumento;
	private JLabelPanambi lblpnmbEstado;
	private JTextFieldUpper jTxtEstado;
	private JLabel lblNewLabel;
	private JTextFieldDouble jTxtTotalPagar;
	private JLabelPanambi lblpnmbTotalAPagar;
	private String from;
	private JButtonPanambi btnpnmbGenerarLquidacion;
	private JLabelPanambi lblpnmbAntiguedad;
	private JLabelPanambi lblpnmbPreAvisodias;
	private JTextFieldInteger jTxtPreaviso;
	private JTextFieldDouble jTxtMeses;
	private JTextFieldInteger jTxtAntiguedad;
	private JCheckBoxPanambi chckbxpnmbRenuncia;
	private JRadioButton rdbJustificado;
	private JRadioButton rdbInjustificado;
	private JButton btnLimpiar;
	private JTextFieldInteger jTxtVacaciones;
	private JLabelPanambi lblpnmbVacaciones;
	private JTextFieldInteger jTxtVacacionesTotal;
	private JButton btnSalir;
	private JTextFieldInteger JTxtPreavisoFalta;
	private JLabelPanambi lblpnmbTipoDeRecision;
	private JButton btnCobro;
	private JLabelPanambi lblpnmbDescuento;
	private JTextFieldDouble jTxtDescuento;
	private JLabelPanambi labelPanambi;

	/**
	 * Create the frame.
	 */
	public JIPagoLiquidacion() throws Exception{
		initialize();
	}
	private void initialize() {
		setResizable(true);
		setMaximizable(false);
		try {
			setTitle("Liquidacion de salario");
			setBounds(100, 100, 889, 505);
			doLimpiar();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		getJPanelSouth().add(getBtnpnmbGenerarLquidacion());
		getJPanelSouth().add(getBtnLimpiar());
		getJPanelSouth().add(getJBtnAplicar());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 855, Short.MAX_VALUE))
						.addComponent(getPanelDatosEmpleado(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 865, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getPanelDatosEmpleado(), GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
					.addGap(80))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnCobro());
		getJPanelSouth().add(getBtnSalir());
		setHelp("generarPagoLiquidacion");
		setShortcuts(this);
	}
	
	private JPanel getPanelDatosEmpleado() {
		if (panelDatosEmpleado == null) {
			panelDatosEmpleado = new JPanel();
			panelDatosEmpleado.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos de empleado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDatosEmpleado.setName("");
			GroupLayout gl_panelDatosEmpleado = new GroupLayout(panelDatosEmpleado);
			gl_panelDatosEmpleado.setHorizontalGroup(
				gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(5)
										.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
												.addComponent(getLblpnmbVacaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(getJTxtVacaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(18)
												.addComponent(getJTxtVacacionesTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
												.addComponent(getLblpnmbPreAvisodias(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(getJTxtPreavisoFalta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(18)
												.addComponent(getJTxtPreaviso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
										.addComponent(getLblNewLabel()))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getJtxtNombre(), GroupLayout.PREFERRED_SIZE, 268, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(4)
										.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addComponent(getLblpnmbTotalAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getJTxtTotalPagar(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
								.addGap(61)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
										.addComponent(getChckbxpnmbRenuncia(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.TRAILING)
											.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbAntiguedad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
									.addComponent(getLblpnmbDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING, false)
										.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtEstado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
												.addComponent(getJTxtDescuento(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
												.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
													.addComponent(getJTxtAntiguedad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
													.addComponent(getJTxtMeses(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addComponent(getLblpnmbTipoDeRecision(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
											.addComponent(getRdbInjustificado())
											.addComponent(getRdbJustificado()))))
								.addGap(136))
							.addGroup(Alignment.TRAILING, gl_panelDatosEmpleado.createSequentialGroup()
								.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
								.addGap(165))))
			);
			gl_panelDatosEmpleado.setVerticalGroup(
				gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGap(70)
								.addComponent(getJTxtAntiguedad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addContainerGap()
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
												.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
													.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addComponent(getLblpnmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
													.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
											.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
												.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
													.addComponent(getJtxtNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addGap(70)
										.addComponent(getJTxtMeses(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addGap(70)
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
											.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addGap(65)
										.addComponent(getLblpnmbAntiguedad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
										.addComponent(getLblpnmbTotalAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtTotalPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
										.addComponent(getLblpnmbDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
						.addGap(2)
						.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
							.addGroup(Alignment.TRAILING, gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbVacaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtVacaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtVacacionesTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addGap(18)
										.addComponent(getLblNewLabel()))
									.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
											.addComponent(getJTxtPreavisoFalta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getLblpnmbPreAvisodias(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJTxtPreaviso(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
								.addGap(58))
							.addGroup(gl_panelDatosEmpleado.createSequentialGroup()
								.addGroup(gl_panelDatosEmpleado.createParallelGroup(Alignment.BASELINE)
									.addComponent(getRdbJustificado())
									.addComponent(getChckbxpnmbRenuncia(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbTipoDeRecision(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getRdbInjustificado())
								.addGap(18)
								.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())))
			);
			panelDatosEmpleado.setLayout(gl_panelDatosEmpleado);
		}
		
		
		return panelDatosEmpleado;
	}
	
	private JButtonPanambi getJBtnAplicar() {
		if (jBtnAplicar == null) {
			jBtnAplicar = new JButtonPanambi();
			jBtnAplicar.setToolTipText("Pagar liquidacion");
			jBtnAplicar.setMnemonic('P');
			jBtnAplicar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doPagar();
				}
			});
			jBtnAplicar.setText("Pagar");
			jBtnAplicar.setMnemonic('P');
		}
		return jBtnAplicar;
	}

	private void doPagar() {
		try{
			
			PagoSalario pagoARealizar = (PagoSalario) getJTablePagos().getValueAt(getJTablePagos().getRowCount() - 1, 5);
			
			DlgPagos dlgPagos = new DlgPagos(getOwner(),pagoARealizar.getTotalpagado(), 2,3,5,6);
			dlgPagos.centerIt();
			dlgPagos.setVisible(true);
			if (dlgPagos.getDetallePagos() == null) {
				throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
			}
			Pago pago = dlgPagos.getPago();
			
			List<DetallePagoSalario> detalles = new ArrayList<DetallePagoSalario>();
			pagoARealizar.setDetallePagoSalario(detalles);
			pagoARealizar.setEstado("P");
			pagoARealizar.setTotalpagado((Double) getJTxtTotalPagar().getValue());
			
			PagoSalario pagoRealizado = controladorPagoSalario.getRealizarPagoSalario(JFramePanambiMain.session.getConn(), pagoARealizar);
			
			pago.setPagoSalario(pagoRealizado);
			pago.setNroRecibo(new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn()));
			Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pagoARealizar.getCodempleado());
			emp.setEstado("I");
			emp.setFechaSalido(new java.util.Date());
			new ControladorEmpleado().modificarEmpleado(JFramePanambiMain.session.getConn(), emp);
			new ControladorPago().registrarPago(JFramePanambiMain.session.getConn(), pago);
			
			if(emp.getUsuario()!=null){
				Usuario usu = emp.getUsuario();
				usu.setEstado("I");
				
				new ControladorUsuario().actualizarUsuario(JFramePanambiMain.session.getConn(), usu, false);
			}
			DlgMessage.showMessage(getOwner(), "Pago registrado exitosamente\nSe ha generado un recibo.", DlgMessage.INFORMATION_MESSAGE);
			
			doGenerarRecibo(pagoRealizado, from);
			doLimpiar();
			getJTxtCodigo().requestFocus();
		} catch (Exception e) {
		
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void doGenerarRecibo(PagoSalario pagoRealizado, String from) {
		try {
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularCompra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			String concepto = "";
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", pagoRealizado.getTotalpagado());
			parameters.put("CLIENTE", "Panambi");
			
			concepto = "Pago de Liquidacion a "+pagoRealizado.getNombre()+" por el periodo de "+pagoRealizado.getFechaDesde()+" al "+pagoRealizado.getFechaHasta();
			
			parameters.put("DESCRIPCION", concepto);
			parameters.put("MONTOLETRAS", "GUARAN�ES "+NumerosALetras.getLetras(pagoRealizado.getTotalpagado()));
			
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;
			Integer numeroRecibo = pagoRealizado.getNumeroRecibo();
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagoSalario(JFramePanambiMain.session.getConn(),numeroRecibo));
			
			List<?> pages = jasperPrint.getPages();
			
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
				
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
			
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}

	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setText("Nombre");
			lblpnmbNombre.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbNombre;
	}
	private JLabelPanambi getLblpnmbSalarioNominal() {
		if (lblpnmbSalarioNominal == null) {
			lblpnmbSalarioNominal = new JLabelPanambi();
			lblpnmbSalarioNominal.setText("Salario Nominal");
			lblpnmbSalarioNominal.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbSalarioNominal;
	}
	private JTextFieldDouble getJTxtSalario() {
		if (jTxtSalario == null) {
			jTxtSalario = new JTextFieldDouble();
			jTxtSalario.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtSalario.setEditable(false);
			jTxtSalario.setDisabledTextColor(Color.BLACK);
			jTxtSalario.setCantDecimals(0);
		}
		return jTxtSalario;
	}
	private JTextFieldUpper getJtxtNombre(){
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
			jTxtNombre.setEditable(false);
		}
		return jTxtNombre;
	}
	
	private JLabelPanambi getLblpnmbAPagar() {
		if (lblpnmbAPagar == null) {
			lblpnmbAPagar = new JLabelPanambi();
			lblpnmbAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
			lblpnmbAPagar.setText("Codigo");
		}
		return lblpnmbAPagar;
	}

	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			
			jTxtCodigo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if(getJTxtCodigo().getValor()==null){
						doLimpiar();
					}
					if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodigo.getValor() != null) {
						Integer codEmpleado = jTxtCodigo.getValor();
						Boolean found = false;
						
						List<Empleado> empleadoLista;
						try {
							empleadoLista = controladorEmpleado.getEmpleadosActivos(JFramePanambiMain.session.getConn(), "",codEmpleado,"",null);
							for (Empleado empleado : empleadoLista) {
								doCargarDatos(empleado);
								found = true;
								break;
							}
						
							if(found == false){
								doPromptEmpleadoPagoSalario();
								
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} 

					}
					super.focusLost(e);
				}
			});
			
			jTxtCodigo.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtCodigo.setDisabledTextColor(Color.RED);
		}
		return jTxtCodigo;
	}
	
	private void doPromptEmpleadoPagoSalario() {
		doLimpiar();
		PromptEmpleadoLiquidacion prompt = new PromptEmpleadoLiquidacion(this);
		prompt.setVisible(true);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setViewportView(getJTablePagos());
		}
		return scrollPane;
	}
	
	private JTablePanambi getJTablePagos() {
		if (jTablePagos == null) {
			String[] columnNames = { "Sel.", "Nro. Item", "Concepto", "Porcentaje", "Monto" ,"Data Pago"," Data Detalle"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, Integer.class);
			types.put(5, Integer.class);
			Integer[] editable = { 0,0 };
			
			jTablePagos = new JTablePanambi(columnNames,editable, types);
			jTablePagos.setColumnSelectionAllowed(true);
			jTablePagos.getColumnModel().getColumn(0).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(0).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(0).setMaxWidth(0);
			jTablePagos.getColumnModel().getColumn(1).setPreferredWidth(10);
			jTablePagos.getColumnModel().getColumn(2).setPreferredWidth(250);
			jTablePagos.getColumnModel().getColumn(3).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(3).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(3).setMaxWidth(0);
			jTablePagos.getColumnModel().getColumn(4).setPreferredWidth(30);
			jTablePagos.getColumnModel().getColumn(5).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(5).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(5).setMaxWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(6).setMaxWidth(0);
			
			jTablePagos.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTablePagos.rowAtPoint(e.getPoint());
						jTablePagos.changeSelection(r, 0, true, true);
						int[] selecteds = jTablePagos.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});

		}
		return jTablePagos;
	}
		
	private void doCargarDatos(Empleado empleado){
		getBtnpnmbGenerarLquidacion().setEnabled(true);
		getJTxtCodigo().setText(empleado.getCodEmpleado().toString());
		getJtxtNombre().setText(empleado.getNombre());
		getJTxtSalario().setText(empleado.getSalario().toString());
		getJTxtDocumento().setText(empleado.getNroDocumento());
		
		//setear la antiguedad
		Calendar cal = Calendar.getInstance();
		DateTime fechaIngreso = new DateTime(empleado.getFechaIngreso().getTime());
		DateTime fechaActual = new DateTime(cal.getTime());
		
		Months months = Months.monthsBetween(fechaIngreso,fechaActual);
		Integer mesesDeAntiguedad = months.getMonths();
		Integer	antiguedad = 0;
		if(mesesDeAntiguedad >= 12){
			antiguedad = mesesDeAntiguedad/12;
			mesesDeAntiguedad = mesesDeAntiguedad - (12*antiguedad);
			
		}
		
		getJTxtMeses().setValue(mesesDeAntiguedad);
		getJTxtAntiguedad().setValue(antiguedad);
		
		Integer vacacionesTotal = 0;
		Double antiguedadD = (double) (antiguedad);
		Double mesesD = (double) mesesDeAntiguedad;
		Double mesesDe = mesesD/100.0;
		
		Double antiguedadDD = antiguedadD + mesesDe;
		
		//setear los dias de pre aviso
		Integer preaviso = 0;
		if(antiguedadDD <=1){
			preaviso = 30;
		}else if(antiguedadDD >1.0 && antiguedadDD <=5.0){
			preaviso = 45;
		}else if(antiguedadDD >5.0 && antiguedadDD <=10.0){
			preaviso = 60;
		}else if(antiguedadDD >10.0){
			preaviso = 90;
		}
		
		if(antiguedadDD <=5.0){
			vacacionesTotal = 12;
		}else if(antiguedadDD >5.0 && antiguedadD <=10.0){
			vacacionesTotal = 18;
		}else if(antiguedadDD >10.0){
			vacacionesTotal = 30;
		}
		
		getJTxtVacacionesTotal().setValor(vacacionesTotal);
		
		getJTxtPreaviso().setValor(preaviso);
		getJTxtEstado().setText("NO GENERADO");
		
		
		getBtnpnmbGenerarLquidacion().setText("Generar");
		PagoSalario pago = null;
		try {
			pago = controladorPagoSalario.getPagoLiquidacion(JFramePanambiMain.session.getConn(), empleado.getCodEmpleado());
			if(pago != null){
				getBtnpnmbGenerarLquidacion().setText("Actualizar");
				getJTxtTotalPagar().setValue(pago.getTotalpagado());
				
				String estado = pago.getEstado();
				if(estado.equals("P")){
					estado = "Pagado";
					getJBtnAplicar().setEnabled(false);
					getJTablePagos().setEnabled(false);
					getBtnCobro().setEnabled(true);
					getJTxtDescuento().setEnabled(true);
					getBtnpnmbGenerarLquidacion().setEnabled(false);
				}else{
					estado= "No pagado";
					getJBtnAplicar().setEnabled(true);
					getJTablePagos().setEnabled(true);
					getBtnCobro().setEnabled(false);
					getBtnpnmbGenerarLquidacion().setEnabled(true);
					
				}
				getJTxtEstado().setText(estado);
				List<DetallePagoSalario> detalles = pago.getDetallePagoSalario();
				getJTablePagos().resetData(0);
				
				
				for (DetallePagoSalario detalle : detalles) {	
					
					getJTablePagos().addRow();
		
					if(detalle.getEstado().equals("I")){
						getJTablePagos().setValueAt(Boolean.FALSE, getJTablePagos().getRowCount() - 1, 0);
					}else{
						getJTablePagos().setValueAt(Boolean.TRUE, getJTablePagos().getRowCount() - 1, 0);
					}			
					
					getJTablePagos().setValueAt(detalle.getNroitem(), getJTablePagos().getRowCount() - 1, 1);
					getJTablePagos().setValueAt(detalle.getConcepto(), getJTablePagos().getRowCount() - 1, 2);
					getJTablePagos().setValueAt(detalle.getPorcentaje(), getJTablePagos().getRowCount() - 1, 3);
					getJTablePagos().setValueAt(detalle.getMonto(), getJTablePagos().getRowCount() - 1, 4);
					getJTablePagos().setValueAt(pago, getJTablePagos().getRowCount() - 1, 5);
					getJTablePagos().setValueAt(detalle, getJTablePagos().getRowCount() - 1, 6);
					
					
				}
				
			}else{
				getJTablePagos().resetData(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private JLabelPanambi getLblpnmbNroDocumento() {
		if (lblpnmbNroDocumento == null) {
			lblpnmbNroDocumento = new JLabelPanambi();
			lblpnmbNroDocumento.setText("Nro. Documento");
			lblpnmbNroDocumento.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbNroDocumento;
	}
	
	private JFormattedTextFieldPanambi getJTxtDocumento() {
		if (jTxtDocumento == null) {
			jTxtDocumento = new JFormattedTextFieldPanambi();
			jTxtDocumento.setEditable(false);
			jTxtDocumento.setFont(new Font("Dialog", Font.PLAIN, 12));
			jTxtDocumento.setDisabledTextColor(Color.RED);
			
		}
		return jTxtDocumento;
	}
	
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setText("Estado");
			lblpnmbEstado.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbEstado;
	}
	private JTextFieldUpper getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextFieldUpper();
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}

	@Override
	public void setValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		if (obj instanceof Empleado) {
			Empleado empleado = (Empleado) obj;
			 doCargarDatos(empleado);
		}
		
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("");
		}
		return lblNewLabel;
	}
	private JTextFieldDouble getJTxtTotalPagar() {
		if (jTxtTotalPagar == null) {
			jTxtTotalPagar = new JTextFieldDouble();
			jTxtTotalPagar.setEditable(false);
		}
		return jTxtTotalPagar;
	}
	private JLabelPanambi getLblpnmbTotalAPagar() {
		if (lblpnmbTotalAPagar == null) {
			lblpnmbTotalAPagar = new JLabelPanambi();
			lblpnmbTotalAPagar.setText("Total a Pagar");
			lblpnmbTotalAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbTotalAPagar;
	}

	private JButtonPanambi getBtnpnmbGenerarLquidacion() {
		if (btnpnmbGenerarLquidacion == null) {
			btnpnmbGenerarLquidacion = new JButtonPanambi();
			btnpnmbGenerarLquidacion.setEnabled(false);
			btnpnmbGenerarLquidacion.setVerticalAlignment(SwingConstants.BOTTOM);
			btnpnmbGenerarLquidacion.setToolTipText("Genera planilla");
			btnpnmbGenerarLquidacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarLiquidacion();
				}
			});
			btnpnmbGenerarLquidacion.setText("Generar Liquidacion");
			btnpnmbGenerarLquidacion.setMnemonic('A');
		}
		return btnpnmbGenerarLquidacion;
	}

	private void doGenerarLiquidacion() {
		try{
			
			Integer vacacionesTotal = getJTxtVacacionesTotal().getValor();
			Integer vacaciones = getJTxtVacaciones().getValor();
			
			if(vacaciones ==null){
				vacaciones = 0;
			}
			
			Integer preavisoTotal = getJTxtPreaviso().getValor();
			Integer preavisoFaltante = getJTxtPreavisoFalta().getValor();
			
			if(preavisoFaltante ==null){
				preavisoFaltante = 0;
			}
			
			if(vacaciones<=vacacionesTotal){
				
				if(preavisoFaltante < preavisoTotal){
					
					int ret = JOptionPane.showConfirmDialog(getOwner(), "<html>Est&aacute seguro que desea generar la liquidacion?.", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
					if(ret == JOptionPane.YES_OPTION){
						
						Integer codempleado = getJTxtCodigo().getValor();
						Integer antiguedad = getJTxtAntiguedad().getValor();
						Integer mesesantiguedad = getJTxtAntiguedad().getValor();
						Integer preaviso = preavisoFaltante;
						Empleado empleado = controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), codempleado);
						
						Date fechaIngreso = empleado.getFechaIngreso();
						Double salarioNominal = empleado.getSalario();
						
						Boolean renuncia = getChckbxpnmbRenuncia().isSelected();
						
						Integer tiporenuncia = 2;
						if(getRdbJustificado().isSelected()){
							tiporenuncia = 1;
						}
						
						controladorPagoSalario.generarPagoLiquidacion(JFramePanambiMain.session.getConn(), codempleado, antiguedad, mesesantiguedad, preaviso, fechaIngreso, renuncia, tiporenuncia, salarioNominal, vacaciones);
						
						doCargarDatos(empleado);
					}
				}else{
		    		DlgMessage.showMessage(getOwner(), "La cantidad de dias de pre aviso faltantes no puede ser mayor a la mostrada\nFavor modifique la cantidad.", DlgMessage.WARNING_MESSAGE);

				}
			
			}else{
	    		DlgMessage.showMessage(getOwner(), "La cantidad de dias de vacaciones disponibles no puede ser mayor a la mostrada\nFavor modifique la cantidad.", DlgMessage.WARNING_MESSAGE);
	    	}
			
		} catch (Exception e) {
		
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	private JLabelPanambi getLblpnmbAntiguedad() {
		if (lblpnmbAntiguedad == null) {
			lblpnmbAntiguedad = new JLabelPanambi();
			lblpnmbAntiguedad.setText("Antiguedad(a\u00F1os)");
			lblpnmbAntiguedad.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbAntiguedad;
	}
	private JLabelPanambi getLblpnmbPreAvisodias() {
		if (lblpnmbPreAvisodias == null) {
			lblpnmbPreAvisodias = new JLabelPanambi();
			lblpnmbPreAvisodias.setText("Pre aviso(dias)");
			lblpnmbPreAvisodias.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbPreAvisodias;
	}
	private JTextFieldInteger getJTxtPreaviso() {
		if (jTxtPreaviso == null) {
			jTxtPreaviso = new JTextFieldInteger();
			jTxtPreaviso.setEditable(false);
		}
		return jTxtPreaviso;
	}
	private JTextFieldDouble getJTxtMeses() {
		if (jTxtMeses == null) {
			jTxtMeses = new JTextFieldDouble();
			jTxtMeses.setEditable(false);
		}
		return jTxtMeses;
	}
	private JTextFieldInteger getJTxtAntiguedad() {
		if (jTxtAntiguedad == null) {
			jTxtAntiguedad = new JTextFieldInteger();
			jTxtAntiguedad.setEditable(false);
		}
		return jTxtAntiguedad;
	}
	private JCheckBoxPanambi getChckbxpnmbRenuncia() {
		if (chckbxpnmbRenuncia == null) {
			chckbxpnmbRenuncia = new JCheckBoxPanambi();
			chckbxpnmbRenuncia.setText("Renuncia");
		}
		return chckbxpnmbRenuncia;
	}
	
	private JRadioButton getRdbJustificado() {
		if (rdbJustificado == null) {
			rdbJustificado = new JRadioButton("Justificado");
			rdbJustificado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionTipoJustificado();
				}
			});
		}
		return rdbJustificado;
	}
	
	private JRadioButton getRdbInjustificado() {
		if (rdbInjustificado == null) {
			rdbInjustificado = new JRadioButton("Injustificado/Normal");
			rdbInjustificado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionTipoInjustificado();
				}
			});
		}
		return rdbInjustificado;
	}
	
	private void doSeleccionTipoInjustificado() {
		
		if(getRdbInjustificado().isSelected()){
			//getRdbNormal().setSelected(false);
			getRdbJustificado().setSelected(false);
			getRdbInjustificado().setSelected(true);
			
		}else{
			//getRdbNormal().setSelected(true);
			getRdbJustificado().setSelected(true);
			getRdbInjustificado().setSelected(false);
			
		}
		
	}
	
	private void doSeleccionTipoJustificado() {
			
			if(getRdbJustificado().isSelected()){
				//getRdbNormal().setSelected(false);
				getRdbJustificado().setSelected(true);
				getRdbInjustificado().setSelected(false);
				
			}else{
				//getRdbNormal().setSelected(true);
				getRdbJustificado().setSelected(false);
				getRdbInjustificado().setSelected(true);
				
			}
			
	}
	
	private JButton getBtnLimpiar() {
		if (btnLimpiar == null) {
			btnLimpiar = new JButton("Limpiar");
			btnLimpiar.setToolTipText("Limpia la pantalla");
			btnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJTxtCodigo().requestFocus();
				}
			});
			btnLimpiar.setMnemonic('L');
		}
		return btnLimpiar;
	}
	
	public void doLimpiar(){
		getJBtnAplicar().setEnabled(false);
		getJTxtAntiguedad().setValor(0);
		getJTxtCodigo().setValor(null);
		getJTxtDocumento().setText("");
		getJTxtEstado().setText("");
		getJTxtMeses().setValue(0);
		getJtxtNombre().setText("");
		getJTxtPreaviso().setValor(0);
		getJTxtTotalPagar().setValue(0.0);
		getJTablePagos().resetData(0);
		getJTxtSalario().setValue(0);
		getRdbJustificado().setSelected(true);
		getRdbInjustificado().setSelected(false);
		getJTxtVacaciones().setValor(0);
		getJTxtVacacionesTotal().setValor(0);
		getBtnpnmbGenerarLquidacion().setText("Generar");
		getBtnpnmbGenerarLquidacion().setEnabled(false);
		getJTxtPreavisoFalta().setValor(0);
		getJTxtDescuento().setValue(0.0);
		getBtnCobro().setEnabled(false);
		getJTxtDescuento().setEnabled(false);
		getBtnpnmbGenerarLquidacion().setEnabled(false);
		
	}
	private JTextFieldInteger getJTxtVacaciones() {
		if (jTxtVacaciones == null) {
			jTxtVacaciones = new JTextFieldInteger();
		}
		return jTxtVacaciones;
	}
	private JLabelPanambi getLblpnmbVacaciones() {
		if (lblpnmbVacaciones == null) {
			lblpnmbVacaciones = new JLabelPanambi();
			lblpnmbVacaciones.setText("Vacaciones");
			lblpnmbVacaciones.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbVacaciones;
	}
	private JTextFieldInteger getJTxtVacacionesTotal() {
		if (jTxtVacacionesTotal == null) {
			jTxtVacacionesTotal = new JTextFieldInteger();
			jTxtVacacionesTotal.setEditable(false);
		}
		return jTxtVacacionesTotal;
	}
	
	public void doRecibo(boolean generar, String titular, String concepto, String montoLetras, Double monto, Integer nrorecibo, Double vuelto) {
		// TODO Auto-generated method stub
		
		if(generar ==true){
			try{
			PanambiUtils.setWaitCursor(getOwner());
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularCompra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", monto);
		
			parameters.put("CLIENTE", titular);

			parameters.put("DESCRIPCION", concepto);
			parameters.put("MONTOLETRAS", montoLetras);
			
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
	
			JasperPrint jasperPrint;
	
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceReciboPagoCuota(JFramePanambiMain.session.getConn(),nrorecibo));
		
			List<?> pages = jasperPrint.getPages();
		
			if (!pages.isEmpty()) {
				String pdffilename = System.getProperty("java.io.tmpdir") + "\\tmp_panambi_" + System.nanoTime() + ".pdf";
				JasperExportManager.exportReportToPdfFile(jasperPrint, pdffilename);
				Desktop desk = Desktop.getDesktop();
				desk.open(new File(pdffilename));
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
		}
		
	}

	private JButton getBtnSalir() {
		if (btnSalir == null) {
			btnSalir = new JButton("Salir");
			btnSalir.setToolTipText("Sale de la pantalla");
			btnSalir.setMnemonic('S');
			btnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
		}
		return btnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JTextFieldInteger getJTxtPreavisoFalta() {
		if (JTxtPreavisoFalta == null) {
			JTxtPreavisoFalta = new JTextFieldInteger();
		}
		return JTxtPreavisoFalta;
	}
	private JLabelPanambi getLblpnmbTipoDeRecision() {
		if (lblpnmbTipoDeRecision == null) {
			lblpnmbTipoDeRecision = new JLabelPanambi();
			lblpnmbTipoDeRecision.setText("Tipo de Recision:");
			lblpnmbTipoDeRecision.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbTipoDeRecision;
	}
	private JButton getBtnCobro() {
		if (btnCobro == null) {
			btnCobro = new JButton("Cobro");
			btnCobro.setToolTipText("Cobro de Saldo");
			btnCobro.setMnemonic('C');
			btnCobro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCobrar();
				}
			});
		}
		return btnCobro;
	}
	
	@SuppressWarnings("unused")
	public void doCobrar() {
		int ret = JOptionPane.showConfirmDialog(getOwner(), "<html>Est&aacute seguro que desea realizar el cobro?.", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(ret == JOptionPane.YES_OPTION){
			
			if(getJTxtDescuento().getValue() == null){
				getJTxtDescuento().setValue(0.0);
			}
			Double descuento = (Double) getJTxtDescuento().getValue();
			
			try {
				PagoSalario pagoEmpleado = (PagoSalario) getJTablePagos().getValueAt(getJTablePagos().getRowCount() - 1, 5);
				DeudaEmpleado deuda = controladordeudaempleado.getDeudaEmpleadoPorPago(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodpagosalario());
				if(deuda.getEstado().equals("P")){
					throw new ValidException("La deuda del empleado se encuentra Pagada");
				}
				
				if(deuda==null){
					if(descuento != 0.0){
						deuda = new DeudaEmpleado();
						deuda.setConcepto("DESCUENTO");
						deuda.setMonto(descuento);
						deuda.setPagoEmpleado(pagoEmpleado);
						deuda.setEstado("NP");
						controladordeudaempleado.insertarDeudaEmpleado(JFramePanambiMain.session.getConn(),deuda);
					}
					//else{
						//DlgMessage.showMessage(getOwner(), "No existe monto pendiente de cobro.", DlgMessage.INFORMATION_MESSAGE);
					//}
					
				}else{
					Double monto = deuda.getMonto() + descuento;
					String concepto = deuda.getConcepto() + "Y DESCUENTO";
	
					deuda.setMonto(monto);
					deuda.setConcepto(concepto);
					controladordeudaempleado.modificarDeudaEmpleado(JFramePanambiMain.session.getConn(), deuda);
					
				}
				
				deuda = controladordeudaempleado.getDeudaEmpleadoPorPago(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodpagosalario());
				
				if(deuda != null){
					DlgPagos dlgPagos = new DlgPagos(getOwner(),deuda.getMonto(), 2,3,5,6);
					dlgPagos.centerIt();
					dlgPagos.setVisible(true);
					if (dlgPagos.getDetallePagos() == null) {
			
						throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
					}
					
					Pago pago = dlgPagos.getPago();
					deuda.setEstado("P");
					pago.setDeudaEmpleado(deuda);
					pago.setNroRecibo(new ControladorPlanPago().getNroRecibo(JFramePanambiMain.session.getConn()));
					new ControladorDeudaEmpleado().setDeudaPagada(JFramePanambiMain.session.getConn(), deuda, pago);
					
					DlgMessage.showMessage(getOwner(), "Pago registrado exitosamente\nSe ha generado un recibo.", DlgMessage.INFORMATION_MESSAGE);
					
					doGenerarRecibo(deuda, pago);
					doLimpiar();
					getJTxtCodigo().requestFocus();
					
					
				}else{
					DlgMessage.showMessage(getOwner(), "No existe monto pendiente de cobro.", DlgMessage.INFORMATION_MESSAGE);

				}	
	
			} catch (ValidException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
			}
			
		}
		
	}
	
	private void doGenerarRecibo(DeudaEmpleado deuda, Pago pago) {
		try {
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularCompra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			String concepto = "";
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			parameters.put("MONTOTOTAL", deuda.getMonto());
			parameters.put("CLIENTE", deuda.getPagoEmpleado().getNombre());
			
			concepto = "Cobro de deuda a "+deuda.getPagoEmpleado().getNombre();
			
			parameters.put("DESCRIPCION", concepto);
			parameters.put("MONTOLETRAS", "GUARAN�ES "+NumerosALetras.getLetras(pago.getMonto()));
			
			URL url = DlgPagoCuota.class.getResource("/py/com/panambi/informes/reports/JRReciboPagoCuota.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;
			Integer numeroRecibo = pago.getNroRecibo();
			
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagoSalario(JFramePanambiMain.session.getConn(),numeroRecibo));
			
			List<?> pages = jasperPrint.getPages();
			
			if (!pages.isEmpty()) {
				JRPdfExporter exporter = new JRPdfExporter();
				File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.exportReport();
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(filetmp);
				}
				
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
			
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}
	private JLabelPanambi getLblpnmbDescuento() {
		if (lblpnmbDescuento == null) {
			lblpnmbDescuento = new JLabelPanambi();
			lblpnmbDescuento.setText("Descuento");
			lblpnmbDescuento.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbDescuento;
	}
	private JTextFieldDouble getJTxtDescuento() {
		if (jTxtDescuento == null) {
			jTxtDescuento = new JTextFieldDouble();
		}
		return jTxtDescuento;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Presione F1 para Ayuda");
			labelPanambi.setForeground(Color.GRAY);
		}
		return labelPanambi;
	}
}
