package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorCliente;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.informes.JIGenerarCodigoBarra;
import py.com.panambi.informes.JIReporteCompras;
import py.com.panambi.informes.JRDataSourceFactura;
import py.com.panambi.informes.JRDataSourcePagare;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.prompts.PromptProductos;
import py.com.panambi.utils.NumerosALetras;
import py.com.panambi.utils.PanambiUtils;

public class JIVenta extends JInternalFramePanambi implements Browseable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5692956630618496093L;
	private JLabelPanambi lblpnmbProveedor;
	private JScrollPane jScrollPaneProductos;
	private JTablePanambi jTableDetalle;
	private JButtonPanambi btnpnmbAgregarItem;
	private JButtonPanambi jBtnRegistrarVenta;
	private JButtonPanambi jBtnSalir;
	private JButtonPanambi jBtnLimpiar;
	private JLabelPanambi labelProducto;
	private JTextFieldInteger jTxtCodProducto;
	private JTextFieldUpper jTxtProducto;
	private JLabelPanambi labelCantidad;
	private JTextFieldInteger jTxtCantidad;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private ControladorCliente controladorCliente = new ControladorCliente();
	private Producto producto;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldDouble jTxtTotal;
	JPopupMenu popupMenu = new JPopupMenu();
	private JTextFieldUpper jTxtDocumento;
	private JLabelPanambi jLabelCliente;
	private Cliente cliente;
	private JLabelPanambi lblpnmbDescuento;
	private JTextFieldInteger jTxtDescuento;
	private JLabelPanambi jLabelMaxDescuento;
	private JLabelPanambi jLabelPrecioLista;
	private DecimalFormat decimalFormat = new DecimalFormat("#,##0");
	private JLabelPanambi labelPanambi;
	private JComboBoxPanambi jCmbSucursal;
	private JLabel jLblImage;
	private List<Sucursal> sucursales = new ArrayList<Sucursal>();

	/**
	 * Create the frame.
	 */
	public JIVenta() throws Exception {
		initialize();
	}

	@SuppressWarnings("unchecked")
	private void doNuevo() {
		try {
			getJTxtDocumento().setText(null);
			getJTableDetalle().resetData(0);
			getJTxtCantidad().setValor(null);
			getJTxtCodProducto().setValor(null);
			getJLblImage().setIcon(null);
			getJTxtTotal().setValue(0.0);
			getJLabelCliente().setText("");
			poblarSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<Sucursal>(sucursales));
			Empleado empleado = new Empleado();
			empleado.getSucursal();
			empleado.getUsuario();
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa());
			cliente = null;

			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if (JFramePanambiMain.session.getUsuario().getPerfilesUsuario() != null) {
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}

			if (perfilesUsuario != null) {
				for (int i = 0; i < perfilesUsuario.size(); i++) {
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if (perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")) {
						admin = true;
					}
				}
			}

			if (admin) {
				getJCmbSucursal().setEnabled(true);
			} else {
				getJCmbSucursal().setEnabled(false);
			}
			getJTxtDocumento().requestFocus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void initialize() {
		setMaximizable(false);
		setTitle("Registro de Ventas");
		setBounds(100, 100, 1136, 524);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLabelProducto(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtProducto(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelCantidad(), GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtCantidad(), GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLblpnmbDescuento(), GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJTxtDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getBtnpnmbAgregarItem(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJLabelMaxDescuento(), GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getJLabelPrecioLista(), GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
							.addGap(180))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE))
							.addGap(12)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJLabelCliente(), GroupLayout.PREFERRED_SIZE, 551, GroupLayout.PREFERRED_SIZE))
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, 139, Short.MAX_VALUE)
							.addComponent(getJLblImage(), GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
							.addGap(185))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
								.addComponent(getJScrollPaneProductos(), GroupLayout.PREFERRED_SIZE, 1098, GroupLayout.PREFERRED_SIZE))
							.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(32, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJLblImage(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbProveedor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getJTxtDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJLabelCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelCantidad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCantidad(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnpnmbAgregarItem(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJLabelMaxDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJLabelPrecioLista(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getJScrollPaneProductos(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(59))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnRegistrarVenta());
		getJPanelSouth().add(getJBtnSalir());

		JMenuItem menuItem4 = new JMenuItem("Eliminar Item");
		menuItem4.setFont(FontPanambi.getLabelInstance());
		menuItem4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				doEliminarDetalle();
			}
		});
		popupMenu.add(menuItem4);
		this.setOpaque(true);
		this.addInternalFrameListener(new javax.swing.event.InternalFrameAdapter() {

			public void internalFrameClosing(javax.swing.event.InternalFrameEvent e) {
				cleanPopPups();
			}

			public void internalFrameClosed(javax.swing.event.InternalFrameEvent e) {
				cleanPopPups();
			}
		});
		centerIt();
		setShortcuts(this);
		setHelp("registrarVenta");
		doNuevo();
	}

	/**
	 * Elimina item del detalle
	 */
	private void doEliminarDetalle() {
		if (getJTableDetalle().getSelectedRow() > -1) {
			int response = JOptionPane.showConfirmDialog(getOwner(), "<html>Eliminando el detalle de la venta.<br><br>Desea continuar?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.YES_OPTION) {
				getJTableDetalle().removeRow(getJTableDetalle().getSelectedRow());
			}
			actualizaTotal();
		}
	}

	private void cleanPopPups() {
		popupMenu.setVisible(false);
	}

	private JLabelPanambi getLblpnmbProveedor() {
		if (lblpnmbProveedor == null) {
			lblpnmbProveedor = new JLabelPanambi();
			lblpnmbProveedor.setText("Cliente");
		}
		return lblpnmbProveedor;
	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = (Producto) obj;
			getJTxtCodProducto().setValor(producto.getCodProducto());
			getJTxtProducto().setText(producto.getDescripcion());
			getJLabelMaxDescuento().setText("Max.Descuento: " + decimalFormat.format(Math.round((producto.getPrecio() * producto.getMaxDescuento() / 100))));
			getJLabelPrecioLista().setText("Precio Lista: " + decimalFormat.format(producto.getPrecio()));
			getJLblImage().setText(null);
			InputStream in = null;
			if (producto.getImagenProducto() != null) {
				try {
					in = new ByteArrayInputStream(producto.getImagenProducto().getImagen());
					BufferedImage image = null;
					image = ImageIO.read(in);
					getJLblImage().setIcon(new ImageIcon(image.getScaledInstance(getJLblImage().getWidth(), getJLblImage().getHeight(), Image.SCALE_SMOOTH)));

				} catch (Exception e) {
					e.printStackTrace();
					getJLblImage().setIcon(null);
					JOptionPane.showMessageDialog(this, "No se pudo obtener imagen");
				}
				getJTxtCantidad().requestFocus();
			} else {
				getJLblImage().setIcon(null);
			}
			getJTxtCantidad().requestFocus();
		}
		if (obj instanceof Cliente) {
			cliente = (Cliente) obj;
			getJTxtDocumento().setText(cliente.getNroDocumento());
			getJLabelCliente().setText(cliente.getNombres() + " " + cliente.getApellidos());
			getJTxtCodProducto().requestFocus();
		}
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = null;
			getJTxtProducto().setText(null);
			getJTxtCodProducto().requestFocus();
		}
		if (obj instanceof Cliente) {
			cliente = null;
			getJLabelCliente().setText(null);
			getJTxtDocumento().requestFocus();
		}
	}

	private JScrollPane getJScrollPaneProductos() {
		if (jScrollPaneProductos == null) {
			jScrollPaneProductos = new JScrollPane();
			jScrollPaneProductos.setViewportView(getJTableDetalle());
		}
		return jScrollPaneProductos;
	}

	private JTablePanambi getJTableDetalle() {
		if (jTableDetalle == null) {
			String[] columnNames = { "Cant.", "Producto", "Costo Unitario", "Total", "Data" };
			jTableDetalle = new JTablePanambi(columnNames);
			jTableDetalle.getColumnModel().getColumn(0).setPreferredWidth(20);
			jTableDetalle.getColumnModel().getColumn(1).setPreferredWidth(300);
			jTableDetalle.getColumnModel().getColumn(2).setPreferredWidth(100);
			jTableDetalle.getColumnModel().getColumn(3).setPreferredWidth(120);
			jTableDetalle.getColumnModel().getColumn(4).setMinWidth(0);
			jTableDetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
			jTableDetalle.getColumnModel().getColumn(4).setMaxWidth(0);
			jTableDetalle.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTableDetalle.rowAtPoint(e.getPoint());
						jTableDetalle.changeSelection(r, 0, true, true);
						int[] selecteds = jTableDetalle.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});
			jTableDetalle.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						doEliminarDetalle();
					}
					super.keyPressed(e);
				}
			});
		}
		return jTableDetalle;
	}

	private JButtonPanambi getBtnpnmbAgregarItem() {
		if (btnpnmbAgregarItem == null) {
			btnpnmbAgregarItem = new JButtonPanambi();
			btnpnmbAgregarItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarItem();
				}
			});
			btnpnmbAgregarItem.setMnemonic('I');
			btnpnmbAgregarItem.setText("Agregar Item");
		}
		return btnpnmbAgregarItem;
	}

	/**
	 * Agrega cada item de producto comprado
	 */
	private void doAgregarItem() {
		try {
			checkAddItem();
			int descuento = (getJTxtDescuento().getValor() == null ? 0 : getJTxtDescuento().getValor());
			boolean yaexiste = false;
			for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
				if (producto.getCodProducto().equals(((DetalleVenta) getJTableDetalle().getValueAt(i, 4)).getProducto().getCodProducto())) {
					yaexiste = true;
					getJTableDetalle().setValueAt((((Integer) getJTableDetalle().getValueAt(i, 0)) + getJTxtCantidad().getValor()), i, 0);
					getJTableDetalle().setValueAt(producto.getDescripcion(), i, 1);
					getJTableDetalle().setValueAt((producto.getPrecio() - descuento), i, 2);
					double totallinea = ((Integer) getJTableDetalle().getValueAt(i, 0)) * (producto.getPrecio() - descuento);
					getJTableDetalle().setValueAt(totallinea, i, 3);
					DetalleVenta detalleVenta = new DetalleVenta();
					detalleVenta.setCantidad(((Integer) getJTableDetalle().getValueAt(i, 0)));
					detalleVenta.setPrecioUnitario((producto.getPrecio() - descuento));
					detalleVenta.setNroitem(getJTableDetalle().getRowCount());
					detalleVenta.setProducto(producto);
					detalleVenta.setTotalItem(totallinea);
					detalleVenta.setDescuentoUnitario(Double.valueOf(Integer.toString(descuento)));
					getJTableDetalle().setValueAt(detalleVenta, i, 4);
				}
			}
			if (!yaexiste) {
				getJTableDetalle().addRow();
				getJTableDetalle().setValueAt(getJTxtCantidad().getValor(), getJTableDetalle().getRowCount() - 1, 0);
				getJTableDetalle().setValueAt(producto.getDescripcion(), getJTableDetalle().getRowCount() - 1, 1);
				getJTableDetalle().setValueAt((producto.getPrecio() - descuento), getJTableDetalle().getRowCount() - 1, 2);
				double totallinea = getJTxtCantidad().getValor() * (producto.getPrecio() - descuento);
				getJTableDetalle().setValueAt(totallinea, getJTableDetalle().getRowCount() - 1, 3);
				DetalleVenta detalleVenta = new DetalleVenta();
				detalleVenta.setCantidad(getJTxtCantidad().getValor());
				detalleVenta.setPrecioUnitario((producto.getPrecio() - descuento));
				detalleVenta.setNroitem(getJTableDetalle().getRowCount());
				detalleVenta.setProducto(producto);
				detalleVenta.setTotalItem(totallinea);
				detalleVenta.setDescuentoUnitario(Double.valueOf(Integer.toString(descuento)));
				getJTableDetalle().setValueAt(detalleVenta, getJTableDetalle().getRowCount() - 1, 4);
			}
			actualizaTotal();
			initializeItem();
		} catch (ValidException e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			if (e.getMessage().contains("producto")) {
				getJTxtCodProducto().requestFocus();
			}
			if (e.getMessage().contains("cantidad")) {
				getJTxtCantidad().requestFocus();
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void actualizaTotal() {
		double total = 0.0;
		for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
			total += (Double) getJTableDetalle().getValueAt(i, 3);
		}
		getJTxtTotal().setValue(total);
	}

	private void initializeItem() {
		getJTxtCodProducto().setValor(null);
		getJTxtCantidad().setValor(null);
		getJTxtProducto().setText(null);
		getJTxtDescuento().setValor(0);
		getJLabelMaxDescuento().setText(null);
		getJLabelPrecioLista().setText(null);
		getJLblImage().setIcon(null);
		producto = null;
		getJTxtCodProducto().requestFocus();

	}

	private void checkAddItem() throws Exception {
		if (producto == null) {
			throw new ValidException("Debe seleccionar un producto");
		}
		if (getJTxtCantidad().getValor() == null || getJTxtCantidad().getValor() < 1) {
			throw new ValidException("Debe ingresar una cantidad > 0");
		}
		int stockDisponible = controladorProducto.getStockDisponible(JFramePanambiMain.session.getConn(), producto, (Sucursal) getJCmbSucursal().getSelectedItem());
		if (stockDisponible < getJTxtCantidad().getValor()) {
			throw new ValidException("No existe en stock la cantidad del producto selecciona. Stock actual: " + stockDisponible);
		}

		if (getJTxtDescuento().getValor() != null && getJTxtDescuento().getValor() > Math.round(producto.getMaxDescuento() * producto.getPrecio() / 100)) {
			getJTxtDescuento().requestFocus();
			throw new ValidException("El descuento no puede exceder el m�ximo establecido");
		}

		for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
			if (producto.getCodProducto().equals(((DetalleVenta) getJTableDetalle().getValueAt(i, 4)).getProducto().getCodProducto())) {
				if (getJTxtDescuento().getValor() != null) {
					if (getJTxtDescuento().getValor() != ((DetalleVenta) getJTableDetalle().getValueAt(i, 4)).getDescuentoUnitario().intValue()) {
						throw new ValidException("Producto agregado anteriormente con descuento distinto");
					}
				}
			}
		}
	}

	private JButtonPanambi getJBtnRegistrarVenta() {
		if (jBtnRegistrarVenta == null) {
			jBtnRegistrarVenta = new JButtonPanambi();
			jBtnRegistrarVenta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarVenta();
				}
			});
			jBtnRegistrarVenta.setMnemonic('R');
			jBtnRegistrarVenta.setText("Registrar Venta");
		}
		return jBtnRegistrarVenta;
	}

	/**
	 * Registra la Venta
	 */
	private void doRegistrarVenta() {
		Connection conn = JFramePanambiMain.session.getConn();
		try {
			if(getJTableDetalle().getRowCount()!=0){
				if(getJTxtDocumento().getText().equals("")){
					DlgMessage.showMessage(getOwner(), "Debe seleccionar un cliente",DlgMessage.WARNING_MESSAGE);
					getJTxtDocumento().requestFocus();
				}else{
					
					ControladorVenta controladorVenta = new ControladorVenta();
					Venta venta = new Venta();
					venta.setTipoVenta("D");
					venta.setTotal((Double) getJTxtTotal().getValue());
					venta.setCliente(cliente);
					venta.setUsuario(JFramePanambiMain.session.getUsuario());
					venta.setSucursal((Sucursal) getJCmbSucursal().getSelectedItem());
					List<DetalleVenta> detalle = new ArrayList<DetalleVenta>();
					for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
						DetalleVenta detalleVenta = (DetalleVenta) getJTableDetalle().getValueAt(i, 4);
						detalle.add(detalleVenta);
					}
					venta.setTotalLetras(NumerosALetras.getLetras(venta.getTotal()));
					venta.setDetalle(detalle);
					DlgPagos dlgPagos = new DlgPagos(getOwner(), venta.getTotal(), venta.getCliente());
					dlgPagos.centerIt();
					dlgPagos.setCliente(venta.getCliente());
					dlgPagos.setVisible(true);
					if (dlgPagos.getDetallePagos() == null) {
						throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
					}
					Pago pago = dlgPagos.getPago();
					pago.setVenta(venta);
					Integer codVenta = null;
	
					///Verificando cuotero
					if (dlgPagos.getDetallePlanPagos() != null && !dlgPagos.getDetallePlanPagos().isEmpty()) {
						venta.setTipoVenta("C");
						codVenta = controladorVenta.registrarVenta(conn, venta, dlgPagos.getDetallePlanPagos());
						doGenerarPagare(codVenta);
						
					} else {
						codVenta = controladorVenta.registrarVenta(conn, pago);
					}
					venta = controladorVenta.getVenta(conn, codVenta);
					venta.setDetalle(detalle);
					Map<String, Object> parameters = new HashMap<String, Object>();
					javax.swing.ImageIcon icon = new ImageIcon(JIReporteCompras.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
					Image image = icon.getImage();
					parameters.put("LOGO", image);
					parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
					parameters.put("TOTAL", venta.getTotal());
					parameters.put("TOTALLETRAS", venta.getTotalLetras());
					parameters.put("IVA", venta.getIva());
					URL url = JIVenta.class.getResource("/py/com/panambi/informes/reports/JRFactura.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					JRDataSourceFactura jrFactura = new JRDataSourceFactura(venta);
					parameters.put("JRDATASOURCE", jrFactura);
					JRDataSourceFactura jrFactura2 = new JRDataSourceFactura(venta);
					parameters.put("JRDATASOURCE2", jrFactura2);
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceFactura(venta));
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}
					doNuevo();
					getJTxtDocumento().requestFocus();
				}
			}else{
				DlgMessage.showMessage(getOwner(), "Debe agregar items a la venta", DlgMessage.WARNING_MESSAGE);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void doGenerarPagare(Integer codVenta){
		
		try {	

			Venta venta = null;
			try{

				venta = new ControladorVenta().getVenta(JFramePanambiMain.session.getConn(), codVenta);
			}catch(Exception e){
				codVenta  = 0;
			}
			
			if(venta!=null){
				if(venta.getTipoVenta().equals("C")){
					PanambiUtils.setWaitCursor(getOwner());
					Map<String, Object> parameters = new HashMap<String, Object>();
					
					parameters.put("PAGARENRO", venta.getNroPagare());
					
					String patronDia = "dd";
					SimpleDateFormat formatoDia = new SimpleDateFormat(patronDia);
					parameters.put("DIA", formatoDia.format(venta.getFecha()));
					
					String patronMes = "MM";
					SimpleDateFormat formatoMes = new SimpleDateFormat(patronMes);
					String numMes = formatoMes.format(venta.getFecha()); 
					String mes = "";
					
					if(numMes.equals("01")){
						mes = "ENERO";
					}else if(numMes.equals("02")){
						mes = "FEBRERO";
					}else if(numMes.equals("03")){
						mes = "MARZO";
					}else if(numMes.equals("04")){
						mes = "ABRIL";
					}else if(numMes.equals("05")){
						mes = "MAYO";
					}else if(numMes.equals("06")){
						mes = "JUNIO";
					}else if(numMes.equals("07")){
						mes = "JULIO";
					}else if(numMes.equals("08")){
						mes = "AGOSTO";
					}else if(numMes.equals("09")){
						mes = "SETIEMBRE";
					}else if(numMes.equals("10")){
						mes = "OCTUBRE";
					}else if(numMes.equals("11")){
						mes = "NOVIEMBRE";
					}else {
						mes = "DICIEMBRE";
					}
					
					parameters.put("MES", mes);
					
					String patronAnho = "yyyy";
					SimpleDateFormat formatoAnho= new SimpleDateFormat(patronAnho);
					
					parameters.put("ANHO", formatoAnho.format(venta.getFecha()));
					
					URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRPagareCuotas.jasper");
					JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
					
					JasperPrint jasperPrint;
					
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourcePagare(JFramePanambiMain.session.getConn(), venta));
					
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}

				}else{
					DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nFactura contado, debe ingresar una factura a cr�dito." , DlgMessage.ERROR_MESSAGE);
				}
			}else{
				DlgMessage.showMessage(getOwner(), "Imposible imprimir pagar�.\nLa factura no existe." , DlgMessage.ERROR_MESSAGE);
			}
							
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}

	private void doSalir() {
		this.dispose();
	}

	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}

	private void doLimpiar() {
		doNuevo();
	}

	private JLabelPanambi getLabelProducto() {
		if (labelProducto == null) {
			labelProducto = new JLabelPanambi();
			labelProducto.setText("Producto");
		}
		return labelProducto;
	}

	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
			jTxtCodProducto.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					try{
						if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() != null) {
							Integer codproducto = ((JTextFieldInteger) e.getComponent()).getValor();
							setProductoFromCod(codproducto);
						}
						if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() == null) {
							producto = null;
							getJTxtCodProducto().setValor(null);
							getJTxtProducto().setText("");
						}
						super.focusLost(e);
					}catch(Exception e1){
						producto = null;
						getJTxtCodProducto().setValor(null);
						getJTxtProducto().setText("");
						getJLblImage().setIcon(null);
						doPromptProducto();
					}
					
				}
			});
		}
		return jTxtCodProducto;
	}

	private void setClienteFromDocumento(String documento) {
		try {
			if (documento == null || documento.trim().isEmpty()) {
				setNoValues(new Cliente(), null);
				throw new ValidException();
			}
			cliente = controladorCliente.getCliente(JFramePanambiMain.session.getConn(), documento);
			if (cliente != null) {
				setValues(cliente, null);
			} else {
				setNoValues(new Cliente(), null);
				throw new ValidException();
			}
		} catch (ValidException e) {
			cliente = null;
			getJTxtDocumento().setText(null);
			getJLabelCliente().setText("");
			doPromptCliente();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void doPromptCliente() {
		String[] columnNames = { "Codigo", "Documento", "Nombres", "Apellidos" };
		String sSelect = "SELECT codcliente, nrodocumento, nombres, apellidos FROM clientes WHERE estado = 'A' order by nombres";
		String[] pks = { "codcliente" };
		JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Cliente", pks);
		jb.setVisible(true);
	}

	private JTextFieldUpper getJTxtProducto() {
		if (jTxtProducto == null) {
			jTxtProducto = new JTextFieldUpper();
			jTxtProducto.setEnabled(false);
		}
		return jTxtProducto;
	}

	private JLabelPanambi getLabelCantidad() {
		if (labelCantidad == null) {
			labelCantidad = new JLabelPanambi();
			labelCantidad.setText("Cantidad");
		}
		return labelCantidad;
	}

	private JTextFieldInteger getJTxtCantidad() {
		if (jTxtCantidad == null) {
			jTxtCantidad = new JTextFieldInteger();
		}
		return jTxtCantidad;
	}

	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setFont(new Font("Dialog", Font.BOLD, 11));
			lblpnmbTotal.setText("Total");
		}
		return lblpnmbTotal;
	}

	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			jTxtTotal.setFont(new Font("Dialog", Font.BOLD, 11));
			jTxtTotal.setEnabled(false);
			jTxtTotal.setDisabledTextColor(Color.BLACK);
			jTxtTotal.setValue(0.0);
		}
		return jTxtTotal;
	}

	private JTextFieldUpper getJTxtDocumento() {
		if (jTxtDocumento == null) {
			jTxtDocumento = new JTextFieldUpper();
			jTxtDocumento.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(e.getOppositeComponent() instanceof JButton) && !jTxtDocumento.getText().trim().isEmpty()) {
						String documento = ((JTextFieldUpper) e.getComponent()).getText();
						setClienteFromDocumento(documento);
					}
					if (!(e.getOppositeComponent() instanceof JButton) && jTxtDocumento.getText().trim().isEmpty()) {
						cliente = null;
						getJTxtDocumento().setText(null);
						getJLabelCliente().setText("");
					}
					super.focusLost(e);
				}
			});

		}
		return jTxtDocumento;
	}

	private JLabelPanambi getJLabelCliente() {
		if (jLabelCliente == null) {
			jLabelCliente = new JLabelPanambi();
			jLabelCliente.setFont(new Font("Dialog", Font.BOLD, 11));
			jLabelCliente.setVerticalAlignment(SwingConstants.TOP);
		}
		return jLabelCliente;
	}

	protected void setProductoFromCod(Integer codproducto) {
		try {
			if (codproducto == null) {
				throw new ValidException();
			}
			Producto producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), codproducto);
			if (producto != null) {
				setValues(producto, null);
			} else {
				setNoValues(new Producto(), null);
			}
		} catch (ValidException e) {
			producto = null;
			getJTxtCodProducto().setValor(null);
			getJTxtProducto().setText("");
			getJLblImage().setIcon(null);
			doPromptProducto();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void doPromptProducto() {
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}

	private JLabelPanambi getLblpnmbDescuento() {
		if (lblpnmbDescuento == null) {
			lblpnmbDescuento = new JLabelPanambi();
			lblpnmbDescuento.setText("Descuento");
		}
		return lblpnmbDescuento;
	}

	private JTextFieldInteger getJTxtDescuento() {
		if (jTxtDescuento == null) {
			jTxtDescuento = new JTextFieldInteger();
			jTxtDescuento.setText("Monto del descuento");
		}
		return jTxtDescuento;
	}

	private JLabelPanambi getJLabelMaxDescuento() {
		if (jLabelMaxDescuento == null) {
			jLabelMaxDescuento = new JLabelPanambi();
			jLabelMaxDescuento.setVerticalAlignment(SwingConstants.TOP);
			jLabelMaxDescuento.setForeground(new Color(70, 130, 180));
			jLabelMaxDescuento.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return jLabelMaxDescuento;
	}

	private JLabelPanambi getJLabelPrecioLista() {
		if (jLabelPrecioLista == null) {
			jLabelPrecioLista = new JLabelPanambi();
			jLabelPrecioLista.setVerticalAlignment(SwingConstants.TOP);
			jLabelPrecioLista.setForeground(new Color(70, 130, 180));
			jLabelPrecioLista.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return jLabelPrecioLista;
	}

	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Sucursal");
		}
		return labelPanambi;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
			jCmbSucursal.setEditable(false);
			jCmbSucursal.setModel(new ListComboBoxModel<Sucursal>(sucursales));
			jCmbSucursal.setSelectedItem(JFramePanambiMain.session.getSucursalOperativa());
		}
		return jCmbSucursal;
	}

	private JLabel getJLblImage() {
		if (jLblImage == null) {
			jLblImage = new JLabel("");
		}
		return jLblImage;
	}

	private void poblarSucursales() throws Exception {
		ControladorSucursal controladorSucursal = new ControladorSucursal();
		sucursales = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
	}

}
