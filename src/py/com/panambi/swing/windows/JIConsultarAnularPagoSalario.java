package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorDeudaEmpleado;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorPago;
import py.com.panambi.controller.ControladorPagoSalario;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarAnularPagoSalario extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnAnular;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JComboBoxPanambi jCmbEstado;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private List<String> tiposPagosEmpleados = new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private List<String> sucursales= new ArrayList<String>();
	private JPopupMenu popupMenu;
	private JMenuItem menuItemDetalles;
	private JMenuItem menuItemAnular;
	private JButtonPanambi jBtnConsultar;
	private ControladorEmpleado controladorEmpleado= new ControladorEmpleado();
	private JLabelPanambi lblpnmbTipoDePago;
	private JComboBoxPanambi jCmbTipoPago;
	private JPanel panel;
	private JTextFieldUpper jTxtNombreApellido;
	private JLabelPanambi lblpnmbEmpleado;
	private JLabelPanambi labelPanambi_1;
	private JTextField jTxtNroCedula;
	private JRadioButton rdbtnTodos;
	private Empleado empleado;
	private JLabelPanambi lblpnmbSucursal;
	private JComboBoxPanambi jCmbSucursal;
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	
	public JIConsultarAnularPagoSalario() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar/Anular pago a empleados");
		setBounds(100,100, 826, 512);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(664, Short.MAX_VALUE)
					.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addGap(7)
							.addComponent(getJDPFechaDesde(), GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
							.addGap(189)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(19)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblpnmbTipoDePago(), GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(getJCmbSucursal(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJCmbEstado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJCmbTipoPago(), GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))))
					.addGap(64))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
					.addGap(22))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(4)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbTipoDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJCmbTipoPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		agregarMenuPoput();
		setShortcuts(this);
		setHelp("consultarAnularPagoEmpleado");
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
//		popupMenu.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				
//			}
//		});
		popupMenu.add(getMenuItemDetalles());
		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			empleado = null;
			tiposPagosEmpleados = null;
			estados=null;
			poblarListaTiposPagosEmpleados();
			poblarListaEstados();
			poblarListaSucursales();
			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
			getJCmbTipoPago().setModel(new ListComboBoxModel<>(tiposPagosEmpleados) );
			getJCmbSucursal().setModel(new ListComboBoxModel<>(sucursales) );
			getJTxtNroCedula().setText("");
			getJTxtNombreApellido().setText("");
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			if(getTablePanambi().getRowCount()>0){
				getTablePanambi().resetData(0);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaTiposPagosEmpleados() throws Exception {
		tiposPagosEmpleados = new ArrayList<String>();
		tiposPagosEmpleados.add("TODOS");
		tiposPagosEmpleados.add("PAGOS DE SALARIOS");
		tiposPagosEmpleados.add("PAGOS DE AGUINALDO");
		tiposPagosEmpleados.add("LIQUIDACION");
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("PAGADOS");
		estados.add("NO PAGADOS");
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listaSucursal= controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		sucursales.add("TODAS");
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver mas detalles...");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetalles();
				}
			});
		}
		return menuItemDetalles; 
	}
	
//	private void doMenuEnableDisnable(){
//		try{
//			Integer fila = getTablePanambi().getSelectedRow();
//			if(fila == -1 ){
//				//DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
//			}else{
//				//"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total pagado", "Estado","Data"};
//				PagoSalario pagoEmpleadoDet = (PagoSalario)getTablePanambi().getValueAt(fila, 7);
//				if(pagoEmpleadoDet.getEstado().equals("NP")){
//					menuItemDetalles.setEnabled(false);
//					menuItemAnular.setEnabled(false);
//				}else{
//					menuItemDetalles.setEnabled(true);
//					menuItemAnular.setEnabled(true);
//				}
//			}
//		}catch (Exception e){
//			logger.error(e.getMessage(),e);
//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//		}
//		
//	}
	
	private void doVerDetalles(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
			}else{
				//"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total pagado", "Estado","Data"};
				PagoSalario pagoEmpleadoDet = (PagoSalario)getTablePanambi().getValueAt(fila, 7);
				if(pagoEmpleadoDet.getEstado().equals("NP")){
					DlgMessage.showMessage(getOwner(), "No es posible visualizar detalles de un pago a emplado con estado NO PAGADO.", DlgMessage.ERROR_MESSAGE);
				}else{
					DlgDetallePagoEmpleado dlgDetallePagoEmpleado= new DlgDetallePagoEmpleado(pagoEmpleadoDet);
					dlgDetallePagoEmpleado.centerIt();
					dlgDetallePagoEmpleado.setVisible(true);
				}
				
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JMenuItem getMenuItemAnular(){
		if (menuItemAnular == null) {
			menuItemAnular = new JMenuItem("Anular");
			menuItemAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
		}
		return menuItemAnular; 
	}
	
	private JButtonPanambi getJBtnAnular() {
		if (jBtnAnular == null) {
			jBtnAnular = new JButtonPanambi();
			jBtnAnular.setMnemonic('A');
			jBtnAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
			jBtnAnular.setToolTipText("Anular pago a empleado");
			jBtnAnular.setText("Anular");
		}
		return jBtnAnular;
	}
	
	
	private void doAnular(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Imposible anular.\nDebe seleccionar un regitro", DlgMessage.ERROR_MESSAGE);
			}else{
				PagoSalario pagoNul = (PagoSalario)getTablePanambi().getValueAt(fila, 7);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				if (!sdf.format(new java.util.Date()).equals(sdf.format(pagoNul.getFechapago()))){
					DlgMessage.showMessage(getOwner(), "Imposible anular pago a empleado.\nS�lo es posible anularlo el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
				}else{
					if(pagoNul.getEstado().equals("NP")){
						DlgMessage.showMessage(getOwner(), "Imposible anular el pago de empleado.\nNo se pueden anular pagos de empledos con estado 'NO PAGADO'.", DlgMessage.ERROR_MESSAGE);
					}else{
						String mensaje = "Confirma la anulaci�n del pago al empleado ?";
						if(pagoNul.getTipo().equals("L")){
							DeudaEmpleado deuda = null;
							deuda = new ControladorDeudaEmpleado().getDeudaEmpleadoPorPago(JFramePanambiMain.session.getConn(), pagoNul.getCodpagosalario());
							if(deuda!=null){
								mensaje+="\nLa deuda relacionada tambi�n ser� anulada.";
								Pago pagoDeuda = null;
								pagoDeuda = new ControladorPago().getPagoDeuda(JFramePanambiMain.session.getConn(), deuda);
								if(pagoDeuda!=null){
									mensaje+="\nEl pago de la deuda tambi�n ser� anulado";
								}
							}
						}
						Integer ret = JOptionPane.showInternalConfirmDialog(this, mensaje, "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (ret == JOptionPane.YES_OPTION) {
									
							String comentariosAnulacion = "ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+".";
							
							new ControladorPagoSalario().anularPagoEmpleado(JFramePanambiMain.session.getConn(), pagoNul, comentariosAnulacion);
							DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa.", DlgMessage.INFORMATION_MESSAGE);
							doConsultarPagoEmpleado();
						}
					}
				
				}
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			
			//tablePanambi = new JTablePanambi((String[]) null);
			String[] columnNames = {"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total a pagar", "Estado","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(1, Date.class);
			types.put(2, Integer.class);
			types.put(5, Double.class);
			Integer[] editable = { };
			tablePanambi= new JTablePanambi(columnNames,editable,types);
//			tablePanambi.addMouseListener(new MouseAdapter() {
//				@Override
//				public void mouseClicked(MouseEvent e) {
//					doMenuEnableDisnable();
//				}
//			});			
			tablePanambi.setRowHeight(20);
						
			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(200);
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(100);
			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(50);
			tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(60);
			
			tablePanambi.getColumnModel().getColumn(7).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(7).setMaxWidth(0);
//			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(2);
//			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(250);
//			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
//			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
		}
		return tablePanambi;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Consultar pagos a emplados");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarPagoEmpleado();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	@SuppressWarnings("deprecation")
	private void doConsultarPagoEmpleado(){
	
		try{
			String tipoPagoEmpleado;
			String estado ;
			Date fechaDesde ;
			Date fechaHasta ;
			Sucursal sucursal = null;
			String nombreSucursal = "";
			
			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			estado = (String)getJCmbEstado().getSelectedItem();

			if(estado.equals("TODOS")){
				estado = null;
			}else if(estado.equals("PAGADOS")){
				estado = "P";
			}else{
				estado = "NP";
			}
			
			nombreSucursal= (String)getJCmbSucursal().getSelectedItem(); 
			
			if(!nombreSucursal.equals("TODAS")){
				sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursal);
			}else sucursal = null;
			
			tipoPagoEmpleado = (String)getJCmbTipoPago().getSelectedItem();
			if(tipoPagoEmpleado.equals("TODOS")){
				tipoPagoEmpleado = null;
			}else if(tipoPagoEmpleado.equals("PAGOS DE SALARIOS")){
				tipoPagoEmpleado= "S";
			}else if(tipoPagoEmpleado.equals("PAGOS DE AGUINALDO")){
				tipoPagoEmpleado = "A";
			}else{
				tipoPagoEmpleado = "L";
			}
			
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
			}
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				List<PagoSalario> listaPagosEmpleados = new ControladorPagoSalario().getPagosEmpleados(JFramePanambiMain.session.getConn(), empleado, tipoPagoEmpleado, sucursal , estado, fechaDesde, fechaHasta);
				if(listaPagosEmpleados.size()==0){
					DlgMessage.showMessage(getOwner(), "Ning�n pago encontrado", DlgMessage.INFORMATION_MESSAGE);
				}
				
				Iterator<PagoSalario> iteratorPagoSalario= listaPagosEmpleados.listIterator();
//				
				if(getTablePanambi().getRowCount()!=0){
					getTablePanambi().resetData(0);
				}
				
				while (iteratorPagoSalario.hasNext()) {
					
					getTablePanambi().addRow();
					PagoSalario pago = (PagoSalario) iteratorPagoSalario.next();
					//"Empleado", "Fecha", "Nro. Recibo","Periodo","Tipo de pago","Total pagado", "Estado","Data"};
					Empleado emp = new ControladorEmpleado().getEmpleado(JFramePanambiMain.session.getConn(), pago.getCodempleado());
					getTablePanambi().setValueAt(emp.getNombre()+" "+emp.getApellido(), getTablePanambi().getRowCount()-1, 0);
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					
					getTablePanambi().setValueAt(formato.format(pago.getFechapago()), getTablePanambi().getRowCount()-1, 1);
					
					if(pago.getNumeroRecibo() !=0){
						getTablePanambi().setValueAt(pago.getNumeroRecibo(), getTablePanambi().getRowCount()-1, 2);
					}
					getTablePanambi().setValueAt(formato.format(pago.getFechaDesde())+" a "+formato.format(pago.getFechaHasta()), getTablePanambi().getRowCount()-1, 3);
					
					String tipoPago = "";
					if(pago.getTipo().equals("S")){
						tipoPago = "Salario";
					}else if(pago.getTipo().equals("L")){
						tipoPago = "Liquidacion";
					}else{
						tipoPago = "Aguinaldo";
					}
					
					getTablePanambi().setValueAt(tipoPago, getTablePanambi().getRowCount()-1, 4);
					getTablePanambi().setValueAt(pago.getTotalpagado(), getTablePanambi().getRowCount()-1, 5);
					String est = "";
					if(pago.getEstado().equals("P")){
						est = "Pagado";
					}else{
						est = "No pagado";
					}
					getTablePanambi().setValueAt(est, getTablePanambi().getRowCount()-1, 6);
					getTablePanambi().setValueAt(pago, getTablePanambi().getRowCount()-1, 7);

				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	 private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
		
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
		}
		return jCmbEstado;
		
	}
	private JLabelPanambi getLblpnmbTipoDePago() {
		if (lblpnmbTipoDePago == null) {
			lblpnmbTipoDePago = new JLabelPanambi();
			lblpnmbTipoDePago.setText("Tipo de pago a empleado : ");
			lblpnmbTipoDePago.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbTipoDePago;
	}
	private JComboBoxPanambi getJCmbTipoPago() {
		if (jCmbTipoPago == null) {
			jCmbTipoPago = new JComboBoxPanambi();
		}
		return jCmbTipoPago;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setToolTipText("Informacion del empleado");
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGap(0, 330, Short.MAX_VALUE)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(getLblpnmbEmpleado(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(10)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
							.addGap(9)
							.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(24)
							.addComponent(getRdbtnTodos(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGap(0, 98, Short.MAX_VALUE)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getRdbtnTodos())
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(4)
								.addComponent(getJTxtNroCedula(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getJTxtNombreApellido(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setToolTipText("Nombre y apellido del empleado");
			jTxtNombreApellido.setText("");
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setFocusable(false);
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		}
		return jTxtNombreApellido;
	}
	private JLabelPanambi getLblpnmbEmpleado() {
		if (lblpnmbEmpleado == null) {
			lblpnmbEmpleado = new JLabelPanambi();
			lblpnmbEmpleado.setText("Empleado");
			lblpnmbEmpleado.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbEmpleado.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbEmpleado;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("Nro. Cedula : ");
			labelPanambi_1.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return labelPanambi_1;
	}
	private JTextField getJTxtNroCedula() {
		if (jTxtNroCedula == null) {
			jTxtNroCedula = new JTextField();
			jTxtNroCedula.setText("");
			jTxtNroCedula.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroCedula.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtNroCedula.setColumns(10);
			jTxtNroCedula.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroCedula();
				}
			});

		}
		return jTxtNroCedula;
	}
	
private void lostFocusNroCedula(){
		
		try{
			Integer longitud = 0;
			try{
				longitud = getJTxtNroCedula().getText().length();
			}catch(Exception e){
				longitud = 0;
			}
			if(longitud==0){
				empleado = null;
				getRdbtnTodos().setSelected(true);
			}else{
				Empleado emp =  controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), getJTxtNroCedula().getText());
				
				if(emp != null){
					setValues(emp, null);
					//getRdbtnTodos().setSelected(false);
				}else{
					doBuscarEmpleados();
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void doBuscarEmpleados(){
			try {
				getJTxtNroCedula().setText("");
				getJTxtNombreApellido().setText("");
				String[] columnNames = { "C�digo", "Nro Documento", "Nombre","Apellido", "Cargo","Sucursal","Estado"};
				String[] pks = { "codEmpleado" };
				String sSelect = "SELECT codempleado, nrodocumento, nombres, apellidos, ";
				sSelect += "(SELECT descripcion FROM cargosempleados WHERE empleados.codcargoempleado = cargosempleados.codcargoempleado),  ";
				sSelect += "(SELECT nombre FROM sucursales WHERE empleados.codsucursal= sucursales.codsucursal), ";
				sSelect += " (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
				sSelect += "FROM empleados ";
				sSelect += "ORDER BY nombres";
				JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Empleado", pks);
				jb.setVisible(true);
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
			
	}

	private JRadioButton getRdbtnTodos() {
		if (rdbtnTodos == null) {
			rdbtnTodos = new JRadioButton("Todos");
			rdbtnTodos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSelectTodosEmpleados();
				}
			});
			rdbtnTodos.setToolTipText("Todos los empleados");
			rdbtnTodos.setSelected(true);
		}
		return rdbtnTodos;
	}
	
	private void doSelectTodosEmpleados(){
		getRdbtnTodos().setSelected(true);
		empleado = null;
		getJTxtNroCedula().setText("");
		getJTxtNombreApellido().setText(null);
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Empleado) {
			this.empleado= (Empleado)obj;
			getRdbtnTodos().setSelected(false);
			getJTxtNombreApellido().setText(empleado.getNombre()+" "+empleado.getApellido());
			getJTxtNroCedula().setText(empleado.getNroDocumento());
			
		}
	}
	
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
}
