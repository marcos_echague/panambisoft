package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Usuario;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.table.JBrowser;

public class JIAsignarEmpleadoUsuario extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2134899660461388500L;
	private JLabelPanambi lblpnmbEmpleado;
	private JLabelPanambi lblpnmbUsuario;
	private JComboBoxPanambi jCmbUsuario;
	private JButtonPanambi btnpnmbAceptar;
	private JButtonPanambi btnpnmbCancelar;
	private JButtonPanambi btnpnmbLimpiar;
	private List<String> usuarios = new ArrayList<String>();
	private List<String> documentos = new ArrayList<String>();
	private ControladorUsuario controladorUsuario  = new ControladorUsuario();
	private ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
	private Empleado empleado;
	private Usuario usuario;
	private JComboBoxPanambi jCmbNroDocumento;
	private JButtonPanambi btnpnmbQuitar;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi labelPanambi_1;
	private JButtonPanambi jBtnExaminarEmpleado;
	private JLabelPanambi lblpnmbPresioneFPara;
	public JIAsignarEmpleadoUsuario() throws Exception {
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Asignar/Quitar usuario a empleado");
		setBounds(100, 100,474, 318);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbAceptar());
		getJPanelSouth().add(getBtnpnmbQuitar());
		getJPanelSouth().add(getBtnpnmbCancelar());

		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(299)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJBtnExaminarEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(81)
					.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(17)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getJCmbNroDocumento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJBtnExaminarEmpleado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout);
			decorate();
			setShortcuts(this);
			setHelp("asignarusuarioempleado");
			doLimpiar();
			setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbNroDocumento(), getJCmbUsuario(), getJBtnExaminarEmpleado(), getBtnpnmbLimpiar(), getBtnpnmbAceptar(), getBtnpnmbQuitar(), getBtnpnmbCancelar()}));
			getJCmbNroDocumento().requestFocus();
	}
	private JLabelPanambi getLblpnmbEmpleado() {
		if (lblpnmbEmpleado == null) {
			lblpnmbEmpleado = new JLabelPanambi();
			lblpnmbEmpleado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEmpleado.setText("Nro. Documento Empleado :");
		}
		return lblpnmbEmpleado;
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuario.setText("Usuario :");
		}
		return lblpnmbUsuario;
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbUsuario() {
		if (jCmbUsuario == null) {
			jCmbUsuario = new JComboBoxPanambi();
			jCmbUsuario.setModel(new ListComboBoxModel<String>(usuarios));
			jCmbUsuario.setFocusable(false);
			jCmbUsuario.setSelectedItem(null);
		}
		return jCmbUsuario;
	}
	
	
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.setMnemonic('A');
			btnpnmbAceptar.setToolTipText("Asignar usuario a empleado");
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbAceptar.setText("Asignar");
		}
		return btnpnmbAceptar;
	}
	
	private void doGrabar()  {
		try {
			usuario = new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(),(String) getJCmbUsuario().getSelectedItem());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			doLimpiar();
			getJCmbNroDocumento().requestFocus();
		}
		
		if (usuario == null){
			
			DlgMessage.showMessage(getOwner(), "Debe seleccionar un USUARIO para el empleado", DlgMessage.WARNING_MESSAGE);
			usuario = null;
			getJCmbUsuario().requestFocus();	
		}else{
			boolean existe = true;
			try {
				 existe = controladorEmpleado.comprobarExistencia(JFramePanambiMain.session.getConn(), usuario);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				doLimpiar();
				getJCmbNroDocumento().requestFocus();
			}
			
			if(existe){
				DlgMessage.showMessage(getOwner(), "El usuario "+usuario.getUsuario()+" ya esta asignado a otro empleado\n"
						+ "Debe seleccionar otro usuario", DlgMessage.ERROR_MESSAGE);
				getJCmbUsuario().setSelectedItem(null);
				getJCmbUsuario().requestFocus();
				usuario = null;
			}else{
				
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la asignacion : \n "
						+ "EMPLEADO : "+empleado.getNombre()+" "+empleado.getApellido()+"\nUSUARIO  : "+usuario.getUsuario(), "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

				if(ret == JOptionPane.YES_OPTION){
					try {

						controladorEmpleado.asignarUsuario(JFramePanambiMain.session.getConn(), empleado, usuario);
						DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
						doLimpiar();
						getJCmbNroDocumento().requestFocus();
						getJCmbNroDocumento().requestFocus();
					} catch (Exception e) {
						DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
						doLimpiar();
						getJCmbNroDocumento().requestFocus();
					}
				}
			}
		}

	}
	
	
	
	private JButtonPanambi getBtnpnmbCancelar() {
		if (btnpnmbCancelar == null) {
			btnpnmbCancelar = new JButtonPanambi();
			btnpnmbCancelar.setMnemonic('S');
			btnpnmbCancelar.setToolTipText("Cerrar ventana");
			btnpnmbCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbCancelar.setText("Salir");
		}
		return btnpnmbCancelar;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Prepara pantalla para nueva asignacion");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbNroDocumento().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			empleado = null;
			usuario = null;
			poblarListaEmpleados();
			getJCmbNroDocumento().setModel(new ListComboBoxModel<String>(documentos));
			getJCmbNroDocumento().setSelectedItem(null);
			
			poblarListaUsuarios();
			getJCmbUsuario().setModel(new ListComboBoxModel<String>(usuarios));
			getJCmbUsuario().setSelectedItem(null);
			
			getBtnpnmbAceptar().setEnabled(false);
			getBtnpnmbQuitar().setEnabled(false);
			
			getJCmbUsuario().setEnabled(false);
			
			//getJCmbNroDocumento().requestFocus();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	
	private void poblarListaEmpleados() throws Exception {
		documentos = new ArrayList<String>();
		List<Empleado> listaemp = new ControladorEmpleado().getEmpleadosActivos(JFramePanambiMain.session.getConn());
		
		for (Empleado emp : listaemp) {
			documentos.add(emp.getNroDocumento());
		}
	}
	
	private void poblarListaUsuarios() throws Exception{
		usuarios = new ArrayList<String>();
		List<Usuario> listausu = controladorUsuario.getUsuariosActivos(JFramePanambiMain.session.getConn());
		for (Usuario usu : listausu) {
			usuarios.add(usu.getUsuario());
		}
	}
	
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbNroDocumento());
	}
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbNroDocumento() {
		if (jCmbNroDocumento == null) {
			jCmbNroDocumento = new JComboBoxPanambi();
			jCmbNroDocumento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lostFocusNroDocumento();
				}
			});
			jCmbNroDocumento.setEditable(false);
			jCmbNroDocumento.setModel(new ListComboBoxModel<String>(documentos));
			jCmbNroDocumento.setSelectedItem(null);
		}
		return jCmbNroDocumento;
	}
	
	private void lostFocusNroDocumento() {
		try {
			Empleado emp = controladorEmpleado.getEmpleado(JFramePanambiMain.session.getConn(), (String) jCmbNroDocumento.getSelectedItem());
			if (emp != null) {
				setValues(emp, null);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Empleado) {
			this.empleado = (Empleado) obj;
			getJCmbNroDocumento().setSelectedItem(((Empleado) (obj)).getNroDocumento());
				
			try {
				usuario = new ControladorEmpleado().getUsuarioDeEmpleado(JFramePanambiMain.session.getConn(), empleado.getNroDocumento());
				if(usuario !=null){
					
							getJCmbUsuario().setSelectedItem(usuario.getUsuario());
							getJCmbUsuario().setEnabled(false);
							
							
							getBtnpnmbAceptar().setEnabled(false);
							getBtnpnmbQuitar().setEnabled(true);
							
				}else {	

					getJCmbUsuario().setSelectedItem(null);
					getJCmbUsuario().setEnabled(true);
					
					getBtnpnmbAceptar().setEnabled(true);
					getBtnpnmbQuitar().setEnabled(false);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}	
		}	
	}
	
	private JButtonPanambi getBtnpnmbQuitar() {
		if (btnpnmbQuitar == null) {
			btnpnmbQuitar = new JButtonPanambi();
			btnpnmbQuitar.setMnemonic('Q');
			btnpnmbQuitar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doQuitarUsuario();
				}
			});
			btnpnmbQuitar.setToolTipText("Quita usuario a empleado");
			btnpnmbQuitar.setText("Quitar");
		}
		return btnpnmbQuitar;
	}
	
	private void doQuitarUsuario(){
		try{
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma el despojo de usuario al empleado ? \n "
					
					+ "USUARIO : "+(String)getJCmbUsuario().getSelectedItem()
				  + "\nEMPLADO : "+empleado.getNombre()+" "+empleado.getApellido(),//(String)getJCmbNroDocumento().getSelectedItem(),
					"", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE );
			
			if(ret == JOptionPane.YES_OPTION){
				controladorEmpleado.asignarUsuario(JFramePanambiMain.session.getConn(), empleado,null);
				DlgMessage.showMessage(getOwner(), "Operacion exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbNroDocumento().requestFocus();
			}else {
				getJCmbNroDocumento().requestFocus();
				empleado = null;
				usuario = null;
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			empleado = null;
			usuario = null;
		}
	}
	
	@Override
	public void setNoValues(Object obj, Integer source) {
		
		
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}
	private JButtonPanambi getJBtnExaminarEmpleado() {
		if (jBtnExaminarEmpleado == null) {
			jBtnExaminarEmpleado = new JButtonPanambi();
			jBtnExaminarEmpleado.setMnemonic('E');
			jBtnExaminarEmpleado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doExaminarEmpleado();
				}
			});
			jBtnExaminarEmpleado.setToolTipText("Examinar empleado (Alt + E)");
			jBtnExaminarEmpleado.setText("Examinar ...");
		}
		return jBtnExaminarEmpleado;
	}
	
	private void doExaminarEmpleado() {
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Nro. de Documento","Nombres", "Apellidos","Usuario"};
			String[] pks = { "codEmpleado" };
			String sSelect = "SELECT codempleado, nrodocumento, nombres, apellidos, ";
			sSelect+="(SELECT usuario FROM usuarios WHERE empleados.codusuario = usuarios.codusuario) ";
			sSelect += "FROM empleados ";
			sSelect += "WHERE estado = 'A' ";
			sSelect += "ORDER BY nombres";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Empleado", pks);
			jb.setVisible(true);
			getJCmbNroDocumento().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda ");
		}
		return lblpnmbPresioneFPara;
	}
}
