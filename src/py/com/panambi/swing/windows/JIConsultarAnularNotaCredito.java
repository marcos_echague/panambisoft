package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.SimpleDateFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;


public class JIConsultarAnularNotaCredito extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5264254496284968813L;
	private JPanel cabecera;
	private JLabelPanambi lblpnmbNroFactura;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbFechaDeVenta;
	private JPanel jPnlCliente;
	private JLabelPanambi lblpnmbCliente;
	private JTextFieldInteger jTxtCodCliente;
	private JTextFieldUpper jTxtNombreApellido;
	private JPanel jPnlSucursal;
	private JLabelPanambi jLblSucursal;
	private JTextFieldInteger jTxtCodSucursal;
	private JTextFieldUpper jTxtNombreSucursal;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JTextField jTxtFechaDevolucion;
	private JLabelPanambi lblpnmbDevolucion;
	private JTextFieldDouble jTxtTotalDevuelto;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private ControladorVenta controladorVenta = new ControladorVenta();
	private Devolucion devolucion;
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbAnular;
	private JLabelPanambi lblpnmbCodDevolucion;
	private JTextFieldInteger jTxtCodDevolucion;
	private JLabelPanambi lblpnmbNotaDeCredito;
	private JTextFieldInteger jTxtNroNotaCredito;
	private JLabelPanambi lblpnmbEstadoDeNota;
	private JTextField jTxtEstadoNotaCredito;
	private JLabelPanambi lblpnmbUsuario;
	private JTextFieldUpper jTxtUsuario;
	
	public JIConsultarAnularNotaCredito() throws Exception{
		initialize();
	}
	
	public JIConsultarAnularNotaCredito(Devolucion dev) throws Exception{
		initialize();
		this.devolucion = dev;
		setValues(devolucion, null);
	}
	
	private void initialize() {
		setMaximizable(false);
		setBounds(100, 100, 733, 505);
		setTitle("Cosultar/Anular nota de credito");
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getCabecera(), GroupLayout.DEFAULT_SIZE, 913, Short.MAX_VALUE)
						.addComponent(getLblpnmbPresioneFPara(), Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(getCabecera(), GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
					.addContainerGap())
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		doLimpiar();
	}
	
	private void doLimpiar(){
		devolucion = null;
		getJTxtNroFactura().setValor(null);
		getJTxtFechaDevolucion().setText("");
		getJTxtCodDevolucion().setValor(null);
		getJTxtNroNotaCredito().setValor(null);
		getJTxtEstadoNotaCredito().setText(null);
		getJTxtUsuario().setText("");
		getJTxtCodCliente().setValor(null);
		getJTxtNombreApellido().setText("");
		getJTxtCodSucursal().setValor(null);
		getJTxtNombreSucursal().setText("");
		getJTxtTotalDevuelto().setValue(0.0);
	}
	
	private JPanel getCabecera() {
		if (cabecera == null) {
			cabecera = new JPanel();
			cabecera.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			cabecera.setLayout(null);
			cabecera.add(getLblpnmbNotaDeCredito());
			cabecera.add(getJTxtNroNotaCredito());
			cabecera.add(getLblpnmbFechaDeVenta());
			cabecera.add(getJTxtFechaDevolucion());
			cabecera.add(getLblpnmbNroFactura());
			cabecera.add(getJTxtNroFactura());
			cabecera.add(getLblpnmbEstadoDeNota());
			cabecera.add(getJTxtEstadoNotaCredito());
			cabecera.add(getLblpnmbCodDevolucion());
			cabecera.add(getJTxtCodDevolucion());
			cabecera.add(getJPnlCliente());
			cabecera.add(getLblpnmbUsuario());
			cabecera.add(getJTxtUsuario());
			cabecera.add(getLblpnmbDevolucion());
			cabecera.add(getJTxtTotalDevuelto());
			cabecera.add(getJPnlSucursal());
			doLimpiar();
			setShortcuts(this);
		}
		return cabecera;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setBounds(28, 86, 127, 20);
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro Factura asociada : ");
		}
		return lblpnmbNroFactura;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setBounds(165, 85, 118, 21);
			jTxtNroFactura.setToolTipText("Numero de Factura");
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
		}
		return jTxtNroFactura;
	}
	
	private void lostFocusCodDevolucion(){
		
		if(getJTxtCodDevolucion().getValor()==null){
			
		}else{
			try {
				Devolucion dev = controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), getJTxtCodDevolucion().getValor());
				if (dev!= null) {
					setValues(dev, null);
				} else {
					devolucion = null;
					getJTxtCodDevolucion().setValor(null);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	
	private void lostFocusNroFactura(){
		
		if(getJTxtNroFactura().getValor()==null){
		
		}else{
			try {
				Venta ven = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				if(ven!= null){
					Devolucion dev = controladorDevolucion.getDevolucionPorVenta(JFramePanambiMain.session.getConn(), ven.getCodVenta());
					if (dev!= null) {
						if(controladorDevolucion.cantidadDevoluciones(JFramePanambiMain.session.getConn(), ven)>1){
							DlgMessage.showMessage(getOwner(), "M�s de un registro para la factura n�mero "+getJTxtNroFactura().getValor()+".\n"
									+ "Se deplegara la opci�n de b�squeda para seleccionar el registro espec�fico.", DlgMessage.WARNING_MESSAGE);
							doLimpiar();
							doBuscar(ven.getCodVenta());
							getJTxtCodDevolucion().requestFocus();
						}else {
							setValues(dev, null);
						}
						
					} else {
						devolucion = null;
						getJTxtNroFactura().setValor(null);
					}
				}else{
					DlgMessage.showMessage(getOwner(), "No existe nota de cr�dito para la factura n�mero "+getJTxtNroFactura().getValor(), DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().setValor(null);
					devolucion = null;
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	private JLabelPanambi getLblpnmbFechaDeVenta() {
		if (lblpnmbFechaDeVenta == null) {
			lblpnmbFechaDeVenta = new JLabelPanambi();
			lblpnmbFechaDeVenta.setBounds(347, 28, 118, 20);
			lblpnmbFechaDeVenta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDeVenta.setText("Fecha de generacion : ");
		}
		return lblpnmbFechaDeVenta;
	}
	private JPanel getJPnlCliente() {
		if (jPnlCliente == null) {
			jPnlCliente = new JPanel();
			jPnlCliente.setBounds(347, 127, 330, 58);
			jPnlCliente.setToolTipText("Informacion del cliente");
			jPnlCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPnlCliente.setLayout(null);
			jPnlCliente.add(getLblpnmbCliente());
			jPnlCliente.add(getJTxtCodCliente());
			jPnlCliente.add(getJTxtNombreApellido());
		}
		return jPnlCliente;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setBounds(2, 2, 325, 20);
			lblpnmbCliente.setText("Cliente");
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.CENTER);
			
			lblpnmbCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbCliente;
	}
	private JTextFieldInteger getJTxtCodCliente() {
		if (jTxtCodCliente == null) {
			jTxtCodCliente = new JTextFieldInteger();
			jTxtCodCliente.setBounds(14, 27, 74, 21);
			jTxtCodCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodCliente.setToolTipText("Codigo del cliente");
			jTxtCodCliente.setEditable(false);
			jTxtCodCliente.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodCliente.setFocusable(false);
		}
		return jTxtCodCliente;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setBounds(97, 27, 218, 21);
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setToolTipText("Nombre del cliente");
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreApellido.setFocusable(false);
		}
		return jTxtNombreApellido;
	}
	private JPanel getJPnlSucursal() {
		if (jPnlSucursal == null) {
			jPnlSucursal = new JPanel();
			jPnlSucursal.setBounds(347, 214, 330, 58);
			jPnlSucursal.setToolTipText("Informacion de sucursal");
			jPnlSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPnlSucursal.setLayout(null);
			jPnlSucursal.add(getJLblSucursal());
			jPnlSucursal.add(getJTxtCodSucursal());
			jPnlSucursal.add(getJTxtNombreSucursal());
		}
		return jPnlSucursal;
	}
	private JLabelPanambi getJLblSucursal() {
		if (jLblSucursal == null) {
			jLblSucursal = new JLabelPanambi();
			jLblSucursal.setBounds(2, 2, 325, 20);
			jLblSucursal.setText("Sucursal");
			jLblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jLblSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return jLblSucursal;
	}
	private JTextFieldInteger getJTxtCodSucursal() {
		if (jTxtCodSucursal == null) {
			jTxtCodSucursal = new JTextFieldInteger();
			jTxtCodSucursal.setBounds(14, 27, 74, 21);
			jTxtCodSucursal.setToolTipText("Codigo de sucursal");
			jTxtCodSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodSucursal.setEditable(false);
			jTxtCodSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodSucursal.setFocusable(false);
		}
		return jTxtCodSucursal;
	}
	private JTextFieldUpper getJTxtNombreSucursal() {
		if (jTxtNombreSucursal == null) {
			jTxtNombreSucursal = new JTextFieldUpper();
			jTxtNombreSucursal.setBounds(97, 27, 218, 21);
			jTxtNombreSucursal.setToolTipText("Nombre de la sucursal");
			jTxtNombreSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreSucursal.setEditable(false);
			jTxtNombreSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreSucursal.setFocusable(false);
		}
		return jTxtNombreSucursal;
	}
	
	
//	private void doGuardarDevolucion(){
//		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Est� seguro que quiere guardar esta devoluci�n?\n"
//				+ "Sucursal : "+venta.getSucursal().getNombre()+" \n"
//						+ "Devoluci�n : Gs. "+getJTxtDevolucion().getValue(), "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//		
//		if (ret == JOptionPane.YES_OPTION) {
//			
//			try{
//				
//				devolucion = new Devolucion();
//				devolucion.setTotal((Double)getJTxtDevolucion().getValue());
//				devolucion.setVenta(venta);
//				devolucion.setUsuario(JFramePanambiMain.session.getUsuario());
//				devolucion.setSucursal(venta.getSucursal());
//				devolucion.setCliente(venta.getCliente());
//				
//				List<DetalleDevolucion> detalle = new ArrayList<DetalleDevolucion>();
//				Integer item = 1;
//				for(int i = 0;i<getJTblDetalleVenta().getRowCount();i++){
//					DetalleDevolucion detalleDevolucion;
//					Integer cantidadDevuelta;
//					
//					if(getJTblDetalleVenta().getValueAt(i, 3)==null){
//						cantidadDevuelta = 0;
//					}else{
//						cantidadDevuelta = (Integer) getJTblDetalleVenta().getValueAt(i, 3);
//					}
//				
//					if(cantidadDevuelta!=0){
//						detalleDevolucion = new DetalleDevolucion();
//						detalleDevolucion.setNroItem(item);
//						detalleDevolucion.setCantidad(cantidadDevuelta);
//						detalleDevolucion.setMontoUnitario((Double)getJTblDetalleVenta().getValueAt(i, 4));
//						detalleDevolucion.setTotalItem((Double)getJTblDetalleVenta().getValueAt(i, 4)*cantidadDevuelta);
//						detalleDevolucion.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), (String)getJTblDetalleVenta().getValueAt(i, 1)));
//						item+=1;
//						detalle.add(detalleDevolucion);
//					}
//				}
//				
//				devolucion.setDetalleDevolucion(detalle);
//				
//				if(devolucion.getVenta().getEstado().equals("A")){
//					
//					if(!controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), devolucion)){
//						Integer codDevolucionGenerado = controladorDevolucion.guardarDevolucion(JFramePanambiMain.session.getConn(), devolucion);
//						DlgMessage.showMessage(getOwner(), "Devoluci�n registrada exitosamente\nSe ha generado una nota de credito para la devolucion", DlgMessage.INFORMATION_MESSAGE);
//						doLimpiar();
//						getJTxtNroFactura().requestFocus();
//						doGenerarNotaCredito(controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), codDevolucionGenerado));
//						
//					}else{
//						DlgMessage.showMessage(getOwner(), "Ya se ha registrado una devolucion para la factura nro "+devolucion.getVenta().getNroComprobante(), DlgMessage.WARNING_MESSAGE);
//					}
//				}else{
//					DlgMessage.showMessage(getOwner(), "La venta con factura nro "+devolucion.getVenta().getNroComprobante()+" se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
//				}
//				
//			} catch (SQLException e) {
//				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
//					DlgMessage.showMessage(getOwner(), "Error de integridad.\nYa se ha registrado una devolucion para esta facturacion.", DlgMessage.ERROR_MESSAGE);
//				} else {
//					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//				}	
//			}catch(Exception e){
//				logger.error(e.getMessage(), e);
//				DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
//			}
//		}
//	}
	
//	private void doGenerarNotaCredito(Devolucion d) {
//		try {
//			PanambiUtils.setWaitCursor(getOwner());
//			Map<String, Object> parameters = new HashMap<String, Object>();
//			javax.swing.ImageIcon icon = new ImageIcon(JIConsultarAnularDevolucionProducto.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
//			Image image = icon.getImage();
//			parameters.put("LOGO", image);
//			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
//			
//			parameters.put("NRONOTACREDITO", d.getNotaCredito());
//			parameters.put("CLIENTE", d.getCliente().getNombres()+" "+d.getCliente().getApellidos());
//			parameters.put("TOTALCREDITO",d.getTotal());
//			
//			
//			URL url = JIConsultarAnularDevolucionProducto.class.getResource("/py/com/panambi/informes/reports/JRNotaCredito.jasper");
//			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
//			
//			JasperPrint jasperPrint;
//			
//			
//			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceNotaCredito(JFramePanambiMain.session.getConn(),d.getCodDevolucion()));
//			
//			List<?> pages = jasperPrint.getPages();
//			if (!pages.isEmpty()) {
//				JasperViewer.viewReport(jasperPrint, false);
//				
//			} else {
//				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
//			}
//		} catch (Exception e) {
//			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
//		} finally {
//			PanambiUtils.setDefaultCursor(getOwner());
//		}
//
//	}
	
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JTextField getJTxtFechaDevolucion() {
		if (jTxtFechaDevolucion == null) {
			jTxtFechaDevolucion = new JTextField();
			jTxtFechaDevolucion.setBounds(486, 29, 118, 20);
			jTxtFechaDevolucion.setToolTipText("Fecha de registro de devolucion");
			jTxtFechaDevolucion.setEditable(false);
			jTxtFechaDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaDevolucion.setColumns(10);
			jTxtFechaDevolucion.setFocusable(false);
		}
		return jTxtFechaDevolucion;
	}
	
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			try{	
				this.devolucion = (Devolucion) obj;
				
					String patron = "dd/MM/yyyy";
				    SimpleDateFormat formato = new SimpleDateFormat(patron);
					getJTxtFechaDevolucion().setText(formato.format(devolucion.getFecha()));
					
					getJTxtNroFactura().setValor(devolucion.getVenta().getNroComprobante());
					getJTxtCodDevolucion().setValor(devolucion.getCodDevolucion());
					
					if(devolucion.getEstadoNotaCredito().equals("A")){
						getJTxtEstadoNotaCredito().setText("Activo");
					}else{
						getJTxtEstadoNotaCredito().setText("Anulado");
					}
					
					getJTxtNroNotaCredito().setValor(devolucion.getNotaCredito());
					getJTxtUsuario().setText(devolucion.getUsuario().getUsuario());
					
					getJTxtCodCliente().setValor(devolucion.getCliente().getCodCliente());
					getJTxtNombreApellido().setText(devolucion.getCliente().getNombres().toString()+" "+devolucion.getCliente().getApellidos().toString());
					getJTxtCodSucursal().setValor(devolucion.getSucursal().getCodSucursal());
					getJTxtNombreSucursal().setText(devolucion.getSucursal().getNombre());
					
					getJTxtTotalDevuelto().setValue(devolucion.getTotal());
					//dtmDetalleVenta = new DefaultTableModel();
					getJTxtTotalDevuelto().setValue(devolucion.getTotal());
				
			}catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}

	public void setNoValues(Object obj, Integer source) {

	}
	
	private JLabelPanambi getLblpnmbDevolucion() {
		if (lblpnmbDevolucion == null) {
			lblpnmbDevolucion = new JLabelPanambi();
			lblpnmbDevolucion.setBounds(14, 245, 141, 20);
			lblpnmbDevolucion.setText("Monto de nota de credito : ");
			lblpnmbDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbDevolucion;
	}
	private JTextFieldDouble getJTxtTotalDevuelto() {
		if (jTxtTotalDevuelto == null) {
			jTxtTotalDevuelto = new JTextFieldDouble();
			jTxtTotalDevuelto.setBounds(167, 244, 116, 21);
			jTxtTotalDevuelto.setForeground(Color.BLACK);
			jTxtTotalDevuelto.setFont(new Font("Dialog", Font.BOLD, 13));
			jTxtTotalDevuelto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotalDevuelto.setEditable(false);
			jTxtTotalDevuelto.setFocusable(false);
		}
		return jTxtTotalDevuelto;
	}
	
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJTxtNroNotaCredito().requestFocus();
				}
			});
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	

	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(Integer codventa) {
		try {
			doLimpiar();
			String[] columnNames = { "Nota de cr�dito", "Fecha", "Monto", "Devoluci�n"," Factura ","Cliente", "Sucursal", "Estado"};
			String[] pks = { "codDevolucion" };
			String sSelect = "SELECT notacredito, to_char(fecha,'dd/MM/yyyy'), total, coddevolucion, "
					+ "(SELECT nrocomprobante from ventas v WHERE v.codventa = d.codventa),";
			sSelect += "(SELECT nombres||' '||apellidos FROM clientes c WHERE c.codcliente= d.codcliente), ";
			sSelect += "(SELECT nombre FROM sucursales s WHERE s.codsucursal= d.codsucursal), "
					+ "CASE WHEN estadonotacredito = 'A' THEN 'Activo' ELSE 'Anulado' END ";
			sSelect += "FROM devoluciones d ";
			sSelect += "WHERE codventa = "+codventa+" ";
			sSelect += "ORDER BY notacredito ";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Devolucion", pks);
			jb.setVisible(true);
			getJTxtNroNotaCredito().requestFocus();
			getJTxtNroFactura().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = { "Nota de cr�dito", "Fecha", "Monto", "Devoluci�n"," Factura ","Cliente", "Sucursal", "Estado"};
			String[] pks = { "codDevolucion" };
			String sSelect = "SELECT notacredito, to_char(fecha,'dd/MM/yyyy'), total, coddevolucion, "
					+ "(SELECT nrocomprobante from ventas v WHERE v.codventa = d.codventa),";
			sSelect += "(SELECT nombres||' '||apellidos FROM clientes c WHERE c.codcliente= d.codcliente), ";
			sSelect += "(SELECT nombre FROM sucursales s WHERE s.codsucursal= d.codsucursal), "
					+ "CASE WHEN estadonotacredito = 'A' THEN 'Activo' ELSE 'Anulado' END ";
			sSelect += "FROM devoluciones d ";
			sSelect += "ORDER BY notacredito ";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Devolucion", pks);
			jb.setVisible(true);
			getJTxtNroNotaCredito().requestFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbAnular() {
		if (btnpnmbAnular == null) {
			btnpnmbAnular = new JButtonPanambi();
			btnpnmbAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnularDevolucion();
				}
			});
			btnpnmbAnular.setText("Anular");
		}
		return btnpnmbAnular;
	}
	
	private void doAnularDevolucion(){
		try{
			if(devolucion == null){
				DlgMessage.showMessage(getOwner(),"Debe cargar los datos de la nota de cr�dito", DlgMessage.ERROR_MESSAGE);
				getJTxtNroNotaCredito().requestFocus();
			}else if(devolucion.getEstadoNotaCredito().equals("I")){
				DlgMessage.showMessage(getOwner(), "No puede anular la nota de cr�dito.\nLa nota de cr�dito ya se encuentra anulada", DlgMessage.ERROR_MESSAGE);
				getJTxtNroNotaCredito().requestFocus();
			}else if(controladorDevolucion.notaCreditoUsada(JFramePanambiMain.session.getConn(), devolucion.getNotaCredito())){
				DlgMessage.showMessage(getOwner(), "No puede anular la nota de cr�dito.\nLa nota de cr�dito ya se ha utilizado como medio de pago en una venta.", DlgMessage.ERROR_MESSAGE);
				getJTxtNroNotaCredito().requestFocus();
			}else{
				Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea anular la nota de cr�dito n�mero "+devolucion.getNotaCredito()+" ?",
						"", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (ret == JOptionPane.YES_OPTION) {
					controladorDevolucion.anularNotaCredito(JFramePanambiMain.session.getConn(), devolucion.getNotaCredito());
					DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJTxtNroNotaCredito().requestFocus();
				}
			}
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JLabelPanambi getLblpnmbCodDevolucion() {
		if (lblpnmbCodDevolucion == null) {
			lblpnmbCodDevolucion = new JLabelPanambi();
			lblpnmbCodDevolucion.setBounds(6, 139, 149, 20);
			lblpnmbCodDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodDevolucion.setText("Cod. Devolucion asociado : ");
		}
		return lblpnmbCodDevolucion;
	}
	private JTextFieldInteger getJTxtCodDevolucion() {
		if (jTxtCodDevolucion == null) {
			jTxtCodDevolucion = new JTextFieldInteger();
			jTxtCodDevolucion.setBounds(165, 138, 118, 21);
			jTxtCodDevolucion.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusCodDevolucion();
				}
			});
		}
		return jTxtCodDevolucion;
	}
	private JLabelPanambi getLblpnmbNotaDeCredito() {
		if (lblpnmbNotaDeCredito == null) {
			lblpnmbNotaDeCredito = new JLabelPanambi();
			lblpnmbNotaDeCredito.setBounds(45, 33, 110, 20);
			lblpnmbNotaDeCredito.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNotaDeCredito.setText("Nota de credito nro : ");
		}
		return lblpnmbNotaDeCredito;
	}
	private JTextFieldInteger getJTxtNroNotaCredito() {
		if (jTxtNroNotaCredito == null) {
			jTxtNroNotaCredito = new JTextFieldInteger();
			jTxtNroNotaCredito.setBounds(165, 32, 118, 21);
			jTxtNroNotaCredito.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroNotaCredito();
				}
			});
		}
		return jTxtNroNotaCredito;
	}
	
	private void lostFocusNroNotaCredito(){
		if(getJTxtNroNotaCredito().getValor()==null){
			//getJTxtNroFactura().requestFocus();
		}else{
			try {
				Devolucion dev = controladorDevolucion.getNotaCredito(JFramePanambiMain.session.getConn(), getJTxtNroNotaCredito().getValor());
				if (dev!= null) {
					setValues(dev, null);
				} else {
					devolucion = null;
					getJTxtNroNotaCredito().setValor(null);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	private JLabelPanambi getLblpnmbEstadoDeNota() {
		if (lblpnmbEstadoDeNota == null) {
			lblpnmbEstadoDeNota = new JLabelPanambi();
			lblpnmbEstadoDeNota.setBounds(330, 77, 135, 20);
			lblpnmbEstadoDeNota.setText("Estado de nota de credito : ");
			lblpnmbEstadoDeNota.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbEstadoDeNota;
	}
	private JTextField getJTxtEstadoNotaCredito() {
		if (jTxtEstadoNotaCredito == null) {
			jTxtEstadoNotaCredito = new JTextField();
			jTxtEstadoNotaCredito.setBounds(486, 78, 118, 20);
			jTxtEstadoNotaCredito.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstadoNotaCredito.setEditable(false);
			jTxtEstadoNotaCredito.setColumns(10);
			jTxtEstadoNotaCredito.setFocusable(false);
		}
		return jTxtEstadoNotaCredito;
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setBounds(55, 192, 100, 20);
			lblpnmbUsuario.setText("Usuario : ");
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbUsuario;
	}
	private JTextFieldUpper getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextFieldUpper();
			jTxtUsuario.setBounds(165, 191, 118, 21);
			jTxtUsuario.setEditable(false);
			jTxtUsuario.setFocusable(false);
		}
		return jTxtUsuario;
	}
}


