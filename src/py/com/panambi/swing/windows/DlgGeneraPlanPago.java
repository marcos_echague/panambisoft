package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.PlanPago;
import py.com.panambi.controller.ControladorParametro;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDate;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.utils.RutinaSistemaFrances;

public class DlgGeneraPlanPago extends JDialogPanambi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7078833989999436773L;
	private final JPanel contentPanel = new JPanel();
	private JLabelPanambi lblpnmbPrimerVencimiento;
	private JTextFieldDate jTxtPrimerVencimiento;
	private JLabelPanambi lblpnmbMontoVenta;
	private JTextFieldDouble jTxtMontoVenta;
	private JLabelPanambi lblpnmbNroCuotas;
	private JTextFieldInteger jTxtNroCuotas;
	private JButtonPanambi btnpnmbGenerarCuotero;
	private JScrollPane scrollPane;
	private JLabelPanambi lblpnmbInteres;
	private JTextFieldDouble jTxtFieldTasa;
	private JTablePanambi jTable;
	private Double capital;
	private ControladorParametro controladorParametro = new ControladorParametro();
	private JButtonPanambi btnpnmbAceptar;
	private List<PlanPago> planespago;

	/**
	 * Create the dialog.
	 */
	public DlgGeneraPlanPago(Window owner, Double capital) throws Exception {
		super(owner);
		this.capital = capital;
		initialize();
	}

	private void initialize() throws Exception {
		setBounds(100, 100, 653, 445);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel jPanelCentral = new JPanel();
			contentPanel.add(jPanelCentral, BorderLayout.CENTER);
			JPanel jPanelCabecera = new JPanel();
			jPanelCabecera.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_jPanelCentral = new GroupLayout(jPanelCentral);
			gl_jPanelCentral.setHorizontalGroup(gl_jPanelCentral.createParallelGroup(Alignment.LEADING).addGroup(
					gl_jPanelCentral
							.createSequentialGroup()
							.addContainerGap()
							.addGroup(
									gl_jPanelCentral.createParallelGroup(Alignment.LEADING).addGroup(gl_jPanelCentral.createSequentialGroup().addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE).addContainerGap())
											.addGroup(gl_jPanelCentral.createSequentialGroup().addComponent(jPanelCabecera, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(11)))));
			gl_jPanelCentral.setVerticalGroup(gl_jPanelCentral.createParallelGroup(Alignment.LEADING).addGroup(
					gl_jPanelCentral.createSequentialGroup().addContainerGap().addComponent(jPanelCabecera, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)));
			{
				lblpnmbPrimerVencimiento = new JLabelPanambi();
				lblpnmbPrimerVencimiento.setText("Primer Vencimiento");
			}
			GroupLayout gl_jPanelCabecera = new GroupLayout(jPanelCabecera);
			gl_jPanelCabecera.setHorizontalGroup(gl_jPanelCabecera.createParallelGroup(Alignment.LEADING).addGroup(
					gl_jPanelCabecera
							.createSequentialGroup()
							.addContainerGap()
							.addGroup(
									gl_jPanelCabecera
											.createParallelGroup(Alignment.LEADING)
											.addGroup(
													gl_jPanelCabecera.createSequentialGroup().addComponent(lblpnmbPrimerVencimiento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getJTxtPrimerVencimiento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(
													gl_jPanelCabecera.createSequentialGroup().addComponent(getLblpnmbNroCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getJTxtNroCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(
									gl_jPanelCabecera
											.createParallelGroup(Alignment.TRAILING)
											.addGroup(
													Alignment.LEADING,
													gl_jPanelCabecera.createSequentialGroup().addComponent(getLblpnmbMontoVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getJTxtMontoVenta(), GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
											.addGroup(
													Alignment.LEADING,
													gl_jPanelCabecera.createSequentialGroup().addComponent(getLblpnmbInteres(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getJTxtFieldTasa(), GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(getBtnpnmbGenerarCuotero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(18)
															.addComponent(getBtnpnmbAceptar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap(21, Short.MAX_VALUE)));
			gl_jPanelCabecera.setVerticalGroup(gl_jPanelCabecera.createParallelGroup(Alignment.LEADING).addGroup(
					gl_jPanelCabecera
							.createSequentialGroup()
							.addContainerGap()
							.addGroup(
									gl_jPanelCabecera
											.createParallelGroup(Alignment.TRAILING)
											.addGroup(
													gl_jPanelCabecera.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbMontoVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtMontoVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(
													gl_jPanelCabecera.createParallelGroup(Alignment.BASELINE).addComponent(lblpnmbPrimerVencimiento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtPrimerVencimiento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(9)
							.addGroup(
									gl_jPanelCabecera
											.createParallelGroup(Alignment.LEADING)
											.addGroup(
													gl_jPanelCabecera.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbNroCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtNroCuotas(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(
													gl_jPanelCabecera.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbInteres(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtFieldTasa(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(getBtnpnmbGenerarCuotero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getBtnpnmbAceptar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap(15, Short.MAX_VALUE)));
			jPanelCabecera.setLayout(gl_jPanelCabecera);
			jPanelCentral.setLayout(gl_jPanelCentral);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			{
				JLabel label = new JLabel("");
				label.setIcon(new ImageIcon(JDialogPanambi.class.getResource("/py/com/panambi/images/bannerInternalFrame.png")));
				panel.add(label);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
		this.centerIt();
		this.setShortcutsHere(this);
		initComponents();
	}

	private void initComponents() throws Exception {
		getJTxtFieldTasa().setValue(controladorParametro.getTasaPrestamo(JFramePanambiMain.session.getConn()));
		getJTxtMontoVenta().setValue(getCapital());

	}

	private JTextFieldDate getJTxtPrimerVencimiento() {
		if (jTxtPrimerVencimiento == null) {
			jTxtPrimerVencimiento = new JTextFieldDate();
		}
		return jTxtPrimerVencimiento;
	}

	private JLabelPanambi getLblpnmbMontoVenta() {
		if (lblpnmbMontoVenta == null) {
			lblpnmbMontoVenta = new JLabelPanambi();
			lblpnmbMontoVenta.setText("Monto Venta");
		}
		return lblpnmbMontoVenta;
	}

	private JTextFieldDouble getJTxtMontoVenta() {
		if (jTxtMontoVenta == null) {
			jTxtMontoVenta = new JTextFieldDouble();
			jTxtMontoVenta.setDisabledTextColor(Color.BLACK);
			jTxtMontoVenta.setEnabled(false);
			jTxtMontoVenta.setEditable(false);
		}
		return jTxtMontoVenta;
	}

	private JLabelPanambi getLblpnmbNroCuotas() {
		if (lblpnmbNroCuotas == null) {
			lblpnmbNroCuotas = new JLabelPanambi();
			lblpnmbNroCuotas.setText("Nro. Cuotas");
		}
		return lblpnmbNroCuotas;
	}

	private JTextFieldInteger getJTxtNroCuotas() {
		if (jTxtNroCuotas == null) {
			jTxtNroCuotas = new JTextFieldInteger();
		}
		return jTxtNroCuotas;
	}

	private JButtonPanambi getBtnpnmbGenerarCuotero() {
		if (btnpnmbGenerarCuotero == null) {
			btnpnmbGenerarCuotero = new JButtonPanambi();
			btnpnmbGenerarCuotero.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGenerarCuotero();
				}
			});
			btnpnmbGenerarCuotero.setMnemonic('G');
			btnpnmbGenerarCuotero.setText("Generar Cuotero");
		}
		return btnpnmbGenerarCuotero;
	}

	private void doGenerarCuotero() {
		try {
			checkData();
			getJTable().resetData(0);
			RutinaSistemaFrances rutina = new RutinaSistemaFrances(getCapital(), getJTxtNroCuotas().getValor(), (Double) getJTxtFieldTasa().getValue() / 100 / 12, getJTxtPrimerVencimiento().getFecha(), 1);
			List<PlanPago> cuotas = rutina.generar();
			int i = 0;
			for (PlanPago planPago : cuotas) {
				getJTable().addRow();
				getJTable().setValueAt(planPago.getVencimiento(), i, 0);
				getJTable().setValueAt(planPago.getNroCuota(), i, 1);
				getJTable().setValueAt(planPago.getMonto(), i, 2);
				getJTable().setValueAt(planPago, i, 3);
				i++;
			}
			if (i > 0) {
				getJTable().changeSelection(0, 0, true, true);
			}
			getBtnpnmbGenerarCuotero().transferFocus();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			logger.error(e.getMessage(), e);
		}

	}

	private void checkData() throws Exception {
		Double baseprestamo = controladorParametro.getBasePrestamo(JFramePanambiMain.session.getConn());
		if (getJTxtNroCuotas().getValor() == null || getJTxtNroCuotas().getValor() < 0) {
			throw new ValidException("El n�mero de cuotas debe ser mayor a 0");
		}
		if (getJTxtPrimerVencimiento().getValue() == null) {
			throw new ValidException("Debe especificar la fecha del primer vencimiento.");
		}
		if (getJTxtFieldTasa().getValue() == null || ((Double) getJTxtFieldTasa().getValue()) == 0.0) {
			throw new ValidException("Falta configurar la tasa de inter�s.");
		}
		if (getJTxtMontoVenta().getValue() == null) {
			throw new ValidException("No se ha establecido el capital.");
		}
		if (((Double) getJTxtMontoVenta().getValue()) < baseprestamo) {
			throw new ValidException("No se permite el pago en cuotas para montos menores a " + baseprestamo);
		}
		GregorianCalendar gcNow = new GregorianCalendar();
		gcNow.set(Calendar.HOUR_OF_DAY, 0);
		gcNow.set(Calendar.MINUTE, 0);
		gcNow.set(Calendar.SECOND, 0);
		gcNow.set(Calendar.MILLISECOND, 0);
		GregorianCalendar gcPrimerVencimiento = new GregorianCalendar();
		gcPrimerVencimiento.setTimeInMillis(getJTxtPrimerVencimiento().getFecha().getTime());
		if (gcPrimerVencimiento.before(gcNow)) {
			throw new ValidException("La fecha del primer vencimiento no puede ser menor a la del d�a.");
		}

	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			scrollPane.setViewportView(getJTable());
		}
		return scrollPane;
	}

	private JLabelPanambi getLblpnmbInteres() {
		if (lblpnmbInteres == null) {
			lblpnmbInteres = new JLabelPanambi();
			lblpnmbInteres.setText("Tasa Interes Anual");
		}
		return lblpnmbInteres;
	}

	private JTextFieldDouble getJTxtFieldTasa() {
		if (jTxtFieldTasa == null) {
			jTxtFieldTasa = new JTextFieldDouble();
			jTxtFieldTasa.setEnabled(false);
			jTxtFieldTasa.setEditable(false);
			jTxtFieldTasa.setDisabledTextColor(Color.BLACK);
		}
		return jTxtFieldTasa;
	}

	private JTablePanambi getJTable() {
		if (jTable == null) {
			String[] columnNames = { "Vencimiento", "Nro. Cuota", "Monto", "Data" };
			jTable = new JTablePanambi(columnNames);
			jTable.getColumnModel().getColumn(0).setPreferredWidth(80);
			jTable.getColumnModel().getColumn(1).setPreferredWidth(30);
			jTable.getColumnModel().getColumn(2).setPreferredWidth(300);
			jTable.getColumnModel().getColumn(3).setMinWidth(0);
			jTable.getColumnModel().getColumn(3).setPreferredWidth(0);
			jTable.getColumnModel().getColumn(3).setMaxWidth(0);
		}
		return jTable;
	}

	/**
	 * @return the capital
	 */
	public Double getCapital() {
		return capital;
	}

	/**
	 * @param capital the capital to set
	 */
	public void setCapital(Double capital) {
		this.capital = capital;
	}

	private void setShortcutsHere(Container component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					//					doCancelar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}

	/**
	 * 
	 */
	private void doAceptar() {
		List<PlanPago> planes = new ArrayList<PlanPago>();
		for (int i = 0; i < getJTable().getRowCount(); i++) {
			PlanPago planpago = (PlanPago) getJTable().getValueAt(i, 3);
			planes.add(planpago);
		}
		if (planes.size() > 0) {
			setPlanespago(planes);
		}
		this.dispose();
	}

	/**
	 * @return the planespago
	 */
	public List<PlanPago> getPlanespago() {
		return planespago;
	}

	/**
	 * @param planespago the planespago to set
	 */
	public void setPlanespago(List<PlanPago> planespago) {
		this.planespago = planespago;
	}

}
