/**
 * 
 */
package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import py.com.panambi.exceptions.PanambiException;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.utils.PanambiUtils;

/**
 *
 */
public class DlgMessage extends JDialogPanambi implements ClipboardOwner {

	/**
	 * 
	 */
	private static final long serialVersionUID = -547859934024265623L;
	private static final String DEFAULT_MESSAGE = "Ha ocurrido un error en la aplicación.";
	private String message = null;
	private Exception exception;
	private JPanel jContentPane = null;
	private JPanel jPanelCenter = null;
	private JLabel jLabelIcon = null;
	public static final int INFORMATION_MESSAGE = 0;
	public static final int ERROR_MESSAGE = 1;
	public static final int WARNING_MESSAGE = 2;
	public static final int QUESTION_MESSAGE = 3;
	private int typeError = ERROR_MESSAGE;
	private JTextArea jTxtMensaje = null;
	private JPanel jPanelBotones = null;
	private JButtonPanambi jBtnOk = null;
	private JButtonPanambi jBtnDetalles = null;
	private boolean statetrace = false;
	private JScrollPane jScrollPane = null;
	private JTextArea jTxtStactTrace;
	private JMenuItem menuItem;

	/**
	 * 
	 */
	public DlgMessage() {
		// TODO Auto-generated constructor stub
		super();
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Frame owner) {
		// TODO Auto-generated constructor stub
		super(owner);
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Dialog owner) {
		super(owner);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Window owner) {
		super(owner);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modal
	 */
	public DlgMessage(Frame owner, boolean modal) {
		super(owner, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public DlgMessage(Frame owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modal
	 */
	public DlgMessage(Dialog owner, boolean modal) {
		super(owner, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public DlgMessage(Dialog owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param modalityType
	 */
	public DlgMessage(Window owner, ModalityType modalityType) {
		super(owner, modalityType);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 */
	public DlgMessage(Window owner, String title) {
		super(owner, title);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 */
	public DlgMessage(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 */
	public DlgMessage(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modalityType
	 */
	public DlgMessage(Window owner, String title, ModalityType modalityType) {
		super(owner, title, modalityType);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @param gc
	 */
	public DlgMessage(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @param gc
	 */
	public DlgMessage(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 * @param title
	 * @param modalityType
	 * @param gc
	 */
	public DlgMessage(Window owner, String title, ModalityType modalityType, GraphicsConfiguration gc) {
		super(owner, title, modalityType, gc);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Dialog owner, Exception e, int type) {
		super(owner);
		setException(e);
		setTypeError(type);
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Window owner, String message, int type) {
		super(owner);
		setMessage(message);
		setTypeError(type);
		initialize();
	}

	/**
	 * @param owner
	 *            Ultimo Agregado
	 */
	public DlgMessage(Window owner, String message, Exception e, int type) {
		super(owner);
		setMessage(message);
		setTypeError(type);
		setException(e);
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Window owner, Exception e, int type) {
		super(owner);
		setException(e);
		setTypeError(type);
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Dialog owner, String message, int type) {
		super(owner);
		setMessage(message);
		setTypeError(type);
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgMessage(Dialog owner, String message, Exception e, int type) {
		super(owner);
		setException(e);
		setMessage(message);
		setTypeError(type);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(385, 115);
		this.setContentPane(getJContentPane());
		this.setResizable(false);
		showhidetrace(false);
		centerIt();
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelCenter(), BorderLayout.NORTH);
			jContentPane.add(getJScrollPane(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelCenter
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelCenter() {
		try {
			if (jPanelCenter == null) {
				GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
				gridBagConstraints2.gridx = 0;
				gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
				gridBagConstraints2.gridwidth = 2;
				gridBagConstraints2.gridy = 1;
				GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
				gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
				gridBagConstraints3.gridy = 0;
				gridBagConstraints3.weightx = 1.0;
				gridBagConstraints3.weighty = 1.0;
				gridBagConstraints3.anchor = GridBagConstraints.WEST;
				gridBagConstraints3.insets = new Insets(3, 0, 0, 0);
				gridBagConstraints3.ipady = 7;
				gridBagConstraints3.gridx = 1;
				GridBagConstraints gridBagConstraints = new GridBagConstraints();
				gridBagConstraints.gridx = 0;
				gridBagConstraints.insets = new Insets(5, 5, 0, 5);
				gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
				gridBagConstraints.weightx = 0.0;
				gridBagConstraints.weighty = 0.0;
				gridBagConstraints.gridy = 0;
				jPanelCenter = new JPanel();
				jPanelCenter.setLayout(new GridBagLayout());
				String filename = "/py/com/panambi/images/Error.png";
				switch (typeError) {
				case ERROR_MESSAGE:
					filename = "/py/com/panambi/images/Error.png";
					break;
				case INFORMATION_MESSAGE:
					filename = "/py/com/panambi/images/Info.png";
					break;
				case WARNING_MESSAGE:
					filename = "/py/com/panambi/images/Warning.png";
					break;
				default:
					break;
				}
				ImageIcon icon = PanambiUtils.createImageIcon(filename, "Logo");
				jLabelIcon = new JLabel(icon);
				jPanelCenter.add(jLabelIcon, gridBagConstraints);
				jPanelCenter.add(getJTxtMensaje(), gridBagConstraints3);
				jPanelCenter.add(getJPanelBotones(), gridBagConstraints2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jPanelCenter;
	}

	/**
	 * @return the type
	 */
	public int getTypeError() {
		return typeError;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setTypeError(int type) {
		this.typeError = type;
	}

	/**
	 * This method initializes jTxtMensaje
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getJTxtMensaje() {
		if (jTxtMensaje == null) {
			jTxtMensaje = new JTextArea();
			switch (typeError) {
			case ERROR_MESSAGE:
				if (getMessage() != null) {
					jTxtMensaje.setText(getMessage());
				} else if (getException() != null) {
					jTxtMensaje.setText(getException().getMessage());
				} else {
					jTxtMensaje.setText(DEFAULT_MESSAGE);
				}
				break;
			case INFORMATION_MESSAGE:
				jTxtMensaje.setText(getMessage());
				break;
			case WARNING_MESSAGE:
				if (getMessage() != null) {
					jTxtMensaje.setText(getMessage());
				} else if (getException() != null) {
					jTxtMensaje.setText(getException().getMessage());
				} else {
					jTxtMensaje.setText(DEFAULT_MESSAGE);
				}
				break;
			default:
				break;
			}
			jTxtMensaje.setBackground(null);
			jTxtMensaje.setBorder(null);
			jTxtMensaje.setOpaque(false);
			jTxtMensaje.setDisabledTextColor(Color.BLACK);
			jTxtMensaje.setEditable(false);
			jTxtMensaje.setEnabled(false);
			jTxtMensaje.setFont(FontPanambi.getLabelInstance());
			jTxtMensaje.setAlignmentX(JComponent.LEFT_ALIGNMENT);
			jTxtMensaje.setAlignmentY(JComponent.CENTER_ALIGNMENT);
			jTxtMensaje.setPreferredSize(new Dimension(240, 40));
			jTxtMensaje.setMaximumSize(new Dimension(1024, 64));
			jTxtMensaje.setLineWrap(true);
			jTxtMensaje.setWrapStyleWord(true);
		}
		return jTxtMensaje;
	}

	/**
	 * This method initializes jPanelBotones
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelBotones() {
		if (jPanelBotones == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			jPanelBotones = new JPanel();
			jPanelBotones.setLayout(flowLayout);
			jPanelBotones.add(getJBtnOk(), null);
			if ((typeError == ERROR_MESSAGE || typeError == WARNING_MESSAGE) && exception != null) {
				if (!(exception instanceof PanambiException)) {
					jPanelBotones.add(getJBtnDetalles(), null);
				}
			}
		}
		return jPanelBotones;
	}

	/**
	 */
	private JButtonPanambi getJBtnOk() {
		if (jBtnOk == null) {
			jBtnOk = new JButtonPanambi();
			jBtnOk.setText("Aceptar");
			jBtnOk.setMnemonic(KeyEvent.VK_A);
			jBtnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					doAceptar();
				}
			});
		}
		return jBtnOk;
	}

	private void doAceptar() {
		this.dispose();
	}

	/**
	 */
	private JButtonPanambi getJBtnDetalles() {
		if (jBtnDetalles == null) {
			jBtnDetalles = new JButtonPanambi();
			jBtnDetalles.setMnemonic(KeyEvent.VK_V);
			jBtnDetalles.setText("Ver Detalles >>");
			jBtnDetalles.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					doVerdetalles();
				}
			});
		}
		return jBtnDetalles;
	}

	private void doVerdetalles() {
		showhidetrace(!statetrace);
	}

	/**
	 * This method initializes jScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setAutoscrolls(true);
			jScrollPane.setViewportView(getJTxtStactTrace());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jTxtStactTrace
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getJTxtStactTrace() {
		if (jTxtStactTrace == null) {
			jTxtStactTrace = new JTextArea();
			jTxtStactTrace.setAutoscrolls(true);
			jTxtStactTrace.setEditable(false);
			jTxtStactTrace.setFont(FontPanambi.getLabelInstance());
			JPopupMenu popup = new JPopupMenu();
			menuItem = new JMenuItem("Seleccionar Todo");
			menuItem.setFont(FontPanambi.getLabelInstance());
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionar();
				}
			});
			popup.add(menuItem);
			menuItem = new JMenuItem("Copiar");
			menuItem.setFont(FontPanambi.getLabelInstance());
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doCopiar();
				}

			});
			popup.add(menuItem);

			// Add listener to the text area so the popup menu can come up.
			MouseListener popupListener = new PopupListener(popup);
			jTxtStactTrace.addMouseListener(popupListener);
			if (getException() != null) {
				jTxtStactTrace.setText(PanambiUtils.getStackTrace(getException()));
			}
		}
		return jTxtStactTrace;
	}

	private void doCopiar() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(new StringSelection(getJTxtStactTrace().getSelectedText()), this);
	}

	private void doSeleccionar() {
		getJTxtStactTrace().requestFocus();
		getJTxtStactTrace().selectAll();
	}

	private void showhidetrace(boolean visible) {
		getJScrollPane().setVisible(visible);
		statetrace = visible;
		if (statetrace) {
			getJBtnDetalles().setText("<<Ocultar Detalle");
			this.setPreferredSize(new Dimension(this.getWidth(), 385));
			getJScrollPane().getVerticalScrollBar().setValue(0);
		} else {
			getJBtnDetalles().setText("Ver Detalle >>");
			this.setPreferredSize(new Dimension(this.getWidth(), 115));
		}
		pack();
	}

	/**
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception
	 *            the exception to set
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public static void showMessage(Window owner, Exception exception, int type) {
		DlgMessage dlg = new DlgMessage(owner, exception, type);
		dlg.setVisible(true);
	}

	public static void showMessage(Window owner, String message, Exception exception, int type) {
		DlgMessage dlg = new DlgMessage(owner, message, exception, type);
		dlg.setVisible(true);
	}

	public static void showMessage(Window owner, String message, int type) {
		DlgMessage dlg = new DlgMessage(owner, message, type);
		dlg.setVisible(true);
	}

	public static void showMessage(Dialog owner, Exception exception, int type) {
		DlgMessage dlg = new DlgMessage(owner, exception, type);
		dlg.setVisible(true);
	}

	public static void showMessage(Dialog owner, String message, int type) {
		DlgMessage dlg = new DlgMessage(owner, message, type);
		dlg.setVisible(true);
	}

	public static void showMessage(Dialog owner, String message, Exception exception, int type) {
		DlgMessage dlg = new DlgMessage(owner, message, exception, type);
		dlg.setVisible(true);
	}

	class PopupListener extends MouseAdapter {
		JPopupMenu popup;

		PopupListener(JPopupMenu popupMenu) {
			popup = popupMenu;
		}

		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}

	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		// TODO Auto-generated method stub

	}

} // @jve:decl-index=0:visual-constraint="10,10"

