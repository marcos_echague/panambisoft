package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.ProductoPerdido;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Usuario;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorProductoPerdido;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.prompts.PromptProductos;

public class JIConsultarAnularProductoPerdido extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4386990495135011952L;
	private JLabelPanambi lblpnmbUsuario;
	private JComboBoxPanambi jCmbUsuario;
	private JScrollPane scrollPaneProductosPerdidos;
	private JLabelPanambi lblpnmbProducto;
	private JComboBoxPanambi jCmbProducto;
	private JTable jTblProductoPerdido;
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JButtonPanambi jBtnConsultar;
	private ControladorProductoPerdido controladorProductoPerdido = new ControladorProductoPerdido();
	private ControladorUsuario controladorUsuario = new ControladorUsuario();
	private Usuario usuario;
	private List<String> usuarios = new ArrayList<String>();
	private ControladorProducto controladorProducto= new ControladorProducto();
	private Producto producto;
	private List<String> productos = new ArrayList<String>();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private Sucursal sucursal ;
	private List<String> sucursales = new ArrayList<String>();
	private DefaultTableModel dtmProductoPerdido;
	private JButton jBtnSU;
	private JButton jBtnSP;
	private JButton jBtnSS;
	private JPanel panel;
	private JMenuItem menuItemAnular;
	private JPopupMenu popupMenu;
	private JLabelPanambi lblpnmbCodigo;
	private JTextFieldInteger jTxtCodProducto;
	private JXDatePicker jDPFechaDesde;
	private JLabelPanambi lblpnmbFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechaHasta;
	private JButtonPanambi btnpnmbAnular;
	
	public JIConsultarAnularProductoPerdido() throws Exception {
		initialize();
	}
	private void initialize() {
		getJPanelCentral().setToolTipText("Seleccionar sucursal");
		setMaximizable(false);
		setBounds(100, 100, 1074, 484);
		setTitle("Consultar/Anular productos dados de baja");
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getBtnpnmbAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(32)
							.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(5)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getJBtnSU())
								.addComponent(getJBtnSS()))
							.addPreferredGap(ComponentPlacement.RELATED, 157, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(296)
									.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(14)
									.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 412, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
							.addGap(155)))
					.addContainerGap(23, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(getScrollPaneProductosPerdidos(), GroupLayout.PREFERRED_SIZE, 1003, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(30, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(37)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(31)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJCmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(30)
									.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJBtnSU())
									.addGap(28)
									.addComponent(getJBtnSS()))))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)))
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblpnmbFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(getScrollPaneProductosPerdidos(), GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
					.addGap(16))
		);
		getJPanelCentral().setLayout(groupLayout);
		setHelp("consultarproductosdebaja");
		setShortcuts(this);
		agregarMenuPoput();
		doLimpiar();
		decorate();
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemAnular());
		getJTblProductoPerdido().setComponentPopupMenu(popupMenu);
	}
	private JMenuItem getMenuItemAnular(){
		if (menuItemAnular == null) {
			menuItemAnular = new JMenuItem("Anular");
			menuItemAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
		}
		return menuItemAnular; 
	}
	
	private void doAnular(){
		try{
			Integer fila = getJTblProductoPerdido().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Imposible anular.\nDebe seleccionar un regitro", DlgMessage.ERROR_MESSAGE);
			}else{
				
				ProductoPerdido productoNul = controladorProductoPerdido.getProductoPerdido(JFramePanambiMain.session.getConn(), (Integer)getJTblProductoPerdido().getValueAt(fila, 0));
				
				if(productoNul.getEstado().equals("I")){
					DlgMessage.showMessage(getOwner(), "Imposible anular compra.\nEl registro ya se encuentra anulado.", DlgMessage.ERROR_MESSAGE);
				}else{
					Integer ret = null;
					
						ret = JOptionPane.showInternalConfirmDialog(this, "Desea anular el registro del producto dado de baja?" ,"" , JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (ret == JOptionPane.YES_OPTION) {
							if(productoNul.getComentario()!=null){
								productoNul.setComentario(productoNul.getComentario().trim()+"\n ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+". ");	
							}else{
								productoNul.setComentario("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+". ");
							}
							
							String comentariosAnulacion  = JOptionPane.showInputDialog("Ingrese un comentario por la anulación del registro de producto de baja", "");
							if(comentariosAnulacion!=null){
								
								if(comentariosAnulacion.equals("")){
									DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario", DlgMessage.WARNING_MESSAGE);
								}else{
									productoNul.setComentario(productoNul.getComentario().trim()+" "+comentariosAnulacion);
									controladorProductoPerdido.anularProductoPerdido(JFramePanambiMain.session.getConn(), productoNul);
									DlgMessage.showMessage(getOwner(), "Anulación exitosa.", DlgMessage.INFORMATION_MESSAGE);
									doConsultarProductoPerdido();
									getJCmbProducto().requestFocus();
								}
							}
							
						}
					}
				}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuario.setText("Usuario : ");
		}
		return lblpnmbUsuario;
	}
	private JComboBoxPanambi getJCmbUsuario() {
		if (jCmbUsuario == null) {
			jCmbUsuario = new JComboBoxPanambi();
			jCmbUsuario.setToolTipText("Seleccionar usuario");
		}
		return jCmbUsuario;
	}
	private JScrollPane getScrollPaneProductosPerdidos() {
		if (scrollPaneProductosPerdidos == null) {
			scrollPaneProductosPerdidos = new JScrollPane();
			scrollPaneProductosPerdidos.setViewportView(getJTblProductoPerdido());
		}
		return scrollPaneProductosPerdidos;
	}
	private JLabelPanambi getLblpnmbProducto() {
		if (lblpnmbProducto == null) {
			lblpnmbProducto = new JLabelPanambi();
			lblpnmbProducto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbProducto.setText("Descripcion : ");
		}
		return lblpnmbProducto;
	}
	private JComboBoxPanambi getJCmbProducto() {
		if (jCmbProducto == null) {
			jCmbProducto = new JComboBoxPanambi();
			jCmbProducto.setToolTipText("Seleccionar Producto");
			jCmbProducto.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusProducto();
				}
			});
		}
		return jCmbProducto;
	}
	
	private void lostFocusProducto(){
		try {
			Producto pro = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), (String) jCmbProducto.getSelectedItem());
			if (pro != null) {
				getJTxtCodProducto().setValor(pro.getCodProducto());
				getJTblProductoPerdido().requestFocus();
			} else {
				getJTblProductoPerdido().requestFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JTable getJTblProductoPerdido() {
		if (jTblProductoPerdido == null) {
			jTblProductoPerdido = new JTable();
			jTblProductoPerdido.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Codigo", "Fecha", "Usuario", "Producto", "Sucursal", "Cantidad", "Comentarios", "Estado","Fecha anulacion"
				}
			));

		}
		return jTblProductoPerdido;
	}
	
	
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			usuario = null;
			producto = null;
			sucursal = null;
			
			poblarListaUsuarios();
			getJCmbUsuario().setModel(new ListComboBoxModel<>(usuarios));
			getJCmbUsuario().setSelectedItem(null);
			
			poblarListaProductos();
			getJCmbProducto().setModel(new ListComboBoxModel<>(productos));
			getJCmbProducto().setSelectedItem(null);
			
			poblarListaSucursales();
			getJCmbSucursal().setModel(new ListComboBoxModel<>(sucursales));
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			
			getJTxtCodProducto().setValor(null);
			//cargarTablaCompleta();
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
				getJBtnSS().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
				getJBtnSS().setEnabled(false);
			}
			
			
		}catch(Exception e ){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	@SuppressWarnings({ "serial", "unused" })
	private void cargarTablaCompleta(){
		try{
			ArrayList<ProductoPerdido> productosPerdidos =  controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn());
			dtmProductoPerdido= new DefaultTableModel() {
				public boolean isCellEditable(int reg, int column) {
					return false;
				}
			};
			Object[] cabecera = { "Codigo", "Fecha", "Usuario", "Producto", "Sucursal", "Cantidad","Comentarios", "Estado", "Fecha anulacion" };
			dtmProductoPerdido.setColumnIdentifiers(cabecera);
			@SuppressWarnings("rawtypes")
			Iterator iterator = productosPerdidos.listIterator();
			while (iterator.hasNext()) {
				ProductoPerdido pp = (ProductoPerdido) iterator.next();
				String estado = null;
				if(pp.getEstado().equals("A")){
					estado = "Activo";
				}else estado = "Anulado";
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				String fechaAnulacion = null;
				if(pp.getFechaAnualcion()!=null){
					fechaAnulacion = formato.format(pp.getFechaAnualcion());
				}
				String fecha= null;
				if(pp.getFecha()!=null){
					fecha= formato.format(pp.getFecha());
				}
				Object[] row = { pp.getCodProductoPerdido(), fecha, pp.getUsuario().getUsuario(), pp.getProducto().getDescripcion(),pp.getSucursal().getNombre(),pp.getCantidad(),pp.getComentario(),estado, fechaAnulacion};
				dtmProductoPerdido.addRow(row);
			}

			getJTblProductoPerdido().setModel(dtmProductoPerdido);
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaUsuarios() throws Exception{
		usuarios = new ArrayList<String>();
		List<Usuario> listausu = controladorUsuario.getUsuarios(JFramePanambiMain.session.getConn());

		for (Usuario usu : listausu) {
			usuarios.add(usu.getUsuario());
		}
	}
	
	private void poblarListaProductos() throws Exception {
		productos = new ArrayList<String>();
		List<Producto> listaprod = controladorProducto.getProductos(JFramePanambiMain.session.getConn());

		for (Producto prod : listaprod) {
			productos.add(prod.getDescripcion());
		}
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales= new ArrayList<String>();
		List<Sucursal> listasuc = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());

		for (Sucursal suc : listasuc) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doConsultarProductoPerdido();
				}
			});
			jBtnConsultar.setToolTipText("Consultar productos dados de baja");
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	@SuppressWarnings({ "serial", "deprecation" })
	private void doConsultarProductoPerdido(){
		try{
			ArrayList<ProductoPerdido> productosPerdidos = null;
			usuario = controladorUsuario.getUsuario(JFramePanambiMain.session.getConn(), (String) getJCmbUsuario().getSelectedItem());
			producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(),(String) getJCmbProducto().getSelectedItem());
			sucursal = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(),(String) getJCmbSucursal().getSelectedItem());
			Date fechaDesde ;
			Date fechaHasta ;

			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
//			if(usuario==null && producto==null && sucursal==null ){
//				productosPerdidos=  controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn());
//			}else if(usuario !=null && producto==null && sucursal ==null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),usuario);
//			}else if(usuario ==null && producto!=null && sucursal ==null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),producto);
//			}else if(usuario ==null && producto==null && sucursal !=null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),sucursal);
//			}else if(usuario !=null && producto!=null && sucursal ==null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),usuario,producto);
//			}else if(usuario !=null && producto==null &&sucursal !=null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),usuario,sucursal);
//			}else if(usuario ==null && producto!=null &&sucursal !=null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),producto,sucursal);
//			}else if(usuario !=null && producto!=null &&sucursal !=null){
//				productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),usuario,producto,sucursal, fechaDesde, fechaHasta);
//			}
//			
			productosPerdidos = controladorProductoPerdido.getProductosPerdidos(JFramePanambiMain.session.getConn(),usuario,producto,sucursal, fechaDesde, fechaHasta);
			
			dtmProductoPerdido= new DefaultTableModel() {
				public boolean isCellEditable(int reg, int column) {
					return false;
				}
			};
			Object[] cabecera = { "Codigo", "Fecha", "Usuario", "Producto", "Sucursal", "Cantidad","Comentarios","Estado", "Fecha anulacion" };
			dtmProductoPerdido.setColumnIdentifiers(cabecera);
			if(productosPerdidos.isEmpty()){
				DlgMessage.showMessage(getOwner(), "No se encuentraron registros", DlgMessage.INFORMATION_MESSAGE);
			}
			
			@SuppressWarnings("rawtypes")
			Iterator iterator = productosPerdidos.listIterator();
			while (iterator.hasNext()) {
				ProductoPerdido pp = (ProductoPerdido) iterator.next();
				String estado;
				if(pp.getEstado().equals("A")){
					estado = "Activo";
				}else estado = "Anulado";
				String patron = "dd/MM/yyyy";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				String fechaAnulacion = null;
				if(pp.getFechaAnualcion()!=null){
					fechaAnulacion = formato.format(pp.getFechaAnualcion());
				}
				String fecha= null;
				if(pp.getFecha()!=null){
					fecha= formato.format(pp.getFecha());
				}
				Object[] row = { pp.getCodProductoPerdido(), fecha, pp.getUsuario().getUsuario(), pp.getProducto().getDescripcion(),pp.getSucursal().getNombre(),pp.getCantidad(),pp.getComentario(),estado, fechaAnulacion};
				dtmProductoPerdido.addRow(row);
			}
			getJTblProductoPerdido().setModel(dtmProductoPerdido);
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.DATE, dias);
        return new java.sql.Date(cal.getTimeInMillis());
   }
	
	private JButton getJBtnSU() {
		if (jBtnSU == null) {
			jBtnSU = new JButton("S/U");
			jBtnSU.setToolTipText("Sin usuario");
			jBtnSU.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSinUsuario();
				}
			});
		}
		return jBtnSU;
	}
	
	private JButton getJBtnSP() {
		if (jBtnSP == null) {
			jBtnSP = new JButton("S/P");
			jBtnSP.setToolTipText("Sin producto");
			jBtnSP.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSinProducto();
				}
			});
		}
		return jBtnSP;
	}
	private JButton getJBtnSS() {
		if (jBtnSS == null) {
			jBtnSS = new JButton("S/S");
			jBtnSS.setToolTipText("Sin sucursal");
			jBtnSS.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSinSucursal();
				}
			});
		}
		return jBtnSS;
	}
	
	private  void doSinUsuario(){
		getJCmbUsuario().setSelectedItem(null);
	}
	
	private  void doSinSucursal(){
		getJCmbSucursal().setSelectedItem(null);
	}
	
	private  void doSinProducto(){
		getJCmbProducto().setSelectedItem(null);
		getJTxtCodProducto().setValue("");
	}
	
	private void decorate(){
		AutoCompleteDecorator.decorate(getJCmbProducto());
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Producto", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(4)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(14)
						.addComponent(getLblpnmbProducto(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
						.addGap(7)
						.addComponent(getJCmbProducto(), GroupLayout.PREFERRED_SIZE, 233, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJBtnSP()))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(1)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(getJTxtCodProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJCmbProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJBtnSP())))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JTextFieldInteger getJTxtCodProducto() {
		if (jTxtCodProducto == null) {
			jTxtCodProducto = new JTextFieldInteger();
			jTxtCodProducto.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(e.getOppositeComponent() instanceof JButton) && jTxtCodProducto.getValor() != null) {
						Integer codproducto = ((JTextFieldInteger) e.getComponent()).getValor();
						setProductoFromCod(codproducto);
					}
					super.focusLost(e);
				}
			});
		}
		return jTxtCodProducto;
	}
	
	protected void setProductoFromCod(Integer codproducto) {
		try {
			if (codproducto == null) {
				throw new ValidException();
			}
			Producto producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), codproducto);
			if (producto != null) {
				setValues(producto, null);
			} else {
				setNoValues(new Producto(), null);
			}
		} catch (ValidException e) {
			doPrompProducto();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doPrompProducto() {
		getJTxtCodProducto().setValor(null);
		getJCmbProducto().setSelectedItem(null);
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = (Producto) obj;
			getJTxtCodProducto().setValor(producto.getCodProducto());
			getJCmbProducto().setSelectedItem(producto.getDescripcion());
			getJTblProductoPerdido().requestFocus();
		}
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Producto) {
			producto = null;
			getJTxtCodProducto().requestFocus();
			getJCmbProducto().setSelectedItem(null);
			getJTxtCodProducto().setValor(null);
		}
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JLabelPanambi getLblpnmbFechaDesde() {
		if (lblpnmbFechaDesde == null) {
			lblpnmbFechaDesde = new JLabelPanambi();
			lblpnmbFechaDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDesde.setText("Fecha desde : ");
		}
		return lblpnmbFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	private JButtonPanambi getBtnpnmbAnular() {
		if (btnpnmbAnular == null) {
			btnpnmbAnular = new JButtonPanambi();
			btnpnmbAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAnular();
				}
			});
			btnpnmbAnular.setToolTipText("Anular registro de producto perdido");
			btnpnmbAnular.setMnemonic('A');
			btnpnmbAnular.setText("Anular");
		}
		return btnpnmbAnular;
	}
}
