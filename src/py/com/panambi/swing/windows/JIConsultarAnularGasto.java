package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Gasto;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.TipoGasto;
import py.com.panambi.controller.ControladorGasto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.controller.ControladorTipoGasto;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIConsultarAnularGasto extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914865339823923546L;
	private JButtonPanambi jBtnAnular;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JComboBoxPanambi jCmbConcepto;
	private JComboBoxPanambi jCmbEstado;
	private JLabelPanambi lblpnmbConcepto;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbPesioneFPara;
	private JXDatePicker jDPFechaDesde;
	private JXDatePicker jDPFechaHasta;
	private JLabelPanambi lblpnmbFechasDesde;
	private JLabelPanambi lblpnmbFechaHasta;
	private ControladorTipoGasto controladorTipoGasto = new ControladorTipoGasto();
	private List<String> conceptosGastos = new ArrayList<String>();
	private List<String> estados = new ArrayList<String>();
	private List<String> sucursales = new ArrayList<String>();
	private JPopupMenu popupMenu;
	private JMenuItem menuItemDetalles;
	private JMenuItem menuItemAnular;
	private JButtonPanambi jBtnConsultar;
	private ControladorGasto controladorGasto = new ControladorGasto();
	private JComboBoxPanambi jCmbSucursal;
	private JLabelPanambi lblpnmbSucursal;
	
	
	public JIConsultarAnularGasto() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Consultar/Anular gastos");
		setBounds(100,100, 869, 480);
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnConsultar());
		getJPanelSouth().add(getJBtnAnular());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(707, Short.MAX_VALUE)
					.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addGap(7)
									.addComponent(getJCmbConcepto(), GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
									.addGap(32)
									.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(14)
									.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addGap(7)
									.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJCmbEstado(), GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))
								.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
									.addGap(67)))))
					.addGap(24))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(2)
									.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(getJCmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJCmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJCmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLblpnmbPesioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(37)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblpnmbFechasDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJDPFechaDesde(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJDPFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLblpnmbFechaHasta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(16)
					.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
					.addContainerGap())
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		agregarMenuPoput();
		setShortcuts(this);
		setHelp("consultarAnularGasto");
	}
	
	private void agregarMenuPoput(){
		popupMenu = new JPopupMenu();
		popupMenu.add(getMenuItemDetalles());
		popupMenu.add(getMenuItemAnular());
		getTablePanambi().setComponentPopupMenu(popupMenu);
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			conceptosGastos = null;
			poblarListaConceptos();
			poblarListaEstados();
			poblarListaSucursales();
			getJCmbConcepto().setModel(new ListComboBoxModel<String>(conceptosGastos));
			getJCmbEstado().setModel(new ListComboBoxModel<String>(estados));
			getJCmbSucursal().setModel(new ListComboBoxModel<String>(sucursales));
			getJDPFechaDesde().setDate(new java.util.Date());
			getJDPFechaHasta().setDate(new java.util.Date());
			getJCmbConcepto().setSelectedItem("TODOS");
			if(getTablePanambi().getRowCount()>0){
				getTablePanambi().resetData(0);
			}
			
			getJCmbSucursal().setSelectedItem(JFramePanambiMain.session.getSucursalOperativa().getNombre());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursal().setEnabled(true);
			}else{
				getJCmbSucursal().setEnabled(false);
			}
			
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarListaConceptos() throws Exception {
		conceptosGastos = new ArrayList<String>();
		List<TipoGasto> listaTipGasto = controladorTipoGasto.getTiposGastos(JFramePanambiMain.session.getConn());
		conceptosGastos.add("TODOS");
		for (TipoGasto tg : listaTipGasto) {
			conceptosGastos.add(tg.getConcepto());
		}
	}
	
	private void poblarListaEstados() throws Exception {
		estados = new ArrayList<String>();
		estados.add("TODOS");
		estados.add("ACTIVOS");
		estados.add("ANULADOS");
	}
	
	private void poblarListaSucursales() throws Exception {
		sucursales = new ArrayList<String>();
		sucursales.add("TODAS");
		List<Sucursal> listaSucursal = new ControladorSucursal().getSucursales(JFramePanambiMain.session.getConn());
		for (Sucursal suc : listaSucursal) {
			sucursales.add(suc.getNombre());
		}
	}
	
	
	private JMenuItem getMenuItemDetalles(){
		if (menuItemDetalles == null) {
			menuItemDetalles = new JMenuItem("Ver mas detalles...");
			menuItemDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doVerDetalles();
				}
			});
		}
		return menuItemDetalles; 
	}
	
	private void doVerDetalles(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Debe seleccionar un regitro para ver sus detalles.", DlgMessage.WARNING_MESSAGE);
			}else{
				
				Gasto gastoDet = (Gasto)getTablePanambi().getValueAt(fila, 8);
				DlgDetalleGastos dlgDetalleGasto= new DlgDetalleGastos(gastoDet);
				dlgDetalleGasto.centerIt();
				dlgDetalleGasto.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private JMenuItem getMenuItemAnular(){
		if (menuItemAnular == null) {
			menuItemAnular = new JMenuItem("Anular");
			menuItemAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
		}
		return menuItemAnular; 
	}
	
	private JButtonPanambi getJBtnAnular() {
		if (jBtnAnular == null) {
			jBtnAnular = new JButtonPanambi();
			jBtnAnular.setMnemonic('A');
			jBtnAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnular();
				}
			});
			jBtnAnular.setToolTipText("Anular gasto");
			jBtnAnular.setText("Anular");
		}
		return jBtnAnular;
	}
	
	
	private void doAnular(){
		try{
			Integer fila = getTablePanambi().getSelectedRow();
			if(fila == -1 ){
				DlgMessage.showMessage(getOwner(),"Imposible anular.\nDebe seleccionar un regitro", DlgMessage.ERROR_MESSAGE);
			}else{
				java.util.Date fechaActual = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Gasto gastonul = (Gasto)getTablePanambi().getValueAt(fila, 8);
				java.util.Date fechaGasto = new java.util.Date(gastonul.getFechaPago().getTime());
				if (!sdf.format(fechaActual).equals(sdf.format(fechaGasto))){
					DlgMessage.showMessage(getOwner(), "Imposible anular gasto.\nS�lo puede anular un gasto el mismo d�a de su creaci�n.", DlgMessage.WARNING_MESSAGE);
				}else{
					Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la anulaci�n del gasto ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (ret == JOptionPane.YES_OPTION) {
						gastonul = (Gasto)getTablePanambi().getValueAt(fila, 8);
						if(gastonul.getEstado().equals("I")){
							DlgMessage.showMessage(getOwner(), "Imposible anular registro de gasto.\nEl registro ya se encuentra anulado.", DlgMessage.ERROR_MESSAGE);
						}else{
							gastonul.setComentarios(gastonul.getComentarios().trim()+"\nANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+".");
							String comentariosAnulacion  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n del gasto", "");
							if(comentariosAnulacion!=null){
								
								if(comentariosAnulacion.equals("")){
									DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario de anulaci�n", DlgMessage.WARNING_MESSAGE);
								
								}else{
									gastonul.setComentarios(gastonul.getComentarios().trim()+" "+comentariosAnulacion);
									
									controladorGasto.anularGasto(JFramePanambiMain.session.getConn(), gastonul);
									DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa.", DlgMessage.INFORMATION_MESSAGE);
									doConsultarGastos();
									getJCmbConcepto().requestFocus();
								}
							}
						}
					}
				}
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbConcepto().requestFocus();
				}
			});
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
		if (tablePanambi == null) {
			//tablePanambi = new JTablePanambi((String[]) null);
			String[] columnNames = {"Codigo","Concepto", "Fecha de pago", "Importe","Comprobante","Vencimiento","Estado","Sucursal","Data"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(2, Integer.class);
			types.put(3, Integer.class);
			types.put(4, Double.class);
			types.put(5, Double.class);
			Integer[] editable = { };
			tablePanambi= new JTablePanambi(columnNames,editable,types);
						
			tablePanambi.setRowHeight(20);
			tablePanambi.getColumnModel().getColumn(0).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(0).setMaxWidth(0);
			
			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(100);
			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(60);
			tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(40);
			tablePanambi.getColumnModel().getColumn(7).setPreferredWidth(60);
			
			tablePanambi.getColumnModel().getColumn(8).setMinWidth(0);
			tablePanambi.getColumnModel().getColumn(8).setPreferredWidth(0);
			tablePanambi.getColumnModel().getColumn(8).setMaxWidth(0);
//			tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(2);
//			tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(250);
//			tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(10);
//			tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(60);
//			tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(60);
			
			tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JComboBoxPanambi getJCmbConcepto() {
		if (jCmbConcepto == null) {
			jCmbConcepto = new JComboBoxPanambi();
		}
		return jCmbConcepto;
	}
	private JLabelPanambi getLblpnmbConcepto() {
		if (lblpnmbConcepto == null) {
			lblpnmbConcepto = new JLabelPanambi();
			lblpnmbConcepto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbConcepto.setText("Concepto : ");
		}
		return lblpnmbConcepto;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbPesioneFPara() {
		if (lblpnmbPesioneFPara == null) {
			lblpnmbPesioneFPara = new JLabelPanambi();
			lblpnmbPesioneFPara.setForeground(Color.GRAY);
			lblpnmbPesioneFPara.setText("Pesione F1 para Ayuda");
		}
		return lblpnmbPesioneFPara;
	}
	private JXDatePicker getJDPFechaDesde() {
		if (jDPFechaDesde == null) {
			jDPFechaDesde = new JXDatePicker();
			jDPFechaDesde.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaDesde.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaDesde.setToolTipText("Fecha de inicio");
			jDPFechaDesde.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaDesde.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaDesde;
	}
	private JXDatePicker getJDPFechaHasta() {
		if (jDPFechaHasta == null) {
			jDPFechaHasta = new JXDatePicker();
			jDPFechaHasta.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDPFechaHasta.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDPFechaHasta.setToolTipText("Fecha tope");
			jDPFechaHasta.setFormats(new String[] {"dd/MM/yyyy"});
			jDPFechaHasta.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDPFechaHasta;
	}
	private JLabelPanambi getLblpnmbFechasDesde() {
		if (lblpnmbFechasDesde == null) {
			lblpnmbFechasDesde = new JLabelPanambi();
			lblpnmbFechasDesde.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechasDesde.setText("Fechas desde : ");
		}
		return lblpnmbFechasDesde;
	}
	private JLabelPanambi getLblpnmbFechaHasta() {
		if (lblpnmbFechaHasta == null) {
			lblpnmbFechaHasta = new JLabelPanambi();
			lblpnmbFechaHasta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaHasta.setText("Fecha hasta : ");
		}
		return lblpnmbFechaHasta;
	}
	
	
	private JButtonPanambi getJBtnConsultar() {
		if (jBtnConsultar == null) {
			jBtnConsultar = new JButtonPanambi();
			jBtnConsultar.setMnemonic('C');
			jBtnConsultar.setToolTipText("Consultar gastos");
			jBtnConsultar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doConsultarGastos();
				}
			});
			jBtnConsultar.setText("Consultar");
		}
		return jBtnConsultar;
	}
	
	@SuppressWarnings("deprecation")
	private void doConsultarGastos(){
		try{
			TipoGasto tipoGasto;
			String concepto ;
			String estado ;
			Date fechaDesde ;
			Date fechaHasta ;
			Sucursal sucursal;

			if(getJDPFechaDesde().getDate()!=null){
				fechaDesde =new Date(getJDPFechaDesde().getDate().getTime());
			}else{
				fechaDesde = new Date(0,0,0);
			}
			
			if(getJDPFechaHasta().getDate()!=null){
				fechaHasta =new Date(getJDPFechaHasta().getDate().getTime());
			}else{
				fechaHasta = new Date(2000, 1, 1);
			}
			
			fechaHasta = sumarFechasDias(fechaHasta, 1);
			
			concepto = (String)getJCmbConcepto().getSelectedItem(); 
			
			if(!concepto.equals("TODOS")){
				tipoGasto = controladorTipoGasto.getTipoGasto(JFramePanambiMain.session.getConn(), concepto);
			}else tipoGasto = null;
			
			estado = (String)getJCmbEstado().getSelectedItem();
				
			if(estado.equals("TODOS")){
				estado = null;
			}else if(estado.equals("ACTIVOS")){
				estado = "A";
			}else{
				estado = "I";
			}
			String suc = (String)getJCmbSucursal().getSelectedItem();
			
			if(!suc.equals("TODAS")){
				sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),suc);
			}else sucursal = null;
			
			
			boolean fechasCorrectas = true;
			
			if(fechaDesde!= null && fechaHasta!=null){
				if(fechaDesde.getTime() > fechaHasta.getTime()){
					fechasCorrectas = false;
				}
				
			}
			
			if(!fechasCorrectas){
				DlgMessage.showMessage(getOwner(), "Error en el ingreso de fechas\nFecha de comienzo mayor a fecha tope \nFavor modifique las fechas.", DlgMessage.ERROR_MESSAGE );
			}else{
				List<Gasto> listaGastos = controladorGasto.getGastos(JFramePanambiMain.session.getConn(), tipoGasto, estado, fechaDesde, fechaHasta, sucursal);
				if(listaGastos.size()==0){
					DlgMessage.showMessage(getOwner(), "Ning�n gasto encontrado.", DlgMessage.INFORMATION_MESSAGE);
				}
				Iterator<Gasto> iteratorGasto= listaGastos.listIterator();
				getTablePanambi().resetData(0);
				while (iteratorGasto.hasNext()) {
					
					getTablePanambi().addRow();
					
					Gasto gas  = (Gasto) iteratorGasto.next();
					//"Codigo","Concepto", "Fecha de pago", "Importe","Comprobante","Vencimiento","Estado","Data"};
					getTablePanambi().setValueAt(gas.getCodGasto(), getTablePanambi().getRowCount()-1, 0);
					getTablePanambi().setValueAt(gas.getTipoGasto().getConcepto(), getTablePanambi().getRowCount()-1, 1);
					
					String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);
					
					//getJTxtFechaAnulacion().setText(formato.format(gasto.getFechaAnulacion()));
					
					getTablePanambi().setValueAt(formato.format(gas.getFechaPago()), getTablePanambi().getRowCount()-1, 2);
					getTablePanambi().setValueAt(gas.getImporte(), getTablePanambi().getRowCount()-1, 3);
					getTablePanambi().setValueAt(gas.getNroComprobante(), getTablePanambi().getRowCount()-1, 4);
					if(gas.getFechaVencimiento()!=null){
						getTablePanambi().setValueAt(formato.format(gas.getFechaVencimiento()), getTablePanambi().getRowCount()-1, 5);
					}
					if(gas.getEstado().equals("A")){
						getTablePanambi().setValueAt("Activo", getTablePanambi().getRowCount()-1, 6);
					}else{
						getTablePanambi().setValueAt("Anulado", getTablePanambi().getRowCount()-1, 6);
					}
					getTablePanambi().setValueAt(gas.getSucursal().getNombre(), getTablePanambi().getRowCount()-1, 7);
					getTablePanambi().setValueAt(gas, getTablePanambi().getRowCount()-1, 8);
				}	
			}
			
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
		
	 private static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
	        Calendar cal = new GregorianCalendar();
	        cal.setTimeInMillis(fch.getTime());
	        cal.add(Calendar.DATE, dias);
	        return new java.sql.Date(cal.getTimeInMillis());
	   }
	
		
		
	
	private JComboBoxPanambi getJCmbEstado() {
		if (jCmbEstado == null) {
			jCmbEstado = new JComboBoxPanambi();
		}
		return jCmbEstado;
		
	}
	private JComboBoxPanambi getJCmbSucursal() {
		if (jCmbSucursal == null) {
			jCmbSucursal = new JComboBoxPanambi();
		}
		return jCmbSucursal;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
}
