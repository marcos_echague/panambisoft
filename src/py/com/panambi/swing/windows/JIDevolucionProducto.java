package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.text.MaskFormatter;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import py.com.panambi.bean.DetalleDevolucion;
import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.informes.JRDataSourceNotaCredito;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.utils.PanambiUtils;


public class JIDevolucionProducto extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5264254496284968813L;
	private JPanel cabecera;
	private JLabelPanambi lblpnmbNroFactura;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbFechaDeVenta;
	private JPanel jPnlCliente;
	private JLabelPanambi lblpnmbCliente;
	private JTextFieldInteger jTxtCodCliente;
	private JTextFieldUpper jTxtNombreApellido;
	private JPanel jPnlSucursal;
	private JLabelPanambi jLblSucursal;
	private JTextFieldInteger jTxtCodSucursal;
	private JTextFieldUpper jTxtNombreSucursal;
	private JScrollPane scrollPane;
	private JTablePanambi jTblDetalleVenta;
	private JPanel jPanelDetalles;
	private JButtonPanambi jBtnGuardarDevolucion;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JTextField jTxtFechaVenta;
	private Venta venta;
	private ControladorVenta controladorVenta = new ControladorVenta();
	private JLabelPanambi lblpnmbSubtotal;
	private JTextFieldDouble jTxtSubTotal;
	private JLabelPanambi lblpnmbIva;
	private JTextFieldDouble jTxtIva;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldDouble jTxtTotal;
	private JLabelPanambi lblpnmbDevolucion;
	private JTextFieldDouble jTxtDevolucion;
	private JButtonPanambi jBtnLimpiar;
	private JButtonPanambi jBtnSalir;
	private Devolucion devolucion;
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	
	public JIDevolucionProducto() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setBounds(100, 100, 773, 505);
		setTitle("Devolucion de productos");
		getJPanelSouth().add(getJBtnLimpiar());
		getJPanelSouth().add(getJBtnGuardarDevolucion());
		getJPanelSouth().add(getJBtnSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(600)
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addComponent(getCabecera(), GroupLayout.PREFERRED_SIZE, 719, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 719, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addComponent(getJPanelDetalles(), GroupLayout.PREFERRED_SIZE, 724, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(getCabecera(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getJPanelDetalles(), GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("registrarDevolucion");
		doLimpiar();
		
	}
	
	private JPanel getCabecera() {
		if (cabecera == null) {
			cabecera = new JPanel();
			cabecera.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_cabecera = new GroupLayout(cabecera);
			gl_cabecera.setHorizontalGroup(
				gl_cabecera.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(8)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
								.addGap(2)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
								.addGap(280)
								.addComponent(getLblpnmbFechaDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(2)
								.addComponent(getJTxtFechaVenta(), GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addComponent(getJPnlCliente(), GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE)
								.addGap(48)
								.addComponent(getJPnlSucursal(), GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE))))
			);
			gl_cabecera.setVerticalGroup(
				gl_cabecera.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_cabecera.createSequentialGroup()
						.addGap(9)
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFechaDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(1)
								.addComponent(getJTxtFechaVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_cabecera.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(10)
								.addComponent(getJPnlCliente(), GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE))
							.addGroup(gl_cabecera.createSequentialGroup()
								.addGap(9)
								.addComponent(getJPnlSucursal(), GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)))
						.addContainerGap())
			);
			cabecera.setLayout(gl_cabecera);
			doLimpiar();
			setShortcuts(this);
		}
		return cabecera;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setToolTipText("Numero de Factura");
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
		}
		return jTxtNroFactura;
	}
	
	private void lostFocusNroFactura(){
		
		if(getJTxtNroFactura().getValor()==null){
			getJTblDetalleVenta().setFocusable(false);
		}else{
			try {
				
				Venta ven = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				if (ven != null) {
					setValues(ven, null);
				} else {
					DlgMessage.showMessage(getOwner(), "No existe venta con factura nro "+getJTxtNroFactura().getValor(), DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().requestFocus();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	private JLabelPanambi getLblpnmbFechaDeVenta() {
		if (lblpnmbFechaDeVenta == null) {
			lblpnmbFechaDeVenta = new JLabelPanambi();
			lblpnmbFechaDeVenta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaDeVenta.setText("Fecha de venta: ");
		}
		return lblpnmbFechaDeVenta;
	}
	private JPanel getJPnlCliente() {
		if (jPnlCliente == null) {
			jPnlCliente = new JPanel();
			jPnlCliente.setToolTipText("Informacion del cliente");
			jPnlCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPnlCliente.setLayout(null);
			jPnlCliente.add(getJTxtCodCliente());
			jPnlCliente.add(getJTxtNombreApellido());
			jPnlCliente.add(getLblpnmbCliente());
		}
		return jPnlCliente;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setBounds(0, 0, 321, 20);
			lblpnmbCliente.setText("Cliente");
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.CENTER);
			
			lblpnmbCliente.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return lblpnmbCliente;
	}
	private JTextFieldInteger getJTxtCodCliente() {
		if (jTxtCodCliente == null) {
			jTxtCodCliente = new JTextFieldInteger();
			jTxtCodCliente.setBounds(12, 25, 74, 21);
			jTxtCodCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodCliente.setToolTipText("Codigo del cliente");
			jTxtCodCliente.setEditable(false);
			jTxtCodCliente.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodCliente.setFocusable(false);
		}
		return jTxtCodCliente;
	}
	private JTextFieldUpper getJTxtNombreApellido() {
		if (jTxtNombreApellido == null) {
			jTxtNombreApellido = new JTextFieldUpper();
			jTxtNombreApellido.setBounds(95, 25, 218, 21);
			jTxtNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreApellido.setToolTipText("Nombre del cliente");
			jTxtNombreApellido.setEditable(false);
			jTxtNombreApellido.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreApellido.setFocusable(false);
		}
		return jTxtNombreApellido;
	}
	private JPanel getJPnlSucursal() {
		if (jPnlSucursal == null) {
			jPnlSucursal = new JPanel();
			jPnlSucursal.setToolTipText("Informacion de sucursal");
			jPnlSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			jPnlSucursal.setLayout(null);
			jPnlSucursal.add(getJLblSucursal());
			jPnlSucursal.add(getJTxtCodSucursal());
			jPnlSucursal.add(getJTxtNombreSucursal());
		}
		return jPnlSucursal;
	}
	private JLabelPanambi getJLblSucursal() {
		if (jLblSucursal == null) {
			jLblSucursal = new JLabelPanambi();
			jLblSucursal.setBounds(0, 0, 321, 20);
			jLblSucursal.setText("Sucursal");
			jLblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jLblSucursal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		}
		return jLblSucursal;
	}
	private JTextFieldInteger getJTxtCodSucursal() {
		if (jTxtCodSucursal == null) {
			jTxtCodSucursal = new JTextFieldInteger();
			jTxtCodSucursal.setBounds(12, 25, 74, 21);
			jTxtCodSucursal.setToolTipText("Codigo de sucursal");
			jTxtCodSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodSucursal.setEditable(false);
			jTxtCodSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtCodSucursal.setFocusable(false);
		}
		return jTxtCodSucursal;
	}
	private JTextFieldUpper getJTxtNombreSucursal() {
		if (jTxtNombreSucursal == null) {
			jTxtNombreSucursal = new JTextFieldUpper();
			jTxtNombreSucursal.setBounds(95, 25, 218, 21);
			jTxtNombreSucursal.setToolTipText("Nombre de la sucursal");
			jTxtNombreSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNombreSucursal.setEditable(false);
			jTxtNombreSucursal.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			jTxtNombreSucursal.setFocusable(false);
		}
		return jTxtNombreSucursal;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblDetalleVenta());
		}
		return scrollPane;
	}

	private JTablePanambi getJTblDetalleVenta() {
		if (jTblDetalleVenta == null) {
			String[] columnNames = { "Item", "Producto", "Cantidad", "Devolucion","Precio unitario","Total"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(2, Integer.class);
			types.put(3, Integer.class);
			types.put(4, Double.class);
			types.put(5, Double.class);
			Integer[] editable = { 3 };
			jTblDetalleVenta = new JTablePanambi(columnNames,editable,types);
			
			anadeListenerAlModelo(jTblDetalleVenta);
			
			jTblDetalleVenta.setRowHeight(20);
			jTblDetalleVenta.getColumnModel().getColumn(0).setPreferredWidth(2);
			jTblDetalleVenta.getColumnModel().getColumn(1).setPreferredWidth(250);
			jTblDetalleVenta.getColumnModel().getColumn(2).setPreferredWidth(10);
			jTblDetalleVenta.getColumnModel().getColumn(3).setPreferredWidth(10);
			jTblDetalleVenta.getColumnModel().getColumn(4).setPreferredWidth(60);
			jTblDetalleVenta.getColumnModel().getColumn(5).setPreferredWidth(60);
			jTblDetalleVenta.setEditingColumn(4);
			jTblDetalleVenta.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTblDetalleVenta.setCellSelectionEnabled(true);
			// configura el campo de texto
			  JFormattedTextField formatoCantidadDevuelta= null;
			  try {
			   // s�lo admite 4 n�meros
			   formatoCantidadDevuelta = new JFormattedTextField(new MaskFormatter("####"));
			  }catch (Exception e) {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			  TableCellEditor editor =  new DefaultCellEditor(formatoCantidadDevuelta);
			  jTblDetalleVenta.getColumnModel().getColumn(4).setCellEditor(editor);
		}
		return jTblDetalleVenta;
	}
	
	/**
     * Se a�ade el listener al model
     */
    private void anadeListenerAlModelo(JTablePanambi tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent evento) {
            	doCheckDevolucion();
            }
            
        });
    }
    
    private void doCheckDevolucion(){
    	Integer rowSelect = getJTblDetalleVenta().getSelectedRow();
    	if(rowSelect!=-1){
    		Integer cantidadFacturada =(Integer)getJTblDetalleVenta().getValueAt(rowSelect, 2);
    		Integer cantidadDevuelta = (Integer)getJTblDetalleVenta().getValueAt(rowSelect, 3);
    		
    		if (cantidadDevuelta ==null){
    			getJTblDetalleVenta().setValueAt(0, rowSelect, 3);
    			cantidadDevuelta = 0;
    		}
    		
	    	if(cantidadDevuelta>cantidadFacturada){
	    		DlgMessage.showMessage(getOwner(), "Cantidad mayor a la facturaci�n\nFavor modifique la cantidad a devolver", DlgMessage.WARNING_MESSAGE);
	    		getJTblDetalleVenta().setValueAt(0, rowSelect, 3);
	    	}else{
	    		Double montoDevuelto = 0.0;
	        	for(int i = 0;i<getJTblDetalleVenta().getRowCount();i++){
	        		montoDevuelto+=(Double)getJTblDetalleVenta().getValueAt(i, 4)*(Integer)getJTblDetalleVenta().getValueAt(i, 3);
	        	}
	        	getJTxtDevolucion().setValue(montoDevuelto);
	        }
    	}
    }
	
	private JPanel getJPanelDetalles() {
		if (jPanelDetalles == null) {
			jPanelDetalles = new JPanel();
			jPanelDetalles.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_jPanelDetalles = new GroupLayout(jPanelDetalles);
			gl_jPanelDetalles.setHorizontalGroup(
				gl_jPanelDetalles.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPanelDetalles.createSequentialGroup()
						.addGap(8)
						.addComponent(getLblpnmbSubtotal(), GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
						.addGap(1)
						.addComponent(getJTxtSubTotal(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
						.addGap(15)
						.addComponent(getLblpnmbIva(), GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addGap(1)
						.addComponent(getJTxtIva(), GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
						.addGap(20)
						.addGroup(gl_jPanelDetalles.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jPanelDetalles.createSequentialGroup()
								.addGap(34)
								.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addComponent(getLblpnmbDevolucion(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
						.addGap(3)
						.addComponent(getJTxtDevolucion(), GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
			);
			gl_jPanelDetalles.setVerticalGroup(
				gl_jPanelDetalles.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jPanelDetalles.createSequentialGroup()
						.addGap(9)
						.addGroup(gl_jPanelDetalles.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbSubtotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtSubTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbIva(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtIva(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtDevolucion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
			);
			jPanelDetalles.setLayout(gl_jPanelDetalles);
		}
		return jPanelDetalles;
	}
	private JButtonPanambi getJBtnGuardarDevolucion() {
		if (jBtnGuardarDevolucion == null) {
			jBtnGuardarDevolucion = new JButtonPanambi();
			jBtnGuardarDevolucion.setMnemonic('G');
			jBtnGuardarDevolucion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					if(getJTblDetalleVenta().getRowCount()==0){
						DlgMessage.showMessage(getOwner(), "No se registraron devoluciones\nDebe ingresar un n�mero de factura para registrar devoluciones", DlgMessage.WARNING_MESSAGE);
						getJTxtNroFactura().requestFocus();
					}else if((Double)getJTxtDevolucion().getValue()==0.0){
						DlgMessage.showMessage(getOwner(), "No se regitraron devoluciones\nDebe ingresar cantidades a devolver", DlgMessage.WARNING_MESSAGE);
						getJTblDetalleVenta().requestFocus();
						getJTblDetalleVenta().changeSelection(0, 3, false, false);
					}else{
						doGuardarDevolucion();
					}
					
				}
			});
			jBtnGuardarDevolucion.setToolTipText("Guardar cambios");
			jBtnGuardarDevolucion.setText("Guardar Devolucion");
		}
		return jBtnGuardarDevolucion;
	}
	
	private void doGuardarDevolucion(){
		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Est� seguro que quiere guardar esta devoluci�n?\n"
				+ "Sucursal : "+JFramePanambiMain.session.getSucursalOperativa().getNombre()+" \n"
						+ "Devoluci�n : Gs. "+getJTxtDevolucion().getValue(), "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if (ret == JOptionPane.YES_OPTION) {
			
			try{
				//*** Cargando devolucion
				devolucion = new Devolucion();
				devolucion.setTotal((Double)getJTxtDevolucion().getValue());
				devolucion.setVenta(venta);
				devolucion.setUsuario(JFramePanambiMain.session.getUsuario());
				devolucion.setSucursal(JFramePanambiMain.session.getSucursalOperativa());
				devolucion.setCliente(venta.getCliente());
				
				List<DetalleDevolucion> detalle = new ArrayList<DetalleDevolucion>();
				Integer item = 1;
				for(int i = 0;i<getJTblDetalleVenta().getRowCount();i++){
					DetalleDevolucion detalleDevolucion;
					Integer cantidadDevuelta;
					
					if(getJTblDetalleVenta().getValueAt(i, 3)==null){
						cantidadDevuelta = 0;
					}else{
						cantidadDevuelta = (Integer) getJTblDetalleVenta().getValueAt(i, 3);
					}
				
					if(cantidadDevuelta!=0){
						detalleDevolucion = new DetalleDevolucion();
						detalleDevolucion.setNroItem(item);
						detalleDevolucion.setCantidad(cantidadDevuelta);
						detalleDevolucion.setMontoUnitario((Double)getJTblDetalleVenta().getValueAt(i, 4));
						detalleDevolucion.setTotalItem((Double)getJTblDetalleVenta().getValueAt(i, 4)*cantidadDevuelta);
						detalleDevolucion.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), (String)getJTblDetalleVenta().getValueAt(i, 1)));
						item+=1;
						detalle.add(detalleDevolucion);
					}
				}
				
				devolucion.setDetalleDevolucion(detalle);
				
				if(devolucion.getVenta().getEstado().equals("A")){
					
					if(!controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), devolucion)){
						String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la devoluci�n", "");
						if(observaciones!=null){
							if(observaciones.equals("")){
								DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario", DlgMessage.WARNING_MESSAGE);
								
							}else{
								devolucion.setObservaciones("REGISTRADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" . "+observaciones);
								
								Integer codDevolucionGenerado = controladorDevolucion.guardarDevolucion(JFramePanambiMain.session.getConn(), devolucion);
								DlgMessage.showMessage(getOwner(), "Devoluci�n registrada exitosamente\nSe ha generado una nota de cr�dito para la devolucion", DlgMessage.INFORMATION_MESSAGE);
								doLimpiar();
								getJTxtNroFactura().requestFocus();
								doGenerarNotaCredito(controladorDevolucion.getDevolucion(JFramePanambiMain.session.getConn(), codDevolucionGenerado));
							}
						}
						
						
						
					}else{
						DlgMessage.showMessage(getOwner(), "Existe registrada una devoluci�n Activa para la factura n�mero "+devolucion.getVenta().getNroComprobante()+".\n"
								+ "Debe anular la devoluci�n con dicho n�mero de factura para poder registrar una nueva.", DlgMessage.WARNING_MESSAGE);
					}
				}else{
					DlgMessage.showMessage(getOwner(), "La venta con factura nro "+devolucion.getVenta().getNroComprobante()+" se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
				}
				
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad.\nYa se ha registrado una devoluci�n para esta facturacion.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}	
			}catch(Exception e){
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	private void doGenerarNotaCredito(Devolucion d) {
		try {
			PanambiUtils.setWaitCursor(getOwner());
			Map<String, Object> parameters = new HashMap<String, Object>();
			javax.swing.ImageIcon icon = new ImageIcon(JIDevolucionProducto.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
			Image image = icon.getImage();
			parameters.put("LOGO", image);
			parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
			
			parameters.put("NRONOTACREDITO", d.getNotaCredito());
			parameters.put("CLIENTE", d.getCliente().getNombres()+" "+d.getCliente().getApellidos());
			parameters.put("TOTALCREDITO",d.getTotal());
			
			
			URL url = JIDevolucionProducto.class.getResource("/py/com/panambi/informes/reports/JRNotaCredito.jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
			
			JasperPrint jasperPrint;
			
			
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceNotaCredito(JFramePanambiMain.session.getConn(),d.getCodDevolucion()));
			
			List<?> pages = jasperPrint.getPages();
			if (!pages.isEmpty()) {
				JasperViewer.viewReport(jasperPrint, false);
				
			} else {
				DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}

	}
	
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JTextField getJTxtFechaVenta() {
		if (jTxtFechaVenta == null) {
			jTxtFechaVenta = new JTextField();
			jTxtFechaVenta.setToolTipText("Fecha de venta");
			jTxtFechaVenta.setEditable(false);
			jTxtFechaVenta.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaVenta.setColumns(10);
			jTxtFechaVenta.setFocusable(false);
		}
		return jTxtFechaVenta;
	}
	
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Venta) {
			try{	
				this.venta = (Venta) obj;
				if(venta.getEstado().equals("I")){
					DlgMessage.showMessage(getOwner(), "Factura n�mero "+venta.getNroComprobante()+" anulada.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().setValor(null);
					getJTxtNroFactura().requestFocus();
					devolucion = null;
					venta = null;
				}else if(controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), venta)){
					DlgMessage.showMessage(getOwner(), "Existe una devolucion Activa para la factura n�mero "+venta.getNroComprobante()+".\nDebe anular la devoluci�n para registrar una nueva con el mismo n�mero de factura.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().setValor(null);
					getJTxtNroFactura().requestFocus();
					devolucion = null;
					doLimpiar();
					getJTxtNroFactura().requestFocus();
					venta = null;
				}else{
					getJTblDetalleVenta().setFocusable(true);
					String patron = "dd/MM/yyyy";
				    SimpleDateFormat formato = new SimpleDateFormat(patron);
					getJTxtFechaVenta().setText(formato.format(venta.getFecha()));
					
					getJTxtCodCliente().setValor(venta.getCliente().getCodCliente());
					getJTxtNombreApellido().setText(venta.getCliente().getNombres().toString()+" "+venta.getCliente().getApellidos().toString());
					getJTxtCodSucursal().setValor(venta.getSucursal().getCodSucursal());
					getJTxtNombreSucursal().setText(venta.getSucursal().getNombre());
					
					getJTxtSubTotal().setValue(venta.getTotal()-venta.getIva());
					getJTxtIva().setValue(venta.getIva());
					getJTxtTotal().setValue(venta.getTotal());

					List<DetalleVenta> detallesVentas = new ArrayList<DetalleVenta>();
					detallesVentas  = controladorVenta.getDetalleVenta(JFramePanambiMain.session.getConn(), venta);
					Iterator<DetalleVenta> iteratorDetalleVenta = detallesVentas.listIterator();
					getJTblDetalleVenta().resetData(0);
					while (iteratorDetalleVenta.hasNext()) {
						getJTblDetalleVenta().addRow();
						DetalleVenta dv = (DetalleVenta) iteratorDetalleVenta.next();
						
						getJTblDetalleVenta().setValueAt(dv.getNroitem(), getJTblDetalleVenta().getRowCount()-1, 0);
						getJTblDetalleVenta().setValueAt(dv.getProducto().getDescripcion(), getJTblDetalleVenta().getRowCount()-1, 1);
						getJTblDetalleVenta().setValueAt(dv.getCantidad(), getJTblDetalleVenta().getRowCount()-1, 2);
						getJTblDetalleVenta().setValueAt(0, getJTblDetalleVenta().getRowCount()-1, 3);
						getJTblDetalleVenta().setValueAt(dv.getPrecioUnitario(), getJTblDetalleVenta().getRowCount()-1, 4);
						getJTblDetalleVenta().setValueAt(dv.getTotalItem(), getJTblDetalleVenta().getRowCount()-1, 5);
					}
					getJTxtDevolucion().setValue(0.0);
					getJTblDetalleVenta().changeSelection(0, 3, false, false);
				}
				
			}catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		
		}
	}
	
	public void setNoValues(Object obj, Integer source) {

	}
	
	private JLabelPanambi getLblpnmbSubtotal() {
		if (lblpnmbSubtotal == null) {
			lblpnmbSubtotal = new JLabelPanambi();
			lblpnmbSubtotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSubtotal.setText("Subtotal : ");
		}
		return lblpnmbSubtotal;
	}
	private JTextFieldDouble getJTxtSubTotal() {
		if (jTxtSubTotal == null) {
			jTxtSubTotal = new JTextFieldDouble();
			jTxtSubTotal.setEditable(false);
			jTxtSubTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSubTotal.setFocusable(false);
		}
		return jTxtSubTotal;
	}
	private JLabelPanambi getLblpnmbIva() {
		if (lblpnmbIva == null) {
			lblpnmbIva = new JLabelPanambi();
			lblpnmbIva.setText("IVA : ");
			lblpnmbIva.setHorizontalAlignment(SwingConstants.RIGHT);
			
		}
		return lblpnmbIva;
	}
	private JTextFieldDouble getJTxtIva() {
		if (jTxtIva == null) {
			jTxtIva = new JTextFieldDouble();
			jTxtIva.setEditable(false);
			jTxtIva.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtIva.setFocusable(false);
			
		}
		return jTxtIva;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setText("Total : ");
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbTotal;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setEditable(false);
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setFocusable(false);
		}
		return jTxtTotal;
	}
	private JLabelPanambi getLblpnmbDevolucion() {
		if (lblpnmbDevolucion == null) {
			lblpnmbDevolucion = new JLabelPanambi();
			lblpnmbDevolucion.setText("Devolucion : ");
			lblpnmbDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbDevolucion;
	}
	private JTextFieldDouble getJTxtDevolucion() {
		if (jTxtDevolucion == null) {
			jTxtDevolucion = new JTextFieldDouble();
			jTxtDevolucion.setForeground(Color.RED);
			jTxtDevolucion.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtDevolucion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtDevolucion.setEditable(false);
			jTxtDevolucion.setFocusable(false);
		}
		return jTxtDevolucion;
	}
	
	
	private JButtonPanambi getJBtnLimpiar() {
		if (jBtnLimpiar == null) {
			jBtnLimpiar = new JButtonPanambi();
			jBtnLimpiar.setMnemonic('L');
			jBtnLimpiar.setToolTipText("Limpiar pantalla");
			jBtnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
					getJTxtNroFactura().requestFocus();
				}
			});
			jBtnLimpiar.setText("Limpiar");
		}
		return jBtnLimpiar;
	}
	
	private void doLimpiar(){
		getJTxtNroFactura().setValor(null);
		getJTxtFechaVenta().setText("");
		getJTxtCodCliente().setValor(null);
		getJTxtNombreApellido().setText("");
		getJTxtCodSucursal().setValor(null);
		getJTxtNombreSucursal().setText("");
		
		getJTxtSubTotal().setValue(null);
		getJTxtIva().setValue(null);
		getJTxtTotal().setValue(null);
		getJTxtDevolucion().setValue(0.0);
		getJTblDetalleVenta().setFocusable(false);
		if(getJTblDetalleVenta().getRowCount()!=0){
			getJTblDetalleVenta().resetData(0);
		}
		
		
	}
	

	private JButtonPanambi getJBtnSalir() {
		if (jBtnSalir == null) {
			jBtnSalir = new JButtonPanambi();
			jBtnSalir.setMnemonic('S');
			jBtnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			jBtnSalir.setToolTipText("Cerrar ventana");
			jBtnSalir.setText("Salir");
		}
		return jBtnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
}


