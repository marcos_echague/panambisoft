package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.DetalleMovimientoProducto;
import py.com.panambi.bean.MovimientoProducto;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.controller.ControladorMovimientoProducto;
import py.com.panambi.controller.ControladorProducto;
import py.com.panambi.controller.ControladorSucursal;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.informes.JIGenerarCodigoBarra;
import py.com.panambi.informes.JRDataSourceNotaRemision;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JBrowser;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.prompts.PromptMovimientos;
import py.com.panambi.swing.prompts.PromptProductos;
import py.com.panambi.utils.PanambiUtils;

public class JIMovimientoProducto extends JInternalFramePanambi implements Browseable {
	private static final long serialVersionUID = 879009476947910158L;
	private JButtonPanambi btnpnmbLimpiar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbSalir;
	private MovimientoProducto movimientoProducto;
	private ControladorProducto controladorProducto = new ControladorProducto();
	private ControladorSucursal controladorSucursal = new ControladorSucursal();
	private ControladorMovimientoProducto controladorMovimientoProducto = new ControladorMovimientoProducto();
	private List<String> sucursales = new ArrayList<String>();
	private List<String> sucursalesDeDestino = new ArrayList<String>();
	private JButtonPanambi btnpnmbBuscarProducto;
	private JLabel lblSucursalDeDestino;
	private JComboBoxPanambi JCmbSucursalDestino;
	private JLabel lblCodigoDeMovimiento;
	private JTextFieldInteger jTxtCodMovimiento;
	private JLabel lblComentarios;
	private JLabel lblFecha;
	private JTextFieldUpper jTxtFecha;
	private JComboBoxPanambi JCmbSucursalOrigen;
	private JScrollPane scrollTablePanel;
	private JTablePanambi table;
	private JLabel lblSucursalOrigen;
	private JTextAreaUpper JtxtAreaComentarios;
	private JLabel lblAnulado;
	private JTextFieldUpper JtxtEstado;
	private JButtonPanambi btnpnmbQuitarProducto;
	private JScrollPane scrollPane;
	private JButtonPanambi btnpnmbAceptar;
	private JButtonPanambi btnpnmbRechazar;
	private JLabelPanambi lblpnmbPresioneFPara;

	/**
	 * Create the frame.
	 */
	public JIMovimientoProducto() throws Exception {
		super();
		initialize();
	}

	private void initialize() {
		setMaximizable(false);
		setResizable(true);
		setTitle("Movimiento de producto");
		setBounds(100, 100, 840, 469);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbAceptar());
		getJPanelSouth().add(getBtnpnmbRechazar());
		getJPanelSouth().add(getBtnpnmbSalir());
		getJPanelCentral().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getScrollTablePanel(), getTable(), getLblSucursalOrigen(), getJCmbSucursalOrigen(), getLblFecha(), getLblComentarios(), getLblSucursalDeDestino(), getBtnpnmbBuscarProducto(), getJtxtAreaComentarios(), getJCmbSucursalDestino(), getJTxtFecha(), getLblCodigoDeMovimiento(), getJTxtCodMovimiento()}));
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblCodigoDeMovimiento())
							.addGap(44)
							.addComponent(getJTxtCodMovimiento(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addGap(150)
							.addComponent(getLblFecha())
							.addGap(4)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
							.addGap(14)
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
						.addComponent(getScrollTablePanel(), GroupLayout.PREFERRED_SIZE, 762, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblAnulado())
									.addComponent(getBtnpnmbBuscarProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(17)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(getJtxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGroup(groupLayout.createSequentialGroup()
										.addGap(1)
										.addComponent(getBtnpnmbQuitarProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addGap(122)
								.addComponent(getLblComentarios())
								.addGap(45)
								.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE))
							.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addComponent(getLblSucursalOrigen(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
								.addGap(44)
								.addComponent(getJCmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
								.addGap(68)
								.addComponent(getLblSucursalDeDestino())
								.addGap(24)
								.addComponent(getJCmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(19)
							.addComponent(getLblCodigoDeMovimiento()))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(getJTxtCodMovimiento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(19)
							.addComponent(getLblFecha()))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblSucursalOrigen()))
						.addComponent(getJCmbSucursalOrigen(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(getLblSucursalDeDestino()))
						.addComponent(getJCmbSucursalDestino(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(3)
									.addComponent(getLblAnulado())
									.addGap(22)
									.addComponent(getBtnpnmbBuscarProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJtxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(19)
									.addComponent(getBtnpnmbQuitarProducto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(9)
									.addComponent(getLblComentarios()))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)))
					.addGap(11)
					.addComponent(getScrollTablePanel(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
		);
		getJPanelCentral().setLayout(groupLayout);
		decorate();
		setHelp("movimientoDeProductos");
		doLimpiar();
		setShortcuts(this);
	}

	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setToolTipText("Prepara la pantalla para un nuevo registro");
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}

	@SuppressWarnings("unchecked")
	private void doLimpiar() {
		try {
			poblarLista();
			getJTxtCodMovimiento().setValor(null);
			
			getJCmbSucursalOrigen().setModel(new ListComboBoxModel<String>(sucursales));
			Sucursal sucursalOrigen = JFramePanambiMain.session.getSucursalOperativa();
			getJCmbSucursalOrigen().setSelectedItem(sucursalOrigen.getNombre());
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJCmbSucursalOrigen().setEnabled(true);
			}else{
				getJCmbSucursalOrigen().setEnabled(false);
			}
			
			
			
			getJtxtAreaComentarios().setText(null);
			
			DateFormat dateformat = new SimpleDateFormat("dd/MM/yyy");
			Date currentDate = new Date();
			getJTxtFecha().setText((dateformat.format(currentDate)).toString());
			
			getTable().resetData(0);
			
			getLblAnulado().setVisible(false);
			getJtxtEstado().setVisible(false);
			getJtxtAreaComentarios().setEnabled(true);
			getJCmbSucursalDestino().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(false);
			getBtnpnmbRechazar().setEnabled(false);
			getBtnpnmbAceptar().setEnabled(false);
			getTable().setEnabled(true);
			getBtnpnmbQuitarProducto().setEnabled(false);
			
			getBtnpnmbBuscarProducto().setEnabled(true);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	private void poblarLista() throws Exception {
		sucursales = new ArrayList<String>();
		List<Sucursal> listasucurs = controladorSucursal.getSucursales(JFramePanambiMain.session.getConn());
		for (Sucursal suc : listasucurs) {
			sucursales.add(suc.getNombre());
		}
	}
	
	private void poblarListaDestino(String nombreSucursalOrigen) throws Exception {
		sucursalesDeDestino = new ArrayList<String>();
		List<Sucursal> listasucurs = controladorSucursal.getSucursalesDestino(JFramePanambiMain.session.getConn(), nombreSucursalOrigen);
		for (Sucursal suc : listasucurs) {
			sucursalesDeDestino.add(suc.getNombre());
		}
	}

	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setToolTipText("Busca registros");
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}

	private void doBuscar() {
		try {
			getBtnpnmbBuscarProducto().setEnabled(false);
			//getJtxtCodProducto().setEnabled(false);
			/*String[] columnNames = { "C�digo", "Fecha","Estado","Sucursal de origen","Sucursal de destino", "Comentarios" };
			String[] pks = { "codMovimientoProducto" };
			String sSelect = "SELECT codmovimientoproducto, fecha, estado, codsucursal_origen, codsucursal_destino, comentarios ";
			sSelect += "FROM movimientosproductos ";
			sSelect += "ORDER BY fecha";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.MovimientoProducto", pks);
			jb.setVisible(true);*/
			
			doPrompMovimiento();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setToolTipText("Graba el registro actual");
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}

	private void doGrabar() {
		try {	
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la creacion del movimiento de productos ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				
				String nombreSucursalOrigen = null;
				if(JCmbSucursalOrigen.getSelectedItem() != null){
					nombreSucursalOrigen = JCmbSucursalOrigen.getSelectedItem().toString();
				}
				Sucursal sucursalOrigen = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursalOrigen);
				controladorSucursal.checkSeleccion(sucursalOrigen);
				int codSucursalOrigen= sucursalOrigen.getCodSucursal();
				
				String nombreSucursalDestino = null;
				if(JCmbSucursalDestino.getSelectedItem() != null){
					nombreSucursalDestino = JCmbSucursalDestino.getSelectedItem().toString();
				}
				Sucursal sucursalDestino = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursalDestino);
				controladorSucursal.checkSeleccion(sucursalDestino);
				int codSucursalDestino= sucursalDestino.getCodSucursal();
				
				String fecha = getJTxtFecha().getText(); 
				DateFormat dateformat = new SimpleDateFormat("dd/MM/yyy");

				Date fechaFormateada = dateformat.parse(fecha);
				//fechaFormateada = dateformat.format(fechaFormateada);
				//String dateString = dateformat.format(fechaFormateada);
				java.sql.Date sqlDate = new java.sql.Date(fechaFormateada.getTime());

				String comentarios = getJtxtAreaComentarios().getText();
				//chequear las cantidades
				boolean error = false;
				for (int i = 0; i < getTable().getRowCount(); i++) {
					Integer cantidadDisponible = (Integer) getTable().getValueAt(i, 4);
					Integer cantidadSolicitada = (Integer) getTable().getValueAt(i, 5);
					error = doValidarCantidades(cantidadDisponible,cantidadSolicitada);
					if (error){
						i = getTable().getRowCount()+1;
					}
				}
				
				if(!error){
					//guarda el movimiento creado
					MovimientoProducto movimientoProducto = new MovimientoProducto();
					movimientoProducto.setCodSucursalDestino(codSucursalDestino);
					movimientoProducto.setCodSucursalOrigen(codSucursalOrigen);
					movimientoProducto.setComentarios(comentarios);
					movimientoProducto.setFecha(sqlDate);
					movimientoProducto.setEstado("ET");
					List<DetalleMovimientoProducto> detalles = new ArrayList<DetalleMovimientoProducto>();
					for (int i = 0; i < getTable().getRowCount(); i++) {
						
						DetalleMovimientoProducto detalle = new DetalleMovimientoProducto();
					
						detalle.setCodProducto((Integer) getTable().getValueAt(i, 1));
						detalle.setCantidad((Integer) getTable().getValueAt(i, 5));
						detalles.add(detalle);
					}
					Integer nroNotaRemision = controladorMovimientoProducto.getNextNumberNotaRemision(JFramePanambiMain.session.getConn());
					movimientoProducto.setNotaRemision(nroNotaRemision);
					movimientoProducto.setDetalleMovimiento(detalles);
					controladorMovimientoProducto.insertarMovimientoProducto(JFramePanambiMain.session.getConn(), movimientoProducto);
					movimientoProducto = controladorMovimientoProducto.getMovimientoProductoPorNotaRemision(JFramePanambiMain.session.getConn(), nroNotaRemision);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa. Se generar� la nota de remisi�n.", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbSucursalOrigen().requestFocus();
					generarNotaRemision(movimientoProducto);
					
				}
			}
				
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void generarNotaRemision(MovimientoProducto movimiento){
		
		try {		
				PanambiUtils.setWaitCursor(getOwner());
				Map<String, Object> parameters = new HashMap<String, Object>();
				javax.swing.ImageIcon icon = new ImageIcon(JIGenerarCodigoBarra.class.getResource("/py/com/panambi/images/logoPanambiSoft.jpg"));
				Image image = icon.getImage();
				
				parameters.put("LOGO", image);
				parameters.put("USUARIO", JFramePanambiMain.session.getUsuario().getUsuario());
				parameters.put("FECHA", movimiento.getFecha());
				parameters.put("REMISIONNRO", movimiento.getNotaRemision());
				parameters.put("DESTINATARIO", "PANAMBI MODAS CASUALES SUCURSAL FERNANDO DE LA MORA");
				parameters.put("RUC", "3398983-1");
				parameters.put("PARTIDA", new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),movimiento.getCodSucursalOrigen() ).getDireccion());
				parameters.put("LLEGADA", new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),movimiento.getCodSucursalDestino()).getDireccion());
				parameters.put("MOTIVO", "TRANSLADO ENTRE LOCALES DE LA MISMA EMPRESA");
				
				
				URL url = JIGenerarCodigoBarra.class.getResource("/py/com/panambi/informes/reports/JRNotaRemision.jasper");
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(url);
	//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JFramePanambiMain.session.getConn());
				
				JasperPrint jasperPrint;
				
				for(int i = 0;i<3;i++){
					if(i==0){
						parameters.put("PIE", "ORIGINAL : CLIENTE");
					}else if (i==1){
						parameters.put("PIE", "Duplicado : Remitente");
					}else{
						parameters.put("PIE", "Triplicado : Archivo tributario");
					}
					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRDataSourceNotaRemision(JFramePanambiMain.session.getConn(), movimiento));
					
					List<?> pages = jasperPrint.getPages();
					if (!pages.isEmpty()) {
//						JasperViewer.viewReport(jasperPrint, false);
//						JasperViewer.viewReport(jasperPrint, false);
//						JasperViewer.viewReport(jasperPrint, false);
						
						JRPdfExporter exporter = new JRPdfExporter();
						File filetmp = new File("tmp//panambifile_" + System.nanoTime() + ".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_FILE, filetmp);
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporter.exportReport();
						if (Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(filetmp);
						}
						
						
		//				String pdffilename = System.getProperty("java.io.tmpdir") + "\\tmp_panambi_" + System.nanoTime() + ".pdf";
		//				JasperExportManager.exportReportToPdfFile(jasperPrint, pdffilename);
		//				Desktop desk = Desktop.getDesktop();
		//				desk.open(new File(pdffilename));
					} else {
						DlgMessage.showMessage(getOwner(), "No se ha generado ning�n registro.", DlgMessage.INFORMATION_MESSAGE);
					}
				}
				
				
				
				
			
		} catch (Exception e) {
			DlgMessage.showMessage(null, e, DlgMessage.ERROR_MESSAGE);
		} finally {
			PanambiUtils.setDefaultCursor(getOwner());
		}
}
	
	private boolean doValidarCantidades(int cantidadDisponible, int cantidadSolicitada) {
		boolean error = false;
		try {
			if(cantidadSolicitada==0){
				DlgMessage.showMessage(getOwner(), "La cantidad solicitada no puede ser cero", DlgMessage.INFORMATION_MESSAGE);
				error = true;
				if(getTable().getRowCount()>0){
					for(int i = 0; i<getTable().getRowCount();i++){
						if((Integer)getTable().getValueAt(i, 5)==0){
							getTable().changeSelection(i, 5, false, false);
							i=getTable().getRowCount() + 1;
						}
					}
				}
				
			}else{
				if (cantidadSolicitada > cantidadDisponible){
					DlgMessage.showMessage(getOwner(), "La cantidad solicitada no puede ser mayor a la del stock", DlgMessage.INFORMATION_MESSAGE);
					error = true;
				}
				
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		return error;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setToolTipText("Cierra la pantalla");
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}

	private void doSalir() {
		this.dispose();
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private void doRehacer() {
		try {

			getTable().resetData(0);
			setValues(movimientoProducto, null);
			movimientoProducto = null;
			
			poblarLista();
			getJTxtCodMovimiento().setValor(null);
			getJCmbSucursalOrigen().setModel(new ListComboBoxModel<String>(sucursales));
			getJCmbSucursalOrigen().setSelectedItem(null);
			getJtxtAreaComentarios().setText(null);
			
			DateFormat dateformat = new SimpleDateFormat("dd/MM/yyy");
			Date currentDate = new Date();
			getJTxtFecha().setText((dateformat.format(currentDate)).toString());
			
			getLblAnulado().setVisible(false);
			getJtxtEstado().setVisible(false);
			getJCmbSucursalOrigen().setEnabled(true);
			getJtxtAreaComentarios().setEnabled(true);
			getJCmbSucursalDestino().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(true);
			getBtnpnmbRechazar().setEnabled(false);
			getBtnpnmbAceptar().setEnabled(false);
			getBtnpnmbBuscarProducto().setEnabled(true);
			getBtnpnmbQuitarProducto().setEnabled(true);
			getTable().setEnabled(true);
			
			
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private void decorate() {
	}
	
	private JButtonPanambi getBtnpnmbBuscarProducto() {
		if (btnpnmbBuscarProducto == null) {
			btnpnmbBuscarProducto = new JButtonPanambi();
			btnpnmbBuscarProducto.setEnabled(false);
			btnpnmbBuscarProducto.setFont(new Font("Dialog", Font.BOLD, 11));
			btnpnmbBuscarProducto.setToolTipText("Buscar productos");
			btnpnmbBuscarProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarProducto();
				}
			});
			btnpnmbBuscarProducto.setText("Agregar Producto");
		}
		return btnpnmbBuscarProducto;
	}
	
	private void doAgregarProducto() {
		try {
			String nombreSucursalOrigen = null;
			if(JCmbSucursalOrigen.getSelectedItem() != null){
				nombreSucursalOrigen = JCmbSucursalOrigen.getSelectedItem().toString();
			}
			Sucursal sucursalOrigen = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), nombreSucursalOrigen);
			controladorSucursal.checkSeleccion(sucursalOrigen);
			int codSucursalOrigen= sucursalOrigen.getCodSucursal();
			
			String[] columnNames = { "C�digo", "Descripci�n", "Sucursal","Cantidad" };
			String[] pks = { "codproducto_productos"};
			String sSelect = "SELECT sp.codproducto_productos, p.descripcion , s.nombre ,sp.cantidad ";
			sSelect += "FROM stockproductos sp JOIN productos p ";
			sSelect += "ON sp.codproducto_productos = p.codproducto ";
			sSelect += "JOIN sucursales s ";
			sSelect += "ON sp.codsucursal_sucursales = s.codsucursal ";
			sSelect += "WHERE codsucursal_sucursales = '"+codSucursalOrigen+"' ";       
			sSelect += "AND sp.cantidad !='0'";
			sSelect += "ORDER BY sp.codproducto_productos";
			
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Producto", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JLabel getLblSucursalDeDestino() {
		if (lblSucursalDeDestino == null) {
			lblSucursalDeDestino = new JLabel("Sucursal destino");
			lblSucursalDeDestino.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblSucursalDeDestino;
	}
	private JComboBoxPanambi getJCmbSucursalDestino() {
		if (JCmbSucursalDestino == null) {
			JCmbSucursalDestino = new JComboBoxPanambi();
			JCmbSucursalDestino.setMaximumSize(new Dimension(40, 21));
			JCmbSucursalDestino.setMinimumSize(new Dimension(30, 21));
		}
		return JCmbSucursalDestino;
	}
	
	private JLabel getLblSucursalOrigen() {
		if (lblSucursalOrigen == null) {
			lblSucursalOrigen = new JLabel("Sucursal Origen");
			lblSucursalOrigen.setLabelFor(getJCmbSucursalOrigen());
			lblSucursalOrigen.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblSucursalOrigen;
	}
	
	private JComboBoxPanambi getJCmbSucursalOrigen() {
		if (JCmbSucursalOrigen == null) {
			JCmbSucursalOrigen = new JComboBoxPanambi();
			
			JCmbSucursalOrigen.addActionListener(new ActionListener() {
				//@Override
				public void actionPerformed(ActionEvent e) {
					if(JCmbSucursalOrigen.getSelectedItem()!= ""){
						getBtnpnmbBuscarProducto().setEnabled(true);
					}
				}
				
			});
			
			JCmbSucursalOrigen.setMaximumSize(new Dimension(40, 21));
			JCmbSucursalOrigen.setMinimumSize(new Dimension(30, 21));
			JCmbSucursalOrigen.setEnabled(true);
		}
		return JCmbSucursalOrigen;
	}
	
	private JLabel getLblCodigoDeMovimiento() {
		if (lblCodigoDeMovimiento == null) {
			lblCodigoDeMovimiento = new JLabel("Codigo de Movimiento");
			lblCodigoDeMovimiento.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblCodigoDeMovimiento;
	}
	private JTextFieldInteger getJTxtCodMovimiento() {
		if (jTxtCodMovimiento == null) {
			jTxtCodMovimiento = new JTextFieldInteger();
			jTxtCodMovimiento.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodMovimiento.setEnabled(false);
			jTxtCodMovimiento.setEditable(false);
		}
		return jTxtCodMovimiento;
	}
	private JLabel getLblComentarios() {
		if (lblComentarios == null) {
			lblComentarios = new JLabel("Comentarios");
			lblComentarios.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblComentarios;
	}
	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha de movimiento");
			lblFecha.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblFecha;
	}
	private JTextFieldUpper getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JTextFieldUpper();
			jTxtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFecha.setEditable(false);
			jTxtFecha.setFocusable(false);
			
		}
		return jTxtFecha;
	}

	private JScrollPane getScrollTablePanel() {
		if (scrollTablePanel == null) {
			scrollTablePanel = new JScrollPane();
			
			scrollTablePanel.setViewportView(getTable());
		}
		return scrollTablePanel;
	}
		
	private JTablePanambi getTable() {
		if (table == null) {
			String[] columnNames = { "Sel", "Codigo", "Descripcion", "Sucursal","Stock","Cantidad a transladar","Bean"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Boolean.class);
			types.put(1, Integer.class);
			types.put(5, Integer.class);
			Integer[] editable = { 0,5 };
			table = new JTablePanambi(columnNames, editable, types);
			table.setRowHeight(20);
			table.getColumnModel().getColumn(0).setPreferredWidth(5);
			table.getColumnModel().getColumn(1).setPreferredWidth(8);
			table.getColumnModel().getColumn(2).setPreferredWidth(200);
			table.getColumnModel().getColumn(3).setPreferredWidth(40);
			table.getColumnModel().getColumn(4).setPreferredWidth(10);
			table.getColumnModel().getColumn(5).setPreferredWidth(40);
			table.getColumnModel().getColumn(6).setPreferredWidth(0);
			table.getColumnModel().getColumn(6).setMinWidth(0);
			table.getColumnModel().getColumn(6).setMaxWidth(0);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setCellSelectionEnabled(true);
			anadeListenerAlModelo(table);
			
		}
		return table;
	}
	
	private void anadeListenerAlModelo(JTablePanambi tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent evento) {
            	if(evento.getType()==TableModelEvent.UPDATE){
            		doCheckMovimiento();
            	}
            	
            }
            
        });
    }
	
	 private void doCheckMovimiento(){
	    	try{
			 	Integer rowSelect = getTable().getSelectedRow();
		    	if(rowSelect!=-1){
		    		try{
		    			Integer cantidadStock  =(Integer)getTable().getValueAt(rowSelect, 4);
		    			Integer cantidadMovimiento = (Integer)getTable().getValueAt(rowSelect, 5);
			    		
			    		if (cantidadMovimiento ==null){
			    			getTable().setValueAt(0, rowSelect, 5);
			    			cantidadMovimiento = 0;
			    		}
			    		
				    	if(cantidadMovimiento>cantidadStock){
				    		DlgMessage.showMessage(getOwner(), "La cantidad solicitada es mayor a la cantidad en stock.\nFavor modifique.", DlgMessage.ERROR_MESSAGE);
				    		getTable().setValueAt(0, rowSelect, 5);
				    		getTable().changeSelection(rowSelect, 5, false, false);
				    	}
		    			
		    		}catch (Exception e) {
						logger.error(e.getMessage(), e);
						DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
					}
		    		
		    	}
	    	}catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
	    }
	
	@SuppressWarnings("unchecked")
	@Override
	public void setValues(Object obj, Integer source) {
		try {
			if (obj instanceof MovimientoProducto) {
				this.movimientoProducto = (MovimientoProducto) obj;
				getJTxtCodMovimiento().setValor(movimientoProducto.getCodMovimientoProducto());

				DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
				Date currentDate = movimientoProducto.getFecha();
				getJTxtFecha().setText((dateformat.format(currentDate)).toString());
								
				if(movimientoProducto.getEstado().trim().equals("AN")){
					getLblAnulado().setVisible(true);
					getJtxtEstado().setVisible(true);
					getJtxtEstado().setText("Anulado");
					
					//getBtnpnmbEliminar().setEnabled(false);
					getBtnpnmbAceptar().setEnabled(false);
					getBtnpnmbRechazar().setEnabled(false);
					getBtnpnmbGrabar().setEnabled(false);
					
				}else if (movimientoProducto.getEstado().trim().equals("ET")) {
					getLblAnulado().setVisible(true);
					getJtxtEstado().setVisible(true);
					getJtxtEstado().setText("En transito");
					
					//getBtnpnmbEliminar().setEnabled(true);
					getBtnpnmbAceptar().setEnabled(true);
					getBtnpnmbRechazar().setEnabled(true);
					getBtnpnmbGrabar().setEnabled(false);
					
				}else if (movimientoProducto.getEstado().trim().equals("RE")) {
					getLblAnulado().setVisible(true);
					getJtxtEstado().setVisible(true);
					getJtxtEstado().setText("Rechazado");
					
					//getBtnpnmbEliminar().setEnabled(true);
					getBtnpnmbAceptar().setEnabled(false);
					getBtnpnmbRechazar().setEnabled(false);
					getBtnpnmbGrabar().setEnabled(false);
					
				}else if (movimientoProducto.getEstado().trim().equals("AC")) {
					getLblAnulado().setVisible(true);
					getJtxtEstado().setVisible(true);
					getJtxtEstado().setText("Aceptado");
					
					//getBtnpnmbEliminar().setEnabled(true);
					getBtnpnmbAceptar().setEnabled(false);
					getBtnpnmbRechazar().setEnabled(false);
					getBtnpnmbGrabar().setEnabled(false);
				}else{
					getLblAnulado().setVisible(false);
					getJtxtEstado().setVisible(false);
				}
				
				getJtxtAreaComentarios().setText(movimientoProducto.getComentarios());
				int codSucursalOrigen = movimientoProducto.getCodSucursalOrigen();
				Sucursal sucursalOrigen = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), codSucursalOrigen);
				getJCmbSucursalOrigen().setSelectedItem(sucursalOrigen.getNombre());
				getJCmbSucursalOrigen().setEnabled(true);
				
				getJCmbSucursalDestino().setEnabled(true);
				poblarListaDestino(sucursalOrigen.getNombre());
				getJCmbSucursalDestino().setModel(new ListComboBoxModel<String>(sucursalesDeDestino));
				
				int codSucursalDestino = movimientoProducto.getCodSucursalDestino();
				Sucursal sucursalDestino = controladorSucursal.getSucursal(JFramePanambiMain.session.getConn(), codSucursalDestino);
				getJCmbSucursalDestino().setSelectedItem(sucursalDestino.getNombre());
				
				List<DetalleMovimientoProducto> detalles = movimientoProducto.getDetalleMovimiento();
				//Si trae un movimiento creado
				getTable().resetData(0);
				for (DetalleMovimientoProducto detalle : detalles) {
				
					Producto producto = controladorMovimientoProducto.getProducto(JFramePanambiMain.session.getConn(), detalle.getCodProducto());
					int cantidadEnStock  = controladorMovimientoProducto.getCantidadProductoSucursal(JFramePanambiMain.session.getConn(), producto.getCodProducto(), sucursalOrigen.getNombre());
					getTable().addRow();
					getTable().setValueAt(Boolean.FALSE, getTable().getRowCount() - 1, 0);
					getTable().setValueAt(detalle.getCodProducto(), getTable().getRowCount() - 1, 1);
					getTable().setValueAt(producto.getDescripcion(), getTable().getRowCount() - 1, 2);
					getTable().setValueAt(sucursalOrigen.getNombre(), getTable().getRowCount() - 1, 3);
					getTable().setValueAt(cantidadEnStock, getTable().getRowCount() - 1, 4);
					getTable().setValueAt(detalle.getCantidad(), getTable().getRowCount() - 1, 5);
					getTable().setValueAt(producto, getTable().getRowCount() - 1, 6);

				}
	
				getBtnpnmbBuscarProducto().setEnabled(false);
				getJCmbSucursalDestino().setEnabled(false);
				getJCmbSucursalOrigen().setEnabled(false);
				getJtxtAreaComentarios().setEnabled(false);
				getTable().setEnabled(false);
				getBtnpnmbQuitarProducto().setEnabled(false);
			}
			//si el movimiento no ha sido creado
			if (obj instanceof Producto) {
				
				Producto producto = (Producto) obj;
				if (isProductoDuplicado(producto)==true) {
					DlgMessage.showMessage(getOwner(), "El producto seleccionado ya ha sido agregado.", DlgMessage.INFORMATION_MESSAGE);
				}else{
				
					String nombreSucursalOrigen = JCmbSucursalOrigen.getSelectedItem().toString();
					getJCmbSucursalOrigen().setEnabled(false);
					
					getJCmbSucursalDestino().setEnabled(true);
					poblarListaDestino(nombreSucursalOrigen);
					getJCmbSucursalDestino().setModel(new ListComboBoxModel<String>(sucursalesDeDestino));
					int cantidadEnStock  = controladorMovimientoProducto.getCantidadProductoSucursal(JFramePanambiMain.session.getConn(), producto.getCodProducto(), nombreSucursalOrigen);
					getTable().addRow();
					
					getTable().setValueAt(Boolean.FALSE, getTable().getRowCount() - 1, 0);
					getTable().setValueAt(producto.getCodProducto(), getTable().getRowCount() - 1, 1);
					getTable().setValueAt(producto.getDescripcion(), getTable().getRowCount() - 1, 2);
					getTable().setValueAt(nombreSucursalOrigen, getTable().getRowCount() - 1, 3);
					getTable().setValueAt(cantidadEnStock, getTable().getRowCount() - 1, 4);
					getTable().setValueAt(0, getTable().getRowCount() - 1, 5);
					getTable().setValueAt(producto, getTable().getRowCount() - 1, 6);
									
					getBtnpnmbGrabar().setEnabled(true);
					//getBtnpnmbEliminar().setEnabled(false);
					getBtnpnmbRechazar().setEnabled(false);
					getBtnpnmbAceptar().setEnabled(false);
					getTable().setEnabled(true);
					getBtnpnmbQuitarProducto().setEnabled(true);
				}
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	@Override
	public void setNoValues(Object obj, Integer source) {
	}
	
	private boolean isProductoDuplicado(Producto producto) {
		boolean ret = false;
		for (int i = 0; i < getTable().getRowCount(); i++) {
			Producto productoTable = (Producto) getTable().getValueAt(i, 6);
			if (productoTable.getCodProducto().equals(producto.getCodProducto())) {
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	private JTextAreaUpper getJtxtAreaComentarios() {
		if (JtxtAreaComentarios == null) {
			JtxtAreaComentarios = new JTextAreaUpper();
		}
		return JtxtAreaComentarios;
	}
	private JLabel getLblAnulado() {
		if (lblAnulado == null) {
			lblAnulado = new JLabel("Estado");
			lblAnulado.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblAnulado.setVisible(false);
		}
		return lblAnulado;
	}
	private JTextFieldUpper getJtxtEstado() {
		if (JtxtEstado == null) {
			JtxtEstado = new JTextFieldUpper();
			JtxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			JtxtEstado.setVisible(false);
			JtxtEstado.setEnabled(false);
		}
		return JtxtEstado;
	}
	private JButtonPanambi getBtnpnmbQuitarProducto() {
		if (btnpnmbQuitarProducto == null) {
			btnpnmbQuitarProducto = new JButtonPanambi();
			btnpnmbQuitarProducto.setEnabled(false);
			btnpnmbQuitarProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doQuitarProducto();
				}
			});
			btnpnmbQuitarProducto.setText("Quitar Producto");
		}
		return btnpnmbQuitarProducto;
	}
	
	private void doQuitarProducto() {
		
		try {
			ArrayList<Integer> toRemove = new ArrayList<Integer>();
			for (int i = 0; i < getTable().getRowCount(); i++) {
				Boolean selected = (Boolean) getTable().getValueAt(i, 0);
				if (selected) {
					toRemove.add(i);
				}
			}
			
			if (toRemove.isEmpty()) {
				throw new ValidException("Debe seleccionar el producto que desea quitar");
			}
			Integer[] toRemoveInt = new Integer[toRemove.size()];
			toRemove.toArray(toRemoveInt);
			getTable().removeRows(toRemoveInt);
		} catch (Exception e) {
			//logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJtxtAreaComentarios());
		}
		return scrollPane;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.setToolTipText("Acepta el movimiento");
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAceptar();
				}
			});
			
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Desea aceptar el movimiento de producto? ", "Confirmaci�n ", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				controladorMovimientoProducto.aceptarMovimiento(JFramePanambiMain.session.getConn(), movimientoProducto);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbSucursalOrigen().requestFocus();
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbRechazar() {
		if (btnpnmbRechazar == null) {
			btnpnmbRechazar = new JButtonPanambi();
			btnpnmbRechazar.setToolTipText("Rechaza el movimiento");
			btnpnmbRechazar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRechazar();
				}
			});
			
			btnpnmbRechazar.setText("Rechazar");
		}
		return btnpnmbRechazar;
	}
	
	private void doRechazar() {
		try {
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Desea rechazar el movimiento de producto? ", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(ret == JOptionPane.YES_OPTION){
				controladorMovimientoProducto.anularMovimiento(JFramePanambiMain.session.getConn(), movimientoProducto);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbSucursalOrigen().requestFocus();
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doPrompMovimiento() {
		PromptMovimientos prompt = new PromptMovimientos(this);
		prompt.setVisible(true);
	}
	
	protected void setProductoFromCod(Integer codproducto) {
		try {
			if (codproducto == null) {
				throw new ValidException();
			}
			Producto producto = controladorProducto.getProducto(JFramePanambiMain.session.getConn(), codproducto);
			if (producto != null) {
				setValues(producto, null);
			} else {
				setNoValues(new Producto(), null);
			}
		} catch (ValidException e) {
			doPrompProducto();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
	private void doPrompProducto() {
		PromptProductos prompt = new PromptProductos(this);
		prompt.setVisible(true);
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
