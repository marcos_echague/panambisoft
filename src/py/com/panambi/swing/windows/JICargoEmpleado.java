package py.com.panambi.swing.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.CargoEmpleado;
import py.com.panambi.controller.ControladorCargoEmpleado;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;

public class JICargoEmpleado extends JInternalFramePanambi implements Browseable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -717719544675165918L;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbDescripcion;
	private JTextFieldInteger jTxtCodigo;
	private JComboBoxPanambi jCmbDescripcion;
	private CargoEmpleado cargoEmpleado;
	private List<String> cargosEmpleados = new ArrayList<String>();
	private ControladorCargoEmpleado  controladorCargoEmpleado = new ControladorCargoEmpleado();
	private JCheckBox chckbxActivo;
    private JLabelPanambi labelPanambi;
    private JLabel lblPresioneFPara;
	
	
	/**
	 * Create the frame.
	 */
	public JICargoEmpleado() throws Exception{
		super();
		initialize();
		
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Cargos de empleado");
		setBounds(100, 100, 390, 290);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(10, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblPresioneFPara())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(getChckbxActivo()))
								.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)))
					.addGap(72))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(getLblPresioneFPara())
					.addPreferredGap(ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getChckbxActivo()))
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblpnmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(35))
		);
		getJPanelCentral().setLayout(groupLayout);
		
		
		
		doLimpiar();
		getJCmbDescripcion().requestFocus();
		decorate();
		setShortcuts(this);
		setHelp("cargosempleados");
		getJCmbDescripcion().requestFocus();
		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJCmbDescripcion(), getChckbxActivo(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
	}

	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar cargo de empleado");
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
	private void doEliminar(){
		try {
			
			Integer ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la eliminaci�n permanente del cargo ?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if (ret == JOptionPane.YES_OPTION){
				controladorCargoEmpleado.borrarCargoEmpleado(JFramePanambiMain.session.getConn(), cargoEmpleado);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbDescripcion().requestFocus();
			}
			
		}catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Grabar cargo de empleado");
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar(){
		try {
			if (cargoEmpleado == null) {
				cargoEmpleado = new CargoEmpleado ();
				cargoEmpleado.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				
				if(chckbxActivo.isSelected()){	
					cargoEmpleado.setEstado("A");
				} else {
					cargoEmpleado.setEstado("I");
				}
				Integer ret = JOptionPane.showConfirmDialog(this, "Confirma la creaci�n del nuevo cargo de empleado ?","",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.YES_OPTION){
					controladorCargoEmpleado.insertarCargoEmpleado(JFramePanambiMain.session.getConn(), cargoEmpleado );
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
				
			} else {
				cargoEmpleado.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				
				if(chckbxActivo.isSelected()){	
					cargoEmpleado.setEstado("A");
				} else {
					cargoEmpleado.setEstado("I");
				}
				
				Integer ret = JOptionPane.showConfirmDialog(this, "Confirma la modificaci�n del cargo de empleado ?","",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.YES_OPTION){
					controladorCargoEmpleado.modificarCargoEmpleado(JFramePanambiMain.session.getConn(), cargoEmpleado);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
				
			}
			
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buascar cargos de empleados");
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar(){
		try {
			doLimpiar();
			String[] columnNames = { "C�digo", "Cargo de Empleado","Estado"};
			String[] pks = {"codcargoEmpleado" };
			String sSelect = "SELECT codcargoempleado, descripcion, estado ";
			sSelect += "FROM cargosempleados ";
			sSelect += "ORDER BY descripcion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.CargoEmpleado", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbDescripcion() {
		if (jCmbDescripcion == null) {
			jCmbDescripcion= new JComboBoxPanambi();
			jCmbDescripcion.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusDescripcion();
				}
			});
			jCmbDescripcion.setEditable(true);
			jCmbDescripcion.setModel(new ListComboBoxModel<String>(cargosEmpleados));
			jCmbDescripcion.setSelectedItem(null);
		}
		return jCmbDescripcion;
	}
	
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("Codigo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbDescripcion() {
		if (lblpnmbDescripcion == null) {
			lblpnmbDescripcion = new JLabelPanambi();
			lblpnmbDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbDescripcion.setText("Cargo de Empleado : ");
		}
		return lblpnmbDescripcion;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setEnabled(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try {
			cargoEmpleado = null;
			problarLista();
			getJCmbDescripcion().setModel(new ListComboBoxModel<String>(cargosEmpleados));
			getJCmbDescripcion().setSelectedItem(null);
			getJTxtCodigo().setValor(null);
			chckbxActivo.setSelected(true);
			btnpnmbEliminar.setEnabled(false);
			btnpnmbGrabar.setEnabled(false);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	
	
	private void problarLista() throws Exception{
		cargosEmpleados= new ArrayList<String>();
		List<CargoEmpleado> listacargos= controladorCargoEmpleado.getCargosEmpleados(JFramePanambiMain.session.getConn());
		for (CargoEmpleado carg : listacargos) {
			cargosEmpleados.add(carg.getDescripcion());
		}
	}


	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof CargoEmpleado) {
			this.cargoEmpleado = (CargoEmpleado) obj;
			getJTxtCodigo().setValor(cargoEmpleado.getCodCargoEmpleado());
			getJCmbDescripcion().setSelectedItem(((CargoEmpleado) (obj)).getDescripcion());
			if(cargoEmpleado.getEstado().equals("A")){
				chckbxActivo.setSelected(true);
			} else chckbxActivo.setSelected(false);
			// getJCmbNombre().requestFocus();
			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);
		}
		
	}
	
	private void lostFocusDescripcion() {
		try {
			CargoEmpleado carg = controladorCargoEmpleado.getCargoEmpleado(JFramePanambiMain.session.getConn(), (String) jCmbDescripcion.getSelectedItem());
			if (carg != null) {
				setValues(carg, null);
			} else {
				doNuevoFromLostFocus();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		cargoEmpleado = null;
		getJTxtCodigo().setValor(null);
		chckbxActivo.setSelected(true);
		Integer longitud = 0;
		try{
			longitud = getJCmbDescripcion().getSelectedItem().toString().length();
		}catch (Exception e){
			longitud = 0;
		}
		
		if(longitud !=0){
			btnpnmbGrabar.setEnabled(true);
		}else{
			btnpnmbGrabar.setEnabled(false);
		}
		btnpnmbEliminar.setEnabled(false);
	}

	
	@Override
	public void setNoValues(Object obj, Integer source) {
	
		
	}
	

	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbDescripcion());
	}
	private JCheckBox getChckbxActivo() {
		if (chckbxActivo == null) {
			chckbxActivo = new JCheckBox("Activo");
		}
		return chckbxActivo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabel getLblPresioneFPara() {
		if (lblPresioneFPara == null) {
			lblPresioneFPara = new JLabel("Presione F1 para Ayuda");
			lblPresioneFPara.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblPresioneFPara.setForeground(Color.GRAY);
		}
		return lblPresioneFPara;
	}
}
