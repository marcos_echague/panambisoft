package py.com.panambi.swing.windows;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import py.com.panambi.bean.Parametro;
import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.controller.ControladorParametro;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldFloat;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class JIParametros extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 241198604553874817L;
	private JLabelPanambi lblpnmbSueldoMnimo;
	private JLabelPanambi lblpnmbPorcentajeDeInteres;
	private JTextFieldFloat jTxtTasaPrestamo;
	private JTextFieldDouble jTxtSueldoMinimo;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi lblpnmbMontoMnimoPara;
	private JTextFieldDouble jTxtMontoBasePrestamo;
	private JButton jBtnModificarValores;
	private JButton btnLimpiar;
	private JButton btnActualizar;
	private JButton btnSalir;
	private Parametro parametro ;
	private ControladorParametro controladorParametro = new ControladorParametro();
	
	public JIParametros() throws Exception {
		initialize();
	}
	private void initialize() {
		setTitle("Configuraci\u00F3n de par\u00E1metros del sistema");
		setMaximizable(false);
		setBounds(100, 100, 445,388);
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(26, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(getLblpnmbSueldoMnimo(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(getLblpnmbMontoMnimoPara(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(getLblpnmbPorcentajeDeInteres(), GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getJBtnModificarValores())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getJTxtTasaPrestamo(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJTxtSueldoMinimo(), GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtMontoBasePrestamo(), GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(36, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(38)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtSueldoMinimo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbSueldoMnimo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtTasaPrestamo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbPorcentajeDeInteres(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtMontoBasePrestamo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbMontoMnimoPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(getJBtnModificarValores())
					.addContainerGap(35, Short.MAX_VALUE))
		);
		getJPanelCentral().setLayout(groupLayout);
		getJPanelSouth().add(getBtnLimpiar());
		getJPanelSouth().add(getBtnActualizar());
		getJPanelSouth().add(getBtnSalir());
		doLimpiar();
		setShortcuts(this);
		setHelp("registrarParametros");
	}
	
	private void doLimpiar(){
		try{
			parametro = controladorParametro.getDatosParametro(JFramePanambiMain.session.getConn());
			getJTxtSueldoMinimo().setValue(parametro.getSueldoMinimo());
			getJTxtMontoBasePrestamo().setValue(parametro.getMontoBasePrestamo());
			getJTxtTasaPrestamo().setValue(parametro.getTasaPrestamo());
			
			getJTxtSueldoMinimo().setEnabled(false);
			getJTxtMontoBasePrestamo().setEnabled(false);
			getJTxtTasaPrestamo().setEnabled(false);
			
			boolean admin = false;
			List<PerfilUsuario> perfilesUsuario = null;
			if(JFramePanambiMain.session.getUsuario().getPerfilesUsuario()!=null){
				perfilesUsuario = JFramePanambiMain.session.getUsuario().getPerfilesUsuario();
			}
			
			if(perfilesUsuario!=null){
				for(int i = 0;i<perfilesUsuario.size();i++){
					PerfilUsuario perfil = perfilesUsuario.get(i);
					if(perfil.getNombre().toUpperCase().equals("ADMINISTRADOR")){
						admin = true;
					}
				}
			}
			
			if(admin){
				getJBtnModificarValores().setVisible(true);
				getBtnActualizar().setVisible(true);
				getBtnActualizar().setEnabled(false);
			}else{
				getJBtnModificarValores().setVisible(false);
				getBtnActualizar().setVisible(false);
			}
			
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(),e, DlgMessage.ERROR_MESSAGE);
			logger.error(e.getMessage(), e);
		}
		
		
	}
	
	private JLabelPanambi getLblpnmbSueldoMnimo() {
		if (lblpnmbSueldoMnimo == null) {
			lblpnmbSueldoMnimo = new JLabelPanambi();
			lblpnmbSueldoMnimo.setHorizontalAlignment(SwingConstants.LEFT);
			lblpnmbSueldoMnimo.setText("Sueldo m\u00EDnimo : ");
		}
		return lblpnmbSueldoMnimo;
	}
	private JLabelPanambi getLblpnmbPorcentajeDeInteres() {
		if (lblpnmbPorcentajeDeInteres == null) {
			lblpnmbPorcentajeDeInteres = new JLabelPanambi();
			lblpnmbPorcentajeDeInteres.setHorizontalAlignment(SwingConstants.LEFT);
			lblpnmbPorcentajeDeInteres.setText("Porcentaje de inter\u00E9s de ventas a cr\u00E9dito : ");
		}
		return lblpnmbPorcentajeDeInteres;
	}
	private JTextFieldFloat getJTxtTasaPrestamo() {
		if (jTxtTasaPrestamo == null) {
			jTxtTasaPrestamo = new JTextFieldFloat();
			jTxtTasaPrestamo.setForeground(Color.BLACK);
			jTxtTasaPrestamo.setDisabledTextColor(Color.BLACK);
			jTxtTasaPrestamo.setFont(new Font("Dialog", Font.PLAIN, 12));
			jTxtTasaPrestamo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtTasaPrestamo;
	}
	private JTextFieldDouble getJTxtSueldoMinimo() {
		if (jTxtSueldoMinimo == null) {
			jTxtSueldoMinimo = new JTextFieldDouble();
			jTxtSueldoMinimo.setForeground(Color.BLACK);
			jTxtSueldoMinimo.setDisabledTextColor(Color.BLACK);
			jTxtSueldoMinimo.setFont(new Font("Dialog", Font.PLAIN, 12));
			jTxtSueldoMinimo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtSueldoMinimo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("%");
		}
		return labelPanambi;
	}
	private JLabelPanambi getLblpnmbMontoMnimoPara() {
		if (lblpnmbMontoMnimoPara == null) {
			lblpnmbMontoMnimoPara = new JLabelPanambi();
			lblpnmbMontoMnimoPara.setHorizontalAlignment(SwingConstants.LEFT);
			lblpnmbMontoMnimoPara.setText("Monto m\u00EDnimo para ventas a cr\u00E9dito : ");
		}
		return lblpnmbMontoMnimoPara;
	}
	private JTextFieldDouble getJTxtMontoBasePrestamo() {
		if (jTxtMontoBasePrestamo == null) {
			jTxtMontoBasePrestamo = new JTextFieldDouble();
			jTxtMontoBasePrestamo.setForeground(Color.BLACK);
			jTxtMontoBasePrestamo.setDisabledTextColor(Color.BLACK);
			jTxtMontoBasePrestamo.setFont(new Font("Dialog", Font.PLAIN, 12));
			jTxtMontoBasePrestamo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jTxtMontoBasePrestamo;
	}
	private JButton getJBtnModificarValores() {
		if (jBtnModificarValores == null) {
			jBtnModificarValores = new JButton("Habilitar campos");
			jBtnModificarValores.setMnemonic('H');
			jBtnModificarValores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doHabilitarCampos();
				}
			});
			jBtnModificarValores.setToolTipText("Habilitar campos para modificacion");
		}
		return jBtnModificarValores;
	}
	
	private void doHabilitarCampos(){
		getJTxtMontoBasePrestamo().setEnabled(true);
		getJTxtSueldoMinimo().setEnabled(true);
		getJTxtTasaPrestamo().setEnabled(true);
		getBtnActualizar().setEnabled(true);
		getJTxtSueldoMinimo().requestFocus();
	}
	
	private JButton getBtnLimpiar() {
		if (btnLimpiar == null) {
			btnLimpiar = new JButton("Limpiar");
			btnLimpiar.setMnemonic('L');
			btnLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJBtnModificarValores().requestFocus();
				}
			});
			btnLimpiar.setToolTipText("Limpiar ventana");
		}
		return btnLimpiar;
	}
	private JButton getBtnActualizar() {
		if (btnActualizar == null) {
			btnActualizar = new JButton("Guardar cambios");
			btnActualizar.setMnemonic('G');
			btnActualizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doActualizarParametros();
				}
			});
			btnActualizar.setToolTipText("guardar cambios");
		}
		return btnActualizar;
	}
	
	private void doActualizarParametros(){
		try{
			
		boolean iguales = false;
		boolean poseeCeros = false;
		boolean porcentajeCorrecto = true;
		if (parametro.getSueldoMinimo().equals(getJTxtSueldoMinimo().getValue())&& 
			parametro.getMontoBasePrestamo().equals(getJTxtMontoBasePrestamo().getValue())&&
			parametro.getTasaPrestamo().equals(getJTxtTasaPrestamo().getValue()	)){
			iguales = true;
		}
		
		if((Double)getJTxtMontoBasePrestamo().getValue()==0.0||
				(Double)getJTxtSueldoMinimo().getValue()==0.0){
			poseeCeros = true;
		}
		
		if((Float)getJTxtTasaPrestamo().getValue()<0 ||(Float)getJTxtTasaPrestamo().getValue()>99){
			porcentajeCorrecto = false;
		}
		
		if(poseeCeros){
			DlgMessage.showMessage(getOwner(), "Los valores ingresados deben ser diferentes de cero", DlgMessage.ERROR_MESSAGE);
		}else{
			if (!porcentajeCorrecto){
				DlgMessage.showMessage(getOwner(), "El porcentaje de la tasa debe estar entre 0 y 99 % ", DlgMessage.ERROR_MESSAGE);
			}else{
				if(iguales){
					DlgMessage.showMessage(getOwner(), "No se ha modificado ningun campo", DlgMessage.ERROR_MESSAGE);
					doLimpiar();
				}else{
					Integer ret = JOptionPane.showConfirmDialog(this, "Confirma la modificación de los parámetros del sistema?","",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(ret == JOptionPane.YES_OPTION){
						Parametro parametroNuevo = new Parametro();
						parametroNuevo.setCodparametro(1);
						parametroNuevo.setSueldoMinimo((Double)getJTxtSueldoMinimo().getValue());
						parametroNuevo.setTasaPrestamo((Float)getJTxtTasaPrestamo().getValue());
						parametroNuevo.setMontoBasePrestamo((Double)getJTxtMontoBasePrestamo().getValue());
						controladorParametro.actualizarParametros(JFramePanambiMain.session.getConn(), parametroNuevo);
						DlgMessage.showMessage(getOwner(), "Operación exitosa", DlgMessage.INFORMATION_MESSAGE);
						doLimpiar();
						getJBtnModificarValores().requestFocus();
						}
					}
				}
			}
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			logger.error(e.getMessage(), e);
		}
		
	}
	
	private JButton getBtnSalir() {
		if (btnSalir == null) {
			btnSalir = new JButton("Salir");
			btnSalir.setMnemonic('S');
			btnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnSalir.setToolTipText("Cerrar ventana");
		}
		return btnSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
}
