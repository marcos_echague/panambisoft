package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Gasto;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.TipoGasto;
import py.com.panambi.controller.ControladorGasto;
import py.com.panambi.controller.ControladorTipoGasto;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;

public class JIGasto extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8673147929357601106L;
	private JLabelPanambi lblpnmbConcepto;
	private JLabelPanambi lblpnmbImporte;
	private JLabelPanambi lblpnmbNroComprobante;
	private JLabelPanambi lblpnmbFecha;
	private JTextFieldDouble jTxtImporte;
	private JFormattedTextFieldPanambi jTxtNroComprobante;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbLimpiar;
	private JComboBoxPanambi jTxtConcepto;
	private List<String> gastos= new ArrayList<String>();
	private Gasto gasto;
	private ControladorGasto controladorGasto = new ControladorGasto();
	private ControladorTipoGasto controladorTipoGasto = new ControladorTipoGasto();
	private JScrollPane scrollPane;
	private JLabel lblComentarios;
	private JTextArea jTxtComentarios;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi labelPanambi_1;
	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	private JLabelPanambi labelPanambi_2;
	private JXDatePicker jDatePickerFecha;
	private JLabelPanambi lblpnmbPresioneFPara;
	private JButtonPanambi btnpnmbRegistrarPago;
	public JIGasto() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setToolTipText("");
		setTitle("Gastos");
		setBounds(100, 100, 528, 455);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbRegistrarPago());
		getJPanelSouth().add(getBtnpnmbSalir());
		
		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getJTxtConcepto(), getJTxtImporte(), getJTxtNroComprobante(), getJTxtComentarios(), getBtnpnmbLimpiar(), getBtnpnmbBuscar(), getBtnpnmbGrabar(), getBtnpnmbEliminar(), getBtnpnmbSalir()}));
		
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(47)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJDatePickerFecha(), GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addGap(43)
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(47)
							.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtConcepto(), GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(67)
							.addComponent(getLblComentarios(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(47)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbNroComprobante(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJTxtNroComprobante(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
									.addComponent(getLblpnmbImporte(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJTxtImporte(), GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)))
					.addGap(18))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(11)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJDatePickerFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJTxtConcepto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbImporte(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJTxtImporte(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabelPanambi_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addComponent(getLblpnmbNroComprobante(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(getJTxtNroComprobante(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(getLblComentarios()))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))))
		);
		getJPanelCentral().setLayout(groupLayout);
		try {
        	// Carga el fichero de ayuda 
            fichero = new File("./bin/py/com/panambi/help/help_set.hs"); 
        	hsURL = fichero.toURI().toURL();
			helpset = new HelpSet(getClass().getClassLoader(), hsURL);
    	    hb = helpset.createHelpBroker();
    	    // Pone ayuda a item de menu al pulsar F1. mntmIndice es el JMenuitem  
    	    hb.enableHelpKey(getRootPane(),"gastos",helpset);
		} catch (Exception e) {
			//logger.error(getOwner(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		} 
		setShortcuts(this);
		doLimpiar();
		getJDatePickerFecha().requestFocus();
	}
	private JLabelPanambi getLblpnmbConcepto() {
		if (lblpnmbConcepto == null) {
			lblpnmbConcepto = new JLabelPanambi();
			lblpnmbConcepto.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbConcepto.setText("Concepto : ");
		}
		return lblpnmbConcepto;
	}
	private JLabelPanambi getLblpnmbImporte() {
		if (lblpnmbImporte == null) {
			lblpnmbImporte = new JLabelPanambi();
			lblpnmbImporte.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbImporte.setText("Importe : ");
		}
		return lblpnmbImporte;
	}
	private JLabelPanambi getLblpnmbNroComprobante() {
		if (lblpnmbNroComprobante == null) {
			lblpnmbNroComprobante = new JLabelPanambi();
			lblpnmbNroComprobante.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroComprobante.setText("Nro. comprobante : ");
		}
		return lblpnmbNroComprobante;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha vencimiento : ");
		}
		return lblpnmbFecha;
	}
	private JTextFieldDouble getJTxtImporte() {
		if (jTxtImporte == null) {
			jTxtImporte = new JTextFieldDouble();
		}
		return jTxtImporte;
	}
	private JFormattedTextFieldPanambi getJTxtNroComprobante() {
		if (jTxtNroComprobante == null) {
			jTxtNroComprobante = new JFormattedTextFieldPanambi();
		}
		return jTxtNroComprobante;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJDatePickerFecha().requestFocus();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		try{
			
			gasto = null;
			poblarLista();
			getJTxtConcepto().setModel(new ListComboBoxModel<String>(gastos));
			getJTxtConcepto().setSelectedItem(null);
			getJTxtImporte().setValue(null);
			getJDatePickerFecha().setDate(new java.util.Date());
			getJTxtNroComprobante().setText("");
			getJTxtComentarios().setText("");
			getJTxtComentarios().setEditable(true);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
		
	}
	
	private void poblarLista() throws Exception {
		gastos = new ArrayList<String>();
		List<TipoGasto> listagas = controladorTipoGasto.getTiposGastosActivos(JFramePanambiMain.session.getConn());
		for (TipoGasto tgas : listagas) {
			gastos.add(tgas.getConcepto());
		}
	}
	
	
	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Gasto) {
			this.gasto = (Gasto) obj;
			
			getJTxtConcepto().setSelectedItem(/*((Gasto) (obj)).getConcepto()*/((Gasto) (obj)).getTipoGasto().getConcepto());
			getJTxtImporte().setValue(gasto.getImporte());
			getJDatePickerFecha().setDate(gasto.getFechaVencimiento());
			getJTxtNroComprobante().setText(gasto.getNroComprobante());
			getJTxtComentarios().setText(gasto.getComentarios().trim());
			
			
		}
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		
	}
	
	
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJTxtConcepto() {
		if (jTxtConcepto == null) {
			jTxtConcepto = new JComboBoxPanambi();
//			jTxtConcepto.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
//				@Override
//				public void focusLost(FocusEvent e) {
//					lostFocusConcepto();
//				}
//			});
			jTxtConcepto.setModel(new ListComboBoxModel<String>(gastos));
			jTxtConcepto.setSelectedItem(null);
		}
		return jTxtConcepto;
	}
	
//	private void lostFocusConcepto(){
//		try {
//			Gasto gas = controladorGasto.getGasto(JFramePanambiMain.session.getConn(), (String) jTxtConcepto.getSelectedItem());
//			if (gas != null) {
//				setValues(gas, null);
//			} else {
//				doNuevoFromLostFocus();
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
//		}
//	}
	
//	private void doNuevoFromLostFocus() {
//		gasto = null;
//		getJTxtCodGasto().setValor(null);
//		getJTxtImporte().setValue(null);
//		getJTxtFecha().setFecha(new java.util.Date());
//		getJTxtNroComprobante().setText(null);
//		getJTxtComentarios().setText(null);
//		
//		if(getJTxtConcepto().getSelectedItem()==null || getJTxtConcepto().getSelectedItem().toString().length()==0){
//			getBtnpnmbGrabar().setEnabled(false);
//		}else getBtnpnmbGrabar().setEnabled(true);
//		
//		getBtnpnmbEliminar().setEnabled(false);
//	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTxtComentarios());
		}
		return scrollPane;
	}
	private JLabel getLblComentarios() {
		if (lblComentarios == null) {
			lblComentarios = new JLabel("Comentarios : ");
			lblComentarios.setHorizontalAlignment(SwingConstants.RIGHT);
			lblComentarios.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return lblComentarios;
	}
	private JTextArea getJTxtComentarios() {
		if (jTxtComentarios == null) {
			jTxtComentarios = new JTextArea();
			jTxtComentarios.setLocation(122, 0);
			jTxtComentarios.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtComentarios.setLineWrap(true);
			jTxtComentarios.setWrapStyleWord(true);
		}
		return jTxtComentarios;
	}
	
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("*");
		}
		return labelPanambi;
	}
	private JLabelPanambi getLabelPanambi_1() {
		if (labelPanambi_1 == null) {
			labelPanambi_1 = new JLabelPanambi();
			labelPanambi_1.setText("*");
		}
		return labelPanambi_1;
	}
	private JLabelPanambi getLabelPanambi_2() {
		if (labelPanambi_2 == null) {
			labelPanambi_2 = new JLabelPanambi();
			labelPanambi_2.setText("*");
		}
		return labelPanambi_2;
	}
	private JXDatePicker getJDatePickerFecha() {
		if (jDatePickerFecha == null) {
			jDatePickerFecha = new JXDatePicker();
			jDatePickerFecha.getEditor().setFont(new Font("Dialog", Font.PLAIN, 11));
			jDatePickerFecha.getEditor().setHorizontalAlignment(SwingConstants.CENTER);
			jDatePickerFecha.setToolTipText("Fecha de comienzo de auditoria");
			jDatePickerFecha.setFormats(new String[] {"dd/MM/yyyy"});
			jDatePickerFecha.setFont(new Font("Dialog", Font.PLAIN, 11));
		}
		return jDatePickerFecha;
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
	private JButtonPanambi getBtnpnmbRegistrarPago() {
		if (btnpnmbRegistrarPago == null) {
			btnpnmbRegistrarPago = new JButtonPanambi();
			btnpnmbRegistrarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doRegistrarPago();
				}
			});
			btnpnmbRegistrarPago.setMnemonic('R');
			btnpnmbRegistrarPago.setToolTipText("Registrar pago del gasto");
			btnpnmbRegistrarPago.setText("Registrar pago");
		}
		return btnpnmbRegistrarPago;
	}
	
	private void doRegistrarPago(){
			try {
				boolean camposCompletos = true;
				boolean longitudComentario = true;
//				if (gasto == null) {
					gasto = new Gasto();
					try{
						gasto.setTipoGasto(controladorTipoGasto.getTipoGasto(JFramePanambiMain.session.getConn(), (String)getJTxtConcepto().getSelectedItem()));
					}catch(Exception e){
						gasto.setTipoGasto(null);
					}
					
					gasto.setNroComprobante(getJTxtNroComprobante().getText().trim());
					gasto.setImporte((Double)getJTxtImporte().getValue());
					
					if(getJDatePickerFecha().getDate()!=null){
						gasto.setFechaVencimiento(new java.sql.Date(getJDatePickerFecha().getDate().getTime()));
					}
					
					String comentarios = getJTxtComentarios().getText().trim();
					gasto.setComentarios(comentarios);
					
					if(gasto.getFechaVencimiento()==null){
						camposCompletos = false;
					}
					
					if(gasto.getTipoGasto()==null){
						camposCompletos = false;
					}
					if(gasto.getImporte()==null){
						camposCompletos = false;
					}
					if(gasto.getComentarios().length()>200){
						longitudComentario = false;
					}
					
					if(camposCompletos){
						
						if(longitudComentario){
							DlgPagos dlgPagos = new DlgPagos(getOwner(),(Double)getJTxtImporte().getValue(),6);
							dlgPagos.centerIt();
							dlgPagos.setVisible(true);
							if (dlgPagos.getDetallePagos() == null) {
								throw new ValidException("Debe seleccionar el detalle de pago correspondiente.");
							}
							Pago pago = dlgPagos.getPago();
							pago.setGasto(gasto);
							gasto.setSucursal(JFramePanambiMain.session.getSucursalOperativa());
							pago.setSucursal(JFramePanambiMain.session.getSucursalOperativa());
							controladorGasto.insertarGasto(JFramePanambiMain.session.getConn(),gasto, pago);
							DlgMessage.showMessage(getOwner(), "Gasto registrado satisfactoriamente", DlgMessage.INFORMATION_MESSAGE);
							doLimpiar();
							
						}else{
							DlgMessage.showMessage(getOwner(), "Longitud del comentario es muy amplia.\nFavor disminuya la longitud del comentario.", DlgMessage.WARNING_MESSAGE);
						}
					
				} else {
					DlgMessage.showMessage(getOwner(), "Debe completar los campos obligatorios.", DlgMessage.WARNING_MESSAGE);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
}
