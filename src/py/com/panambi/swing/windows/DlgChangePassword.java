package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.connect.EncryptDecrypt;
import py.com.panambi.connect.MD5;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.LoginException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextPassword;

public class DlgChangePassword extends JDialogPanambi implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610850113222236567L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblPassword;
	private JLabel lblPasswordR;
	private JTextPassword jTxtPassword;
	private JButtonPanambi jBtnOk;
	private JButtonPanambi jBtnCancelar;
	private JTextPassword jTxtPasswordR;
	private JLabelPanambi lblpnmbAnterior;
	private JTextPassword jTxtAnterior;
	/**
	 * Create the dialog.
	 */
	public DlgChangePassword() {
		initialize();
	}
		
	private void initialize() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgChangePassword.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Cambio de contrase\u00F1a");
		setBounds(100, 100, 382, 221);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(17)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblPassword())
						.addComponent(getLblPasswordR())
						.addComponent(getLblpnmbAnterior(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
							.addComponent(getJTxtPasswordR(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(getJTxtPassword(), GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJTxtAnterior(), GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(37, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap(15, Short.MAX_VALUE)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtAnterior(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblpnmbAnterior(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(7)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtPassword(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblPassword()))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtPasswordR(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblPasswordR()))
					.addGap(32))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.add(getJBtnOk());
			buttonPane.add(getJBtnCancelar());
		}

		setShortcutsHere(this);
		centerIt();
	}

	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Contrase\u00F1a");
		}
		return lblPassword;
	}

	private JLabel getLblPasswordR() {
		if (lblPasswordR == null) {
			lblPasswordR = new JLabel("Repita contrase\u00F1a");
		}
		return lblPasswordR;
	}

	private JTextPassword getJTxtPassword() {
		if (jTxtPassword == null) {
			jTxtPassword = new JTextPassword();
			jTxtPassword.setActionCommand("OK");
			jTxtPassword.addActionListener(this);
		}
		return jTxtPassword;
	}
	
	private JTextPassword getJTxtPasswordR() {
		if (jTxtPasswordR == null) {
			jTxtPasswordR = new JTextPassword();
			jTxtPasswordR.setActionCommand("OK");
			jTxtPasswordR.addActionListener(this);
		}
		return jTxtPasswordR;
	}
	
	private void setShortcutsHere(Container component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doCancelar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private JButtonPanambi getJBtnOk() {
		if (jBtnOk == null) {
			jBtnOk = new JButtonPanambi();
			jBtnOk.setToolTipText("Aceptar");
			jBtnOk.setMnemonic('A');
			jBtnOk.setText("Aceptar");
			jBtnOk.setActionCommand("OK");
			jBtnOk.addActionListener(this);
		}
		return jBtnOk;
	}

	private JButtonPanambi getJBtnCancelar() {
		if (jBtnCancelar == null) {
			jBtnCancelar = new JButtonPanambi();
			jBtnCancelar.setToolTipText("Cancelar");
			jBtnCancelar.setMnemonic('C');
			jBtnCancelar.setText("Cancelar");
			jBtnCancelar.setActionCommand("CANCEL");
			jBtnCancelar.addActionListener(this);
		}
		return jBtnCancelar;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("OK".equals(cmd)) {
			doAceptar();
		}
		if ("CANCEL".equals(cmd)) {
			doCancelar();
		}
	}

	private void doCancelar() {
		this.dispose();
	}

	private void doAceptar() {
		try {
			
			Usuario usuario = JFramePanambiMain.session.getUsuario();
			
			String passwd = new String(getJTxtAnterior().getPassword());
			MD5 md5 = new MD5();
			md5.init();
			md5.update(passwd.toCharArray(), passwd.toCharArray().length);
			md5.md5final();
			String encpasswd = new String(md5.toHexString());
			logger.debug(encpasswd);
			
			boolean valido = ConnectionManager.validateUser(JFramePanambiMain.session.getConn(), usuario.getUsuario(), encpasswd);
			
			if(valido){
								
				String passwding = new String(getJTxtPassword().getPassword());
				String passwdingR = new String(getJTxtPasswordR().getPassword());
				
				if(passwding.length()<7){
					DlgMessage.showMessage(this,"Por cuestiones de seguridad, la contrase�a debe tener un m�nimo de 7 car�cteres",DlgMessage.WARNING_MESSAGE);
					doLimpiar();
					getJTxtAnterior().requestFocus();
				}else{
					
					if(passwding.equals(passwdingR)==false){
						doLimpiar();
						getJTxtAnterior().requestFocus();
						throw new LoginException("La contrase�a no coincide.");
						
					}
					md5 = new MD5();
					md5.init();
					md5.update(passwding.toCharArray(), passwding.toCharArray().length);
					md5.md5final();
					encpasswd = new String(md5.toHexString());
					logger.debug(encpasswd);					
					ControladorUsuario.setearPassword(JFramePanambiMain.session.getConn(), usuario, encpasswd);
					DlgMessage.showMessage(this,"Contrase�a actualizada correctamente",DlgMessage.WARNING_MESSAGE);
					this.dispose();
				}
			}else{
				DlgMessage.showMessage(this,"La contrase�a actual no es correcta.",DlgMessage.WARNING_MESSAGE);
				doLimpiar();
				getJTxtAnterior().requestFocus();
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}
	}
	private JLabelPanambi getLblpnmbAnterior() {
		if (lblpnmbAnterior == null) {
			lblpnmbAnterior = new JLabelPanambi();
			lblpnmbAnterior.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbAnterior.setText("Actual");
		}
		return lblpnmbAnterior;
	}
	private JTextPassword getJTxtAnterior() {
		if (jTxtAnterior == null) {
			jTxtAnterior = new JTextPassword();
			jTxtAnterior.setActionCommand("OK");
		}
		return jTxtAnterior;
	}
	
	private void doLimpiar(){
		getJTxtAnterior().setText("");
		getJTxtPassword().setText("");
		getJTxtPasswordR().setText("");
	}
}
