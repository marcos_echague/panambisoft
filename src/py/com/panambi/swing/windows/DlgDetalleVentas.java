package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;

public class DlgDetalleVentas extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbNroFactura;
	private JLabelPanambi lblpnmbFecha;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbCliente;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbTipo;
	private JLabelPanambi lblpnmbIva2;
	private JLabelPanambi lblpnmbTotal;
	private JLabelPanambi lblpnmbUsuario;
	private JLabelPanambi lblpnmbFechaAnulacin;
	private JLabelPanambi lblpnmbObservaciones;
	private JTextAreaUpper jTxtObservaciones;
	private JTextFieldInteger jTxtCodigo;
	private JFormattedTextFieldPanambi jTxtNroFactura;
	private JFormattedTextFieldPanambi jTxtCliente;
	private JLabelPanambi lblpnmbTotalLetras;
	private JTextFieldDouble jTxtIva;
	private JTextFieldDouble jTxtTotal;
	private JTextAreaUpper jTxtTotalLetras;
	private JLabelPanambi lblpnmbDetalleDeVenta;
	private ControladorVenta controladorVenta = new ControladorVenta();
	private Venta venta;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JFormattedTextFieldPanambi jTxtFecha;
	private JFormattedTextFieldPanambi jtxtUsuario;
	private JFormattedTextFieldPanambi jTxtTipo;
	private JFormattedTextFieldPanambi jTxtSucursal;
	private JFormattedTextFieldPanambi jTxtEstado;
	private JFormattedTextFieldPanambi jTxtFechaAnulacion;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JButtonPanambi btnDetallesDelPago;
	private JButtonPanambi jBtnDetalleCuota ;
	public DlgDetalleVentas(Venta venta) {
		this.venta = venta;
		initialize();
		
	}
	private void initialize() {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetalleVentas.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 817, 599);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		getBtnpnmbAceptar().requestFocus();
		if(venta.getTipoVenta().equals("D")){
			getBtnDetallesDelPago().setVisible(true);
			getJBtnDetalleCuota().setVisible(false);
		}else{
			getBtnDetallesDelPago().setVisible(false);
			getJBtnDetalleCuota().setVisible(true);
		}
		
	}

	private void cargarValores(){
		try{
			
			getJTxtCodigo().setText(venta.getCodVenta()+"");
			getJTxtNroFactura().setText(venta.getNroComprobante()+"");
			getJTxtCliente().setText(venta.getCliente().getNombres()+" "+venta.getCliente().getApellidos());
			getJTxtIva().setValue(venta.getIva());
			getJTxtTotal().setValue(venta.getTotal());
			getJTxtTotalLetras().setText(venta.getTotalLetras().trim());
			
			String  patronFecha = "dd/MM/yyyy hh:mm:ss";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
			getJTxtFecha().setText(formatoFecha.format(venta.getFecha()));
			getJtxtUsuario().setText(venta.getUsuario().getUsuario());
			String tipoVenta = "";
			if(venta.getTipoVenta().equals("C")){
				tipoVenta = "CREDITO";
			}else{
				tipoVenta = "CONTADO";
			}
			getJTxtTipo().setText(tipoVenta);
			
			getJTxtSucursal().setText(venta.getSucursal().getNombre());
			
			String estado = "";
			if(venta.getEstado().equals("A")){
				estado= "ACTIVO";
			}else{
				estado = "ANULADO";
			}
			getJTxtEstado().setText(estado);
						
			if(venta.getFechaAnulacion()!=null){
				getJTxtFechaAnulacion().setText(formatoFecha.format(venta.getFechaAnulacion()));
			}else{
				getJTxtFechaAnulacion().setVisible(false);
				getLblpnmbFechaAnulacin().setVisible(false);
			}
			
			if(venta.getObservaciones()!=null){
				getJTxtObservaciones().setText(venta.getObservaciones().trim());
			}
			
			List<DetalleVenta> detalleVenta = new ArrayList<DetalleVenta>();
			detalleVenta = controladorVenta.getDetalleVenta(JFramePanambiMain.session.getConn(),venta);
			Iterator<DetalleVenta> iteratorDetalle=  detalleVenta.listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				DetalleVenta det= (DetalleVenta) iteratorDetalle.next();
				//"Nro. Item","Descripcion del producto", "Precio unitario", "Cantidad","Descuento unitario","Total item"};
				getTablePanambi().setValueAt(det.getNroitem(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(det.getProducto().getDescripcion(), getTablePanambi().getRowCount()-1, 1);
					
				getTablePanambi().setValueAt(det.getPrecioUnitario(), getTablePanambi().getRowCount()-1, 2);
				getTablePanambi().setValueAt(det.getCantidad(), getTablePanambi().getRowCount()-1, 3);
				getTablePanambi().setValueAt(det.getDescuentoUnitario(), getTablePanambi().getRowCount()-1, 4);
				getTablePanambi().setValueAt(det.getTotalItem(), getTablePanambi().getRowCount()-1, 5);
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 801, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLES DE VENTA");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 801, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbNroFactura() {
		if (lblpnmbNroFactura == null) {
			lblpnmbNroFactura = new JLabelPanambi();
			lblpnmbNroFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbNroFactura.setText("Nro. Factura : ");
		}
		return lblpnmbNroFactura;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha : ");
		}
		return lblpnmbFecha;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Cliente : ");
		}
		return lblpnmbCliente;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbTipo() {
		if (lblpnmbTipo == null) {
			lblpnmbTipo = new JLabelPanambi();
			lblpnmbTipo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTipo.setText("Tipo : ");
		}
		return lblpnmbTipo;
	}
	private JLabelPanambi getLblpnmbIva2() {
		if (lblpnmbIva2 == null) {
			lblpnmbIva2 = new JLabelPanambi();
			lblpnmbIva2.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbIva2.setText("Iva : ");
		}
		return lblpnmbIva2;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Total : ");
		}
		return lblpnmbTotal;
	}
	private JLabelPanambi getLblpnmbUsuario() {
		if (lblpnmbUsuario == null) {
			lblpnmbUsuario = new JLabelPanambi();
			lblpnmbUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbUsuario.setText("Usuario : ");
		}
		return lblpnmbUsuario;
	}
	private JLabelPanambi getLblpnmbFechaAnulacin() {
		if (lblpnmbFechaAnulacin == null) {
			lblpnmbFechaAnulacin = new JLabelPanambi();
			lblpnmbFechaAnulacin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacin.setText("Fecha anulaci\u00F3n : ");
		}
		return lblpnmbFechaAnulacin;
	}
	private JLabelPanambi getLblpnmbObservaciones() {
		if (lblpnmbObservaciones == null) {
			lblpnmbObservaciones = new JLabelPanambi();
			lblpnmbObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbObservaciones.setText("Observaciones : ");
		}
		return lblpnmbObservaciones;
	}
	private JTextAreaUpper getJTxtObservaciones() {
		if (jTxtObservaciones == null) {
			jTxtObservaciones = new JTextAreaUpper();
			jTxtObservaciones.setFocusable(false);
			jTxtObservaciones.setEditable(false);
			jTxtObservaciones.setFont(new Font("Dialog", Font.PLAIN, 10));
		}
		return jTxtObservaciones;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JFormattedTextFieldPanambi getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JFormattedTextFieldPanambi();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.setFocusable(false);
			jTxtNroFactura.setEditable(false);
		}
		return jTxtNroFactura;
	}
	private JFormattedTextFieldPanambi getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JFormattedTextFieldPanambi();
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setFocusable(false);
			jTxtCliente.setEditable(false);
		}
		return jTxtCliente;
	}
	private JLabelPanambi getLblpnmbTotalLetras() {
		if (lblpnmbTotalLetras == null) {
			lblpnmbTotalLetras = new JLabelPanambi();
			lblpnmbTotalLetras.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotalLetras.setText("Total letras : ");
		}
		return lblpnmbTotalLetras;
	}
	private JTextFieldDouble getJTxtIva() {
		if (jTxtIva == null) {
			jTxtIva = new JTextFieldDouble();
			jTxtIva.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtIva.setFocusable(false);
			jTxtIva.setEditable(false);
		}
		return jTxtIva;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setFocusable(false);
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JTextAreaUpper getJTxtTotalLetras() {
		if (jTxtTotalLetras == null) {
			jTxtTotalLetras = new JTextAreaUpper();
			jTxtTotalLetras.setFocusable(false);
			jTxtTotalLetras.setEditable(false);
			jTxtTotalLetras.setFont(new Font("Dialog", Font.PLAIN, 10));
		}
		return jTxtTotalLetras;
	}
	private JLabelPanambi getLblpnmbDetalleDeVenta() {
		if (lblpnmbDetalleDeVenta == null) {
			lblpnmbDetalleDeVenta = new JLabelPanambi();
			lblpnmbDetalleDeVenta.setText("Detalle de venta");
		}
		return lblpnmbDetalleDeVenta;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. Item","Descripcion del producto", "Precio unitario", "Cantidad","Descuento unitario","Total item"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(2, Double.class);
				types.put(3, Integer.class);
				types.put(4, Double.class);
				types.put(5, Double.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
//				tablePanambi.getColumnModel().getColumn(0).setMinWidth(0);
//				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(0);
//				tablePanambi.getColumnModel().getColumn(0).setMaxWidth(0);
//				
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(300);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(50);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(70);
				

				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JFormattedTextFieldPanambi getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JFormattedTextFieldPanambi();
			jTxtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFecha.setFocusable(false);
			jTxtFecha.setEditable(false);
		}
		return jTxtFecha;
	}
	private JFormattedTextFieldPanambi getJtxtUsuario() {
		if (jtxtUsuario == null) {
			jtxtUsuario = new JFormattedTextFieldPanambi();
			jtxtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			jtxtUsuario.setFocusable(false);
			jtxtUsuario.setEditable(false);
		}
		return jtxtUsuario;
	}
	private JFormattedTextFieldPanambi getJTxtTipo() {
		if (jTxtTipo == null) {
			jTxtTipo = new JFormattedTextFieldPanambi();
			jTxtTipo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTipo.setFocusable(false);
			jTxtTipo.setEditable(false);
		}
		return jTxtTipo;
	}
	private JFormattedTextFieldPanambi getJTxtSucursal() {
		if (jTxtSucursal == null) {
			jTxtSucursal = new JFormattedTextFieldPanambi();
			jTxtSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSucursal.setFocusable(false);
			jTxtSucursal.setEditable(false);
		}
		return jTxtSucursal;
	}
	private JFormattedTextFieldPanambi getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JFormattedTextFieldPanambi();
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}
	private JFormattedTextFieldPanambi getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JFormattedTextFieldPanambi();
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
		}
		return jTxtFechaAnulacion;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 801, 483);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 726, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(22)
									.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
									.addGap(127)
									.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(22)
									.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
									.addGap(127)
									.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJtxtUsuario(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(22)
									.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)
									.addGap(55)
									.addComponent(getLblpnmbTipo(), GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJTxtTipo(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(getJBtnDetalleCuota(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(22)
									.addComponent(getLblpnmbIva2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(getJTxtIva(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
									.addGap(113)
									.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(22)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addGap(6)
											.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
											.addGap(113)
											.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_panel_1.createSequentialGroup()
											.addComponent(getLblpnmbTotalLetras(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addGap(5)
											.addComponent(getJTxtTotalLetras(), GroupLayout.PREFERRED_SIZE, 205, Short.MAX_VALUE)
											.addGap(18)
											.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
												.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
									.addGap(10)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
										.addComponent(getJTxtObservaciones(), GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)))))
						.addGap(29))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap(54, Short.MAX_VALUE)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJtxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbTipo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(getJTxtTipo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJBtnDetalleCuota(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbIva2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtIva(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(10)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addGap(1)
										.addComponent(getJTxtTotalLetras(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addGap(1)
											.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
												.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addGroup(gl_panel_1.createSequentialGroup()
											.addGap(31)
											.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
												.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(getJTxtObservaciones(), GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))))))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getLblpnmbTotalLetras(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
						.addGap(53))
			);
			panel_1.setLayout(gl_panel_1);
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 523, 801, 38);
			panel_2.add(getBtnpnmbAceptar());
			panel_2.add(getBtnDetallesDelPago());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	private JButtonPanambi getBtnDetallesDelPago() {
		if (btnDetallesDelPago == null) {
			btnDetallesDelPago = new JButtonPanambi();
			btnDetallesDelPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doVerDetallePago();
				}
			});
			btnDetallesDelPago.setToolTipText("Ver detalles del pago relacionado a la venta");
			btnDetallesDelPago.setText("Detalles del pago");
		}
		return btnDetallesDelPago;
	}
	
	private void doVerDetallePago(){
		try{
			Pago pago = controladorVenta.getPagoDeVenta(JFramePanambiMain.session.getConn(), venta);
			if(pago == null ){
				DlgMessage.showMessage(getOwner(),"No existe pago asociado a la venta.", DlgMessage.WARNING_MESSAGE);
			}else{
				
				DlgDetallePagos detallePago= new DlgDetallePagos(pago);
				detallePago.centerIt();
				detallePago.setVisible(true);
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}
	
		
		
		private JButtonPanambi getJBtnDetalleCuota() {
		if (jBtnDetalleCuota == null) {
			jBtnDetalleCuota = new JButtonPanambi();
			jBtnDetalleCuota.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doDetalleCuotas();
				}
			});
			jBtnDetalleCuota.setText("Detalle de cuotas");
			jBtnDetalleCuota.setToolTipText("");
		}
		return jBtnDetalleCuota;
	
	}
		
	private void doDetalleCuotas(){
		
		DlgDetalleCuotas detalleCuotas= new DlgDetalleCuotas(venta);
		detalleCuotas.centerIt();
		detalleCuotas.setVisible(true);
	}
}
