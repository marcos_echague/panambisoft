package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Banco;
import py.com.panambi.bean.Bonificacion;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.FormaPago;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.TipoDescuentoSalario;
import py.com.panambi.controller.ControladorBanco;
import py.com.panambi.controller.ControladorBonificacion;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorFormaPago;
import py.com.panambi.controller.ControladorTipoDescuentoSalario;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldFloat;

import javax.swing.JButton;
import java.awt.Dimension;

public class DlgAplicar extends JDialogPanambi {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2398408365286756579L;
	private JPanel panelCentral;
	private JLabelPanambi lblpnmbAPagar;
	private JPanel panel;
	private JComboBoxPanambi jCmbTipoDescuento;
	private List<String> tipoDescuentosPersonales = new ArrayList<String>();
	private List<String> tipoAsignacionesPersonales = new ArrayList<String>();
	private ControladorTipoDescuentoSalario controladorTipoDescuentoSalario = new ControladorTipoDescuentoSalario();
	private ControladorBonificacion controladorBonificacion = new ControladorBonificacion();
	private ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
	private JScrollPane scrollPane;
	private JTablePanambi jTableDetalle;
	private Double codigo;
	private Empleado empleado;
	private JButtonPanambi jBtnAplicar;
	JPopupMenu popupMenu = new JPopupMenu();
	private JLabelPanambi lblpnmbNombre;
	private JRadioButton rdbtnMonto;
	private JRadioButton rdbtnPorcentaje;
	private JLabelPanambi lblpnmbSalarioNominal;
	private JTextFieldDouble jTxtSalario;
	private JTextFieldUpper jTxtNombre;
	private JTextFieldInteger jtxtCodigo;
	private JTextFieldDouble jTxtMonto;
	private JTextFieldFloat jTxtPorcentaje;
	private JComboBoxPanambi jCmbAsignacion;
	private JRadioButton rdbtnTipoDescuento;
	private JRadioButton rdbtnAsignacion;
	private JButton btnInactivar;
	private JButton btnSalir;

	/**
	 * Create the dialog.
	 */
	public DlgAplicar(Empleado empleado) {
		this.empleado = empleado;
		initialize();
	}

	private void initialize() {
		setModal(true);
		setResizable(false);
		getJtxtCodigo().setValor(empleado.getCodEmpleado());
		getJTxtSalario().setValue(empleado.getSalario());
		getTextFieldUpper_1().setText(empleado.getNombre()+" "+empleado.getApellido());
		getRdbtnMonto().setSelected(true);
		getJTxtMonto().setEnabled(true);
		getRdbtnPorcentaje().setSelected(false);
		getJTxtPorcentaje().setEnabled(false);
		getJTxtPorcentaje().setValue((float)0);
		getJTxtMonto().setValue(0.0);
		
		getJCmbTipoDescuento().setEditable(false);
		getJCmbAsignacion().setEditable(false);
		
		getJCmbTipoDescuento().setEnabled(true);
		getJCmbAsignacion().setEnabled(false);
		
		getRdbtnTipoDescuento().setSelected(true);
		getRdbtnAsignacion().setSelected(false);
		
		
		doCargarTabla();
		try {
			setTitle("Aplicacion de descuento");
			//setBounds(100, 100, 853, 487);
			setBounds(100, 100, 889, 505);
			getContentPane().add(getPanelCentral(), BorderLayout.CENTER);

		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GroupLayout gl_panelCentral = new GroupLayout(panelCentral);
			gl_panelCentral.setHorizontalGroup(
				gl_panelCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelCentral.createSequentialGroup()
						.addGap(36)
						.addGroup(gl_panelCentral.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelCentral.createSequentialGroup()
								.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panelCentral.createParallelGroup(Alignment.LEADING)
									.addComponent(getJtxtCodigo(), GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
								.addContainerGap(522, Short.MAX_VALUE))
							.addGroup(gl_panelCentral.createSequentialGroup()
								.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 281, Short.MAX_VALUE)
								.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getTextFieldUpper_1(), GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
								.addGap(95))
							.addGroup(gl_panelCentral.createSequentialGroup()
								.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())))
			);
			gl_panelCentral.setVerticalGroup(
				gl_panelCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panelCentral.createSequentialGroup()
						.addGap(35)
						.addGroup(gl_panelCentral.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelCentral.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJtxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panelCentral.createSequentialGroup()
								.addGap(1)
								.addGroup(gl_panelCentral.createParallelGroup(Alignment.BASELINE)
									.addComponent(getTextFieldUpper_1(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbNombre(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panelCentral.createParallelGroup(Alignment.BASELINE)
							.addComponent(getLblpnmbSalarioNominal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtSalario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addComponent(getPanel(), GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
						.addContainerGap())
			);
			panelCentral.setLayout(gl_panelCentral);
		}
		return panelCentral;
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Detalle", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getRdbtnTipoDescuento())
							.addComponent(getRdbtnPorcentaje())
							.addComponent(getRdbtnMonto()))
						.addPreferredGap(ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())
							.addGroup(Alignment.TRAILING, gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(getJCmbTipoDescuento(), GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
									.addComponent(getRdbtnAsignacion(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(getJCmbAsignacion(), GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
									.addContainerGap(67, Short.MAX_VALUE))
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(getJTxtPorcentaje(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addContainerGap()))))
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(76)
						.addComponent(getJBtnAplicar(), GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(getBtnInactivar())
						.addGap(25)
						.addComponent(getBtnSalir())
						.addContainerGap(439, Short.MAX_VALUE))
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 744, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(19, Short.MAX_VALUE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJCmbTipoDescuento(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJCmbAsignacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getRdbtnTipoDescuento())
							.addComponent(getRdbtnAsignacion()))
						.addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getRdbtnMonto())
							.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(6)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJTxtPorcentaje(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getRdbtnPorcentaje()))
						.addGap(18)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(getJBtnAplicar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getBtnInactivar())
							.addComponent(getBtnSalir()))
						.addContainerGap())
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	

	private JButtonPanambi getJBtnAplicar() {
		if (jBtnAplicar == null) {
			jBtnAplicar = new JButtonPanambi();
			jBtnAplicar.setMnemonic('A');
			jBtnAplicar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarDetalle();
				}
			});
			jBtnAplicar.setText("Aplicar");
		}
		return jBtnAplicar;
	}

	private void doAgregarDetalle(){
		
		try {
			Boolean flag = true;
			Integer codempleado = getJtxtCodigo().getValor();
			Double monto = 0.0;
			Float porcentaje = (float) 0.0;
			String concepto = "";
			String tipo = "D";
			monto = (Double) getJTxtMonto().getValue();
			porcentaje = (Float) getJTxtPorcentaje().getValue();
			Boolean porcentajeMenorACien = true;
			Integer cod = null;
			if(getRdbtnAsignacion().isSelected()){
				tipo = "A";
				concepto = getJCmbAsignacion().getSelectedItem().toString();
				Bonificacion tipobonificacion = controladorBonificacion.getBonificacion(JFramePanambiMain.session.getConn(), concepto);
				cod = tipobonificacion.getCodBonificacion();
				
			}else{
				concepto = getJCmbTipoDescuento().getSelectedItem().toString();
				TipoDescuentoSalario tipoDescuento = controladorTipoDescuentoSalario.getTipoDescuento(JFramePanambiMain.session.getConn(), concepto);
				cod = tipoDescuento.getCodTipoDescuento();
			}
			
			if(getRdbtnMonto().isSelected() && monto ==0.0){
				flag = false;
				DlgMessage.showMessage(getOwner(), "El monto no puede ser cero.", DlgMessage.ERROR_MESSAGE);
				
			}
			
			if(porcentaje>(float)100){
				getJTxtPorcentaje().setValue((float)0);
				flag =false;
				porcentajeMenorACien =false;
				DlgMessage.showMessage(getOwner(), "El porcentaje no puede ser mayor a 100.", DlgMessage.ERROR_MESSAGE);
			}
			
			if(getRdbtnPorcentaje().isSelected() && porcentaje ==(float) 0.0 && porcentajeMenorACien==true){
				flag = false;
				DlgMessage.showMessage(getOwner(), "El porcentaje no puede ser cero ni mayor a 100.", DlgMessage.ERROR_MESSAGE);
				
			}
			
			// si eso ya existe
			if(flag){
				Boolean continuar = controladorEmpleado.getVerificarDetalleDuplicado(JFramePanambiMain.session.getConn(), cod, tipo, codempleado);
				if(continuar == false){
					controladorEmpleado.getAplicarDescuentoPersonal(JFramePanambiMain.session.getConn(), codempleado, monto, porcentaje, concepto, tipo);
					doCargarTabla();
				}else{
					DlgMessage.showMessage(getOwner(), "El detalle ya existe, inactive el existente para crear uno nuevo.", DlgMessage.ERROR_MESSAGE);
				}
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private JLabelPanambi getLblpnmbNombre() {
		if (lblpnmbNombre == null) {
			lblpnmbNombre = new JLabelPanambi();
			lblpnmbNombre.setText("Nombre");
			lblpnmbNombre.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbNombre;
	}
		
	private JRadioButton getRdbtnMonto() {
		if (rdbtnMonto == null) {
			rdbtnMonto = new JRadioButton("Monto");
			rdbtnMonto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionDescuentoMonto();
				}
			});
		}
		return rdbtnMonto;
	}
	
	private void doSeleccionDescuentoMonto() {
		
		if(getRdbtnMonto().isSelected()){
			getRdbtnMonto().setSelected(true);
			getJTxtMonto().setEnabled(true);
			getRdbtnPorcentaje().setSelected(false);
			getJTxtPorcentaje().setEnabled(false);
			getJTxtPorcentaje().setValue((float)0);
		}else{
			getRdbtnMonto().setSelected(false);
			getJTxtMonto().setEnabled(false);
			getRdbtnPorcentaje().setSelected(true);
			getJTxtPorcentaje().setEnabled(true);
			getJTxtMonto().setValue(0.0);
			
		}
		
	}
	
	private JRadioButton getRdbtnPorcentaje() {
		if (rdbtnPorcentaje == null) {
			rdbtnPorcentaje = new JRadioButton("Porcentaje");
			rdbtnPorcentaje.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionDescuentoPorcentaje();
				}
			});
		}
		return rdbtnPorcentaje;
	}
	
	private void doSeleccionDescuentoPorcentaje() {
		
		if(getRdbtnPorcentaje().isSelected()){
			getRdbtnMonto().setSelected(false);
			getJTxtMonto().setEnabled(false);
			getRdbtnPorcentaje().setSelected(true);
			getJTxtPorcentaje().setEnabled(true);
			getJTxtMonto().setValue(0.0);
			
		}else{
			getRdbtnMonto().setSelected(true);
			getJTxtMonto().setEnabled(true);
			getRdbtnPorcentaje().setSelected(false);
			getJTxtPorcentaje().setEnabled(false);
			getJTxtPorcentaje().setValue((float)0);
		}
		
	}
	
	private JLabelPanambi getLblpnmbSalarioNominal() {
		if (lblpnmbSalarioNominal == null) {
			lblpnmbSalarioNominal = new JLabelPanambi();
			lblpnmbSalarioNominal.setText("Salario Nominal");
			lblpnmbSalarioNominal.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbSalarioNominal;
	}
	private JTextFieldDouble getJTxtSalario() {
		if (jTxtSalario == null) {
			jTxtSalario = new JTextFieldDouble();
			jTxtSalario.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtSalario.setEditable(false);
			jTxtSalario.setDisabledTextColor(Color.RED);
			jTxtSalario.setCantDecimals(0);
		}
		return jTxtSalario;
	}
	private JTextFieldUpper getTextFieldUpper_1() {
		if (jTxtNombre == null) {
			jTxtNombre = new JTextFieldUpper();
			jTxtNombre.setEditable(false);
		}
		return jTxtNombre;
	}
	
	private JLabelPanambi getLblpnmbAPagar() {
		if (lblpnmbAPagar == null) {
			lblpnmbAPagar = new JLabelPanambi();
			lblpnmbAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
			lblpnmbAPagar.setText("Codigo");
		}
		return lblpnmbAPagar;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbTipoDescuento() {
		if (jCmbTipoDescuento == null) {
			jCmbTipoDescuento = new JComboBoxPanambi();
			
			tipoDescuentosPersonales = new ArrayList<String>();
			List<TipoDescuentoSalario> listatipos;
			try {
				listatipos = controladorTipoDescuentoSalario.getTiposDescuentosPersonales(JFramePanambiMain.session.getConn());
				for (TipoDescuentoSalario form : listatipos) {
					tipoDescuentosPersonales.add(form.getNombre());
				}
				
				jCmbTipoDescuento.setModel(new ListComboBoxModel<String>(tipoDescuentosPersonales));
				
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		}
		return jCmbTipoDescuento;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTableDetalle());
		}
		return scrollPane;
	}
	
	private JTablePanambi getJTableDetalle() {
		
		String[] columnNames = { "Sel.", "Codigo", "Concepto", "Porcentaje", "Monto","Estado"};
		HashMap<Integer, Class<?>> types = new HashMap<>();
		types.put(0, Boolean.class);
		types.put(1, Integer.class);
		types.put(5, Integer.class);
		Integer[] editable = { 0,0 };
		
		if (jTableDetalle == null) {
			jTableDetalle =  new JTablePanambi(columnNames,editable, types);
			jTableDetalle.getColumnModel().getColumn(0).setPreferredWidth(10);
			jTableDetalle.getColumnModel().getColumn(1).setPreferredWidth(10);
			jTableDetalle.getColumnModel().getColumn(2).setPreferredWidth(60);
			jTableDetalle.getColumnModel().getColumn(3).setPreferredWidth(10);
			jTableDetalle.getColumnModel().getColumn(4).setPreferredWidth(10);
			jTableDetalle.getColumnModel().getColumn(5).setPreferredWidth(10);
			jTableDetalle.setFocusable(false);
			jTableDetalle.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTableDetalle.rowAtPoint(e.getPoint());
						jTableDetalle.changeSelection(r, 0, true, true);
						int[] selecteds = jTableDetalle.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});
			jTableDetalle.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					logger.debug("key on table");
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						//doEliminarDetalle();
					}
					super.keyPressed(e);
				}
			});

		}
		return jTableDetalle;
	}
	private JTextFieldInteger getJtxtCodigo() {
		if (jtxtCodigo == null) {
			jtxtCodigo = new JTextFieldInteger();
			jtxtCodigo.setEditable(false);
		}
		return jtxtCodigo;
	}
	private JTextFieldDouble getJTxtMonto() {
		if (jTxtMonto == null) {
			jTxtMonto = new JTextFieldDouble();
		}
		return jTxtMonto;
	}
	private JTextFieldFloat getJTxtPorcentaje() {
		if (jTxtPorcentaje == null) {
			jTxtPorcentaje = new JTextFieldFloat();
			jTxtPorcentaje.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					doComprobarPorcentaje();
				}
			});
		}
		return jTxtPorcentaje;
	}

	private void doComprobarPorcentaje(){
		
		if((Float)getJTxtPorcentaje().getValue()>(float)100){
			getJTxtPorcentaje().setValue((float)0);
		}
	}
	private JComboBoxPanambi getJCmbAsignacion() {
		if (jCmbAsignacion == null) {
			jCmbAsignacion = new JComboBoxPanambi();
			tipoAsignacionesPersonales = new ArrayList<String>();
			List<Bonificacion> listatipos;
			try {
				listatipos = controladorBonificacion.getBonificaciones(JFramePanambiMain.session.getConn());
				for (Bonificacion form : listatipos) {
					tipoAsignacionesPersonales.add(form.getDescripcion());
				}
				
				jCmbAsignacion.setModel(new ListComboBoxModel<String>(tipoAsignacionesPersonales));
				
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		}
		return jCmbAsignacion;
	}
	private JRadioButton getRdbtnTipoDescuento() {
		if (rdbtnTipoDescuento == null) {
			rdbtnTipoDescuento = new JRadioButton("Tipo de descuento");
			rdbtnTipoDescuento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionarTipoDeDetalleDescuento();
				}
			});
		}
		return rdbtnTipoDescuento;
	}
	
	private void doSeleccionarTipoDeDetalleDescuento(){
		if(getRdbtnTipoDescuento().isSelected()){
			getJCmbTipoDescuento().setEnabled(true);
			getJCmbAsignacion().setEnabled(false);
			getRdbtnAsignacion().setSelected(false);
		}else{
			getJCmbTipoDescuento().setEnabled(false);
			getJCmbAsignacion().setEnabled(true);
			getRdbtnAsignacion().setSelected(true);
		}
		
		getJTxtPorcentaje().setValue((float)0);
		getJTxtMonto().setValue(0.0);
		
	}
	
	private JRadioButton getRdbtnAsignacion() {
		if (rdbtnAsignacion == null) {
			rdbtnAsignacion = new JRadioButton("Asignacion");
			rdbtnAsignacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSeleccionarTipoDeDetalleAsignacion();
				}
			});
		}
		return rdbtnAsignacion;
	}
	
	private void doSeleccionarTipoDeDetalleAsignacion(){
		if(getRdbtnAsignacion().isSelected()){
			getJCmbTipoDescuento().setEnabled(false);
			getJCmbAsignacion().setEnabled(true);
			getRdbtnTipoDescuento().setSelected(false);
		}else{
			getJCmbTipoDescuento().setEnabled(true);
			getJCmbAsignacion().setEnabled(false);
			getRdbtnTipoDescuento().setSelected(true);
		}
		
		getJTxtPorcentaje().setValue((float)0);
		getJTxtMonto().setValue(0.0);
	}
	
	private void doCargarTabla(){
		List<List<String>> detalles;
		try {
			detalles = controladorEmpleado.getAsignacionesDescuentos(JFramePanambiMain.session.getConn(), getJtxtCodigo().getValor());
			getJTableDetalle().resetData(0);
			if(detalles.size()>0){
				//getBtnActivar().setEnabled(true);
				getBtnInactivar().setEnabled(true);
			}else{
				//getBtnActivar().setEnabled(false);
				getBtnInactivar().setEnabled(false);
			}
			
			for (List<String> detalle : detalles) {
				getJTableDetalle().addRow();
				if(detalle.get(4)=="A"){
					getJTableDetalle().setValueAt(true, getJTableDetalle().getRowCount() - 1, 0);
				}else{
					getJTableDetalle().setValueAt(false, getJTableDetalle().getRowCount() - 1, 0);
				}
				
				getJTableDetalle().setValueAt(detalle.get(0), getJTableDetalle().getRowCount() - 1, 1);
				getJTableDetalle().setValueAt(detalle.get(1), getJTableDetalle().getRowCount() - 1, 2);
				getJTableDetalle().setValueAt(detalle.get(2), getJTableDetalle().getRowCount() - 1, 3);
				getJTableDetalle().setValueAt(detalle.get(3), getJTableDetalle().getRowCount() - 1, 4);
				if(detalle.get(4).equals("A")){
					getJTableDetalle().setValueAt("Activo", getJTableDetalle().getRowCount() - 1, 5);
				}else{
					getJTableDetalle().setValueAt("Inactivo", getJTableDetalle().getRowCount() - 1, 5);
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	private JButton getBtnInactivar() {
		if (btnInactivar == null) {
			btnInactivar = new JButton("Inactivar");
			btnInactivar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doCambiarEstadoDescuento("I");
				}
			});
		}
		return btnInactivar;
	}
	
	private void doCambiarEstadoDescuento(String estado){		
		List<Integer> detalles = new ArrayList<Integer>();
		for (int i = 0; i < getJTableDetalle().getRowCount(); i++) {
			
			if((boolean) getJTableDetalle().getValueAt(i, 0) == true){
				String codigo = getJTableDetalle().getValueAt(i, 1).toString();
				Integer detalle = Integer.valueOf(codigo);
				detalles.add(detalle);	
			}
			
		}
		
		try {
			controladorEmpleado.getCambiarEstadoDetalle(JFramePanambiMain.session.getConn(),estado,detalles);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		doCargarTabla();
	}
	private JButton getBtnSalir() {
		if (btnSalir == null) {
			btnSalir = new JButton("Salir");
			btnSalir.setPreferredSize(new Dimension(75, 23));
			btnSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doCancelar();
				}
			});
		}
		return btnSalir;
	}
	
	private void doCancelar(){
		this.dispose();
	}
}
