package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Pago;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JFormattedTextFieldPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextAreaUpper;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JTablePanambi;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class DlgDetallePagos extends JDialogPanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 242232377597913379L;
	private JPanel panel;
	private JLabelPanambi lblpnmbDetallesDeVenta;
	private JLabelPanambi lblpnmbCodigo;
	private JLabelPanambi lblpnmbFecha;
	private JLabelPanambi lblpnmbEstado;
	private JLabelPanambi lblpnmbTotal;
	private JLabelPanambi lblpnmbFechaAnulacin;
	private JLabelPanambi lblpnmbObservaciones;
	private JTextAreaUpper jTxtObservaciones;
	private JTextFieldInteger jTxtCodigo;
	private JTextFieldDouble jTxtMonto;
	private JLabelPanambi lblpnmbDetalleDeVenta;
	private Pago pago;
	private JScrollPane scrollPane;
	private JTablePanambi tablePanambi;
	private JFormattedTextFieldPanambi jTxtFecha;
	private JFormattedTextFieldPanambi jTxtEstado;
	private JFormattedTextFieldPanambi jTxtFechaAnulacion;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButtonPanambi btnpnmbAceptar;
	private JLabelPanambi lblpnmbNroRecibo;
	private JTextFieldInteger jTxtNroRecibo;
	public DlgDetallePagos(Pago pago) {
		this.pago = pago;
		initialize();
		
	}
	private void initialize() {
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgDetallePagos.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setBounds(100, 100, 731, 458);
		getContentPane().setLayout(null);
		getContentPane().add(getPanel());
		cargarValores();
		getContentPane().add(getPanel_1_1());
		getContentPane().add(getPanel_2());
		getBtnpnmbAceptar().requestFocus();
		
	}

	private void cargarValores(){
		try{
			
			getJTxtCodigo().setText(pago.getCodPago()+"");
			getJTxtMonto().setValue(pago.getMonto());
			
			String  patronFecha = "dd/MM/yyyy hh:mm:ss";
			SimpleDateFormat formatoFecha = new SimpleDateFormat(patronFecha);
			getJTxtFecha().setText(formatoFecha.format(pago.getFecha()));

			
			String estado = "";
			if(pago.getEstado().equals("A")){
				estado= "ACTIVO";
			}else{
				estado = "ANULADO";
			}
			getJTxtEstado().setText(estado);
						
			if(pago.getFechaAnulacion()!=null){
				getJTxtFechaAnulacion().setText(formatoFecha.format(pago.getFechaAnulacion()));
			}else{
				getJTxtFechaAnulacion().setVisible(false);
				getLblpnmbFechaAnulacin().setVisible(false);
			}
			
			if(pago.getObservaciones()!=null){
				getJTxtObservaciones().setText(pago.getObservaciones().trim());
			}
			
			if(pago.getNroRecibo()!=0){
				getJTxtNroRecibo().setValor(pago.getNroRecibo());
			}else{
				getJTxtNroRecibo().setVisible(false);
				getLblpnmbNroRecibo().setVisible(false);
			}
			
			List<DetallePago> detallePago = new ArrayList<DetallePago>();
			detallePago = pago.getDetallePago();
			Iterator<DetallePago> iteratorDetalle=  detallePago.listIterator();
			
			while (iteratorDetalle.hasNext()) {
				
				getTablePanambi().addRow();
				
				DetallePago det= (DetallePago) iteratorDetalle.next();
				//"Nro. Item","Forma de pago", "Monto", "Banco","Nro. Cheque","Nro. Boucher","Nro. Nota Cr�dito"
				getTablePanambi().setValueAt(det.getNroitem(), getTablePanambi().getRowCount()-1, 0);
				getTablePanambi().setValueAt(det.getFormaPago().getNombre(), getTablePanambi().getRowCount()-1, 1);
					
				getTablePanambi().setValueAt(det.getMonto(), getTablePanambi().getRowCount()-1, 2);
				if(det.getBanco()!=null){
					getTablePanambi().setValueAt(det.getBanco().getDescripcion(), getTablePanambi().getRowCount()-1, 3);
				}
				if(det.getChequenro()!=null){
					getTablePanambi().setValueAt(det.getChequenro(), getTablePanambi().getRowCount()-1, 4);
				}
				if(det.getBouchernro()!=null){
					getTablePanambi().setValueAt(det.getBouchernro(), getTablePanambi().getRowCount()-1, 5);
				}
				if(det.getNotacredito()!=0){
					getTablePanambi().setValueAt(det.getNotacredito(), getTablePanambi().getRowCount()-1, 6);
				}
				
			}
			getBtnpnmbAceptar().requestFocus();
		}catch(Exception e ){
			logger.error(e.getMessage(),e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
				
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(0, 0, 715, 42);
			panel .setBackground(new Color(0,0,139));
			panel.setLayout(null);
			panel.add(getLblpnmbDetallesDeVenta());
		}
		return panel;
	}
	private JLabelPanambi getLblpnmbDetallesDeVenta() {
		if (lblpnmbDetallesDeVenta == null) {
			lblpnmbDetallesDeVenta = new JLabelPanambi();
			lblpnmbDetallesDeVenta.setForeground(Color.WHITE);
			lblpnmbDetallesDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetallesDeVenta.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblpnmbDetallesDeVenta.setText("DETALLE DEL PAGO");
			lblpnmbDetallesDeVenta.setBounds(0, 0, 715, 40);
		}
		return lblpnmbDetallesDeVenta;
	}
	private JLabelPanambi getLblpnmbCodigo() {
		if (lblpnmbCodigo == null) {
			lblpnmbCodigo = new JLabelPanambi();
			lblpnmbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCodigo.setText("C\u00F3digo : ");
		}
		return lblpnmbCodigo;
	}
	private JLabelPanambi getLblpnmbFecha() {
		if (lblpnmbFecha == null) {
			lblpnmbFecha = new JLabelPanambi();
			lblpnmbFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFecha.setText("Fecha : ");
		}
		return lblpnmbFecha;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Monto : ");
		}
		return lblpnmbTotal;
	}
	private JLabelPanambi getLblpnmbFechaAnulacin() {
		if (lblpnmbFechaAnulacin == null) {
			lblpnmbFechaAnulacin = new JLabelPanambi();
			lblpnmbFechaAnulacin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFechaAnulacin.setText("Fecha anulaci\u00F3n : ");
		}
		return lblpnmbFechaAnulacin;
	}
	private JLabelPanambi getLblpnmbObservaciones() {
		if (lblpnmbObservaciones == null) {
			lblpnmbObservaciones = new JLabelPanambi();
			lblpnmbObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbObservaciones.setText("Observaciones : ");
		}
		return lblpnmbObservaciones;
	}
	private JTextAreaUpper getJTxtObservaciones() {
		if (jTxtObservaciones == null) {
			jTxtObservaciones = new JTextAreaUpper();
			jTxtObservaciones.setFocusable(false);
			jTxtObservaciones.setEditable(false);
			jTxtObservaciones.setFont(new Font("Dialog", Font.PLAIN, 10));
		}
		return jTxtObservaciones;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JTextFieldDouble getJTxtMonto() {
		if (jTxtMonto == null) {
			jTxtMonto = new JTextFieldDouble();
			jTxtMonto.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtMonto.setFocusable(false);
			jTxtMonto.setEditable(false);
		}
		return jTxtMonto;
	}
	private JLabelPanambi getLblpnmbDetalleDeVenta() {
		if (lblpnmbDetalleDeVenta == null) {
			lblpnmbDetalleDeVenta = new JLabelPanambi();
			lblpnmbDetalleDeVenta.setHorizontalAlignment(SwingConstants.CENTER);
			lblpnmbDetalleDeVenta.setText("Detalles del pago");
		}
		return lblpnmbDetalleDeVenta;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablePanambi());
		}
		return scrollPane;
	}
	private JTablePanambi getTablePanambi() {
			if (tablePanambi == null) {
				//tablePanambi.setBounds(30, 335, 714, 147);
				String[] columnNames = {"Nro. Item","Forma de pago", "Monto", "Banco","Nro. Cheque","Nro. Boucher","Nro. Nota Cr�dito"};
				HashMap<Integer, Class<?>> types = new HashMap<>();
				types.put(0, Integer.class);
				types.put(2, Double.class);
				
				Integer[] editable = { };
				tablePanambi= new JTablePanambi(columnNames,editable,types);
							
				tablePanambi.setRowHeight(20);
				
				tablePanambi.getColumnModel().getColumn(0).setPreferredWidth(40);
				tablePanambi.getColumnModel().getColumn(1).setPreferredWidth(100);
				tablePanambi.getColumnModel().getColumn(2).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(3).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(4).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(5).setPreferredWidth(70);
				tablePanambi.getColumnModel().getColumn(6).setPreferredWidth(70);
				
				tablePanambi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablePanambi;
	}
	private JFormattedTextFieldPanambi getJTxtFecha() {
		if (jTxtFecha == null) {
			jTxtFecha = new JFormattedTextFieldPanambi();
			jTxtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFecha.setFocusable(false);
			jTxtFecha.setEditable(false);
		}
		return jTxtFecha;
	}
	private JFormattedTextFieldPanambi getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JFormattedTextFieldPanambi();
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
		}
		return jTxtEstado;
	}
	private JFormattedTextFieldPanambi getJTxtFechaAnulacion() {
		if (jTxtFechaAnulacion == null) {
			jTxtFechaAnulacion = new JFormattedTextFieldPanambi();
			jTxtFechaAnulacion.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaAnulacion.setFocusable(false);
			jTxtFechaAnulacion.setEditable(false);
		}
		return jTxtFechaAnulacion;
	}
	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setFocusTraversalKeysEnabled(false);
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_1.setBounds(0, 40, 715, 340);
			GroupLayout gl_panel_1 = new GroupLayout(panel_1);
			gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(24)
						.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(6)
						.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
						.addGap(160)
						.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, 715, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(20)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 671, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(24)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtObservaciones(), GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(10)
										.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(getLblpnmbNroRecibo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(getJTxtNroRecibo(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
								.addGap(160)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))))
			);
			gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtFecha(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(6)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGap(5)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbFechaAnulacin(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtFechaAnulacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
									.addComponent(getLblpnmbNroRecibo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtNroRecibo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addGap(1)
										.addComponent(getJTxtObservaciones(), GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
									.addComponent(getLblpnmbObservaciones(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGap(11)
						.addComponent(getLblpnmbDetalleDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(5)
						.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
			);
			panel_1.setLayout(gl_panel_1);
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel_2.setBounds(0, 380, 715, 38);
			panel_2.add(getBtnpnmbAceptar());
			getBtnpnmbAceptar().requestFocus();
		}
		return panel_2;
	}
	private JButtonPanambi getBtnpnmbAceptar() {
		if (btnpnmbAceptar == null) {
			btnpnmbAceptar = new JButtonPanambi();
			btnpnmbAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doAceptar();
				}
			});
			btnpnmbAceptar.setText("Aceptar");
		}
		return btnpnmbAceptar;
	}
	
	private void doAceptar(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbNroRecibo() {
		if (lblpnmbNroRecibo == null) {
			lblpnmbNroRecibo = new JLabelPanambi();
			lblpnmbNroRecibo.setText("Nro. recibo : ");
			lblpnmbNroRecibo.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbNroRecibo;
	}
	private JTextFieldInteger getJTxtNroRecibo() {
		if (jTxtNroRecibo == null) {
			jTxtNroRecibo = new JTextFieldInteger();
			jTxtNroRecibo.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroRecibo.setFocusable(false);
			jTxtNroRecibo.setEditable(false);
		}
		return jTxtNroRecibo;
	}
}
