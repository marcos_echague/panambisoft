package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Banco;
import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.FormaPago;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.controller.ControladorBanco;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorFormaPago;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;
import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.prompts.PromptNotasCreditos;

public class DlgPagos extends JDialogPanambi implements Browseable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2398408365286756579L;
	private JPanel panelCentral;
	private JLabelPanambi lblpnmbAPagar;
	private JTextFieldDouble jTxtAPagar;
	private JPanel panel;
	private JLabelPanambi lblpnmbMonto;
	private JTextFieldDouble jTxtMontoDetalle;
	private JLabelPanambi lblpnmbFormaDePago;
	private JComboBoxPanambi jCmbFormaPago;
	private List<FormaPago> formasPagos = new ArrayList<FormaPago>();
	private List<Banco> bancos = new ArrayList<Banco>();
	private ControladorFormaPago controladorFormaPago = new ControladorFormaPago();
	private ControladorBanco controladorBanco = new ControladorBanco();
	private JLabelPanambi lblpnmbVuelto;
	private JTextFieldDouble jTxtVuelto;
	private JScrollPane scrollPane;
	private JTablePanambi jTablePagos;
	private Double montoapagar;
	private JButtonPanambi jBtnAgregarPago;
	private JButtonPanambi jBtnRegistrarPago;
	private List<DetallePago> detallePagos;
	private List<PlanPago> detallePlanPagos = new ArrayList<PlanPago>();
	private Pago pago;
	JPopupMenu popupMenu = new JPopupMenu();
	private JLabelPanambi jLabelNumero;
	private JTextFieldUpper jTxtNumero;
	private JLabelPanambi jLabelBanco;
	private JComboBoxPanambi jCmbBanco;
	private ArrayList<Integer> formaspagoexluidas = new ArrayList<Integer>();
	private Cliente cliente;

	/**
	 * Create the dialog.
	 * @wbp.parser.constructor
	 */
	public DlgPagos(JFrame owner, Double montoapagar) {
		super(owner);
		this.montoapagar = montoapagar;
		initialize();
	}
	
	public DlgPagos(JFrame owner, Double montoapagar, Cliente cliente) {
		super(owner);
		this.montoapagar = montoapagar;
		this.cliente = cliente;
		initialize();
	}
	public DlgPagos(Window owner, Double montoapagar) {
		super(owner);
		this.montoapagar = montoapagar;
		initialize();
	}

	public DlgPagos(JFrame owner, Double montoapagar, Integer... formaspagoexcluidas) {
		super(owner);
		this.montoapagar = montoapagar;
		if (formaspagoexcluidas != null) {
			for (int i = 0; i < formaspagoexcluidas.length; i++) {
				Integer fpagoexcluida = formaspagoexcluidas[i];
				this.formaspagoexluidas.add(fpagoexcluida);
			}
		}
		initialize();
	}
	
	public DlgPagos(JFrame owner, Double montoapagar, Cliente cliente, Integer... formaspagoexcluidas) {
		super(owner);
		this.cliente = cliente;
		this.montoapagar = montoapagar;
		if (formaspagoexcluidas != null) {
			for (int i = 0; i < formaspagoexcluidas.length; i++) {
				Integer fpagoexcluida = formaspagoexcluidas[i];
				this.formaspagoexluidas.add(fpagoexcluida);
			}
		}
		initialize();
	}

	public DlgPagos(Window owner, Double montoapagar, Integer... formaspagoexcluidas) {
		super(owner);
		this.montoapagar = montoapagar;
		if (formaspagoexcluidas != null) {
			for (int i = 0; i < formaspagoexcluidas.length; i++) {
				Integer fpagoexcluida = formaspagoexcluidas[i];
				this.formaspagoexluidas.add(fpagoexcluida);
			}
		}
		initialize();
	}

	private void initialize() {
		setModal(true);
		setResizable(false);
		try {
			setTitle("Detalle de Pago");
			setBounds(100, 100, 516, 419);
			poblarListaFormaPago();
			poblarListaBancos();
			getContentPane().add(getPanelCentral(), BorderLayout.CENTER);
			getJTxtAPagar().setValue(getMontoapagar());
			getJTxtMontoDetalle().setValue(getMontoapagar());
			JMenuItem menuItem4 = new JMenuItem("Eliminar Item");
			menuItem4.setFont(FontPanambi.getLabelInstance());
			menuItem4.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					doEliminarDetalle();
				}
			});
			popupMenu.add(menuItem4);
			this.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					cleanPopPups();
				}

				@Override
				public void windowClosed(WindowEvent e) {
					cleanPopPups();
				}
			});

			doChangeFormaPago();

		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JPanel getPanelCentral() throws Exception {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GroupLayout gl_panelCentral = new GroupLayout(panelCentral);
			gl_panelCentral.setHorizontalGroup(gl_panelCentral.createParallelGroup(Alignment.LEADING)
					.addGroup(
							gl_panelCentral
									.createSequentialGroup()
									.addGroup(
											gl_panelCentral
													.createParallelGroup(Alignment.LEADING)
													.addGroup(
															gl_panelCentral.createSequentialGroup().addGap(36).addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
																	.addComponent(getJTxtAPagar(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(63).addComponent(getLblpnmbVuelto(), GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
																	.addPreferredGap(ComponentPlacement.RELATED).addComponent(getJTxtVuelto(), GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
													.addGroup(gl_panelCentral.createSequentialGroup().addGap(64).addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE))).addContainerGap(69, Short.MAX_VALUE)));
			gl_panelCentral.setVerticalGroup(gl_panelCentral.createParallelGroup(Alignment.LEADING).addGroup(
					gl_panelCentral
							.createSequentialGroup()
							.addGap(35)
							.addGroup(
									gl_panelCentral
											.createParallelGroup(Alignment.LEADING)
											.addGroup(
													gl_panelCentral.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtAPagar(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(
													gl_panelCentral.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbVuelto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
															.addComponent(getJTxtVuelto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))).addGap(18).addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 297, Short.MAX_VALUE).addContainerGap()));
			panelCentral.setLayout(gl_panelCentral);
		}
		return panelCentral;
	}

	private JLabelPanambi getLblpnmbAPagar() {
		if (lblpnmbAPagar == null) {
			lblpnmbAPagar = new JLabelPanambi();
			lblpnmbAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
			lblpnmbAPagar.setText("A Pagar");
		}
		return lblpnmbAPagar;
	}

	private JTextFieldDouble getJTxtAPagar() {
		if (jTxtAPagar == null) {
			jTxtAPagar = new JTextFieldDouble();
			jTxtAPagar.setEnabled(false);
			jTxtAPagar.setEditable(false);
			jTxtAPagar.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtAPagar.setCantDecimals(0);
			jTxtAPagar.setDisabledTextColor(Color.RED);
		}
		return jTxtAPagar;
	}

	private JPanel getPanel() throws Exception {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Detalle", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(gl_panel
					.createParallelGroup(Alignment.TRAILING)
					.addGroup(
							gl_panel.createSequentialGroup().addContainerGap(43, Short.MAX_VALUE).addComponent(getJBtnAgregarPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(82)
									.addComponent(getJBtnRegistrarPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(28))
					.addGroup(gl_panel.createSequentialGroup().addContainerGap().addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 326, GroupLayout.PREFERRED_SIZE).addContainerGap(17, Short.MAX_VALUE))
					.addGroup(
							gl_panel.createSequentialGroup()
									.addGap(46)
									.addGroup(
											gl_panel.createParallelGroup(Alignment.TRAILING, false).addComponent(getJLabelBanco(), Alignment.LEADING, 0, 0, Short.MAX_VALUE).addComponent(getLblpnmbMonto(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
													.addComponent(getLblpnmbFormaDePago(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE).addComponent(getJLabelNumero(), Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 75, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(
											gl_panel.createParallelGroup(Alignment.LEADING).addComponent(getJCmbFormaPago(), GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
													.addComponent(getJTxtMontoDetalle(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(getJTxtNumero(), GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
													.addComponent(getJCmbBanco(), GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
			gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(
					gl_panel.createSequentialGroup()
							.addContainerGap()
							.addGroup(
									gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbMonto(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJTxtMontoDetalle(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(
									gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(getLblpnmbFormaDePago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJCmbFormaPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(
									gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(getJLabelNumero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJTxtNumero(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(
									gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(getJLabelBanco(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJCmbBanco(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(
									gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(getJBtnAgregarPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(getJBtnRegistrarPago(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap()));
			panel.setLayout(gl_panel);
		}
		return panel;
	}

	private JLabelPanambi getLblpnmbMonto() {
		if (lblpnmbMonto == null) {
			lblpnmbMonto = new JLabelPanambi();
			lblpnmbMonto.setText("Monto");
		}
		return lblpnmbMonto;
	}

	private JTextFieldDouble getJTxtMontoDetalle() {
		if (jTxtMontoDetalle == null) {
			jTxtMontoDetalle = new JTextFieldDouble();
		}
		return jTxtMontoDetalle;
	}

	private JLabelPanambi getLblpnmbFormaDePago() {
		if (lblpnmbFormaDePago == null) {
			lblpnmbFormaDePago = new JLabelPanambi();
			lblpnmbFormaDePago.setText("Forma de Pago");
		}
		return lblpnmbFormaDePago;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbFormaPago() throws Exception {
		if (jCmbFormaPago == null) {
			jCmbFormaPago = new JComboBoxPanambi();
			jCmbFormaPago.setModel(new ListComboBoxModel<FormaPago>(formasPagos));
			jCmbFormaPago.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					doChangeFormaPago();

				}
			});
		}
		return jCmbFormaPago;
	}

	private void doChangeFormaPago() {
		try {
			int codformapago = ((FormaPago) getJCmbFormaPago().getSelectedItem()).getCodFormaPago();
			switch (codformapago) {
			case 2:
			case 3:
				getJLabelNumero().setText("Boucher Nro.");
				getJLabelNumero().setVisible(true);
				getJTxtNumero().setVisible(true);
				getJLabelBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				break;
			case 4:
				getJLabelNumero().setText("Cheque Nro.");
				getJLabelNumero().setVisible(true);
				getJTxtNumero().setVisible(true);
				getJLabelBanco().setVisible(true);
				getJCmbBanco().setVisible(true);
				break;

			case 5:
				getJLabelNumero().setText("Nota de Cr�dito Nro.");
				getJLabelNumero().setVisible(true);
				getJTxtNumero().setVisible(true);
				getJLabelBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				PromptNotasCreditos promptNotasCreditos = new PromptNotasCreditos(this, getCliente());
				promptNotasCreditos.setVisible(true);
				break;
			case 6:
				getJLabelNumero().setVisible(false);
				getJTxtNumero().setVisible(false);
				getJLabelBanco().setVisible(false);
				getJCmbBanco().setVisible(false);

				DlgGeneraPlanPago dlgPlanPago = new DlgGeneraPlanPago(getOwner(), (Double) getJTxtMontoDetalle().getValue());
				dlgPlanPago.setVisible(true);
				if (dlgPlanPago.getPlanespago() != null && !dlgPlanPago.getPlanespago().isEmpty()) {
					getJBtnAgregarPago().doClick();
					getDetallePlanPagos().addAll(dlgPlanPago.getPlanespago());
				}
				break;

			default:
				getJLabelNumero().setVisible(false);
				getJTxtNumero().setVisible(false);
				getJTxtNumero().setText(null);
				getJLabelBanco().setVisible(false);
				getJCmbBanco().setVisible(false);
				break;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}

	/**
	 * Carga los valores del combo para formas de pago
	 * 
	 * @throws Exception
	 */
	private void poblarListaFormaPago() throws Exception {
		boolean notacredito = false;
		if (getCliente() != null) {
			ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
			List<Devolucion> devoluciones = controladorDevolucion.getNotasCreditoActivasCliente(JFramePanambiMain.session.getConn(), getCliente());
			if (!devoluciones.isEmpty()) {
				notacredito = true;
			}
		}
		List<FormaPago> listaform = controladorFormaPago.getFormasPagosActivas(JFramePanambiMain.session.getConn(), this.formaspagoexluidas);
		formasPagos = new ArrayList<FormaPago>();
		for (FormaPago form : listaform) {
			if (form.getCodFormaPago() == 5) {//Nota de cr�dito
				if (notacredito) {
					formasPagos.add(form);
				}
			} else {
				formasPagos.add(form);
			}
		}
	}

	/**
	 * Carga los valores del combo para Bancos
	 * 
	 * @throws Exception
	 */
	private void poblarListaBancos() throws Exception {
		bancos = new ArrayList<Banco>();
		List<Banco> listaform = controladorBanco.getBancosActivos(JFramePanambiMain.session.getConn());
		for (Banco form : listaform) {
			bancos.add(form);
		}
	}

	private JLabelPanambi getLblpnmbVuelto() {
		if (lblpnmbVuelto == null) {
			lblpnmbVuelto = new JLabelPanambi();
			lblpnmbVuelto.setText("Vuelto");
			lblpnmbVuelto.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblpnmbVuelto;
	}

	private JTextFieldDouble getJTxtVuelto() {
		if (jTxtVuelto == null) {
			jTxtVuelto = new JTextFieldDouble();
			jTxtVuelto.setFont(new Font("Dialog", Font.BOLD, 12));
			jTxtVuelto.setEnabled(false);
			jTxtVuelto.setEditable(false);
			jTxtVuelto.setDisabledTextColor(Color.BLACK);
			jTxtVuelto.setCantDecimals(0);
			jTxtVuelto.setValue(0);
		}
		return jTxtVuelto;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTablePagos());
		}
		return scrollPane;
	}

	private JTablePanambi getJTablePagos() {
		if (jTablePagos == null) {
			String[] columnNames = { "Forma de Pago", "Monto", "Data" };
			jTablePagos = new JTablePanambi(columnNames);
			jTablePagos.getColumnModel().getColumn(0).setPreferredWidth(150);
			jTablePagos.getColumnModel().getColumn(1).setPreferredWidth(60);
			jTablePagos.getColumnModel().getColumn(2).setMinWidth(0);
			jTablePagos.getColumnModel().getColumn(2).setPreferredWidth(0);
			jTablePagos.getColumnModel().getColumn(2).setMaxWidth(0);
			jTablePagos.setFocusable(false);
			jTablePagos.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					boolean filaseleccionada = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						int r = jTablePagos.rowAtPoint(e.getPoint());
						jTablePagos.changeSelection(r, 0, true, true);
						int[] selecteds = jTablePagos.getSelectedRows();
						for (int i = 0; i < selecteds.length; i++) {
							int sel = selecteds[i];
							if (sel == r) {
								filaseleccionada = true;
							}
						}
						if (filaseleccionada) {
							popupMenu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				}
			});
			jTablePagos.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					logger.debug("key on table");
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						doEliminarDetalle();
					}
					super.keyPressed(e);
				}
			});

		}
		return jTablePagos;
	}

	/**
	 * @return the montoapagar
	 */
	public Double getMontoapagar() {
		return montoapagar;
	}

	/**
	 * @param montoapagar
	 *            the montoapagar to set
	 */
	public void setMontoapagar(Double montoapagar) {
		this.montoapagar = montoapagar;
	}

	private JButtonPanambi getJBtnAgregarPago() {
		if (jBtnAgregarPago == null) {
			jBtnAgregarPago = new JButtonPanambi();
			jBtnAgregarPago.setMnemonic('A');
			jBtnAgregarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAgregarPago();
				}
			});
			jBtnAgregarPago.setText("Agregar Pago");
		}
		return jBtnAgregarPago;
	}

	private void doAgregarPago() {
		try {
			checkDetalle();
			DetallePago detallePago = new DetallePago();
			detallePago.setMonto((Double) getJTxtMontoDetalle().getValue());
			detallePago.setFormaPago((FormaPago) getJCmbFormaPago().getSelectedItem());
			switch (detallePago.getFormaPago().getCodFormaPago()) {
			case 2:
			case 3:
				detallePago.setBouchernro(getJTxtNumero().getText().trim());
				break;
			case 4:
				detallePago.setChequenro(getJTxtNumero().getText().trim());
				detallePago.setBanco((Banco) getJCmbBanco().getSelectedItem());
				break;
			case 5:
				detallePago.setNotacredito(Integer.valueOf(getJTxtNumero().getText().trim()));
				break;
			default:
				break;
			}
			// Identificando si es o no nueva forma de pago para efectivo
			int rowacumula = -1;
			double acumulado = 0.0;
			if (((FormaPago) getJCmbFormaPago().getSelectedItem()).getCodFormaPago() == 1) {
				for (int i = 0; i < getJTablePagos().getRowCount(); i++) {
					DetallePago dp = ((DetallePago) getJTablePagos().getValueAt(i, 2));
					if (dp.getFormaPago().getCodFormaPago() == 1) {
						rowacumula = i;
						acumulado = dp.getMonto();
					}
				}

			}
			if (rowacumula == -1) {
				getJTablePagos().addRow();
				getJTablePagos().setValueAt(detallePago.getFormaPago().getNombre(), getJTablePagos().getRowCount() - 1, 0);
				getJTablePagos().setValueAt(detallePago.getMonto(), getJTablePagos().getRowCount() - 1, 1);
				getJTablePagos().setValueAt(detallePago, getJTablePagos().getRowCount() - 1, 2);
			} else {
				detallePago.setMonto(acumulado + detallePago.getMonto());
				getJTablePagos().setValueAt(detallePago.getMonto(), rowacumula, 1);
				getJTablePagos().setValueAt(detallePago, rowacumula, 2);
			}
			getJCmbFormaPago().setSelectedIndex(0);
			doChangeFormaPago();
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		} finally {
			calculate();
		}

	}

	private void checkDetalle() throws Exception {
		if (getJTxtMontoDetalle().getValue() == null || (Double) getJTxtMontoDetalle().getValue() == 0.0) {
			throw new ValidException("Debe ingresar valor para el monto");
		}

		Double montototal = (Double) getJTxtMontoDetalle().getValue();
		Double montototalefectivo = 0.0;
		if (!(((FormaPago) getJCmbFormaPago().getSelectedItem()).getCodFormaPago() > 1)) {
			montototalefectivo = (Double) getJTxtMontoDetalle().getValue();
		}
		if (montototal - getMontoapagar() > montototalefectivo) {
			throw new ValidException("No puede generar vuelto mayor al importe efectivo.");
		}
		int codformapago = ((FormaPago) getJCmbFormaPago().getSelectedItem()).getCodFormaPago();
		switch (codformapago) {
		case 2:
		case 3:
			if (getJTxtNumero().getText().trim().isEmpty()) {
				throw new ValidException("Debe ingresar el Nro. de Boucher para las operaciones con tarjetas.");
			}
			break;
		case 4:
			if (getJTxtNumero().getText().trim().isEmpty()) {
				throw new ValidException("Debe ingresar el Nro. de Cheque para las operaciones con cheques.");
			}
			break;
		case 5:
				Devolucion nota =null;
				try{
					nota = new ControladorDevolucion().getNotaCredito(JFramePanambiMain.session.getConn(), Integer.parseInt(getJTxtNumero().getText()));
				}catch(Exception e){
					nota = null;
				}
										
				if(nota !=null){
					if(nota.getTotal()+1-1!=(Double)getJTxtMontoDetalle().getValue()+1-1){
						throw new ValidException("Datos incorrectos de la nota de cr�dito.\nFavor verifique.");
						
					}else{
						boolean yaUtilizado = false;
						for(int i = 0;i<getJTablePagos().getRowCount();i++){
							Integer nroAgregado;
							try{
								nroAgregado = ((DetallePago) getJTablePagos().getValueAt(i, 2)).getNotacredito();
							}catch(Exception e ){
								nroAgregado = 0;
							}
							if(nota.getNotaCredito() == nroAgregado){
								yaUtilizado = true;
							}
						}
						
						if(yaUtilizado){							
							throw new ValidException("Nota de cr�dito ya agregada");
						}
					}
				}else{
					throw new ValidException("Datos incorrectos de la nota de cr�dito.\nFavor verifique.");
				}
		
			break;
		case 6:
			if (!montototal.equals(getMontoapagar())) {
				getJCmbFormaPago().setSelectedIndex(0);
				throw new ValidException("El pago a cr�dito solo es posible realizar contra la venta total.");
			}
			break;
		default:
			break;
		}

	}

	/**
	 * Re-Calcula detalle de pago
	 */
	private void calculate() {
		double montototal = 0;
		for (int i = 0; i < getJTablePagos().getRowCount(); i++) {
			montototal += (Double) getJTablePagos().getValueAt(i, 1);
		}
		if (montototal >= montoapagar) {
			getJTxtAPagar().setDisabledTextColor(new Color(0, 111, 55));
			getJBtnRegistrarPago().setEnabled(true);
			if (montototal > montoapagar) {
				getJTxtVuelto().setValue(montototal - montoapagar);
			} else {
				getJTxtVuelto().setValue(0.0);
				getJTxtMontoDetalle().setValue(0.0);
			}
			getJBtnRegistrarPago().requestFocus();

		} else {
			getJTxtVuelto().setValue(0.0);
			getJTxtAPagar().setDisabledTextColor(Color.RED);
			getJBtnRegistrarPago().setEnabled(false);
			getJTxtMontoDetalle().setValue(montoapagar - montototal);
			getJTxtMontoDetalle().requestFocus();
		}
		this.repaint();

	}

	private JButtonPanambi getJBtnRegistrarPago() {
		if (jBtnRegistrarPago == null) {
			jBtnRegistrarPago = new JButtonPanambi();
			jBtnRegistrarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRegistrarPago();
				}
			});
			jBtnRegistrarPago.setEnabled(false);
			jBtnRegistrarPago.setMnemonic('R');
			jBtnRegistrarPago.setText("Registrar Pago");
		}
		return jBtnRegistrarPago;
	}

	private void doRegistrarPago() {
		int returnValue = JOptionPane.showConfirmDialog(this, "Confirma el registro del pago ?", null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
		if (returnValue == JOptionPane.YES_OPTION) {
			this.pago = new Pago();
			this.detallePagos = new ArrayList<DetallePago>();
			for (int i = 0; i < getJTablePagos().getRowCount(); i++) {
				DetallePago detallePago = new DetallePago();
				detallePago = (DetallePago) getJTablePagos().getValueAt(i, 2);
				detallePagos.add(detallePago);
			}
			double acumulador = 0.0;
			for (DetallePago detallePago : detallePagos) {
				acumulador += detallePago.getMonto();
			}
			pago.setMonto(acumulador);
			pago.setDetallePago(detallePagos);
			pago.setSucursal(JFramePanambiMain.session.getSucursalOperativa());
			this.dispose();

		}

	}

	public List<DetallePago> getDetallePagos() {
		return detallePagos;
	}

	public void setPagos(List<DetallePago> pagos) {
		this.detallePagos = pagos;
	}

	private void cleanPopPups() {
		popupMenu.setVisible(false);
	}

	/**
	 * Elimina item del detalle
	 */
	private void doEliminarDetalle() {
		if (getJTablePagos().getSelectedRow() > -1) {
			int response = JOptionPane.showConfirmDialog(getOwner(), "<html>Eliminando el detalle del pago.<br><br>Desea continuar?", "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.YES_OPTION) {
				if (((DetallePago) getJTablePagos().getValueAt(getJTablePagos().getSelectedRow(), 2)).getFormaPago().getCodFormaPago() == 6) {//para tipo creditos
					this.detallePlanPagos = new ArrayList<PlanPago>();
				}
				getJTablePagos().removeRow(getJTablePagos().getSelectedRow());
			}
			calculate();
		}
	}

	private JLabelPanambi getJLabelNumero() {
		if (jLabelNumero == null) {
			jLabelNumero = new JLabelPanambi();
			jLabelNumero.setText("Nro. Cheque");
		}
		return jLabelNumero;
	}

	private JTextFieldUpper getJTxtNumero() {
		if (jTxtNumero == null) {
			jTxtNumero = new JTextFieldUpper();
		}
		return jTxtNumero;
	}

	private JLabelPanambi getJLabelBanco() {
		if (jLabelBanco == null) {
			jLabelBanco = new JLabelPanambi();
			jLabelBanco.setText("Banco");
		}
		return jLabelBanco;
	}

	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbBanco() {
		if (jCmbBanco == null) {
			jCmbBanco = new JComboBoxPanambi();
			jCmbBanco.setModel(new ListComboBoxModel<Banco>(bancos));
		}
		return jCmbBanco;
	}

	/**
	 * @return the pago
	 */
	public Pago getPago() {
		return pago;
	}

	/**
	 * @param pago the pago to set
	 */
	public void setPago(Pago pago) {
		this.pago = pago;
	}

	/**
	 * @return the detallePlanPagos
	 */
	public List<PlanPago> getDetallePlanPagos() {
		return detallePlanPagos;
	}

	/**
	 * @param detallePlanPagos the detallePlanPagos to set
	 */
	public void setDetallePlanPagos(List<PlanPago> detallePlanPagos) {
		this.detallePlanPagos = detallePlanPagos;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		try {
			this.cliente = cliente;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}

	}

	@Override
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			Devolucion devolucion = (Devolucion) obj;
			getJTxtMontoDetalle().setValue(devolucion.getTotal());	
			devolucion.getNotaCredito();
			getJTxtNumero().setText("" + devolucion.getNotaCredito());
			getJBtnAgregarPago().requestFocus();
		}

	}

	@Override
	public void setNoValues(Object obj, Integer source) {
		if (obj instanceof Devolucion) {
			DlgMessage.showMessage(getOwner(), "No ha seleccionado ninguna nota de cr�dito", DlgMessage.ERROR_MESSAGE);
		}
	}

}
