package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import py.com.panambi.bean.Bonificacion;
import py.com.panambi.controller.ControladorBonificacion;
import py.com.panambi.interfaces.Browseable;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JCheckBoxPanambi;
import py.com.panambi.swing.components.JComboBoxPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.table.JBrowser;

public class JIBonificacion extends JInternalFramePanambi implements Browseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7824387487569069818L;
	private JCheckBoxPanambi chckbxpnmbActivo;
	private JTextFieldInteger jTxtCodigo;
	private JLabelPanambi labelPanambi;
	private JLabelPanambi lblpnmbBonificacion;
	private JComboBoxPanambi jCmbDescripcion;
	private JLabelPanambi labelPanambi_2;
	private JLabelPanambi labelPanambi_3;
	private JButtonPanambi btnpnmbSalir;
	private JButtonPanambi btnpnmbEliminar;
	private JButtonPanambi btnpnmbGrabar;
	private JButtonPanambi btnpnmbBuscar;
	private JButtonPanambi btnpnmbLimpiar;
	private List<String> bonificaciones = new ArrayList<String>();
	private Bonificacion bonificacion;
	private ControladorBonificacion controladorBonificacion = new ControladorBonificacion();
	
	public JIBonificacion() throws Exception{
		initialize();
	}
	private void initialize() {
		setTitle("Bonificaciones");
		setMaximizable(false);
		setBounds(100, 100, 390, 290);
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbBuscar());
		getJPanelSouth().add(getBtnpnmbGrabar());
		getJPanelSouth().add(getBtnpnmbEliminar());
		getJPanelSouth().add(getBtnpnmbSalir());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(228)
					.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
					.addGap(21)
					.addComponent(getChckbxpnmbActivo(), GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(getLblpnmbBonificacion(), GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addComponent(getLabelPanambi_3(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(getLabelPanambi(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(getJTxtCodigo(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getChckbxpnmbActivo(), GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(getLblpnmbBonificacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getJCmbDescripcion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLabelPanambi_2(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		getJPanelCentral().setLayout(groupLayout);
		doLimpiar();
		decorate();
		setShortcuts(this);
		setHelp("bonificaciones");
		getJCmbDescripcion().requestFocus();
	}
	
	@SuppressWarnings("unchecked")
	private void doLimpiar(){
		
		try{
			bonificacion = null;
			poblarLista();
			getJCmbDescripcion().setModel(new ListComboBoxModel<String>(bonificaciones));
			getJCmbDescripcion().setSelectedItem(null);
			getJTxtCodigo().setText(null);
			getChckbxpnmbActivo().setSelected(true);
			getBtnpnmbEliminar().setEnabled(false);
			getBtnpnmbGrabar().setEnabled(false);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this.getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void poblarLista() throws Exception {
		bonificaciones= new ArrayList<String>();
		
		List<Bonificacion> listabon= controladorBonificacion.getBonificaciones(JFramePanambiMain.session.getConn());
		
		for (Bonificacion bon: listabon) {
			bonificaciones.add(bon.getDescripcion());
		}
		
	}
	
	private JCheckBoxPanambi getChckbxpnmbActivo() {
		if (chckbxpnmbActivo == null) {
			chckbxpnmbActivo = new JCheckBoxPanambi();
			chckbxpnmbActivo.setText("Activo");
		}
		return chckbxpnmbActivo;
	}
	private JTextFieldInteger getJTxtCodigo() {
		if (jTxtCodigo == null) {
			jTxtCodigo = new JTextFieldInteger();
			jTxtCodigo.setText((String) null);
			jTxtCodigo.setFocusable(false);
			jTxtCodigo.setEnabled(false);
			jTxtCodigo.setEditable(false);
		}
		return jTxtCodigo;
	}
	private JLabelPanambi getLabelPanambi() {
		if (labelPanambi == null) {
			labelPanambi = new JLabelPanambi();
			labelPanambi.setText("Codigo : ");
			labelPanambi.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return labelPanambi;
	}
	private JLabelPanambi getLblpnmbBonificacion() {
		if (lblpnmbBonificacion == null) {
			lblpnmbBonificacion = new JLabelPanambi();
			lblpnmbBonificacion.setText("Bonificacion : ");
			lblpnmbBonificacion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbBonificacion;
	}
	@SuppressWarnings("unchecked")
	private JComboBoxPanambi getJCmbDescripcion() {
		if (jCmbDescripcion == null) {
			jCmbDescripcion = new JComboBoxPanambi();
			jCmbDescripcion.getEditor().getEditorComponent().addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					lostFocusDescripcion();
				}
			});
			jCmbDescripcion.setEditable(true);
			jCmbDescripcion.setModel(new ListComboBoxModel<String>(bonificaciones));
			jCmbDescripcion.setSelectedItem(null);
			jCmbDescripcion.setEditable(true);
		}
		return jCmbDescripcion;
	}
	
	private void lostFocusDescripcion() {
		try {
			Bonificacion bon= controladorBonificacion.getBonificacion(JFramePanambiMain.session.getConn(), (String) jCmbDescripcion.getSelectedItem());
			if (bon != null) {
				setValues(bon, null);
				
			} else {
				doNuevoFromLostFocus();
			}
		}catch(NullPointerException ne){
			getJCmbDescripcion().requestFocus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private void doNuevoFromLostFocus() {
		bonificacion = null;
		getJTxtCodigo().setValor(null);
		chckbxpnmbActivo.setSelected(true);
		Integer longitud = 0;
		
		try{
			longitud = getJCmbDescripcion().getSelectedItem().toString().length();
		}catch(Exception e){
			longitud = 0;
		}
		if(longitud!=0){
			btnpnmbGrabar.setEnabled(true);
		}else{
			btnpnmbGrabar.setEnabled(false);
		}
		btnpnmbEliminar.setEnabled(false);
		
	}
	
	private JLabelPanambi getLabelPanambi_2() {
		if (labelPanambi_2 == null) {
			labelPanambi_2 = new JLabelPanambi();
			labelPanambi_2.setText("*");
		}
		return labelPanambi_2;
	}
	private JLabelPanambi getLabelPanambi_3() {
		if (labelPanambi_3 == null) {
			labelPanambi_3 = new JLabelPanambi();
			labelPanambi_3.setText("Presione F1 para Ayuda");
			labelPanambi_3.setForeground(Color.GRAY);
		}
		return labelPanambi_3;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doSalir();
				}
			});
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JButtonPanambi getBtnpnmbEliminar() {
		if (btnpnmbEliminar == null) {
			btnpnmbEliminar = new JButtonPanambi();
			btnpnmbEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doEliminar();
				}
			});
			btnpnmbEliminar.setMnemonic('E');
			btnpnmbEliminar.setToolTipText("Eliminar bonificaicion");
			btnpnmbEliminar.setText("Eliminar");
		}
		return btnpnmbEliminar;
	}
	
private void doEliminar() {
		
		Integer ret = JOptionPane.showInternalConfirmDialog(this, "Esta seguro que desea eliminar permanentemente\n la bonificaci�n?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(ret == JOptionPane.YES_OPTION){
			try {
				controladorBonificacion.borrarBonificacion(JFramePanambiMain.session.getConn(), bonificacion);
				DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJCmbDescripcion().requestFocus();
			} catch (SQLException e) {
				if (e.getMessage().contains("for�nea") || e.getMessage().contains("foreign")) {
					DlgMessage.showMessage(getOwner(), "Error de integridad al borrar registro.", DlgMessage.ERROR_MESSAGE);
				} else {
					DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
		bonificacion=null;
		jCmbDescripcion.requestFocus();
	}
	
	private JButtonPanambi getBtnpnmbGrabar() {
		if (btnpnmbGrabar == null) {
			btnpnmbGrabar = new JButtonPanambi();
			btnpnmbGrabar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doGrabar();
				}
			});
			btnpnmbGrabar.setMnemonic('G');
			btnpnmbGrabar.setToolTipText("Guardar bonificacion");
			btnpnmbGrabar.setText("Grabar");
		}
		return btnpnmbGrabar;
	}
	
	private void doGrabar() {
		try {
			if (bonificacion== null) {
				bonificacion= new Bonificacion();
				bonificacion.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				
				if(chckbxpnmbActivo.isSelected()){
					bonificacion.setEstado("A");
				}else bonificacion.setEstado("I");
				Integer ret = JOptionPane.showConfirmDialog(this, "Confirma grabar la nueva bonificaci�n ?", "", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE );
				if(ret == JOptionPane.YES_OPTION){
					controladorBonificacion.insertarBonificacion(JFramePanambiMain.session.getConn(), bonificacion);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
				
			} else {
				bonificacion.setDescripcion((String) getJCmbDescripcion().getSelectedItem());
				if(chckbxpnmbActivo.isSelected()){
					bonificacion.setEstado("A");
				}else bonificacion.setEstado("I");
				
				Integer ret = JOptionPane.showConfirmDialog(this, "Esta seguro que desea modificar la bonificaci�n ?", "", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE );
				if(ret == JOptionPane.YES_OPTION){
					controladorBonificacion.modificarBonificacion(JFramePanambiMain.session.getConn(), bonificacion);
					DlgMessage.showMessage(getOwner(), "Operaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
			}
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbBuscar() {
		if (btnpnmbBuscar == null) {
			btnpnmbBuscar = new JButtonPanambi();
			btnpnmbBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doBuscar();
				}
			});
			btnpnmbBuscar.setMnemonic('B');
			btnpnmbBuscar.setToolTipText("Buscar bonificaciones");
			btnpnmbBuscar.setText("Buscar");
		}
		return btnpnmbBuscar;
	}
	
	private void doBuscar() {
		try {
			doLimpiar();
			String[] columnNames = {"C�digo", "        Descripci�n        ","Estado"};
			String[] pks = { "codBonificacion" };
			String sSelect = "SELECT codbonificacion, descripcion, (SELECT CASE WHEN estado = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END) ";
			sSelect += "FROM bonificaciones ";
			sSelect += "ORDER BY descripcion";
			JBrowser jb = new JBrowser(JFramePanambiMain.session.getConn(), this, columnNames, sSelect, true, "py.com.panambi.bean.Bonificacion", pks);
			jb.setVisible(true);
		} catch (Exception e) {
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doLimpiar();
					getJCmbDescripcion().requestFocus();
				}
			});
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	@Override
	public void setValues(Object obj, Integer source) {
			
		if (obj instanceof Bonificacion ) {
			this.bonificacion= (Bonificacion) obj;
			getJTxtCodigo().setValor(bonificacion.getCodBonificacion());
			getJCmbDescripcion().setSelectedItem(((Bonificacion) (obj)).getDescripcion());
			
			if(bonificacion.getEstado().equals("A")){
				chckbxpnmbActivo.setSelected(true);
			}else chckbxpnmbActivo.setSelected(false);
			
			btnpnmbEliminar.setEnabled(true);
			btnpnmbGrabar.setEnabled(true);
		}
		
	}
	@Override
	public void setNoValues(Object obj, Integer source) {
		// TODO Auto-generated method stub
		
	}
	private void decorate() {
		AutoCompleteDecorator.decorate(getJCmbDescripcion());
	}
}
