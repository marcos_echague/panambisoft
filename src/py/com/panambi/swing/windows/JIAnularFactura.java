package py.com.panambi.swing.windows;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Venta;
import py.com.panambi.controller.ControladorDevolucion;
import py.com.panambi.controller.ControladorVenta;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JLabelPanambi;
import py.com.panambi.swing.components.JTextFieldDouble;
import py.com.panambi.swing.components.JTextFieldInteger;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.table.JTablePanambi;

public class JIAnularFactura extends JInternalFramePanambi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3881160709898553341L;
	private JLabelPanambi lblpnmbNumeroDeFactura;
	private JTextFieldInteger jTxtNroFactura;
	private JLabelPanambi lblpnmbFechaDeFacturacion;
	private JTextField jTxtFechaFactura;
	private JLabelPanambi lblpnmbCliente;
	private JLabelPanambi lblpnmbFacturadoPor;
	private JLabelPanambi lblpnmbSucursal;
	private JLabelPanambi lblpnmbTotal;
	private JTextFieldUpper jTxtCliente;
	private JPanel panel;
	private JTextFieldUpper jTxtUsuario;
	private JTextFieldUpper jTxtSucursal;
	private JTextFieldDouble jTxtTotal;
	private JScrollPane scrollPane;
	private JTablePanambi jTblDetalleVenta;
	private Venta venta;
	private ControladorVenta controladorVenta = new ControladorVenta();
	private ControladorDevolucion controladorDevolucion = new ControladorDevolucion();
	private JLabelPanambi lblpnmbTipoDeVenta;
	private JTextFieldUpper jTxtTipoVenta;
	private JButtonPanambi btnpnmbAnularFactura;
	private JButtonPanambi btnpnmbLimpiar;
	private JLabelPanambi lblpnmbEstado;
	private JTextField jTxtEstado;
	private JButtonPanambi btnpnmbSalir;
	private JLabelPanambi lblpnmbPresioneFPara;
	
	public JIAnularFactura() throws Exception{
		initialize();
	}
	private void initialize() {
		setMaximizable(false);
		setTitle("Anulacion de facturas");
		setBounds(100, 100, 941, 464);
		doLimpiar();
		getJPanelSouth().add(getBtnpnmbLimpiar());
		getJPanelSouth().add(getBtnpnmbAnularFactura());
		GroupLayout groupLayout = new GroupLayout(getJPanelCentral());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(getLblpnmbNumeroDeFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 523, Short.MAX_VALUE)
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(getPanel(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 884, Short.MAX_VALUE)
								.addComponent(getScrollPane(), GroupLayout.DEFAULT_SIZE, 884, Short.MAX_VALUE))
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(17)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(getLblpnmbNumeroDeFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJTxtNroFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(getLblpnmbPresioneFPara(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(11)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(getScrollPane(), GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addGap(23))
		);
		getJPanelCentral().setLayout(groupLayout);
		setShortcuts(this);
		setHelp("anularFactura");
		getJPanelSouth().add(getBtnpnmbSalir());
	}
	
	private void doLimpiar(){
		getJTxtNroFactura().setValor(null);
		getJTxtFechaFactura().setText("");
		getJTxtCliente().setText("");
		getJTxtSucursal().setText("");
		getJTxtTipoVenta().setText("");
		getJTxtUsuario().setText("");
		getJTxtTotal().setValue(null);
		getJTxtEstado().setText("");
		getJTblDetalleVenta().setFocusable(false);
		
		if(getJTblDetalleVenta().getRowCount()!=0){
			getJTblDetalleVenta().resetData(0);
		}		
	}
	
	
	private JLabelPanambi getLblpnmbNumeroDeFactura() {
		if (lblpnmbNumeroDeFactura == null) {
			lblpnmbNumeroDeFactura = new JLabelPanambi();
			lblpnmbNumeroDeFactura.setText("N�mero de factura : ");
		}
		return lblpnmbNumeroDeFactura;
	}
	private JTextFieldInteger getJTxtNroFactura() {
		if (jTxtNroFactura == null) {
			jTxtNroFactura = new JTextFieldInteger();
			jTxtNroFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtNroFactura.setToolTipText("N�mero de factura a anular");
			jTxtNroFactura.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					lostFocusNroFactura();
				}
			});
		}
		return jTxtNroFactura;
	}
	
private void lostFocusNroFactura(){
		
		if(getJTxtNroFactura().getValor()==null){
			//getJTxtNroFactura().requestFocus();
			getJTblDetalleVenta().setFocusable(false);
		}else{
			try {
				
				Venta ven = controladorVenta.getVentaPorFactura(JFramePanambiMain.session.getConn(), getJTxtNroFactura().getValor());
				if (ven != null) {
					setValues(ven, null);
				} else {
					getJTblDetalleVenta().setFocusable(false);
					DlgMessage.showMessage(getOwner(), "No existe venta con factura nro. "+getJTxtNroFactura().getValor(), DlgMessage.WARNING_MESSAGE);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		}
	}
	
	
	private JLabelPanambi getLblpnmbFechaDeFacturacion() {
		if (lblpnmbFechaDeFacturacion == null) {
			lblpnmbFechaDeFacturacion = new JLabelPanambi();
			lblpnmbFechaDeFacturacion.setText("Fecha facturaci�n : ");
			lblpnmbFechaDeFacturacion.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblpnmbFechaDeFacturacion;
	}
	private JTextField getJTxtFechaFactura() {
		if (jTxtFechaFactura == null) {
			jTxtFechaFactura = new JTextField();
			jTxtFechaFactura.setFocusable(false);
			jTxtFechaFactura.setToolTipText("Fecha de facturaci�n");
			jTxtFechaFactura.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtFechaFactura.setEditable(false);
			jTxtFechaFactura.setColumns(10);
		}
		return jTxtFechaFactura;
	}
	private JLabelPanambi getLblpnmbCliente() {
		if (lblpnmbCliente == null) {
			lblpnmbCliente = new JLabelPanambi();
			lblpnmbCliente.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbCliente.setText("Cliente : ");
		}
		return lblpnmbCliente;
	}
	private JLabelPanambi getLblpnmbFacturadoPor() {
		if (lblpnmbFacturadoPor == null) {
			lblpnmbFacturadoPor = new JLabelPanambi();
			lblpnmbFacturadoPor.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbFacturadoPor.setText("Facturado por : ");
		}
		return lblpnmbFacturadoPor;
	}
	private JLabelPanambi getLblpnmbSucursal() {
		if (lblpnmbSucursal == null) {
			lblpnmbSucursal = new JLabelPanambi();
			lblpnmbSucursal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbSucursal.setText("Sucursal : ");
		}
		return lblpnmbSucursal;
	}
	private JLabelPanambi getLblpnmbTotal() {
		if (lblpnmbTotal == null) {
			lblpnmbTotal = new JLabelPanambi();
			lblpnmbTotal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTotal.setText("Total : ");
		}
		return lblpnmbTotal;
	}
	private JTextFieldUpper getJTxtCliente() {
		if (jTxtCliente == null) {
			jTxtCliente = new JTextFieldUpper();
			jTxtCliente.setFocusable(false);
			jTxtCliente.setToolTipText("Cliente ");
			jTxtCliente.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtCliente.setEditable(false);
		}
		return jTxtCliente;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setFocusable(false);
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(8)
								.addComponent(getLblpnmbFechaDeFacturacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(1)
								.addComponent(getJTxtFechaFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(76)
								.addComponent(getLblpnmbFacturadoPor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getLblpnmbTipoDeVenta(), GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
								.addGap(3)
								.addComponent(getJTxtTipoVenta(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
								.addGap(3)
								.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(43)
								.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
								.addGap(8)
								.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addGap(34)
								.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
								.addGap(8)
								.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(14, Short.MAX_VALUE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(8)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getJTxtEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(getLblpnmbFechaDeFacturacion(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(1)
										.addComponent(getJTxtFechaFactura(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(1)
										.addComponent(getLblpnmbFacturadoPor(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbTipoDeVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtTipoVenta(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbEstado(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(6)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(1)
										.addComponent(getLblpnmbCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(1)
										.addComponent(getJTxtCliente(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addGap(1)
										.addComponent(getLblpnmbSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addComponent(getJTxtSucursal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblpnmbTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(getJTxtTotal(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGap(17))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JTextFieldUpper getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextFieldUpper();
			jTxtUsuario.setFocusable(false);
			jTxtUsuario.setToolTipText("Usuario encargado de facturaci�n");
			jTxtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtUsuario.setEditable(false);
		}
		return jTxtUsuario;
	}
	private JTextFieldUpper getJTxtSucursal() {
		if (jTxtSucursal == null) {
			jTxtSucursal = new JTextFieldUpper();
			jTxtSucursal.setFocusable(false);
			jTxtSucursal.setToolTipText("Sucursal de facturaci�n");
			jTxtSucursal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtSucursal.setEditable(false);
		}
		return jTxtSucursal;
	}
	private JTextFieldDouble getJTxtTotal() {
		if (jTxtTotal == null) {
			jTxtTotal = new JTextFieldDouble();
			jTxtTotal.setFocusable(false);
			jTxtTotal.setToolTipText("Total facturado");
			jTxtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTotal.setEditable(false);
		}
		return jTxtTotal;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getJTblDetalleVenta());
			scrollPane.setFocusable(false);
		}
		return scrollPane;
	}
	private JTablePanambi getJTblDetalleVenta() {
		if (jTblDetalleVenta == null) {
			String[] columnNames = { "Item", "Producto", "Cantidad","Precio unitario","Descuento unitario", "Total item"};
			HashMap<Integer, Class<?>> types = new HashMap<>();
			types.put(0, Integer.class);
			types.put(2, Integer.class);
			types.put(3, Integer.class);
			types.put(4, Double.class);
			types.put(5, Double.class);
			Integer[] editable = { };
			jTblDetalleVenta = new JTablePanambi(columnNames,editable,types);
			jTblDetalleVenta.getColumnModel().getColumn(0).setPreferredWidth(2);
			jTblDetalleVenta.getColumnModel().getColumn(1).setPreferredWidth(250);
			jTblDetalleVenta.getColumnModel().getColumn(2).setPreferredWidth(10);
			jTblDetalleVenta.getColumnModel().getColumn(3).setPreferredWidth(60);
			jTblDetalleVenta.getColumnModel().getColumn(4).setPreferredWidth(60);
			jTblDetalleVenta.getColumnModel().getColumn(5).setPreferredWidth(60);
			jTblDetalleVenta.setFocusable(false);
			
		}
		return jTblDetalleVenta;
	}
	
	public void setValues(Object obj, Integer source) {
		if (obj instanceof Venta) {
			try{	
				this.venta = (Venta) obj;
//				JOptionPane.showMessageDialog(null, venta.getFecha());
//				if(venta.getEstado().equals("I")){
//					
//					DlgMessage.showMessage(getOwner(), "Imposible anular factura.\nLa Factura nro. "+venta.getNroComprobante()+" ya se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
//					getJTxtNroFactura().setValor(null);
//					getJTxtNroFactura().requestFocus();
//				}else if(controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), venta)){
//					DlgMessage.showMessage(getOwner(), "Imposible anular factura.\nLa factura nro. "+venta.getNroComprobante()+" posee devoluciones registradas.", DlgMessage.WARNING_MESSAGE);
//					getJTxtNroFactura().setValor(null);
//					getJTxtNroFactura().requestFocus();
////				}else if(venta.getEstado().equals("A")){
////					DlgMessage.showMessage(getOwner(), "Factura nro "+venta.getNroComprobante()+" se encuentra activa\nNo es posible anularla.", DlgMessage.WARNING_MESSAGE);
////					getJTxtNroFactura().setValor(null);
////					getJTxtNroFactura().requestFocus();
				if(venta ==null){
					doLimpiar();
				}else{
					getJTblDetalleVenta().setFocusable(true);
					String patron = "dd/MM/yyyy";
				    SimpleDateFormat formato = new SimpleDateFormat(patron);
					getJTxtFechaFactura().setText(formato.format(venta.getFecha()));
					getJTxtUsuario().setText(venta.getUsuario().getUsuario());
					if(venta.getTipoVenta().equals("C")){
						getJTxtTipoVenta().setText("CREDITO");
					}
					if(venta.getTipoVenta().equals("D")){
						getJTxtTipoVenta().setText("CONTADO");
					}
					
					if(venta.getEstado().equals("A")){
						getJTxtEstado().setText("ACTIVO");
					}else getJTxtEstado().setText("ANULADO");
					
					getJTxtSucursal().setText(venta.getSucursal().getNombre());
					getJTxtCliente().setText(venta.getCliente().getNombres().toString()+" "+venta.getCliente().getApellidos().toString());
					getJTxtTotal().setValue(venta.getTotal());
					//dtmDetalleVenta = new DefaultTableModel();
					List<DetalleVenta> detallesVentas = new ArrayList<DetalleVenta>();
					detallesVentas  = controladorVenta.getDetalleVenta(JFramePanambiMain.session.getConn(), venta);
					Iterator<DetalleVenta> iteratorDetalleVenta = detallesVentas.listIterator();
					getJTblDetalleVenta().resetData(0);
					while (iteratorDetalleVenta.hasNext()) {
						getJTblDetalleVenta().addRow();
						DetalleVenta dv = (DetalleVenta) iteratorDetalleVenta.next();
						
						getJTblDetalleVenta().setValueAt(dv.getNroitem(), getJTblDetalleVenta().getRowCount()-1, 0);
						getJTblDetalleVenta().setValueAt(dv.getProducto().getDescripcion(), getJTblDetalleVenta().getRowCount()-1, 1);
						getJTblDetalleVenta().setValueAt(dv.getCantidad(), getJTblDetalleVenta().getRowCount()-1, 2);
						getJTblDetalleVenta().setValueAt(dv.getPrecioUnitario(), getJTblDetalleVenta().getRowCount()-1, 3);
						getJTblDetalleVenta().setValueAt(dv.getDescuentoUnitario(), getJTblDetalleVenta().getRowCount()-1, 4);
						getJTblDetalleVenta().setValueAt(dv.getTotalItem(), getJTblDetalleVenta().getRowCount()-1, 5);
					}
				}
				
			}catch (Exception e) {
				DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
			}
		
		}
	}
	 
	
	private JLabelPanambi getLblpnmbTipoDeVenta() {
		if (lblpnmbTipoDeVenta == null) {
			lblpnmbTipoDeVenta = new JLabelPanambi();
			lblpnmbTipoDeVenta.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbTipoDeVenta.setText("Tipo de venta : ");
		}
		return lblpnmbTipoDeVenta;
	}
	private JTextFieldUpper getJTxtTipoVenta() {
		if (jTxtTipoVenta == null) {
			jTxtTipoVenta = new JTextFieldUpper();
			jTxtTipoVenta.setFocusable(false);
			jTxtTipoVenta.setToolTipText("Tipo de venta");
			jTxtTipoVenta.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtTipoVenta.setEditable(false);
		}
		return jTxtTipoVenta;
	}
	private JButtonPanambi getBtnpnmbAnularFactura() {
		if (btnpnmbAnularFactura == null) {
			btnpnmbAnularFactura = new JButtonPanambi();
			btnpnmbAnularFactura.setMnemonic('A');
			btnpnmbAnularFactura.setToolTipText("Anular factura");
			btnpnmbAnularFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doAnularVenta();
				}
			});
			btnpnmbAnularFactura.setText("Anular factura");
		}
		return btnpnmbAnularFactura;
	}
	
	private void doAnularVenta(){
		try{
			if(getJTxtNroFactura().getValor()!=null){
				Date fechaActual = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				
				if(venta.getEstado().equals("I")){
					DlgMessage.showMessage(getOwner(), "Imposible anular factura.\nLa Factura nro. "+venta.getNroComprobante()+" ya se encuentra anulada.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().requestFocus();
				}else if(controladorDevolucion.facturaYaRegistrada(JFramePanambiMain.session.getConn(), venta)){
					DlgMessage.showMessage(getOwner(), "Imposible anular factura.\nLa factura nro. "+venta.getNroComprobante()+" posee devoluciones registradas.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().requestFocus();	
					
				}else if (!sdf.format(fechaActual).equals(sdf.format(venta.getFecha()))){
					DlgMessage.showMessage(getOwner(), "Imposible anular factura.\nSolo es posible anularla el mismo dia de su creaci�n.", DlgMessage.WARNING_MESSAGE);
					getJTxtNroFactura().requestFocus();
				}else{
					Integer ret =null;
					if(venta.getTipoVenta().equals("D")){
						ret = JOptionPane.showInternalConfirmDialog(this, "Se anulara el pagos asociados a la venta.\nConfirma la anulaci�n de la factura?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if(ret == JOptionPane.YES_OPTION){
							doAnularVentaContado();
						}
					}else{
						ret = JOptionPane.showInternalConfirmDialog(this, "Confirma la anulaci�n de la factura ?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if(ret == JOptionPane.YES_OPTION){
							doAnularVentaCredito();
						}
					}
					
					
//					if(ret == JOptionPane.YES_OPTION){
//						String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n de la factura", "Motivo de anulacion :"); 
//						if(observaciones!=null){
//							if(observaciones.trim().toUpperCase().equals("MOTIVO DE ANULACION :")){
//								observaciones = "SIN COMENTARIOS.";
//							}
//							if(observaciones.equals("")){
//								observaciones = "SIN COMENTARIOS.";
//							}
//						}else{
//							observaciones = "SIN COMENTARIOS.";
//						}
//						
//						venta.setObservaciones("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" : "+observaciones);
//						controladorVenta.anularVentaCredito(JFramePanambiMain.session.getConn(), venta);
//						DlgMessage.showMessage(getOwner(), "Operacion exitosa", DlgMessage.INFORMATION_MESSAGE);
//						doLimpiar();
//						getJTxtNroFactura().requestFocus();	
//					}
				}
			}else {
				DlgMessage.showMessage(getOwner(), "Debe ingresar un n�mero de factura", DlgMessage.ERROR_MESSAGE);
				getJTxtNroFactura().requestFocus();
			}
				
		}catch(Exception e){
			DlgMessage.showMessage(getOwner(), e, DlgMessage.ERROR_MESSAGE);
		}		
	}
	
	private void doAnularVentaContado() throws Exception{
		String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n de la factura", "");
		if(observaciones!=null){
			
			if(observaciones.equals("")){
				DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario de anulaci�n", DlgMessage.WARNING_MESSAGE);
			
			}else{
				
				venta.setObservaciones("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" : "+observaciones);
				controladorVenta.anularVentaContado(JFramePanambiMain.session.getConn(), venta);
				DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
				doLimpiar();
				getJTxtNroFactura().requestFocus();
			}
		}
		
		
	}
	
	private void doAnularVentaCredito() throws Exception{
		List<Pago> pagosVentaCredito = new ArrayList<Pago>();
		
		pagosVentaCredito = controladorVenta.getPagosActivosVentaCredito(JFramePanambiMain.session.getConn(), venta);
		
		Integer cantidadPagos = 0;
		
		try{
			cantidadPagos = pagosVentaCredito.size(); 
		}catch(Exception e){
			cantidadPagos = 0;
		}
		
		if (cantidadPagos ==0){
			String observaciones  = JOptionPane.showInputDialog("Ingrese un comentario por la anulaci�n de la factura", "");
			if(observaciones!=null){
				if(observaciones.equals("")){
					DlgMessage.showMessage(getOwner(), "Debe ingresar un comentario de anulaci�n", DlgMessage.WARNING_MESSAGE);
				}else{
					venta.setObservaciones("ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+" : "+observaciones);
					controladorVenta.anularVentaCredito(JFramePanambiMain.session.getConn(), venta);
					DlgMessage.showMessage(getOwner(), "Anulaci�n exitosa", DlgMessage.INFORMATION_MESSAGE);
					doLimpiar();
					getJTxtNroFactura().requestFocus();
				}
			}
			
		}else{
			String mensaje="Imposible anular factura.\nLa factura a cr�dito posee cuotas pagadas.";
				mensaje+="\nFavor elimine los pagos relacionados.";
			
			DlgMessage.showMessage(getOwner(),mensaje, DlgMessage.ERROR_MESSAGE);
		}
		
		
	}
	
	private JButtonPanambi getBtnpnmbLimpiar() {
		if (btnpnmbLimpiar == null) {
			btnpnmbLimpiar = new JButtonPanambi();
			btnpnmbLimpiar.setMnemonic('L');
			btnpnmbLimpiar.setToolTipText("Limpiar pantalla");
			btnpnmbLimpiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					doLimpiar();
				}
			});
			btnpnmbLimpiar.setText("Limpiar");
		}
		return btnpnmbLimpiar;
	}
	private JLabelPanambi getLblpnmbEstado() {
		if (lblpnmbEstado == null) {
			lblpnmbEstado = new JLabelPanambi();
			lblpnmbEstado.setHorizontalAlignment(SwingConstants.RIGHT);
			lblpnmbEstado.setText("Estado : ");
		}
		return lblpnmbEstado;
	}
	private JTextField getJTxtEstado() {
		if (jTxtEstado == null) {
			jTxtEstado = new JTextField();
			jTxtEstado.setFocusable(false);
			jTxtEstado.setEditable(false);
			jTxtEstado.setHorizontalAlignment(SwingConstants.CENTER);
			jTxtEstado.setFont(new Font("Dialog", Font.PLAIN, 11));
			jTxtEstado.setColumns(10);
		}
		return jTxtEstado;
	}
	private JButtonPanambi getBtnpnmbSalir() {
		if (btnpnmbSalir == null) {
			btnpnmbSalir = new JButtonPanambi();
			btnpnmbSalir.setMnemonic('S');
			btnpnmbSalir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSalir();
				}
			});
			btnpnmbSalir.setToolTipText("Cerrar ventana");
			btnpnmbSalir.setText("Salir");
		}
		return btnpnmbSalir;
	}
	
	private void doSalir(){
		this.dispose();
	}
	private JLabelPanambi getLblpnmbPresioneFPara() {
		if (lblpnmbPresioneFPara == null) {
			lblpnmbPresioneFPara = new JLabelPanambi();
			lblpnmbPresioneFPara.setForeground(Color.GRAY);
			lblpnmbPresioneFPara.setText("Presione F1 para Ayuda");
		}
		return lblpnmbPresioneFPara;
	}
}
