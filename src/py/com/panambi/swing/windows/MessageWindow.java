package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

import py.com.panambi.swing.font.FontPanambi;

public class MessageWindow extends JWindow {

	private static final long serialVersionUID = 657325018360971369L;
	private JPanel jContentPane = null;
	private JPanel jPanel = null;
	private JLabel jlabel = null;
	private String message;

	/**
	 * This is the default constructor
	 */
	public MessageWindow(String message) {
		super();
		this.message = message;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setBackground(Color.BLACK);
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel(), java.awt.BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(java.awt.FlowLayout.LEFT);
			jlabel = new JLabel(message);
			jlabel.setFont(FontPanambi.getLabelInstance());
			jPanel = new JPanel();
			jPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
			jPanel.setLayout(flowLayout);
			jPanel.add(jlabel, null);
			jPanel.setBackground(Color.WHITE);
		}
		return jPanel;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		jlabel.setText(message);
	}

}