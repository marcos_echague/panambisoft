package py.com.panambi.swing.windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.connect.EncryptDecrypt;
import py.com.panambi.connect.MD5;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.LoginException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JTextPassword;
import java.awt.Toolkit;

public class DlgPasswordRequest extends JDialogPanambi implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610850113222236567L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblPassword;
	private JLabel lblPasswordR;
	private JTextPassword jTxtPassword;
	private JButtonPanambi jBtnOk;
	private JButtonPanambi jBtnCancelar;
	private JTextPassword jTxtPasswordR;
	private boolean fromLogin = false;
	/**
	 * Create the dialog.
	 */
	public DlgPasswordRequest() {
		initialize();
	}
	
	/**
	 * @param owner
	 */
	public DlgPasswordRequest(Frame owner, boolean fromLogin) {
		super(owner);
		this.fromLogin = fromLogin;
		initialize();
	}
	
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DlgPasswordRequest.class.getResource("/py/com/panambi/images/mariposaIzquierda.png")));
		setTitle("Cambio de contrase\u00F1a");
		setBounds(100, 100, 364, 210);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(28)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(getLblPassword())
						.addComponent(getLblPasswordR()))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(getJTxtPasswordR(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(getJTxtPassword(), GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE))
					.addContainerGap(25, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(32)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtPassword(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblPassword()))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(getJTxtPasswordR(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblPasswordR()))
					.addContainerGap(49, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.add(getJBtnOk());
			buttonPane.add(getJBtnCancelar());
		}

		setShortcutsHere(this);
		centerIt();
	}

	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Contrase\u00F1a");
		}
		return lblPassword;
	}

	private JLabel getLblPasswordR() {
		if (lblPasswordR == null) {
			lblPasswordR = new JLabel("Repita contrase\u00F1a");
		}
		return lblPasswordR;
	}

	private JTextPassword getJTxtPassword() {
		if (jTxtPassword == null) {
			jTxtPassword = new JTextPassword();
			jTxtPassword.setActionCommand("OK");
			jTxtPassword.addActionListener(this);
		}
		return jTxtPassword;
	}
	
	private JTextPassword getJTxtPasswordR() {
		if (jTxtPasswordR == null) {
			jTxtPasswordR = new JTextPassword();
			jTxtPasswordR.setActionCommand("OK");
			jTxtPasswordR.addActionListener(this);
		}
		return jTxtPasswordR;
	}
	
	private void setShortcutsHere(Container component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doCancelar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private JButtonPanambi getJBtnOk() {
		if (jBtnOk == null) {
			jBtnOk = new JButtonPanambi();
			jBtnOk.setMnemonic('A');
			jBtnOk.setText("Aceptar");
			jBtnOk.setActionCommand("OK");
			jBtnOk.addActionListener(this);
		}
		return jBtnOk;
	}

	private JButtonPanambi getJBtnCancelar() {
		if (jBtnCancelar == null) {
			jBtnCancelar = new JButtonPanambi();
			jBtnCancelar.setMnemonic('C');
			jBtnCancelar.setText("Cancelar");
			jBtnCancelar.setActionCommand("CANCEL");
			jBtnCancelar.addActionListener(this);
		}
		return jBtnCancelar;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("OK".equals(cmd)) {
			doAceptar();
		}
		if ("CANCEL".equals(cmd)) {
			doCancelar();
		}
	}

	private void doCancelar() {
		if (fromLogin) {
			JFramePanambiMain.session = null;
			getOwner().dispose();
		}
		this.dispose();
	}

	private void doAceptar() {
		try {
			String passwding = new String(getJTxtPassword().getPassword());
			String passwdingR = new String(getJTxtPasswordR().getPassword());
			
			if(passwding.length()<7){
				DlgMessage.showMessage(this,"Por cuestiones de seguridad, la contrase�a debe tener un m�nimo de 7 car�cteres",DlgMessage.WARNING_MESSAGE);
				getJTxtPassword().requestFocus();
			}else{
				
				if(passwding.equals(passwdingR)==false){
					throw new LoginException("La contrase�a no coincide.");
				}
				String masterpasswd = EncryptDecrypt.decrypt(JFramePanambiMain.appproperties.getProperty("mstrpasswd"));
				MD5 md5 = new MD5();
				md5.init();
				md5.update(passwding.toCharArray(), passwding.toCharArray().length);
				md5.md5final();
				String encpasswd = new String(md5.toHexString());
				logger.debug(encpasswd);
				Connection conn = ConnectionManager.getConnection(JFramePanambiMain.appproperties.getProperty("mstruser"), masterpasswd, JFramePanambiMain.appproperties.getProperty("dbhost"), JFramePanambiMain.appproperties.getProperty("dbname"));
				Usuario usuario = JFramePanambiMain.session.getUsuario();
				ControladorUsuario.setearPassword(conn, usuario, encpasswd);
				this.dispose();
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}
	}
	
}
