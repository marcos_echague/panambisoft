/**
 *
 */
package py.com.panambi.utils;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

import py.com.panambi.swing.font.FontPanambi;
import py.com.panambi.swing.windows.JDialogPanambi;
import py.com.panambi.swing.windows.JInternalFramePanambi;

/**
 * 
 */
public class PanambiUtils {

	public static ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = PanambiUtils.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			return null;
		}
	}

	public static String getStackTrace(Exception e) {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		e.printStackTrace(new PrintWriter(writer, true));
		return writer.toString();
	}

	public boolean isMail(String text) {
		boolean ret = false;
		if (text != null) {
			String regx = "[\\w._%+-]+@[\\w.-]+\\.[A-Za-z]{2,4}";
			if (text.equals("")) {
				ret = true;
			}
			if (text.matches(regx)) {
				ret = true;
			}
		}
		return ret;
	}

	/**
	 * Retorna parametros desde, hasta como timestamp desde la hora 0:00 hasta
	 * la hora 23:59
	 * 
	 * @param desde
	 * @param hasta
	 * @return
	 */
	public Vector<Timestamp> getDesdeHastaFromDates(Date desde, Date hasta) {
		Vector<Timestamp> ret = new Vector<Timestamp>();
		if (desde != null && hasta != null) {
			GregorianCalendar gcdesde = new GregorianCalendar();
			gcdesde.setTimeInMillis(desde.getTime());
			gcdesde.set(Calendar.HOUR_OF_DAY, 0);
			gcdesde.set(Calendar.MINUTE, 0);
			gcdesde.set(Calendar.SECOND, 0);
			gcdesde.set(Calendar.MILLISECOND, 0);
			ret.add(new Timestamp(gcdesde.getTimeInMillis()));

			GregorianCalendar gchasta = new GregorianCalendar();
			gchasta.setTimeInMillis(hasta.getTime());
			gchasta.set(Calendar.HOUR_OF_DAY, 23);
			gchasta.set(Calendar.MINUTE, 59);
			gchasta.set(Calendar.SECOND, 59);
			gchasta.set(Calendar.MILLISECOND, 999);
			ret.add(new Timestamp(gchasta.getTimeInMillis()));
		}
		return ret;
	}

	/**
	 * Retorna parametros desde, hasta como timestamp desde la hora 0:00 hasta
	 * la hora 23:59
	 * 
	 * @param desde
	 * @param hasta
	 * @return
	 */
	public Vector<Timestamp> getDesdeHastaFromGregorian(GregorianCalendar gcdesde, GregorianCalendar gchasta) {
		Vector<Timestamp> ret = new Vector<Timestamp>();
		if (gcdesde != null && gchasta != null) {
			gcdesde.set(Calendar.HOUR_OF_DAY, 0);
			gcdesde.set(Calendar.MINUTE, 0);
			gcdesde.set(Calendar.SECOND, 0);
			gcdesde.set(Calendar.MILLISECOND, 0);
			ret.add(new Timestamp(gcdesde.getTimeInMillis()));

			gchasta.set(Calendar.HOUR_OF_DAY, 23);
			gchasta.set(Calendar.MINUTE, 59);
			gchasta.set(Calendar.SECOND, 59);
			gchasta.set(Calendar.MILLISECOND, 999);
			ret.add(new Timestamp(gchasta.getTimeInMillis()));
		}
		return ret;
	}

	private static void setCursorContainer(Container c, Integer cursortype) {
		Component[] components = ((Container) c).getComponents();
		for (int i = 0; i < components.length; i++) {
			components[i].setCursor(Cursor.getPredefinedCursor(cursortype));
			if (components[i] instanceof Container) {
				setCursorContainer((Container) components[i], cursortype);
			}
		}
	}

	/**
	 * Calcula el mejor ancho para cada columna verificando sus datos. Considera
	 * la �ltima columna como de Object Bean, no visible.
	 * 
	 * @param container
	 * @param table
	 */
	public void resizeColumns(Container container, JTable table) {
		int totalWidth = 0;
		int ultWidht = 0;
		SimpleDateFormat formatime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat formadate = new SimpleDateFormat("dd-MM-yyyy");
		for (int i = 0; i < table.getColumnCount() - 1; i++) {
			int columnWidth = 0;
			TableColumn column = table.getColumnModel().getColumn(i);
			String columnName = (String) column.getHeaderValue();
			Font font = FontPanambi.getLabelInstance();
			FontMetrics fontMetrics = table.getFontMetrics(font);
			columnWidth = fontMetrics.stringWidth(columnName);
			for (int j = 0; j < table.getRowCount(); j++) {
				String texto = "";
				if (table.getValueAt(j, i) != null) {
					if (table.getValueAt(j, i) instanceof Timestamp) {
						texto = formatime.format(((Timestamp) table.getValueAt(j, i)));
					} else if (table.getValueAt(j, i) instanceof Date) {
						texto = formadate.format(((Date) table.getValueAt(j, i)));
					} else if (table.getValueAt(j, i) instanceof Integer) {
						texto = Integer.toString((Integer) table.getValueAt(j, i));
					} else {
						texto = (String) table.getValueAt(j, i);
					}

				}
				if (texto != null) {
					if (fontMetrics.stringWidth(texto) > columnWidth) {
						columnWidth = fontMetrics.stringWidth(texto);
					}
				}
			}
			column.setPreferredWidth(columnWidth + 28);
			totalWidth = totalWidth + (columnWidth + 28);
			if ((i + 1) == table.getColumnCount() - 1) {
				ultWidht = columnWidth + 28;
			}
		}
		if (totalWidth < (container.getWidth() - 13)) {
			table.getColumnModel().getColumn(table.getColumnCount() - 2).setPreferredWidth(((container.getWidth() - 13) - totalWidth) + ultWidht);
		}
	}

	public static void setWaitCursor(Component c) {
		if (c != null) {
			c.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			if (c instanceof Container) {
				setCursorContainer((Container) c, Cursor.WAIT_CURSOR);
			}
			if (c instanceof JInternalFramePanambi) {
				if (((JInternalFramePanambi) c).getOwner() != null) {
					(((JInternalFramePanambi) c).getOwner()).setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					Component[] components = (((JInternalFramePanambi) c).getOwner()).getComponents();
					for (int i = 0; i < components.length; i++) {
						components[i].setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						if (components[i] instanceof Container) {
							setCursorContainer((Container) components[i], Cursor.WAIT_CURSOR);
						}
					}
				}
			}
			if (c instanceof JDialogPanambi) {
				if (((JDialogPanambi) c).getOwner() != null) {
					(((JDialogPanambi) c).getOwner()).setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					Component[] components = (((JDialogPanambi) c).getOwner()).getComponents();
					for (int i = 0; i < components.length; i++) {
						components[i].setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						if (components[i] instanceof Container) {
							setCursorContainer((Container) components[i], Cursor.WAIT_CURSOR);
						}
					}
					if (((JDialogPanambi) c).getOwner() instanceof JFrame) {
						setWaitCursor(((JDialogPanambi) c).getOwner());
					}
				}
			}
		}
	}

	public static void setDefaultCursor(final Component c) {
		if (c != null) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					if (c instanceof Container) {
						setCursorContainer((Container) c, Cursor.DEFAULT_CURSOR);
					}
					if (c instanceof JInternalFramePanambi) {
						if (((JInternalFramePanambi) c).getOwner() != null) {
							(((JInternalFramePanambi) c).getOwner()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							Component[] components = (((JInternalFramePanambi) c).getOwner()).getComponents();
							for (int i = 0; i < components.length; i++) {
								components[i].setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
								if (components[i] instanceof Container) {
									setCursorContainer((Container) components[i], Cursor.DEFAULT_CURSOR);
								}
							}
						}
					}
					if (c instanceof JDialogPanambi) {
						if (((JDialogPanambi) c).getOwner() != null) {
							(((JDialogPanambi) c).getOwner()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							Component[] components = (((JDialogPanambi) c).getOwner()).getComponents();
							for (int i = 0; i < components.length; i++) {
								components[i].setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
								if (components[i] instanceof Container) {
									setCursorContainer((Container) components[i], Cursor.DEFAULT_CURSOR);
								}
							}
							if (((JDialogPanambi) c).getOwner() instanceof JFrame) {
								setDefaultCursor(((JDialogPanambi) c).getOwner());
							}
						}
					}
				}
			});

		}
	}

}
