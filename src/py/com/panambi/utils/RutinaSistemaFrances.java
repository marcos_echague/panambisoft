package py.com.panambi.utils;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import py.com.panambi.bean.PlanPago;

public class RutinaSistemaFrances {

	/**
	 * @return the capital
	 */
	public Double getCapital() {
		return capital;
	}

	/**
	 * @param capital the capital to set
	 */
	public void setCapital(Double capital) {
		this.capital = capital;
	}

	/**
	 * @return the cantidadcuotas
	 */
	public Integer getCantidadcuotas() {
		return cantidadcuotas;
	}

	/**
	 * @param cantidadcuotas the cantidadcuotas to set
	 */
	public void setCantidadcuotas(Integer plazomeses) {
		this.cantidadcuotas = plazomeses;
	}

	/**
	 * @return the tasa
	 */
	public Double getTasa() {
		return tasa;
	}

	/**
	 * @param tasa the tasa to set
	 */
	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}

	private Double capital;
	private Integer cantidadcuotas;
	private Double tasa;
	private Date primerVencimiento;
	private Integer periodicidadMeses;

	public RutinaSistemaFrances(Double capital, Integer cantidadcuotas, Double tasa, Date primerVencimiento, Integer periodicidadMeses) {
		this.capital = capital;
		this.cantidadcuotas = cantidadcuotas;
		this.tasa = tasa;
		this.primerVencimiento = primerVencimiento;
		this.setPeriodicidadMeses(periodicidadMeses);
	}

	public List<PlanPago> generar() throws Exception {
		List<PlanPago> ret = new ArrayList<PlanPago>();

		int p = getCantidadcuotas() * -1;
		double b = (1 + getTasa());
		double A = (1 - Math.pow(b, p)) / getTasa();

		double montocuota = getCapital() / A;
		//Redondeando a la decima
		BigDecimal bd = new BigDecimal(montocuota);
		montocuota = bd.setScale(-1, BigDecimal.ROUND_HALF_UP).doubleValue();
		//////
		System.out.println(montocuota);
		
		GregorianCalendar gcLastVencimiento = new GregorianCalendar();
		gcLastVencimiento.setTimeInMillis(getPrimerVencimiento().getTime());
		for (int i = 0; i < getCantidadcuotas(); i++) {
			PlanPago planPago = new PlanPago();
			planPago.setMonto(montocuota);
			planPago.setNroCuota((i + 1));
			planPago.setVencimiento(new Date(gcLastVencimiento.getTimeInMillis()));
			planPago.setEstado("A");
			gcLastVencimiento.add(Calendar.MONTH, getPeriodicidadMeses());
			ret.add(planPago);
		}
		return ret;
	}

	/**
	 * @return the primerVencimiento
	 */
	public Date getPrimerVencimiento() {
		return primerVencimiento;
	}

	/**
	 * @param primerVencimiento the primerVencimiento to set
	 */
	public void setPrimerVencimiento(Date primerVencimiento) {
		this.primerVencimiento = primerVencimiento;
	}

	public Integer getPeriodicidadMeses() {
		return periodicidadMeses;
	}

	public void setPeriodicidadMeses(Integer periodicidadMeses) {
		this.periodicidadMeses = periodicidadMeses;
	}

}
