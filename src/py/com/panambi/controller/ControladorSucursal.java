package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorSucursal extends ControladorPanambi {
	/**
	 * Inserta sucursal en la Base de Datos
	 * 
	 * @param conn
	 * @param sucursal
	 * @throws Exception
	 */
	public void insertarSucursal(Connection conn, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(sucursal);
			isDuplicated(conn, sucursal);
			String sql = "INSERT INTO sucursales ";
			sql += " (nombre, direccion, telefono, estado, principal ) ";
			sql += " VALUES (?, ?, ?, ?, ?) ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, sucursal.getNombre());
			ps.setString(2, sucursal.getDireccion());
			ps.setString(3, sucursal.getTelefono());
			ps.setString(4, sucursal.getEstado());
			ps.setBoolean(5, sucursal.getPrincipal());
			ps.executeUpdate();
			
			if(sucursal.getPrincipal()){
				establecerSucursalPrincipal(conn, sucursal);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Setea la sucursal como principal.
	 * @param conn
	 * @param sucursal
	 */
	private void establecerSucursalPrincipal(Connection conn, Sucursal sucursal) throws Exception{
		PreparedStatement psAllFalse = null;
		PreparedStatement psSetPrincipal = null;
		try {
			
			String sql = "UPDATE sucursales ";
			sql += " SET principal = FALSE ";
			
			psAllFalse = conn.prepareStatement(sql);
			psAllFalse.executeUpdate();
			
			sql = "UPDATE sucursales ";
			sql += " SET principal = TRUE ";
			sql += " WHERE  codsucursal = ? ";
			psSetPrincipal = conn.prepareStatement(sql);
			psSetPrincipal.setInt(1, sucursal.getCodSucursal());
			psSetPrincipal.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psAllFalse);
			ConnectionManager.closeStatments(psSetPrincipal);
		}
	}

	/**
	 * Elimina la sucursal de la base de datos
	 * 
	 * @param conn
	 * @param sucursal
	 * @throws Exception
	 */
	public void borrarSucursal(Connection conn, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(sucursal);
			String sql = "DELETE FROM sucursales ";
			sql += " WHERE codsucursal = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, sucursal.getCodSucursal());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Lista todas las sucursales desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Sucursal> getSucursales(Connection conn) throws Exception {
		List<Sucursal> sucursales = new ArrayList<Sucursal>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM sucursales ORDER BY nombre");
			rs = ps.executeQuery();
			while (rs.next()) {
				Sucursal sucursal = new Sucursal();
				sucursal.setCodSucursal(rs.getInt("codsucursal"));
				sucursal.setNombre(rs.getString("nombre"));
				sucursal.setDireccion(rs.getString("direccion"));
				sucursal.setTelefono(rs.getString("telefono"));
				sucursal.setEstado(rs.getString("estado"));
				sucursal.setPrincipal(rs.getBoolean("principal"));
				sucursales.add(sucursal);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return sucursales;
	}
	
	/**
	 * Obtiene todas las sucursales activas
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Sucursal> getSucursalesActivas(Connection conn) throws Exception {
		List<Sucursal> sucursales = new ArrayList<Sucursal>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM sucursales WHERE estado = 'A' ORDER BY nombre ");
			rs = ps.executeQuery();
			while (rs.next()) {
				Sucursal sucursal = new Sucursal();
				sucursal.setCodSucursal(rs.getInt("codsucursal"));
				sucursal.setNombre(rs.getString("nombre"));
				sucursal.setDireccion(rs.getString("direccion"));
				sucursal.setTelefono(rs.getString("telefono"));
				sucursal.setEstado(rs.getString("estado"));
				sucursal.setPrincipal(rs.getBoolean("principal"));
				sucursales.add(sucursal);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return sucursales;
	}

	/**
	 * 
	 * Devuelve la sucursal a partir del c�digo
	 * 
	 * @param conn
	 * @param codsucursal
	 * @return
	 * @throws Exception
	 */
	public Sucursal getSucursal(Connection conn, Integer codsucursal) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Sucursal ret = null;
		try {
			String sql = "SELECT * FROM sucursales WHERE codsucursal = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codsucursal);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Sucursal();
				ret.setCodSucursal(codsucursal);
				ret.setNombre(rs.getString("nombre"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setEstado(rs.getString("estado"));
				ret.setPrincipal(rs.getBoolean("principal"));
			} else {
				throw new ValidException("No existe la sucursal para el c�digo [" + codsucursal + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

	/**
	 * Devuelve Sucursal por Nombre
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public Sucursal getSucursal(Connection conn, String nombre) throws Exception {
		Sucursal ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM sucursales WHERE nombre = ?");
			ps.setString(1, nombre);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Sucursal();
				ret.setCodSucursal(rs.getInt("codsucursal"));
				ret.setNombre(rs.getString("nombre"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setEstado(rs.getString("estado"));
				ret.setPrincipal(rs.getBoolean("principal"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Actualiza datos de la sucursal
	 * 
	 * @param conn
	 * @param sucursal
	 * @throws Exception
	 */
	public void modificarSucursal(Connection conn, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(sucursal);
			checkValues(sucursal);
			isDuplicated(conn, sucursal);
			String sql = "UPDATE sucursales ";
			sql += " SET nombre = ?, ";
			sql += "direccion = ?, ";
			sql += "telefono = ? ,";
			sql += "estado = ?, ";
			sql += "principal = ? ";
			sql += " WHERE codsucursal = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, sucursal.getNombre());
			ps.setString(2, sucursal.getDireccion());
			ps.setString(3, sucursal.getTelefono());
			ps.setString(4, sucursal.getEstado());
			ps.setBoolean(5, sucursal.getPrincipal());
			ps.setInt(6, sucursal.getCodSucursal());
			ps.executeUpdate();
			
			if(sucursal.getPrincipal()){
				establecerSucursalPrincipal(conn, sucursal);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Verifica reglas en datos
	 * 
	 * @param sucursal
	 * @throws ValidException
	 */
	private void checkValues(Sucursal sucursal) throws ValidException {
		if (sucursal.getNombre() == null) {
			throw new ValidException("Debe ingresar nombre de sucursal.");
		}
		if (sucursal.getNombre().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre de sucursal.");
		}

	}

	/**
	 * Verifica que se haya seleccionado la sucursal
	 * 
	 * @param sucursal
	 * @throws ValidException
	 */
	public void checkSeleccion(Sucursal sucursal) throws ValidException {
		if (sucursal == null) {
			throw new ValidException("Debe seleccionar una sucursal.");
		}
		if (sucursal.getCodSucursal() == null) {
			throw new ValidException("Debe seleccionar una sucursal.");
		}
	}

	/**
	 * Verifica si existe en la base de datos una sucursal con el mismo nombre a
	 * la envia
	 * 
	 * @param conn
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Sucursal sucursal) throws Exception {
		boolean ret = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT COUNT(1) FROM sucursales ";
			sSelect += "WHERE nombre = ? ";
			if (sucursal.getCodSucursal() != null) {
				sSelect += " AND codsucursal <> ? ";
			}
			ps = conn.prepareStatement(sSelect);
			ps.setString(1, sucursal.getNombre());
			if (sucursal.getCodSucursal() != null) {
				ps.setInt(2, sucursal.getCodSucursal());
			}
			rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			if (cantidad > 0) {
				throw new ValidException("La sucursal " + sucursal.getNombre() + " ya existe.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	/**
	 * Retorna lista de sucursales excepto la sucursal origen
	 * @param conn
	 * @param sucursalOrigen
	 * @return lista de sucursales
	 * @throws Exception
	 */
	public List<Sucursal> getSucursalesDestino(Connection conn, String sucursalOrigen) throws Exception {
		List<Sucursal> sucursales = new ArrayList<Sucursal>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM sucursales where nombre != '"+sucursalOrigen+"' ORDER BY nombre");
			rs = ps.executeQuery();
			while (rs.next()) {
				Sucursal sucursal = new Sucursal();
				sucursal.setCodSucursal(rs.getInt("codsucursal"));
				sucursal.setNombre(rs.getString("nombre"));
				sucursal.setDireccion(rs.getString("direccion"));
				sucursal.setTelefono(rs.getString("telefono"));
				sucursal.setPrincipal(rs.getBoolean("principal"));
				sucursales.add(sucursal);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return sucursales;
	}
}
