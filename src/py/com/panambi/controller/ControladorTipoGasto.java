package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.TipoGasto;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorTipoGasto extends ControladorPanambi {
	
	public void insertarTipoGasto(Connection conn, TipoGasto tipoGasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(tipoGasto);
			isDuplicated(conn, tipoGasto);
			String sql = "INSERT INTO tiposgastos ";
			sql += " (concepto, estado ) ";
			sql += " VALUES (?, ? ) ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, tipoGasto.getConcepto());
			ps.setString(2, tipoGasto.getEstado());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public void borrarTipoGasto(Connection conn, TipoGasto tipoGasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(tipoGasto);
			String sql = "DELETE FROM tiposgastos ";
			sql += " WHERE codtipogasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, tipoGasto.getCodigo());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Obtiene todos los tipos de gastos.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<TipoGasto> getTiposGastos(Connection conn) throws Exception {
		List<TipoGasto> tiposGastos = new ArrayList<TipoGasto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM tiposgastos ORDER BY concepto");
			rs = ps.executeQuery();
			while (rs.next()) {
				TipoGasto tipoGasto = new TipoGasto();
				tipoGasto.setCodigo(rs.getInt("codtipogasto"));
				tipoGasto.setConcepto(rs.getString("concepto"));
				tipoGasto.setEstado(rs.getString("estado"));
				tiposGastos.add(tipoGasto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return tiposGastos;
	}
	
	/**
	 * Obtiene todos los tipos de gastos activos.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<TipoGasto> getTiposGastosActivos(Connection conn) throws Exception {
		List<TipoGasto> tiposGastos = new ArrayList<TipoGasto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM tiposgastos WHERE estado = 'A' ORDER BY concepto");
			rs = ps.executeQuery();
			while (rs.next()) {
				TipoGasto tipoGasto = new TipoGasto();
				tipoGasto.setCodigo(rs.getInt("codtipogasto"));
				tipoGasto.setConcepto(rs.getString("concepto"));
				tipoGasto.setEstado(rs.getString("estado"));
				tiposGastos.add(tipoGasto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return tiposGastos;
	}
	
	
	/**
	 * Obtiene un tipo de gasto por codigo de tipo de gasto.
	 * @param conn
	 * @param codtipogasto
	 * @return
	 * @throws Exception
	 */
	public TipoGasto getTipoGasto(Connection conn, Integer codtipogasto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipoGasto ret = null;
		try {
			String sql = "SELECT * FROM tiposgastos WHERE codtipogasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codtipogasto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new TipoGasto();
				ret.setCodigo(codtipogasto);
				ret.setConcepto(rs.getString("concepto"));
				ret.setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe el tipo de gasto para el c�digo [" + codtipogasto + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	public TipoGasto getTipoGasto(Connection conn, String concepto) throws Exception {
		TipoGasto ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM tiposgastos WHERE concepto = ?");
			ps.setString(1, concepto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new TipoGasto();
				ret.setCodigo(rs.getInt("codtipogasto"));
				ret.setConcepto(rs.getString("concepto"));
				ret.setEstado(rs.getString("estado"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	public void modificarTipoGasto(Connection conn, TipoGasto tipoGasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(tipoGasto);
			checkValues(tipoGasto);
			isDuplicated(conn, tipoGasto);
			String sql = "UPDATE tiposgastos ";
			sql += " SET concepto = ?, ";
			sql += "estado = ? ";
			sql += " WHERE codtipogasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, tipoGasto.getConcepto());
			ps.setString(2, tipoGasto.getEstado());
			ps.setInt(3, tipoGasto.getCodigo());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	private void checkValues(TipoGasto tipoGasto) throws ValidException {
		if (tipoGasto.getConcepto() == null) {
			throw new ValidException("Debe ingresar nombre del tipo de gasto.");
		}
		if (tipoGasto.getConcepto().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre del tipo de gasto.");
		}

	}
	
	public void checkSeleccion(TipoGasto tipoGasto) throws ValidException {
		if (tipoGasto == null) {
			throw new ValidException("Debe seleccionar un tipo de gasto.");
		}
		if (tipoGasto.getCodigo() == null) {
			throw new ValidException("Debe seleccionar un tipo de gasto.");
		}
	}
	
	private boolean isDuplicated(Connection conn, TipoGasto tipoGasto) throws Exception {
		boolean ret = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT COUNT(1) FROM tiposgastos ";
			sSelect += "WHERE concepto = ? ";
			if (tipoGasto.getCodigo() != null) {
				sSelect += " AND codtipogasto <> ? ";
			}
			ps = conn.prepareStatement(sSelect);
			ps.setString(1, tipoGasto.getConcepto());
			if (tipoGasto.getCodigo() != null) {
				ps.setInt(2, tipoGasto.getCodigo());
			}
			rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			if (cantidad > 0) {
				throw new ValidException(" El tipo de gasto" + tipoGasto.getConcepto() + " ya existe.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
}
