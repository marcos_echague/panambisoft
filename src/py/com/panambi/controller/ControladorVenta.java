package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.DetalleVenta;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Venta;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorVenta extends ControladorPanambi {

	/**
	 * Retorna una venta a partir del numero de comprobante
	 * 
	 * @param conn
	 *            Conexion a la base de datos
	 * @param nroComprobante
	 *            N�mero de factura
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaPorFactura(Connection conn, Integer nroComprobante) throws Exception {

		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codventa FROM ventas WHERE nrocomprobante = ? ");
			ps.setInt(1, nroComprobante);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Venta();
				ret = getVentaPorCodVenta(conn, rs.getInt("codventa"));
				// ret.setCodVenta(rs.getInt("codventa"));
				// ret.setNroComprobante(rs.getInt("nrocomprobante"));
				// ret.setFecha(rs.getDate("fecha"));
				// ret.setTotal(rs.getDouble("total"));
				// ret.setIva(rs.getDouble("iva"));
				// ret.setTipoVenta(rs.getString("tipoventa"));
				// ret.setTotalLetras(rs.getString("totalletras"));
				// ret.setEstado(rs.getString("estado"));
				// ret.setCliente(new
				// ControladorCliente().getCliente(JFramePanambiMain.session.getConn(),
				// rs.getInt("codcliente")));
				// ret.setUsuario(new
				// ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(),
				// rs.getInt("codusuario")));
				// ret.setSucursal(new
				// ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),
				// rs.getInt("codsucursal")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	public Venta getVentaPagadaPorFactura(Connection conn, Integer nroComprobante) throws Exception{

		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("SELECT * FROM ventas v, pagos p  WHERE v.codventa = p.codventa AND v.nrocomprobante = ? AND p.nrorecibo is not null ");
			ps.setInt(1, nroComprobante);
			rs = ps.executeQuery();
			
			if(rs.next()){
				ret= new Venta();
				ret = getVentaPorCodVenta(conn, rs.getInt("codventa"));
			}
		}catch(Exception e){
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	/**
	 * Obtiene todos los pagos relacionados a una venta a credito.
	 * 
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public List<Pago> getPagosVentaCredito(Connection conn, Venta venta) throws Exception {

		List<Pago> ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM pagos WHERE codventa = ? ");
			ps.setInt(1, venta.getCodVenta());
			rs = ps.executeQuery();

			ret = new ArrayList<Pago>();
			while (rs.next()) {
				Pago pago = new Pago();
				pago = new ControladorPago().getPago(conn, rs.getInt("codpago"));
				ret.add(pago);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Obtiene todos los pagos activos de una venta a cr�dito.
	 * 
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public List<Pago> getPagosActivosVentaCredito(Connection conn, Venta venta) throws Exception {

		List<Pago> ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM pagos WHERE estado = 'A' AND codventa = ? ");
			ps.setInt(1, venta.getCodVenta());
			rs = ps.executeQuery();

			ret = new ArrayList<Pago>();
			while (rs.next()) {
				Pago pago = new Pago();
				pago = new ControladorPago().getPago(conn, rs.getInt("codpago"));
				ret.add(pago);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Obtiene una venta a credito a partir de un numero de factura.
	 * 
	 * @param conn
	 * @param nroComprobante
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaCreditoPorFactura(Connection conn, Integer nroComprobante) throws Exception {

		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas WHERE nrocomprobante = ? AND tipoventa = 'C' ");
			ps.setInt(1, nroComprobante);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Venta();
				ret = getVentaPorCodVenta(conn, rs.getInt("codventa"));
				// ret.setCodVenta(rs.getInt("codventa"));
				// ret.setNroComprobante(rs.getInt("nrocomprobante"));
				// ret.setFecha(rs.getDate("fecha"));
				// ret.setTotal(rs.getDouble("total"));
				// ret.setIva(rs.getDouble("iva"));
				// ret.setTipoVenta(rs.getString("tipoventa"));
				// ret.setTotalLetras(rs.getString("totalletras"));
				// ret.setEstado(rs.getString("estado"));
				// ret.setCliente(new
				// ControladorCliente().getCliente(JFramePanambiMain.session.getConn(),
				// rs.getInt("codcliente")));
				// ret.setUsuario(new
				// ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(),
				// rs.getInt("codusuario")));
				// ret.setSucursal(new
				// ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),
				// rs.getInt("codsucursal")));

			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Obtiene una venta a credito con cuotas pendientes por pagar.
	 * 
	 * @param conn
	 * @param nroComprobante
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaCreditoPendientePorFactura(Connection conn, Integer nroComprobante) throws Exception {

		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas v " + " WHERE nrocomprobante = ? " + "AND tipoventa = 'C' " + "AND 0 NOT IN (SELECT count(*) FROM planespagos pp " + " WHERE pp.estado = 'D'" + " AND pp.codventa = v.codventa)");
			ps.setInt(1, nroComprobante);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Venta();
				ret = getVentaPorCodVenta(conn, rs.getInt("codventa"));
				// ret.setCodVenta(rs.getInt("codventa"));
				// ret.setNroComprobante(rs.getInt("nrocomprobante"));
				// ret.setFecha(rs.getDate("fecha"));
				// ret.setTotal(rs.getDouble("total"));
				// ret.setIva(rs.getDouble("iva"));
				// ret.setTipoVenta(rs.getString("tipoventa"));
				// ret.setTotalLetras(rs.getString("totalletras"));
				// ret.setEstado(rs.getString("estado"));
				// ret.setCliente(new
				// ControladorCliente().getCliente(JFramePanambiMain.session.getConn(),
				// rs.getInt("codcliente")));
				// ret.setUsuario(new
				// ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(),
				// rs.getInt("codusuario")));
				// ret.setSucursal(new
				// ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),
				// rs.getInt("codsucursal")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Devuelve una venta por el codigo de venta.
	 * 
	 * @param conn
	 * @param codVenta
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaPorCodVenta(Connection conn, Integer codVenta) throws Exception {
		
		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas WHERE codventa = ? ");
			ps.setInt(1, codVenta);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Venta();
				ret.setCodVenta(rs.getInt("codventa"));
				ret.setNroComprobante(rs.getInt("nrocomprobante"));
				ret.setFecha(rs.getTimestamp("fecha"));
				ret.setTotal(rs.getDouble("total"));
				ret.setIva(rs.getDouble("iva"));
				ret.setTipoVenta(rs.getString("tipoventa"));
				ret.setTotalLetras(rs.getString("totalletras"));
				ret.setEstado(rs.getString("estado"));
				ret.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				ret.setObservaciones(rs.getString("observaciones"));
				ret.setNroPagare(rs.getInt("nropagare"));
				ret.setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
				ret.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
				ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	
public Venta getVenta(Connection conn, Integer codVenta) throws Exception{
		
		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("SELECT * FROM ventas WHERE codventa = ? ");
			ps.setInt(1, codVenta);
			rs = ps.executeQuery();
			
			if(rs.next()){
				ret= new Venta();
				ret.setCodVenta(rs.getInt("codventa"));
				ret.setNroComprobante(rs.getInt("nrocomprobante"));
				ret.setFecha(rs.getTimestamp("fecha"));
				ret.setTotal(rs.getDouble("total"));
				ret.setIva(rs.getDouble("iva"));
				ret.setTipoVenta(rs.getString("tipoventa"));
				ret.setTotalLetras(rs.getString("totalletras"));
				ret.setEstado(rs.getString("estado"));
				ret.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				ret.setObservaciones(rs.getString("observaciones"));
				ret.setNroPagare(rs.getInt("nropagare"));
				ret.setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
				ret.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
				ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			}
		}catch(Exception e){
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	

	/**
	 * Obtiene el monto total factura en segun parametro introducidos.
	 * 
	 * @param conn
	 * @param cliente
	 * @param sucursal
	 * @param estado
	 * @param tipoVenta
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public Double getMontoFacturado(Connection conn, Cliente cliente, Sucursal sucursal, String estado, String tipoVenta, Date fechaDesde, Date fechaHasta) throws Exception {
		Double monto = 0.0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT SUM(total) AS suma FROM ventas ";
			sql += " WHERE estado ='A' " + "AND 1 = 1 ";
			if (cliente != null) {
				sql += " AND codcliente =  " + cliente.getCodCliente() + " ";
			}
			if (sucursal != null) {
				sql += " AND codsucursal =  " + sucursal.getCodSucursal() + " ";
			}

			if (tipoVenta != null) {
				if (tipoVenta.equals("C")) {
					sql += " AND tipoventa =  'C' ";
				} else if (tipoVenta.equals("D")) {
					sql += " AND tipoventa =  'D' ";
				}
			}

			if (estado != null) {
				if (estado.equals("A")) {
					sql += " AND estado =  'A' ";
				} else if (estado.equals("I")) {
					sql += " AND estado =  'I' ";
				}
			}

			if (fechaDesde != null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' ";
			} else if (fechaDesde == null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '2000-01-01' AND '" + fechaHasta + "' ";
			} else if (fechaDesde != null && fechaHasta == null) {
				sql += " AND fecha BETWEEN " + fechaDesde + "AND '2999-01-01' ";
			}

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				monto = rs.getDouble("suma");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return monto;
	}

	/**
	 * Obtiene el monto total decaudado segun los parametros ingresados.
	 * 
	 * @param conn
	 * @param cliente
	 * @param sucursal
	 * @param estado
	 * @param tipoVenta
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public Double getTotalReacudado(Connection conn, Cliente cliente, Sucursal sucursal, String estado, String tipoVenta, Date fechaDesde, Date fechaHasta) throws Exception {
		Double monto = 0.0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT SUM(p.monto) AS suma FROM pagos p, ventas v ";
			sql += " WHERE v.codventa = p.codventa ";
			sql += " AND p.codventa is not null ";
			sql += " AND p.estado ='A' " + "AND 1 = 1 ";
			if (cliente != null) {
				sql += " AND v.codcliente =  " + cliente.getCodCliente() + " ";
			}
			if (sucursal != null) {
				sql += " AND v.codsucursal =  " + sucursal.getCodSucursal() + " ";
			}

			if (tipoVenta != null) {
				if (tipoVenta.equals("C")) {
					sql += " AND tipoventa =  'C' ";
				} else if (tipoVenta.equals("D")) {
					sql += " AND tipoventa =  'D' ";
				}
			}

			if (estado != null) {
				if (estado.equals("A")) {
					sql += " AND v.estado =  'A' ";
				} else if (estado.equals("I")) {
					sql += " AND v.estado =  'I' ";
				}
			}

			if (fechaDesde != null && fechaHasta != null) {
				sql += " AND v.fecha BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' ";
			} else if (fechaDesde == null && fechaHasta != null) {
				sql += " AND v.fecha BETWEEN '2000-01-01' AND '" + fechaHasta + "' ";
			} else if (fechaDesde != null && fechaHasta == null) {
				sql += " AND v.fecha BETWEEN " + fechaDesde + "AND '2999-01-01' ";
			}

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				monto = rs.getDouble("suma");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return monto;
	}

	/**
	 * Devulve las ventas segun los parametros ingresados.
	 * 
	 * @param conn
	 * @param cliente
	 * @param sucursal
	 * @param estado
	 * @param tipoVenta
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<Venta> getVentas(Connection conn, Cliente cliente, Sucursal sucursal, String estado, String tipoVenta, Date fechaDesde, Date fechaHasta) throws Exception {
		List<Venta> ventas = new ArrayList<Venta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT * FROM ventas ";
			sql += " WHERE 1 = 1 ";
			if (cliente != null) {
				sql += " AND codcliente =  " + cliente.getCodCliente() + " ";
			}

			if (sucursal != null) {
				sql += " AND codsucursal =  " + sucursal.getCodSucursal() + " ";
			}

			if (tipoVenta != null) {
				if (tipoVenta.equals("D")) {
					sql += " AND tipoventa = 'D' ";
				} else if (tipoVenta.equals("C")) {
					sql += " AND tipoventa = 'C' ";
				}
			}

			if (estado != null) {
				if (estado.equals("A")) {
					sql += " AND estado = 'A' ";
				} else
					sql += " AND estado = 'I' ";
			}

			if (fechaDesde != null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' ";
			} else if (fechaDesde == null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '2000-01-01' AND '" + fechaHasta + "' ";
			} else if (fechaDesde != null && fechaHasta == null) {
				sql += " AND fecha BETWEEN " + fechaDesde + "AND '2999-01-01' ";
			}

			sql += " ORDER BY fecha DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Venta venta = new Venta();
				venta = getVentaPorCodVenta(conn, rs.getInt("codventa"));
				ventas.add(venta);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ventas;
	}

	/**
	 * Retorna una venta a credito, a partir del numero de comprobante.
	 * 
	 * @param conn
	 *            Conexi�n a la base de datos
	 * @param nroComprobante
	 *            N�mero de factura
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaCredito(Connection conn, Integer nroComprobante) throws Exception {

		Venta ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas WHERE nrocomprobante = ? AND tipoventa = 'C'");
			ps.setInt(1, nroComprobante);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Venta();
				ret = getVentaPorCodVenta(conn, rs.getInt("codventa"));
				// ret.setCodVenta(rs.getInt("codventa"));
				// ret.setNroComprobante(rs.getInt("nrocomprobante"));
				// ret.setFecha(rs.getDate("fecha"));
				// ret.setTotal(rs.getDouble("total"));
				// ret.setIva(rs.getDouble("iva"));
				// ret.setTipoVenta(rs.getString("tipoventa"));
				// ret.setTotalLetras(rs.getString("totalletras"));
				// ret.setEstado(rs.getString("estado"));
				// ret.setCliente(new
				// ControladorCliente().getCliente(JFramePanambiMain.session.getConn(),
				// rs.getInt("codcliente")));
				// ret.setUsuario(new
				// ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(),
				// rs.getInt("codusuario")));
				// ret.setSucursal(new
				// ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(),
				// rs.getInt("codsucursal")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Devuelve el detalle de las ventas, a partir de una venta.
	 * 
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public List<DetalleVenta> getDetalleVenta(Connection conn, Venta venta) throws Exception {
		List<DetalleVenta> detallesVenta = new ArrayList<DetalleVenta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM detalleventas " + "WHERE codventa = ? " + "ORDER BY nroitem ");
			ps.setInt(1, venta.getCodVenta());
			rs = ps.executeQuery();

			while (rs.next()) {

				DetalleVenta detalleVenta = new DetalleVenta();
				detalleVenta.setNroitem(rs.getInt("nroitem"));
				detalleVenta.setCantidad(rs.getInt("cantidad"));
				detalleVenta.setPrecioUnitario(rs.getDouble("preciounitario"));
				detalleVenta.setDescuentoUnitario(rs.getDouble("descuentounitario"));
				detalleVenta.setTotalItem(rs.getDouble("totalitem"));
				detalleVenta.setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
				detalleVenta.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
				detallesVenta.add(detalleVenta);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return detallesVenta;

	}

	/**
	 * Retorna el numero de pagare a ingresar.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Integer getNextNumeroPagare(Connection conn ) throws Exception{
		Integer numero = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT MAX(nropagare) maximo FROM ventas ");
			rs = ps.executeQuery();
			
			if (rs.next()) {
				numero = rs.getInt("maximo");
			}
			if(numero == null){
				numero = 0;
			}
			numero = numero+1;
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return numero;

	}
	
	/**
	 * Anula venta
	 * 
	 * @param conn
	 * @param venta
	 * @throws Exception
	 */
	public void anularVentaCredito(Connection conn, Venta venta) throws Exception {
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE ventas ";
			sql += " SET estado = 'I' , " + "  fechaanulacion = current_timestamp, " + "  observaciones = ? ";
			sql += " WHERE codventa = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, venta.getObservaciones());
			ps.setInt(2, venta.getCodVenta());
			ps.executeUpdate();

			List<DetalleVenta> detalle = getDetalleVenta(conn, venta);
			for (DetalleVenta detalleVenta : detalle) {
				// ***Reponer cantidad en Stock
				repToStock(conn, detalleVenta.getProducto(), detalleVenta.getCantidad(), venta.getSucursal());

			}

//			Pago pago = getPagoDeVenta(conn, venta);
//			
//			// ***Devuelta activa a la nota de credito utilizada.
//			for (DetallePago detallepago : pago.getDetallePago()) {
//				if (detallepago.getFormaPago().getNombre().equals("NOTA DE CREDITO")) {
//					new ControladorDevolucion().setNotaActiva(conn, detallepago.getNotacredito());
//				}
//			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Anula una venta contado, tambien anula el pago asociado a la venta.
	 * 
	 * @param conn
	 * @param venta
	 * @throws Exception
	 */
	public void anularVentaContado(Connection conn, Venta venta) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE ventas ";
			sql += " SET estado = 'I' , " + "  fechaanulacion = current_timestamp, " + "  observaciones = ? ";
			sql += " WHERE codventa = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, venta.getObservaciones());
			ps.setInt(2, venta.getCodVenta());
			ps.executeUpdate();

			List<DetalleVenta> detalle = getDetalleVenta(conn, venta);
			for (DetalleVenta detalleVenta : detalle) {
				// ***Reponer cantidad en Stock
				repToStock(conn, detalleVenta.getProducto(), detalleVenta.getCantidad(), venta.getSucursal());

			}

			// ***Se anula el pago correspontiente a la venta
			Pago pago = getPagoDeVenta(conn, venta);
			for (DetallePago detallepago : pago.getDetallePago()) {
				if(detallepago.getFormaPago().getNombre().equals("NOTA DE CREDITO")){
					new ControladorDevolucion().setNotaActiva(conn, detallepago.getNotacredito());
				}
			}
			pago.setObservaciones(venta.getObservaciones());
			anularPago(conn, pago);
			conn.commit();

		} catch (Exception e) {
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}

	private void anularPago(Connection conn, Pago pago) throws Exception {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I' , " + "  fechaanulacion = current_timestamp, " + "  observaciones = ? ";
			sql += " WHERE codpago = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, pago.getObservaciones());
			ps.setInt(2, pago.getCodPago());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Obtiene el pago de una venta con el detalle inclu�do.
	 * 
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public Pago getPagoDeVenta(Connection conn, Venta venta) throws Exception {
		Pago pago = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE codventa = ? ");
		// PreparedStatement psDetalles = null;
		// String sqlDetalle = "SELECT * FROM detallepagos WHERE codpago = ";

		ps.setInt(1, venta.getCodVenta());
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			pago = new Pago();
			pago = new ControladorPago().getPago(conn, rs.getInt("codpago"));
			// pago .setCodPago(rs.getInt("codpago"));
			// pago .setMonto(rs.getDouble("monto"));
			// pago .setFecha(rs.getTimestamp("fecha"));
			// pago .setNroRecibo(rs.getInt("nrorecibo"));
			// pago .setObservaciones(rs.getString("observaciones"));
			// pago .setEstado(rs.getString("estado"));
			// if(rs.getObject("codventa")!=null){
			// pago .setVenta(new ControladorVenta().getVentaPorCodVenta(conn,
			// rs.getInt("codventa")));
			// }else pago .setVenta(null);
			//
			// if(rs.getObject("codventa")!=null){
			// pago .setVenta(new ControladorVenta().getVentaPorCodVenta(conn,
			// rs.getInt("codventa")));
			// } else pago .setVenta(null);
			//
			// if(rs.getObject("codgasto")!=null){
			// pago .setGasto(new ControladorGasto().getGasto(conn,
			// rs.getInt("codgasto")));
			// }else pago .setGasto(null);
			//
			// //pago .setCompra(new ControladorCompra().getCompra(conn,
			// rs.getInt("codcompra")));
			//
			// //*** Cargando detalles
			// sqlDetalle+=" "+pago.getCodPago();
			// psDetalles = conn.prepareStatement(sqlDetalle);
			// List<DetallePago> detalles = new ArrayList<DetallePago>();
			// ResultSet rsDetalles = psDetalles.executeQuery();
			// while (rsDetalles.next()) {
			// DetallePago detalle = new DetallePago();
			// detalle.setNroitem(rsDetalles.getInt("nroitem"));
			// detalle.setMonto(rsDetalles.getDouble("monto"));
			// detalle.setBanco(new ControladorBanco().getBanco(conn,
			// rsDetalles.getInt("codbanco")));
			// detalle.setFormaPago(new
			// ControladorFormaPago().getFormaPago(conn,rsDetalles.getInt("codformapago")));
			// detalle.setCodpago(rsDetalles.getInt("codpago"));
			// detalle.setChequenro(rsDetalles.getString("chequenro"));
			// detalle.setBouchernro(rsDetalles.getString("bouchernro"));
			// detalle.setNotacredito(rsDetalles.getInt("notacredito"));
			// detalles.add(detalle);
			// }
			//
			// pago. setDetallePago(detalles);
		}
		rs.close();
		ps.close();
		return pago;
	}

	/**
	 * Repone stock de que se encuentran en venta anulada.
	 * 
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void repToStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") + cantidad);
				rsSelUpdateStock.updateRow();
			}
			rsSelUpdateStock.close();

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}

	/**
	 * 
	 * @param conn
	 * @param pago
	 * @return
	 * @throws Exception
	 */
	public Integer registrarVenta(Connection conn, Pago pago) throws Exception {
		Integer codventa = null;
		try {
			conn.setAutoCommit(false);
			ControladorPago controladorPago = new ControladorPago();
			codventa = insertarVenta(conn, pago.getVenta(), false);
			pago.getVenta().setCodVenta(codventa);
			controladorPago.registrarPago(conn, pago);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
		return codventa;
	}
	
	/**
	 * 
	 * @param conn
	 * @param pago
	 * @return
	 * @throws Exception
	 */
	public Integer registrarVenta(Connection conn, Venta venta, List<PlanPago> planespago) throws Exception {
		Integer codventa = null;
		try {
			conn.setAutoCommit(false);
			ControladorPlanPago controladorPlanPago = new ControladorPlanPago();
			codventa = insertarVenta(conn, venta, false);
			venta.setCodVenta(codventa);
			controladorPlanPago.insertarPlanesPago(conn, venta, planespago);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
		return codventa;
	}


	/**
	 * Insert una venta con su detalle
	 * 
	 * @param conn
	 * @param venga
	 * @return
	 * @throws Exception
	 */
	public Integer insertarVenta(Connection conn, Venta venta, Boolean transaccionUnica) throws Exception {
		Integer codventa = null;
		String sSelMax = "select max(nrocomprobante) from ventas";
		PreparedStatement psMax = null;
		
		String sInsCab = "INSERT INTO ventas( ";
		sInsCab += "total, iva, tipoventa, totalletras, codcliente, ";
		sInsCab += "codusuario, codsucursal, nrocomprobante, estado, observaciones, nropagare) ";
		sInsCab += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement psCab = null;
		PreparedStatement psDet = null;
		Integer nrocomprobante = 1; 
		try {
			if (transaccionUnica) {
				conn.setAutoCommit(false);
			}
			psMax = conn.prepareStatement(sSelMax);
			ResultSet rsMax = psMax.executeQuery();
			if (rsMax.next()) {
				nrocomprobante = rsMax.getInt(1);
				nrocomprobante++;
			}
			rsMax.close();
			
			psCab = conn.prepareStatement(sInsCab, Statement.RETURN_GENERATED_KEYS);
			psCab.setDouble(1, venta.getTotal());
			psCab.setDouble(2, Math.round(venta.getTotal() / 11));
			psCab.setString(3, venta.getTipoVenta());
			psCab.setString(4, venta.getTotalLetras());
			psCab.setInt(5, venta.getCliente().getCodCliente());
			psCab.setInt(6, venta.getUsuario().getCodUsuario());
			psCab.setInt(7, venta.getSucursal().getCodSucursal());
			psCab.setInt(8, nrocomprobante);
			psCab.setString(9, "A");
			psCab.setString(10, venta.getObservaciones());
			if(venta.getTipoVenta().equals("C")){
				Integer nroPagare = new ControladorPlanPago().getNroPagare(conn);
				psCab.setInt(11, nroPagare);
			}else{
				psCab.setNull(11, Types.INTEGER);
			}
			psCab.executeUpdate();
			ResultSet rsKey = psCab.getGeneratedKeys();
			if (rsKey.next()) {
				codventa = rsKey.getInt(1);
			}
			rsKey.close();

			String sInsDetalle = "INSERT INTO detalleventas( ";
			sInsDetalle += "nroitem, cantidad, preciounitario, descuentounitario, totalitem, ";
			sInsDetalle += "codventa, codproducto) ";
			sInsDetalle += "VALUES (?, ?, ?, ?, ?, ";
			sInsDetalle += "?, ?) ";
			psDet = conn.prepareStatement(sInsDetalle);
			List<DetalleVenta> detalle = venta.getDetalle();
			int i = 0;
			for (DetalleVenta detalleVenta : detalle) {
				i++;
				psDet.setInt(1, i);
				psDet.setInt(2, detalleVenta.getCantidad());
				psDet.setDouble(3, detalleVenta.getPrecioUnitario());
				psDet.setDouble(4, detalleVenta.getDescuentoUnitario());
				psDet.setDouble(5, detalleVenta.getTotalItem());
				psDet.setInt(6, codventa);
				psDet.setInt(7, detalleVenta.getProducto().getCodProducto());
				psDet.executeUpdate();
				updateStock(conn, detalleVenta.getProducto(), detalleVenta.getCantidad(), venta.getSucursal());
			}
			psDet.close();
			if (transaccionUnica) {
				conn.commit();
			}
		} catch (Exception e) {
			if (transaccionUnica) {
				conn.rollback();
			}
			throw e;
		} finally {
			ConnectionManager.closeStatments(psCab, psDet, psMax);
			if (transaccionUnica) {
				conn.setAutoCommit(true);
			}
		}
		return codventa;
	}
	
	/**
	 * Actualiza la cantidad del producto en el stock por sucursal
	 * 
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void updateStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			if (rsSelUpdateStock.next()) {
				if (rsSelUpdateStock.getInt("cantidad") - cantidad < 0) {
					throw new ValidException("Stock agotado para " + cantidad + " " + producto.getDescripcion());
				}
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") - cantidad);
				rsSelUpdateStock.updateRow();
			}
			rsSelUpdateStock.close();

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}

}
