package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Producto;
import py.com.panambi.bean.StockProducto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;

public class ControladorStockProducto extends ControladorPanambi {

	/**
	 * Obtiene la cantidad de productos que existen en una sucusal.
	 * @param conn
	 * @param producto
	 * @param sucursal
	 * @return
	 */
	
	public Integer getCantidad(Connection conn, Producto producto, Sucursal sucursal) throws Exception{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer ret;
		try{
			String sql = "SELECT cantidad "
					+ "FROM stockproductos "
					+ "WHERE codproducto_productos = ? "
					+ "AND codsucursal_sucursales = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, producto.getCodProducto());
			ps.setInt(2, sucursal.getCodSucursal());
			rs = ps.executeQuery();
			
			if(rs.next()){
				ret = rs.getInt("cantidad");
			}else{
				ret = 0;
			}
		}catch(Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	
	/**
	 * Retorna el stock de productos segun los parametros ingresados.
	 * @param conn
	 * @param producto
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	public List<StockProducto> consultarStock(Connection conn, Producto producto, Sucursal sucursal) throws Exception{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StockProducto> ret = new ArrayList<StockProducto>();;
		try{
			String sql = "SELECT * "
					+ "FROM stockproductos "
					+ "WHERE 1=1 ";
			
			if(producto!=null){
				sql+="AND codproducto_productos = ? ";
			}
			
			if(sucursal!=null){
				sql+= "AND codsucursal_sucursales = ? ";
			}
					
			ps = conn.prepareStatement(sql);
			
			Integer indice = 1;
			if(producto!=null){
				ps.setInt(indice, producto.getCodProducto());
				indice = indice +1;
			}
			
			if(sucursal!=null){
				ps.setInt(indice, sucursal.getCodSucursal());
			}
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				StockProducto stock = new StockProducto();
				stock.setProducto(new ControladorProducto().getProducto(conn, rs.getInt("codproducto_productos")));
				stock.setSucursal(new ControladorSucursal().getSucursal(conn,rs.getInt("codsucursal_sucursales")));
				stock.setCantidad(rs.getInt("cantidad"));
				
				ret.add(stock);
			}
		}catch(Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	
	/**
	 * Actualiza el stock de productos
	 * @param conn
	 * @param producto
	 * @param sucursal
	 * @param cantidad
	 * @throws Exception
	 */
	public void actualizarStock(Connection conn , Producto producto, Sucursal sucursal, Integer cantidad) throws Exception{
		PreparedStatement ps = null;
		
		try{
			String sql = "UPDATE stockproductos "
					+ "SET cantidad = ? "
					+ "WHERE codproducto_productos = ? "
					+ "AND codsucursal_sucursales = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, cantidad);
			ps.setInt(2, producto.getCodProducto());
			ps.setInt(3, sucursal.getCodSucursal());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}	
}
