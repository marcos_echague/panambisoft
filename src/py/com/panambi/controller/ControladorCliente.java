package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Cliente;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorCliente extends ControladorPanambi {
	
	/**
	 * Registra un nuevo cliente en la base de datos.
	 * @param conn
	 * @param cliente
	 * @throws Exception
	 */
	public void insertarCliente(Connection conn, Cliente cliente) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(cliente);
			isDuplicated(conn, cliente);
			String sql = "INSERT INTO clientes ";
			sql += " (nrodocumento, nombres, apellidos, telefono, email, direccion, fechaingreso, estado ) ";
			sql += " VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, cliente.getNroDocumento());
			ps.setString(2, cliente.getNombres());
			ps.setString(3, cliente.getApellidos());
			ps.setString(4, cliente.getTelefono());
			ps.setString(5, cliente.getEmail());
			ps.setString(6, cliente.getDireccion());
			ps.setDate(7, cliente.getFechaIngreso());
			ps.setString(8, cliente.getEstado());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	
	
	
	/**
	 * Borra un cliente de la base de tados
	 * @param conn
	 * @param cliente
	 * @throws Exception
	 */
	public void borrarCliente(Connection conn, Cliente cliente) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(cliente);
			String sql = "DELETE FROM clientes ";
			sql += " WHERE codcliente = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, cliente.getCodCliente());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Lista todos los clientes desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Cliente> getClientes(Connection conn) throws Exception {
		List<Cliente> clientes = new ArrayList<Cliente>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM clientes ORDER BY nrodocumento");
			rs = ps.executeQuery();
			while (rs.next()) {
				Cliente cliente= new Cliente();
				cliente.setCodCliente(rs.getInt("codcliente"));
				cliente.setNroDocumento(rs.getString("nrodocumento"));
				cliente.setNombres(rs.getString("nombres"));
				cliente.setApellidos(rs.getString("apellidos"));
				cliente.setTelefono(rs.getString("telefono"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setEmail(rs.getString("email"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setFechaIngreso(rs.getDate("fechaingreso"));
				cliente.setEstado(rs.getString("estado"));
				clientes.add(cliente);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return clientes;
	}
	
	/**
	 * Obtiene cantidad de cuotas pendientes por pagar que tiene un cliente.
	 * @param conn
	 * @param cliente
	 * @return
	 * @throws Exception
	 */
	public Integer getCantidadCuotasPendientes(Connection conn, Cliente cliente) throws Exception {

		Integer cantidadCuotasPendientes = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT count(*) AS cantidad FROM ventas v, planespagos pp , clientes c ";
			sql+= " WHERE v.codventa = pp.codventa"
					+ "AND c.codcliente = v.codcliente"
					+ "AND v.estado = 'A'"
					+ "AND pp.estado = 'D' "
					+ "AND codcliente = ? ";	
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, cliente.getCodCliente());
			rs = ps.executeQuery();
			if (rs.next()) {
				cantidadCuotasPendientes = rs.getInt("cantidad");
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return cantidadCuotasPendientes;
	}
	
	/**
	 * 
	 * Devuelve un cliente a partir del numero de c�digo
	 * 
	 * @param conn
	 * @param codcliente
	 * @return
	 * @throws Exception
	 */
	public Cliente getCliente(Connection conn, Integer codcliente) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cliente ret = null;
		try {
			String sql = "SELECT * FROM clientes WHERE codcliente = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codcliente);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Cliente();
				ret.setCodCliente(codcliente);
				ret.setNroDocumento(rs.getString("nrodocumento"));
				ret.setNombres(rs.getString("nombres"));
				ret.setApellidos(rs.getString("apellidos"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setEmail(rs.getString("email"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setFechaIngreso(rs.getDate("fechaingreso"));
				ret.setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe el cliente para el c�digo [" + codcliente + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

	/**
	 * Devuelve Cliente por Numero de documento
	 * 
	 * @param conn
	 * @param nrodocumento
	 * @return
	 */
	public Cliente getCliente(Connection conn, String nrodocumento) throws Exception {
		Cliente ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM clientes WHERE nrodocumento = ?");
			ps.setString(1, nrodocumento);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Cliente();
				ret.setCodCliente(rs.getInt("codcliente"));
				ret.setNroDocumento(rs.getString("nrodocumento"));
				ret.setNombres(rs.getString("nombres"));
				ret.setApellidos(rs.getString("apellidos"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setEmail(rs.getString("email"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setFechaIngreso(rs.getDate("fechaingreso"));
				ret.setEstado(rs.getString("estado"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}
	

	/**
	 * Actualiza datos del cliente
	 * 
	 * @param conn
	 * @param cliente
	 * @throws Exception
	 */
	public void modificarCliente(Connection conn, Cliente cliente) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(cliente);
			checkValues(cliente);
			isDuplicated(conn, cliente);
			String sql = "UPDATE clientes ";
			sql += " SET nrodocumento = ?, ";
			sql += "nombres = ?, ";
			sql += "apellidos = ?, ";
			sql += "telefono = ?, ";
			sql += "email = ?, ";
			sql += "direccion = ?, ";
			sql += "fechaingreso = ?, ";
			sql += "estado = ? ";
			sql += " WHERE codcliente = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, cliente.getNroDocumento());
			ps.setString(2, cliente.getNombres());
			ps.setString(3, cliente.getApellidos());
			ps.setString(4, cliente.getTelefono());
			ps.setString(5, cliente.getEmail());
			ps.setString(6, cliente.getDireccion());
			ps.setDate(7, cliente.getFechaIngreso());
			ps.setString(8, cliente.getEstado());
			ps.setInt(9, cliente.getCodCliente());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	
	/**
	 * Verifica reglas en datos
	 * 
	 * @param cliente
	 * @throws ValidException
	 */
	private void checkValues(Cliente cliente) throws ValidException {
		if (cliente.getNroDocumento() == null) {
			throw new ValidException("Debe ingresar el numero de documento del cliente.");
		}
		if (cliente.getNroDocumento().trim().isEmpty()) {
			throw new ValidException("Debe ingresar el numero de documento del cliente.");
		}

	}

	/**
	 * Verifica que se haya seleccionado el cliente
	 * 
	 * @param cliente
	 * @throws ValidException
	 */
	private void checkSeleccion(Cliente cliente) throws ValidException {
		if (cliente == null) {
			throw new ValidException("Debe seleccionar un cliente.");
		}
		if (cliente.getCodCliente() == null) {
			throw new ValidException("Debe seleccionar un cliente.");
		}
	}

	/**
	 * Verifica si existe en la base de datos un cliente con el mismo numero de documento
	 * 
	 * @param conn
	 * @param cliente
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Cliente cliente) throws Exception {
		boolean ret = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT COUNT(1) FROM clientes ";
			sSelect += "WHERE nrodocumento = ? ";
			if (cliente.getCodCliente() != null) {
				sSelect += " AND codcliente <> ? ";
			}
			ps = conn.prepareStatement(sSelect);
			ps.setString(1, cliente.getNroDocumento());
			
			if (cliente.getCodCliente() != null) {
				ps.setInt(2, cliente.getCodCliente());
			}
			
			rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			if (cantidad > 0) {
				throw new ValidException("El cliente con nro de documento " + cliente.getNroDocumento() + " ya existe.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

}
