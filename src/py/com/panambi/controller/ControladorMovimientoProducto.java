package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import py.com.panambi.bean.DetalleMovimientoProducto;
import py.com.panambi.bean.MovimientoProducto;
import py.com.panambi.bean.Producto;
//import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorMovimientoProducto {
	
	/**
	 * Obtiene un movimiento de producto a partir de su numero de nota de remision.
	 * @param conn
	 * @param codnotaRemision
	 * @return
	 * @throws Exception
	 */
	public MovimientoProducto getMovimientoProductoPorNotaRemision(Connection conn, Integer codnotaRemision) throws Exception {
		MovimientoProducto movimientoProducto = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codmovimientoproducto FROM movimientosproductos WHERE notaremision = ? ");
		ps.setInt(1, codnotaRemision);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			movimientoProducto = getMovimientoProducto(conn, rs.getInt("codmovimientoproducto"));
		}
		return movimientoProducto;
	}
	
	public MovimientoProducto getMovimientoProducto(Connection conn, Integer codmovimientoproducto) throws Exception {
		MovimientoProducto movimientoProducto = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM movimientosproductos WHERE codmovimientoproducto = ? ");		
		PreparedStatement psDetalles = conn.prepareStatement("SELECT * FROM detallemovimientos WHERE codmovimientoproducto = "+codmovimientoproducto+"");
		
		ps.setInt(1, codmovimientoproducto);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			movimientoProducto = new MovimientoProducto();
			movimientoProducto.setCodMovimientoProducto(codmovimientoproducto);
			movimientoProducto.setFecha(rs.getDate("fecha"));
			movimientoProducto.setComentarios(rs.getString("comentarios"));
			movimientoProducto.setCodSucursalOrigen(rs.getInt("codsucursal_origen"));
			movimientoProducto.setCodSucursalDestino(rs.getInt("codsucursal_destino"));
			movimientoProducto.setEstado(rs.getString("estado"));
			movimientoProducto.setNotaRemision(rs.getInt("notaremision"));
			
			// Cargando detalles
			List<DetalleMovimientoProducto> detalles = new ArrayList<DetalleMovimientoProducto>();
			ResultSet rsDetalles = psDetalles.executeQuery();
			while (rsDetalles.next()) {
				detalles.add(getDetalleMovimientoProducto(conn, rsDetalles.getInt("codmovimientoproducto"), rsDetalles.getInt("codproducto")));
			}
			movimientoProducto.setDetalleMovimiento(detalles);
		}
		rs.close();
		ps.close();
		return movimientoProducto;
	}
	
	public Producto getProducto(Connection conn, Integer codproducto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Producto ret = null;
		try {
			String sql = "SELECT codproducto, descripcion FROM productos WHERE codproducto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codproducto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Producto();
				ret.setCodProducto(codproducto);
				ret.setDescripcion(rs.getString("descripcion"));
				
			} else {
				throw new ValidException("No existe el producto para el c�digo [" + codproducto + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	public DetalleMovimientoProducto getDetalleMovimientoProducto(Connection conn, Integer codMovimiento, Integer codProducto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DetalleMovimientoProducto detalle = null;
		try {
			String sql = "SELECT nroitem, cantidad, codmovimientoproducto, codproducto FROM detallemovimientos WHERE codmovimientoproducto = ? and codproducto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codMovimiento);
			ps.setInt(2, codProducto);
			rs = ps.executeQuery();
			if (rs.next()) {
				detalle = new DetalleMovimientoProducto();
				detalle.setNroitem(rs.getInt("nroitem"));
				detalle.setCantidad(rs.getInt("cantidad"));
				detalle.setCodMovimientoProducto(rs.getInt("codmovimientoproducto"));
				detalle.setCodProducto(rs.getInt("codproducto"));
			} else {
				throw new ValidException("No existe detalle para el movimiento [" + codMovimiento + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return detalle;
	}
	
	/**
	 * Lista todas los productos desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Producto> getProductosToMove(Connection conn) throws Exception {
		List<Producto> productos = new ArrayList<Producto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codproducto, descripcion, precio FROM productos");
			rs = ps.executeQuery();
			while (rs.next()) {
				Producto producto = new Producto();
				producto.setCodProducto(rs.getInt("codproducto"));
				producto.setDescripcion(rs.getString("descipcion"));
				producto.setPrecio(rs.getInt("precio"));
				productos.add(producto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return productos;
	}
	
	/**
	 * Obtiene el numero de nota de remision a utilizar para el movimiento de productos.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Integer getNextNumberNotaRemision(Connection conn) throws Exception{
		Integer ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSelect = "SELECT max(notaremision) AS maximo FROM movimientosproductos ";
		ps = conn.prepareStatement(sSelect);
		rs = ps.executeQuery();
		
		if (rs.next()) {
			ret = rs.getInt("maximo");
		}
		
		if(ret == null){
			ret = 0;
		}
		
		ret=ret+1;
		return ret;
		
	}
	public Integer getCantidadProductoSucursal(Connection conn, Integer codProducto, String nombreSucursal) throws Exception{
		int cantidad = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if(nombreSucursal != ""){
				String sSelect = "SELECT codsucursal FROM sucursales ";
				sSelect += "WHERE nombre = ? ";
				ps = conn.prepareStatement(sSelect);
				ps.setString(1, nombreSucursal);
				rs = ps.executeQuery();
				
				int codSucursal=0;
				if (rs.next()) {
					codSucursal = rs.getInt(1);
				}
				
				sSelect = "SELECT cantidad FROM stockproductos ";
				sSelect += "WHERE codsucursal_sucursales = ? ";
				sSelect += " AND codproducto_productos =  ? ";

				ps1 = conn.prepareStatement(sSelect);
				ps1.setInt(1, codSucursal);
				ps1.setInt(2, codProducto);
				rs1 = ps1.executeQuery();
				
				if (rs1.next()) {
					cantidad = rs1.getInt(1);
				}
				
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
			ConnectionManager.closeResultSets(rs1);
			ConnectionManager.closeStatments(ps1);
		}
		return cantidad;
	}
	
	public void insertarMovimientoProducto(Connection conn, MovimientoProducto movimientoProducto) throws Exception{
		
		ResultSet rs = null;
		//ResultSet rs1 = null;
		ResultSet rs2 = null;
		//ResultSet rs3 = null;
		//ResultSet rs5 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		try {
				conn.setAutoCommit(false);
			    
				String sSelectId = "SELECT NEXTVAL('movimientosproductos_codmovimientoproducto_seq')";
				
				ps = conn.prepareStatement(sSelectId);
				rs = ps.executeQuery();
				int codMovimientoProducto=0;
				if (rs.next()) {
					codMovimientoProducto = rs.getInt(1);
				}
				Date currentDate = movimientoProducto.getFecha();
				String  sInsert = "INSERT INTO movimientosproductos ";
						sInsert += "(codmovimientoproducto, fecha, comentarios, codsucursal_origen, ";
						sInsert += "codsucursal_destino, estado, notaremision) VALUES (?, ?, ?, ?, ?, ?, ?)";
				ps1 = conn.prepareStatement(sInsert);
				ps1.setInt(1, codMovimientoProducto);
				ps1.setDate(2, (java.sql.Date) currentDate);
				ps1.setString(3, movimientoProducto.getComentarios());
				ps1.setInt(4, movimientoProducto.getCodSucursalOrigen());
				ps1.setInt(5, movimientoProducto.getCodSucursalDestino());
				ps1.setString(6, movimientoProducto.getEstado());
				ps1.setInt(7, movimientoProducto.getNotaRemision());
				ps1.execute();
				
				String  sInsertDetalle = "INSERT INTO detallemovimientos ";
				sInsertDetalle += "(nroitem, cantidad, codmovimientoproducto, codproducto) ";
				sInsertDetalle += "VALUES (?, ?, ?, ?)";
				ps3 = conn.prepareStatement(sInsertDetalle);
				
				int nroItem = 1;
				List<DetalleMovimientoProducto> detalles = movimientoProducto.getDetalleMovimiento();
				for (DetalleMovimientoProducto detalle : detalles) {
			
				   //Actualiza el registro en stockproductos de la sucursal de origen
				   String sUpdateOrigen = "UPDATE stockproductos SET cantidad = cantidad - ? ";
				   sUpdateOrigen += "WHERE codsucursal_sucursales = ? ";
				   sUpdateOrigen += "AND codproducto_productos =  ? ";
					ps5 = conn.prepareStatement(sUpdateOrigen);
					ps5.setInt(3, detalle.getCodProducto());
					ps5.setInt(2, movimientoProducto.getCodSucursalOrigen());
					ps5.setInt(1, detalle.getCantidad());
					ps5.executeUpdate();
					    
					String sSelectExist = "SELECT cantidad FROM stockproductos ";
					sSelectExist += "WHERE codsucursal_sucursales = '"+movimientoProducto.getCodSucursalDestino()+"' ";
					sSelectExist += "AND codproducto_productos = '"+detalle.getCodProducto()+"' ";
					ps2 = conn.prepareStatement(sSelectExist);
					rs2 = ps2.executeQuery();
					//int cantidadActual = 0;
					if (rs2.next()) {
						//Actualiza el destino
						/*String sUpdateStock = "UPDATE stockproductos SET cantidad = cantidad + ? ";
						sUpdateStock += "WHERE codsucursal_sucursales = ? ";
						sUpdateStock += "AND codproducto_productos =  ? ";
						
						ps3 = conn.prepareStatement(sUpdateStock);
						ps3.setInt(1, detalle.getCodProducto());
						ps3.setInt(2, movimientoProducto.getCodSucursalDestino());
						ps3.setInt(3, detalle.getCantidad());
					    ps3.executeUpdate();*/
					}else{
						//Inserta registro en stockproductos para la sucursal destino y el producto
						/*String sInsertStock = "INSERT INTO stockproductos(codproducto_productos, codsucursal_sucursales, cantidad) ";
						sInsertStock += "VALUES (?, ?, ?)";
						ps4 = conn.prepareStatement(sInsertStock);
						ps4.setInt(1, detalle.getCodProducto());
						ps4.setInt(2, movimientoProducto.getCodSucursalDestino());
						ps4.setInt(3, detalle.getCantidad());
					    ps4.executeUpdate();*/
					}
					
					ps3.setInt(1, nroItem);
					ps3.setInt(2, detalle.getCantidad());
					ps3.setInt(3, codMovimientoProducto);
					ps3.setInt(4, detalle.getCodProducto());
					nroItem++;
					
					ps3.execute();
					ps3.clearParameters();
				}
				conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
			ConnectionManager.closeStatments(ps1);
			ConnectionManager.closeStatments(ps2);
			ConnectionManager.closeStatments(ps3);
			ConnectionManager.closeStatments(ps4);
		}
	}

	/**
	 * Verifica reglas en datos
	 * 
	 * @param sucursal
	 * @throws ValidException
	 */
	public Integer checkCantidad(Connection conn,int cantidad, int codSucursalOrigen, int codProducto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int cantidadOk = 0;
		int diferenciaDeCantidad = 0;
		try {
			if (cantidad == 0) {
				throw new ValidException("La cantidad a transladar no puede ser cero, introduzca una cantidad valida. ");
			}
				String sSelect = "SELECT cantidad FROM stockproductos ";
				sSelect += "WHERE codsucursal_sucursales = '"+codSucursalOrigen+"' ";
				sSelect += "AND codproducto_productos = '"+codProducto+"' ";
				ps = conn.prepareStatement(sSelect);
				rs = ps.executeQuery();
				if (rs.next()) {
					cantidadOk = rs.getInt(1);
				}
			
			if ( cantidad > cantidadOk) {
				throw new ValidException("La cantidad seleccionada es mayor al stock, introduzca una cantidad valida. ");
			}else{
				diferenciaDeCantidad = cantidadOk - cantidad;
				
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return diferenciaDeCantidad;

	}
	
	public void checkSeleccion(Producto producto) throws ValidException {
		if (producto == null) {
			throw new ValidException("Debe seleccionar un producto.");
		}
		if (producto.getCodProducto() == null) {
			throw new ValidException("Debe seleccionar un producto.");
		}
	}
	
	/**
	 * Anula el movimiento de producto, setea a AN
	 * 
	 * @param conn
	 * @param movimientoProducto
	 * @throws Exception
	 */
	public void anularMovimiento(Connection conn, MovimientoProducto movimientoProducto) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		try {
			checkSeleccion(movimientoProducto);
			String sql = "UPDATE movimientosproductos SET estado = 'RE'";
			sql += " WHERE codmovimientoproducto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, movimientoProducto.getCodMovimientoProducto());
						
			String sqlUpdateOrigen = "UPDATE stockproductos SET cantidad = cantidad + ?";
			sqlUpdateOrigen += " WHERE  codproducto_productos = ? and codsucursal_sucursales = "+movimientoProducto.getCodSucursalOrigen()+"";
			ps1 = conn.prepareStatement(sqlUpdateOrigen);
			
			List<DetalleMovimientoProducto> detalles = movimientoProducto.getDetalleMovimiento();
			for (DetalleMovimientoProducto detalleMov : detalles) {
				
				ps1.setInt(1, detalleMov.getCantidad());
				ps1.setInt(2, detalleMov.getCodProducto());
				ps1.addBatch();
				
			}
			
			ps.executeUpdate();
			ps1.executeBatch();
			
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public void checkSeleccion(MovimientoProducto movimientoProducto) throws ValidException {
		if (movimientoProducto == null) {
			throw new ValidException("Debe seleccionar un movimiento de producto.");
		}
		if (movimientoProducto.getCodMovimientoProducto() == null) {
			throw new ValidException("Debe seleccionar un movimiento de producto.");
		}
	}
	
	/**
	 * Acepta el movimiento de producto, setea a AC la cabecera y actualiza o crea el stock en la sucursal destino
	 * 
	 * @param conn
	 * @param movimientoProducto
	 * @throws Exception
	 */
	public void aceptarMovimiento(Connection conn, MovimientoProducto movimientoProducto) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs = null;
	
		try {
			checkSeleccion(movimientoProducto);
			String sql = "UPDATE movimientosproductos SET estado = 'AC'";
			sql += " WHERE codmovimientoproducto = ? and estado = 'ET' ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, movimientoProducto.getCodMovimientoProducto());
			
						
			String sqlUpdateDestino = "UPDATE stockproductos SET cantidad = cantidad + ?";
			sqlUpdateDestino += " WHERE  codproducto_productos = ? and codsucursal_sucursales = "+movimientoProducto.getCodSucursalDestino();
			ps1 = conn.prepareStatement(sqlUpdateDestino);
			
			String sInsertStock = "INSERT INTO stockproductos(codproducto_productos, codsucursal_sucursales, cantidad) ";
			sInsertStock += "VALUES (?, ?, ?)";
			ps2 = conn.prepareStatement(sInsertStock);
			
			List<DetalleMovimientoProducto> detalles = movimientoProducto.getDetalleMovimiento();
			for (DetalleMovimientoProducto detalleMov : detalles) {
				
				String sSelectExist = "SELECT cantidad FROM stockproductos ";
				sSelectExist += "WHERE codsucursal_sucursales = '"+movimientoProducto.getCodSucursalDestino()+"' ";
				sSelectExist += "AND codproducto_productos = '"+detalleMov.getCodProducto()+"' ";
				ps3 = conn.prepareStatement(sSelectExist);
				rs = ps3.executeQuery();
				if (rs.next()) {
					ps1.setInt(1, detalleMov.getCantidad());
					ps1.setInt(2, detalleMov.getCodProducto());
					ps1.addBatch();
					
				}else{
					//Inserta registro en stockproductos para la sucursal destino y el producto
					ps2.setInt(1, detalleMov.getCodProducto());
					ps2.setInt(2, movimientoProducto.getCodSucursalDestino());
					ps2.setInt(3, detalleMov.getCantidad());
				    ps2.addBatch();
				}
			}
			
			ps1.executeBatch();
			ps2.executeBatch();
			ps.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
			ConnectionManager.closeStatments(ps1);
			ConnectionManager.closeStatments(ps2);
			ConnectionManager.closeStatments(ps3);

		}
	}
	
	/**
	 * Rechazar el movimiento de producto, setea a RE la cabecera
	 * 
	 * @param conn
	 * @param movimientoProducto
	 * @throws Exception
	 */
	public void rechazarMovimiento(Connection conn, MovimientoProducto movimientoProducto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
	
		try {
			checkSeleccion(movimientoProducto);
			String sql = "UPDATE movimientosproductos SET estado = 'RE'";
			sql += " WHERE codmovimientoproducto = ? and estado = 'ET' ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, movimientoProducto.getCodMovimientoProducto());
			ps.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);

		}
	}
	
	/**
	 * Filtrar movimientos
	 * 
	 * @param conn
	 * @param 
	 * @throws Exception
	 */
	public List<MovimientoProducto> getMovimientosByFilters(Connection conn, Integer codsucursalOrigen, Integer codsucursalDestino, String estado, String fechaDesde, String fechaHasta) throws Exception {
		List<MovimientoProducto> ret = new ArrayList<MovimientoProducto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT codmovimientoproducto, fecha, estado, codsucursal_origen, codsucursal_destino, comentarios ";
			sSelect += "FROM movimientosproductos WHERE 1 = 1 ";
			
			if(estado != "" ){
				sSelect += " and estado = '"+ estado+"'";
			}

			if(codsucursalOrigen != null){
				sSelect += " and codsucursal_origen = '"+ codsucursalOrigen+"'";
			}
			
			if(codsucursalDestino != null){
				sSelect += " and codsucursal_destino = '"+ codsucursalDestino+"'";
			}
			
			if(fechaDesde != ""){
				sSelect += " and fecha >= '"+ fechaDesde+"'";
			}
			
			if(fechaHasta != ""){
				sSelect += " and fecha <= '"+ fechaHasta+"'";
			}
			
			sSelect += " ORDER BY notaremision DESC ";
			ps = conn.prepareStatement(sSelect);			
			rs = ps.executeQuery();
			while (rs.next()) {
				try {
					MovimientoProducto movimientoProducto = getMovimientoProducto(conn, rs.getInt("codmovimientoproducto"));
					ret.add(movimientoProducto);
				} catch (ValidException ve) {
				}
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);

		}
		return ret;
	}
}
