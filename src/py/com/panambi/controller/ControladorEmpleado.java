package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import py.com.panambi.bean.Bonificacion;
import py.com.panambi.bean.CargoEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.TipoDescuentoSalario;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorEmpleado extends ControladorPanambi {
	
	public void borrarEmpleado(Connection conn, Empleado empleado) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(empleado);
			String sql = "DELETE FROM empleados ";
			sql += " WHERE codempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, empleado.getCodEmpleado());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Devuelve empleado por codigo de empleado
	 * @param conn
	 * @param codempleado
	 * @return
	 * @throws Exception
	 */
	public Empleado getEmpleado(Connection conn, Integer codempleado) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Empleado ret = null;
		try {
			String sql = "SELECT * FROM empleados WHERE codempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codempleado);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Empleado();
				ret.setCodEmpleado(codempleado);
				ret.setNroDocumento(rs.getString("nrodocumento"));
				ret.setNombre(rs.getString("nombres"));
				ret.setApellido(rs.getString("apellidos"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setEmail(rs.getString("email"));
				ret.setSalario(rs.getDouble("salario"));
				ret.setFechaIngreso(rs.getDate("fechaingreso"));
				ret.setFechaSalido(rs.getDate("fechasalida"));
				ret.setEstado(rs.getString("estado"));
				
				Usuario usuario = new Usuario();
				if(rs.getInt("codusuario")<=0){
					usuario = null;
				}else usuario = new Usuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));
				ret.setUsuario(usuario);
				
				ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
				ret.setCargoEmpleado(new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado")));
				
				try{
					ret.setCodAsistencia(rs.getString("codigoasistencia"));
				}catch (Exception e ){
					ret.setCodAsistencia("");
				}
			} else {
				throw new ValidException("No existe el empleado con el c�digo [" + codempleado + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	public Usuario getUsuarioDeEmpleado(Connection conn , String nrodocumento) throws Exception{
		Usuario ret;
		Integer codUsuario = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codusuario FROM empleados WHERE nrodocumento = ? ");
			ps.setString(1, nrodocumento);
			rs = ps.executeQuery();
			if (rs.next()) {
				try{
					codUsuario = rs.getInt("codusuario");
				}catch (Exception e){
					codUsuario = 0;
				}
			}
			
			if(codUsuario !=0){
				ret = new Usuario(JFramePanambiMain.session.getConn(),codUsuario);
			}else ret = null;
			
		}catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}
	
	
	public void asignarUsuario(Connection conn , Empleado empleado, Usuario usuario) throws Exception{
		PreparedStatement ps = null;

		try{
			if(usuario!=null){
				String sql = "UPDATE empleados ";
				sql += "SET codusuario = ? ";
				sql += "WHERE codempleado = ? ";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, usuario.getCodUsuario());
				ps.setInt(2, empleado.getCodEmpleado());

			}else{
				String sql = "UPDATE empleados SET codusuario = null WHERE codempleado = ? ";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, empleado.getCodEmpleado());
				
			}
			ps.executeUpdate();
			
		}catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
			
	}
	
	public boolean comprobarExistencia(Connection conn, Usuario usuario) throws Exception{
		
		boolean existe = false;
		Integer cantidadUsuarios = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			String sql = "SELECT COUNT(*) FROM  empleados WHERE codusuario = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, usuario.getCodUsuario());
			rs = ps.executeQuery();
			while(rs.next()){
				cantidadUsuarios = rs.getInt(1);
			}
			if(cantidadUsuarios !=0){
				existe = true;
			}else existe = false;
			return existe;
			
		}catch(Exception e ){
			
			throw e;
		
		} finally{
			
			ConnectionManager.closeStatments(ps);
			ConnectionManager.closeResultSets(rs);
		}
		
	}
	
	/**
	 * Devuelve empleado por nro de documento
	 * @param conn
	 * @param nrodocumento
	 * @return
	 * @throws Exception
	 */
	public Empleado getEmpleado(Connection conn, String nrodocumento) throws Exception {
		Empleado ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM empleados WHERE nrodocumento = ? ");
			ps.setString(1, nrodocumento);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Empleado();
				ret = getEmpleado(conn, rs.getInt("codempleado"));
				
//				ret.setCodEmpleado(rs.getInt("codempleado"));
//				ret.setNroDocumento(rs.getString("nrodocumento"));
//				ret.setNombre(rs.getString("nombres"));
//				ret.setApellido(rs.getString("apellidos"));
//				ret.setDireccion(rs.getString("direccion"));
//				ret.setTelefono(rs.getString("telefono"));
//				ret.setEmail(rs.getString("email"));
//				ret.setSalario(rs.getDouble("salario"));
//				ret.setFechaIngreso(rs.getDate("fechaingreso"));
//				ret.setFechaSalido(rs.getDate("fechasalida"));
//				ret.setEstado(rs.getString("estado"));
//
//				ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//				ret.setCargoEmpleado(new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado")));
				
				try{
					ret.setCodAsistencia(rs.getString("codigoasistencia"));
				}catch (Exception e ){
					ret.setCodAsistencia("");
				}
				
				/*try{
					ret.setUsuario(new Usuario(JFramePanambiMain.session.getConn(),rs.getInt("codusuario")));
				}catch (Exception e){
					ret.setUsuario(null);
				}*/
									
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}
	
	/**
	 * Modifica los datos de un empleado registrado
	 * @param conn
	 * @param empleado
	 * @throws Exception
	 */
	
	public void modificarEmpleado(Connection conn, Empleado empleado) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(empleado);
			checkValues(empleado);
			//isDuplicated(conn, empleado);
			String sql = "UPDATE empleados ";
			sql += " SET nrodocumento = ?, ";
			sql += "nombres = ?, ";
			sql += "apellidos = ?, ";
			sql += "direccion = ?, ";
			sql += "telefono = ?, ";
			sql += "email = ?, ";
			sql += "salario = ?, ";
			sql += "fechaingreso = ?, ";
			sql += "fechasalida = ?, ";
			sql += "codsucursal = ?, ";
			sql += "codcargoempleado = ?, ";
			sql += "estado = ? ,";
			sql += "codigoasistencia = ? ";
			sql += " WHERE codempleado = ? ";
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, empleado.getNroDocumento());
			ps.setString(2, empleado.getNombre());
			ps.setString(3, empleado.getApellido());
			ps.setString(4, empleado.getDireccion());
			ps.setString(5, empleado.getTelefono());
			ps.setString(6, empleado.getEmail());
			ps.setDouble(7, empleado.getSalario());
			
			java.sql.Date sqlFechaIngreso = new java.sql.Date(empleado.getFechaIngreso().getTime());
			ps.setDate(8, sqlFechaIngreso);
			
			if(empleado.getFechaSalido()!=null){
				java.sql.Date sqlFechaSalida = new java.sql.Date(empleado.getFechaSalido().getTime());
				ps.setDate(9, sqlFechaSalida);
			}else ps.setDate(9, null);
			
			ps.setInt(10, empleado.getSucursal().getCodSucursal());
			ps.setInt(11, empleado.getCargoEmpleado().getCodCargoEmpleado());
			ps.setString(12, empleado.getEstado());
			ps.setString(13, empleado.getCodAsistencia());
			ps.setInt(14, empleado.getCodEmpleado());
			ps.executeUpdate();
			
			if(empleado.getEstado().equals("I") && empleado.getUsuario()!=null){
				Usuario usu = empleado.getUsuario();
				usu.setEstado("I");
				new ControladorUsuario().actualizarUsuario(conn, usu, false);
				asignarUsuario(conn, empleado,null);
			}
			
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Inserta un nuevo empleado
	 * @param conn
	 * @param empleado
	 * @throws Exception
	 */
	public void insertarEmpleado(Connection conn, Empleado empleado) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(empleado);
			isDuplicated(conn, empleado);
			String sql = "INSERT INTO empleados ";
			sql += " (nrodocumento, nombres, apellidos,telefono,direccion,email,salario,fechaingreso,fechasalida,codsucursal,";
			sql+= " codcargoempleado,estado,codigoasistencia )";
			sql += " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, empleado.getNroDocumento());
			ps.setString(2, empleado.getNombre());
			ps.setString(3, empleado.getApellido());
			ps.setString(4, empleado.getTelefono());
			ps.setString(5, empleado.getDireccion());
			ps.setString(6, empleado.getEmail());
			ps.setDouble(7, empleado.getSalario());
			
			java.sql.Date sqlFechaIngreso = new java.sql.Date(empleado.getFechaIngreso().getTime());
			ps.setDate(8, sqlFechaIngreso);
			
			
			if(empleado.getFechaSalido()!=null){
				java.sql.Date sqlFechaSalida = new java.sql.Date(empleado.getFechaSalido().getTime());
				ps.setDate(9, sqlFechaSalida);
			}else ps.setDate(9, null);
			
			
			ps.setInt(10, empleado.getSucursal().getCodSucursal());
			ps.setInt(11, empleado.getCargoEmpleado().getCodCargoEmpleado());
			ps.setString(12, empleado.getEstado());
			ps.setString(13, empleado.getCodAsistencia());
			
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	
	/**
	 * Comprueba si ya existe un empleado con el mismo nombre
	 * @param conn
	 * @param empleado
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Empleado empleado) throws Exception {
		boolean ret = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT COUNT(1) FROM empleados ";
			sSelect += "WHERE nrodocumento = ? ";
			if (empleado.getCodEmpleado() != null) {
				sSelect += " AND codempleado <> ? ";
			}
			ps = conn.prepareStatement(sSelect);
			ps.setString(1, empleado.getNroDocumento());
			if (empleado.getCodEmpleado() != null) {
				ps.setInt(2, empleado.getCodEmpleado());
			}
			rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			if (cantidad > 0) {
				throw new ValidException("La empleado " + empleado.getNombre() + " ya existe.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	
	/**
	 * Obtiene la lista de todos los empleados registrados, con todos su informacion
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Empleado> getEmpleados(Connection conn) throws Exception {
		List<Empleado> empleados = new ArrayList<Empleado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM empleados ORDER BY nrodocumento");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCodEmpleado(rs.getInt("codempleado"));
				empleado.setNroDocumento(rs.getString("nrodocumento"));
				empleado.setNombre(rs.getString("nombres"));
				empleado.setApellido(rs.getString("apellidos"));
				empleado.setDireccion(rs.getString("direccion"));
				empleado.setTelefono(rs.getString("telefono"));
				empleado.setEmail(rs.getString("email"));
				empleado.setSalario(rs.getDouble("salario"));
				empleado.setFechaIngreso(rs.getDate("fechaingreso"));
				empleado.setFechaSalido(rs.getDate("fechasalida"));
				empleado.setEstado(rs.getString("estado"));
				empleado.setCodAsistencia(rs.getString("codigoasistencia"));
				
				Sucursal sucursal = new Sucursal();
				if(rs.getInt("codsucursal")<=0){
					sucursal = null;
				}else sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")); 
				empleado.setSucursal(sucursal);

				Usuario usuario = new Usuario();
				if(rs.getInt("codusuario")<=0){
					usuario = null;
				}else usuario = new Usuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));
				empleado.setUsuario(usuario);
				
				CargoEmpleado cargoEmpleado = new CargoEmpleado();
				if(rs.getInt("codcargoempleado")<=0){
					cargoEmpleado = null;
				}else cargoEmpleado = new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado"));
				empleado.setCargoEmpleado(cargoEmpleado);
				
				empleados.add(empleado);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return empleados;
	}
	
	/**
	 * Obtiene todos los empleados activos.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Empleado> getEmpleadosActivos(Connection conn) throws Exception {
		List<Empleado> empleados = new ArrayList<Empleado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM empleados	WHERE estado = 'A' ORDER BY nrodocumento");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCodEmpleado(rs.getInt("codempleado"));
				empleado.setNroDocumento(rs.getString("nrodocumento"));
				empleado.setNombre(rs.getString("nombres"));
				empleado.setApellido(rs.getString("apellidos"));
				empleado.setDireccion(rs.getString("direccion"));
				empleado.setTelefono(rs.getString("telefono"));
				empleado.setEmail(rs.getString("email"));
				empleado.setSalario(rs.getDouble("salario"));
				empleado.setFechaIngreso(rs.getDate("fechaingreso"));
				empleado.setFechaSalido(rs.getDate("fechasalida"));
				empleado.setEstado(rs.getString("estado"));
				empleado.setCodAsistencia(rs.getString("codigoasistencia"));
				
				Sucursal sucursal = new Sucursal();
				if(rs.getInt("codsucursal")<=0){
					sucursal = null;
				}else sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")); 
				empleado.setSucursal(sucursal);

				Usuario usuario = new Usuario();
				if(rs.getInt("codusuario")<=0){
					usuario = null;
				}else usuario = new Usuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));
				empleado.setUsuario(usuario);
				
				CargoEmpleado cargoEmpleado = new CargoEmpleado();
				if(rs.getInt("codcargoempleado")<=0){
					cargoEmpleado = null;
				}else cargoEmpleado = new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado"));
				empleado.setCargoEmpleado(cargoEmpleado);
				
				empleados.add(empleado);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return empleados;
	}

	
	/**
	 * Comprueba si se encuentra seleccionado un empleado
	 * @param empleado
	 * @throws ValidException
	 */
	private void checkSeleccion(Empleado empleado) throws ValidException {
		if (empleado== null) {
			throw new ValidException("Debe seleccionar un empleado.");
		}
		if (empleado.getCodEmpleado() == null) {
			throw new ValidException("Debe seleccionar un empleado.");
		}
	}
	
	/**
	 * 
	 * @param empleado
	 * @throws ValidException
	 */
	
	private void checkValues(Empleado empleado) throws ValidException {
		if (empleado.getNombre() == null) {
			throw new ValidException("Debe ingresar nombre de empleado.");
		}
		if (empleado.getNombre().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre de empleado.");
		}

	}
	
	/**
	 * Devuelve empleado por codigo de empleado
	 * @param conn
	 * @param codempleado
	 * @return
	 * @throws Exception
	 */

	public List<Empleado> getEmpleados(Connection conn, String documentoempleado, Integer codigoempleado, String nombreempleado, Integer codsucursal) throws Exception {
		List<Empleado> empleados = new ArrayList<Empleado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT * FROM empleados";
			sSelect += " WHERE 1=1 ";
			if(!documentoempleado.isEmpty()){
				sSelect += " AND nrodocumento LIKE '%"+documentoempleado+"%'";
			}
			
			if(codigoempleado!=null){
				sSelect += " AND codempleado ="+codigoempleado;
			}
			
			if(!nombreempleado.isEmpty()){
				sSelect += " AND (nombres LIKE '%"+nombreempleado+"%'";
				sSelect += " OR apellidos LIKE '%"+nombreempleado+"%')";
			}
			
			if(codsucursal != null){
				sSelect += " AND codsucursal = "+codsucursal;
			}
			
			ps = conn.prepareStatement(sSelect);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCodEmpleado(rs.getInt("codempleado"));
				empleado.setNroDocumento(rs.getString("nrodocumento"));
				empleado.setNombre(rs.getString("nombres"));
				empleado.setApellido(rs.getString("apellidos"));
				empleado.setDireccion(rs.getString("direccion"));
				empleado.setTelefono(rs.getString("telefono"));
				empleado.setEmail(rs.getString("email"));
				empleado.setSalario(rs.getDouble("salario"));
				empleado.setFechaIngreso(rs.getDate("fechaingreso"));
				empleado.setFechaSalido(rs.getDate("fechasalida"));
				empleado.setEstado(rs.getString("estado"));
				empleado.setCodAsistencia(rs.getString("codigoasistencia"));
				
				Sucursal sucursal = new Sucursal();
				if(rs.getInt("codsucursal")<=0){
					sucursal = null;
				}else sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")); 
				empleado.setSucursal(sucursal);

				Usuario usuario = new Usuario();
				if(rs.getInt("codusuario")<=0){
					usuario = null;
				}else usuario = new Usuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));
				empleado.setUsuario(usuario);
				
				CargoEmpleado cargoEmpleado = new CargoEmpleado();
				if(rs.getInt("codcargoempleado")<=0){
					cargoEmpleado = null;
				}else cargoEmpleado = new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado"));
				empleado.setCargoEmpleado(cargoEmpleado);
				
				empleados.add(empleado);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return empleados;
	}
	
	public List<Empleado> getEmpleadosActivos(Connection conn, String documentoempleado, Integer codigoempleado, String nombreempleado, Integer codsucursal) throws Exception {
		List<Empleado> empleados = new ArrayList<Empleado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT * FROM empleados";
			sSelect += " WHERE estado = 'A' ";
			
			if(!documentoempleado.isEmpty()){
				sSelect += " AND nrodocumento LIKE '%"+documentoempleado+"%'";
			}
			
			if(codigoempleado!=null){
				sSelect += " AND codempleado ="+codigoempleado;
			}
			
			if(!nombreempleado.isEmpty()){
				sSelect += " AND (nombres LIKE '%"+nombreempleado+"%'";
				sSelect += " OR apellidos LIKE '%"+nombreempleado+"%')";
			}
			
			if(codsucursal != null){
				sSelect += " AND codsucursal = "+codsucursal;
			}
			
			ps = conn.prepareStatement(sSelect);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCodEmpleado(rs.getInt("codempleado"));
				empleado.setNroDocumento(rs.getString("nrodocumento"));
				empleado.setNombre(rs.getString("nombres"));
				empleado.setApellido(rs.getString("apellidos"));
				empleado.setDireccion(rs.getString("direccion"));
				empleado.setTelefono(rs.getString("telefono"));
				empleado.setEmail(rs.getString("email"));
				empleado.setSalario(rs.getDouble("salario"));
				empleado.setFechaIngreso(rs.getDate("fechaingreso"));
				empleado.setFechaSalido(rs.getDate("fechasalida"));
				empleado.setEstado(rs.getString("estado"));
				empleado.setCodAsistencia(rs.getString("codigoasistencia"));
				
				Sucursal sucursal = new Sucursal();
				if(rs.getInt("codsucursal")<=0){
					sucursal = null;
				}else sucursal = new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")); 
				empleado.setSucursal(sucursal);

				Usuario usuario = new Usuario();
				if(rs.getInt("codusuario")<=0){
					usuario = null;
				}else usuario = new Usuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario"));
				empleado.setUsuario(usuario);
				
				CargoEmpleado cargoEmpleado = new CargoEmpleado();
				if(rs.getInt("codcargoempleado")<=0){
					cargoEmpleado = null;
				}else cargoEmpleado = new CargoEmpleado(JFramePanambiMain.session.getConn(),rs.getInt("codcargoempleado"));
				empleado.setCargoEmpleado(cargoEmpleado);
				
				empleados.add(empleado);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return empleados;
	}
	
	public void getAplicarDescuentoPersonal(Connection conn, Integer codempleado, Double monto, Float porcentaje, String concepto, String tipo) throws Exception {
		PreparedStatement ps = null;
		try {
			//verificar que no existan dos con el mismo concepto vigente
			Integer codigoExternoTipoDescuento = null;
			Integer codigoExternoTipoBonificacion = null;
			
			ControladorTipoDescuentoSalario controladortipodescuentosalario = new ControladorTipoDescuentoSalario();	
			TipoDescuentoSalario descuentoSalario = controladortipodescuentosalario.getTipoDescuento(conn, concepto);
			if (descuentoSalario != null){
				codigoExternoTipoDescuento = descuentoSalario.getCodTipoDescuento();
			}
			
			ControladorBonificacion controladorBonificacion = new ControladorBonificacion();
			Bonificacion bonificacion = controladorBonificacion.getBonificacion(conn, concepto);
			if (bonificacion != null){
				codigoExternoTipoBonificacion = bonificacion.getCodBonificacion();
			}
			
			if(tipo.equals("D")){
				codigoExternoTipoBonificacion = null;
			}else{
				codigoExternoTipoDescuento = null;
			}
			
			String sql = "INSERT INTO descuentossalarios";
			sql += " (coddescuentosalario, porcentaje, monto, tipo,codempleado, fechainicio, fechafin, concepto, codtipodescuentosalario, estado, codbonificacion)";
			sql += "  VALUES (NEXTVAL('descuentossalarios_coddescuentosalario_seq'), ?, ?, ?,  ?, current_date, null, ?, ?,?, ?)";	
			ps = conn.prepareStatement(sql);
			ps.setFloat(1, porcentaje);
			ps.setDouble(2, monto);
			ps.setString(3, tipo);
			ps.setInt(4, codempleado);
			ps.setString(5, concepto);
			
			if(codigoExternoTipoDescuento != null){
				ps.setInt(6, codigoExternoTipoDescuento);
			}else{
				ps.setNull(6, java.sql.Types.INTEGER);
			}
			
			ps.setString(7, "A");
			
			if(codigoExternoTipoBonificacion != null){
				ps.setInt(8, codigoExternoTipoBonificacion);
			}else{
				ps.setNull(8, java.sql.Types.INTEGER);
			}
			
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
		
	public void  getCambiarEstadoDetalle(Connection conn, String estado, List<Integer> detalle) throws Exception {
		PreparedStatement ps = null;
		try {
			
			java.util.Date date = Calendar.getInstance().getTime();
			date.setTime(0);
			java.sql.Date sqlDate = new java.sql.Date(date.getTime()); 
			
			String sql = "UPDATE descuentossalarios SET fechafin = ?, estado = ? WHERE coddescuentosalario= ?";		
			ps = conn.prepareStatement(sql);
			
			for(int i=0;i<detalle.size();i++){
				if(estado.equals("I")){
					ps.setNull(1, java.sql.Types.DATE); 
					ps.setString(2, estado);
					ps.setInt(3, detalle.get(i));
				}else{
					ps.setDate(1,sqlDate);
					ps.setString(2, estado);
					ps.setInt(3, detalle.get(i));
				}
				
				ps.executeUpdate();
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public Boolean  getVerificarDetalleDuplicado(Connection conn, Integer codigo, String tipo, Integer codempleado) throws Exception {
		PreparedStatement ps = null;
		Boolean duplicado = false;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM descuentossalarios WHERE codempleado =? AND codtipodescuentosalario = ? AND codbonificacion is null AND tipo = ? AND estado = ?  ";
			ps = conn.prepareStatement(sql);
			if(tipo.equals("D")){
				//verifica descuento duplicado	
				ps = conn.prepareStatement(sql);
				ps.setInt(1, codempleado); 
				ps.setInt(2, codigo);
				ps.setString(3, tipo);
				ps.setString(4, "A");
				
			}else{
				//verifica bonificacion duplicada
				sql = "SELECT * FROM descuentossalarios WHERE codempleado =? AND codtipodescuentosalario is null AND codbonificacion =? AND tipo = ? AND estado = ?  ";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, codempleado); 
				ps.setInt(2, codigo);
				ps.setString(3, tipo);
				ps.setString(4, "A");
			}
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				duplicado = true;
			}
			

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return duplicado;
	}

	public List<List<String>> getAsignacionesDescuentos(Connection conn, Integer codigoempleado) throws Exception {
		List<String> detalle = new ArrayList<String>();
		List<List<String>> detalles = new ArrayList<List<String>>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT * FROM descuentossalarios";
			sSelect += " WHERE codempleado = "+codigoempleado;		
			ps = conn.prepareStatement(sSelect);
			rs = ps.executeQuery();
			while (rs.next()){
				detalle = new ArrayList<String>();
				detalle.add(rs.getString("coddescuentosalario"));
				detalle.add(rs.getString("concepto"));
				detalle.add(rs.getString("porcentaje"));
				detalle.add(rs.getString("monto"));
				detalle.add(rs.getString("estado"));
				detalles.add((ArrayList<String>) detalle);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return detalles;
	}
	public Empleado getEmpleadoDeUsuario(Connection conn, Integer codusuario) throws Exception {
		Empleado ret;
		Integer codEmpleado = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codempleado FROM empleados WHERE codusuario = ? ");
			ps.setInt(1, codusuario);
			rs = ps.executeQuery();
			if (rs.next()) {
				codEmpleado = rs.getInt("codempleado");
			}
			if (codEmpleado != 0) {
				ret = getEmpleado(conn, codEmpleado);
			} else
				ret = null;
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

}
