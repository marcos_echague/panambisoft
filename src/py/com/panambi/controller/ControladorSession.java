package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import py.com.panambi.bean.Session;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorSession extends ControladorPanambi {

	public ControladorSession() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Registra la sesion en la DB y retorna el codigo generado.
	 * 
	 * @param conn
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public Integer insertarSesion(Connection conn, Session session) throws Exception {
		PreparedStatement ps = null;
		Integer codigo = null;
		try {
			String sql = "INSERT INTO sessions ";
			sql += " (codusuario, pid, inicio, hostip) ";
			sql += " VALUES (?, pg_backend_pid(), now(), ?) ";
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, session.getUsuario().getCodUsuario());
			ps.setString(2, session.getHostip());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				codigo = rs.getInt(1);
			}
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return codigo;
	}

	public Integer finalizarSesion(Connection conn, Session session) throws Exception {
		PreparedStatement ps = null;
		Integer codigo = null;
		try {
			String sql = "UPDATE sessions ";
			sql += " set fin = now() ";
			sql += " WHERE codsession = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, session.getCodSession());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return codigo;
	}

	/**
	 * 
	 * Devuelve la sesion a partir del c�digo
	 * 
	 * @param conn
	 * @param codsession
	 * @return
	 * @throws Exception
	 */
	public Session getSession(Connection conn, Integer codsession) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Session ret = null;
		try {
			ControladorUsuario controladorUsuario = new ControladorUsuario();
			String sql = "SELECT * FROM sessions WHERE codsession = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codsession);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Session();
				ret.setCodSession(codsession);
				ret.setFin(rs.getTimestamp("fin"));
				ret.setInicio(rs.getTimestamp("inicio"));
				ret.setHostip(rs.getString("hostip"));
				ret.setUsuario(controladorUsuario.getUsuario(conn, rs.getInt("codusuario")));
				ret.setConn(conn);
			} else {
				throw new ValidException("No existe la sesi�n para el c�digo [" + codsession + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

}
