package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.exceptions.ValidException;

public class ControladorPerfilUsuario extends ControladorPanambi {
	
	public void insertarPerfil(Connection conn, PerfilUsuario perfil) throws Exception {
		try {
			checkValues(perfil);
			isDuplicated(conn, perfil);
			String sql = "INSERT INTO perfiles ";
			sql += " (nombre, estado ) ";
			sql += " VALUES (?, ?) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, perfil.getNombre());
			ps.setString(2, perfil.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	public void borrarPerfil(Connection conn, PerfilUsuario perfil) throws Exception {
		try {
			checkSeleccion(perfil);
			String sql = "DELETE FROM perfiles ";
			sql += " WHERE codperfil = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, perfil.getCodPerfil());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Lista todas los perfiles de usuario desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<PerfilUsuario> getPerfiles(Connection conn) throws Exception {
		List<PerfilUsuario> perfiles = new ArrayList<PerfilUsuario>();
		PreparedStatement ps = conn.prepareStatement("SELECT codperfil FROM perfiles ORDER BY nombre");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			perfiles.add(new PerfilUsuario(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return perfiles;
	}

	/**
	 * Devuelve perfil  de usuario por Nombre
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public PerfilUsuario getPerfil(Connection conn, String nombre) throws Exception {
		PerfilUsuario ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codperfil FROM perfiles WHERE nombre = ? ");
		ps.setString(1, nombre);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new PerfilUsuario(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Actualiza datos del perfil
	 * @param conn
	 * @param perfil
	 * @throws Exception
	 */
	public void modificarPerfil(Connection conn, PerfilUsuario perfil) throws Exception {
		try {
			checkSeleccion(perfil);
			checkValues(perfil);
			isDuplicated(conn, perfil);
			String sql = "UPDATE perfiles ";
			sql += " SET nombre = ?, ";
			sql += "estado = ? ";
			sql += " WHERE codperfil = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, perfil.getNombre());
			ps.setString(2, perfil.getEstado());
			ps.setInt(3, perfil.getCodPerfil());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Verifica reglas en datos
	 * @param perfil
	 * @throws ValidException
	 */
	private void checkValues(PerfilUsuario perfil) throws ValidException {
		if (perfil.getNombre() == null) {
			throw new ValidException("Debe ingresar nombre del perfil de usuario.");
		}
		if (perfil.getNombre().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre del perfil de usuario.");
		}

	}
	

	/**
	 * Verifica que se haya seleccionado el perfil de usuario
	 * @param perfil
	 * @throws ValidException
	 */
	private void checkSeleccion(PerfilUsuario perfil) throws ValidException {
		if (perfil == null) {
			throw new ValidException("Debe seleccionar un perfil de usuairo.");
		}
		if (perfil.getCodPerfil() == null) {
			throw new ValidException("Debe seleccionar un perfil de usuario.");
		}
	}
	/**
	 * Verifica si existe en la base de datos un perfil con el mismo nombre a la envia
	 * @param conn
	 * @param perfil
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, PerfilUsuario perfil) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM perfiles ";
		sSelect += "WHERE nombre = ? ";
		if (perfil.getCodPerfil() != null) {
			sSelect += " AND codperfil <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, perfil.getNombre());
		if (perfil.getCodPerfil() != null) {
			ps.setInt(2, perfil.getCodPerfil());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("El perfil " + perfil.getNombre() + " ya existe.");
		}
		return ret;
	}

	public void darBajaPerfilUsuario(Connection conn, PerfilUsuario perfil) throws Exception {
		try {
			checkSeleccion(perfil);
			String sql = "UPDATE perfiles ";
		    sql+= "SET estado = 'I' ";
		    sql+="WHERE codperfil = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, perfil.getCodPerfil());
			ps.executeUpdate();
			ps.close();
			}  catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
		
	}

}
