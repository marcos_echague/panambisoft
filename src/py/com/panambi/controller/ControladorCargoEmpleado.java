package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.CargoEmpleado;
import py.com.panambi.exceptions.ValidException;

public class ControladorCargoEmpleado extends ControladorPanambi {
	
	public void  insertarCargoEmpleado(Connection conn, CargoEmpleado cargoEmpleado) throws Exception {
		try {
			checkValues(cargoEmpleado);
			isDuplicated(conn, cargoEmpleado);
			String sql = "INSERT INTO cargosempleados ";
			sql += " (descripcion, estado ) ";
			sql += " VALUES ( ?, ? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cargoEmpleado.getDescripcion());
			ps.setString(2, cargoEmpleado.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Actualiza datos del cargo de empleados
	 * @param conn
	 * @param cargoEmpleado
	 * @throws Exception
	 */
	public void modificarCargoEmpleado(Connection conn, CargoEmpleado cargoEmpleado) throws Exception {
		try {
			checkSeleccion(cargoEmpleado);
			checkValues(cargoEmpleado);
			isDuplicated(conn, cargoEmpleado);
			String sql = "UPDATE cargosempleados ";
			sql += " SET descripcion = ?, ";
			sql += " estado = ? ";
			sql += " WHERE codcargoempleado = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cargoEmpleado.getDescripcion());
			ps.setString(2, cargoEmpleado.getEstado());
			ps.setInt(3, cargoEmpleado.getCodCargoEmpleado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	
	public void borrarCargoEmpleado(Connection conn, CargoEmpleado cargoEmpleado) throws Exception {
		try {
			checkSeleccion(cargoEmpleado);
			String sql = "DELETE FROM cargosempleados ";
			sql += " WHERE codcargoempleado = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, cargoEmpleado.getCodCargoEmpleado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	
	public CargoEmpleado getCargoEmpleado(Connection conn, String descripcion) throws Exception {
		CargoEmpleado ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codcargoempleado FROM cargosempleados WHERE descripcion = ?");
		ps.setString(1, descripcion);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new CargoEmpleado(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Lista todas las formas de pago desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<CargoEmpleado> getCargosEmpleados(Connection conn) throws Exception {
		List<CargoEmpleado> cargosEmpleados = new ArrayList<CargoEmpleado>();
		PreparedStatement ps = conn.prepareStatement("SELECT codcargoempleado FROM cargosempleados ORDER BY descripcion");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			cargosEmpleados.add(new CargoEmpleado(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return cargosEmpleados;
	
	}
	
	/**
	 * Verifica reglas en datos
	 * @param cargoEmpleado
	 * @throws ValidException
	 */
	private void checkValues(CargoEmpleado cargoEmpleado) throws ValidException {
		
		if (cargoEmpleado.getDescripcion() == null) {
			throw new ValidException("Debe ingresar nombre del cargo de empleado.");
		}
		
	}

	/**
	 * Verifica si existe en la base de datos un cargo de empleado con el mismo nombre
	 * @param conn
	 * @param cargoEmpleado
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, CargoEmpleado cargoEmpleado) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM cargosempleados ";
		sSelect += "WHERE descripcion = ? ";
		if (cargoEmpleado.getCodCargoEmpleado() != null) {
			sSelect += " AND codcargoempleado <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, cargoEmpleado.getDescripcion());
		if (cargoEmpleado.getCodCargoEmpleado() != null) {
			ps.setInt(2, cargoEmpleado.getCodCargoEmpleado());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("El cargo de empleado" + cargoEmpleado.getDescripcion() + " ya existe.");
		}
		return ret;
	}
	
	/**
	 * Verifica que se haya seleccionado el cargo de empleado
	 * @param cargoEmpleado
	 * @throws ValidException
	 */
	private void checkSeleccion(CargoEmpleado cargoEmpleado) throws ValidException {
		if (cargoEmpleado == null) {
			throw new ValidException("Debe seleccionar un cargo de empleado.");
		}
		if (cargoEmpleado.getCodCargoEmpleado() == null) {
			throw new ValidException("Debe seleccionar un cargo de empleado.");
		}
	}
	
	

}
