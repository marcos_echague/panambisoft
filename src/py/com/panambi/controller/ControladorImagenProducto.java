package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.bean.ImagenProducto;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorImagenProducto extends ControladorPanambi {
	
	/**
	 * Inserta imagen de producto ala Base de Datos
	 * 
	 * @param conn
	 * @param imagenProducto
	 * @throws Exception
	 */
	public void insertarImagenProducto(Connection conn, ImagenProducto imagenProducto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(imagenProducto);
			isDuplicated(conn, imagenProducto);
			String sql = "INSERT INTO imagenesproductos ";
			sql += " (imagen, descripcion, codproducto) ";
			sql += " VALUES (?, ?, ?) ";
			ps = conn.prepareStatement(sql);
			ps.setBytes(1, imagenProducto.getImagen());
			ps.setString(2, imagenProducto.getDescripcion());
			ps.setInt(3, imagenProducto.getCodProducto());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Elimina la imagen del producto de la base de datos
	 * 
	 * @param conn
	 * @param imagenProducto
	 * @throws Exception
	 */
	public void borrarImagenProducto(Connection conn, ImagenProducto imagenProducto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(imagenProducto);
			String sql = "DELETE FROM imagenesproductos ";
			sql += " WHERE codimagen = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, imagenProducto.getCodImagen());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Devuelve Imagen del producto codigo del producto
	 * 
	 * @param conn
	 * @param codProducto
	 * @return
	 */
	public ImagenProducto getImagenProducto(Connection conn, Integer codProducto) throws Exception {
		ImagenProducto ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM imagenesproductos WHERE codproducto = ?");
			ps.setInt(1, codProducto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new ImagenProducto();
				ret.setCodImagen(rs.getInt("codimagen"));
				ret.setImagen(rs.getBytes("imagen"));
				ret.setDescripcion(rs.getString("descripcion"));
				ret.setCodProducto(rs.getInt("codproducto"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Actualiza datos de la imagen de un producto
	 * 
	 * @param conn
	 * @param imagenProducto
	 * @throws Exception
	 */
	public void modificarImagenProducto(Connection conn, ImagenProducto imagenProducto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(imagenProducto);
			checkValues(imagenProducto);
			isDuplicated(conn, imagenProducto);
			String sql = "UPDATE imagenesproductos ";
			sql += " SET imagen = ?, ";
			sql += "descripcion = ?, ";
			sql += "codproducto = ? ";
			sql += " WHERE codimagen = ? ";
			ps = conn.prepareStatement(sql);
			ps.setBytes(1, imagenProducto.getImagen());
			ps.setString(2, imagenProducto.getDescripcion());
			ps.setInt(3, imagenProducto.getCodProducto());
			ps.setInt(4, imagenProducto.getCodImagen());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Verifica reglas en datos
	 * 
	 * @param imagenProducto
	 * @throws ValidException
	 */
	private void checkValues(ImagenProducto imagenProducto) throws ValidException {
		if (imagenProducto.getImagen() == null) {
			throw new ValidException("Debe ingresar una imagen al producto.");
		}
		if (imagenProducto.getImagen().toString().isEmpty()) {
			throw new ValidException("Debe ingresar una imagen al producto.");
		}

	}
	
	/**
	 * Verifica que se haya seleccionado una imagen de producto
	 * 
	 * @param imagenProducto
	 * @throws ValidException
	 */
	private void checkSeleccion(ImagenProducto imagenProducto) throws ValidException {
		if (imagenProducto == null) {
			throw new ValidException("Debe seleccionar una imagen de un producto producto.");
		}
		if (imagenProducto.getCodImagen() == null) {
			throw new ValidException("Debe seleccionar una imagen.");
		}
	}

	/**
	 * Verifica si existe en la base de datos una imagen del producto similar
	 * 
	 * 
	 * @param conn
	 * @param imagenProducto
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, ImagenProducto imagenProducto) throws Exception {
		boolean ret = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSelect = "SELECT COUNT(1) FROM imagenesproductos";
			sSelect += "WHERE imagen = ? ";
			if (imagenProducto.getCodImagen() != null) {
				sSelect += " AND codimagen <> ? ";
			}
			ps = conn.prepareStatement(sSelect);
			ps.setBytes(1, imagenProducto.getImagen());
			if (imagenProducto.getCodImagen() != null) {
				ps.setInt(2, imagenProducto.getCodImagen());
			}
			rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			if (cantidad > 0) {
				throw new ValidException("La imagen de produto ya existe para otro producto.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}


	
	


}
