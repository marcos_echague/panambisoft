package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.TipoDescuentoSalario;
import py.com.panambi.exceptions.ValidException;

public class ControladorTipoDescuentoSalario extends ControladorPanambi {
	
	public boolean comprobarReferencia(Connection conn , TipoDescuentoSalario tipoDescuentoSalario) throws Exception{
		boolean existe = false;
		Integer codigoTipoDescuento ;
		PreparedStatement ps = conn.prepareStatement("SELECT DISTINCT codtipodescuentosalario FROM descuentossalarios");
		ResultSet rs = ps.executeQuery();
		while (rs.next()){
			codigoTipoDescuento = rs.getInt(1);
			if(codigoTipoDescuento == tipoDescuentoSalario.getCodTipoDescuento()){
				existe = true;
			}
		}
		
		return existe;
	}
	
	public void insertarTipoDescuento(Connection conn, TipoDescuentoSalario tipoDescuentoSalario) throws Exception {
		try {
			checkValues(tipoDescuentoSalario);
			isDuplicated(conn, tipoDescuentoSalario);
			String sql = "INSERT INTO tiposdescuentossalarios ";
			sql += " (nombre, formaaplicacion, tipo, porcentaje, montofijo, estado) ";
			
			sql += " VALUES ( ?, ?, ?, ? ,? ,? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, tipoDescuentoSalario.getNombre());
			ps.setString(2, tipoDescuentoSalario.getFormaAplicacion());
			ps.setString(3, tipoDescuentoSalario.getTipoDescuento());
			ps.setFloat(4, tipoDescuentoSalario.getPorcentaje());
			ps.setDouble(5, tipoDescuentoSalario.getMontoFijo());
			ps.setString(6, tipoDescuentoSalario.getEstado());
			
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	public void borrarTipoDescuento(Connection conn, TipoDescuentoSalario tipoDescuentoSalario) throws Exception {
		try {
			checkSeleccion(tipoDescuentoSalario);
			String sql = "DELETE FROM tiposdescuentossalarios ";
			sql += " WHERE codtipodescuentosalario = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, tipoDescuentoSalario.getCodTipoDescuento());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	public void darBajaTipoDescuento(Connection conn, TipoDescuentoSalario tipoDescuentoSalario) throws Exception {
		try {
			checkSeleccion(tipoDescuentoSalario);
			String sql = "UPDATE tiposdescuentossalarios ";
			sql += " SET estado = 'I' ";
			sql += " WHERE codtipodescuentosalario = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, tipoDescuentoSalario.	getCodTipoDescuento());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
		/**
		 * Lista todas los tipos de descuentos desde la Base de Datos
		 * 
		 * @param conn
		 * @return
		 * @throws Exception
		 */
		public List<TipoDescuentoSalario> getTiposDescuentos (Connection conn) throws Exception {
			List<TipoDescuentoSalario> tiposDescuentosSalarios = new ArrayList<TipoDescuentoSalario>();
			PreparedStatement ps = conn.prepareStatement("SELECT codtipodescuentosalario FROM tiposdescuentossalarios ORDER BY nombre");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				tiposDescuentosSalarios.add(new TipoDescuentoSalario(conn, rs.getInt(1)));
			}
			rs.close();
			ps.close();
			return tiposDescuentosSalarios;
		}

		/**
		 * Devuelve Tipo de descuento por Nombre
		 * 
		 * @param conn
		 * @param nombre
		 * @return
		 */
		public TipoDescuentoSalario getTipoDescuento(Connection conn , String nombre) throws Exception {
			TipoDescuentoSalario ret = null;
			PreparedStatement ps = conn.prepareStatement("SELECT codtipodescuentosalario FROM tiposdescuentossalarios WHERE nombre = ?");
			ps.setString(1, nombre);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ret = new TipoDescuentoSalario(conn, rs.getInt(1));
			}
			rs.close();
			ps.close();
			return ret;
		}
		/**
		 * Actualiza datos del tipo de descuento de salario
		 * @param conn
		 * @param tipoDescuentoSalario
		 * @throws Exception
		 */
		public void modificarTipoDescuento(Connection conn, TipoDescuentoSalario tipoDescuentoSalario) throws Exception {
			try {
				checkSeleccion(tipoDescuentoSalario);
				checkValues(tipoDescuentoSalario);
				isDuplicated(conn, tipoDescuentoSalario);
				//TipoDescuentoSalario tipoDescuentoAnterior = new ControladorTipoDescuentoSalario().getTipoDescuento(JFramePanambiMain.session.getConn(), tipoDescuentoSalario.getNombre());
				
				/*if (tipoDescuentoAnterior.getTipoDescuento()!= tipoDescuentoSalario.getTipoDescuento()){
					
				}*/
				//if(tipoDescuentoAnterior.getTipoDescuento().equals(tipoDescuentoSalario.getTipoDescuento())){

					
					String sql = "UPDATE tiposdescuentossalarios ";
					sql += " SET nombre = ?, ";
					sql += "estado = ?, ";
					sql += "formaaplicacion = ?, ";
					sql += "tipo = ?, ";
					sql += "porcentaje = ?, ";
					sql += "montofijo = ? ";
					sql += " WHERE codtipodescuentosalario = ? ";
					
					PreparedStatement ps = conn.prepareStatement(sql);
					
					ps.setString(1, tipoDescuentoSalario.getNombre());
					ps.setString(2, tipoDescuentoSalario.getEstado());
					ps.setString(3, tipoDescuentoSalario.getFormaAplicacion());
					ps.setString(4, tipoDescuentoSalario.getTipoDescuento());
					ps.setFloat(5, tipoDescuentoSalario.getPorcentaje());
					ps.setDouble(6, tipoDescuentoSalario.getMontoFijo());
					ps.setDouble(7, tipoDescuentoSalario.getCodTipoDescuento());
					ps.executeUpdate();
					ps.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
			
		}
			
		
		/**
		 * Verifica reglas en datos
		 * @param tipoDescuentoSalario
		 * @throws ValidException
		 */
		private void checkValues(TipoDescuentoSalario tipoDescuentoSalario) throws ValidException {
			if (tipoDescuentoSalario.getNombre() == null) {
				throw new ValidException("Debe ingresar nombre del tipo de descuento.");
			}
			if (tipoDescuentoSalario.getNombre().trim().isEmpty()) {
				throw new ValidException("Debe ingresar nombre de tipod de descuento.");
			}

		}
		/**
		 * Verifica que se haya seleccionado la sucursal
		 * @param tipoDescuentoSalario
		 * @throws ValidException
		 */
		private void checkSeleccion(TipoDescuentoSalario tipoDescuentoSalario) throws ValidException {
			if (tipoDescuentoSalario == null) {
				throw new ValidException("Debe seleccionar un tipo de descuento.");
			}
			if (tipoDescuentoSalario.getCodTipoDescuento() == null) {
				throw new ValidException("Debe seleccionar un tipo de descuento.");
			}
		}
		/**
		 * Verifica si existe en la base de datos un tipo de descuento con el mismo nombre a la envia
		 * @param conn
		 * @param tipoDescuentoSalario
		 * @return
		 * @throws Exception
		 */
		private boolean isDuplicated(Connection conn, TipoDescuentoSalario tipoDescuentoSalario) throws Exception {
			boolean ret = false;
			String sSelect = "SELECT COUNT(1) FROM tiposdescuentossalarios ";
			sSelect += "WHERE nombre = ? ";
			if (tipoDescuentoSalario.getCodTipoDescuento() != null) {
				sSelect += " AND codtipodescuentosalario <> ? ";
			}
			PreparedStatement ps = conn.prepareStatement(sSelect);
			ps.setString(1, tipoDescuentoSalario.getNombre());
			if (tipoDescuentoSalario.getCodTipoDescuento() != null) {
				ps.setInt(2, tipoDescuentoSalario.getCodTipoDescuento());
			}
			ResultSet rs = ps.executeQuery();
			int cantidad = 0;
			if (rs.next()) {
				cantidad = rs.getInt(1);
			}
			rs.close();
			ps.close();
			if (cantidad > 0) {
				throw new ValidException("El tipo de descuento " + tipoDescuentoSalario.getNombre() + " ya existe.");
			}
			return ret;
		}
		
		/**
		 * Lista todas los tipos de descuentos personales desde la Base de Datos
		 * 
		 * @param conn
		 * @return
		 * @throws Exception
		 */
		public List<TipoDescuentoSalario> getTiposDescuentosPersonales (Connection conn) throws Exception {
			List<TipoDescuentoSalario> tiposDescuentosSalarios = new ArrayList<TipoDescuentoSalario>();
			PreparedStatement ps = conn.prepareStatement("SELECT codtipodescuentosalario FROM tiposdescuentossalarios WHERE formaaplicacion = 'P' ORDER BY nombre");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				tiposDescuentosSalarios.add(new TipoDescuentoSalario(conn, rs.getInt(1)));
			}
			rs.close();
			ps.close();
			return tiposDescuentosSalarios;
		}


}


