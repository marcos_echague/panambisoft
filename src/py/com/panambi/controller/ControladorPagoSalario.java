package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;

import py.com.panambi.bean.DetallePagoSalario;
import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.main.JFramePanambiMain;


public class ControladorPagoSalario {
	static Logger logger = Logger.getLogger(ControladorPagoSalario.class.getName());

	public ControladorPagoSalario() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Obtiene todos los pagos de salarios generados para el periodo vigente
	 * @param conn
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<PagoSalario> getPagosSalario(Connection conn, Date fechaDesde, Date fechaHasta, String tipo) throws Exception {
		ResultSet rs;
		PreparedStatement ps;
		
		List<PagoSalario> ret = new ArrayList<PagoSalario>();
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();

		String sSelect = "SELECT p.codpagoempleado,p.totaldescuento,p.totalpagado,p.fechapago,p.fechacreacion,p.salarionominal,";
			   sSelect +=" p.codempleado,p.estado,p.nrorecibo, e.nrodocumento, e.nombres||' '||e.apellidos as nombre, numerolista, fechadesde, fechahasta ";
			   sSelect +=" FROM pagosempleados p, empleados e ";
			   sSelect +=" WHERE p.codempleado = e.codempleado AND fechadesde >= ?  AND fechahasta <= ? AND tipo= ? order by codempleado asc";
		ps = conn.prepareStatement(sSelect);
		ps.setDate(1, (java.sql.Date) fechaDesde);
		ps.setDate(2, (java.sql.Date) fechaHasta);
		ps.setString(3, tipo);
		
		rs = ps.executeQuery();
		
		while (rs.next()) {
			PagoSalario pagoSalario = new PagoSalario();
			
			pagoSalario.setCodpagosalario(rs.getInt("codpagoempleado"));
			pagoSalario.setCodempleado(rs.getInt("codempleado"));	
			pagoSalario.setFechapago(rs.getDate("fechapago"));
			pagoSalario.setFechacreacion(rs.getDate("fechacreacion"));
			pagoSalario.setTotaldescuento(rs.getDouble("totaldescuento"));
			pagoSalario.setTotalpagado(rs.getDouble("totalpagado"));
			pagoSalario.setSalarionominal(rs.getDouble("salarionominal"));
			pagoSalario.setNumeroRecibo(rs.getInt("nrorecibo"));
			pagoSalario.setEstado(rs.getString("estado"));
			pagoSalario.setNrodocumento(rs.getString("nrodocumento"));
			pagoSalario.setNombre(rs.getString("nombre"));
			pagoSalario.setNumerolista(rs.getInt("numerolista"));
			pagoSalario.setFechaDesde(rs.getDate("fechadesde"));
			pagoSalario.setFechaHasta(rs.getDate("fechahasta"));
			detallePagoSalario = getDetallesPagoSalario(conn, pagoSalario.getCodpagosalario());
			pagoSalario.setDetallePagoSalario(detallePagoSalario);
			ret.add(pagoSalario);
		}
		
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Obtiene todos los pagos de salarios segun los parametros.
	 * @param conn
	 * @param empleado
	 * @param tipoPagoEmpleado
	 * @param estado
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<PagoSalario> getPagosEmpleados(Connection conn, Empleado empleado,  String tipoPagoEmpleado, Sucursal sucursal ,  String estado, Date fechaDesde, Date fechaHasta ) throws Exception {
		List<PagoSalario> pagosEmpleados = new ArrayList<PagoSalario>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT codpagoempleado , pe.codempleado AS codigoempleado FROM pagosempleados pe, empleados e ";
			sql+=" WHERE pe.codempleado = e.codempleado ";
			if(empleado!=null){
				sql+=" AND codigoempleado =  "+empleado.getCodEmpleado()+" ";
			}
			
			if(tipoPagoEmpleado!=null){
				sql+=" AND  tipo =  '"+tipoPagoEmpleado+"'";
			}
			
			if(sucursal!=null){
				sql+=" AND  codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(estado!=null){
				if(estado.equals("P")){
					sql+=" AND pe.estado = 'P' ";
				}else sql+=" AND pe.estado = 'NP' ";
			}
			
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY codpagoempleado DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				PagoSalario pagoEmpleado= new PagoSalario();
				pagoEmpleado =  getPagoSalario(conn, rs.getInt("codpagoempleado"));
				pagosEmpleados.add(pagoEmpleado);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return pagosEmpleados;
	}
	
	/*
	 * Obtiene un pago
	 */
	
	public PagoSalario getPagoSalario(Connection conn, Integer codPagoEmpleado) throws Exception {
		ResultSet rs;
		PreparedStatement ps;
		PagoSalario pagoSalario = null;
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();

		String sSelect = "SELECT p.codpagoempleado,p.totaldescuento,p.totalpagado,p.fechapago,p.fechacreacion,p.salarionominal,";
			   sSelect +=" p.codempleado,p.estado,p.nrorecibo, e.nrodocumento, e.nombres||' '||e.apellidos as nombre, numerolista,fechadesde,fechahasta,tipo ";
			   sSelect +=" FROM pagosempleados p, empleados e ";
			   sSelect +=" WHERE p.codempleado = e.codempleado AND p.codpagoempleado=? ";
		ps = conn.prepareStatement(sSelect);
		ps.setInt(1, codPagoEmpleado);
		rs = ps.executeQuery();
		
		while (rs.next()) {
			pagoSalario = new PagoSalario();
			pagoSalario.setCodpagosalario(rs.getInt("codpagoempleado"));
			pagoSalario.setCodempleado(rs.getInt("codempleado"));	
			pagoSalario.setFechapago(rs.getDate("fechapago"));
			pagoSalario.setFechacreacion(rs.getDate("fechacreacion"));
			pagoSalario.setTotaldescuento(rs.getDouble("totaldescuento"));
			pagoSalario.setTotalpagado(rs.getDouble("totalpagado"));
			pagoSalario.setSalarionominal(rs.getDouble("salarionominal"));
			pagoSalario.setNumeroRecibo(rs.getInt("nrorecibo"));
			pagoSalario.setEstado(rs.getString("estado"));
			pagoSalario.setNrodocumento(rs.getString("nrodocumento"));
			pagoSalario.setNombre(rs.getString("nombre"));
			pagoSalario.setTipo(rs.getString("tipo"));
			pagoSalario.setNumerolista(rs.getInt("numerolista"));
			pagoSalario.setFechaDesde(rs.getDate("fechadesde"));
			pagoSalario.setFechaHasta(rs.getDate("fechahasta"));
			detallePagoSalario = getDetallesPagoSalario(conn, pagoSalario.getCodpagosalario());
			pagoSalario.setDetallePagoSalario(detallePagoSalario);
		}
		
		rs.close();
		ps.close();
		return pagoSalario;
	}
	
	public void anularPagoEmpleado(Connection conn, PagoSalario pagoEmpleado, String comentarios) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE pagosempleados ";
			sql += " SET estado = 'NP', ";
			sql += "nrorecibo = null ";
			sql += " WHERE codpagoempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, pagoEmpleado.getCodpagosalario());
			ps.executeUpdate();
			ControladorPago controladorPago = new ControladorPago();
			controladorPago.anularPago(conn, pagoEmpleado, comentarios);
			
			
			
			if(pagoEmpleado.getTipo().equals("L")){
				Empleado emp = new ControladorEmpleado().getEmpleado(conn, pagoEmpleado.getCodempleado());
				emp.setEstado("A");
				emp.setFechaSalido(null);
				
				new ControladorEmpleado().modificarEmpleado(JFramePanambiMain.session.getConn(), emp);
				
				
				
				boolean poseeDeuda = false;
				boolean poseePago = false;
				DeudaEmpleado deuda = null;
				Pago pagoDeuda = null;
				deuda = new ControladorDeudaEmpleado().getDeudaEmpleadoPorPago(JFramePanambiMain.session.getConn(), pagoEmpleado.getCodpagosalario());
				if(deuda!=null){
					poseeDeuda=true;
					pagoDeuda = new ControladorPago().getPagoDeuda(JFramePanambiMain.session.getConn(), deuda);
					if(pagoDeuda!=null){
						poseePago = true;
					}
				}
				
				if(poseeDeuda){
					new ControladorPago().eliminarPago(JFramePanambiMain.session.getConn(), deuda);
				}
//				if(poseeDeuda){
//					new ControladorDeudaEmpleado().anularDeudaEmpleado(JFramePanambiMain.session.getConn(), deuda);
//				}
				
			}
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public List<DetallePagoSalario> getDetallesPagoSalario(Connection conn, Integer codpagosalario) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();
		DetallePagoSalario detalle;
		try {
			String sql = "SELECT codpagoempleado,nroitem,monto,concepto,porcentaje, codtipodescuentosalario, estado FROM detallepagosempleados WHERE codpagoempleado = ? order by nroitem asc";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codpagosalario);
			rs = ps.executeQuery();
			while (rs.next()) {
				detalle = new DetallePagoSalario();
				detalle.setCodpagosalario(rs.getInt("codpagoempleado"));
				detalle.setNroitem(rs.getInt("nroitem"));
				detalle.setMonto(rs.getDouble("monto"));
				detalle.setConcepto(rs.getString("concepto"));
				detalle.setPorcentaje(rs.getDouble("porcentaje"));
				detalle.setCodtipodescuentosalario(rs.getInt("codtipodescuentosalario"));
				detalle.setEstado(rs.getString("estado"));
				detallePagoSalario.add(detalle);
			} 

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return detallePagoSalario;
	}
	
	public PagoSalario getRealizarPagoSalario(Connection conn, PagoSalario pagoRealizado) throws Exception {
		
		ResultSet rs1 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PagoSalario ret = new PagoSalario();
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();
		
		try{
			
			String sUpdateDetalle = "UPDATE detallepagosempleados SET estado=? WHERE codpagoempleado=? and nroitem=? ";
			ps2 = conn.prepareStatement(sUpdateDetalle);
		
			detallePagoSalario = pagoRealizado.getDetallePagoSalario();
			for (int i = 0; i < detallePagoSalario.size(); i++) {
				
				DetallePagoSalario detalle = (DetallePagoSalario) detallePagoSalario.get(i);
				
				ps2.setString(1,detalle.getEstado());
				ps2.setInt(2,detalle.getCodpagosalario());
				ps2.setInt(3,detalle.getNroitem());
				ps2.executeUpdate();
			}
			
			
			Integer nroRecibo = new ControladorPlanPago().getNroRecibo(conn);
			
			String sUpdate = "UPDATE pagosempleados SET totaldescuento=?, totalpagado=?, fechapago=current_date, estado=?, nrorecibo=? WHERE codpagoempleado = ?";
			ps = conn.prepareStatement(sUpdate);
			ps.setDouble(1,pagoRealizado.getTotaldescuento());
			ps.setDouble(2,pagoRealizado.getTotalpagado());
			ps.setString(3,pagoRealizado.getEstado());
			ps.setInt(4,nroRecibo);
			ps.setInt(5,pagoRealizado.getCodpagosalario());
			ps.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs1);
			ConnectionManager.closeStatments(ps,ps1,ps2);
		}
		
		ret = getPagoSalario(conn, pagoRealizado.getCodpagosalario());
		
		return ret;
	}
	
	/*
	 * Genera la Planilla de pagos de salario
	 */
	
	public void generarPagoSalario(Connection conn, Date fechaDesde, Date fechaHasta) throws Exception {
		
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs4 = null;
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement psInsertarCabecera = null;
		PreparedStatement psConsultarExistencia = null;
		PreparedStatement psBorrarDetalles = null;
		
		java.sql.Date fechaDesdeSql = new java.sql.Date(fechaDesde.getTime());
		java.sql.Date fechaHastaSql = new java.sql.Date(fechaHasta.getTime());
		
		String sIntertCabecera = "INSERT INTO pagosempleados(codpagoempleado, totaldescuento, totalpagado, salarionominal,codempleado, fechacreacion, estado, fechadesde, fechahasta,numerolista, tipo)";
		sIntertCabecera += "VALUES (?, ?, ?, ?,?, current_date, ?,?,?,?,?);";
	
		String sConsultarExistencia = "SELECT * FROM pagosempleados WHERE tipo ='S' AND fechadesde = ? AND fechahasta = ? AND codempleado = ?";
		String sBorrarDetalle = "DELETE FROM detallepagosempleados WHERE codpagoempleado = ?";
		try {
			psInsertarCabecera = conn.prepareStatement(sIntertCabecera);
			
			psConsultarExistencia = conn.prepareStatement(sConsultarExistencia);
			psBorrarDetalles = conn.prepareStatement(sBorrarDetalle);
			conn.setAutoCommit(false);
			
		
			String sSelectEmpleado =  "SELECT codempleado, salario FROM empleados WHERE estado = 'A' ";
			ps2 = conn.prepareStatement(sSelectEmpleado);
			rs2 = ps2.executeQuery();
			Integer numerolista = 1;
			while (rs2.next()) {
				Integer nroitem = 1;
				Integer codpagoempleado = 0;
				Integer codempleado = rs2.getInt("codempleado");
				Double salarioNominal = rs2.getDouble("salario");
				Double totaldescuento = 0.0;
				Double totalpagado = 0.0;
				String estado = "NP";
				
				psConsultarExistencia.setDate(1, fechaDesdeSql);
				psConsultarExistencia.setDate(2, fechaHastaSql);
				psConsultarExistencia.setInt(3, codempleado);
				rs4 = psConsultarExistencia.executeQuery();
				if(rs4.next()){
					if(rs4.getString("estado").equals("NP")){
						codpagoempleado = rs4.getInt("codpagoempleado");
						psBorrarDetalles.setInt(1, codpagoempleado);
						psBorrarDetalles.execute();
						
						generarDetallePagoSalario(conn, codpagoempleado, salarioNominal, codempleado);

					}
					nroitem++;
				}else{
				
					String sSelectId = "SELECT NEXTVAL('pagosempleados_codpagoempleado_seq')";
					
					ps1 = conn.prepareStatement(sSelectId);
					rs1 = ps1.executeQuery();
					
					if (rs1.next()) {
						codpagoempleado = rs1.getInt(1);
					}
					//cargar cabecera de pago
					psInsertarCabecera.setInt(1, codpagoempleado);
					psInsertarCabecera.setDouble(2, totaldescuento);
					psInsertarCabecera.setDouble(3, totalpagado);
					psInsertarCabecera.setDouble(4, salarioNominal);
					psInsertarCabecera.setInt(5, codempleado);
					psInsertarCabecera.setString(6, estado);
					psInsertarCabecera.setDate(7, fechaDesdeSql);
					psInsertarCabecera.setDate(8, fechaHastaSql);
					psInsertarCabecera.setInt(9, numerolista);
					psInsertarCabecera.setString(10, "S");
					psInsertarCabecera.execute();
					numerolista++;			
					
					generarDetallePagoSalario(conn, codpagoempleado, salarioNominal, codempleado);
					nroitem++;
				}
				
			}
			
		conn.commit();
		
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs1, rs2, rs4);
			ConnectionManager.closeStatments(ps1, ps2, psInsertarCabecera,psConsultarExistencia,psBorrarDetalles );
			
		}

	}
	
	/*
	 * Inserta los detalles de pagos de salario y actualiza la cabecera del pago
	 */
	
	public void generarDetallePagoSalario(Connection conn, Integer codpagoempleado, Double salarioNominal, Integer codempleado)throws Exception{
		PreparedStatement psInsertarDetalle = null;
		ResultSet rs = null;
		ResultSet rs3 = null;
		PreparedStatement ps = null;
		PreparedStatement ps3 = null;
		PreparedStatement psActualizarCabecera = null;
		Double totaldescuento = 0.0;
		Double total = 0.0;
		Double totalpagado = 0.0;
		Integer nroitem =1;
		
		String sUpdate =  "UPDATE pagosempleados SET totaldescuento=?, totalpagado=? , salarionominal=? WHERE codpagoempleado = ?";
		String sInsertDetalle = "INSERT INTO detallepagosempleados(codpagoempleado,nroitem, monto, concepto, porcentaje, codtipodescuentosalario, estado, codtipobonificacion) VALUES (?,?, ?, ?, ?, ?, ?,?)";
		
		try {
				
			psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
			//cargar salario
			psInsertarDetalle.setInt(1, codpagoempleado);
			psInsertarDetalle.setInt(2, nroitem);
			psInsertarDetalle.setDouble(3, salarioNominal);
			psInsertarDetalle.setString(4, "SALARIO");
			psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
			psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
			psInsertarDetalle.setString(7,"A");
			psInsertarDetalle.setNull(8, java.sql.Types.INTEGER);
			psInsertarDetalle.execute();
			total += salarioNominal;
			//Cargar descuentos generales
			String sSelectDesc =  "SELECT codtipodescuentosalario, nombre,porcentaje,tipo, montofijo FROM tiposdescuentossalarios WHERE estado = 'A' AND formaaplicacion='M' ";
			ps = conn.prepareStatement(sSelectDesc);
			rs = ps.executeQuery();
			String concepto ="";
			Double porcentaje = 0.0;
			Double monto = 0.0;
			Integer codtipodescuentosalario = 0;
				
			while (rs.next()) {	
				nroitem++;
				codtipodescuentosalario = rs.getInt("codtipodescuentosalario");
				concepto = rs.getString("nombre");
				porcentaje = rs.getDouble("porcentaje");
				monto = rs.getDouble("montofijo");
				
				if(porcentaje != 0.0){
					monto = salarioNominal * (porcentaje/100);
				}
				
				totaldescuento += monto;
				psInsertarDetalle.setInt(1, codpagoempleado);
				psInsertarDetalle.setInt(2, nroitem);
				psInsertarDetalle.setDouble(3, monto);
				psInsertarDetalle.setString(4, concepto);
				psInsertarDetalle.setDouble(5, porcentaje);
				psInsertarDetalle.setInt(6, codtipodescuentosalario);
				psInsertarDetalle.setString(7,"A");
				psInsertarDetalle.setNull(8, java.sql.Types.INTEGER);
				psInsertarDetalle.execute();
				
			}
			//Cargar descuentos personales
			String sSelectDescPersonal =  "SELECT concepto, porcentaje, monto,codtipodescuentosalario, codbonificacion  FROM descuentossalarios WHERE codempleado = "+codempleado;
			ps3 = conn.prepareStatement(sSelectDescPersonal);
			rs3 = ps3.executeQuery();
				
			while (rs3.next()) {	
				nroitem++;
				codtipodescuentosalario = rs3.getInt("codtipodescuentosalario");
				Integer codTipoAsig = rs3.getInt("codbonificacion");;
				
				concepto = rs3.getString("concepto");
				porcentaje = rs3.getDouble("porcentaje");
				monto = rs3.getDouble("monto");
				if(porcentaje != null && porcentaje != 0.0){
					monto = salarioNominal * (porcentaje/100);
				}else{
					porcentaje = 0.0;
				}
				
				if(codtipodescuentosalario!= null){
					totaldescuento += monto;
				}else{
					total +=monto;
				}
				
				psInsertarDetalle.setInt(1, codpagoempleado);
				psInsertarDetalle.setInt(2, nroitem);
				psInsertarDetalle.setDouble(3, monto);
				psInsertarDetalle.setString(4, concepto);
				psInsertarDetalle.setDouble(5, porcentaje);
				
				if(codtipodescuentosalario != null && codtipodescuentosalario != 0){
					psInsertarDetalle.setInt(6, codtipodescuentosalario);
				}else{
					psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
				}
				
				psInsertarDetalle.setString(7,"A");
				
				if(codTipoAsig != null && codTipoAsig != 0){
					psInsertarDetalle.setInt(8, codTipoAsig);
				}else{
					psInsertarDetalle.setNull(8, java.sql.Types.INTEGER);
				}
				
				psInsertarDetalle.execute();
				
			}
			
			//actualizar la cabecera
			totalpagado = total - totaldescuento;
			
			psActualizarCabecera = conn.prepareStatement(sUpdate);
			psActualizarCabecera.setDouble(1,totaldescuento);
			psActualizarCabecera.setDouble(2,totalpagado);
			psActualizarCabecera.setDouble(3,salarioNominal);
			psActualizarCabecera.setInt(4,codpagoempleado);
			psActualizarCabecera.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs, rs3);
			ConnectionManager.closeStatments(psInsertarDetalle, psActualizarCabecera, ps, ps3 );
			
		}
	
		
	}
	
	/*
	 * Genera la Planilla de pagos de aguinaldo
	 */
	public void generarPagoAguinaldo(Connection conn, Date fechaDesde, Date fechaHasta) throws Exception {
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		ResultSet rs6 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement psConsultaAguinaldo = null;
		PreparedStatement psInsertarDetalle = null;
		PreparedStatement psInsertarCabecera = null;
		PreparedStatement psActualizarCabecera = null;
		PreparedStatement psConsultaExistenciaAguinaldo = null;
		PreparedStatement psBorrarDetalles = null;
		
		java.sql.Date fechaDesdeSql = new java.sql.Date(fechaDesde.getTime());
		java.sql.Date fechaHastaSql = new java.sql.Date(fechaHasta.getTime());
		

		String sIntertCabecera = "INSERT INTO pagosempleados(codpagoempleado, totaldescuento, totalpagado, salarionominal,codempleado, fechacreacion, estado, fechadesde, fechahasta,numerolista,tipo)";
		sIntertCabecera += "VALUES (?, ?, ?, ?,?, current_date, ?,?,?,?,?);";
		String sInsertDetalle = "INSERT INTO detallepagosempleados(codpagoempleado,nroitem, monto, concepto, porcentaje, codtipodescuentosalario, estado) VALUES (?,?, ?, ?, ?, ?, ?)";
		String sBorrarDetalles = "DELETE FROM detallepagosempleados WHERE codpagoempleado = ?";
		String sUpdateCabecera = "UPDATE pagosempleados set totalpagado = ?, salarionominal = ? where codpagoempleado = ?";
		
		try {
			conn.setAutoCommit(false);

				String sSelectEmpleado =  "SELECT codempleado, salario FROM empleados WHERE estado = 'A' ";//AND tipo = 'P'";
				ps2 = conn.prepareStatement(sSelectEmpleado);
				rs2 = ps2.executeQuery();
				Integer numerolista = 1;
				while (rs2.next()) {
					Integer nroitem = 1;
					Integer codpagoempleado = 0;
					Integer codempleado = rs2.getInt("codempleado");
					Double salarioNominal = rs2.getDouble("salario");
					Double totaldescuento = 0.0;
					Double totalpagado = 0.0;
					String estado = "NP";
					Integer mesesTrabajados = 0;
										
					//Trae todos los salarios del anho para ese empleado
					String sSelectAguinaldo = "SELECT COALESCE((sum(totalpagado)),0), count(*) FROM pagosempleados WHERE tipo = 'S' AND estado = 'P' AND codempleado = ? AND fechadesde >= ?  AND fechahasta <= ? ";
					psConsultaAguinaldo = conn.prepareStatement(sSelectAguinaldo);
					psConsultaAguinaldo.setInt(1, codempleado);
					psConsultaAguinaldo.setDate(2, fechaDesdeSql);
					psConsultaAguinaldo.setDate(3, fechaHastaSql);
					rs4 = psConsultaAguinaldo.executeQuery();
					
					if (rs4.next()) {
						
						totalpagado = (rs4.getDouble(1))/12;
						mesesTrabajados = rs4.getInt(2);
					}
					
					String concepto = "AGUINALDO POR "+mesesTrabajados.toString()+" MESES TRABAJADOS";
					// pregunto si ya existe, si no existe, debo crear, si existe debo preguntar si ya se pago
					
					String sSelectExisteAguinaldo = "SELECT * FROM pagosempleados WHERE tipo = 'A' AND fechadesde = ? AND fechahasta = ? AND codempleado = ? ";
					psConsultaExistenciaAguinaldo = conn.prepareStatement(sSelectExisteAguinaldo);
					psConsultaExistenciaAguinaldo.setDate(1, fechaDesdeSql);
					psConsultaExistenciaAguinaldo.setDate(2,fechaHastaSql);
					psConsultaExistenciaAguinaldo.setInt(3, codempleado);
					
					rs5 = psConsultaExistenciaAguinaldo.executeQuery();
					if(rs5.next()){
						if(rs5.getString("estado").equals("NP")){
							//Solo regenera si no se pago
							//Borra los detalles
							codpagoempleado = rs5.getInt("codpagoempleado");
							psBorrarDetalles = conn.prepareStatement(sBorrarDetalles);
							psBorrarDetalles.setInt(1, codpagoempleado);
							psBorrarDetalles.execute();
							
							//Crea los detalles
							psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
							psInsertarDetalle.setInt(1, codpagoempleado);
							psInsertarDetalle.setInt(2, nroitem);
							psInsertarDetalle.setDouble(3, totalpagado);
							psInsertarDetalle.setString(4, concepto);
							psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
							psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
							psInsertarDetalle.setString(7,"A");
							psInsertarDetalle.execute();
							
							psActualizarCabecera = conn.prepareStatement(sUpdateCabecera);
							psActualizarCabecera.setDouble(1, totalpagado);
							psActualizarCabecera.setDouble(2, salarioNominal);
							psActualizarCabecera.setInt(3, codpagoempleado);
							psActualizarCabecera.execute();
							
						}
						
					}else{
						
						String sSelectId = "SELECT NEXTVAL('pagosempleados_codpagoempleado_seq')";
						ps1 = conn.prepareStatement(sSelectId);
						rs1 = ps1.executeQuery();
						
						if (rs1.next()) {
							codpagoempleado = rs1.getInt(1);
						}
					
					
						psInsertarCabecera = conn.prepareStatement(sIntertCabecera);
						psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
						//cargar cabecera de pago
						psInsertarCabecera.setInt(1, codpagoempleado);
						psInsertarCabecera.setDouble(2, totaldescuento);
						psInsertarCabecera.setDouble(3, totalpagado);
						psInsertarCabecera.setDouble(4, salarioNominal);
						psInsertarCabecera.setInt(5, codempleado);
						psInsertarCabecera.setString(6, estado);
						psInsertarCabecera.setDate(7, fechaDesdeSql);
						psInsertarCabecera.setDate(8, fechaHastaSql);
						psInsertarCabecera.setInt(9, numerolista);
						psInsertarCabecera.setString(10, "A");
						psInsertarCabecera.execute();
						numerolista++;
						//cargar salario
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, nroitem);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, concepto);
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						nroitem++;
					}
				}
	
			conn.commit();
		
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs, rs1, rs2, rs3, rs4, rs5, rs6);
			ConnectionManager.closeStatments(ps, ps1, ps2, ps3,psInsertarDetalle,psInsertarCabecera,psActualizarCabecera,psConsultaAguinaldo, psBorrarDetalles,psConsultaExistenciaAguinaldo );
		}

	}
	
	public PagoSalario getPagoSalarioPorEmpleado(Connection conn, Integer codEmpleado, Date fechadesde, Date fechahasta) throws Exception {
		ResultSet rs;
		PreparedStatement ps;
		PagoSalario pagoSalario = new PagoSalario();
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();

		String sSelect = "SELECT p.codpagoempleado,p.totaldescuento,p.totalpagado,p.fechapago,p.fechacreacion,p.salarionominal,";
			   sSelect +=" p.codempleado,p.estado,p.nrorecibo, e.nrodocumento, e.nombres||' '||e.apellidos as nombre, numerolista,fechadesde,fechahasta ";
			   sSelect +=" FROM pagosempleados p, empleados e ";
			   sSelect +=" WHERE p.codempleado = e.codempleado AND p.codpagoempleado=? AND tipo ='S'";
		ps = conn.prepareStatement(sSelect);
		ps.setInt(1, codEmpleado);
		rs = ps.executeQuery();
		
		while (rs.next()) {
			
			pagoSalario.setCodpagosalario(rs.getInt("codpagoempleado"));
			pagoSalario.setCodempleado(rs.getInt("codempleado"));	
			pagoSalario.setFechapago(rs.getDate("fechapago"));
			pagoSalario.setFechacreacion(rs.getDate("fechacreacion"));
			pagoSalario.setTotaldescuento(rs.getDouble("totaldescuento"));
			pagoSalario.setTotalpagado(rs.getDouble("totalpagado"));
			pagoSalario.setSalarionominal(rs.getDouble("salarionominal"));
			pagoSalario.setNumeroRecibo(rs.getInt("nrorecibo"));
			pagoSalario.setEstado(rs.getString("estado"));
			pagoSalario.setNrodocumento(rs.getString("nrodocumento"));
			pagoSalario.setNombre(rs.getString("nombre"));
			pagoSalario.setNumerolista(rs.getInt("numerolista"));
			pagoSalario.setFechaDesde(rs.getDate("fechadesde"));
			pagoSalario.setFechaHasta(rs.getDate("fechahasta"));
			detallePagoSalario = getDetallesPagoSalario(conn, pagoSalario.getCodpagosalario());
			pagoSalario.setDetallePagoSalario(detallePagoSalario);
		}
		
		rs.close();
		ps.close();
		return pagoSalario;
	}
	
	/*
	 * Obtiene la liquidacion generada para el empleado indicado
	 */
	
	public PagoSalario getPagoLiquidacion(Connection conn, Integer codEmpleado) throws Exception {
		ResultSet rs;
		PreparedStatement ps;
		PagoSalario pagoSalario = null;
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();

		String sSelect = "SELECT p.codpagoempleado,p.totaldescuento,p.totalpagado,p.fechapago,p.fechacreacion,p.salarionominal,";
			   sSelect +=" p.codempleado,p.estado,p.nrorecibo, e.nrodocumento, e.nombres||' '||e.apellidos as nombre, numerolista,fechadesde,fechahasta ";
			   sSelect +=" FROM pagosempleados p, empleados e ";
			   sSelect +=" WHERE p.codempleado = e.codempleado AND p.codempleado = ? AND p.tipo ='L'";
		ps = conn.prepareStatement(sSelect);
		ps.setInt(1, codEmpleado);
		rs = ps.executeQuery();
		
		while (rs.next()) {
			pagoSalario = new PagoSalario();
			pagoSalario.setCodpagosalario(rs.getInt("codpagoempleado"));
			pagoSalario.setCodempleado(rs.getInt("codempleado"));	
			pagoSalario.setFechapago(rs.getDate("fechapago"));
			pagoSalario.setFechacreacion(rs.getDate("fechacreacion"));
			pagoSalario.setTotaldescuento(rs.getDouble("totaldescuento"));
			pagoSalario.setTotalpagado(rs.getDouble("totalpagado"));
			pagoSalario.setSalarionominal(rs.getDouble("salarionominal"));
			pagoSalario.setNumeroRecibo(rs.getInt("nrorecibo"));
			pagoSalario.setEstado(rs.getString("estado"));
			pagoSalario.setNrodocumento(rs.getString("nrodocumento"));
			pagoSalario.setNombre(rs.getString("nombre"));
			pagoSalario.setNumerolista(rs.getInt("numerolista"));
			pagoSalario.setFechaDesde(rs.getDate("fechadesde"));
			pagoSalario.setFechaHasta(rs.getDate("fechahasta"));
			detallePagoSalario = getDetallesPagoSalario(conn, pagoSalario.getCodpagosalario());
			pagoSalario.setDetallePagoSalario(detallePagoSalario);
		}
		
		rs.close();
		ps.close();
		return pagoSalario;
	}
	
	/*
	 * Genera la Planilla de pagos de liquidacion
	 */
	public void generarPagoLiquidacion(Connection conn, Integer codempleado, Integer antiguedad, Integer mesesantiguedad, Integer preaviso, Date fechaIngreso, 
			Boolean renuncia, Integer tiporenuncia, Double salarioNominal, Integer vacaciones) throws Exception {
		
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement psConsultaAguinaldo = null;
		PreparedStatement psInsertarDetalle = null;
		PreparedStatement psInsertarCabecera = null;
		PreparedStatement psActualizarCabecera = null;
		PreparedStatement psActualizarDetalle = null;
		ControladorDeudaEmpleado controladoDeudaempleado = new ControladorDeudaEmpleado();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date fechaDesdeT = cal.getTime();
		java.sql.Date fechaDesde = new java.sql.Date(fechaDesdeT.getTime());

		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal
		Date fechaHastaT = cal.getTime();
		java.sql.Date fechaHasta = new java.sql.Date(fechaHastaT.getTime());
		
		if(antiguedad == 0){
			if(mesesantiguedad >= 6){
				antiguedad = 1;
			}	
		}
		
		if(antiguedad == 9){
			if(mesesantiguedad >= 5){
				antiguedad = 10;
			}	
		}
		
		PagoSalario pagoAguinaldo = null;
		PagoSalario pagoSalario = null;
		Integer codpagoempleado = 0;
		Double totaldescuento = 0.0;
		Double totalpagado = 0.0;
		String estado = "NP";
		Integer mesesTrabajados = 0;
		java.sql.Date fechaIngresoSql = new java.sql.Date(fechaIngreso.getTime());
		Double totalPagadoAlEmpleado = 0.0;
		DateTime fechaInicio = new DateTime(fechaHasta.getTime());
		cal = Calendar.getInstance();
		DateTime fechaActual = new DateTime(cal.getTime());
		int days = Days.daysBetween(fechaInicio, fechaActual).getDays();
		
		//insertar
		String sIntertCabecera = "INSERT INTO pagosempleados(codpagoempleado, totaldescuento, totalpagado, salarionominal,codempleado, fechacreacion, estado, fechadesde, fechahasta,numerolista,tipo)";
		sIntertCabecera += "VALUES (?, ?, ?, ?,?, current_date, ?,current_date,?,?,'L')";
		String sInsertDetalle = "INSERT INTO detallepagosempleados(codpagoempleado,nroitem, monto, concepto, porcentaje, codtipodescuentosalario, estado) VALUES (?,?, ?, ?, ?, ?, ?)";
		String sUpdateCabecera = "UPDATE pagosempleados SET totalpagado = ?, totaldescuento =? WHERE codpagoempleado = ? AND tipo ='L'";
		String sAguinaldo = "SELECT * FROM pagosempleados WHERE tipo = 'A' AND codempleado = ? AND (SELECT EXTRACT(YEAR FROM (fechadesde)))= (SELECT EXTRACT(YEAR FROM (current_date)))";
		String deleteDetails = "DELETE FROM detallepagosempleados WHERE codpagoempleado =?";
		String sSalario = "SELECT * FROM pagosempleados WHERE tipo ='S' and fechadesde = ? and fechahasta = ? and codempleado ="+codempleado;;
		
		String sSelect = "SELECT * from pagosempleados where tipo ='L' and codempleado ="+codempleado;
		
		ps = conn.prepareStatement(sSelect);
		rs = ps.executeQuery();
		
		psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
		if(rs.next()) {
			
			codpagoempleado = rs.getInt("codpagoempleado");
			ps5 = conn.prepareStatement(deleteDetails);
			ps5.setInt(1,codpagoempleado);
			ps5.executeUpdate();
			
			//INSERTA SALARIO POR DIAS TRABAJADOS
			totalpagado = (days * (salarioNominal/31)) - (days * (salarioNominal/31))*0.9;;
			psInsertarDetalle.setInt(1, codpagoempleado);
			psInsertarDetalle.setInt(2, 1);
			psInsertarDetalle.setDouble(3, totalpagado);
			psInsertarDetalle.setString(4, "SALARIO A LA FECHA CON DESCUENTO DE IPS");
			psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
			psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
			psInsertarDetalle.setString(7,"A");
			psInsertarDetalle.execute();
			
			
			totalPagadoAlEmpleado += totalpagado;
			
			ps2 = conn.prepareStatement(sAguinaldo);
			ps2.setInt(1, codempleado);
			rs2 = ps2.executeQuery();
			totalpagado = 0.0;
			
			if(rs2.next()){
				String estadoAguinaldo = rs2.getString("estado");
				
				if(estadoAguinaldo.equals("NP")){
					
					Integer codpagoempleadoAguinaldo =  rs2.getInt("codpagoempleado");
					pagoAguinaldo = generarUnPagoAguinaldo(conn, codempleado, false, codpagoempleadoAguinaldo);
					totalpagado = pagoAguinaldo.getTotalpagado();
					
				}else{
					
					totalpagado = 0.0;
				}
				
			}else{
				
				pagoAguinaldo = generarUnPagoAguinaldo(conn, codempleado, true, 0);
				totalpagado = pagoAguinaldo.getTotalpagado();
				
				if(totalpagado ==null){
					totalpagado = 0.0;
				}
			}
			//INSERTA AGUINALDO
			psInsertarDetalle.setInt(1, codpagoempleado);
			psInsertarDetalle.setInt(2, 2);
			psInsertarDetalle.setDouble(3, totalpagado);
			psInsertarDetalle.setString(4, "AGUINALDO");
			psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
			psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
			psInsertarDetalle.setString(7,"A");
			psInsertarDetalle.execute();
			
			totalPagadoAlEmpleado += totalpagado;
			
			//INSERTA PAGO POR VACACIONES
			totalpagado = vacaciones * (salarioNominal/31);
			psInsertarDetalle.setInt(1, codpagoempleado);
			psInsertarDetalle.setInt(2, 3);
			psInsertarDetalle.setDouble(3, totalpagado);
			psInsertarDetalle.setString(4, "VACACIONES");
			psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
			psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
			psInsertarDetalle.setString(7,"A");
			psInsertarDetalle.execute();
			
			totalPagadoAlEmpleado += totalpagado;
			
			if(renuncia == true){
				if(tiporenuncia ==1){
					//renuncia justificada
					totalpagado = (salarioNominal/2 * antiguedad);
										
					psInsertarDetalle.setInt(1, codpagoempleado);
					psInsertarDetalle.setInt(2, 4);
					psInsertarDetalle.setDouble(3, totalpagado);
					psInsertarDetalle.setString(4, "INDEMINIZACION");
					psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
					psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
					psInsertarDetalle.setString(7,"A");
					psInsertarDetalle.execute();
					
					totalPagadoAlEmpleado += totalpagado;
					
					totalpagado = ((salarioNominal/31)*preaviso)/2;
					
					psInsertarDetalle.setInt(1, codpagoempleado);
					psInsertarDetalle.setInt(2, 5);
					psInsertarDetalle.setDouble(3, totalpagado);
					psInsertarDetalle.setString(4, "FALTA DE PRE AVISO");
					psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
					psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
					psInsertarDetalle.setString(7,"A");
					psInsertarDetalle.execute();
					
					totalPagadoAlEmpleado += totalpagado;
					
				}else if (tiporenuncia == 2){
					//renuncia injustificada y normal solo se le cobra el preaviso si no cumplio
					if(preaviso != 0){
						//debe cargarse en deuda
						Double monto = ((salarioNominal/31)*preaviso);
						
						DeudaEmpleado deudapreaviso = controladoDeudaempleado.getDeudaEmpleadoPorPago(conn,codempleado);
						
						if(deudapreaviso == null){
							deudapreaviso = new DeudaEmpleado();
							deudapreaviso.setConcepto("FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
							deudapreaviso.setMonto(monto);
							deudapreaviso.setPagoEmpleado(getPagoLiquidacion(conn, codempleado));
							deudapreaviso.setEstado("NP");
							controladoDeudaempleado.insertarDeudaEmpleado(conn,deudapreaviso);
						}else{
							deudapreaviso.setConcepto("FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
							deudapreaviso.setMonto(monto);
							deudapreaviso.setPagoEmpleado(getPagoLiquidacion(conn, codempleado));
							deudapreaviso.setEstado("NP");
							controladoDeudaempleado.modificarDeudaEmpleado(conn,deudapreaviso);
						}
						
						
						/*psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, 3);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, "FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						
						totaldescuento =+ totalpagado;*/
					}
					
				}
				
			}else{
				//DESPIDO
				if(tiporenuncia==1){
					//Si es injustificada debo pagarle pre aviso e indemnizacion
					totalpagado = (salarioNominal/2) * antiguedad;
					psInsertarDetalle.setInt(1, codpagoempleado);
					psInsertarDetalle.setInt(2, 4);
					psInsertarDetalle.setDouble(3, totalpagado);
					psInsertarDetalle.setString(4, "INDEMNIZACION A FAVOR DEL EMPLEADO");
					psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
					psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
					psInsertarDetalle.setString(7,"A");
					psInsertarDetalle.execute();
					
					totalPagadoAlEmpleado += totalpagado;
					
					if(preaviso!= 0){
						totalpagado = ((salarioNominal/31)*preaviso)/2;
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, 5);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, "FALTA DE PRE AVISO");
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						
						totaldescuento += totalpagado;
					}
				}
				
			}
			
			psActualizarCabecera = conn.prepareStatement(sUpdateCabecera);
			psActualizarCabecera.setDouble(1, totalPagadoAlEmpleado);
			psActualizarCabecera.setDouble(2, totaldescuento);
			psActualizarCabecera.setInt(3, codpagoempleado);
			psActualizarCabecera.executeUpdate();
			
		}else{
			
			try {
				
				String sSelectId = "SELECT NEXTVAL('pagosempleados_codpagoempleado_seq')";
				ps1 = conn.prepareStatement(sSelectId);
				rs1 = ps1.executeQuery();
				
				if (rs1.next()) {
					codpagoempleado = rs1.getInt(1);
				}
				psInsertarCabecera = conn.prepareStatement(sIntertCabecera);
				
				//cargar cabecera de pago
				psInsertarCabecera.setInt(1, codpagoempleado);
				psInsertarCabecera.setDouble(2, totaldescuento);
				psInsertarCabecera.setDouble(3, totalpagado);
				psInsertarCabecera.setDouble(4, salarioNominal);
				psInsertarCabecera.setInt(5, codempleado);
				psInsertarCabecera.setString(6, estado);
				psInsertarCabecera.setDate(7, fechaIngresoSql);
				psInsertarCabecera.setInt(8, 1);
				psInsertarCabecera.execute();
				
				//INSERTA SALARIO POR DIAS TRABAJADOS
				totalpagado = (days * (salarioNominal/31)) - (days * (salarioNominal/31))*0.9;;
				psInsertarDetalle.setInt(1, codpagoempleado);
				psInsertarDetalle.setInt(2, 1);
				psInsertarDetalle.setDouble(3, totalpagado);
				psInsertarDetalle.setString(4, "SALARIO A LA FECHA CON DESCUENTO DE IPS");
				psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
				psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
				psInsertarDetalle.setString(7,"A");
				psInsertarDetalle.execute();
				
				
				totalPagadoAlEmpleado += totalpagado;
				
				totalpagado = 0.0;
				ps2 = conn.prepareStatement(sAguinaldo);
				rs2 = ps2.executeQuery();
				if(rs2.next()){
					String estadoAguinaldo = rs2.getString("estado");
					
					if(estadoAguinaldo.equals("NP")){
						Integer codpagoempleadoAguinaldo =  rs2.getInt("codpagoempleado");
						pagoAguinaldo = generarUnPagoAguinaldo(conn, codempleado, false, codpagoempleadoAguinaldo);
						totalpagado = pagoAguinaldo.getTotalpagado();
						
					}else{
						
						totalpagado = 0.0;
					}
					
				}else{
					
					pagoAguinaldo = generarUnPagoAguinaldo(conn, codempleado, true, 0);
					totalpagado = pagoAguinaldo.getTotalpagado();
					
					if(totalpagado ==null){
						totalpagado = 0.0;
					}
				}
				//INSERTA AGUINALDO
				psInsertarDetalle.setInt(1, codpagoempleado);
				psInsertarDetalle.setInt(2, 2);
				psInsertarDetalle.setDouble(3, totalpagado);
				psInsertarDetalle.setString(4, "AGUINALDO");
				psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
				psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
				psInsertarDetalle.setString(7,"A");
				psInsertarDetalle.execute();
				
				totalPagadoAlEmpleado =+ totalpagado;
				
				//INSERTA PAGO POR VACACIONES
				totalpagado = vacaciones * (salarioNominal/31);
				psInsertarDetalle.setInt(1, codpagoempleado);
				psInsertarDetalle.setInt(2, 3);
				psInsertarDetalle.setDouble(3, totalpagado);
				psInsertarDetalle.setString(4, "VACACIONES");
				psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
				psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
				psInsertarDetalle.setString(7,"A");
				psInsertarDetalle.execute();
				
				totalPagadoAlEmpleado =+ totalpagado;
						
				if(renuncia == true){
					if(tiporenuncia ==1){
						//renuncia justificada
						totalpagado = (salarioNominal/2 * antiguedad);
											
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, 4);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, "INDEMINIZACION");
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						
						totalPagadoAlEmpleado =+ totalpagado;
						
						totalpagado = ((salarioNominal/31)*preaviso)/2;
						
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, 5);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, "FALTA DE PRE AVISO");
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						
						totalPagadoAlEmpleado =+ totalpagado;
						
					}else if (tiporenuncia == 2){
						//renuncia injustificada y normal solo se le cobra el preaviso si no cumplio
						if(preaviso != 0){
							/*totalpagado = ((salarioNominal/31)*preaviso);
							psInsertarDetalle.setInt(1, codpagoempleado);
							psInsertarDetalle.setInt(2, 4);
							psInsertarDetalle.setDouble(3, totalpagado);
							psInsertarDetalle.setString(4, "FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
							psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
							psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
							psInsertarDetalle.setString(7,"A");
							psInsertarDetalle.execute();
							
							totaldescuento =+ totalpagado;*/
							Double monto = ((salarioNominal/31)*preaviso);
							DeudaEmpleado deudapreaviso = controladoDeudaempleado.getDeudaEmpleadoPorPago(conn,codempleado);
							
							if(deudapreaviso == null){
								deudapreaviso = new DeudaEmpleado();
								deudapreaviso.setConcepto("FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
								deudapreaviso.setMonto(monto);
								deudapreaviso.setPagoEmpleado(getPagoLiquidacion(conn, codempleado));
								deudapreaviso.setEstado("NP");
								controladoDeudaempleado.insertarDeudaEmpleado(conn,deudapreaviso);
							}else{
								deudapreaviso.setConcepto("FALTA DE PRE AVISO A FAVOR DE LA EMPRESA");
								deudapreaviso.setMonto(monto);
								deudapreaviso.setPagoEmpleado(getPagoLiquidacion(conn, codempleado));
								deudapreaviso.setEstado("NP");
								controladoDeudaempleado.modificarDeudaEmpleado(conn,deudapreaviso);
							}
							
						}
						
					}
					
				}else{
					//DESPIDO
					if(tiporenuncia==1){
						//Si es injustificada debo pagarle pre aviso e indemnizacion
						totalpagado = (salarioNominal/2) * antiguedad;
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, 4);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, "INDEMNIZACION A FAVOR DEL EMPLEADO");
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
						
						totalPagadoAlEmpleado =+ totalpagado;
						
						if(preaviso!= 0){
							totalpagado = ((salarioNominal/31)*preaviso)/2;
							psInsertarDetalle.setInt(1, codpagoempleado);
							psInsertarDetalle.setInt(2, 5);
							psInsertarDetalle.setDouble(3, totalpagado);
							psInsertarDetalle.setString(4, "FALTA DE PRE AVISO");
							psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
							psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
							psInsertarDetalle.setString(7,"A");
							psInsertarDetalle.execute();
							
							totaldescuento += totalpagado;
						}
					}
					
				}
				
				psActualizarCabecera = conn.prepareStatement(sUpdateCabecera);
				psActualizarCabecera.setDouble(1, totalPagadoAlEmpleado);
				psActualizarCabecera.setDouble(2, totaldescuento);
				psActualizarCabecera.setInt(3, codpagoempleado);
				psActualizarCabecera.executeUpdate();
							
			} catch (Exception e) {
				throw e;
			} finally {
				ConnectionManager.closeResultSets(rs, rs1, rs2, rs3, rs4);
				ConnectionManager.closeStatments(ps, ps1, ps2, ps3,psInsertarDetalle,psInsertarCabecera,psActualizarCabecera,psConsultaAguinaldo );
				
			}
		}
	
	}
	
	@SuppressWarnings("unused")
	public PagoSalario generarUnPagoAguinaldo(Connection conn, Integer codempleado, Boolean insert, Integer codpagoEmpleado) throws Exception {
		PagoSalario pago;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement psConsultaAguinaldo = null;
		PreparedStatement psInsertarDetalle = null;
		PreparedStatement psInsertarCabecera = null;
		PreparedStatement psActualizarCabecera = null;
		PreparedStatement psEliminarDetalle = null;
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDateOfPreviousMonth = cal.getTime();
		
		cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		Date lastDateOfPreviousMonth = cal.getTime();
		
		java.sql.Date fechaDesdeSql = new java.sql.Date(firstDateOfPreviousMonth.getTime());
		java.sql.Date fechaHastaSql = new java.sql.Date(lastDateOfPreviousMonth.getTime());
		

		String sIntertCabecera = "INSERT INTO pagosempleados(codpagoempleado, totaldescuento, totalpagado, salarionominal,codempleado, fechacreacion, estado, fechadesde, fechahasta,numerolista,tipo)";
		sIntertCabecera += "VALUES (?, ?, ?, ?,?, current_date, ?,?,?,?,?);";
		String sInsertDetalle = "INSERT INTO detallepagosempleados(codpagoempleado,nroitem, monto, concepto, porcentaje, codtipodescuentosalario, estado) VALUES (?,?, ?, ?, ?, ?, ?)";
		
		String sUpdateCabecera = "UPDATE pagosempleados set totalpagado = ?, salarionominal = ? where codpagoempleado = ?";
		String sDeleteDetails = "DELETE from detallepagosempleados where codpagoempleado = ?";
		
		try {
			conn.setAutoCommit(false);

				String sSelectEmpleado =  "SELECT salario FROM empleados WHERE estado = 'A' and codempleado="+codempleado;
				ps2 = conn.prepareStatement(sSelectEmpleado);
				rs2 = ps2.executeQuery();
				
				if (rs2.next()) {
					Integer numerolista = 1;
					Integer nroitem = 1;
					Integer codpagoempleado = 0;
					Double salarioNominal = rs2.getDouble("salario");
					Double totaldescuento = 0.0;
					Double totalpagado = 0.0;
					String estado = "NP";
					Integer mesesTrabajados = 0;
					
					//Trae todos los salarios del anho para ese empleado
					String sSelectAguinaldo = "SELECT COALESCE((sum(totalpagado)),0), count(*) FROM pagosempleados WHERE tipo = 'S' AND estado = 'P' AND codempleado = ? AND fechadesde >= ?  AND fechahasta <= ? ";
					psConsultaAguinaldo = conn.prepareStatement(sSelectAguinaldo);
					psConsultaAguinaldo.setInt(1, codempleado);
					psConsultaAguinaldo.setDate(2, fechaDesdeSql);
					psConsultaAguinaldo.setDate(3, fechaHastaSql);
					
					rs4 = psConsultaAguinaldo.executeQuery();
					
					if (rs4.next()) {
						totalpagado = (rs4.getDouble(1))/12;
						mesesTrabajados = rs4.getInt(2);
						if(totalpagado==null){
							totalpagado=0.0;
						}
					}
					
					if(insert==true){
						
						String sSelectId = "SELECT NEXTVAL('pagosempleados_codpagoempleado_seq')";
						ps1 = conn.prepareStatement(sSelectId);
						rs1 = ps1.executeQuery();
						
						if (rs1.next()) {
							codpagoempleado = rs1.getInt(1);
							codpagoEmpleado = codpagoempleado;
						}
						
						String concepto = "AGUINALDO POR "+mesesTrabajados+" MESES TRABAJADOS";
						psInsertarCabecera = conn.prepareStatement(sIntertCabecera);
						psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
						//cargar cabecera de pago
						psInsertarCabecera.setInt(1, codpagoempleado);
						psInsertarCabecera.setDouble(2, totaldescuento);
						psInsertarCabecera.setDouble(3, totalpagado);
						psInsertarCabecera.setDouble(4, salarioNominal);
						psInsertarCabecera.setInt(5, codempleado);
						psInsertarCabecera.setString(6, estado);
						psInsertarCabecera.setDate(7, fechaDesdeSql);
						psInsertarCabecera.setDate(8, fechaHastaSql);
						psInsertarCabecera.setInt(9, numerolista);
						psInsertarCabecera.setString(10, "A");
						psInsertarCabecera.execute();
						
						//cargar salario
						psInsertarDetalle.setInt(1, codpagoempleado);
						psInsertarDetalle.setInt(2, nroitem);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, concepto);
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
							
					
					}else{
						//actualizo
						psEliminarDetalle = conn.prepareStatement(sDeleteDetails);
						psEliminarDetalle.setInt(1, codpagoEmpleado);
						psEliminarDetalle.execute();
						
						//actualizar cabecera
						psActualizarCabecera = conn.prepareStatement(sUpdateCabecera);
						psActualizarCabecera.setDouble(1, totalpagado);
						psActualizarCabecera.setDouble(2, salarioNominal);
						psActualizarCabecera.setInt(3, codpagoempleado);
						psActualizarCabecera.execute();
						
						//cargar aguinaldo
						String concepto = "AGUINALDO POR "+mesesTrabajados+" MESES TRABAJADOS";
						psInsertarDetalle = conn.prepareStatement(sInsertDetalle);
						psInsertarDetalle.setInt(1, codpagoEmpleado);
						psInsertarDetalle.setInt(2, nroitem);
						psInsertarDetalle.setDouble(3, totalpagado);
						psInsertarDetalle.setString(4, concepto);
						psInsertarDetalle.setNull(5, java.sql.Types.INTEGER);
						psInsertarDetalle.setNull(6, java.sql.Types.INTEGER);
						psInsertarDetalle.setString(7,"A");
						psInsertarDetalle.execute();
	
					}	
			}
			
			conn.commit();
			pago = getPagoSalario(conn, codpagoEmpleado);
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs, rs1, rs2, rs3, rs4);
			ConnectionManager.closeStatments(ps, ps1, ps2, ps3,psInsertarDetalle,psInsertarCabecera,psActualizarCabecera,psConsultaAguinaldo );
			
		}
		
		return pago;
	}
	
	public PagoSalario getPagoAguinaldo(Connection conn, Integer codpagoempleado) throws Exception {
		ResultSet rs;
		PreparedStatement ps;
		PagoSalario pagoSalario = new PagoSalario();
		List<DetallePagoSalario> detallePagoSalario = new ArrayList<DetallePagoSalario>();

		String sSelect = "SELECT p.codpagoempleado,p.totaldescuento,p.totalpagado,p.fechapago,p.fechacreacion,p.salarionominal,";
			   sSelect +=" p.codempleado,p.estado,p.nrorecibo, e.nrodocumento, e.nombres||' '||e.apellidos as nombre, numerolista,fechadesde,fechahasta ";
			   sSelect +=" FROM pagosempleados p, empleados e ";
			   sSelect +=" WHERE p.codempleado = e.codempleado AND p.codpagoempleado = ? AND tipo ='A'";
		ps = conn.prepareStatement(sSelect);
		ps.setInt(1, codpagoempleado);
		rs = ps.executeQuery();
		
		while (rs.next()) {
			
			pagoSalario.setCodpagosalario(rs.getInt("codpagoempleado"));
			pagoSalario.setCodempleado(rs.getInt("codempleado"));	
			pagoSalario.setFechapago(rs.getDate("fechapago"));
			pagoSalario.setFechacreacion(rs.getDate("fechacreacion"));
			pagoSalario.setTotaldescuento(rs.getDouble("totaldescuento"));
			pagoSalario.setTotalpagado(rs.getDouble("totalpagado"));
			pagoSalario.setSalarionominal(rs.getDouble("salarionominal"));
			pagoSalario.setNumeroRecibo(rs.getInt("nrorecibo"));
			pagoSalario.setEstado(rs.getString("estado"));
			pagoSalario.setNrodocumento(rs.getString("nrodocumento"));
			pagoSalario.setNombre(rs.getString("nombre"));
			pagoSalario.setNumerolista(rs.getInt("numerolista"));
			pagoSalario.setFechaDesde(rs.getDate("fechadesde"));
			pagoSalario.setFechaHasta(rs.getDate("fechahasta"));
			detallePagoSalario = getDetallesPagoSalario(conn, pagoSalario.getCodpagosalario());
			pagoSalario.setDetallePagoSalario(detallePagoSalario);
		}
		
		rs.close();
		ps.close();
		return pagoSalario;
	}

}
