package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Banco;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorBanco extends ControladorPanambi {

	/**
	 * Inserta un nuevo banco en la base de datos.
	 * @param conn
	 * @param banco
	 * @throws Exception
	 */
	public void insertarBanco(Connection conn, Banco banco) throws Exception {
		try {
			checkValues(banco);
			isDuplicated(conn, banco);
			String sql = "INSERT INTO bancos ";
			sql += " (descripcion, estado ) ";
			sql += " VALUES (?, ? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, banco.getDescripcion());
			ps.setString(2, banco.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Actualiza datos del banco
	 * 
	 * @param connsw
	 * @param formaPago
	 * @throws Exception
	 */
	public void modificarBanco(Connection conn, Banco banco) throws Exception {
		try {
			checkSeleccion(banco);
			checkValues(banco);
			isDuplicated(conn, banco);
			String sql = "UPDATE bancos ";
			sql += " SET descripcion = ?, ";
			sql += " estado = ? ";
			sql += " WHERE codbanco = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, banco.getDescripcion());
			ps.setString(2, banco.getEstado());
			ps.setInt(3, banco.getCodbanco());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Elimina un banco.
	 * @param conn
	 * @param banco
	 * @throws Exception
	 */
	public void borrarBanco(Connection conn, Banco banco) throws Exception {
		try {
			checkSeleccion(banco);
			String sql = "DELETE FROM bancos ";
			sql += " WHERE codbanco = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, banco.getCodbanco());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	
	/**
	 * Lista todas los bancos activos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Banco> getBancosActivos(Connection conn) throws Exception {
		List<Banco> bancos = new ArrayList<Banco>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bancos WHERE estado = 'A' ORDER BY descripcion");
			rs = ps.executeQuery();
			while (rs.next()) {
				Banco banco = new Banco();
				banco.setCodbanco(rs.getInt("codbanco"));
				banco.setDescripcion(rs.getString("descripcion"));
				banco.setEstado(rs.getString("estado"));
				bancos.add(banco);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return bancos;
	}
	
	/**
	 * Lista todas los bancos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Banco> getBancos(Connection conn) throws Exception {
		List<Banco> bancos = new ArrayList<Banco>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bancos");
			rs = ps.executeQuery();
			while (rs.next()) {
				Banco banco = new Banco();
				banco.setCodbanco(rs.getInt("codbanco"));
				banco.setDescripcion(rs.getString("descripcion"));
				banco.setEstado(rs.getString("estado"));
				bancos.add(banco);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return bancos;
	}

	/**
	 * Devuelve Banco por descripcion
	 * 
	 * @param conn
	 * @param descripcion
	 * @return
	 */
	public Banco getBanco(Connection conn, String descripcion) throws Exception {
		Banco ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM bancos WHERE descripcion = ?");
		ps.setString(1, descripcion);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = new Banco();
			ret.setCodbanco(rs.getInt("codbanco"));
			ret.setDescripcion(descripcion);
			ret.setEstado(rs.getString("estado"));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Devuelve Banco por codigo
	 * 
	 * @param conn
	 * @param descripcion
	 * @return
	 */
	public Banco getBanco(Connection conn, Integer codBanco) throws Exception {
		Banco ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM bancos WHERE codbanco = ?");
		ps.setInt(1, codBanco);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = new Banco();
			ret.setCodbanco(rs.getInt("codbanco"));
			ret.setDescripcion(rs.getString("descripcion"));
			ret.setEstado(rs.getString("estado"));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	private void checkValues(Banco banco) throws ValidException {
		if (banco.getDescripcion() == null) {
			throw new ValidException("Debe ingresar nombre del banco.");
		}
		if (banco.getDescripcion().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre del banco.");
		}

	}
	
	/**
	 * Verifica que se haya seleccionado el banco
	 * 
	 * @param banco
	 * @throws ValidException
	 */
	private void checkSeleccion(Banco banco) throws ValidException {
		if (banco == null) {
			throw new ValidException("Debe seleccionar un banco.");
		}
		if (banco.getCodbanco() == null) {
			throw new ValidException("Debe seleccionar un banco.");
		}
	}

	/**
	 * Verifica si existe en la base de datos un banco con el mismo
	 * nombre a la envia
	 * 
	 * @param conn
	 * @param formaPago
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Banco banco) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM bancos ";
		sSelect += "WHERE descripcion = ? ";
		if (banco.getCodbanco() != null) {
			sSelect += " AND codbanco <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, banco.getDescripcion());
		if (banco.getCodbanco() != null) {
			ps.setInt(2, banco.getCodbanco());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("El banco " + banco.getDescripcion() + " ya existe.");
		}
		return ret;
	}
}
