package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.mapping.Array;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Gasto;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PagoSalario;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Venta;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorPago extends ControladorPanambi {

//	/**
//	 * Registra un pago de cuotas.
//	 * @param conn
//	 * @param monto
//	 * @param planPago
//	 * @throws Exception
//	 */
//	public void insertarPagoCuota(Connection conn, PlanPago planPago, FormaPago formaPago, Double monto) throws Exception {
//		PreparedStatement ps = null;
//		try {
//			String sql = "INSERT INTO pagos ";
//			sql += " (codplanpago, codformapago, monto) ";
//			sql += " VALUES (?, ?, ? ) ";
//			ps = conn.prepareStatement(sql);
//			ps.setInt(1,planPago.getCodPlanPago());
//			ps.setInt(2, formaPago.getCodFormaPago());
//			ps.setDouble(3, monto);
//			ps.executeUpdate();
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			ConnectionManager.closeStatments(ps);
//		}
//	}
//	
//	public void insertarCliente(Connection conn, Cliente cliente) throws Exception {
//		
//	}
	
	/**
	 * Inserta pago de cuota con detalle inclu�do
	 * @param conn
	 * @param pago
	 * @param nroRecibo
	 * @param cuotasPagadas
	 * @throws Exception
	 */
	public void registrarPagoPlanPago(Connection conn, Pago pago, Integer nroRecibo, List<Integer> cuotasPagadas) throws Exception {
		PreparedStatement psInsCab = null;
		PreparedStatement psInsDet = null;
		PreparedStatement psInsRel = null;
		try {
			
			// ***Insertando cabecera
			conn.setAutoCommit(false);
			String sInsCab = "INSERT INTO pagos(monto, codventa, nrorecibo, codsucursal) ";
			sInsCab += "VALUES (?, ?, ?, ?)";
			psInsCab = conn.prepareStatement(sInsCab, Statement.RETURN_GENERATED_KEYS);
			psInsCab.setDouble(1, pago.getMonto());
			psInsCab.setInt(2, pago. getVenta().getCodVenta());
			psInsCab.setInt(3, nroRecibo);
			psInsCab.setInt(4, pago.getSucursal().getCodSucursal());
			
			psInsCab.executeUpdate();
			ResultSet rsKey = psInsCab.getGeneratedKeys();
			Integer codpago = null;
			if (rsKey.next()) {
				codpago = rsKey.getInt(1);
			}
			rsKey.close();

			// ***Insertando detalle
			String sInsDetalle = "INSERT INTO detallepagos( codpago, nroitem, monto, codformapago,codbanco, chequenro, bouchernro, notacredito) ";
			sInsDetalle += " VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
			psInsDet = conn.prepareStatement(sInsDetalle);
			List<DetallePago> detalle = pago.getDetallePago();
			psInsDet.setInt(1, codpago);
			
			int i = 0;
			for (DetallePago detallePago : detalle) {
				i++;
				psInsDet.setInt(2, i);
				psInsDet.setDouble(3, detallePago.getMonto());
				psInsDet.setInt(4, detallePago.getFormaPago().getCodFormaPago());
				
				if (detallePago.getBanco() != null) {
					psInsDet.setInt(5, detallePago.getBanco().getCodbanco());
				} else {
					psInsDet.setNull(5, Types.INTEGER);
				}
				
				if (detallePago.getChequenro() != null && !detallePago.getChequenro().trim().isEmpty()) {
					psInsDet.setString(6, detallePago.getChequenro());
				} else {
					psInsDet.setNull(6, Types.VARCHAR);
				}
				
				if (detallePago.getBouchernro() != null && !detallePago.getBouchernro().trim().isEmpty()) {
					psInsDet.setString(7, detallePago.getBouchernro());
				} else {
					psInsDet.setNull(7, Types.VARCHAR);
				}

				if (detallePago.getNotacredito() != null ) {
					psInsDet.setInt(8, detallePago.getNotacredito());
					new ControladorDevolucion().setNotaUsada(conn, detallePago.getNotacredito());
					
				} else {
					psInsDet.setNull(8, Types.INTEGER);
				}
				
				psInsDet.executeUpdate();

			}
			
			String sINsRelacion = "INSERT INTO pagosplanespagos (codpago, codplanpago) ";
					sINsRelacion+=  "VALUES (?, ?) ";
					
			psInsRel = conn.prepareStatement(sINsRelacion);
			psInsRel.setInt(1, codpago);
		
			for(int j = 0 ; j< cuotasPagadas.size();j++){
				psInsRel.setInt(2, cuotasPagadas.get(j));
				psInsRel.executeUpdate();
			}
			
			//new ControladorPlanPago() .setCuotaPagada(conn, pago.getPlanPago().getCodPlanPago(),nroRecibo);
			setCuotaPagada(conn, cuotasPagadas);	
			
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(psInsCab);
		}
	}
	
	/**
	 * Setea como pagada una cuota.
	 * @param conn
	 * @param cuotas
	 * @throws Exception
	 */
	private void setCuotaPagada(Connection conn, List<Integer> cuotas) throws Exception{
		PreparedStatement ps = null;
		try{
		String sql = "UPDATE planespagos ";
		sql += " SET estado = 'P' ,";
		sql += " fechapago = now() ";
		sql += " WHERE codplanpago = ? ";
		ps = conn.prepareStatement(sql);
		
		for(int i = 0; i<cuotas.size();i++){
			ps.setInt(1, cuotas.get(i));
			ps.executeUpdate();
		}
		
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Registra un pago de cuotas.
	 * 
	 * @param conn
	 * @param monto
	 * @param planPago
	 * @throws Exception
	 */
	public void registrarPago(Connection conn, Pago pago) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement psDetalle = null;
		PreparedStatement psUpdateNotaCredito = null;
		try {
			String sql = "INSERT INTO pagos ";
			sql += " (monto, ";
			if (pago.getCompra() != null) {
				sql += "codcompra";
			}else if (pago.getGasto() != null) {
				sql += "codgasto";
			}else if (pago.getVenta() != null) {
				sql += "codventa";
			}else if (pago.getPagoSalario() != null) {
				sql += "codpagoempleado";
			}else if (pago.getDeudaEmpleado() != null) {
				sql += "coddeudaempleado";
			}
			if(pago.getNroRecibo()!=null){
				sql+=" ,nrorecibo ";
			}
			sql += ", codsucursal ) VALUES (?, ? ";
			if(pago.getNroRecibo()!=null){
				sql+=" ,? ";
			}
			sql+=" ,? )";
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, pago.getMonto());
			if (pago.getCompra() != null) {
				ps.setDouble(2, pago.getCompra().getCodcompra());
			}else if (pago.getGasto() != null) {
				ps.setDouble(2, pago.getGasto().getCodGasto());
			}else if (pago.getVenta() != null) {
				ps.setDouble(2, pago.getVenta().getCodVenta());
			}else if (pago.getPagoSalario() != null) {
				ps.setDouble(2, pago.getPagoSalario().getCodpagosalario());
			}else if(pago.getDeudaEmpleado() != null){
				ps.setDouble(2, pago.getDeudaEmpleado().getCodDeudaEmpleado());
			}
			
			if(pago.getNroRecibo()!=null){
				ps.setInt(3, pago.getNroRecibo());
				ps.setInt(4, pago.getSucursal().getCodSucursal());
			}else{
				ps.setInt(3, pago.getSucursal().getCodSucursal());
			}
			
			ps.executeUpdate();
			int codigo = 0;
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				codigo = rs.getInt(1);
			}
			rs.close();
			
			String sUpdateNotaCredito = "UPDATE devoluciones SET estadonotacredito = 'U' WHERE notacredito = ? ";
			psUpdateNotaCredito = conn.prepareStatement(sUpdateNotaCredito);
			

			String sInsDetalle = "INSERT INTO detallepagos( ";
			sInsDetalle += "codpago, nroitem, monto, codbanco, codformapago, chequenro, bouchernro, notacredito) ";
			sInsDetalle += "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
			psDetalle = conn.prepareStatement(sInsDetalle);
			List<DetallePago> detallesPago = pago.getDetallePago();
			psDetalle.setInt(1, codigo);
			int i = 0;
			for (DetallePago detalle : detallesPago) {
				i++;
				psDetalle.setInt(2, i);
				psDetalle.setDouble(3, detalle.getMonto());
				if (detalle.getBanco() != null) {
					psDetalle.setInt(4, detalle.getBanco().getCodbanco());
				} else {
					psDetalle.setNull(4, Types.INTEGER);
				}
				psDetalle.setInt(5, detalle.getFormaPago().getCodFormaPago());
				if (detalle.getChequenro() != null && !detalle.getChequenro().trim().isEmpty()) {
					psDetalle.setString(6, detalle.getChequenro());
				} else {
					psDetalle.setNull(6, Types.VARCHAR);
				}
				if (detalle.getBouchernro() != null && !detalle.getBouchernro().trim().isEmpty()) {
					psDetalle.setString(7, detalle.getBouchernro());
				} else {
					psDetalle.setNull(7, Types.VARCHAR);
				}
				if (detalle.getNotacredito() != null && detalle.getNotacredito() != 0) {
					psDetalle.setInt(8, detalle.getNotacredito());
					psUpdateNotaCredito.setInt(1, detalle.getNotacredito());
					psUpdateNotaCredito.executeUpdate();
				}else{
					psDetalle.setNull(8, Types.INTEGER);
				}
				psDetalle.executeUpdate();
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Devuelve un pago con detalle inclido, a partir de un numero de recibo.
	 * @param conn
	 * @param nrorecibo
	 * @return
	 * @throws Exception
	 */
	public Pago getPagoPorRecibo(Connection conn, Integer nrorecibo) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE nrorecibo = ? ");		
		PreparedStatement psDetalles = null;
		String sqlDetalle = "SELECT * FROM detallepagos WHERE codpago = ";
		
		ps.setInt(1, nrorecibo);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago .setCodPago(rs.getInt("codpago"));
			pago .setMonto(rs.getDouble("monto"));
			pago .setFecha(rs.getTimestamp("fecha"));
			pago .setNroRecibo(rs.getInt("nrorecibo"));
			pago .setObservaciones(rs.getString("observaciones"));
			pago .setEstado(rs.getString("estado"));
			pago .setSucursal(new ControladorSucursal().getSucursal(conn, rs.getInt("codsucursal")));
			
			if(rs.getObject("codcompra")!=null){
				pago .setCompra(new ControladorCompra().getCompra(conn, rs.getInt("codcompra")));
			}else pago .setVenta(null);
			
			if(rs.getObject("codventa")!=null){
				pago .setVenta(new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa")));
			} else pago .setVenta(null);
			
			if(rs.getObject("codgasto")!=null){
				pago .setGasto(new ControladorGasto().getGasto(conn, rs.getInt("codgasto")));
			}else pago .setGasto(null);
			
			if(rs.getObject("codpagoempleado")!=null){
				pago .setPagoSalario(new ControladorPagoSalario().getPagoAguinaldo(conn, rs.getInt("codpagosalario")));
			}else pago .setPagoSalario(null);
			
			if(rs.getObject("coddeudaempleado")!=null){
				pago .setDeudaEmpleado(new ControladorDeudaEmpleado().getDeudaEmpleado(conn, rs.getInt("coddeudaempleado")));
			}else pago .setPagoSalario(null);
			
			//pago .setCompra(new ControladorCompra().getCompra(conn, rs.getInt("codcompra")));
			
			//*** Cargando detalles
			sqlDetalle+=" "+pago.getCodPago();
			psDetalles = conn.prepareStatement(sqlDetalle);
			List<DetallePago> detalles = new ArrayList<DetallePago>();
			ResultSet rsDetalles = psDetalles.executeQuery();
			while (rsDetalles.next()) {
				DetallePago detalle = new DetallePago();
				detalle.setNroitem(rsDetalles.getInt("nroitem"));
				detalle.setMonto(rsDetalles.getDouble("monto"));
				detalle.setBanco(new ControladorBanco().getBanco(conn, rsDetalles.getInt("codbanco")));
				detalle.setFormaPago(new ControladorFormaPago().getFormaPago(conn,rsDetalles.getInt("codformapago")));
				detalle.setCodpago(rsDetalles.getInt("codpago"));
				detalle.setChequenro(rsDetalles.getString("chequenro"));
				detalle.setBouchernro(rsDetalles.getString("bouchernro"));
				detalle.setNotacredito(rsDetalles.getInt("notacredito"));
				detalles.add(detalle);
			}
			
			pago. setDetallePago(detalles);
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * Obtiene el ultimo pago de cuota para un numero de recibo.
	 * @param conn
	 * @param nrorecibo
	 * @return
	 * @throws Exception
	 */
	public Pago getUltimoPagoCuota(Connection conn, Integer codplanpago) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT codpago FROM pagosplanespagos WHERE codplanpago = ? ");		
		
		ps.setInt(1, codplanpago);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
			
		}
		rs.close();
		ps.close();
		return pago;
	}

	/**
	 * Obtiene un pago con detalle incluido a partir del codigo de pago.
	 * @param conn
	 * @param codpago
	 * @return
	 * @throws Exception
	 */
	public Pago getPago(Connection conn, Integer codpago) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE codpago = ? ");	
		PreparedStatement psDetalles = null;
		String sqlDetalle = "SELECT * FROM detallepagos WHERE codpago = ";
		
		ps.setInt(1, codpago);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago .setCodPago(rs.getInt("codpago"));
			pago .setMonto(rs.getDouble("monto"));
			pago .setFecha(rs.getTimestamp("fecha"));
			pago .setNroRecibo(rs.getInt("nrorecibo"));
			pago .setObservaciones(rs.getString("observaciones"));
			pago .setEstado(rs.getString("estado"));
			pago .setSucursal(new ControladorSucursal().getSucursal(conn, rs.getInt("codsucursal")));
			
			if(rs.getObject("codcompra")!=null){
				pago .setCompra(new ControladorCompra().getCompra(conn, rs.getInt("codcompra")));
			}else pago .setCompra(null);
			
			if(rs.getObject("codventa")!=null){
				pago .setVenta(new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa")));
			} else pago .setVenta(null);
			
			if(rs.getObject("codgasto")!=null){
				pago .setGasto(new ControladorGasto().getGasto(conn, rs.getInt("codgasto")));
			}else pago .setGasto(null);
			
			if(rs.getObject("codpagoempleado")!=null){
				pago .setPagoSalario(new ControladorPagoSalario().getPagoSalario(conn, rs.getInt("codpagoempleado")));
			}else pago .setPagoSalario(null);
			
			if(rs.getObject("coddeudaempleado")!=null){
				pago .setDeudaEmpleado(new ControladorDeudaEmpleado().getDeudaEmpleado(conn, rs.getInt("coddeudaempleado")));
			}else pago .setDeudaEmpleado(null);
			
			//*** Cargando detalles
			sqlDetalle+=" "+pago.getCodPago();
			psDetalles = conn.prepareStatement(sqlDetalle);
			List<DetallePago> detalles = new ArrayList<DetallePago>();
			ResultSet rsDetalles = psDetalles.executeQuery();
			while (rsDetalles.next()) {
				DetallePago detalle = new DetallePago();
				detalle.setNroitem(rsDetalles.getInt("nroitem"));
				detalle.setMonto(rsDetalles.getDouble("monto"));
				detalle.setBanco(new ControladorBanco().getBanco(conn, rsDetalles.getInt("codbanco")));
				detalle.setFormaPago(new ControladorFormaPago().getFormaPago(conn,rsDetalles.getInt("codformapago")));
				detalle.setCodpago(rsDetalles.getInt("codpago"));
				detalle.setChequenro(rsDetalles.getString("chequenro"));
				detalle.setBouchernro(rsDetalles.getString("bouchernro"));
				detalle.setNotacredito(rsDetalles.getInt("notacredito"));
				detalles.add(detalle);
			}
			
			pago. setDetallePago(detalles);
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * Devuelve una lista con todos los pagos activos efectuados.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Pago> getPagosActivos(Connection conn, Date fechaDesde, Date fechaHasta, Sucursal sucursal) throws Exception {
		List<Pago> pagosActivos = new ArrayList<Pago>();
		String sql = "SELECT * FROM pagos WHERE estado = 'A' ";
		
		if(sucursal!=null){
			sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
		}
		
		if(fechaDesde!=null && fechaHasta!=null){
			sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
		}else if (fechaDesde ==null && fechaHasta !=null){
			sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
		}else if (fechaDesde !=null && fechaHasta ==null){
			sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
		}
		
		
		
		PreparedStatement ps = conn.prepareStatement(sql);	
		
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			Pago pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
			pagosActivos.add(pago);
		}
			
		rs.close();
		ps.close();
		
		return pagosActivos;
	}

	
	/**
	 * Retorna la cantidad de pagos que se realizo para un numero de factura dado.
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public Integer getCantidadPagosPorFactura(Connection conn, Venta venta) throws Exception{
		
		Integer ret = 0;
		
		PreparedStatement ps = conn.prepareStatement("SELECT count(*) AS cantidad FROM pagos WHERE codventa = ? ");
		ps.setInt(1, venta.getCodVenta());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			ret = rs.getInt("cantidad");
		}
		
		return ret;
	}

	public void anularPagoCuota(Connection conn, Pago pago) throws Exception{
		try{
			ControladorPlanPago controladorPlanPago = new ControladorPlanPago();
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I', ";
			sql += " fechaanulacion = current_timestamp , ";
			sql += " observaciones = ? ";
			sql += " WHERE codpago = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pago.getObservaciones());
			ps.setInt(2, pago.getCodPago());
			ps.executeUpdate();
			ps.close();
			
			List<PlanPago> cuotasAnuladas = new ArrayList<PlanPago>();
			cuotasAnuladas = controladorPlanPago.getCuotas(conn, pago);
			
			//**devuelta el estado de la cuota a "DEBE"
			for(int i=0;i<cuotasAnuladas.size();i++){
				PlanPago cuota = new PlanPago();
				cuota = cuotasAnuladas.get(i);
				controladorPlanPago.setCuotaAnulada(conn, cuota.getCodPlanPago());
			}
			
			//***Devuelta activa a la nota de credito utilizada.
			for (DetallePago detallepago : pago.getDetallePago()) {
				if(detallepago.getFormaPago().getNombre().equals("NOTA DE CREDITO")){
					new ControladorDevolucion().setNotaActiva(conn, detallepago.getNotacredito());
				}
			}
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	/**
	 * Anula un pago relacionado a un gasto.
	 * @param conn
	 * @param gasto
	 * @throws Exception
	 */
	public void anularPago(Connection conn, Gasto gasto) throws Exception{
		try{
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I', ";
			sql += " fechaanulacion = current_timestamp , ";
			sql += " observaciones = ? ";
			sql += " WHERE codgasto = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, gasto.getComentarios());
			ps.setInt(2, gasto.getCodGasto());
			ps.executeUpdate();
			ps.close();
			
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	public void anularPago(Connection conn, PagoSalario pagoEmpleado, String comentarios) throws Exception{
		try{
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I', ";
			sql += " fechaanulacion = current_timestamp , ";
			sql += " observaciones = ? ";
			sql += " WHERE codpagoempleado = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, comentarios);
			ps.setInt(2, pagoEmpleado.getCodpagosalario());
			ps.executeUpdate();
			ps.close();
			
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	/**
	 * Anula un pago de una deuda de empleado.
	 * @param conn
	 * @param deudaEmpleado
	 * @throws Exception
	 */
	public void anularPago(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception{
		try{
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I', ";
			sql += "  observaciones = ? , ";
			sql += " fechaanulacion = current_timestamp  ";
			sql += " WHERE coddeudaempleado = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, "ANULADO POR "+JFramePanambiMain.session.getUsuario().getUsuario()+".");
			ps.setInt(2, deudaEmpleado.getCodDeudaEmpleado());
			ps.executeUpdate();
			ps.close();
			
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	public void eliminarPago(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception{
		try{
			List <Pago> pa = getPago(conn, deudaEmpleado);
			
			if(pa.size()!=0){
				for(int i = 0;i<pa.size();i++){
					String sqlDetalle = "UPDATE pagos SET estado = 'I' WHERE codpago  = ? ";
					PreparedStatement psDetalle = conn.prepareStatement(sqlDetalle);
					psDetalle.setInt(1, pa.get(i).getCodPago());
					psDetalle.executeUpdate();
					
				}
//				String sql = "DELETE FROM pagos WHERE coddeudaempleado = ? ";
//				PreparedStatement ps = conn.prepareStatement(sql);
//				ps.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
//				ps.executeUpdate();
//				ps.close();
				
				
			}
			
			String sqlDeuda = "UPDATE deudasempleado SET estado = 'I' WHERE coddeudaempleado = ? ";
			PreparedStatement psDeuda = conn.prepareStatement(sqlDeuda);
			psDeuda.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
			psDeuda.executeUpdate();
			psDeuda.close();
			
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	
	/**
	 * Anula un pago relacionado a una compra.
	 * @param conn
	 * @param compra
	 * @throws Exception
	 */
	public void anularPago(Connection conn, Compra compra) throws Exception{
		try{
			String sql = "UPDATE pagos ";
			sql += " SET estado = 'I', ";
			sql += " fechaanulacion = current_timestamp , ";
			sql += " observaciones = ? ";
			sql += " WHERE codcompra = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, compra.getComentarios());
			ps.setInt(2, compra.getCodcompra());
			ps.executeUpdate();
			ps.close();
			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	
	/**
	 * Obtiene un pago a partir de una venta dada.
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public Pago getPagoPorVenta(Connection conn, Venta venta) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE codventa = ? ");		

		ps.setInt(1, venta.getCodVenta());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * Devuelve el pago relacionado al pago al empleado.
	 * @param conn
	 * @param pagoEmpleado
	 * @return
	 * @throws Exception
	 */
	public Pago getPago(Connection conn, PagoSalario pagoEmpleado) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE codpagoempleado = ? ");		

		ps.setInt(1, pagoEmpleado.getCodpagosalario());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	public Pago getPagoDeuda(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE coddeudaempleado = ? AND estado = 'A' ");		

		ps.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * 
	 * @param conn
	 * @param pagoEmpleado
	 * @return
	 * @throws Exception
	 */
	public Pago getPago(Connection conn, PlanPago cuota) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagosplanespagos WHERE codplanpago = ? ");		

		ps.setInt(1, cuota.getCodPlanPago());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * Obtiene el pago relacionado a la deuda del empleado.
	 * @param conn
	 * @param deudaEmpleado
	 * @return
	 * @throws Exception
	 */
	public List<Pago> getPago(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception {
		List <Pago> pagos= new ArrayList<Pago>();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM pagos WHERE coddeudaempleado = ? ");		

		ps.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			Pago pago = new Pago();
			pago = getPago(conn, rs.getInt("codpago"));
			pagos.add(pago);		
			
		}
		rs.close();
		ps.close();
		return pagos;
	}
}
