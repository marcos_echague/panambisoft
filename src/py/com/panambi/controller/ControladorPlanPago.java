package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.PlanPago;
import py.com.panambi.bean.Venta;
import py.com.panambi.connect.ConnectionManager;

public class ControladorPlanPago extends ControladorPanambi {
	/**
	 * Retorna una cuota de un plan de pagos.
	 * @param conn
	 * @param codPlanPago
	 * @return
	 * @throws Exception
	 */
	public PlanPago getCuota(Connection conn, Integer codPlanPago) throws Exception {
		PlanPago ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM planespagos WHERE codplanpago = ? ");
			ps.setInt(1, codPlanPago);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new PlanPago();
				ret.setCodPlanPago(rs.getInt("codplanpago"));
				ret.setCodVenta(rs.getInt("codventa"));
				ret.setNroCuota(rs.getInt("nrocuota"));
				ret.setMonto(rs.getDouble("monto"));
				ret.setVencimiento(rs.getDate("vencimiento"));
				ret.setFechaPago(rs.getDate("fechapago"));
				ret.setEstado(rs.getString("estado"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Obtiene la primera cuota a pagar de una venta
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public PlanPago getPrimeraCuota(Connection conn, Venta venta) throws Exception {
		PlanPago ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM planespagos WHERE codventa = ? AND nrocuota = ?");
			ps.setInt(1, venta.getCodVenta());
			ps.setInt(2, 1);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new PlanPago();
				ret.setCodPlanPago(rs.getInt("codplanpago"));
				ret.setCodVenta(rs.getInt("codventa"));
				ret.setNroCuota(rs.getInt("nrocuota"));
				ret.setMonto(rs.getDouble("monto"));
				ret.setVencimiento(rs.getDate("vencimiento"));
				ret.setFechaPago(rs.getDate("fechapago"));
				ret.setEstado(rs.getString("estado"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Devuelve la cuota inmediata a pagar.
	 * @param conn
	 * @param planPago
	 * @return
	 * @throws Exception
	 */
	public Integer ultimoPagado(Connection conn, PlanPago planPago) throws Exception {
		Integer codCuotaAPagar = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT MIN(codplanpago) FROM planespagos WHERE codventa = ? AND estado = 'D'");
			ps.setInt(1, planPago.getCodVenta());
			rs = ps.executeQuery();
			if (rs.next()) {
				codCuotaAPagar = rs.getInt(1);
			}

			return codCuotaAPagar;

		} catch (Exception e) {
			throw e;

		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

	}

	/**
	 * Devuelve vardadero o falso para indicar si una cuota ya se encuetra pagada a partir del codigo de la cuota
	 * @param conn
	 * @param codCuota
	 * @return
	 * @throws Exception
	 */
	public boolean yaPagado(Connection conn, Integer codCuota) throws Exception {
		String estado = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT estado FROM planespagos WHERE codplanpago = ? ");
			ps.setInt(1, codCuota);
			rs = ps.executeQuery();
			if (rs.next()) {
				estado = rs.getString(1);
			}

			if (estado.equals("P")) {
				return true;
			} else
				return false;

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Retorna todos los clientes que hicieron ventas a credito.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Cliente> getClientes(Connection conn) throws Exception {
		List<Cliente> clientes = new ArrayList<Cliente>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT DISTINCT c.codcliente, c.nrodocumento, c.nombres , c.apellidos, c.telefono, c.email, c.direccion, c.fechaingreso, c.estado " + "FROM CLIENTES c JOIN VENTAS v " + "ON (c.codcliente = v.codcliente) " + "JOIN PLANESPAGOS pp "
					+ "ON (v.codventa = pp.codventa) " + "ORDER BY c.apellidos, c.nombres ");
			rs = ps.executeQuery();
			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setCodCliente(rs.getInt("codcliente"));
				cliente.setNroDocumento(rs.getString("nrodocumento"));
				cliente.setNombres(rs.getString("nombres"));
				cliente.setApellidos(rs.getString("apellidos"));
				cliente.setTelefono(rs.getString("telefono"));
				cliente.setEmail(rs.getString("email"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setFechaIngreso(rs.getDate("fechaingreso"));
				cliente.setEstado(rs.getString("estado"));
				clientes.add(cliente);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return clientes;
	}

	/**
	 * Retorna todas las ventas a credito.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Venta> getVentas(Connection conn) throws Exception {
		List<Venta> ventas = new ArrayList<Venta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT DISTINCT v.codventa, v.nrocomprobante, v.fecha, v.total, v.iva, v.tipoventa, v.totalletras, v.codcliente, v.codusuario, v.codsucursal " + "FROM clientes c JOIN ventas v " + "ON (c.codcliente = v.codcliente) " + "JOIN planespagos pp "
					+ "ON (v.codventa = pp.codventa) " + "ORDER BY v.codventa ");
			rs = ps.executeQuery();
			while (rs.next()) {
				Venta venta = new Venta();
				venta = new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa"));
				ventas.add(venta);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ventas;
	}

	/**
	 * Retorna todas las ventas a credito realizo un cliente espeficico.
	 * @param conn
	 * @param cliente
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Venta> getVentas(Connection conn, Cliente cliente) throws Exception {
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas " + "WHERE codcliente = ? " + "AND tipoventa = 'C'");
			ps.setInt(1, cliente.getCodCliente());
			rs = ps.executeQuery();
			while (rs.next()) {
				Venta venta = new Venta();
				venta = new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa"));
				ventas.add(venta);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ventas;

	}

	/**
	 * Obtiene todas las ventas activas a cargo de un cliente.
	 * @param conn
	 * @param cliente
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Venta> getVentasActivas(Connection conn, Cliente cliente) throws Exception {
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM ventas " + "WHERE codcliente = ? " + "AND tipoventa = 'C' " + "AND estado = 'A' ");
			ps.setInt(1, cliente.getCodCliente());
			rs = ps.executeQuery();
			while (rs.next()) {
				Venta venta = new Venta();
				venta = new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa"));
				ventas.add(venta);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ventas;

	}

	/**
	 * Devuelve cantidad de cuotas de un plan de pagos.
	 * @param conn
	 * @param codPlanPago
	 * @return
	 * @throws Exception
	 */
	public Integer getCantidadCuotas(Connection conn, Integer codVenta) throws Exception {
		Integer cantCuotas = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT MAX(nrocuota) FROM planespagos " + "WHERE codventa = ? ");
			ps.setInt(1, codVenta);
			rs = ps.executeQuery();
			if (rs.next()) {
				cantCuotas = rs.getInt(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return cantCuotas;

	}

	/**
	 * Retorna todos los datos de las cuotas de una venta a credito.
	 * @param conn
	 * @param codVenta
	 * @return
	 * @throws Exception
	 */

	public List<PlanPago> getPlanesPagos(Connection conn, Integer codVenta) throws Exception {
		List<PlanPago> planesPagos = new ArrayList<PlanPago>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM planespagos " + "WHERE codventa = ? " + "ORDER BY nrocuota ");
			ps.setInt(1, codVenta);
			rs = ps.executeQuery();

			while (rs.next()) {

				PlanPago planPago = new PlanPago();
				planPago.setCodPlanPago(rs.getInt("codplanpago"));
				planPago.setNroCuota(rs.getInt("nrocuota"));
				planPago.setMonto(rs.getDouble("monto"));
				planPago.setVencimiento(rs.getDate("vencimiento"));
				planPago.setFechaPago(rs.getDate("fechapago"));
				planPago.setEstado(rs.getString("estado"));
				planPago.setCodVenta(rs.getInt("codventa")/*new ControladorVenta().getVe(JFramePanambiMain.session.getConn(), rs.getInt("codcliente"))*/);
				planesPagos.add(planPago);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return planesPagos;

	}

	/**
	 * Obtiene la mayor cuota pagada de un plan de pagos.
	 * @param conn
	 * @param codVenta
	 * @return
	 * @throws Exception
	 */
	public Integer getMayorCuotaPagada(Connection conn, Integer codVenta) throws Exception {
		Integer maximo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT max(nrocuota) AS maximo FROM planespagos " + "WHERE codventa = ? " + "AND estado = 'P' ");
			ps.setInt(1, codVenta);
			rs = ps.executeQuery();

			if (rs.next()) {
				maximo = rs.getInt("maximo");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return maximo;

	}

	/**
	 * Obtiene las cuotas relacionadas a un pago realizado.
	 * @param conn
	 * @param pago
	 * @return
	 * @throws Exception
	 */
	public List<PlanPago> getCuotas(Connection conn, Pago pago) throws Exception {
		List<PlanPago> planesPagos = new ArrayList<PlanPago>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codplanpago FROM pagosplanespagos " + "WHERE codpago = ? ");
			ps.setInt(1, pago.getCodPago());
			rs = ps.executeQuery();

			while (rs.next()) {
				PlanPago planPago = new PlanPago();
				planPago = getCuota(conn, rs.getInt("codplanpago"));
				planesPagos.add(planPago);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return planesPagos;

	}

	/**
	 * Obtiene todas las cuotas pagadas de un plan de pagos.
	 * @param conn
	 * @param pago
	 * @return
	 * @throws Exception
	 */
	public List<PlanPago> getCuotasPagadas(Connection conn, Pago pago) throws Exception {
		List<PlanPago> planesPagos = new ArrayList<PlanPago>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT codplanpago FROM pagosplanespagos " + "WHERE codpago = ? ");
			ps.setInt(1, pago.getCodPago());
			rs = ps.executeQuery();

			while (rs.next()) {
				PlanPago planPago = new PlanPago();
				planPago = getCuota(conn, rs.getInt("codplanpago"));
				planesPagos.add(planPago);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return planesPagos;

	}

	/**
	 * Setea el estado de una cuota a pagada y asigna un numero de recibo a la cuota.
	 * @param conn
	 * @param codPlanPago
	 * @throws Exception
	 */
	public void setCuotaPagada(Connection conn, Integer codPlanPago) throws Exception {
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE planespagos ";
			sql += " SET estado = 'P', ";
			sql += " fechapago = ?,";
			sql += " nrorecibo = ? ";
			sql += " WHERE codplanpago = ? ";

			ps = conn.prepareStatement(sql);
			ps.setTimestamp(1, new Timestamp(new Date().getTime()));

			ps.setInt(2, getNroRecibo(conn));
			ps.setInt(3, codPlanPago);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}

	}

	/**
	 * Anula una cuota pagada.
	 * Cambia el estado a 'Debe' y borra la fecha de la cuota.
	 * @param conn
	 * @param codPlanPago
	 * @throws Exception
	 */
	public void setCuotaAnulada(Connection conn, Integer codPlanPago) throws Exception {
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE planespagos ";
			sql += " SET estado = 'D', ";
			sql += " fechapago = null ";
			sql += " WHERE codplanpago = ? ";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, codPlanPago);

			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}

	}

	/**
	 * Setea una cuota como pagada y asigna un n�mero de recibo a la cuota pagada.
	 * @param conn
	 * @param codPlanPago
	 * @param nroRecibo
	 * @throws Exception
	 */
	public void setCuotaPagada(Connection conn, Integer codPlanPago, Integer nroRecibo) throws Exception {
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE planespagos ";
			sql += " SET estado = 'P', ";
			sql += " fechapago = ?,";
			sql += " nrorecibo = ? ";
			sql += " WHERE codplanpago = ? ";

			ps = conn.prepareStatement(sql);
			ps.setTimestamp(1, new Timestamp(new Date().getTime()));

			ps.setInt(2, nroRecibo);
			ps.setInt(3, codPlanPago);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}

	}

	/**
	 * Obtiene el n�mero de recibo a utilizar para el pago de la cuota.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Integer getNroRecibo(Connection conn) throws Exception {

		Integer ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT MAX(nrorecibo) AS maximo FROM pagos");
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = rs.getInt("maximo");
			}
			if (ret == null) {
				ret = 0;
			}
			ret = ret + 1;

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
		
		

		//		ControladorSecuencias controladorSecuencias = new ControladorSecuencias();
		//		Integer ret = null;
		//		try{
		//			ret = controladorSecuencias.getNroRecibo(conn);
		//			controladorSecuencias.aumentarNroRecibo(conn);
		//		}catch (Exception e ){
		//			throw e;
		//		}

	}
	
	public Integer getNroPagare(Connection conn) throws Exception {

		Integer ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT MAX(nropagare) AS maximo FROM ventas");
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = rs.getInt("maximo");
			}
			if (ret == null) {
				ret = 0;
			}
			ret = ret + 1;

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Obtiene la cantidad de cuotas pendientes de una venta a credito.
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public Integer getCantidadCuotasPendientes(Connection conn, Venta venta) throws Exception {

		Integer ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT COUNT(*) AS cantidad FROM planespagos " + "WHERE estado = 'D' " + " AND codventa = ? ");
			ps.setInt(1, venta.getCodVenta());
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = rs.getInt("cantidad");
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;

		//		ControladorSecuencias controladorSecuencias = new ControladorSecuencias();
		//		Integer ret = null;
		//		try{
		//			ret = controladorSecuencias.getNroRecibo(conn);
		//			controladorSecuencias.aumentarNroRecibo(conn);
		//		}catch (Exception e ){
		//			throw e;
		//		}

	}

	/**
	 * Obtiene todos los clientes que posean ventas a credito activas.
	 * @param conn
	 * @param nrodocumento
	 * @return
	 * @throws Exception
	 */
	public Cliente getClienteConCuotasActivas(Connection conn, String nrodocumento) throws Exception {
		Cliente ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT c.codcliente AS codigoCliente " + " FROM clientes c , ventas v " + "WHERE c.codcliente = v.codcliente AND v.tipoventa = 'C' AND v.estado = 'A' " + "AND c.estado = 'A' AND nrodocumento = ? ");
			ps.setString(1, nrodocumento);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Cliente();
				ret = new ControladorCliente().getCliente(conn, rs.getInt("codigoCliente"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Obtiene un cliente que posee cuotas activas pendientes por pagar.
	 * @param conn
	 * @param nrodocumento
	 * @return
	 * @throws Exception
	 */
	public Cliente getClienteConCuotasActivasPendientes(Connection conn, String nrodocumento) throws Exception {
		Cliente ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT DISTINCT * FROM clientes c , ventas v" + "WHERE c.codcliente = v.codcliente " + " AND v.tipoventa = 'C' " + " AND v.estado = 'A'" + " AND 0 NOT IN (SELECT count(*) FROM planespagos pp " + " WHERE pp.estado = 'D'" + " AND pp.codventa = v.codventa)"
					+ " AND nrodocumento = ? ");
			ps.setString(1, nrodocumento.trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Cliente();
				ret.setCodCliente(rs.getInt("c.codcliente"));
				ret.setNroDocumento(rs.getString("c.nrodocumento"));
				ret.setNombres(rs.getString("c.nombres"));
				ret.setApellidos(rs.getString("c.apellidos"));
				ret.setTelefono(rs.getString("c.telefono"));
				ret.setEmail(rs.getString("c.email"));
				ret.setDireccion(rs.getString("c.direccion"));
				ret.setFechaIngreso(rs.getDate("c.fechaingreso"));
				ret.setEstado(rs.getString("c.estado"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	public void insertarPlanesPago(Connection conn, Venta venta, List<PlanPago> planespago) throws Exception {
		PreparedStatement psInsert = null;
		try {
			String sInsert = "INSERT INTO planespagos( ";
			sInsert += "nrocuota, monto, vencimiento, estado, ";
			sInsert += "codventa) ";
			sInsert += "VALUES (?, ?, ?, ?, ?) ";
			psInsert = conn.prepareStatement(sInsert);
			psInsert.setString(4, "D");//Se registra como pendiente de pago
			psInsert.setInt(5, venta.getCodVenta());
			for (PlanPago planPago : planespago) {
				psInsert.setInt(1, planPago.getNroCuota());
				psInsert.setDouble(2, planPago.getMonto());
				psInsert.setDate(3, (java.sql.Date)planPago.getVencimiento());
				psInsert.executeUpdate();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psInsert);
		}
	}
}
