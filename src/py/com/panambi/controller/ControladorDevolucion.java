package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Cliente;
import py.com.panambi.bean.DetalleDevolucion;
import py.com.panambi.bean.Devolucion;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Venta;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorDevolucion extends ControladorPanambi {
	
	/**
	 * Retorna toda la informacion de la devolcion incluido el detalle con el codigo de la devolucion.
	 * @param conn
	 * @param coddevolucion
	 * @return
	 * @throws Exception
	 */
	public Devolucion getDevolucion(Connection conn, Integer coddevolucion) throws Exception {
		Devolucion devolucion= null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM devoluciones WHERE coddevolucion = ? ");		
		PreparedStatement psDetalles = conn.prepareStatement("SELECT * FROM detalledevoluciones WHERE coddevolucion = "+coddevolucion+"");
		
		ps.setInt(1, coddevolucion);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			devolucion = new Devolucion();
			devolucion .setCodDevolucion(coddevolucion);
			devolucion .setFecha(rs.getDate("fecha"));
			devolucion .setTotal(rs.getDouble("total"));
			devolucion .setNotaCredito(rs.getInt("notacredito"));
			devolucion .setEstadoDevolucion(rs.getString("estadodevolucion"));
			devolucion .setEstadoNotaCredito(rs.getString("estadonotacredito"));
			devolucion .setObservaciones(rs.getString("observaciones"));
			devolucion .setFechaAnulacion(rs.getDate("fechaanulacion"));
			devolucion .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
			devolucion .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			devolucion .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			devolucion .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
			
			// Cargando detalles
			List<DetalleDevolucion> detalles = new ArrayList<DetalleDevolucion>();
			ResultSet rsDetalles = psDetalles.executeQuery();
			while (rsDetalles.next()) {
				detalles.add(getDetalleDevolucion(conn, rsDetalles.getInt("coddevolucion"), rsDetalles.getInt("nroitem")));
			}
			devolucion. setDetalleDevolucion(detalles);
		}
		rs.close();
		ps.close();
		return devolucion;
	}
	
	/**
	 * Retorna la informacion de la nota de credito.
	 * @param conn
	 * @param coddevolucion
	 * @return
	 * @throws Exception
	 */
	public Devolucion getNotaCredito(Connection conn, Integer codnotacredito) throws Exception {
		Devolucion devolucion= null;
		PreparedStatement ps = conn.prepareStatement("SELECT coddevolucion FROM devoluciones WHERE notacredito = ? ");	
		Integer codDevolucion = null;
		ps.setInt(1, codnotacredito);
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			devolucion = new Devolucion();
			codDevolucion = rs.getInt("coddevolucion");
			devolucion = getDevolucion(conn, codDevolucion);
//			devolucion .setCodDevolucion(rs.getInt("coddevolucion"));
//			devolucion .setFecha(rs.getDate("fecha"));
//			devolucion .setTotal(rs.getDouble("total"));
//			devolucion .setNotaCredito(rs.getInt("notacredito"));
//			devolucion .setEstadoDevolucion(rs.getString("estadodevolucion"));
//			devolucion .setEstadoNotaCredito(rs.getString("estadonotacredito"));
//			devolucion .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
//			devolucion .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
//			devolucion .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//			devolucion .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
		}
		rs.close();
		ps.close();
		return devolucion;
	}
	
	/**
	 * Devuelve una nota de credito activa.
	 * @param conn
	 * @param codnotacredito
	 * @return
	 * @throws Exception
	 */
	public Devolucion getNotaCreditoActiva(Connection conn, Integer codnotacredito) throws Exception {
		Devolucion devolucion= null;
		Integer codDevolucion = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM devoluciones WHERE notacredito = ? AND estadonotacredito = 'A'");				
		ps.setInt(1, codnotacredito);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			devolucion = new Devolucion();
			codDevolucion = rs.getInt("coddevolucion");
			devolucion = getDevolucion(conn, codDevolucion);
//			
//			devolucion .setCodDevolucion(rs.getInt("coddevolucion"));
//			devolucion .setFecha(rs.getDate("fecha"));
//			devolucion .setTotal(rs.getDouble("total"));
//			devolucion .setNotaCredito(rs.getInt("notacredito"));
//			devolucion .setEstadoDevolucion(rs.getString("estadodevolucion"));
//			devolucion .setEstadoNotaCredito(rs.getString("estadonotacredito"));
//			devolucion .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
//			devolucion .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
//			devolucion .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//			devolucion .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
		}
		rs.close();
		ps.close();
		return devolucion;
	}
	
	/**
	 * Retorna notas de credito segun los parametros pasados.
	 * @param conn
	 * @param cliente
	 * @param sucursal
	 * @param estado
	 * @return
	 * @throws Exception
	 */
	public List<Devolucion> getNotasCreditoForParameter(Connection conn, Cliente cliente, Sucursal sucursal, String estado) throws Exception {
		
		List<Devolucion> ret = new ArrayList<Devolucion>();
		String sql = "";
		PreparedStatement ps = null;//conn.prepareStatement(
		
		sql += " SELECT * FROM devoluciones WHERE 1 = 1 ";
				
		if(cliente !=null){
		sql+= " AND codcliente = "+cliente.getCodCliente()+" ";
		}
		
		if(sucursal!=null){
			sql+= " AND codsucursal = "+sucursal.getCodSucursal()+" ";
		}
		
		if(estado!=null){
			sql+= " AND estadonotacredito = "+estado+" ";
		}
		
		sql+= " ORDER BY notacredito DESC";
		
		ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			Devolucion dev= new Devolucion();
			dev = getDevolucion(conn, rs.getInt("coddevolucion"));
			ret.add(dev);
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Obtiene todas las devoluciones segun los parametros introducidos.
	 * @param conn
	 * @param cliente
	 * @param sucursal
	 * @param estado
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<Devolucion> getDevoluciones(Connection conn, Cliente cliente, Sucursal sucursal, String estado, Date fechaDesde, Date fechaHasta) throws Exception {
		List<Devolucion> devoluciones= new ArrayList<Devolucion>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT * FROM devoluciones ";
			sql += " WHERE 1 = 1 ";
			if (cliente != null) {
				sql += " AND codcliente =  " + cliente.getCodCliente() + " ";
			}

			if (sucursal != null) {
				sql += " AND codsucursal =  " + sucursal.getCodSucursal() + " ";
			}

			if (estado != null) {
				if (estado.equals("A")) {
					sql += " AND estadodevolucion = 'A' ";
				} else
					sql += " AND estadodevolucion = 'I' ";
			}

			if (fechaDesde != null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' ";
			} else if (fechaDesde == null && fechaHasta != null) {
				sql += " AND fecha BETWEEN '2000-01-01' AND '" + fechaHasta + "' ";
			} else if (fechaDesde != null && fechaHasta == null) {
				sql += " AND fecha BETWEEN " + fechaDesde + "AND '2999-01-01' ";
			}

			sql += " ORDER BY coddevolucion DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Devolucion devolucion= new Devolucion();
				devolucion = getDevolucion(conn, rs.getInt("coddevolucion"));
				devoluciones.add(devolucion);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return devoluciones;
	}
	
	/**
	 * Devuelve la venta en la que fue utilizada como medio de pago la nota de cr�dito.
	 * @param conn
	 * @param devolucion
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaUsoNota(Connection conn, Devolucion devolucion) throws Exception {
		Venta venta = new Venta();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT p.codventa AS codventa FROM pagos p , detallepagos dp ";
			sql += " WHERE p.codpago = dp. codpago ";
			sql += "AND dp.notacredito =  ? ";
			sql += "AND p.estado = 'A' ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1,devolucion.getNotaCredito());
			rs = ps.executeQuery();
			if (rs.next()) {
				venta = new ControladorVenta().getVenta(conn, rs.getInt("codventa"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return venta;
	}
	
	public boolean notaCreditoUsada(Connection conn, Integer nronotacredito) throws Exception{
		Integer conteo = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT count(*) AS cantidad FROM detallepagos WHERE notacredito = ? ";
		try{
			ps = conn.prepareStatement(sql);
			ps.setInt(1, nronotacredito);
			rs = ps.executeQuery();
			
			if(rs.next()){
				conteo = rs.getInt("cantidad");
			}
			
			if(conteo == 0){
				return false;
			}else{
				return true;
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Anula una nota de credito.
	 * @param conn
	 * @param nronotacredito
	 * @throws Exception
	 */
//	public void anularNotaCredito(Connection conn, Integer nronotacredito) throws Exception{
//		PreparedStatement ps =null;
//		try{
//			ps = conn.prepareStatement("UPDATE devoluciones SET estadonotacredito = 'I' WHERE notacredito = ? ");
//			ps.setInt(1, nronotacredito);
//			ps.executeUpdate();
//		}catch(Exception e ){
//			throw e;
//		}finally{
//			ConnectionManager.closeStatments(ps);
//		}
//	}
	
	/**
	 * Anula una devolucion registrada, cambia el estado de activo a anulado.
	 * @param conn
	 * @param devolucion
	 * @throws Exception
	 */
	public void anularDevolucion(Connection conn, Devolucion devolucion) throws Exception{
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement("UPDATE devoluciones SET fechaanulacion = current_timestamp, estadodevolucion = 'I', estadonotacredito = 'I', observaciones = ? WHERE coddevolucion = ? ");
			ps.setString(1, devolucion.getObservaciones());
			ps.setInt(2, devolucion.getCodDevolucion());
			ps.executeUpdate();
			
			List<DetalleDevolucion> detalle = devolucion.getDetalleDevolucion();
			
			for (DetalleDevolucion detalleDevolucion : detalle) {
				
				// ***Reduce la cantidad en Stock
				reduceStock(conn, detalleDevolucion.getProducto(), detalleDevolucion.getCantidad(), devolucion.getSucursal());
				
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Setea una nota de credito como usada.
	 * @param conn
	 * @param devolucion
	 * @throws Exception
	 */
	public void setNotaUsada(Connection conn, Integer nronotacredito) throws Exception{
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement("UPDATE devoluciones SET estadonotacredito = 'U' WHERE notacredito = ? ");
			ps.setInt(1, nronotacredito);
			ps.executeUpdate();
			
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Setea una nota de credito como activa.
	 * @param conn
	 * @param nronotacredito
	 * @throws Exception
	 */
	public void setNotaActiva(Connection conn, Integer nronotacredito) throws Exception{
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement("UPDATE devoluciones SET estadonotacredito= 'A' WHERE notacredito = ? ");
			ps.setInt(1, nronotacredito);
			ps.executeUpdate();
			
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * Devuelve una devolucion con detalle incluido a partir de una venta.
	 * @param conn
	 * @param codventa
	 * @return
	 * @throws Exception
	 */
	public Devolucion getDevolucionPorVenta(Connection conn, Integer codventa) throws Exception {
		Devolucion devolucion= null;
		Integer codDevolucion = null;
		
		PreparedStatement ps = conn.prepareStatement("SELECT coddevolucion FROM devoluciones WHERE codventa = ? ");		
//		PreparedStatement psDetalles = null;
//		String sqlDetalle = "SELECT * FROM detalledevoluciones WHERE coddevolucion = ";
//		
		ps.setInt(1, codventa);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			devolucion = new Devolucion();
			codDevolucion = rs.getInt("coddevolucion");
			devolucion = getDevolucion(conn, codDevolucion);
//			devolucion .setCodDevolucion(rs.getInt("coddevolucion"));
//			devolucion .setFecha(rs.getDate("fecha"));
//			devolucion .setTotal(rs.getDouble("total"));
//			devolucion .setNotaCredito(rs.getInt("notacredito"));
//			devolucion .setEstadoDevolucion(rs.getString("estadodevolucion"));
//			devolucion .setEstadoNotaCredito(rs.getString("estadonotacredito"));
//			devolucion .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
//			devolucion .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
//			devolucion .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//			devolucion .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
			
			// Cargando detalles
//			sqlDetalle+=" "+devolucion.getCodDevolucion();
//			psDetalles = conn.prepareStatement(sqlDetalle);
//			List<DetalleDevolucion> detalles = new ArrayList<DetalleDevolucion>();
//			ResultSet rsDetalles = psDetalles.executeQuery();
//			while (rsDetalles.next()) {
//				detalles.add(getDetalleDevolucion(conn, rsDetalles.getInt("coddevolucion"), rsDetalles.getInt("nroitem")));
//			}
//			devolucion. setDetalleDevolucion(detalles);
		}
		rs.close();
		ps.close();
		return devolucion;
	}
	
	/**
	 * Devuelve todas las devoluciones registradas.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Devolucion> getAllDevoluciones(Connection conn) throws Exception {
		
		List<Devolucion> devoluciones = new ArrayList<Devolucion>();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM devoluciones ");	
		ResultSet rs = null;
		try{
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Devolucion devolucion= null;
				devolucion = new Devolucion();
				devolucion .setCodDevolucion(rs.getInt("coddevolucion"));
				devolucion .setFecha(rs.getDate("fecha"));
				devolucion .setTotal(rs.getDouble("total"));
				devolucion .setNotaCredito(rs.getInt("notacredito"));
				devolucion .setEstadoDevolucion(rs.getString("estadodevolucion"));
				devolucion .setEstadoNotaCredito(rs.getString("estadonotacredito"));
				devolucion .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
				devolucion .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
				devolucion .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
				devolucion .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
				
				// Cargando detalles
				List<DetalleDevolucion> detalles = new ArrayList<DetalleDevolucion>();
				PreparedStatement psDetalles = null;
				psDetalles = conn.prepareStatement("SELECT * FROM detalledevoluciones WHERE coddevolucion ="+devolucion.getCodDevolucion());
				ResultSet rsDetalles = psDetalles.executeQuery();
				while (rsDetalles.next()) {
					detalles.add(getDetalleDevolucion(conn, rsDetalles.getInt("coddevolucion"), rsDetalles.getInt("nroitem")));
				}
				devolucion. setDetalleDevolucion(detalles);
				devoluciones.add(devolucion);
				psDetalles.close();
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
		return devoluciones;
	}
	
	
	/**
	 * Devulve todas las notas de credito activas.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Devolucion> getNotasCreditoActivas(Connection conn) throws Exception {
		
		List<Devolucion> notasActivas= new ArrayList<Devolucion>();
		Integer codDevolucion = null;
		PreparedStatement ps = conn.prepareStatement("SELECT coddevolucion FROM devoluciones WHERE estadonotacredito = 'A' ");	
		ResultSet rs = null;
		try{
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Devolucion notaCreditoActiva= null;
				notaCreditoActiva = new Devolucion();
				codDevolucion = rs.getInt("coddevolucion");
				notaCreditoActiva = getDevolucion(conn, codDevolucion);
//				notaCreditoActiva .setCodDevolucion(rs.getInt("coddevolucion"));
//				notaCreditoActiva .setFecha(rs.getDate("fecha"));
//				notaCreditoActiva .setTotal(rs.getDouble("total"));
//				notaCreditoActiva .setNotaCredito(rs.getInt("notacredito"));
//				notaCreditoActiva .setEstadoDevolucion(rs.getString("estadodevolucion"));
//				notaCreditoActiva .setEstadoNotaCredito(rs.getString("estadonotacredito"));
//				notaCreditoActiva .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
//				notaCreditoActiva .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
//				notaCreditoActiva .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//				notaCreditoActiva .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
				notasActivas.add(notaCreditoActiva);
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
		return notasActivas;
	}
	
	/**
	 * Devuelve todas las notas de credito activas de un cliente.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Devolucion> getNotasCreditoActivasCliente(Connection conn, Cliente cli) throws Exception {
		
		List<Devolucion> notasActivas= new ArrayList<Devolucion>();
		Integer codDevolucion = null;
		PreparedStatement ps = conn.prepareStatement("SELECT coddevolucion FROM devoluciones WHERE codcliente = ? AND estadonotacredito = 'A' ");	
		ResultSet rs = null;
		try{
			ps.setInt(1, cli.getCodCliente());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Devolucion notaCreditoActiva= null;
				notaCreditoActiva = new Devolucion();
				codDevolucion = rs.getInt("coddevolucion");
				notaCreditoActiva = getDevolucion(conn, codDevolucion);
//				notaCreditoActiva .setCodDevolucion(rs.getInt("coddevolucion"));
//				notaCreditoActiva .setFecha(rs.getDate("fecha"));
//				notaCreditoActiva .setTotal(rs.getDouble("total"));
//				notaCreditoActiva .setNotaCredito(rs.getInt("notacredito"));
//				notaCreditoActiva .setEstadoDevolucion(rs.getString("estadodevolucion"));
//				notaCreditoActiva .setEstadoNotaCredito(rs.getString("estadonotacredito"));
//				notaCreditoActiva .setVenta(new ControladorVenta().getVentaPorCodVenta(JFramePanambiMain.session.getConn(), rs.getInt("codventa")));
//				notaCreditoActiva .setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
//				notaCreditoActiva .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
//				notaCreditoActiva .setCliente(new ControladorCliente().getCliente(JFramePanambiMain.session.getConn(), rs.getInt("codcliente")));
				notasActivas.add(notaCreditoActiva);
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
		return notasActivas;
	}
	
	
	
	/**
	 * Comprueva si un cliente posee alguna nota de credito.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public boolean poseeNota(Connection conn, Cliente cli) throws Exception {
		
		Integer cantidadNotas = null;
		PreparedStatement ps = conn.prepareStatement("SELECT COUNT(*) AS cantidad FROM devoluciones WHERE codcliente = ? ");	
		ResultSet rs = null;
		try{
			ps.setInt(1, cli.getCodCliente());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				
				cantidadNotas = rs.getInt("cantidad");
			}
			
			if(cantidadNotas == 0 ){
				return false;
			}else{
				return true;
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
	}
	
	/**
	 * Obtiene la venta asociada a la nota de credito utilizada en un pago.
	 * @param conn
	 * @param nota
	 * @return
	 * @throws Exception
	 */
	public Venta getVentaNotaUsada(Connection conn, Devolucion nota) throws Exception {
		
		Venta ret = new Venta();
		ret=null;
		PreparedStatement ps = conn.prepareStatement("SELECT codventa FROM detallepagos, pagos WHERE notacredito = ? ");	
		ResultSet rs = null;
		try{
			ps.setInt(1, nota.getNotaCredito());
			rs = ps.executeQuery();
			
			if (rs.next()) {

				ret = new ControladorVenta().getVentaPorCodVenta(conn, rs.getInt("codventa"));
				
			}else{
				
				ret = null;
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
		return ret;
	}
	
	
	/**
	 * Comprueva si un cliente posee alguna nota de credito en desuso.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public boolean poseeNotaSinUso(Connection conn, Cliente cli) throws Exception {
		
		Integer cantidadNotas = null;
		PreparedStatement ps = conn.prepareStatement("SELECT COUNT(*) AS cantidad FROM devoluciones d WHERE codcliente = ? "+
		" AND d.estadonotacredito = 'A' AND d.notacredito NOT IN (SELECT dp.notacredito FROM detallepagos dp, pagos p "
		+ " WHERE dp.codpago = p.codpago AND dp.notacredito is not null AND p.estado = 'A')");
		
				
		ResultSet rs = null;
		try{
			ps.setInt(1, cli.getCodCliente());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				cantidadNotas = rs.getInt("cantidad");
			}
						
			if(cantidadNotas == 0 ){
				return false;
			}else{
				return true;
			}
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		
	}
	
	
	/**
	 * Obtiene una cada item de una devolucion de producto.
	 * @param conn
	 * @param coddevolucion
	 * @param nroitem
	 * @return
	 * @throws Exception
	 */
	public DetalleDevolucion getDetalleDevolucion(Connection conn, Integer coddevolucion, Integer nroitem) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DetalleDevolucion detalle = null;
		try {
			String sql = "SELECT nroitem, cantidad, montounitario, totalitem, coddevolucion, codproducto "
					+ "FROM detalledevoluciones WHERE coddevolucion = ? and nroitem = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, coddevolucion);
			ps.setInt(2, nroitem);
			rs = ps.executeQuery();
			if (rs.next()) {
				detalle = new DetalleDevolucion();
				detalle. setNroItem(rs.getInt("nroitem"));
				detalle. setCantidad(rs.getInt("cantidad"));
				detalle. setMontoUnitario(rs.getDouble("montounitario"));
				detalle. setTotalItem(rs.getDouble("totalitem"));
				detalle. setCoddevolucion(rs.getInt("coddevolucion"));
				detalle. setProducto(new ControladorProducto().getProducto(conn, rs.getInt("codproducto")) );
			} else {
				throw new ValidException("No existe detalle para la devolucion [ " + coddevolucion+ " ]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return detalle;
	}

	/**
	 * Guarda una devolucion junto con el detalle de la devolucion y devuelve el codigo de la devolucion generada
	 * @param conn Conexi�n a la base de datos
	 * @param devolucion
	 * @throws Exception
	 */
	public Integer guardarDevolucion(Connection conn, Devolucion devolucion) throws Exception {
		PreparedStatement psInsCab = null;
		PreparedStatement psInsDet = null;
		try {
			// ***Insertando cabecera
			conn.setAutoCommit(false);
			String sInsCab = "INSERT INTO devoluciones (total, codventa, codusuario, codsucursal, codcliente, notacredito, observaciones) ";
			sInsCab += "VALUES (?, ?, ?, ?, ?, ?, ?)";
			psInsCab = conn.prepareStatement(sInsCab, Statement.RETURN_GENERATED_KEYS);
			psInsCab.setDouble(1, devolucion.getTotal());
			psInsCab.setInt(2, devolucion.getVenta().getCodVenta());
			psInsCab.setInt(3, devolucion.getUsuario().getCodUsuario());
			psInsCab.setInt(4, devolucion.getSucursal().getCodSucursal());
			psInsCab.setInt(5, devolucion.getCliente().getCodCliente());
			psInsCab.setInt(6, getNumNotaCredito(conn));
			if(devolucion.getObservaciones()!=null && devolucion.getObservaciones()!=""){
				psInsCab.setString(7, devolucion.getObservaciones().trim());
			}else{
				psInsCab.setString(7, null);
			}
			
			psInsCab.executeUpdate();
			
			ResultSet rsKey = psInsCab.getGeneratedKeys();
			Integer coddevolucion = null;
			if (rsKey.next()) {
				coddevolucion = rsKey.getInt(1);
			}
			rsKey.close();

			// ***Insertando detalle
			String sInsDetalle = "INSERT INTO detalledevoluciones(coddevolucion, nroitem, cantidad, montounitario, totalitem, codproducto) ";
			sInsDetalle += " VALUES (?, ?, ?, ?, ?, ?) ";
			psInsDet = conn.prepareStatement(sInsDetalle);
			List<DetalleDevolucion> detalle = devolucion.getDetalleDevolucion();
			psInsDet.setInt(1, coddevolucion);
			for (DetalleDevolucion detalleDevolucion : detalle) {
				psInsDet.setInt(2, detalleDevolucion.getNroItem());
				psInsDet.setInt(3, detalleDevolucion.getCantidad());
				psInsDet.setDouble(4, detalleDevolucion.getMontoUnitario());
				psInsDet.setDouble(5, detalleDevolucion.getTotalItem());
				psInsDet.setInt(6, detalleDevolucion.getProducto().getCodProducto());
				psInsDet.executeUpdate();

				// ***Reponer cantidad en Stock
				repToStock(conn, detalleDevolucion.getProducto(), detalleDevolucion.getCantidad(), devolucion.getSucursal());
				
			}
			conn.commit();
			
			return coddevolucion;
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(psInsCab);
		}
	}
	
	/**
	 * Obtiene el numero de nota de credito a insertar
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	private Integer getNumNotaCredito(Connection conn) throws Exception{
		PreparedStatement ps = null;
		Integer ret = 0;
		String sql = "";
		try{
			if(!existenRegistros(conn)){
				ret = 1;
			}else{
				sql = "SELECT MAX(notacredito)+1 maxnotacredito FROM devoluciones";
				ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				
				if(rs.next()){
					ret = rs.getInt("maxnotacredito");
				}
			}
			
			return ret;
			
		}catch(Exception e){
			throw e;
		}finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Comprueba si existen registros en la tabla devoluciones.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	private boolean existenRegistros(Connection conn) throws Exception{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer cantidad= 0;
		String sql = "";
		
		try{
			sql = "SELECT COUNT(*) AS cantidad FROM devoluciones";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if(rs.next()){
				cantidad = rs.getInt("cantidad");
			}
			
			if(cantidad>0){
				return true;
			}else{
				return false;
			}
			
		}catch(Exception e){
			throw e;
		}finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	
	/**
	 * Repone stock de productos devueltos.
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void repToStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
//		PreparedStatement psInsStock = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
//			boolean insertar = true;
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") + cantidad);
				rsSelUpdateStock.updateRow();
//				insertar = false;
			}
			rsSelUpdateStock.close();

//			if (insertar) {
//				String sInsStock = "INSERT INTO stockproductos(codproducto_productos, codsucursal_sucursales, cantidad) ";
//				sInsStock += "VALUES (?, ?, ?) ";
//				psInsStock = conn.prepareStatement(sInsStock);
//				psInsStock.setInt(1, producto.getCodProducto());
//				psInsStock.setInt(2, sucursal.getCodSucursal());
//				psInsStock.setInt(3, cantidad);
//				psInsStock.executeUpdate();
//			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}
	
	/**
	 * Disminuya el stock de productos de una sucursal. 
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void reduceStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") - cantidad);
				rsSelUpdateStock.updateRow();
			}
			rsSelUpdateStock.close();

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}
	
	/**
	 * Comprueba si hay una devolucion con un nro de factura dado.
	 * @param conn
	 * @param devolucion
	 * @return
	 */
	public boolean facturaYaRegistrada(Connection conn, Devolucion devolucion) throws Exception{
		PreparedStatement ps= null;
		Integer cantidad = 0;
		
		try {
			String sql = "SELECT count(codventa) AS cantidad FROM devoluciones WHERE codventa = ? AND estadodevolucion = 'A'";
			ps= conn.prepareStatement(sql);
			ps.setInt(1, devolucion.getVenta().getCodVenta());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cantidad = rs.getInt("cantidad");	
			}
		
			if(cantidad >0){
				return true;
			}else {
				return false;
			}	
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Retorna la cantidad de devoluciones registradas para una venta.
	 * @param conn
	 * @param venta
	 * @return
	 * @throws Exception
	 */
	public Integer cantidadDevoluciones(Connection conn, Venta venta) throws Exception{
		PreparedStatement ps= null;
		Integer cantidad = 0;
		
		try {
			String sql = "SELECT count(codventa) AS cantidad FROM devoluciones WHERE codventa = ? ";
			ps= conn.prepareStatement(sql);
			ps.setInt(1, venta.getCodVenta());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cantidad = rs.getInt("cantidad");	
			}
			
			return cantidad;
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Comprueba si existe una devolucion activa para un numero de factura.
	 * @param conn
	 * @param nroFactura
	 * @return
	 * @throws Exception
	 */
	public boolean facturaYaRegistrada(Connection conn, Venta venta) throws Exception{
		PreparedStatement ps= null;
		Integer cantidad = 0;
		
		try {
			String sql = "SELECT count(codventa) AS cantidad FROM devoluciones WHERE codventa = ? AND estadodevolucion = 'A' ";
			ps= conn.prepareStatement(sql);
			ps.setInt(1, venta.getCodVenta());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cantidad = rs.getInt("cantidad");	
			}
		
			if(cantidad >0){
				return true;
			}else {
				return false;
			}	
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
}
