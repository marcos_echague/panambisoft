package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.connect.ConnectionManager;

public class ControladorSecuencias extends ControladorPanambi {

	/**
	 * Obtiene el n�mero de recibo a utilizar
	 * @param conn Conexi�n a la base de datos
	 * @return
	 * @throws Exception
	 */
	public Integer getNroRecibo(Connection conn) throws Exception{
		Integer ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			ps = conn.prepareStatement("SELECT nrorecibo FROM secuencias");
			rs = ps.executeQuery();
			
			if(rs.next()){
				ret = rs.getInt("nrorecibo");
			}
			
			if(ret==null){
				iniciarSecuenacias(conn);
				ret = 1;
			}
		}catch (Exception e){
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
		
		
	}
	
	/**
	 * Aumenta el n�mero de recibo de pago de cuotas.
	 * @param conn
	 * @throws Exception
	 */
	public void aumentarNroRecibo(Connection conn) throws Exception {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE secuencias ";
			sql += " SET nrorecibo = nrorecibo + 1 ";
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Inicializa los numeros que controlan las secuencias en 1.
	 * @param conn
	 * @throws Exception
	 */
	public void iniciarSecuenacias(Connection conn) throws Exception {
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO secuencias(nrorecibo, nronotaremision, nrocomprobanteventa) ";
			sql += " VALUES (1, 1, 1) ";
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
}



