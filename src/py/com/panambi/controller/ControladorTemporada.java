package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Temporada;
import py.com.panambi.exceptions.ValidException;

public class ControladorTemporada extends ControladorPanambi{
	
	public void insertarTemporada(Connection conn, Temporada temporada) throws Exception{
		try {
			checkValues(temporada);
			isDuplicated(conn, temporada);
			String sql = "INSERT INTO temporadas ";
			sql += " (nombre, estado) ";
			sql += " VALUES (?,?) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, temporada.getNombre().toString());
			ps.setString(2, temporada.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
	}
	
	/**
	 * Comprueba si hay integridad referencial para eliminar una temporada
	 * @param conn
	 * @param temporada
	 * @return
	 * @throws Exception
	 */
	public boolean comprobarReferencia(Connection conn, Temporada temporada) throws Exception{
		String sql = "SELECT DISTINCT codtemporada FROM productos";
		Integer codigoTemporada ;
		boolean existe = false;
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			codigoTemporada = rs.getInt(1);
			if (codigoTemporada.equals(temporada.getCodTemporada())){
				existe= true;
			}
		}
		
		return existe;
		
	}
	
	/**
	 * Borra una temporada de la base de datos
	 * @param conn
	 * @param temporada
	 * @throws Exception
	 */
	public void borrarTemporada(Connection conn, Temporada temporada) throws Exception {
		try {
			checkSeleccion(temporada);
			String sql = "DELETE FROM temporadas ";
			sql += " WHERE codtemporada = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, temporada.getCodTemporada());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Lista todas las temporadas desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Temporada> getTemporadas(Connection conn) throws Exception {
		List<Temporada> temporadas = new ArrayList<Temporada>();
		PreparedStatement ps = conn.prepareStatement("SELECT codtemporada FROM temporadas ORDER BY nombre");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			temporadas.add(new Temporada(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return temporadas;
	
	}
	
	/**
	 * Obtiene todas las temporadas activas.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Temporada> getTemporadasActivas(Connection conn) throws Exception {
		List<Temporada> temporadas = new ArrayList<Temporada>();
		PreparedStatement ps = conn.prepareStatement("SELECT codtemporada FROM temporadas WHERE estado = 'A' ORDER BY nombre");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			temporadas.add(new Temporada(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return temporadas;
	
	}

	
	/**
	 * Devuelve Temporada por codigo.
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public Temporada getTemporada(Connection conn, Integer codtemporada) throws Exception {
		Temporada ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codtemporada FROM temporadas WHERE codtemporada = ?");
		ps.setInt(1, codtemporada);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new Temporada(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Devuelve Temporada por Nombre
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public Temporada getTemporada(Connection conn, String nombre) throws Exception {
		Temporada ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codtemporada FROM temporadas WHERE nombre = ?");
		ps.setString(1, nombre);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new Temporada(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Actualiza datos de la temporada
	 * @param conn
	 * @param temporada
	 * @throws Exception
	 */
	
	public void modificarTemporada(Connection conn, Temporada temporada) throws Exception {
		try {
			checkSeleccion(temporada);
			checkValues(temporada);
			isDuplicated(conn, temporada);
			String sql = "UPDATE temporadas ";
			sql += " SET nombre = ?,";
			sql += " estado = ?";	
			sql += " WHERE codtemporada = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, temporada.getNombre());
			ps.setString(2, temporada.getEstado());
			ps.setInt(3, temporada.getCodTemporada());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	
	
	
	private void checkValues(Temporada temporada) throws ValidException {
		if (temporada.getNombre() == null) {
			throw new ValidException("Debe ingresar nombre de temporada.");
		}
		if (temporada.getNombre().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre de temporada.");
		}

	}
	
	/**
	 * Verifica que se haya seleccionado la temporada
	 * @param temporada
	 * @throws ValidException
	 */
	private void checkSeleccion(Temporada temporada) throws ValidException {
		if (temporada == null) {
			throw new ValidException("Debe seleccionar una temporada.");
		}
		if (temporada.getCodTemporada() == null) {
			throw new ValidException("Debe seleccionar una temporada.");
		}
	}
	
	
	
	/**
	 * Verifica si existe en la base de datos una temporada con el mismo nombre a la envia
	 * @param conn
	 * @param temporada
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Temporada temporada) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM temporadas ";
		sSelect += "WHERE nombre = ? ";
		if (temporada.getCodTemporada() != null) {
			sSelect += " AND codtemporada <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, temporada.getNombre());
		if (temporada.getCodTemporada() != null) {
			ps.setInt(2,temporada.getCodTemporada());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("La temporada " + temporada.getNombre() + " ya existe.");
		}
		return ret;
	}

}
