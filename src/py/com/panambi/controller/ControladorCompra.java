package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import py.com.panambi.bean.Compra;
import py.com.panambi.bean.DetalleCompra;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Proveedor;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorCompra {
	static Logger logger = Logger.getLogger(ControladorCompra.class.getName());

	public ControladorCompra() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param conn
	 * @param codcompra
	 * @return
	 * @throws Exception
	 */
	public Compra getCompra(Connection conn, Integer codcompra) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement psDetalle = null;
		ResultSet rsDetalle = null;
		ResultSet rs = null;
		Compra ret = null;
		ControladorSucursal controladorSucursal = new ControladorSucursal();
		ControladorProveedor controladorProveedor = new ControladorProveedor();
		ControladorProducto controladorProducto = new ControladorProducto();
		try {
			String sql = "SELECT * FROM compras WHERE codcompra = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codcompra);
			rs = ps.executeQuery();

			String sqlDetalle = "SELECT * FROM detallecompras WHERE codcompra = ? order by nroitem ";
			psDetalle = conn.prepareStatement(sqlDetalle);
			psDetalle.setInt(1, codcompra);
			rsDetalle = psDetalle.executeQuery();
			if (rs.next()) {
				ret = new Compra();
				ret.setCodcompra(codcompra);
				ret.setFecha(rs.getTimestamp("fecha"));
				ret.setEstado(rs.getString("estado"));
				ret.setFacturanro(rs.getString("facturanro"));
				ret.setSucursal(controladorSucursal.getSucursal(conn, rs.getInt("codsucursal")));
				ret.setProveedor(controladorProveedor.getProveedor(conn, rs.getInt("codproveedor")));
				ret.setTotal(rs.getDouble("total"));
				ret.setPagaderodias(rs.getInt("pagaderodias"));
				ret.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				ret.setComentarios(rs.getString("comentarios"));
				List<DetalleCompra> detalle = new ArrayList<DetalleCompra>();
				while (rsDetalle.next()) {
					DetalleCompra detalleCompra = new DetalleCompra();
					detalleCompra.setNroitem(rsDetalle.getInt("nroItem"));
					detalleCompra.setCantidad(rsDetalle.getInt("cantidad"));
					detalleCompra.setCostounitario(rsDetalle.getDouble("costounitario"));
					detalleCompra.setProducto(controladorProducto.getProducto(conn, rsDetalle.getInt("codproducto")));
					detalle.add(detalleCompra);
				}
				ret.setDetalleCompra(detalle);
			} else {
				throw new ValidException("No existe la compra para el c�digo [" + codcompra + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs, rsDetalle);
			ConnectionManager.closeStatments(ps, psDetalle);
		}
		return ret;
	}

	/**
	 * Registra la compra con el detalle incluido
	 * 
	 * @param conn
	 * @param compra
	 * @throws Exception
	 */
	public Integer registrarCompra(Connection conn, Compra compra) throws Exception {
		PreparedStatement psInsCab = null;
		PreparedStatement psInsDet = null;
		PreparedStatement psSelProducto = null;
		try {
			checkData(compra);
			// ***Insertando cabecera
			conn.setAutoCommit(false);
			String sInsCab = "INSERT INTO compras(fecha, total, codproveedor, codsucursal, pagaderodias, facturanro, comentarios) ";
			sInsCab += "VALUES (?, ?, ?, ?, ?, ?, ?)";
			psInsCab = conn.prepareStatement(sInsCab, Statement.RETURN_GENERATED_KEYS);
			psInsCab.setTimestamp(1, compra.getFecha());
			psInsCab.setDouble(2, compra.getTotal());
			psInsCab.setInt(3, compra.getProveedor().getCodProveedor());
			psInsCab.setInt(4, compra.getSucursal().getCodSucursal());
			psInsCab.setInt(5, compra.getPagaderodias());
			if (compra.getFacturanro() != null) {
				psInsCab.setString(6, compra.getFacturanro());
			} else {
				psInsCab.setNull(6, Types.VARCHAR);
			}
			psInsCab.setString(7, compra.getComentarios());

			psInsCab.executeUpdate();
			ResultSet rsKey = psInsCab.getGeneratedKeys();
			Integer codcompra = null;
			if (rsKey.next()) {
				codcompra = rsKey.getInt(1);
			}
			rsKey.close();

			String sSelectProducto = "SELECT codproducto, precio FROM productos WHERE codproducto = ?";
			psSelProducto = conn.prepareCall(sSelectProducto, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// ***Insertando detalle
			String sInsDetalle = "INSERT INTO detallecompras(codcompra, nroitem, cantidad, costounitario, codproducto) ";
			sInsDetalle += " VALUES (?, ?, ?, ?, ?) ";
			psInsDet = conn.prepareStatement(sInsDetalle);
			List<DetalleCompra> detalle = compra.getDetalleCompra();
			psInsDet.setInt(1, codcompra);
			for (DetalleCompra detalleCompra : detalle) {
				psInsDet.setInt(2, detalleCompra.getNroitem());
				psInsDet.setInt(3, detalleCompra.getCantidad());
				psInsDet.setDouble(4, detalleCompra.getCostounitario());
				psInsDet.setInt(5, detalleCompra.getProducto().getCodProducto());
				psInsDet.executeUpdate();

				// ***Actualiza cantidad en Stock
				addToStock(conn, detalleCompra.getProducto(), detalleCompra.getCantidad(), compra.getSucursal());

				// Actualiza precio producto
				psSelProducto.setInt(1, detalleCompra.getProducto().getCodProducto());
				ResultSet rsProducto = psSelProducto.executeQuery();
				if (rsProducto.next()) {
					double newprice = Math.round((detalleCompra.getCostounitario() * 0.40 + detalleCompra.getCostounitario()));
					rsProducto.updateDouble("precio", newprice);
					rsProducto.updateRow();
				}
				rsProducto.close();
			}

			conn.commit();
			return codcompra;
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(psInsCab, psInsDet, psSelProducto);
		}
	}
	
	public void anularCompra(Connection conn, Compra compra) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE compras ";
			sql += " SET estado = 'I', ";
			sql += "comentarios = ?, ";
			sql += "fechaanulacion = now() ";
			sql += " WHERE codcompra = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, compra.getComentarios());
			ps.setInt(2, compra.getCodcompra());
			ps.executeUpdate();
			
			for (DetalleCompra detalleCompra: compra.getDetalleCompra()) {
				// ***Reponer cantidad en Stock
				reduceToStock(conn, detalleCompra.getProducto(), detalleCompra.getCantidad(), compra.getSucursal());
				
			}
			ControladorPago controladorPago = new ControladorPago();
			controladorPago.anularPago(conn, compra);
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public Pago getPagoDeCompra(Connection conn, Compra compra) throws Exception {
		Pago pago= null;
		PreparedStatement ps = conn.prepareStatement("SELECT codpago FROM pagos WHERE codcompra = ? ");		
//		PreparedStatement psDetalles = null;
//		String sqlDetalle = "SELECT * FROM detallepagos WHERE codpago = ";
		
		ps.setInt(1, compra.getCodcompra());
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			pago = new Pago();
			pago = new ControladorPago().getPago(conn, rs.getInt("codpago"));
		}
		rs.close();
		ps.close();
		return pago;
	}
	
	/**
	 * Reduce el stock de productos.
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void reduceToStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") - cantidad);
				rsSelUpdateStock.updateRow();
			}
			rsSelUpdateStock.close();

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}
	
	
	
	/**
	 * Devuelve compras segun parametros
	 * @param conn
	 * @param proveedor
	 * @param sucursal
	 * @param pagados
	 * @param pendientes
	 * @param anulados
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<Compra> getCompras(Connection conn, Proveedor proveedor, Sucursal sucursal,boolean pagados, boolean pendientes, boolean anulados, Date fechaDesde, Date fechaHasta ) throws Exception {
		List<Compra> compras = new ArrayList<Compra>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT * FROM compras ";
			sql+=" WHERE 1 = 1 ";
			if(proveedor!=null){
				sql+=" AND codproveedor =  "+proveedor.getCodProveedor()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(pagados && !pendientes && !anulados){
				sql+=" AND estado =  'P' ";
			}else if(!pagados && pendientes && !anulados){
				sql+=" AND estado =  'N' ";
			}else if(!pagados && !pendientes && anulados){
				sql+=" AND estado =  'I' ";
			}else if(pagados && pendientes && !anulados){
				sql+=" AND (estado = 'P' OR estado = 'N') ";
			}else if(pagados && !pendientes && anulados){
				sql+=" AND (estado =  'P' OR estado = 'I') ";
			}else if(!pagados && pendientes && anulados){
				sql+=" AND (estado =  'N' OR estado = 'I') ";
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			sql+=" ORDER BY fecha DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Compra compra = new Compra();
				compra = getCompra(conn, rs.getInt("codcompra"));
				compras.add(compra);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return compras;
	}
	
	/**
	 * Obtiene el total facturado segun los parametros ingresados.
	 * @param conn
	 * @param proveedor
	 * @param sucursal
	 * @param pagados
	 * @param pendientes
	 * @param anulados
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public Double getMontoFacturado(Connection conn, Proveedor proveedor, Sucursal sucursal,boolean pagados, boolean pendientes, boolean anulados, Date fechaDesde, Date fechaHasta ) throws Exception {
		Double monto = 0.0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT SUM(total) AS suma FROM compras ";
			sql+=" WHERE (estado ='P' OR estado = 'N') "
					+ "AND 1 = 1 ";
			if(proveedor!=null){
				sql+=" AND codproveedor =  "+proveedor.getCodProveedor()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(pagados && !pendientes && !anulados){
				sql+=" AND estado =  'P' ";
			}else if(!pagados && pendientes && !anulados){
				sql+=" AND estado =  'N' ";
			}else if(!pagados && !pendientes && anulados){
				sql+=" AND estado =  'I' ";
			}else if(pagados && pendientes && !anulados){
				sql+=" AND (estado = 'P' OR estado = 'N') ";
			}else if(pagados && !pendientes && anulados){
				sql+=" AND (estado =  'P' OR estado = 'I') ";
			}else if(!pagados && pendientes && anulados){
				sql+=" AND (estado =  'N' OR estado = 'I') ";
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				monto = rs.getDouble("suma");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return monto;
	}
	
	/**
	 * Obtiene el monto total pagado segun los parametros introducidos.
	 * @param conn
	 * @param proveedor
	 * @param sucursal
	 * @param pagados
	 * @param pendientes
	 * @param anulados
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public Double getMontoPagado(Connection conn, Proveedor proveedor, Sucursal sucursal,boolean pagados, boolean pendientes, boolean anulados, Date fechaDesde, Date fechaHasta ) throws Exception {
		Double monto = 0.0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT SUM(total) AS suma FROM compras ";
			sql+=" WHERE estado ='P' "
					+ "AND 1 = 1 ";
			if(proveedor!=null){
				sql+=" AND codproveedor =  "+proveedor.getCodProveedor()+" ";
			}
			if(sucursal!=null){
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal()+" ";
			}
			
			if(pagados && !pendientes && !anulados){
				sql+=" AND estado =  'P' ";
			}else if(!pagados && pendientes && !anulados){
				sql+=" AND estado =  'N' ";
			}else if(!pagados && !pendientes && anulados){
				sql+=" AND estado =  'I' ";
			}else if(pagados && pendientes && !anulados){
				sql+=" AND (estado = 'P' OR estado = 'N') ";
			}else if(pagados && !pendientes && anulados){
				sql+=" AND (estado =  'P' OR estado = 'I') ";
			}else if(!pagados && pendientes && anulados){
				sql+=" AND (estado =  'N' OR estado = 'I') ";
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				monto = rs.getDouble("suma");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return monto;
	}


	/**
	 * Registra la compra con el detalle incluido
	 * 
	 * @param conn
	 * @param compra
	 * @throws Exception
	 */
	public void registrarPagoCompra(Connection conn, Pago pago) throws Exception {
		PreparedStatement psUdp = null;
		try {
			ControladorPago controladorPago = new ControladorPago();
			conn.setAutoCommit(false);
			String sUpdCab = "UPDATE compras SET estado = 'P' ";
			sUpdCab += "WHERE codcompra = ? ";
			psUdp = conn.prepareStatement(sUpdCab);
			psUdp.setInt(1, pago.getCompra().getCodcompra());
			psUdp.executeUpdate();
			controladorPago.registrarPago(conn, pago);
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(psUdp);
		}
	}

	/**
	 * Verifica la consistencia de datos
	 * 
	 * @param compra
	 * @throws Exception
	 */
	private void checkData(Compra compra) throws Exception {
		if (compra == null) {
			throw new ValidException("Datos de la compra incompletos");
		}
		if (compra.getDetalleCompra().isEmpty()) {
			throw new ValidException("Debe agregar al menos un item de detalle para la compra");
		}
		if (compra.getTotal() == 0.0) {
			throw new ValidException("El total de la compra debe ser mayor a 0");
		}
	}

	public List<Compra> listarComprasPendientes(Connection conn, Date apagarDesde, Date apagarHasta, Proveedor proveedor, String facturanro) throws Exception {
		List<Compra> ret = new ArrayList<Compra>();
		PreparedStatement ps = null;
		try {
			ret = new ArrayList<Compra>();
			String sSel = "SELECT codcompra FROM compras ";
			sSel += "WHERE estado = 'N' ";
			if (apagarDesde != null) {
				sSel += "AND fecha + pagaderodias * interval '1 day' >= ? ";
			}
			if (apagarHasta != null) {
				sSel += "AND fecha + pagaderodias * interval '1 day' <= ? ";
			}
			if (proveedor != null) {
				sSel += "AND codproveedor = ? ";
			}
			if (facturanro != null && !facturanro.trim().isEmpty()) {
				sSel += "AND facturanro = ? ";
			}
			sSel += "order by fecha + pagaderodias * interval '1 day' ";
			ps = conn.prepareStatement(sSel);
			int k = 1;
			if (apagarDesde != null) {
				ps.setDate(k, apagarDesde);
				k++;
			}
			if (apagarHasta != null) {
				ps.setDate(k, apagarHasta);
				k++;
			}
			if (proveedor != null) {
				ps.setInt(k, proveedor.getCodProveedor());
				k++;
			}
			if (facturanro != null && !facturanro.trim().isEmpty()) {
				ps.setString(k, facturanro);
				k++;
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Compra compra = getCompra(conn, rs.getInt("codcompra"));
				ret.add(compra);
			}
			rs.close();
			if (ret.isEmpty()) {
				throw new ValidException("No existen facturas pendientes de pago de acuerdo al criterio de b�squeda ingresado");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Actualiza la cantidad del producto en el stock por sucursal
	 * 
	 * @param conn
	 * @param producto
	 * @param cantidad
	 * @param sucursal
	 * @throws Exception
	 */
	private void addToStock(Connection conn, Producto producto, Integer cantidad, Sucursal sucursal) throws Exception {
		PreparedStatement psSelUpdate = null;
		PreparedStatement psInsStock = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, producto.getCodProducto());
			psSelUpdate.setInt(2, sucursal.getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			boolean insertar = true;
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") + cantidad);
				rsSelUpdateStock.updateRow();
				insertar = false;
			}
			rsSelUpdateStock.close();

			if (insertar) {
				String sInsStock = "INSERT INTO stockproductos(codproducto_productos, codsucursal_sucursales, cantidad) ";
				sInsStock += "VALUES (?, ?, ?) ";
				psInsStock = conn.prepareStatement(sInsStock);
				psInsStock.setInt(1, producto.getCodProducto());
				psInsStock.setInt(2, sucursal.getCodSucursal());
				psInsStock.setInt(3, cantidad);
				psInsStock.executeUpdate();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}

	}
}
