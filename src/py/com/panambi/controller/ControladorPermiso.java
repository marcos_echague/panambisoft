package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Permiso;
import py.com.panambi.connect.ConnectionManager;

public class ControladorPermiso extends ControladorPanambi {
	
	/**
	 * Obtiene todos los permisos de un perfil.
	 * @param conn
	 * @param perfilUsuario
	 * @return
	 * @throws Exception
	 */
	public List<Permiso> getPermisos(Connection conn, PerfilUsuario perfilUsuario) throws Exception{
		List<Permiso> retPermisos = new ArrayList<Permiso>();
		PreparedStatement ps = conn.prepareStatement("SELECT codprograma_programas FROM perfiles_programas WHERE codperfil_perfiles = ? ");
		ps.setInt(1,perfilUsuario.getCodPerfil());
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			retPermisos.add(getPermiso(conn, rs.getInt("codprograma_programas")));
		}
		rs.close();
		ps.close();
		
		return retPermisos;
	}
	
	/**
	 * Obitene un permiso (programa o ventana) a partir del codigo del permiso.
	 * @param conn
	 * @param codpermiso
	 * @return
	 * @throws Exception
	 */
	public Permiso getPermiso(Connection conn, Integer codpermiso) throws Exception {
		Permiso ret = new Permiso();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM programas WHERE codprograma = ? ");
		ps.setInt(1,codpermiso);
		ResultSet rs = ps.executeQuery();

		if (rs.next()){
			ret.setCodPermiso(rs.getInt("codprograma"));
			ret.setNombre(rs.getString("nombre"));
			ret.setClase(rs.getString("clase"));
		}
		return ret;
	}
	
	/**
	 * Obtiene todas las opciones para permisos del sistema.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Permiso> getPermisos(Connection conn) throws Exception {
		List<Permiso> ret = new ArrayList<Permiso>();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM programas  ORDER BY nombre ASC ");
		ResultSet rs = ps.executeQuery();

		while (rs.next()){
			Permiso per = new Permiso();
			per.setCodPermiso(rs.getInt("codprograma"));
			per.setNombre(rs.getString("nombre"));
			per.setClase(rs.getString("clase"));
			
			ret.add(per);
		}
		return ret;
	}
	
	
	/**
	 * Obtiene datos de un permiso a partir del nombre del permiso.
	 * @param conn
	 * @param nombrepermiso
	 * @return
	 * @throws Exception
	 */
	public Permiso getPermiso(Connection conn, String nombrepermiso) throws Exception {
		Permiso ret = new Permiso();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM programas WHERE nombre = ? ");
		ps.setString(1,nombrepermiso.toUpperCase().trim());
		ResultSet rs = ps.executeQuery();

		if (rs.next()){
			ret.setCodPermiso(rs.getInt("codprograma"));
			ret.setNombre(rs.getString("nombre"));
			ret.setClase(rs.getString("clase"));
		}
		return ret;
	}
	
	/**
	 * Informa si un perfil tiene un permiso asignado o no.
	 * @param conn
	 * @param codpermiso
	 * @return
	 * @throws Exception
	 */
	public boolean permisoAsignado(Connection conn, PerfilUsuario perfil, Permiso permiso) throws Exception {
		boolean ret = false;
		Integer cantidad= 0;
		PreparedStatement ps = conn.prepareStatement("SELECT count(*) as cantidad FROM perfiles_programas WHERE codperfil_perfiles = ? AND codprograma_programas = ? ");
		ps.setInt(1,perfil.getCodPerfil());
		ps.setInt(2, permiso.getCodPermiso());
		ResultSet rs = ps.executeQuery();

		if (rs.next()){
			cantidad = rs.getInt("cantidad");
		}
		
		if(cantidad == 0){
			ret = false;
		}else ret = true;
		
		return ret;
	}
	
	/**
	 * Asigna un permiso a un perfil.
	 * @param conn
	 * @param perfil
	 * @param permiso
	 * @throws Exception
	 */
	public void asignarPermisoAPerfil(Connection conn, PerfilUsuario perfil, Permiso permiso) throws Exception {
		PreparedStatement ps = null;
		try {
			
			String sql = "INSERT INTO perfiles_programas ";
			sql += " (codperfil_perfiles, codprograma_programas) ";
			sql += " VALUES (?, ?) ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, perfil.getCodPerfil());
			ps.setInt(2, permiso.getCodPermiso());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Elimina un permiso a un perfil de usuario.
	 * @param conn
	 * @param perfil
	 * @param permiso
	 * @throws Exception
	 */
	public void sacarPermiso(Connection conn, PerfilUsuario perfil , Permiso permiso) throws Exception {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM perfiles_programas ";
			sql += " WHERE codperfil_perfiles = ? AND codprograma_programas = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, perfil.getCodPerfil());
			ps.setInt(2, permiso.getCodPermiso());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	
}
