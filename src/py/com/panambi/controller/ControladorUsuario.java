package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.PerfilUsuario;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorUsuario extends ControladorPanambi {

	/**
	 * Inserta Usuario
	 * 
	 * @param conn
	 * @param usuario
	 * @throws Exception
	 */
	public void insertarUsuario(Connection conn, Usuario usuario) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			checkValues(usuario);
			isDuplicated(conn, usuario);
			String sql = "INSERT INTO usuarios ";
			sql += " (usuario, estado, nombrecompleto, passwd, correo) ";
			sql += " VALUES (?, ?, ?, ?, ?) ";
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, usuario.getUsuario());
			ps.setString(2, usuario.getEstado());
			ps.setString(3, usuario.getNombreCompleto());
			ps.setString(4, usuario.getPasswd());
			ps.setString(5, usuario.getCorreo());
			ps.executeUpdate();
			ResultSet rsGenKey = ps.getGeneratedKeys();
			if (rsGenKey.next()) {
				usuario.setCodUsuario(rsGenKey.getInt(1));
			}
			rsGenKey.close();
			actualizarPerfiles(conn, usuario);
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
			conn.setAutoCommit(true);
		}
	}

	/**
	 * Actualiza los perfiles para un Usuario
	 * 
	 * @param conn
	 * @param usuario
	 * @throws Exception
	 */
	private void actualizarPerfiles(Connection conn, Usuario usuario) throws Exception {
		PreparedStatement psEliminar = null;
		PreparedStatement psInsertar = null;
		try {
			// Elimina todos los perfiles actuales
			String sDelete = "DELETE FROM usuarios_perfiles WHERE codusuario_usuarios = ?";
			psEliminar = conn.prepareStatement(sDelete);
			psEliminar.setInt(1, usuario.getCodUsuario());
			psEliminar.executeUpdate();

			// Inserta los perfiles nuevos
			String sInsert = "INSERT INTO usuarios_perfiles (codusuario_usuarios, codperfil_perfiles) VALUES (?, ?)";
			psInsertar = conn.prepareStatement(sInsert);
			psInsertar.setInt(1, usuario.getCodUsuario());
			List<PerfilUsuario> perfiles = usuario.getPerfilesUsuario();
			for (PerfilUsuario perfilUsuario : perfiles) {
				psInsertar.setInt(2, perfilUsuario.getCodPerfil());
				psInsertar.executeUpdate();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psEliminar, psInsertar);
		}

	}

	/**
	 * Actualiza datos del usuario
	 * 
	 * @param conn
	 * @param usuario
	 * @param reestablecer
	 * @throws Exception
	 */
	public void actualizarUsuario(Connection conn, Usuario usuario, boolean reestablecer) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(usuario);
			checkValues(usuario);
			isDuplicated(conn, usuario);
			conn.setAutoCommit(false);
			String sql = "UPDATE usuarios ";
			sql += " SET usuario = ?, estado = ?, nombrecompleto = ?, correo = ? ";
			if (reestablecer) {
				sql += ", passwd = ?";
			}

			sql += " WHERE codusuario = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, usuario.getUsuario());
			ps.setString(2, usuario.getEstado());
			ps.setString(3, usuario.getNombreCompleto());
			ps.setString(4, usuario.getCorreo());

			if (reestablecer) {
				ps.setString(5, usuario.getPasswd());
				ps.setInt(6, usuario.getCodUsuario());
			} else {
				ps.setInt(5, usuario.getCodUsuario());
			}

			ps.executeUpdate();
			actualizarPerfiles(conn, usuario);
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
			conn.setAutoCommit(true);
		}

	}

	/**
	 * Verifica que se haya seleccionado un usuario
	 * 
	 * @param usuario
	 * @throws ValidException
	 */
	private void checkSeleccion(Usuario usuario) throws ValidException {
		if (usuario == null) {
			throw new ValidException("Debe seleccionar un usuario.");
		}
		if (usuario.getCodUsuario() == null) {
			throw new ValidException("Debe seleccionar un usuario.");
		}
	}

	/**
	 * Valida datos de usuario
	 * 
	 * @param usuario
	 * @throws ValidException
	 */
	private void checkValues(Usuario usuario) throws ValidException {
		if (usuario != null) {
			if (usuario.getUsuario().trim().equals("") || usuario.getUsuario() == null || usuario.getUsuario().trim().isEmpty()) {
				throw new ValidException("Debe ingresar nombre de usuario.");
			}
			if (usuario.getCorreo().contains("@") == false) {
				throw new ValidException("Debe ingresar una direccion de correo valida.");
			}
		}

	}

	/**
	 * Valida que usuario no se duplique
	 * 
	 * @param conn
	 * @param usuario
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Usuario usuario) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM usuarios ";
		sSelect += "WHERE usuario = ? ";
		if (usuario.getCodUsuario() != null) {
			sSelect += " AND codusuario <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, usuario.getUsuario());
		if (usuario.getCodUsuario() != null) {
			ps.setInt(2, usuario.getCodUsuario());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			ret = true;
			throw new ValidException("El usuario " + usuario.getUsuario() + " ya existe.");
		}
		return ret;
	}

	/**
	 * Setea password
	 * 
	 * @param conn
	 * @param usuario
	 * @param password
	 * @throws Exception
	 */
	public static void setearPassword(Connection conn, Usuario usuario, String password) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE usuarios ";
			sql += " SET passwd = ?, reset = ? ";
			sql += " WHERE codusuario = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, password);
			ps.setBoolean(2, true);
			ps.setInt(3, usuario.getCodUsuario());
			ps.executeUpdate();
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
			conn.setAutoCommit(true);
		}
	}

	/**
	 * Lista todos los usuarios desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Usuario> getUsuarios(Connection conn) throws Exception {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		PreparedStatement ps = conn.prepareStatement("SELECT codusuario FROM usuarios ORDER BY usuario");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			usuarios.add(new Usuario(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return usuarios;
	}
	
	/**
	 * Obtiene todos los usuarios activos.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Usuario> getUsuariosActivos(Connection conn) throws Exception {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		PreparedStatement ps = conn.prepareStatement("SELECT codusuario FROM usuarios WHERE estado = 'A' ORDER BY usuario");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			usuarios.add(new Usuario(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return usuarios;
	}

	/**
	 * Lista los usuarios que no estan asignado a ningun empleado
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	/*
	 * public List<Usuario> getUsuariosSinAsigancion(Connection conn) throws
	 * Exception { List<Usuario> usuarios = new ArrayList<Usuario>();
	 * PreparedStatement ps = conn.prepareStatement(
	 * "SELECT usuario from usuarios WHERE NOT EXISTS(SELECT codusuario from empleados) ORDER BY usuario"
	 * ); ResultSet rs = ps.executeQuery(); while (rs.next()) { usuarios.add(new
	 * Usuario(conn, rs.getInt(1))); } rs.close(); ps.close(); return usuarios;
	 * }
	 */

	/**
	 * Devuelve Usuario por Nombre
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public Usuario getUsuario(Connection conn, String nombre) throws Exception {
		Usuario ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codusuario FROM usuarios WHERE usuario = ?");
		ps.setString(1, nombre);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new Usuario(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}

	public Usuario getUsuario(Connection conn, Integer codusuario) throws Exception {
		Usuario ret = new Usuario();
		PreparedStatement psPerfiles = null;
		PreparedStatement psUsuarios = null;
		try {
			String sqlPerfiles = "SELECT uper.* FROM usuarios_perfiles uper JOIN perfiles per ON per.codperfil = uper.codperfil_perfiles ";
			sqlPerfiles += "WHERE uper.codusuario_usuarios = ? ";
			sqlPerfiles += "ORDER BY PER.NOMBRE";
			psPerfiles = conn.prepareStatement(sqlPerfiles);

			String sqlUsuarios = "SELECT * FROM usuarios WHERE codusuario = ? ";
			psUsuarios = conn.prepareStatement(sqlUsuarios);
			psUsuarios.setInt(1, codusuario);
			ResultSet rs = psUsuarios.executeQuery();
			if (rs.next()) {
				ret.setCodUsuario(codusuario);
				ret.setNombreCompleto(rs.getString("nombrecompleto"));
				ret.setUsuario(rs.getString("usuario"));
				ret.setEstado(rs.getString("estado"));
				ret.setCorreo(rs.getString("correo"));
				// Cargando perfiles
				List<PerfilUsuario> perfiles = new ArrayList<PerfilUsuario>();
				psPerfiles.setInt(1, codusuario);
				ResultSet rsPerfiles = psPerfiles.executeQuery();
				while (rsPerfiles.next()) {
					perfiles.add(new PerfilUsuario(conn, rsPerfiles.getInt("codperfil_perfiles")));
				}
				rs.close();
				ret.setPerfilesUsuario(perfiles);
			} else {
				throw new ValidException("No existe el usuario para el c�digo [" + codusuario + "]");
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			ConnectionManager.closeStatments(psPerfiles, psUsuarios);
		}
		return ret;
	}

	/**
	 * Registro el intento de fallido y bloquea al usuario si alcanza el n�mero
	 * m�ximo de fallas posibles.
	 * 
	 * @param conn
	 * @param usuario
	 * @throws Exception
	 */
	public void addIntentosFallidos(Connection conn, Usuario usuario) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdate = "SELECT codusuario, intentosfallidos, (intentosfallidos + 1) intentoscalc, estado FROM usuarios WHERE usuario = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdate, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setString(1, usuario.getUsuario());
			ResultSet rsSelUpdate = psSelUpdate.executeQuery();
			if (rsSelUpdate.next()) {
				rsSelUpdate.updateInt("intentosfallidos", rsSelUpdate.getInt("intentoscalc"));
				if (rsSelUpdate.getInt("intentoscalc") >= Usuario.MAX_INTENTOS_FALLIDOS) {
					rsSelUpdate.updateString("estado", "I");
				}
				rsSelUpdate.updateRow();
			}
			rsSelUpdate.close();

		} catch (Exception e) {
			throw e;
		} finally {
			try {
				psSelUpdate.close();
			} catch (Exception e) {
			}
		}

	}

	/**
	 * Inicializa el conteo de intentos fallidos
	 * 
	 * @param conn
	 * @param usuario
	 * @throws Exception
	 */
	public void resetIntentosFallidos(Connection conn, Usuario usuario) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdate = "UPDATE usuarios SET intentosfallidos = 0 WHERE usuario = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdate);
			psSelUpdate.setString(1, usuario.getUsuario());
			psSelUpdate.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				psSelUpdate.close();
			} catch (Exception e) {
			}
		}

	}

}
