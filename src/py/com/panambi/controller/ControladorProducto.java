package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.ImagenProducto;
import py.com.panambi.bean.Producto;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorProducto extends ControladorPanambi {
	
	public Integer getStockDisponible(Connection conn, Producto producto, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		Integer ret = 0;
		try {
			String sSelect = "SELECT st.cantidad ";
			sSelect += "FROM productos prod ";
			sSelect += "JOIN stockproductos st ON (prod.codproducto = st.codproducto_productos)";
			sSelect += "WHERE prod.estado = 'A'";
			sSelect += "AND st.codproducto_productos = ? ";
			sSelect += "AND st.codsucursal_sucursales = ? ";
			ps = conn.prepareStatement(sSelect);
			ps.setInt(1, producto.getCodProducto());
			ps.setInt(2, sucursal.getCodSucursal());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				ret = rs.getInt(1);
			}
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	/**
	 * Obtiene un producto a partir del codigo.
	 * @param conn
	 * @param codproducto
	 * @return
	 * @throws Exception
	 */
	public Producto getProducto(Connection conn, Integer codproducto) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Producto ret = null;

		try {
			String sql = "SELECT * FROM productos WHERE codproducto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codproducto);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Producto();
				ret.setCodProducto(codproducto);
				ret.setDescripcion(rs.getString("descripcion"));
				ret.setPrecio(rs.getDouble("precio"));
				ret.setMaxDescuento(rs.getFloat("maxdescuento"));
				ret.setStockMinimo(rs.getInt("stockminimo"));
				ret.setCodTemporada(rs.getInt("codtemporada"));
				ret.setEstado(rs.getString("estado"));
				ret.setImagenProducto(new ControladorImagenProducto().getImagenProducto(JFramePanambiMain.session.getConn(), codproducto));
			} else {
				throw new ValidException("No existe el producto para el c�digo [" + codproducto + "]");
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}
	
	/**
	 * Devuelve un producto sin enviar una execpcion en el caso que sea nulo.
	 * @param conn
	 * @param codproducto
	 * @return
	 * @throws Exception
	 */
	public Producto getProductoAllowNull(Connection conn, Integer codproducto) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Producto ret = null;

		try {
			String sql = "SELECT * FROM productos WHERE codproducto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codproducto);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Producto();
				ret.setCodProducto(codproducto);
				ret.setDescripcion(rs.getString("descripcion"));
				ret.setPrecio(rs.getDouble("precio"));
				ret.setMaxDescuento(rs.getFloat("maxdescuento"));
				ret.setStockMinimo(rs.getInt("stockminimo"));
				ret.setCodTemporada(rs.getInt("codtemporada"));
				ret.setEstado(rs.getString("estado"));
				ret.setImagenProducto(new ControladorImagenProducto().getImagenProducto(JFramePanambiMain.session.getConn(), codproducto));
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Lista de todos los productos desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Producto> getProductos(Connection conn) throws Exception {
		List<Producto> productos = new ArrayList<Producto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM productos ORDER BY descripcion");
			rs = ps.executeQuery();

			while (rs.next()) {
				Producto producto = new Producto();
				producto.setCodProducto(rs.getInt("codproducto"));
				producto.setDescripcion(rs.getString("descripcion"));
				producto.setPrecio(rs.getDouble("precio"));
				producto.setMaxDescuento(rs.getFloat("maxdescuento"));
				producto.setStockMinimo(rs.getInt("stockminimo"));
				producto.setCodTemporada(rs.getInt("codtemporada"));
				producto.setEstado(rs.getString("estado"));
				productos.add(producto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return productos;
	}

	/**
	 * Devuelve Producto por Descripcion
	 * 
	 * @param conn
	 * @param descricpcion
	 * @return
	 */
	public Producto getProducto(Connection conn, String descripcion) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Producto ret = null;

		try {
			String sql = "SELECT * FROM productos WHERE descripcion = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, descripcion);
			rs = ps.executeQuery();

			if (rs.next()) {
				ret = new Producto();
				ret.setCodProducto(rs.getInt("codproducto"));
				ret.setDescripcion(rs.getString("descripcion"));
				ret.setPrecio(rs.getDouble("precio"));
				ret.setMaxDescuento(rs.getFloat("maxdescuento"));
				ret.setStockMinimo(rs.getInt("stockminimo"));
				ret.setCodTemporada(rs.getInt("codtemporada"));
				ret.setEstado(rs.getString("estado"));
				ret.setImagenProducto(new ControladorImagenProducto().getImagenProducto(JFramePanambiMain.session.getConn(), ret.getCodProducto()));
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

	public void insertarProducto(Connection conn, Producto producto) throws Exception {
		try {
			conn.setAutoCommit(false);
			checkValues(producto);
			isDuplicated(conn, producto);
			String sql = "INSERT INTO productos";
			sql += " ( descripcion, precio, maxdescuento, stockminimo, codtemporada, estado ) ";
			sql += "VALUES ( ?, ?, ?, ?, ?, ? ) ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, producto.getDescripcion());
			String nombreProductoInsertado = producto.getDescripcion();
			ps.setDouble(2, producto.getPrecio());
			ps.setDouble(3, producto.getMaxDescuento());
			ps.setInt(4, producto.getStockMinimo());
			ps.setInt(5, producto.getCodTemporada());
			ps.setString(6, producto.getEstado());
			ps.executeUpdate();

			if (producto.getImagenProducto() != null) {

				insertarImagenProducto(JFramePanambiMain.session.getConn(), producto);
				PreparedStatement psImagen = conn.prepareStatement("INSERT INTO imagenesproductos ( imagen, descripcion, codproducto ) " + " VALUES( ?, ?, ? ) ");
				psImagen.setBytes(1, producto.getImagenProducto().getImagen());
				psImagen.setString(2, producto.getImagenProducto().getDescripcion());

				psImagen.setInt(3, new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), nombreProductoInsertado).getCodProducto());
				psImagen.executeUpdate();
				psImagen.close();

			}
			ps.close();
			conn.commit();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
	}

	private void insertarImagenProducto(Connection conn, Producto producto) throws Exception {

		PreparedStatement psImagen = conn.prepareStatement("INSERT INTO imagenesproductos ( imagen, descripcion, codproducto ) " + " VALUES( ?, ?, ? ) ");

		psImagen.setBytes(1, producto.getImagenProducto().getImagen());
		psImagen.setString(2, producto.getImagenProducto().getDescripcion());
		psImagen.setInt(3, new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), producto.getDescripcion()).getCodProducto());
		psImagen.executeUpdate();
		psImagen.close();
	}

	/**
	 * Actualiza el maximo descuento de los productos que posee una temporada
	 * dada
	 * 
	 * @param conn
	 * @param producto
	 * @param temporada
	 * @throws Exception
	 */

	public void aplicarDescuentoTemporada(Connection conn, float maxDescuento, Integer codTemporada) throws Exception {
		PreparedStatement ps = conn.prepareStatement("UPDATE productos SET maxdescuento = ? WHERE codtemporada = ? ");
		ps.setFloat(1, maxDescuento);
		ps.setInt(2, codTemporada);
		ps.executeUpdate();
		ps.close();
	}

	private void checkValues(Producto producto) throws ValidException {

		if (producto.getDescripcion() == null && producto.getPrecio() == 0.0) {
			throw new ValidException("Debe ingresar nombre y precio del producto.");
		} else if (producto.getDescripcion() == null) {
			throw new ValidException("Debe ingresar nombre de producto.");
		} else if (producto.getPrecio() == 0.0) {
			throw new ValidException("Debe ingresar precio al producto.");
		}

		if (producto.getDescripcion().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre de producto.");
		}

	}

	private void checkSeleccion(Producto producto) throws ValidException {
		if (producto == null) {
			throw new ValidException("Debe seleccionar un producto.");
		}
		if (producto.getCodProducto() == null) {
			throw new ValidException("Debe seleccionar un producto.");
		}
	}

	private boolean isDuplicated(Connection conn, Producto producto) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM productos ";
		sSelect += "WHERE descripcion = ? ";
		if (producto.getCodProducto() != null) {
			sSelect += " AND codproducto <> ? ";
		}

		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, producto.getDescripcion());
		if (producto.getCodProducto() != null) {
			ps.setInt(2, producto.getCodProducto());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("El producto  " + producto.getDescripcion() + " ya existe.");
		}
		return ret;
	}

	/**
	 * Actualiza datos del producto
	 * 
	 * @param conn
	 * @param prodcuto
	 * @throws Exception
	 */
	public void modificarProducto(Connection conn, Producto producto) throws Exception {

		try {
			checkSeleccion(producto);
			checkValues(producto);
			isDuplicated(conn, producto);
			String sql = "UPDATE productos ";
			sql += " SET descripcion = ?, ";
			sql += "precio = ?, ";
			sql += "maxdescuento = ?, ";
			sql += "stockminimo = ?, ";
			sql += "codtemporada = ?, ";
			sql += "estado = ? ";
			sql += " WHERE codproducto = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, producto.getDescripcion());
			ps.setDouble(2, producto.getPrecio());
			ps.setDouble(3, producto.getMaxDescuento());
			ps.setInt(4, producto.getStockMinimo());
			ps.setInt(5, producto.getCodTemporada());
			ps.setString(6, producto.getEstado());
			ps.setInt(7, producto.getCodProducto());
			ps.executeUpdate();
			ps.close();

			if (producto.getImagenProducto() != null) {

				producto.getImagenProducto().setCodProducto(producto.getCodProducto());

				if (existeImagen(JFramePanambiMain.session.getConn(), producto.getCodProducto())) {
					actualizarImagenProducto(JFramePanambiMain.session.getConn(), producto.getImagenProducto());
				} else {
					insertarImagenProducto(JFramePanambiMain.session.getConn(), producto);

				}

			} else {
				borrarImagenProducto(JFramePanambiMain.session.getConn(), producto.getCodProducto());
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	private boolean existeImagen(Connection conn, Integer codProducto) throws Exception {
		boolean existencia = false;

		String sql = "SELECT COUNT(1) FROM imagenesproductos WHERE codproducto = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codProducto);
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}

		if (cantidad > 0) {
			existencia = true;
		}

		return existencia;
	}

	private void actualizarImagenProducto(Connection conn, ImagenProducto imagenProducto) throws Exception {

		if (imagenProducto != null) {
			String sql = "UPDATE imagenesproductos SET ";
			if (imagenProducto.getImagen() != null) {
				sql += "imagen = ? , ";
			} else {
				sql += "imagen = null , ";
			}

			if (imagenProducto.getDescripcion() != null) {
				sql += "descripcion = ?  ";
			} else {
				sql += "descripcion = null ";
			}

			sql += " WHERE codproducto = ? ";

			int indicador = 1;
			PreparedStatement psImagen = conn.prepareStatement(sql);
			if (imagenProducto.getImagen() != null) {
				psImagen.setBytes(indicador, imagenProducto.getImagen());
				indicador = indicador + 1;
			}

			if (imagenProducto.getDescripcion() != null) {
				psImagen.setString(indicador, imagenProducto.getDescripcion());
				indicador = indicador + 1;
			}
			if (imagenProducto.getCodProducto() != null) {
				psImagen.setInt(indicador, imagenProducto.getCodProducto());
				psImagen.executeUpdate();
				psImagen.close();
			}
			psImagen.close();

		}
	}

	public void borrarProducto(Connection conn, Producto producto) throws Exception {
		try {
			checkSeleccion(producto);
			if (producto.getImagenProducto() != null) {
				borrarImagenProducto(JFramePanambiMain.session.getConn(), producto.getImagenProducto());
			}
			String sql = "DELETE FROM productos ";
			sql += " WHERE codproducto = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, producto.getCodProducto());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	private void borrarImagenProducto(Connection conn, ImagenProducto imagenProducto) throws Exception {

		String sql = "DELETE FROM imagenesproductos ";
		sql += " WHERE codproducto = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, imagenProducto.getCodProducto());
		ps.executeUpdate();
		ps.close();

	}

	private void borrarImagenProducto(Connection conn, Integer codProducto) throws Exception {
		String sql = "DELETE FROM imagenesproductos ";
		sql += " WHERE codproducto = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codProducto);
		ps.executeUpdate();
		ps.close();

	}

	/**
	 * 
	 * @param conn
	 * @param codproducto
	 * @param nombreproducto
	 * @param solodisponibles
	 * @param codsucursal
	 * @param soloactivos
	 * @return
	 * @throws Exception
	 */
	public List<Producto> getProductosByFilters(Connection conn, Integer codproducto, String nombreproducto, Boolean solodisponibles, Integer codsucursal, Boolean soloactivos) throws Exception {
		List<Producto> ret = new ArrayList<Producto>();
		PreparedStatement ps = null;
		try {
			String select = "select distinct prod.codproducto from productos prod ";
			if (solodisponibles != null && solodisponibles) {
				select += "join stockproductos stock on prod.codproducto = codproducto_productos ";
			}
			select += "where 1 = 1 ";
			if (codproducto != null) {
				select += "and prod.codproducto = ? ";
			}
			if (nombreproducto != null) {
				select += "and prod.descripcion like ? ";
			}
			if (solodisponibles != null && solodisponibles) {
				select += "and stock.cantidad > 0 ";
			}
			if (codsucursal != null) {
				select += "and stock.codsucursal_sucursales = ? ";
			}
			if (soloactivos) {
				select += "and estado = 'A' ";
			}
			select += "order by prod.codproducto";
			ps = conn.prepareStatement(select);
			int k = 1;
			if (codproducto != null) {
				ps.setInt(k, codproducto);
				k++;
			}
			if (nombreproducto != null) {
				ps.setString(k, "%" + nombreproducto.trim() + "%");
				k++;
			}
			if (codsucursal != null) {
				ps.setInt(k, codsucursal);
				k++;
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				try {
					Producto producto = getProducto(conn, rs.getInt("codproducto"));
					ret.add(producto);
				} catch (ValidException ve) {
				}
			}
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

	/**
	 * Devuelve lista de sucursales donde hay stock del producto
	 * 
	 * @param producto
	 * @return
	 * @throws Exception 
	 */
	public List<Sucursal> getSucursalesDisponibles(Connection conn, Producto producto) throws Exception {
		PreparedStatement ps = null;
		List<Sucursal> sucursales = new ArrayList<Sucursal>();
		try {
			String sSelect = "SELECT distinct st.codsucursal_sucursales ";
			sSelect += "FROM productos prod ";
			sSelect += "JOIN stockproductos st ON (prod.codproducto = st.codproducto_productos)";
			sSelect += "WHERE prod.estado = 'A'";
			sSelect += "AND st.cantidad > 0";
			ps = conn.prepareStatement(sSelect);
			ResultSet rs = ps.executeQuery();
			ControladorSucursal controladorSucursal = new ControladorSucursal();
			while (rs.next()) {
				Sucursal sucursal = controladorSucursal.getSucursal(conn, rs.getInt(1));
				sucursales.add(sucursal);
			}
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return sucursales;
	}
	
	/**
	 * 
	 * @param conn
	 * @param codproducto
	 * @param nombreproducto
	 * @param soloactivos
	 * @return
	 * @throws Exception
	 */
	public List<String[]> getPreciosProductosByFilters(Connection conn, Integer codproducto, String nombreproducto, Boolean soloactivos) throws Exception {
		List<String[]> ret = new ArrayList<String[]>();
		
		PreparedStatement ps = null;
		try {
			String select = "select distinct p.codproducto, p.descripcion, d.costounitario, c.codproveedor, t.razonsocial "
					+ "from productos p join detallecompras d "
					+ "on p.codproducto = d.codproducto "
					+ "join compras c "
					+ "on d.codcompra = c.codcompra "
					+ "join proveedores t "
					+ "on c.codproveedor = t.codproveedor ";
					
			select += "where 1 = 1 ";
			if (codproducto != null) {
				select += "and p.codproducto = ? ";
			}
			if (nombreproducto != null) {
				select += "and p.descripcion like ? ";
			}

			if (soloactivos) {
				select += "and p.estado = 'A' ";
			}
	
			select += "order by codproducto, codproveedor ";
			ps = conn.prepareStatement(select);
			int k = 1;
			if (codproducto != null) {
				ps.setInt(k, codproducto);
				k++;
			}
			if (nombreproducto != null) {
				ps.setString(k, "%" + nombreproducto.trim() + "%");
				k++;
			}
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				//try {
					String[] detalleProducto = { rs.getString(1),rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5) };
					ret.add(detalleProducto);
				//} catch (ValidException ve) {
				//}
			}
			rs.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}

	
	
	public List<Producto> getAuditoriaProductos(Connection conn, boolean inserciones, boolean modificaciones, boolean eliminaciones, Date fechaDesde, Date fechaHasta) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Producto> ret ;

		try {
			ret = new ArrayList<Producto>();
			String sql = "SELECT * FROM au_productos ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Producto p = new Producto();
				p.setAuCod(rs.getInt("cod_auproductos"));
				p.setAuCodSession(rs.getInt("au_codsession"));
				p.setAuUsuario(new ControladorUsuario().getUsuario(conn,rs.getString("au_usuario")));
				p.setAuOperacion(rs.getString("au_operacion"));
				p.setAuFecha(rs.getDate("au_fecha"));
				p.setCodProducto(rs.getInt("codproducto"));
				p.setDescripcion(rs.getString("descripcion"));
				p.setPrecio(rs.getDouble("precio"));
				p.setMaxDescuento(rs.getFloat("maxdescuento"));
				p.setStockMinimo(rs.getInt("stockminimo"));
				p.setCodTemporada(rs.getInt("codtemporada"));
				p.setEstado(rs.getString("estado"));
				
				ret.add(p);
			}
			} catch (Exception e) {
				throw e;
			} finally {
				ConnectionManager.closeResultSets(rs);
				ConnectionManager.closeStatments(ps);
			}
			return ret;
	}
	
}
