package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import py.com.panambi.bean.Parametro;
import py.com.panambi.exceptions.ValidException;

public class ControladorParametro {


	public ControladorParametro() {
	}
	
	/**
	 * Actualiza los valores de los parametros del sistema
	 * @param conn
	 * @param parametro
	 * @throws Exception
	 */
	public void actualizarParametros(Connection conn, Parametro parametro) throws Exception {
		PreparedStatement ps = conn.prepareStatement("UPDATE parametros SET tasaprestamo = ?, "
				+ "sueldominimo = ?,  "
				+ "montobaseprestamo = ? ");
		ps.setFloat(1, parametro.getTasaPrestamo());
		ps.setDouble(2, parametro.getSueldoMinimo());
		ps.setDouble(3, parametro.getMontoBasePrestamo());
		ps.executeUpdate();
		ps.close();
	}
	
	/**
	 * Obtiene todos los valores de los parametros del sistema.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Parametro getDatosParametro(Connection conn) throws Exception {
		Parametro ret = new Parametro();
		String sSelect = "SELECT * FROM parametros ";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sSelect);
		if (rs.next()) {
			ret.setCodparametro(rs.getInt("codparametro"));
			ret.setMontoBasePrestamo(rs.getDouble("montobaseprestamo"));
			ret.setSueldoMinimo(rs.getDouble("sueldominimo"));
			ret.setTasaPrestamo(rs.getFloat("tasaprestamo"));
		} else {
			throw new ValidException("No se encontraron par�metros del sistema.");
		}
		rs.close();
		st.close();
		return ret;
	}

	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Double getTasaPrestamo(Connection conn) throws Exception {
		Double ret = 0.0;
		String sSelect = "SELECT tasaprestamo FROM parametros WHERE codparametro = 1";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sSelect);
		if (rs.next()) {
			ret = rs.getDouble(1);
		} else {
			throw new ValidException("No est� definida la tasa de pr�stamo en la tabla de par�metros.");
		}
		rs.close();
		st.close();
		return ret;
	}

	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Double getSueldoMinimo(Connection conn) throws Exception {
		Double ret = 0.0;
		String sSelect = "SELECT sueldominimo FROM parametros WHERE codparametro = 1";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sSelect);
		if (rs.next()) {
			ret = rs.getDouble(1);
		} else {
			throw new ValidException("No est� definido el sueldo minimo en la tabla de par�metros.");
		}
		rs.close();
		st.close();
		return ret;
	}
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Double getBasePrestamo(Connection conn) throws Exception {
		Double ret = 0.0;
		String sSelect = "SELECT montobaseprestamo FROM parametros WHERE codparametro = 1";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sSelect);
		if (rs.next()) {
			ret = rs.getDouble(1);
		} else {
			throw new ValidException("No est� definido el monto base para la habilitaci�n de pago en cuotas.");
		}
		rs.close();
		st.close();
		return ret;
	}

}
