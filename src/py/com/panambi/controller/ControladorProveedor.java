package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import py.com.panambi.bean.Proveedor;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorProveedor {
	static Logger logger = Logger.getLogger(ControladorProveedor.class.getName());

	public ControladorProveedor() {
		// TODO Auto-generated constructor stub
	}

	public Proveedor getProveedor(Connection conn, Integer codproveedor) throws Exception {
		PreparedStatement ps = null;
		Proveedor ret = null;
		try {
			String sql = "SELECT * FROM proveedores WHERE codproveedor = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codproveedor);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Proveedor();
				ret.setCodProveedor(codproveedor);
				ret.setRazonSocial(rs.getString("razonsocial"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setNroDocumento(rs.getString("nrodocumento"));
				ret.setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe el proveedor para el c�digo [" + codproveedor + "]");
			}
			rs.close();
			ps.close();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	/**
	 * Lista todos los proveedores desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Proveedor> getProveedores(Connection conn) throws Exception {
		List<Proveedor> proveedores = new ArrayList<Proveedor>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM proveedores ORDER BY razonsocial ");
			rs = ps.executeQuery();
			while (rs.next()) {
				Proveedor proveedor = new Proveedor();
				proveedor.setCodProveedor((rs.getInt("codproveedor")));
				proveedor.setRazonSocial((rs.getString("razonsocial")));
				proveedor.setDireccion(rs.getString("direccion"));
				proveedor.setTelefono(rs.getString("telefono"));
				proveedor.setNroDocumento((rs.getString("nrodocumento")));
				proveedor.setEstado((rs.getString("estado")));
				proveedores.add(proveedor);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return proveedores;
	}

	/**
	 * Devuelve Proveedor por Razon Social
	 * 
	 * @param conn
	 * @param razonsocial
	 * @return
	 */
	public Proveedor getProveedor(Connection conn, String razonsocial) throws Exception {
		Proveedor ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT * FROM proveedores WHERE razonsocial = ?");
			ps.setString(1, razonsocial);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Proveedor();
				ret.setCodProveedor(rs.getInt("codproveedor"));
				ret.setRazonSocial(rs.getString("razonsocial"));
				ret.setDireccion(rs.getString("direccion"));
				ret.setTelefono(rs.getString("telefono"));
				ret.setNroDocumento(rs.getString("nrodocumento"));
				ret.setEstado((rs.getString("estado")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;
	}

	public void insertarProveedor(Connection conn, Proveedor proveedor) throws Exception {
		try {
			checkValues(proveedor);
			isDuplicated(conn, proveedor);
			String sql = "INSERT INTO proveedores ";
			sql += " (razonsocial, nrodocumento, direccion, telefono, estado) ";
			sql += " VALUES (?, ?, ?, ?, ? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, proveedor.getRazonSocial());
			ps.setString(2, proveedor.getNroDocumento());
			ps.setString(3, proveedor.getDireccion());
			ps.setString(4, proveedor.getTelefono());
			ps.setString(5, proveedor.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Actualiza datos del proveedor
	 * 
	 * @param conn
	 * @param proveedor
	 * @throws Exception
	 */
	public void modificarProveedor(Connection conn, Proveedor proveedor) throws Exception {
		try {
			checkSeleccion(proveedor);
			checkValues(proveedor);
			isDuplicated(conn, proveedor);
			String sql = "UPDATE proveedores ";
			sql += " SET razonsocial = ?, ";
			sql += "nrodocumento = ?, ";
			sql += "direccion = ?, ";
			sql += "telefono = ? ,";
			sql += "estado = ? ";
			sql += " WHERE codproveedor = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, proveedor.getRazonSocial());
			ps.setString(2, proveedor.getNroDocumento());
			ps.setString(3, proveedor.getDireccion());
			ps.setString(4, proveedor.getTelefono());
			ps.setString(5, proveedor.getEstado());
			ps.setInt(6, proveedor.getCodProveedor());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

	}

	public void borrarProveedor(Connection conn, Proveedor proveedor) throws Exception {
		try {
			checkSeleccion(proveedor);
			String sql = "DELETE FROM proveedores ";
			sql += " WHERE codproveedor = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, proveedor.getCodProveedor());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Verifica que se haya seleccionado la proveedor
	 * 
	 * @param proveedor
	 * @throws ValidException
	 */
	private void checkSeleccion(Proveedor proveedor) throws ValidException {
		if (proveedor == null) {
			throw new ValidException("Debe seleccionar un proveedor.");
		}

		if (proveedor.getCodProveedor() == null) {
			throw new ValidException("Debe seleccionar un proveedor.");
		}
	}

	/**
	 * Verifica si existe en la base de datos un proveedor con la misma razon
	 * social a la envia
	 * 
	 * @param conn
	 * @param proveedor
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Proveedor proveedor) throws Exception {

		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM proveedores ";
		sSelect += "WHERE razonsocial= ? ";

		if (proveedor.getCodProveedor() != null) {
			sSelect += " AND codproveedor <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, proveedor.getRazonSocial());

		if (proveedor.getCodProveedor() != null) {
			ps.setInt(2, proveedor.getCodProveedor());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;

		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();

		if (cantidad > 0) {
			throw new ValidException("La proveedor " + proveedor.getRazonSocial() + " ya existe.");
		}
		return ret;
	}

	/**
	 * Verifica reglas en datos
	 * 
	 * @param proveedor
	 * @throws ValidException
	 */
	private void checkValues(Proveedor proveedor) throws ValidException {

		if (proveedor.getRazonSocial() == null) {
			throw new ValidException("Debe ingresar razon social del proveedor.");
		}
		if (proveedor.getRazonSocial().trim().isEmpty()) {
			throw new ValidException("Debe ingresar razon social del proveedor.");
		}
	}

}
