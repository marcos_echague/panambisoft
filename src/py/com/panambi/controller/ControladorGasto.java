package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Gasto;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.TipoGasto;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorGasto extends ControladorPanambi {
	
	/**
	 * Inserta un nuevo gasto en la base de datos del sistema
	 * @param conn
	 * @param gasto
	 * @throws Exception
	 */
	public void insertarGasto(Connection conn, Gasto gasto, Pago pago) throws Exception {
		PreparedStatement ps = null;
		try {
			checkValues(gasto);
//			isDuplicated(conn, gasto);
			ControladorPago controladorPago = new ControladorPago();
			conn.setAutoCommit(false);
			String sql = "INSERT INTO gastos ";
			sql += " (importe, codtipogasto, nrocomprobante, comentarios,fechavencimiento,codsucursal) ";
			sql += " VALUES (?, ?, ?, ?, ?, ? ) ";
			ps = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, gasto.getImporte());
			ps.setInt(2, gasto.getTipoGasto().getCodigo());
			if (gasto.getNroComprobante() != null) {
				ps.setString(3, gasto.getNroComprobante());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
//			ps.setString(3, gasto.getNroComprobante());
//			ps.setDate(4, gasto.getFechaPago());
			ps.setString(4, gasto.getComentarios().trim());
			ps.setDate(5, gasto.getFechaVencimiento());
			ps.setInt(6, gasto.getSucursal().getCodSucursal());
			ps.executeUpdate();
			ResultSet rsKey = ps.getGeneratedKeys();
			Integer codgasto = null;
			if (rsKey.next()) {
				codgasto = rsKey.getInt(1);
			}
			rsKey.close();
			pago.setGasto(getGasto(conn, codgasto));
			controladorPago.registrarPago(conn, pago);
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Elimina el gasto de la base de datos
	 * 
	 * @param conn
	 * @param gasto
	 * @throws Exception
	 */
	public void borrarGasto(Connection conn, Gasto gasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(gasto);
			String sql = "DELETE FROM gastos ";
			sql += " WHERE codgasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, gasto.getCodGasto());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Anula un gasto, pasa el estado a Inactivo.
	 * 
	 * @param conn
	 * @param gasto
	 * @throws Exception
	 */
	public void anularGasto(Connection conn, Gasto gasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(gasto);
			conn.setAutoCommit(false);
			String sql = "UPDATE gastos ";
			sql += " SET estado = 'I', ";
			sql += "comentarios = ?, ";
			sql += "fechaanulacion = now() ";
			sql += " WHERE codgasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, gasto.getComentarios());
			ps.setInt(2, gasto.getCodGasto());
			ps.executeUpdate();
			ControladorPago controladorPago = new ControladorPago();
//			Pago pago = new Pago();
//			pago = controladorPago.getPago(conn, gA)
			controladorPago.anularPago(conn, gasto);
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}

	/**
	 * Lista todos los gastos desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Gasto> getGastos(Connection conn) throws Exception {
		List<Gasto> gastos = new ArrayList<Gasto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM gastos ORDER BY codTipoGasto");
			rs = ps.executeQuery();
			while (rs.next()) {
				Gasto gasto = new Gasto();
				gasto.setCodGasto(rs.getInt("codgasto"));
				gasto.setTipoGasto(new ControladorTipoGasto().getTipoGasto(JFramePanambiMain.session.getConn(), rs.getInt("codtipogasto")));
				gasto.setImporte(rs.getDouble("importe"));
				gasto.setNroComprobante(rs.getString("nrocomprobante"));
				gasto.setFechaPago(rs.getTimestamp("fechapago"));
				gasto.setComentarios(rs.getString("comentarios"));
				gasto.setFechaVencimiento(rs.getDate("fechavencimiento"));
				gasto.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				gasto.setEstado(rs.getString("estado"));
				gasto.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
				gastos.add(gasto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return gastos;
	}
	
	/**
	 * Obtiene los gastos segun parametros indicados.
	 * @param conn
	 * @param tipoGasto
	 * @param estado
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<Gasto> getGastos(Connection conn, TipoGasto tipoGasto, String estado, Date fechaDesde, Date fechaHasta  ,Sucursal sucursal) throws Exception {
		List<Gasto> gastos = new ArrayList<Gasto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT * FROM gastos ";
			sql+=" WHERE 1 = 1 ";
			if(tipoGasto!=null){
				sql+=" AND codtipogasto =  "+tipoGasto.getCodigo()+" ";
			}
			if(estado!=null){
				if(estado.equals("A")){
					sql+=" AND estado = 'A' ";
				}else sql+=" AND estado = 'I' ";
			}
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fechapago BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fechapago BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fechapago BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			if(sucursal!=null){	
				sql+=" AND codsucursal =  "+sucursal.getCodSucursal();
			}
			
			sql+=" ORDER BY fechapago DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Gasto gasto = new Gasto();
				gasto.setCodGasto(rs.getInt("codgasto"));
				gasto.setTipoGasto(new ControladorTipoGasto().getTipoGasto(JFramePanambiMain.session.getConn(), rs.getInt("codtipogasto")));
				gasto.setImporte(rs.getDouble("importe"));
				gasto.setNroComprobante(rs.getString("nrocomprobante"));
				gasto.setFechaPago(rs.getTimestamp("fechapago"));
				gasto.setComentarios(rs.getString("comentarios"));
				gasto.setFechaVencimiento(rs.getDate("fechavencimiento"));
				gasto.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				gasto.setEstado(rs.getString("estado"));
				gasto.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
				gastos.add(gasto);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return gastos;
	}

	/**
	 * Devuelve un gasto a partir del codigo
	 * @param conn
	 * @param codgasto
	 * @return
	 * @throws Exception
	 */
	public Gasto getGasto(Connection conn, Integer codgasto) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Gasto ret = null;
		try {
			String sql = "SELECT * FROM gastos WHERE codgasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codgasto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Gasto();
				ret.setCodGasto(codgasto);
				ret.setTipoGasto(new ControladorTipoGasto().getTipoGasto(JFramePanambiMain.session.getConn(),rs.getInt("codtipogasto")));
				ret.setImporte(rs.getDouble("importe"));
				ret.setNroComprobante(rs.getString("nrocomprobante"));
				ret.setComentarios(rs.getString("comentarios"));
				ret.setFechaPago(rs.getTimestamp("fechapago"));
				ret.setFechaVencimiento(rs.getDate("fechavencimiento"));
				ret.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				ret.setEstado(rs.getString("estado"));
				ret .setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			} else {
				throw new ValidException("No existe el gasto para el c�digo [" + codgasto + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	/**
	 * Devuelve Gasto por Concepto
	 * 
	 * @param conn
	 * @param concepto
	 * @return
	 */
	public Gasto getGasto(Connection conn, String concepto) throws Exception {
		Gasto ret = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement("SELECT * FROM gastos WHERE codtipogasto = ?");
			ps.setString(1, concepto);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new Gasto();
				ret.setCodGasto(rs.getInt("codgasto"));
				ret.setTipoGasto(new ControladorTipoGasto().getTipoGasto(JFramePanambiMain.session.getConn(),rs.getInt("codtipogasto")));
				ret.setImporte(rs.getDouble("importe"));
				ret.setNroComprobante(rs.getString("nrocomprobante"));
				ret.setFechaPago(rs.getTimestamp("fechapago"));
				ret.setComentarios(rs.getString("comentarios"));
				ret.setFechaVencimiento(rs.getDate("fechavencimiento"));
				ret.setFechaAnulacion(rs.getTimestamp("fechaanulacion"));
				ret.setEstado(rs.getString("estado"));
				ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}

		return ret;
	}

	/**
	 * Actualiza los datos del gasto
	 * @param conn
	 * @param gasto
	 * @throws Exception
	 */
			
	public void modificarGasto(Connection conn, Gasto gasto) throws Exception {
		PreparedStatement ps = null;
		try {
			checkSeleccion(gasto);
			checkValues(gasto);
//			isDuplicated(conn, gasto);
			String sql = "UPDATE gastos ";
			sql += " SET codtipogasto = ?, ";
			sql += "importe = ?, ";
			sql += "nrocomprobante = ?, ";
			sql += "fechapago = ?, ";
			sql += "comentarios = ?, ";
			sql += "fechavencimiento = ? ";
			sql += " WHERE codgasto = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, gasto.getTipoGasto().getCodigo());
			ps.setDouble(2, gasto.getImporte());
			ps.setString(3, gasto.getNroComprobante());
			ps.setTimestamp(4, gasto.getFechaPago());
			ps.setString(5, gasto.getComentarios().trim());
			ps.setDate(6, gasto.getFechaVencimiento());
			ps.setInt(7, gasto.getCodGasto());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Comprueba si los valores de los gastos son correctos
	 * @param gasto
	 * @throws ValidException
	 */
	private void checkValues(Gasto gasto) throws ValidException {
		if (gasto.getTipoGasto() == null) {
			throw new ValidException("Debe seleccionar un concepto de gasto.");
		}
//		if (gasto.getCodTipoGasto().()) {
//			throw new ValidException("Debe seleccionar un concepto de gasto.");
//		}
	}
	
	
	
	/**
	 * Compreuba si un gasto esta seleccionado
	 * @param gasto
	 * @throws ValidException
	 */
	private void checkSeleccion(Gasto gasto) throws ValidException {
		if (gasto == null) {
			throw new ValidException("Debe seleccionar un gasto.");
		}
		if (gasto.getCodGasto() == null) {
			throw new ValidException("Debe seleccionar un gasto.");
		}
	}
	
	/**
	 * Comprueba si existe un valor duplicado para el gasto
	 * @param conn
	 * @param gasto
	 * @return
	 * @throws Exception
	 */
//	private boolean isDuplicated(Connection conn, Gasto gasto) throws Exception {
//		boolean ret = false;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			String sSelect = "SELECT COUNT(1) FROM gastos ";
//			sSelect += "WHERE concepto = ? ";
//			if (gasto.getCodGasto() != null) {
//				sSelect += " AND codgasto <> ? ";
//			}
//			ps = conn.prepareStatement(sSelect);
//			ps.setString(1, gasto.getConcepto());
//			if (gasto.getCodGasto() != null) {
//				ps.setInt(2, gasto.getCodGasto());
//			}
//			rs = ps.executeQuery();
//			int cantidad = 0;
//			if (rs.next()) {
//				cantidad = rs.getInt(1);
//			}
//			if (cantidad > 0) {
//				throw new ValidException("La gasto " + gasto.getConcepto() + " ya existe.");
//			}
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			ConnectionManager.closeResultSets(rs);
//			ConnectionManager.closeStatments(ps);
//		}
//		return ret;
//	}
}
