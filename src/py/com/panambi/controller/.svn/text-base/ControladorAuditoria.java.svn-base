package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import py.com.panambi.connect.ConnectionManager;

public class ControladorAuditoria extends ControladorPanambi{
	
	/**
	 * Obtiene los nombres de la columna de una tabla de la base de datos.
	 * @param conn
	 * @param nombretabla
	 * @return
	 * @throws Exception
	 */
	public List<String> getColumnNames(Connection conn , String nombretabla) throws Exception{
		List<String> ret = new ArrayList<String>();
		PreparedStatement ps = null;
		try{
			String sql ="SELECT upper(column_name) AS columna FROM information_schema.columns WHERE table_name = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, nombretabla.toLowerCase());
			ResultSet rs = ps.executeQuery();
			while (rs.next()){
				ret.add(rs.getString("columna"));
			}
			return ret;
		}catch (Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeStatments(ps);
		}
	}
	
	/**
	 * Retorna dinamicamente el ResultSet 
	 * @param conn
	 * @param tabla
	 * @param inserciones
	 * @param modificaciones
	 * @param elimnaciones
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public ResultSet getResultSetAudit(Connection conn, String tabla, boolean inserciones, boolean modificaciones, boolean elimnaciones, Date fechaDesde, Date fechaHasta) throws Exception{

		String sql ="SELECT au_usuario, au_operacion, au_fecha, ";
		try{
			List<String> columnas = getColumnNames(conn, tabla); 
			
			for (int i = 0;i<columnas.size();i++){
				sql+=columnas.get(i);
				if(i!=columnas.size()-1){
					sql+=", ";
				}else sql+=" ";
			}
			
			sql+=" FROM au_"+tabla;
			sql+= getRestricciones(inserciones, modificaciones, elimnaciones, fechaDesde, fechaHasta);
			sql+= " ORDER BY au_fecha ";
			
			if(tabla.toLowerCase().indexOf("detalle")!= -1){
				sql+= " , nroitem ";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
		
			return rs;
			
		}catch (Exception e ){
			throw e;
		}
	}
	
	/**
	 * Obtiene el resultset de la tabla de auditoria con los detalles.
	 * @param conn
	 * @param tabla
	 * @param inserciones
	 * @param modificaciones
	 * @param elimnaciones
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public ResultSet getResultSetAuditDetails(Connection conn, String tabla, String tablaDetalles, String columnaJoin, boolean inserciones, boolean modificaciones, boolean elimnaciones, Date fechaDesde, Date fechaHasta) throws Exception{

		String sql ="SELECT au_"+tabla+".au_usuario, au_"+tabla+".au_operacion, au_"+tabla+".au_fecha, ";
		try{
			List<String> columnas = getColumnNames(conn, tabla);
			List<String> columnasDestalles = getColumnNames(conn, tablaDetalles);
			
			for (int i = 0;i<columnas.size();i++){
				sql+="au_"+tabla+"."+columnas.get(i);
				if(i!=columnas.size()-1){
					sql+=", ";
				}else sql+=", ";
			}
			
			for (int i = 0;i<columnasDestalles.size();i++){
				sql+="au_"+tablaDetalles+"."+columnasDestalles.get(i);
				if(i!=columnasDestalles.size()-1){
					sql+=", ";
				}else sql+=" ";
			}
			
			sql+=" FROM au_"+tabla+" JOIN au_"+tablaDetalles+" ";
			sql+=" ON au_"+tabla+"."+columnaJoin+"  = au_"+tablaDetalles+"."+columnaJoin+" ";
			sql+= getRestricciones(inserciones, modificaciones, elimnaciones, fechaDesde, fechaHasta);
			sql+= " ORDER BY "+"au_"+tabla+"."+columnaJoin;
			JOptionPane.showMessageDialog(null, sql);
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
		
			return rs;
			
		}catch (Exception e ){
			throw e;
		}
	}
	
	public ResultSet getResultSetAuditDetails2(Connection conn, String tabla, String tablaDetalles, String columnaJoin, boolean inserciones, boolean modificaciones, boolean elimnaciones, Date fechaDesde, Date fechaHasta) throws Exception{

		String sql ="SELECT coalesce(au_"+tabla+".au_usuario::text,'N/A')||', '||coalesce(au_"+tabla+".au_operacion::text,'N/A')||', '||coalesce(au_"+tabla+".au_fecha::text,'N/A')||', '||";
		try{
			List<String> columnas = getColumnNames(conn, tabla);
			List<String> columnasDestalles = getColumnNames(conn, tablaDetalles);
			
			for (int i = 0;i<columnas.size();i++){
				sql+="coalesce(au_"+tabla+"."+columnas.get(i);
				if(i!=columnas.size()-1){
					sql+="::text,'N/A')||', '||";
				}else sql+="::text,'N/A') AS cabecera , ";
			}
			
			sql+="coalesce(au_"+tablaDetalles+".au_usuario::text,'N/A')||', '||coalesce(au_"+tablaDetalles+".au_operacion::text,'N/A')||', '||coalesce(au_"+tablaDetalles+".au_fecha::text,'N/A')||', '||";
			
			for (int i = 0;i<columnasDestalles.size();i++){
				sql+="coalesce(au_"+tablaDetalles+"."+columnasDestalles.get(i);
				if(i!=columnasDestalles.size()-1){
					sql+="::text,'N/A')||', '||";
				}else sql+="::text,'N/A') AS detalle ";
			}
			
			sql+=" FROM au_"+tabla+" JOIN au_"+tablaDetalles+" ";
			sql+=" ON au_"+tabla+"."+columnaJoin+"  = au_"+tablaDetalles+"."+columnaJoin+" ";
			sql+= getRestricciones(inserciones, modificaciones, elimnaciones, fechaDesde, fechaHasta);
			sql+= " ORDER BY "+"au_"+tabla+"."+columnaJoin;
			JOptionPane.showInputDialog("jhkhk", sql);
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
		
			return rs;
			
		}catch (Exception e ){
			throw e;
		}
	}
	
	/**
	 * Retorna dinamicamente el ResultSet 
	 * @param conn
	 * @param tabla
	 * @param inserciones
	 * @param modificaciones
	 * @param elimnaciones
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<ResultSet> getResultSetAuditPrueba(Connection conn, String tabla, boolean inserciones, boolean modificaciones, boolean elimnaciones, Date fechaDesde, Date fechaHasta) throws Exception{
		//ResultSet ret = null;
		String sql ="SELECT au_usuario, au_operacion, au_fecha, ";
		try{
			List<String> columnas = getColumnNames(conn, tabla); 
			
			for (int i = 0;i<columnas.size();i++){
				sql+=columnas.get(i);
				if(i!=columnas.size()-1){
					sql+=", ";
				}else sql+=" ";
			}
			
			sql+=" FROM au_"+tabla;
			sql+= getRestricciones(inserciones, modificaciones, elimnaciones, fechaDesde, fechaHasta);
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			String sqlDetalle ="";
			List<ResultSet> rsDetalleList = new ArrayList<ResultSet>();
			while(rs.next()){
				PreparedStatement psDetalle = null;
				ResultSet rsDetalle = null;
				sqlDetalle ="";
				sqlDetalle = "SELECT au_usuario, au_operacion, au_fecha, ";
				List<String> columnasDetalles = getColumnNames(conn, "detalle"+tabla);
				
				
				for (int i = 0;i<columnasDetalles.size();i++){
					
					sqlDetalle+=columnasDetalles.get(i)+"";
					
					if(i!=columnasDetalles.size()-1){
						sqlDetalle+=", ";
					}else sqlDetalle+=" ";
				}
				
				sqlDetalle+=" FROM au_detalle"+tabla;
				sqlDetalle+=" WHERE "+columnas.get(0)+" = "+rs.getObject(4);
				psDetalle = conn.prepareStatement(sqlDetalle);
				rsDetalle = psDetalle.executeQuery();
				rsDetalleList.add(rsDetalle);
			}
			
		
			return rsDetalleList;
			
		}catch (Exception e ){
			throw e;
		}
	}
	

	/**
	 * Obtiene dinamicamente la lista de los datos de una tabla de auditoria .
	 * @param conn
	 * @param tabla
	 * @param inserciones
	 * @param modificaciones
	 * @param elimnaciones
	 * @param fechaDesde
	 * @param fechaHasta
	 * @return
	 * @throws Exception
	 */
	public List<List<Object>> getInfoAudit(Connection conn, String tabla, boolean inserciones, boolean modificaciones, boolean elimnaciones, Date fechaDesde, Date fechaHasta) throws Exception{

		List<List<Object>> ret = new ArrayList<List<Object>>();
		
		PreparedStatement ps = null;
		String sql ="SELECT au_usuario, au_operacion, au_fecha, ";
		try{
			List<String> columnas = getColumnNames(conn, tabla); 
			
			for (int i = 0;i<columnas.size();i++){
				sql+=columnas.get(i);
				if(i!=columnas.size()-1){
					sql+=", ";
				}else sql+=" ";
			}
			
			sql+=" FROM au_"+tabla;
			
			sql+= getRestricciones(inserciones, modificaciones, elimnaciones, fechaDesde, fechaHasta);
			
			sql+= "  ORDER BY au_fecha ";
			
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()){
				List <Object> row = new ArrayList<Object>();
				row.add(rs.getString("au_usuario").toString());
				row.add(rs.getString("au_operacion"));
				row.add(rs.getDate("au_fecha").toString());
				for (int i = 0 ; i<columnas.size(); i++){
					row.add(rs.getObject(columnas.get(i)));
				}
				ret.add(row);
			}
			return ret;
			
		}catch (Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeStatments(ps);
		}
		
	}
	
	/**
	 * Genera dinamicamente las restricciones sql.
	 * @param i
	 * @param u
	 * @param d
	 * @param desde
	 * @param hasta
	 * @return
	 */
	private String getRestricciones(boolean i, boolean u, boolean d, Date desde, Date hasta){
		String clausula = "";
		if(i && !u && !d){
			clausula += " WHERE au_operacion = 'I' ";
			clausula+= getRestriccionFechaAnd(desde, hasta);
		}else if(!i && u && !d){
			clausula += " WHERE au_operacion = 'U' ";
			clausula+= getRestriccionFechaAnd(desde, hasta);
		}else if(!i && !u && d){
			clausula += " WHERE au_operacion = 'D' ";
			clausula+= getRestriccionFechaAnd(desde, hasta);
		}else if(i && u && !d){
			clausula += " WHERE (au_operacion = 'I' "
					+ " OR au_operacion = 'U') ";
			clausula+= getRestriccionFechaAnd(desde, hasta);
		}else if(i && !u && d){
			clausula += " WHERE (au_operacion = 'I' "
					+ " OR au_operacion = 'D') ";
			clausula+= getRestriccionFechaAnd(desde, hasta);
		}else if(!i && u && d){
			clausula += " WHERE (au_operacion = 'U' "
					+ " OR au_operacion = 'D') ";
			clausula += getRestriccionFechaAnd(desde, hasta);
		}
		
		if(i && u && d){
			clausula+=getRestriccionFechaWhere(desde, hasta);
		}
		
		//clausula+=" ORDER BY au_fecha ";
		
		return clausula;
		
	}
	
	/**
	 * Obtiene dinamicamente la restriccion AND de fechas
	 * @param d
	 * @param h
	 * @return
	 */
	private String getRestriccionFechaAnd(Date d, Date h){
		String ret = "";
		if(d !=null){
			ret += "AND au_fecha BETWEEN '"+d+"' AND ";
			if(h!=null){
				ret+=" '"+h+"' ";
			}else{
				ret+="current_timestamp ";
			}
		}else{
			if(h!=null){
				ret+= "AND au_fecha BETWEEN '01-01-2001' AND '"+h+"' ";
			}else{
				//ret+= "AND au_fecha BETWEEN '01-01-2001' AND current_timestamp ";
			}
		}
		return ret;
	}
	
	
	/**
	 * Obtiene dinamicamente la restriccion WHERE de fecha.
	 * @param d
	 * @param h
	 * @return
	 */
	private String getRestriccionFechaWhere(Date d, Date h){
		String ret = "";
		if(d !=null){
			ret += " WHERE au_fecha BETWEEN '"+d+"' AND ";
			if(h!=null){
				ret+=" '"+h+"' ";
			}else{
				ret+=" current_timestamp ";
			}
		}else{
			if(h!=null){
				ret+= " WHERE au_fecha BETWEEN '01-01-2001' AND '"+h+"' ";
			}else{
				//ret+= "WHERE au_fecha BETWEEN '01-01-2001' AND current_timestamp ";
			}
		}
		return ret;
	}

}
