package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.FormaPago;
import py.com.panambi.exceptions.ValidException;

public class ControladorFormaPago extends ControladorPanambi {

	public void insertarFormaPago(Connection conn, FormaPago formaPago) throws Exception {
		try {
			checkValues(formaPago);
			isDuplicated(conn, formaPago);
			String sql = "INSERT INTO formaspagos ";
			sql += " (nombre, estado ) ";
			sql += " VALUES (?, ? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, formaPago.getNombre());
			ps.setString(2, formaPago.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Actualiza datos de la forma de pago
	 * 
	 * @param connsw
	 * @param formaPago
	 * @throws Exception
	 */
	public void modificarFormaPago(Connection conn, FormaPago formaPago) throws Exception {
		try {
			checkSeleccion(formaPago);
			checkValues(formaPago);
			isDuplicated(conn, formaPago);
			String sql = "UPDATE formaspagos ";
			sql += " SET nombre = ?, ";
			sql += " estado = ? ";
			sql += " WHERE codformapago = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, formaPago.getNombre());
			ps.setString(2, formaPago.getEstado());
			ps.setInt(3, formaPago.getCodFormaPago());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	public void borrarFormaPago(Connection conn, FormaPago formaPago) throws Exception {
		try {
			checkSeleccion(formaPago);
			String sql = "DELETE FROM formaspagos ";
			sql += " WHERE codformapago = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, formaPago.getCodFormaPago());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Devuelve forma de pago por Nombre
	 * 
	 * @param conn
	 * @param nombre
	 * @return
	 */
	public FormaPago getFormaPago(Connection conn, String nombre) throws Exception {
		FormaPago ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codformapago FROM formaspagos WHERE nombre = ?");
		ps.setString(1, nombre);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new FormaPago(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Devuelve forma de pago por codigo
	 * @param conn
	 * @param codformapago
	 * @return
	 * @throws Exception
	 */
	public FormaPago getFormaPago(Connection conn, Integer codformapago) throws Exception {
		FormaPago ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT codformapago FROM formaspagos WHERE codformapago = ?");
		ps.setInt(1, codformapago);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = new FormaPago(conn, rs.getInt(1));
		}
		rs.close();
		ps.close();
		return ret;
	}

	/**
	 * Lista todas las formas de pago desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<FormaPago> getFormasPagos(Connection conn) throws Exception {
		List<FormaPago> formasPago = new ArrayList<FormaPago>();
		String sSel = "SELECT codformapago FROM formaspagos ";
		sSel += "ORDER BY nombre";
		PreparedStatement ps = conn.prepareStatement(sSel);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			formasPago.add(new FormaPago(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return formasPago;

	}

	/**
	 * Lista todas las formas de pago desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<FormaPago> getFormasPagosActivas(Connection conn, ArrayList<Integer> formaspagoexcluidas) throws Exception {
		List<FormaPago> formasPago = new ArrayList<FormaPago>();
		String sSel = "SELECT codformapago FROM formaspagos WHERE estado = 'A' ";
		if (formaspagoexcluidas != null && !formaspagoexcluidas.isEmpty()) {
			sSel += "AND codformapago not in (";
			
			for (Integer fpexc : formaspagoexcluidas) {
				sSel += fpexc + ", ";
			}
			sSel = sSel.substring(0, sSel.length() - 2) + ") ";
		}
		sSel += "ORDER BY codformapago";
		PreparedStatement ps = conn.prepareStatement(sSel);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			formasPago.add(new FormaPago(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return formasPago;

	}
	
	/**
	 * Lista todas las formas de pago desde la Base de Datos
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<FormaPago> getFormasPagosActivas(Connection conn) throws Exception {
		List<FormaPago> formasPago = new ArrayList<FormaPago>();
		String sSel = "SELECT codformapago FROM formaspagos WHERE estado = 'A' ";
		sSel += "ORDER BY codformapago";
		PreparedStatement ps = conn.prepareStatement(sSel);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			formasPago.add(new FormaPago(conn, rs.getInt(1)));
		}
		rs.close();
		ps.close();
		return formasPago;

	}

	/**
	 * Verifica reglas en datos
	 * 
	 * @param formaPago
	 * @throws ValidException
	 */
	private void checkValues(FormaPago formaPago) throws ValidException {
		if (formaPago.getNombre() == null) {
			throw new ValidException("Debe ingresar nombre de la forma de pago.");
		}
		if (formaPago.getNombre().trim().isEmpty()) {
			throw new ValidException("Debe ingresar nombre de la forma de pago.");
		}

	}

	/**
	 * Verifica que se haya seleccionado la forma de pago
	 * 
	 * @param formaPago
	 * @throws ValidException
	 */
	private void checkSeleccion(FormaPago formaPago) throws ValidException {
		if (formaPago == null) {
			throw new ValidException("Debe seleccionar una forma de pago.");
		}
		if (formaPago.getCodFormaPago() == null) {
			throw new ValidException("Debe seleccionar una forma de pago.");
		}
	}

	/**
	 * Verifica si existe en la base de datos una forma de pago con el mismo
	 * nombre a la envia
	 * 
	 * @param conn
	 * @param formaPago
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, FormaPago formaPago) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM formaspagos ";
		sSelect += "WHERE nombre = ? ";
		if (formaPago.getCodFormaPago() != null) {
			sSelect += " AND codformaPago <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, formaPago.getNombre());
		if (formaPago.getCodFormaPago() != null) {
			ps.setInt(2, formaPago.getCodFormaPago());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("La Forma de Pago " + formaPago.getNombre() + " ya existe.");
		}
		return ret;
	}

}
