package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.DetallePago;
import py.com.panambi.bean.DeudaEmpleado;
import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Pago;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorDeudaEmpleado extends ControladorPanambi {

	/**
	 * Devuelve una deuda de empleado a partir del codigo del mismo.
	 * @param conn
	 * @param coddeudaempleado
	 * @return
	 * @throws Exception
	 */
	public DeudaEmpleado getDeudaEmpleado(Connection conn, Integer coddeudaempleado) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DeudaEmpleado ret = null;
		try {
			String sql = "SELECT * FROM deudasempleado WHERE coddeudaempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, coddeudaempleado);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new DeudaEmpleado();
				ret.setCodDeudaEmpleado(rs.getInt("coddeudaempleado"));
				ret.setPagoEmpleado(new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(),rs.getInt("codpagoempleado")));
				ret.setMonto(rs.getDouble("monto"));
				ret.setConcepto(rs.getString("concepto"));

				ret.setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe la deuda para el c�digo [" + coddeudaempleado+ "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	/**
	 * Retorna todas las deudas del empleado 
	 * @param conn
	 * @param empleado
	 * @param estado
	 * @return
	 * @throws Exception
	 */
	public List<DeudaEmpleado> getDeudasEmpleados(Connection conn, Empleado empleado, Sucursal sucursal,   String estado) throws Exception {
		List<DeudaEmpleado> deudasEmpleados = new ArrayList<DeudaEmpleado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		try {
			sql = "SELECT coddeudaempleado FROM deudasempleado dp, pagosempleados pe, empleados e ";
			sql+=" WHERE dp.codpagoempleado = pe.codpagoempleado ";
			sql+=" AND pe.codempleado = e.codempleado";
			
			if(empleado!=null){
				sql+=" AND pe.codempleado =  "+empleado.getCodEmpleado()+" ";
			}
			
			if(sucursal!=null){
				sql+=" AND e.codsucursal=  "+sucursal.getCodSucursal()+" ";
			}
			
			if(estado!=null){
				if(estado.equals("P")){
					sql+=" AND dp.estado = 'P' ";
				}else if (estado.equals("NP")){
					sql+=" AND dp.estado = 'NP' ";
				}else{
					sql+=" AND dp.estado = 'I' ";
				}
			}else{
				sql+=" AND (dp.estado = 'P' or dp.estado = 'NP') ";
			}
			

			
			sql+=" ORDER BY coddeudaempleado DESC ";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				DeudaEmpleado deuda = new DeudaEmpleado();
				deuda =  getDeudaEmpleado(conn, rs.getInt("coddeudaempleado"));
				deudasEmpleados.add(deuda);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return deudasEmpleados;
	}
	
	
	/**
	 * Obtiene una deuda de eompleado a partir de un codigo del pago del empleado.
	 * @param conn
	 * @param codpagoempleado
	 * @return
	 * @throws Exception
	 */
	public DeudaEmpleado getDeudaEmpleadoPorPago(Connection conn, Integer codpagoempleado) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DeudaEmpleado ret = null;
		try {
			String sql = "SELECT * FROM deudasempleado WHERE codpagoempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codpagoempleado);
			rs = ps.executeQuery();
			while (rs.next()) {
				ret = new DeudaEmpleado();
				ret.setCodDeudaEmpleado(rs.getInt("coddeudaempleado"));
				ret.setPagoEmpleado(new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(),rs.getInt("codpagoempleado")));
				ret.setMonto(rs.getDouble("monto"));
				ret.setConcepto(rs.getString("concepto"));
				ret.setEstado(rs.getString("estado"));
			} 
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	/**
	 * Obtiene una deuda de empleado activa.
	 * @param conn
	 * @param codpagoempleado
	 * @return
	 * @throws Exception
	 */
	public DeudaEmpleado getDeudaEmpleadoActivaPorPago(Connection conn, Integer codpagoempleado) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DeudaEmpleado ret = null;
		try {
			String sql = "SELECT * FROM deudasempleado WHERE codpagoempleado = ? AND estado <> 'I' ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codpagoempleado);
			rs = ps.executeQuery();
			if (rs.next()) {
				ret = new DeudaEmpleado();
				ret.setCodDeudaEmpleado(rs.getInt("coddeudaempleado"));
				ret.setPagoEmpleado(new ControladorPagoSalario().getPagoSalario(JFramePanambiMain.session.getConn(),rs.getInt("codpagoempleado")));
				ret.setMonto(rs.getDouble("monto"));
				ret.setConcepto(rs.getString("concepto"));

				ret.setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe la deuda para el pago del empleado con c�digo [" + codpagoempleado+ "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return ret;

	}
	
	
	
	/**
	 * Inserta una deuda de empleado.
	 * @param conn
	 * @param deuda
	 * @throws Exception
	 */
	public void insertarDeudaEmpleado(Connection conn, DeudaEmpleado deuda) throws Exception {
		PreparedStatement ps = null;
		try {

			String sql = "INSERT INTO deudasempleado ";
			sql += " (codpagoempleado, monto, concepto, estado ) ";
			sql += " VALUES (?, ?, ?, ? ) ";
			ps = conn.prepareStatement(sql);//,Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, deuda.getPagoEmpleado().getCodpagosalario());
			ps.setDouble(2, deuda.getMonto());
			ps.setString(3, deuda.getConcepto());
			ps.setString(4, deuda.getEstado());
			ps.executeUpdate();

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	
	/**
	 * Actualiza una deuda de empleado.
	 * @param conn
	 * @param deuda
	 * @throws Exception
	 */
	
	public void modificarDeudaEmpleado(Connection conn, DeudaEmpleado deuda) throws Exception {
		PreparedStatement ps = null;
		try {
			
			String sql = "UPDATE deudasempleado ";
			sql += " SET monto = ?, ";
			sql += "concepto = ?, ";
			sql += "estado = ? ";
			sql += " WHERE coddeudaempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setDouble(1, deuda.getMonto());
			ps.setString(2, deuda.getConcepto());
			ps.setString(3, deuda.getEstado());
			ps.setInt(4, deuda.getCodDeudaEmpleado());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public void anularPagoDeudaEmpleado(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE deudasempleado ";
			sql += " SET estado = 'NP' ";
			sql += " WHERE coddeudaempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
			ps.executeUpdate();
			ControladorPago controladorPago = new ControladorPago();
			controladorPago.anularPago(conn, deudaEmpleado);
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public void setDeudaPagada(Connection conn, DeudaEmpleado deuda, Pago pago) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE deudasempleado ";
			sql += " SET estado = 'P' ";
			sql += " WHERE coddeudaempleado = ? ";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, deuda.getCodDeudaEmpleado());
			ps.executeUpdate();
			registrarPago(conn, deuda, pago);
			conn.commit();
			
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
			conn.setAutoCommit(true);
		}

	}
	
	public void registrarPago(Connection conn, DeudaEmpleado deuda, Pago pago) throws Exception {
		PreparedStatement ps = null;
		PreparedStatement psDetalle = null;
		PreparedStatement psUpdateNotaCredito = null;
		try {
			String sql = "INSERT INTO pagos ";
			sql += " (monto, coddeudaempleado , nrorecibo , codsucursal ) VALUES (?, ?, ?, ? ) ";
			
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, deuda.getMonto());
			ps.setInt(2, deuda.getCodDeudaEmpleado());
			ps.setInt(3, new ControladorPlanPago().getNroRecibo(conn));
			ps.setInt(4,JFramePanambiMain.session.getSucursalOperativa().getCodSucursal());
			
			ps.executeUpdate();
			int codigo = 0;
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				codigo = rs.getInt(1);
			}
			rs.close();
			
			String sUpdateNotaCredito = "UPDATE devoluciones SET estadonotacredito = 'U' WHERE notacredito = ? ";
			psUpdateNotaCredito = conn.prepareStatement(sUpdateNotaCredito);
			

			String sInsDetalle = "INSERT INTO detallepagos( ";
			sInsDetalle += "codpago, nroitem, monto, codbanco, codformapago, chequenro, bouchernro, notacredito) ";
			sInsDetalle += "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
			psDetalle = conn.prepareStatement(sInsDetalle);
			List<DetallePago> detallesPago = pago.getDetallePago();
			psDetalle.setInt(1, codigo);
			int i = 0;
			for (DetallePago detalle : detallesPago) {
				i++;
				psDetalle.setInt(2, i);
				psDetalle.setDouble(3, detalle.getMonto());
				if (detalle.getBanco() != null) {
					psDetalle.setInt(4, detalle.getBanco().getCodbanco());
				} else {
					psDetalle.setNull(4, Types.INTEGER);
				}
				psDetalle.setInt(5, detalle.getFormaPago().getCodFormaPago());
				if (detalle.getChequenro() != null && !detalle.getChequenro().trim().isEmpty()) {
					psDetalle.setString(6, detalle.getChequenro());
				} else {
					psDetalle.setNull(6, Types.VARCHAR);
				}
				if (detalle.getBouchernro() != null && !detalle.getBouchernro().trim().isEmpty()) {
					psDetalle.setString(7, detalle.getBouchernro());
				} else {
					psDetalle.setNull(7, Types.VARCHAR);
				}
				if (detalle.getNotacredito() != null && detalle.getNotacredito() != 0) {
					psDetalle.setInt(8, detalle.getNotacredito());
					psUpdateNotaCredito.setInt(1, detalle.getNotacredito());
					psUpdateNotaCredito.executeUpdate();
				}else{
					psDetalle.setNull(8, Types.INTEGER);
				}
				psDetalle.executeUpdate();
			}

		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
	public void anularDeudaEmpleado(Connection conn, DeudaEmpleado deudaEmpleado) throws Exception {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE deudasempleado ";
			sql += " SET estado = 'I' ";
			sql += " WHERE coddeudaempleado = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, deudaEmpleado.getCodDeudaEmpleado());
			ps.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(ps);
		}
	}
	
}
