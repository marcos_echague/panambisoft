package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import py.com.panambi.bean.Producto;
import py.com.panambi.bean.ProductoPerdido;
import py.com.panambi.bean.Sucursal;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.main.JFramePanambiMain;

public class ControladorProductoPerdido extends ControladorPanambi {
	
	/**
	 * Retorna un producto dado de baja a partir del codigo del producto dado de baja.
	 * @param conn
	 * @param codproductobaja
	 * @return
	 * @throws Exception
	 */
	public ProductoPerdido getProductoPerdido(Connection conn, Integer codproductobaja) throws Exception {
		PreparedStatement ps = null;
		ProductoPerdido ret = new ProductoPerdido();
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("SELECT * FROM productosperdidos WHERE codproductoperdido = ? ");
			ps.setInt(1, codproductobaja);
			rs = ps.executeQuery();
		if (rs.next()) {
			
			ret.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			ret.setFecha(rs.getDate("fecha"));
			ret.setCantidad(rs.getInt("cantidad"));
			ret.setComentario(rs.getString("comentario"));
			ret.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			ret.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			ret.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			ret.setEstado(rs.getString("estado"));
			ret.setFechaAnualcion(rs.getDate("fechaanulacion"));
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return ret;
		
	}
	
	/**
	 * Registra un nuevo producto perdido en la base de datos, y descuenta el stock del producto en cuestion.
	 * @param conn
	 * @param productoPerdido
	 * @throws Exception
	 */
	public void registrarProductoPerdido(Connection conn, ProductoPerdido productoPerdido) throws Exception {
		PreparedStatement ps = null;
		try{
			String sql = "INSERT INTO productosperdidos "
					+ "(fecha, cantidad, comentario, codproducto, codusuario, codsucursal) "
					+ "VALUES (now(), ?, ?, ?, ?, ?) ";
			ps = conn.prepareStatement(sql);
//			ps.setDate(1, productoPerdido.getFecha());
			ps.setInt(1, productoPerdido.getCantidad());
			ps.setString(2, productoPerdido.getComentario());
			ps.setInt(3, productoPerdido.getProducto().getCodProducto());
			ps.setInt(4, productoPerdido.getUsuario().getCodUsuario());
			ps.setInt(5, productoPerdido.getSucursal().getCodSucursal());
			
			ps.executeUpdate();
		}catch(Exception e ){
			throw e;
		}finally{
			ConnectionManager.closeStatments(ps);
		}
		
	}
	
	
	/**
	 * Devuelve todos los registros de productos perdidos
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("SELECT * FROM productosperdidos ");
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaanulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve los registro de los productos perdidos segun los parametros pasados
	 * @param conn
	 * @param usuario
	 * @param producto
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn,Usuario usuario, Producto producto, Sucursal sucursal, Date fechaDesde , Date fechaHasta) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE 1=1 ";
			if(usuario!=null){
				sql+= " AND codusuario = "+usuario.getCodUsuario();
			}
			if(producto!=null){
				sql+= " AND codproducto = "+producto.getCodProducto();
			}
			if(sucursal!=null){
				sql+= " AND codsucursal = "+sucursal.getCodSucursal();
			}	
			
			if(fechaDesde!=null && fechaHasta!=null){
				sql+=" AND fecha BETWEEN '"+fechaDesde+"' AND '"+fechaHasta+"' "; 
			}else if (fechaDesde ==null && fechaHasta !=null){
				sql+=" AND fecha BETWEEN '2000-01-01' AND '"+fechaHasta+"' ";
			}else if (fechaDesde !=null && fechaHasta ==null){
				sql+=" AND fecha BETWEEN "+fechaDesde+"AND '2999-01-01' ";
			}
			
			ps = conn.prepareStatement(sql);
//			ps.setInt(1, usuario.getCodUsuario());
//			ps.setInt(2, producto.getCodProducto());
//			ps.setInt(3, sucursal.getCodSucursal());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaanulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	/**
	 * Devuelve todos los registros de productos perdidos registrados por un usuario.
	 * @param conn
	 * @param usuario
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn, Usuario usuario) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codusuario = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, usuario.getCodUsuario());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve todos los registro de los productos perdidos de un producto dado.
	 * @param conn
	 * @param producto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn, Producto producto ) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codproducto= ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, producto.getCodProducto());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve todos los productos perdidos de una sucursal dada.
	 * @param conn
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codsucursal = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, sucursal.getCodSucursal());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve un los registros de productos perdidos de un usuario y un producto dado.
	 * @param conn
	 * @param usuario
	 * @param producto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn,Usuario usuario, Producto producto) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codusuario = ? "
					+ "AND codproducto = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, usuario.getCodUsuario());
			ps.setInt(2, producto.getCodProducto());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve los registro de productos perdidos de un usuario y una sucursal dada.
	 * @param conn
	 * @param usuario
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn,Usuario usuario, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codusuario = ? "
					+ "AND codsucursal = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, usuario.getCodUsuario());
			ps.setInt(2, sucursal.getCodSucursal());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	/**
	 * Devuelve los registros de productos perdidos dado un producto y una sucursal.
	 * @param conn
	 * @param producto
	 * @param sucursal
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ProductoPerdido> getProductosPerdidos(Connection conn, Producto producto, Sucursal sucursal) throws Exception {
		PreparedStatement ps = null;
		ArrayList <ProductoPerdido> productosPerdidos = new ArrayList<ProductoPerdido>();
		ResultSet rs = null;
		try{
			String sql = "SELECT * FROM productosperdidos "
					+ "WHERE codproducto = ? "
					+ "AND codsucursal = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, producto.getCodProducto());
			ps.setInt(2, sucursal.getCodSucursal());
			rs = ps.executeQuery();
		while (rs.next()) {
			ProductoPerdido productoPerdido = new ProductoPerdido();
			productoPerdido.setCodProductoPerdido(rs.getInt("codproductoperdido"));
			productoPerdido.setFecha(rs.getDate("fecha"));
			productoPerdido.setCantidad(rs.getInt("cantidad"));
			productoPerdido.setComentario(rs.getString("comentario"));
			productoPerdido.setUsuario(new ControladorUsuario().getUsuario(JFramePanambiMain.session.getConn(), rs.getInt("codusuario")));
			productoPerdido.setProducto(new ControladorProducto().getProducto(JFramePanambiMain.session.getConn(), rs.getInt("codproducto")));
			productoPerdido.setSucursal(new ControladorSucursal().getSucursal(JFramePanambiMain.session.getConn(), rs.getInt("codsucursal")));
			productoPerdido.setEstado(rs.getString("estado"));
			productoPerdido.setFechaAnualcion(rs.getDate("fechaAnulacion"));
			productosPerdidos.add(productoPerdido);
		}
	} catch (Exception e) {
		throw e;
	} finally {
		ConnectionManager.closeResultSets(rs);
		ConnectionManager.closeStatments(ps);
	}
		return productosPerdidos;
		
	}
	
	public void anularProductoPerdido(Connection conn, ProductoPerdido productoPerdido) throws Exception {
		PreparedStatement ps = null;
		try {
			conn.setAutoCommit(false);
			String sql = "UPDATE productosperdidos ";
			sql += " SET estado = 'I', ";
			sql += "comentario = ?, ";
			sql += "fechaanulacion = now() ";
			sql += " WHERE codproductoperdido = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, productoPerdido.getComentario());
			ps.setInt(2, productoPerdido.getCodProductoPerdido());
			ps.executeUpdate();
			
			// ***Reponer cantidad en Stock
			addToStock(conn, productoPerdido);
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception e1) {
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
			ConnectionManager.closeStatments(ps);
		}
	}
	
	private void addToStock(Connection conn, ProductoPerdido productoPerdido) throws Exception {
		PreparedStatement psSelUpdate = null;
		try {
			String sSelUpdateStock = "SELECT codproducto_productos, codsucursal_sucursales, cantidad FROM stockproductos WHERE codproducto_productos = ? AND codsucursal_sucursales = ? ";
			psSelUpdate = conn.prepareStatement(sSelUpdateStock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			psSelUpdate.setInt(1, productoPerdido.getProducto().getCodProducto());
			psSelUpdate.setInt(2, productoPerdido.getSucursal().getCodSucursal());
			ResultSet rsSelUpdateStock = psSelUpdate.executeQuery();
			if (rsSelUpdateStock.next()) {
				rsSelUpdateStock.updateInt("cantidad", rsSelUpdateStock.getInt("cantidad") + productoPerdido.getCantidad());
				rsSelUpdateStock.updateRow();
			}
			rsSelUpdateStock.close();
		}catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeStatments(psSelUpdate);
		}
	}
}
