package py.com.panambi.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.bean.Bonificacion;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class ControladorBonificacion extends ControladorPanambi {

	/**
	 * Inserta una nueva bonificacion.
	 * @param conn
	 * @param bonificacion
	 * @throws Exception
	 */
	public void insertarBonificacion(Connection conn, Bonificacion bonificacion) throws Exception {
		try {
			
			isDuplicated(conn, bonificacion);
			String sql = "INSERT INTO bonificaciones ";
			sql += " (descripcion, estado ) ";
			sql += " VALUES (?, ? ) ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, bonificacion.getDescripcion());
			ps.setString(2, bonificacion.getEstado());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Actualiza los datos de una bonifiacion.
	 * @param conn
	 * @param bonificacion
	 * @throws Exception
	 */
	public void modificarBonificacion(Connection conn, Bonificacion bonificacion) throws Exception {
		try {
			isDuplicated(conn, bonificacion);
			String sql = "UPDATE bonificaciones ";
			sql += " SET descripcion = ?, ";
			sql += " estado = ? ";
			sql += " WHERE codbonificacion = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, bonificacion.getDescripcion());
			ps.setString(2, bonificacion.getEstado());
			ps.setInt(3, bonificacion.getCodBonificacion());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Elimna una bonificacion.
	 * @param conn
	 * @param bonificacion
	 * @throws Exception
	 */
	public void borrarBonificacion(Connection conn, Bonificacion bonificacion) throws Exception {
		try {
			String sql = "DELETE FROM bonificaciones ";
			sql += " WHERE codbonificacion = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, bonificacion.getCodBonificacion());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * Devulve todas las bonificaciones.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Bonificacion> getBonificaciones(Connection conn) throws Exception {
		List<Bonificacion> bonificaciones = new ArrayList<Bonificacion>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bonificaciones ORDER BY descripcion");
			rs = ps.executeQuery();
			while (rs.next()) {
				Bonificacion bonificacion= new Bonificacion();
				bonificacion = getBonificacion(conn, rs.getInt("codbonificacion"));
				bonificaciones.add(bonificacion);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return bonificaciones;
	}
	
	/**
	 * Devuelve todas las bonificaciones activas.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<Bonificacion> getBonificacionesActivas(Connection conn) throws Exception {
		List<Bonificacion> bonificaciones = new ArrayList<Bonificacion>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bonificaciones WHERE estado = 'A' ORDER BY descripcion");
			rs = ps.executeQuery();
			while (rs.next()) {
				Bonificacion bonificacion= new Bonificacion();
				bonificacion = getBonificacion(conn, rs.getInt("codbonificacion"));
				bonificaciones.add(bonificacion);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
		return bonificaciones;
	}
	
	/**
	 * Devuelve una bonificacione por codigo de bonificacion.
	 * @param conn
	 * @param codBonificacion
	 * @return
	 * @throws Exception
	 */
	public Bonificacion getBonificacion(Connection conn, Integer codBonificacion) throws Exception {
		Bonificacion ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM bonificaciones WHERE codbonificacion = ?");
		ps.setInt(1, codBonificacion);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = new Bonificacion();
			ret.setCodBonificacion(rs.getInt("codbonificacion"));
			ret.setDescripcion(rs.getString("descripcion"));
			ret.setEstado(rs.getString("estado"));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Devuelve una bonificacion por descripcion.
	 * @param conn
	 * @param descripcion
	 * @return
	 * @throws Exception
	 */
	public Bonificacion getBonificacion(Connection conn, String descripcion) throws Exception {
		Bonificacion ret = null;
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM bonificaciones WHERE descripcion = ?");
		ps.setString(1, descripcion);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = new Bonificacion();
			ret = getBonificacion(conn, rs.getInt("codbonificacion"));
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	/**
	 * Comprueba si la bonificacion ya existe
	 * @param conn
	 * @param bonificacion
	 * @return
	 * @throws Exception
	 */
	private boolean isDuplicated(Connection conn, Bonificacion bonificacion) throws Exception {
		boolean ret = false;
		String sSelect = "SELECT COUNT(1) FROM bonificaciones ";
		sSelect += "WHERE descripcion = ? ";
		if (bonificacion.getCodBonificacion() != null) {
			sSelect += " AND codbonificacion <> ? ";
		}
		PreparedStatement ps = conn.prepareStatement(sSelect);
		ps.setString(1, bonificacion.getDescripcion());
		if (bonificacion.getCodBonificacion() != null) {
			ps.setInt(2, bonificacion.getCodBonificacion());
		}
		ResultSet rs = ps.executeQuery();
		int cantidad = 0;
		if (rs.next()) {
			cantidad = rs.getInt(1);
		}
		rs.close();
		ps.close();
		if (cantidad > 0) {
			throw new ValidException("La bonificacion " + bonificacion.getDescripcion() + " ya existe.");
		}
		return ret;
	}
}
