/**
 * 
 */
package py.com.panambi.interfaces;


/**
 *
 */
public interface Browseable {
	public void setValues(Object obj, Integer source);
	public void setNoValues(Object obj, Integer source);
}
