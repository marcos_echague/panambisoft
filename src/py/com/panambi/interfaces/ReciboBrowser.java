package py.com.panambi.interfaces;

public interface ReciboBrowser {
	public void doRecibo(boolean generar, String titular , String concepto, String montoLetras , Double monto, Integer nrorecibo, Double vuelto);
}
