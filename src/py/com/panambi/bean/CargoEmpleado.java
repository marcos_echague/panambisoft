package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CargoEmpleado extends PanambiBean {
	
	private Integer codCargoEmpleado;
	private String descripcion;
	private String estado;
	
	public CargoEmpleado(){
		
	}
	
	public CargoEmpleado(Connection conn , Integer codCargo) throws Exception {
		
		String sql = "SELECT * FROM cargosempleados WHERE codcargoempleado = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codCargo);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			setCodCargoEmpleado(codCargo);
			setDescripcion(rs.getString("descripcion"));
			setEstado(rs.getString("estado"));
		}
		
		ps.close();
		rs.close();
		
	}

	public Integer getCodCargoEmpleado() {
		return codCargoEmpleado;
	}

	public void setCodCargoEmpleado(Integer codCargoEmpleado) {
		this.codCargoEmpleado = codCargoEmpleado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}