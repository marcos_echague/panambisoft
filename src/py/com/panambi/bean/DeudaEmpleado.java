package py.com.panambi.bean;

public class DeudaEmpleado extends PanambiBean {

	private Integer codDeudaEmpleado;
	private PagoSalario pagoEmpleado;
	private Double monto;
	private String concepto;
	private String estado;
	
	public Integer getCodDeudaEmpleado() {
		return codDeudaEmpleado;
	}
	public void setCodDeudaEmpleado(Integer codDeudaEmpleado) {
		this.codDeudaEmpleado = codDeudaEmpleado;
	}
	
	public PagoSalario getPagoEmpleado() {
		return pagoEmpleado;
	}
	public void setPagoEmpleado(PagoSalario pagoEmpleado) {
		this.pagoEmpleado = pagoEmpleado;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
