package py.com.panambi.bean;

public class DetalleCompra extends PanambiBean {
	private Integer nroitem;
	private Integer cantidad;
	private Double costounitario;
	private Producto producto;

	public DetalleCompra() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the nroitem
	 */
	public Integer getNroitem() {
		return nroitem;
	}

	/**
	 * @param nroitem
	 *            the nroitem to set
	 */
	public void setNroitem(Integer nroitem) {
		this.nroitem = nroitem;
	}

	/**
	 * @return the cantidad
	 */
	public Integer getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad
	 *            the cantidad to set
	 */
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the costounitario
	 */
	public Double getCostounitario() {
		return costounitario;
	}

	/**
	 * @param costounitario
	 *            the costounitario to set
	 */
	public void setCostounitario(Double costounitario) {
		this.costounitario = costounitario;
	}

	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}

	/**
	 * @param producto
	 *            the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}
