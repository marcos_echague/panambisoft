package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.InvalidUserException;
import py.com.panambi.exceptions.ValidException;

public class Usuario extends PanambiBean {
	private Integer codUsuario;
	private String usuario;
	private String passwd;
	private String estado;
	private String nombreCompleto;
	private String correo;
	private Boolean reset;
	private Integer intentosfallidos;
	private List<PerfilUsuario> perfilesUsuario;
	public static final Integer MAX_INTENTOS_FALLIDOS = 5;//M�ximo n�mero de intentos fallidos

	public Usuario() {

	}

	public Usuario(Connection conn, Integer codusuario) throws Exception {
		PreparedStatement psPerfiles = null;
		PreparedStatement psUsuarios = null;
		try {
			String sqlPerfiles = "SELECT uper.* FROM usuarios_perfiles uper JOIN perfiles per ON per.codperfil = uper.codperfil_perfiles ";
			sqlPerfiles += "WHERE uper.codusuario_usuarios = ? ";
			sqlPerfiles += "ORDER BY PER.NOMBRE";
			psPerfiles = conn.prepareStatement(sqlPerfiles);

			String sqlUsuarios = "SELECT * FROM usuarios WHERE codusuario = ? ";
			psUsuarios = conn.prepareStatement(sqlUsuarios);
			psUsuarios.setInt(1, codusuario);
			ResultSet rs = psUsuarios.executeQuery();
			if (rs.next()) {
				setCodUsuario(codusuario);
				setNombreCompleto(rs.getString("nombrecompleto"));
				setUsuario(rs.getString("usuario"));
				setEstado(rs.getString("estado"));
				setCorreo(rs.getString("correo"));
				setIntentosfallidos(rs.getInt("intentosfallidos"));
				// Cargando perfiles
				List<PerfilUsuario> perfiles = new ArrayList<PerfilUsuario>();
				psPerfiles.setInt(1, codusuario);
				ResultSet rsPerfiles = psPerfiles.executeQuery();
				while (rsPerfiles.next()) {
					perfiles.add(new PerfilUsuario(conn, rsPerfiles.getInt("codperfil_perfiles")));
				}
				rs.close();
				setPerfilesUsuario(perfiles);
			} else {
				throw new ValidException("No existe el usuario para el c�digo [" + codusuario + "]");
			}
			rs.close();
			psUsuarios.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			ConnectionManager.closeStatments(psPerfiles, psUsuarios);
		}
	}

	public Usuario(Connection conn, String nombreUsuario) throws Exception {
		PreparedStatement ps = null;
		String sql = "SELECT * FROM usuarios WHERE usuario = ? ";
		ps = conn.prepareStatement(sql);
		ps.setString(1, nombreUsuario);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setCodUsuario(rs.getInt("codusuario"));
			setUsuario(rs.getString("usuario"));
			setPasswd(rs.getString("passwd"));
			setEstado(rs.getString("estado"));
			setNombreCompleto(rs.getString("nombrecompleto"));
			setCorreo(rs.getString("correo"));
			setReset(rs.getBoolean("reset"));
			setIntentosfallidos(rs.getInt("intentosfallidos"));
		}else{
			throw new InvalidUserException("Usuario inexistente");
		}
		rs.close();
		ps.close();
	}

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public List<PerfilUsuario> getPerfilesUsuario() {
		return perfilesUsuario;
	}

	public void setPerfilesUsuario(List<PerfilUsuario> perfilesUsuario) {
		this.perfilesUsuario = perfilesUsuario;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setReset(Boolean reset) {
		this.reset = reset;
	}

	public Boolean getReset() {
		return reset;
	}

	/**
	 * @return the intentosfallidos
	 */
	public Integer getIntentosfallidos() {
		return intentosfallidos;
	}

	/**
	 * @param intentosfallidos
	 *            the intentosfallidos to set
	 */
	public void setIntentosfallidos(Integer intentosfallidos) {
		this.intentosfallidos = intentosfallidos;
	}
}
