package py.com.panambi.bean;

import java.sql.Date;

public class Cliente extends PanambiBean {
	
	private Integer codCliente;
	private String nroDocumento;
	private String nombres;
	private String apellidos;
	private String telefono;
	private String email;
	private String direccion;
	private Date fechaIngreso;
	private String estado;
	
	public Cliente(){
		
	}
	
	public Integer getCodCliente() {
		return codCliente;
	}


	public void setCodCliente(Integer codCliente) {
		this.codCliente = codCliente;
	}


	public String getNroDocumento() {
		return nroDocumento;
	}


	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}


	public String getNombres() {
		return nombres;
	}


	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}


	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
