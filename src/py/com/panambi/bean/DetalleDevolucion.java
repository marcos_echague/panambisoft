package py.com.panambi.bean;

public class DetalleDevolucion extends PanambiBean {
	
	private Integer nroItem;
	private Integer cantidad;
	private Double montoUnitario;
	private Double totalItem;
	private Integer coddevolucion;
	private Producto producto;
	
	
	public DetalleDevolucion() {
		// TODO Auto-generated constructor stub
	}

	public Integer getNroItem() {
		return nroItem;
	}

	public void setNroItem(Integer nroItem) {
		this.nroItem = nroItem;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getMontoUnitario() {
		return montoUnitario;
	}

	public void setMontoUnitario(Double montoUnitario) {
		this.montoUnitario = montoUnitario;
	}

	public Double getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(Double totalItem) {
		this.totalItem = totalItem;
	}

	public Integer getCoddevolucion() {
		return coddevolucion;
	}

	public void setCoddevolucion(Integer coddevolucion) {
		this.coddevolucion = coddevolucion;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	
}
