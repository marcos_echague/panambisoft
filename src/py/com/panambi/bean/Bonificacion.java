package py.com.panambi.bean;

public class Bonificacion extends PanambiBean {
	
	Integer codBonificacion;
	String descripcion;
	String estado;
	
	public Bonificacion() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCodBonificacion() {
		return codBonificacion;
	}

	public void setCodBonificacion(Integer codBonificacion) {
		this.codBonificacion = codBonificacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
}
