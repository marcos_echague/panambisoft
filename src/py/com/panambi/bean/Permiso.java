package py.com.panambi.bean;

public class Permiso extends PanambiBean {
	private Integer codPermiso;
	private String nombre;
	private String clase;
	
	public Integer getCodPermiso() {
		return codPermiso;
	}
	public void setCodPermiso(Integer codPermiso) {
		this.codPermiso = codPermiso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	
	
	
}
