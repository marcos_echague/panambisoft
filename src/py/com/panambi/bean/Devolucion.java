package py.com.panambi.bean;

import java.sql.Date;
import java.util.List;

public class Devolucion extends PanambiBean {
	
	private Integer codDevolucion;
	private Date fecha;
	private Double total;
	private Venta venta;
	private Usuario usuario;
	private Sucursal sucursal;
	private Cliente cliente;
	private Integer notaCredito;
	private String estadoDevolucion;
	private String estadoNotaCredito;
	private List<DetalleDevolucion> detalleDevolucion;
	private String observaciones;
	private Date fechaAnulacion;
	
	public Integer getCodDevolucion() {
		return codDevolucion;
	}
	public void setCodDevolucion(Integer codDevolucion) {
		this.codDevolucion = codDevolucion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Integer getNotaCredito() {
		return notaCredito;
	}
	public void setNotaCredito(Integer notaCredito) {
		this.notaCredito = notaCredito;
	}
	
	public String getEstadoDevolucion() {
		return estadoDevolucion;
	}
	public void setEstadoDevolucion(String estadoDevolucion) {
		this.estadoDevolucion = estadoDevolucion;
	}
	public String getEstadoNotaCredito() {
		return estadoNotaCredito;
	}
	public void setEstadoNotaCredito(String estadoNotaCredito) {
		this.estadoNotaCredito = estadoNotaCredito;
	}
	public List<DetalleDevolucion> getDetalleDevolucion() {
		return detalleDevolucion;
	}
	public void setDetalleDevolucion(List<DetalleDevolucion> detalleDevolucion) {
		this.detalleDevolucion = detalleDevolucion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Date getFechaAnulacion() {
		return fechaAnulacion;
	}
	public void setFechaAnulacion(Date fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}	
	
	

}
