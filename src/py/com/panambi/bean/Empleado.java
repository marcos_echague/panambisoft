package py.com.panambi.bean;

import java.util.Date;

public class Empleado extends PanambiBean {
	private Integer codEmpleado;
	private String nroDocumento;
	private String nombre;
	private String apellido;
	private String direccion;
	private String telefono;
	private String email;
	private Double salario;
	private Date fechaIngreso;
	private Date fechaSalido;
	private String codAsistencia;
	private Sucursal sucursal;
	private Usuario usuario;
	private CargoEmpleado cargoEmpleado;
	private String estado;
	public Integer getCodEmpleado() {
		return codEmpleado;
	}
	public void setCodEmpleado(Integer codEmpleado) {
		this.codEmpleado = codEmpleado;
	}
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Double getSalario() {
		return salario;
	}
	public void setSalario(Double salario) {
		this.salario = salario;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Date getFechaSalido() {
		return fechaSalido;
	}
	public void setFechaSalido(Date fechaSalido) {
		this.fechaSalido = fechaSalido;
	}
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public CargoEmpleado getCargoEmpleado() {
		return cargoEmpleado;
	}
	public void setCargoEmpleado(CargoEmpleado cargoEmpleado) {
		this.cargoEmpleado = cargoEmpleado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodAsistencia() {
		return codAsistencia;
	}
	public void setCodAsistencia(String codAsistencia) {
		this.codAsistencia = codAsistencia;
	}
	
	
}
