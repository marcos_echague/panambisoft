package py.com.panambi.bean;


public class DetallePagoSalario extends PanambiBean{
	 
	  private Integer codpagosalario;
	  private Integer nroitem;
	  private Double monto;
	  private String concepto;
	  private Double porcentaje;
	  private Integer codtipodescuentosalario;
	  private String estado;
	  
	public Integer getCodpagosalario() {
		return codpagosalario;
	}
	public void setCodpagosalario(Integer codpagosalario) {
		this.codpagosalario = codpagosalario;
	}
	public Integer getNroitem() {
		return nroitem;
	}
	public void setNroitem(Integer nroitem) {
		this.nroitem = nroitem;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getCodtipodescuentosalario() {
		return codtipodescuentosalario;
	}
	public void setCodtipodescuentosalario(Integer codtipodescuentosalario) {
		this.codtipodescuentosalario = codtipodescuentosalario;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	  
}