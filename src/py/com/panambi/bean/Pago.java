package py.com.panambi.bean;

import java.sql.Timestamp;
import java.util.List;

public class Pago extends PanambiBean {

	/**
	 * 
	 */
	private Integer codPago;
	private Double monto;
	private Venta venta;
	private Gasto gasto;
	private Compra compra;
	private PagoSalario pagoSalario;
	private DeudaEmpleado deudaEmpleado;
	private Timestamp  fecha;
	private List<DetallePago> detallePago;
	private Integer nroRecibo;
	private String estado;
	private String observaciones;
	private Timestamp fechaAnulacion;
	private Sucursal sucursal;

	public Integer getCodPago() {
		return codPago;
	}

	public void setCodPago(Integer codPago) {
		this.codPago = codPago;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public Gasto getGasto() {
		return gasto;
	}

	public void setGasto(Gasto gasto) {
		this.gasto = gasto;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public PagoSalario getPagoSalario() {
		return pagoSalario;
	}

	public void setPagoSalario(PagoSalario pagoSalario) {
		this.pagoSalario = pagoSalario;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public DeudaEmpleado getDeudaEmpleado() {
		return deudaEmpleado;
	}

	public void setDeudaEmpleado(DeudaEmpleado deudaEmpleado) {
		this.deudaEmpleado = deudaEmpleado;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the detallePago
	 */
	public List<DetallePago> getDetallePago() {
		return detallePago;
	}

	/**
	 * @param detallePago
	 *            the detallePago to set
	 */
	public void setDetallePago(List<DetallePago> detallePago) {
		this.detallePago = detallePago;
	}

	public Integer getNroRecibo() {
		return nroRecibo;
	}

	public void setNroRecibo(Integer nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Timestamp getFechaAnulacion() {
		return fechaAnulacion;
	}

	public void setFechaAnulacion(Timestamp fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}	
	
}
