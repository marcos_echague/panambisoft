package py.com.panambi.bean;

import java.sql.Timestamp;
import java.util.List;

public class Compra extends PanambiBean {
	private Integer codcompra;
	private Timestamp fecha;
	private Double total;
	private Proveedor proveedor;
	private Sucursal sucursal;
	private List<DetalleCompra> detalleCompra;
	private Integer pagaderodias;
	private String estado;
	private String facturanro;
	private Timestamp fechaAnulacion ;
	private String comentarios;

	public Compra() {
	}

	/**
	 * @return the codcompra
	 */
	public Integer getCodcompra() {
		return codcompra;
	}

	/**
	 * @param codcompra
	 *            the codcompra to set
	 */
	public void setCodcompra(Integer codcompra) {
		this.codcompra = codcompra;
	}

	/**
	 * @return the fecha
	 */
	public Timestamp getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor
	 *            the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	/**
	 * @return the detalleCompra
	 */
	public List<DetalleCompra> getDetalleCompra() {
		return detalleCompra;
	}

	/**
	 * @param detalleCompra
	 *            the detalleCompra to set
	 */
	public void setDetalleCompra(List<DetalleCompra> detalleCompra) {
		this.detalleCompra = detalleCompra;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the pagaderodias
	 */
	public Integer getPagaderodias() {
		return pagaderodias;
	}

	/**
	 * @param pagaderodias the pagaderodias to set
	 */
	public void setPagaderodias(Integer pagaderodias) {
		this.pagaderodias = pagaderodias;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the facturanro
	 */
	public String getFacturanro() {
		return facturanro;
	}

	/**
	 * @param facturanro the facturanro to set
	 */
	public void setFacturanro(String facturanro) {
		this.facturanro = facturanro;
	}

	public Timestamp getFechaAnulacion() {
		return fechaAnulacion;
	}

	public void setFechaAnulacion(Timestamp fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	
	
	

}
