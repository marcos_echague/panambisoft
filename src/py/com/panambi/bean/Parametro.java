package py.com.panambi.bean;

public class Parametro extends PanambiBean {

	private Integer codparametro;
	private Float tasaPrestamo;
	private Double sueldoMinimo;
	private Double montoBasePrestamo;
	public Parametro(){
		
	}
	public Integer getCodparametro() {
		return codparametro;
	}
	public void setCodparametro(Integer codparametro) {
		this.codparametro = codparametro;
	}
	
	public Float getTasaPrestamo() {
		return tasaPrestamo;
	}
	public void setTasaPrestamo(Float tasaPrestamo) {
		this.tasaPrestamo = tasaPrestamo;
	}
	public Double getSueldoMinimo() {
		return sueldoMinimo;
	}
	public void setSueldoMinimo(Double sueldoMinimo) {
		this.sueldoMinimo = sueldoMinimo;
	}
	public Double getMontoBasePrestamo() {
		return montoBasePrestamo;
	}
	public void setMontoBasePrestamo(Double montoBasePrestamo) {
		this.montoBasePrestamo = montoBasePrestamo;
	}
	
}
