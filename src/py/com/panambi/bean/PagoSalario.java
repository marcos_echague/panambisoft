package py.com.panambi.bean;

import java.util.Date;
import java.util.List;

public class PagoSalario extends PanambiBean{
	
	  private Integer codpagosalario;
	  private Integer codempleado;	
	  private String nrodocumento;	
	  private String nombre;	
	  private Date fechapago;
	  private Date fechacreacion;
	  private Double totaldescuento;
	  private Double totalpagado;
	  private Double salarionominal;
	  private Integer numeroRecibo;
	  private String estado;//NP: no pagado anulado, P: pagado
	  private Integer numerolista;
	  private Date fechaDesde;
	  private Date fechaHasta;
	  private String tipo; //A:aguinaldo, S:salario, L:liquidacion
	  private List<DetallePagoSalario> detallePagoSalario;
	  
	public Integer getCodpagosalario() {
		return codpagosalario;
	}
	public void setCodpagosalario(Integer codpagosalario) {
		this.codpagosalario = codpagosalario;
	}
	public Integer getCodempleado() {
		return codempleado;
	}
	public void setCodempleado(Integer codempleado) {
		this.codempleado = codempleado;
	}
	public String getNrodocumento() {
		return nrodocumento;
	}
	public void setNrodocumento(String nrodocumento) {
		this.nrodocumento = nrodocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechapago() {
		return fechapago;
	}
	public void setFechapago(Date fechapago) {
		this.fechapago = fechapago;
	}
	public Date getFechacreacion() {
		return fechacreacion;
	}
	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}
	public Double getTotaldescuento() {
		return totaldescuento;
	}
	public void setTotaldescuento(Double totaldescuento) {
		this.totaldescuento = totaldescuento;
	}
	public Double getTotalpagado() {
		return totalpagado;
	}
	public void setTotalpagado(Double totalpagado) {
		this.totalpagado = totalpagado;
	}
	public Double getSalarionominal() {
		return salarionominal;
	}
	public void setSalarionominal(Double salarionominal) {
		this.salarionominal = salarionominal;
	}
	public Integer getNumeroRecibo() {
		return numeroRecibo;
	}
	public void setNumeroRecibo(Integer numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public List<DetallePagoSalario> getDetallePagoSalario() {
		return detallePagoSalario;
	}
	public void setDetallePagoSalario(List<DetallePagoSalario> detallePagoSalario) {
		this.detallePagoSalario = detallePagoSalario;
	}

	public Integer getNumerolista() {
		return numerolista;
	}
	public void setNumerolista(Integer numerolista) {
		this.numerolista = numerolista;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
