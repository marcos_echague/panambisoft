package py.com.panambi.bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Timestamp;

public class Session implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8491277370594815491L;
	private Integer codSession;
	private Connection conn;
	private Usuario usuario;
	private Empleado empleado;
	private Integer pid;
	private Timestamp inicio;
	private Timestamp fin;
	private String hostip;
	private Sucursal sucursalOperativa;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	/**
	 * @return the codsesion
	 */
	public Integer getCodSession() {
		return codSession;
	}

	/**
	 * @param codsesion
	 *            the codsesion to set
	 */
	public void setCodSession(Integer codsession) {
		this.codSession = codsession;
	}

	/**
	 * @return the pid
	 */
	public Integer getPid() {
		return pid;
	}

	/**
	 * @param pid
	 *            the pid to set
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}

	/**
	 * @return the inicio
	 */
	public Timestamp getInicio() {
		return inicio;
	}

	/**
	 * @param inicio
	 *            the inicio to set
	 */
	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}

	/**
	 * @return the fin
	 */
	public Timestamp getFin() {
		return fin;
	}

	/**
	 * @param fin
	 *            the fin to set
	 */
	public void setFin(Timestamp fin) {
		this.fin = fin;
	}

	/**
	 * @return the hostip
	 */
	public String getHostip() {
		return hostip;
	}

	/**
	 * @param hostip
	 *            the hostip to set
	 */
	public void setHostip(String hostip) {
		this.hostip = hostip;
	}

	/**
	 * @return the empleado
	 */
	public Empleado getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado
	 *            the empleado to set
	 */
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	/**
	 * @return the sucursalOperativa
	 */
	public Sucursal getSucursalOperativa() {
		return sucursalOperativa;
	}

	/**
	 * @param sucursalOperativa the sucursalOperativa to set
	 */
	public void setSucursalOperativa(Sucursal sucursal) {
		this.sucursalOperativa = sucursal;
	}

}
