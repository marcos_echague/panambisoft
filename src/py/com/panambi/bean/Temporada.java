package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.exceptions.ValidException;

public class Temporada extends PanambiBean  {
	private Integer codTemporada;
	private String nombre;  
	private String estado;
	
	public Temporada(){
		
	}
	
	public Temporada (Connection conn, Integer codtemporada) throws Exception{
		
		String sql = "SELECT * FROM temporadas WHERE codtemporada = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codtemporada);
		ResultSet rs = ps.executeQuery();
		
		if (rs.next()) {
			setCodTemporada(codtemporada);
			setNombre(rs.getString("nombre"));
			setEstado(rs.getString("estado"));
		} else {
			throw new ValidException("No existe la temporada para el c�digo [" + codtemporada + "]");
		}
		
		rs.close();
		ps.close();
	}
	
	public Integer getCodTemporada() {
		return codTemporada;
	}

	public void setCodTemporada(Integer codTemporada) {
		this.codTemporada = codTemporada;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
