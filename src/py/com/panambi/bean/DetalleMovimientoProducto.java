package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class DetalleMovimientoProducto {
	private Integer codMovimientoProducto;
	private Integer nroItem;
	private Integer cantidad;
	private Integer codProducto;
	
	public DetalleMovimientoProducto() {
	}
	
	public Integer getCodMovimientoProducto() {
		return codMovimientoProducto;
	}
	public void setCodMovimientoProducto(Integer codMovimientoProducto) {
		this.codMovimientoProducto = codMovimientoProducto;
	}
	public Integer getNroitem() {
		return nroItem;
	}
	public void setNroitem(Integer nroitem) {
		this.nroItem = nroitem;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}
	
}
