package py.com.panambi.bean;

import java.sql.Date;

import org.apache.log4j.Logger;

public class PanambiBean {
	
	private Integer auCod;
	private Integer auCodSession;
	private Usuario auUsuario;
	private String auOperacion;
	private Date auFecha;
	
	Logger logger = Logger.getLogger(this.getClass());

	public Integer getAuCod() {
		return auCod;
	}

	public void setAuCod(Integer auCod) {
		this.auCod = auCod;
	}

	public Integer getAuCodSession() {
		return auCodSession;
	}

	public void setAuCodSession(Integer auCodSession) {
		this.auCodSession = auCodSession;
	}

	public Usuario getAuUsuario() {
		return auUsuario;
	}

	public void setAuUsuario(Usuario auUsuario) {
		this.auUsuario = auUsuario;
	}

	public String getAuOperacion() {
		return auOperacion;
	}

	public void setAuOperacion(String auOperacion) {
		this.auOperacion = auOperacion;
	}

	public Date getAuFecha() {
		return auFecha;
	}

	public void setAuFecha(Date auFecha) {
		this.auFecha = auFecha;
	}
	
	

}
