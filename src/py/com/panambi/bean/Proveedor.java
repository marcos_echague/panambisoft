package py.com.panambi.bean;

public class Proveedor extends PanambiBean {
	private Integer codProveedor;
	private String razonSocial;
	private String direccion;
	private String telefono;
	private String nroDocumento;
	private String estado;

	public Proveedor() {
	}

	/**
	 * @return the codProveedor
	 */
	public Integer getCodProveedor() {
		return codProveedor;
	}

	/**
	 * @param codProveedor
	 *            the codProveedor to set
	 */
	public void setCodProveedor(Integer codProveedor) {
		this.codProveedor = codProveedor;
	}

	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial() {
		return razonSocial;
	}

	/**
	 * @param razonSocial
	 *            the razonSocial to set
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion
	 *            the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the nroDocumento
	 */
	public String getNroDocumento() {
		return nroDocumento;
	}

	/**
	 * @param nroDocumento
	 *            the nroDocumento to set
	 */
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return getRazonSocial();
	}

}
