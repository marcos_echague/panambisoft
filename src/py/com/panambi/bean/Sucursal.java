package py.com.panambi.bean;

public class Sucursal extends PanambiBean {
	private Integer codSucursal;
	private String nombre;
	private String direccion;
	private String telefono;
	private String estado;
	private Boolean principal;

	public Sucursal() {
	}

	public Integer getCodSucursal() {
		return codSucursal;
	}

	public void setCodSucursal(Integer codSucursal) {
		this.codSucursal = codSucursal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the principal
	 */
	public Boolean getPrincipal() {
		return principal;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	@Override
	public String toString() {
		return getNombre();
	}
	
	@Override
	public boolean equals(Object obj) {
		Sucursal sucursal = (Sucursal)obj;
		boolean ret = false;
		if (getCodSucursal() != null) {
			ret = (getCodSucursal().equals(sucursal.getCodSucursal()) && getNombre().equals(sucursal.getNombre()));
		}
		return ret;
	}
}
