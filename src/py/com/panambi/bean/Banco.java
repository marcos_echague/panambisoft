package py.com.panambi.bean;

public class Banco extends PanambiBean {

	private Integer codbanco;
	private String descripcion;
	private String estado;

	public Banco() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return getDescripcion();
	}

	/**
	 * @return the codbanco
	 */
	public Integer getCodbanco() {
		return codbanco;
	}

	/**
	 * @param codbanco
	 *            the codbanco to set
	 */
	public void setCodbanco(Integer codbanco) {
		this.codbanco = codbanco;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
