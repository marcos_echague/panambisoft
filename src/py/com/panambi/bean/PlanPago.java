package py.com.panambi.bean;

import java.util.Date;

public class PlanPago extends PanambiBean {
	
	private Integer codPlanPago;
	private Integer nroCuota;
	private double monto;
	private Date vencimiento;
	private Date fechaPago;
	private String estado;
	private Integer codVenta;
		
	public Integer getCodPlanPago() {
		return codPlanPago;
	}
	public void setCodPlanPago(Integer codPlanPago) {
		this.codPlanPago = codPlanPago;
	}
	public Integer getNroCuota() {
		return nroCuota;
	}
	public void setNroCuota(Integer nroCuota) {
		this.nroCuota = nroCuota;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Date getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getCodVenta() {
		return codVenta;
	}
	public void setCodVenta(Integer codVenta) {
		this.codVenta = codVenta;
	}
	
}
