package py.com.panambi.bean;

public class DetalleVenta extends PanambiBean {

	private Integer nroitem;
	private Integer cantidad;
	private Double precioUnitario;
	private Double descuentoUnitario;
	private Double totalItem;
	private Venta venta;
	private Producto producto;
	public Integer getNroitem() {
		return nroitem;
	}
	public void setNroitem(Integer nroitem) {
		this.nroitem = nroitem;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public Double getDescuentoUnitario() {
		return descuentoUnitario;
	}
	public void setDescuentoUnitario(Double descuentoUnitario) {
		this.descuentoUnitario = descuentoUnitario;
	}
	public Double getTotalItem() {
		return totalItem;
	}
	public void setTotalItem(Double totalItem) {
		this.totalItem = totalItem;
	}
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	
}
