package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.exceptions.ValidException;

public class FormaPago extends PanambiBean {

	private Integer codFormaPago;
	private String nombre;
	private String estado;

	public FormaPago() {

	}

	public FormaPago(Connection conn, Integer codformaPago) throws Exception {

		String sql = "SELECT * FROM formaspagos WHERE codformapago = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codformaPago);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			setCodFormaPago(codformaPago);
			setNombre(rs.getString("nombre"));
			setEstado(rs.getString("estado"));
		} else {
			throw new ValidException("No existe la forma de pago para el c�digo [" + codformaPago + "]");
		}
		rs.close();
		ps.close();
	}

	public Integer getCodFormaPago() {
		return codFormaPago;
	}

	public void setCodFormaPago(Integer codFormaPago) {
		this.codFormaPago = codFormaPago;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return getNombre();
	}

}
