package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.exceptions.ValidException;

public class TipoDescuentoSalario extends PanambiBean {
	
	private Integer codTipoDescuento;
	private String nombre;
	private Float porcentaje;
	private Double montoFijo;
	private String estado;
	private String tipoDescuento;
	private String formaAplicacion;
	

	public TipoDescuentoSalario (){
		
	}
	
	public TipoDescuentoSalario (Connection conn , Integer codtipoDes) throws Exception{
		String sql = "SELECT * FROM tiposdescuentossalarios WHERE codtipodescuentosalario = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, codtipoDes);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setCodTipoDescuento((codtipoDes));
			setNombre(rs.getString("nombre"));
			setEstado(rs.getString("estado"));
			setFormaAplicacion(rs.getString("formaaplicacion"));
			setTipoDescuento(rs.getString("tipo"));
			setPorcentaje((rs.getFloat("porcentaje")));
			setMontoFijo(rs.getDouble("montofijo"));
		} else {
			throw new ValidException("No existe el tipo de descuento para el c�digo [" + codtipoDes + "]");
		}
		rs.close();
		ps.close();
	}

	public Integer getCodTipoDescuento() {
		return codTipoDescuento;
	}

	public void setCodTipoDescuento(Integer codTipoDescuento) {
		this.codTipoDescuento = codTipoDescuento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Float getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Float porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoDescuento() {
		return tipoDescuento;
	}

	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}

	public Double getMontoFijo() {
		return montoFijo;
	}

	public void setMontoFijo(Double montoFijo) {
		this.montoFijo = montoFijo;
	}

	public String getFormaAplicacion() {
		return formaAplicacion;
	}

	public void setFormaAplicacion(String formaAplicacion) {
		this.formaAplicacion = formaAplicacion;
	}
	
	

	
}
