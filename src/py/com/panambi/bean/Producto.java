package py.com.panambi.bean;

public class Producto extends PanambiBean {

	private Integer codProducto;
	private String descripcion;
	private double precio;
	private float maxDescuento;
	private Integer stockMinimo;
	private Integer codTemporada;
	private String estado;
	private ImagenProducto imagenProducto;

	public Producto() {

	}

	public Integer getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Integer getStockMinimo() {
		return stockMinimo;
	}

	public void setStockMinimo(Integer stockMinimo) {
		this.stockMinimo = stockMinimo;
	}

	public Integer getCodTemporada() {
		return codTemporada;
	}

	public void setCodTemporada(Integer codTemporada) {
		this.codTemporada = codTemporada;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public ImagenProducto getImagenProducto() {
		return imagenProducto;
	}

	public void setImagenProducto(ImagenProducto imagenProducto) {
		this.imagenProducto = imagenProducto;
	}

	public float getMaxDescuento() {
		return maxDescuento;
	}

	public void setMaxDescuento(float maxDescuento) {
		this.maxDescuento = maxDescuento;
	}

	@Override
	public String toString() {
		return getDescripcion();
	}

}
