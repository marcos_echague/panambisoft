package py.com.panambi.bean;

import java.sql.Date;

public class ProductoPerdido extends PanambiBean {

	private Integer codProductoPerdido;
	private Date fecha;
	private Integer cantidad;
	private String comentario;
	private Producto producto;
	private Usuario usuario;
	private Sucursal sucursal;
	private String estado;
	private Date fechaAnualcion;
	public Integer getCodProductoPerdido() {
		return codProductoPerdido;
	}
	public void setCodProductoPerdido(Integer codProductoPerdido) {
		this.codProductoPerdido = codProductoPerdido;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaAnualcion() {
		return fechaAnualcion;
	}
	public void setFechaAnualcion(Date fechaAnualcion) {
		this.fechaAnualcion = fechaAnualcion;
	}
	 
	
}
