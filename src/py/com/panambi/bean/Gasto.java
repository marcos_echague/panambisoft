package py.com.panambi.bean;

import java.sql.Date;
import java.sql.Timestamp;

public class Gasto extends PanambiBean {
	
	private Integer codGasto;
	private Double importe;
	private TipoGasto tipoGasto;
	private String nroComprobante;
	private Timestamp fechaPago;
	private String comentarios;
	private Date fechaVencimiento;
	private String estado;
	private Timestamp fechaAnulacion;
	private Sucursal sucursal;
	
	public Gasto(){
		
	}

	public Integer getCodGasto() {
		return codGasto;
	}

	public void setCodGasto(Integer codGasto) {
		this.codGasto = codGasto;
	}


	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public TipoGasto getTipoGasto() {
		return tipoGasto;
	}

	public void setTipoGasto(TipoGasto tipoGasto) {
		this.tipoGasto = tipoGasto;
	}

	public String getNroComprobante() {
		return nroComprobante;
	}

	public void setNroComprobante(String nroComprobante) {
		this.nroComprobante = nroComprobante;
	}

	public Timestamp getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Timestamp fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Timestamp getFechaAnulacion() {
		return fechaAnulacion;
	}

	public void setFechaAnulacion(Timestamp fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	
	
}
