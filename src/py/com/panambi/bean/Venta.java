package py.com.panambi.bean;

import java.sql.Timestamp;
import java.util.List;

public class Venta extends PanambiBean {

	private Integer codVenta;
	private Integer nroComprobante;
	private Timestamp fecha;
	private Double total;
	private Double iva;
	private String tipoVenta;
	private String totalLetras;
	private Cliente cliente;
	private Usuario usuario;
	private Sucursal sucursal;
	private String estado;
	private String observaciones;
	private Timestamp fechaAnulacion;
	private Integer nroPagare;
	private List<DetalleVenta> detalle;

	/**
	 * 
	 * @return the codVenta
	 */
	public Integer getCodVenta() {
		return codVenta;
	}

	public void setCodVenta(Integer codVenta) {
		this.codVenta = codVenta;
	}

	public Integer getNroComprobante() {
		return nroComprobante;
	}

	public void setNroComprobante(Integer nroComprobante) {
		this.nroComprobante = nroComprobante;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public String getTipoVenta() {
		return tipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public String getTotalLetras() {
		return totalLetras;
	}

	public void setTotalLetras(String totalLetras) {
		this.totalLetras = totalLetras;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Timestamp getFechaAnulacion() {
		return fechaAnulacion;
	}

	public void setFechaAnulacion(Timestamp fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}
	public Integer getNroPagare() {
		return nroPagare;
	}
	public void setNroPagare(Integer nroPagare) {
		this.nroPagare = nroPagare;
	}

	/**
	 * @return the detalle
	 */
	public List<DetalleVenta> getDetalle() {
		return detalle;
	}

	/**
	 * @param detalle
	 *            the detalle to set
	 */
	public void setDetalle(List<DetalleVenta> detalle) {
		this.detalle = detalle;
	}

}
