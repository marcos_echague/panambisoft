package py.com.panambi.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.exceptions.ValidException;

public class PerfilUsuario extends PanambiBean {
	Integer codPerfil;
	String nombre;
	String estado;
	
	public PerfilUsuario() {
	}

	public PerfilUsuario(Connection conn, Integer codperfil) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM perfiles WHERE codperfil = ? ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, codperfil);
			rs = ps.executeQuery();
			if (rs.next()) {
				setCodPerfil(codperfil);
				setNombre(rs.getString("nombre"));
				setEstado(rs.getString("estado"));
			} else {
				throw new ValidException("No existe el perfil para el c�digo [" + codperfil + "]");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			ConnectionManager.closeResultSets(rs);
			ConnectionManager.closeStatments(ps);
		}
	}


	public Integer getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(Integer codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
