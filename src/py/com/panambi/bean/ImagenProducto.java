package py.com.panambi.bean;

public class ImagenProducto extends PanambiBean {
	
	private Integer codImagen;
	private byte[] imagen;
	private String descripcion;
	private Integer codProducto;
	
	public ImagenProducto(){
		
	}
	
	public Integer getCodImagen() {
		return codImagen;
	}
	public void setCodImagen(Integer codImagen) {
		this.codImagen = codImagen;
	}
	public byte[] getImagen() {
		return imagen;
	}
	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}
	
}
