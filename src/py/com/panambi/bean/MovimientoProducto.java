package py.com.panambi.bean;

import java.util.Date;
import java.util.List;

public class MovimientoProducto extends PanambiBean {
	
	private Integer codMovimientoProducto;
	private Date fecha;
	private String estado; // (ET-AC-RE-AN) (En transito-Aceptado-Rechazado-Anulado)
	private String comentarios;
	private Integer codSucursalOrigen;
	private Integer codSucursalDestino;
	private Integer notaRemision;
	private List<DetalleMovimientoProducto> detalleMovimiento;
	 
	public MovimientoProducto(){
		
	}

	public Integer getCodMovimientoProducto() {
		return codMovimientoProducto;
	}

	public void setCodMovimientoProducto(Integer codMovimientoProducto) {
		this.codMovimientoProducto = codMovimientoProducto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Integer getCodSucursalOrigen() {
		return codSucursalOrigen;
	}

	public void setCodSucursalOrigen(Integer codSucursalOrigen) {
		this.codSucursalOrigen = codSucursalOrigen;
	}

	public Integer getCodSucursalDestino() {
		return codSucursalDestino;
	}

	public void setCodSucursalDestino(Integer codSucursalDestino) {
		this.codSucursalDestino = codSucursalDestino;
	}

	public List<DetalleMovimientoProducto> getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(List<DetalleMovimientoProducto> detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}

	public Integer getNotaRemision() {
		return notaRemision;
	}

	public void setNotaRemision(Integer notaRemision) {
		this.notaRemision = notaRemision;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}