package py.com.panambi.bean;

public class DetallePago extends PanambiBean {

	private Integer nroitem;
	private Double monto;
	private FormaPago formaPago;
	private String chequenro;
	private Banco banco;
	private String bouchernro;
	private Integer codpago;
	private Integer notacredito;

	public DetallePago() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the nroitem
	 */
	public Integer getNroitem() {
		return nroitem;
	}

	/**
	 * @param nroitem
	 *            the nroitem to set
	 */
	public void setNroitem(Integer nroitem) {
		this.nroitem = nroitem;
	}

	/**
	 * @return the monto
	 */
	public Double getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(Double monto) {
		this.monto = monto;
	}

	/**
	 * @return the formaPago
	 */
	public FormaPago getFormaPago() {
		return formaPago;
	}

	/**
	 * @param formaPago
	 *            the formaPago to set
	 */
	public void setFormaPago(FormaPago formaPago) {
		this.formaPago = formaPago;
	}

	/**
	 * @return the chequenro
	 */
	public String getChequenro() {
		return chequenro;
	}

	/**
	 * @param chequenro
	 *            the chequenro to set
	 */
	public void setChequenro(String chequenro) {
		this.chequenro = chequenro;
	}

	/**
	 * @return the banco
	 */
	public Banco getBanco() {
		return banco;
	}

	/**
	 * @param banco
	 *            the banco to set
	 */
	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	/**
	 * @return the bouchernro
	 */
	public String getBouchernro() {
		return bouchernro;
	}

	/**
	 * @param bouchernro
	 *            the bouchernro to set
	 */
	public void setBouchernro(String bouchernro) {
		this.bouchernro = bouchernro;
	}

	/**
	 * @return the codpago
	 */
	public Integer getCodpago() {
		return codpago;
	}

	/**
	 * @param codpago
	 *            the codpago to set
	 */
	public void setCodpago(Integer codpago) {
		this.codpago = codpago;
	}

	public Integer getNotacredito() {
		return notacredito;
	}

	public void setNotacredito(Integer notacredito) {
		this.notacredito = notacredito;
	}
	
	

}
