package py.com.panambi.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.connect.MD5;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.InvalidUserException;
import py.com.panambi.exceptions.LoginException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.JTextPassword;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.JDialogPanambi;

public class DlgLoginValidaAdministrador extends JDialogPanambi implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610850113222236567L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblUsuario;
	private JLabel lblPassword;
	private JTextFieldUpper jTxtUsuario;
	private JTextPassword jTxtPassword;
	private JButtonPanambi jBtnOk;
	private JButtonPanambi jBtnCancelar;
	private JLabel label;
	private boolean validada;
	private JPanel panel;
	private JLabel labelWarning;
	private JTextArea lblElCambioDe;

	/**
	 * @return the validada
	 */
	public boolean isValidada() {
		return validada;
	}

	/**
	 * @param validada the validada to set
	 */
	public void setValidada(boolean valido) {
		this.validada = valido;
	}

	/**
	 * Create the dialog.
	 */
	public DlgLoginValidaAdministrador() {
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgLoginValidaAdministrador(Frame owner) {
		super(owner);
		initialize();
	}

	private void initialize() {
		setResizable(false);
		setTitle("Login - Administrador");
		setName("DlgLogin");
		setBounds(100, 100, 340, 281);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(30)
							.addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addGap(42)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblUsuario())
								.addComponent(getLblPassword()))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(getJTxtPassword(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(getPanel(), GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(22)
							.addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(41)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblUsuario())
								.addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblPassword())
								.addComponent(getJTxtPassword(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 94, Short.MAX_VALUE)
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.add(getJBtnOk());
			buttonPane.add(getJBtnCancelar());
		}

		setShortcutsHere(this);
		centerIt();
	}

	private JLabel getLblUsuario() {
		if (lblUsuario == null) {
			lblUsuario = new JLabel("Usuario");
		}
		return lblUsuario;
	}

	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Password");
		}
		return lblPassword;
	}

	private JTextFieldUpper getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextFieldUpper();
		}
		return jTxtUsuario;
	}

	private JTextPassword getJTxtPassword() {
		if (jTxtPassword == null) {
			jTxtPassword = new JTextPassword();
			jTxtPassword.setActionCommand("OK");
			jTxtPassword.addActionListener(this);
		}
		return jTxtPassword;
	}

	private void setShortcutsHere(Container component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doCancelar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private JButtonPanambi getJBtnOk() {
		if (jBtnOk == null) {
			jBtnOk = new JButtonPanambi();
			jBtnOk.setMnemonic('A');
			jBtnOk.setText("Aceptar");
			jBtnOk.setActionCommand("OK");
			jBtnOk.addActionListener(this);
		}
		return jBtnOk;
	}

	private JButtonPanambi getJBtnCancelar() {
		if (jBtnCancelar == null) {
			jBtnCancelar = new JButtonPanambi();
			jBtnCancelar.setMnemonic('C');
			jBtnCancelar.setText("Cancelar");
			jBtnCancelar.setActionCommand("CANCEL");
			jBtnCancelar.addActionListener(this);
		}
		return jBtnCancelar;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("OK".equals(cmd)) {
			doAceptar();
		}
		if ("CANCEL".equals(cmd)) {
			doCancelar();
		}
	}

	private void doCancelar() {
		this.dispose();
	}

	/**
	 * Valida usuario y deja en primer plano a la aplicación principal
	 */
	private void doAceptar() {
		try {
			validada = false;
			String passwding = new String(getJTxtPassword().getPassword());
			MD5 md5 = new MD5();
			md5.init();
			md5.update(passwding.toCharArray(), passwding.toCharArray().length);
			md5.md5final();
			String encpasswd = new String(md5.toHexString());
			String nombreUsuario = getJTxtUsuario().getText().trim();
			Usuario usuario = null;
			ControladorUsuario controladorUsuario = new ControladorUsuario();
			try {
				usuario = new Usuario(JFramePanambiMain.session.getConn(), nombreUsuario);
				validada = ConnectionManager.validateUserAdministrator(JFramePanambiMain.session.getConn(), getJTxtUsuario().getText().trim(), encpasswd);
				if (!validada) {
					if (usuario.getEstado().equals("A")) {
						controladorUsuario.addIntentosFallidos(JFramePanambiMain.session.getConn(), usuario);
						throw new LoginException("Usuario o password incorrecto.");
					} else if (usuario.getEstado().equals("I")) {
						throw new LoginException("Usuario bloqueado.");
					} else {
						throw new LoginException("Usuario o password incorrecto.");
					}
				}
			} catch (InvalidUserException invaliduser) {
				throw new LoginException("Usuario o password incorrecto.");
			}
			controladorUsuario.resetIntentosFallidos(JFramePanambiMain.session.getConn(), usuario);
			this.dispose();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("");
			ImageIcon imgIcon = new ImageIcon(DlgLoginValidaAdministrador.class.getResource("/py/com/panambi/images/lock.png"));
			label.setIcon(imgIcon);
		}
		return label;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(getLabelWarning())
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(getLblElCambioDe(), GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
						.addGap(25))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addComponent(getLblElCambioDe(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(getLabelWarning())))
						.addContainerGap(14, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
		}
		return panel;
	}
	private JLabel getLabelWarning() {
		if (labelWarning == null) {
			labelWarning = new JLabel("");
			labelWarning.setIcon(new ImageIcon(DlgLoginValidaAdministrador.class.getResource("/py/com/panambi/images/Warning.png")));
		}
		return labelWarning;
	}
	private JTextArea getLblElCambioDe() {
		if (lblElCambioDe == null) {
			lblElCambioDe = new JTextArea("El cambio de sucursal operativa har\u00E1 que se cierren todas las ventanas activas.");
			lblElCambioDe.setWrapStyleWord(true);
			lblElCambioDe.setEnabled(false);
			lblElCambioDe.setEditable(false);
			lblElCambioDe.setLineWrap(true);
			lblElCambioDe.setBackground(getParent().getBackground());
			lblElCambioDe.setOpaque(false);
			lblElCambioDe.setDisabledTextColor(Color.BLACK);
		}
		return lblElCambioDe;
	}
}
