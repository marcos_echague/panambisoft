package py.com.panambi.login;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.Inet4Address;
import java.sql.Connection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;

import py.com.panambi.bean.Empleado;
import py.com.panambi.bean.Session;
import py.com.panambi.bean.Usuario;
import py.com.panambi.connect.ConnectionManager;
import py.com.panambi.connect.EncryptDecrypt;
import py.com.panambi.connect.MD5;
import py.com.panambi.controller.ControladorEmpleado;
import py.com.panambi.controller.ControladorSession;
import py.com.panambi.controller.ControladorUsuario;
import py.com.panambi.exceptions.InvalidUserException;
import py.com.panambi.exceptions.LoginException;
import py.com.panambi.main.JFramePanambiMain;
import py.com.panambi.swing.components.JButtonPanambi;
import py.com.panambi.swing.components.JTextFieldUpper;
import py.com.panambi.swing.components.JTextPassword;
import py.com.panambi.swing.windows.DlgMessage;
import py.com.panambi.swing.windows.DlgPasswordRequest;
import py.com.panambi.swing.windows.JDialogPanambi;

public class DlgLogin extends JDialogPanambi implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610850113222236567L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblUsuario;
	private JLabel lblPassword;
	private JTextFieldUpper jTxtUsuario;
	private JTextPassword jTxtPassword;
	private JButtonPanambi jBtnOk;
	private JButtonPanambi jBtnCancelar;
	private JLabel label;

	/**
	 * Create the dialog.
	 */
	public DlgLogin() {
		initialize();
	}

	/**
	 * @param owner
	 */
	public DlgLogin(Frame owner) {
		super(owner);
		initialize();
	}

	private void initialize() {
		setResizable(false);
		setTitle("Login");
		setName("DlgLogin");
		setBounds(100, 100, 340, 210);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addGroup(
				gl_contentPanel.createSequentialGroup().addGap(30).addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE).addGap(42).addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING).addComponent(getLblUsuario()).addComponent(getLblPassword()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false).addComponent(getJTxtPassword(), GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(92, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addGroup(
				gl_contentPanel
						.createSequentialGroup()
						.addGroup(
								gl_contentPanel
										.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPanel.createSequentialGroup().addGap(22).addComponent(getLabel(), GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
										.addGroup(
												gl_contentPanel.createSequentialGroup().addGap(41)
														.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE).addComponent(getLblUsuario()).addComponent(getJTxtUsuario(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE).addComponent(getLblPassword()).addComponent(getJTxtPassword(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(30, Short.MAX_VALUE)));
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.add(getJBtnOk());
			buttonPane.add(getJBtnCancelar());
		}

		setShortcutsHere(this);
		centerIt();
	}

	private JLabel getLblUsuario() {
		if (lblUsuario == null) {
			lblUsuario = new JLabel("Usuario");
		}
		return lblUsuario;
	}

	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Password");
		}
		return lblPassword;
	}

	private JTextFieldUpper getJTxtUsuario() {
		if (jTxtUsuario == null) {
			jTxtUsuario = new JTextFieldUpper();
		}
		return jTxtUsuario;
	}

	private JTextPassword getJTxtPassword() {
		if (jTxtPassword == null) {
			jTxtPassword = new JTextPassword();
			jTxtPassword.setActionCommand("OK");
			jTxtPassword.addActionListener(this);
		}
		return jTxtPassword;
	}

	private void setShortcutsHere(Container component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doCancelar();
				}
				super.keyPressed(e);
			}
		});

		if (component instanceof Container) {
			for (int i = 0; i < component.getComponents().length; i++) {
				if (component.getComponents()[i] instanceof JComponent) {
					JComponent comp = (JComponent) component.getComponents()[i];
					setShortcutsHere(comp);
				}
			}
		}
	}

	private JButtonPanambi getJBtnOk() {
		if (jBtnOk == null) {
			jBtnOk = new JButtonPanambi();
			jBtnOk.setMnemonic('A');
			jBtnOk.setText("Aceptar");
			jBtnOk.setActionCommand("OK");
			jBtnOk.addActionListener(this);
		}
		return jBtnOk;
	}

	private JButtonPanambi getJBtnCancelar() {
		if (jBtnCancelar == null) {
			jBtnCancelar = new JButtonPanambi();
			jBtnCancelar.setMnemonic('C');
			jBtnCancelar.setText("Cancelar");
			jBtnCancelar.setActionCommand("CANCEL");
			jBtnCancelar.addActionListener(this);
		}
		return jBtnCancelar;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("OK".equals(cmd)) {
			doAceptar();
		}
		if ("CANCEL".equals(cmd)) {
			doCancelar();
		}
	}

	private void doCancelar() {
		this.dispose();
		getOwner().dispose();
	}

	/**
	 * Valida usuario y deja en primer plano a la aplicación principal
	 */
	private void doAceptar() {
		try {
			String passwding = new String(getJTxtPassword().getPassword());
			String masterpasswd = EncryptDecrypt.decrypt(JFramePanambiMain.appproperties.getProperty("mstrpasswd"));
			MD5 md5 = new MD5();
			md5.init();
			md5.update(passwding.toCharArray(), passwding.toCharArray().length);
			md5.md5final();
			String encpasswd = new String(md5.toHexString());
			Connection conn = ConnectionManager.getConnection(JFramePanambiMain.appproperties.getProperty("mstruser"), masterpasswd, JFramePanambiMain.appproperties.getProperty("dbhost"), JFramePanambiMain.appproperties.getProperty("dbname"));
			String nombreUsuario = getJTxtUsuario().getText().trim();
			Usuario usuario = null;
			ControladorUsuario controladorUsuario = new ControladorUsuario();
			try {
				usuario = new Usuario(conn, nombreUsuario);
				boolean valido = ConnectionManager.validateUser(conn, getJTxtUsuario().getText().trim(), encpasswd);
				if (!valido) {
					if (usuario.getEstado().equals("A")) {
						controladorUsuario.addIntentosFallidos(conn, usuario);
						throw new LoginException("Usuario o password incorrecto.");
					} else if (usuario.getEstado().equals("I")) {
						throw new LoginException("Usuario bloqueado.");
					} else {
						throw new LoginException("Usuario o password incorrecto.");
					}
				}
			} catch (InvalidUserException invaliduser) {
				throw new LoginException("Usuario o password incorrecto.");
			}
			controladorUsuario.resetIntentosFallidos(conn, usuario);
			JFramePanambiMain.session = new Session();
			JFramePanambiMain.session.setUsuario(usuario);
			JFramePanambiMain.session.setConn(conn);
			JFramePanambiMain.session.setHostip(Inet4Address.getLocalHost().getHostAddress());

			ControladorSession controladorSession = new ControladorSession();
			Integer codsession = controladorSession.insertarSesion(conn, JFramePanambiMain.session);
			JFramePanambiMain.session = controladorSession.getSession(conn, codsession);

			ControladorEmpleado controladorEmpleado = new ControladorEmpleado();
			Empleado empleado = controladorEmpleado.getEmpleadoDeUsuario(conn, usuario.getCodUsuario());
			JFramePanambiMain.session.setEmpleado(empleado);
			if (empleado != null) {
				JFramePanambiMain.session.setSucursalOperativa(empleado.getSucursal());
				((JFramePanambiMain)getOwner()).setMainTitle();
				((JFramePanambiMain)getOwner()).markSucursal();
			}
			// Registrando inicio de sesion
			if (usuario.getReset() == false) {
				DlgPasswordRequest dlgPasswordRequest = new DlgPasswordRequest((JFrame) getOwner(), true);
				dlgPasswordRequest.setVisible(true);
			}

			this.dispose();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			DlgMessage.showMessage(this, e, DlgMessage.ERROR_MESSAGE);
		}
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("");
			ImageIcon imgIcon = new ImageIcon(DlgLogin.class.getResource("/py/com/panambi/images/lock.png"));
			label.setIcon(imgIcon);
		}
		return label;
	}

}
