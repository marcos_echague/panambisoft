package py.com.panambi.connect;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.postgresql.util.Base64;

public class EncryptDecrypt {

	private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
	private static byte[] key = { 0x53, 0x55, 0x50, 0x45, 0x52, 0x6b, 0x47, 0x65, 0x6e, 0x50, 0x61, 0x6e, 0x38, 0x30, 0x21, 0x32 };// "SUPERkGenPan80!2";

	public static String encrypt(String strToEncrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			final String encryptedString = Base64.encodeBytes(strToEncrypt.getBytes());
			return encryptedString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static String decrypt(String strToDecrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			final String decryptedString = new String(Base64.decode(strToDecrypt));
			return decryptedString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String hexadecimal(String input, String charsetName) throws Exception {
		if (input == null)
			throw new NullPointerException();
		return asHex(input.getBytes(charsetName));
	}

	public static String asHex(byte[] buf) {
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}

}
