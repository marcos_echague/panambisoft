package py.com.panambi.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConnectionManager {
	static Logger logger = Logger.getLogger(ConnectionManager.class);

	/**
	 * Retorna una conexi�n a la base de datos validando usuario y password de
	 * sistema.
	 * 
	 * @param user
	 * @param password
	 * @return
	 */
	public static Connection getConnection(String user, String password, String dbhost, String dbname) throws Exception {
		Connection ret = null;
		try {
			String url = "jdbc:postgresql://" + dbhost + "/" + dbname;
			Properties props = new Properties();
			props.setProperty("user", user);
			props.setProperty("password", password);
			ret = DriverManager.getConnection(url, props);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return ret;
	}

	public static boolean validateUser(Connection conn, String user, String password) throws Exception {
		boolean ret = false;
		String sSel = "SELECT COUNT(1) ";
		sSel += "FROM usuarios ";
		sSel += "WHERE usuario = ? ";
		sSel += "AND passwd = ? ";
		sSel += "AND estado = ? ";
		PreparedStatement ps = conn.prepareStatement(sSel);
		ps.setString(1, user);
		ps.setString(2, password);
		ps.setString(3, "A");
		ResultSet rs = ps.executeQuery();
		if (rs.next() && rs.getInt(1) == 1) {
			ret = true;
		}
		rs.close();
		ps.close();
		return ret;
	}
	
	public static boolean validateUserAdministrator(Connection conn, String user, String password) throws Exception {
		boolean ret = false;
		String sSel = "SELECT COUNT(1) ";
		sSel += "FROM usuarios ";
		sSel += "WHERE usuario = ? ";
		sSel += "AND passwd = ? ";
		sSel += "AND estado = ? ";
		sSel += "AND codusuario in ";
		sSel += "    (SELECT codusuario FROM usuarios_perfiles ";
		sSel += "    WHERE codperfil_perfiles = (SELECT codperfil FROM perfiles WHERE nombre = 'ADMINISTRADOR' and estado = 'A')) ";
		PreparedStatement ps = conn.prepareStatement(sSel);
		ps.setString(1, user);
		ps.setString(2, password);
		ps.setString(3, "A");
		ResultSet rs = ps.executeQuery();
		if (rs.next() && rs.getInt(1) == 1) {
			ret = true;
		}
		rs.close();
		ps.close();
		return ret;
	}


	public static void closeStatments(Statement... st) {
		for (Statement statement : st) {
			try {
				statement.close();
			} catch (Exception e) {
			}
		}
	}
	
	public static void closeResultSets(ResultSet... rs) {
		for (ResultSet resultset : rs) {
			try {
				resultset.close();
			} catch (Exception e) {
			}
		}
	}


}
