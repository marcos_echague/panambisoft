package py.com.panambi.exceptions;

public class PanambiException extends Exception {

	public PanambiException() {
		// TODO Auto-generated constructor stub
	}

	public PanambiException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PanambiException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PanambiException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PanambiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
