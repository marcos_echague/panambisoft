package py.com.panambi.exceptions;

public class ValidException extends PanambiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8628969342840113430L;

	public ValidException() {
		// TODO Auto-generated constructor stub
	}

	public ValidException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValidException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ValidException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
