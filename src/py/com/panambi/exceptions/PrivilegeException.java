package py.com.panambi.exceptions;

public class PrivilegeException extends PanambiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5283485611563084600L;

	public PrivilegeException() {
		// TODO Auto-generated constructor stub
	}

	public PrivilegeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PrivilegeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PrivilegeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PrivilegeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
