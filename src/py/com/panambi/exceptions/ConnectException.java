/**
 * 
 */
package py.com.panambi.exceptions;

/**
 * Exception generada al ocurrir un error en el login 
 */
public class ConnectException extends PanambiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2931470792699653467L;

	public ConnectException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConnectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ConnectException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConnectException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConnectException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
